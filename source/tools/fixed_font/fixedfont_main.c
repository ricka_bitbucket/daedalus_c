#include <inttypes.h>
#include <stdio.h>

#include "core/types.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../../examples/metal_render_to_file/stb_image.h"

int main(int argc, const char* argv[]) {
  const char* file_path = "../../../source/tools/fixed_font/charmap-oldschool_white.png";

  int width, height, comp;
  stbi_uc* data = stbi_load(file_path, &width, &height, &comp, 3);
  if (!data) {
    fprintf(stderr, "Couldn't load image from '%s'\n", file_path);
    return 1;
  }

  const int char_step_x = 7;
  // const int padding_x = 1;
  const int char_step_y = 9;
  // const int padding_y = 1;

  int char_x = 1;
  int char_y = 1;

  int c = 0;
  for (; c <= '~'; c++) {
    uint64_t total = 0;

    for (int y = 0; y < 7; y++) {
      uint64 line_val    = 0;
      char line_debug[6] = {0};
      for (int x = 0; x < 5; x++) {
        if (data[((char_y + y) * width + char_x + x) * comp]) {
#if defined(_WIN32)
          line_val = line_val | (int64)1 << (5 - x - 1);
#else
          line_val = line_val | (int64)1 << x;
#endif
          line_debug[x] = '1';
        } else {
          line_debug[x] = '0';
        }
      }
      // printf("%s\n", line_debug);
      total = total | (line_val << y * 5);
    }

    printf("0x%" PRIx32 ",0x%" PRIx32 ",\n", (uint32_t)(total & 0xFFFFFFFF),
           (uint32_t)((total >> 32) & 0xFFFFFFFF));

    char_x += char_step_x;
    if ((char_x + char_step_x) > width) {
      char_y += char_step_y;
      char_x = 1;
    }
  }
  for (; c < 256; c++) {
    uint64 total = 0;
    printf("0x%" PRIx32 ",0x%" PRIx32 ",\n", (uint32_t)(total & 0xFFFFFFFF),
           (uint32_t)((total >> 32) & 0xFFFFFFFF));
  }
  return 0;
}
