#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/containers.h"
#include "core/dd_math.h"
#include "core/log_remote.h"
#include "core/memory.inl"
#include "core/network.h"
#include "core/profiler.h"
#include "core/profiler.inl"
#include "core/thread.h"
#include "core/timer.h"
#include "core/types.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "hub.inl"
#include "telemetry/telemetry_server.h"
#include "telemetry/telemetry_types.inl"
#include "ui/ui.h"

struct memory_table_headers {
  int16 timestamp_width;
  int16 size_width;
  int16 filename_width;

  bool should_combine_locations;

  uint32 selected_row;
  uint32 sort_by;
  int32 sort_direction;
};

enum { COLUMN_MARGIN = 5 };

static void draw_memory_buttons(dd_ui_t* ui, ui_rect_t rect, struct dd_telemetry_memory_t* memory,
                                struct memory_table_headers* in_out_headers) {
  rect.width = rect.height;
  ui_api->widget.checkbox(ui, rect, stringview("combine"), __LINE__,
                          &in_out_headers->should_combine_locations);
}

static void draw_memory_table_headers(dd_ui_t* ui, ui_rect_t rect,
                                      struct dd_telemetry_memory_t* memory,
                                      struct memory_table_headers* in_out_headers) {
  g_draw2d_api_->rect(&ui->draw_data, rect, ui->theme->status_bar.color_success);

  const ui_rect_t header_rect = rect;

  uint32 id = __LINE__;

  ui_rect_t rect_timestamp =
      rect_split_off_left(&rect, in_out_headers->timestamp_width, COLUMN_MARGIN);
  ui_rect_t rect_size     = rect_split_off_left(&rect, in_out_headers->size_width, COLUMN_MARGIN);
  ui_rect_t rect_filename = rect;
  // rect_split_off_left(&rect, in_out_headers->filename_width, COLUMN_MARGIN);

  if (rect_contains_point(header_rect, ui->mouse.x, ui->mouse.y)) {
    ui_api->interaction.set_next_hover(ui, ui->current_ui_layer, id);

    if (ui->hover_id == id) {
      if (ui->event.event_type == UIEventMouseButton0Up) {
        if (rect_contains_point(rect_size, ui->mouse.x, ui->mouse.y)) {
          if (in_out_headers->sort_by == 1) {
            in_out_headers->sort_direction = -in_out_headers->sort_direction;
          } else {
            in_out_headers->sort_by        = 1;
            in_out_headers->sort_direction = 1;
          }
        } else if (rect_contains_point(rect_filename, ui->mouse.x, ui->mouse.y)) {
          if (in_out_headers->sort_by == 2) {
            in_out_headers->sort_direction = -in_out_headers->sort_direction;
          } else {
            in_out_headers->sort_by        = 2;
            in_out_headers->sort_direction = 1;
          }
        } else {
          if (in_out_headers->sort_by == 0) {
            in_out_headers->sort_direction = -in_out_headers->sort_direction;
          } else {
            in_out_headers->sort_by        = 0;
            in_out_headers->sort_direction = 1;
          }

          for (uint32 block_idx = 0; block_idx < array_count(memory->stored_buffers);
               block_idx++) {
            const struct telemetry_memory_processed_t* block = memory->stored_buffers + block_idx;
            uint8* entry_it                                  = block->data;
            uint8* entry_end                                 = entry_it + block->byte_count;
            const uint32 entries_byte_count                  = *(uint32*)entry_it;
            entry_it += sizeof(uint32);
            entry_end = entry_it + entries_byte_count;
            while (entry_it < entry_end) {
              switch (*(entry_it++)) {
                case MEMORY_SENTINAL_THREAD_ID: {
                  entry_it += sizeof(dd_memory_threaddata_t);
                } break;
                case MEMORY_SENTINAL_ALLOC: {
                  dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)entry_it;
                  entry_it += sizeof(dd_memory_alloc_data_t);

                  dd_devprintf("allo;%p;%u\n", ad->address_returned, ad->byte_count);
                } break;
                case MEMORY_SENTINAL_FREE: {
                  dd_memory_free_data_t* fd = (dd_memory_free_data_t*)entry_it;
                  entry_it += sizeof(*fd);

                  dd_devprintf("free;%p;%u\n", fd->address_freed, fd->byte_count);
                } break;
              }
            }
          }
        }
      }
    }
  }

  ui_api->widget.label(ui, rect_timestamp, ui->theme->foreground.color, TextAlignmentLeft,
                       stringview("Time"));
  ui_api->widget.label(ui, rect_size, ui->theme->foreground.color, TextAlignmentLeft,
                       stringview("Size"));
  ui_api->widget.label(ui, rect_filename, ui->theme->foreground.color, TextAlignmentLeft,
                       stringview("Filename"));
}

static void draw_memory_table(dd_ui_t* ui, ui_rect_t rect, struct dd_telemetry_memory_t* memory,
                              const struct memory_table_headers* headers, uint32 id) {
  // Find memory usage before active view
  uint32 block_count = (uint32)array_count(memory->stored_buffers);
  uint32 block_idx   = 0;
  uint8* entry_it    = NULL;
  uint8* entry_end   = NULL;

  char line_buffer[1024] = {0};

  ui_rect_t line = rect;
  line.height    = 8;

  // const ui_rect_t original_line = line;
  ui_rect_t rect_timestamp = rect_split_off_left(&line, headers->timestamp_width, COLUMN_MARGIN);
  ui_rect_t rect_size      = rect_split_off_left(&line, headers->size_width, COLUMN_MARGIN);
  ui_rect_t rect_filename  = line;
  // rect_split_off_left(&line, headers->filename_width, COLUMN_MARGIN);

  float y     = line.y;
  float end_y = rect.y + rect.height;

  static float view_line_offset = 0;

  const float visible_lines = (float)(1 + rect.height / (line.height + 1));
  float total_entry_count   = 0;
  for (block_idx = 0; block_idx < block_count; block_idx++) {
    total_entry_count += memory->stored_buffers[block_idx].entry_count;
  }

  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_api->interaction.set_next_hover(ui, ui->current_ui_layer, id);

    if (ui->hover_id == id) {
      if (ui->event.event_type == UIEventMouseScroll) {
        int32 delta = (*(int32*)&(ui->event.data));
        if (delta < 0) {
          view_line_offset = (float)min(total_entry_count - visible_lines, view_line_offset + 3);
        } else {
          if (view_line_offset > 3) {
            view_line_offset -= 3;
          } else {
            view_line_offset = 0;
          }
        }
      }
    }
  }

  ui_rect_t rect_scrollbar = rect_split_off_right(&rect, UIScrollbarThickness, 1);

  uint32 current_line = 0;
  block_idx           = 0;
  while (block_idx < block_count) {
    const struct telemetry_memory_processed_t* block = memory->stored_buffers + block_idx;
    entry_it                                         = block->data;
    const uint32 entries_byte_count                  = *(uint32*)entry_it;
    entry_it += sizeof(uint32);
    entry_end = entry_it + entries_byte_count;
    while (entry_it < entry_end && rect_timestamp.y < end_y) {
      switch (*(entry_it++)) {
        case MEMORY_SENTINAL_THREAD_ID: {
          entry_it += sizeof(dd_memory_threaddata_t);
        } break;
        case MEMORY_SENTINAL_ALLOC: {
          dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)entry_it;
          entry_it += sizeof(dd_memory_alloc_data_t);

          if (current_line >= view_line_offset) {
            rect_timestamp.y = rect_size.y = rect_filename.y = y;
            snprintf(line_buffer, sizeof(line_buffer) - 1, "%llu", ad->timestamp);
            ui_api->widget.label(ui, rect_timestamp, ui->theme->foreground.color,
                                 TextAlignmentLeft, stringview(line_buffer));
            snprintf(line_buffer, sizeof(line_buffer) - 1, "alloc %u", ad->byte_count);
            ui_api->widget.label(ui, rect_size, ui->theme->foreground.color, TextAlignmentLeft,
                                 stringview(line_buffer));
            snprintf(line_buffer, sizeof(line_buffer) - 1, "%s (%u)", ad->file_name,
                     ad->line_number);
            ui_api->widget.label(ui, rect_filename, ui->theme->foreground.color, TextAlignmentLeft,
                                 stringview(line_buffer));

            y += line.height + 1;
          }

          ++current_line;
        } break;
        case MEMORY_SENTINAL_FREE: {
          dd_memory_free_data_t* fd = (dd_memory_free_data_t*)entry_it;
          entry_it += sizeof(dd_memory_free_data_t);

          if (current_line >= view_line_offset) {
            rect_timestamp.y = rect_size.y = y;
            snprintf(line_buffer, sizeof(line_buffer) - 1, "%llu", fd->timestamp);
            ui_api->widget.label(ui, rect_timestamp, ui->theme->foreground.color,
                                 TextAlignmentLeft, stringview(line_buffer));
            snprintf(line_buffer, sizeof(line_buffer) - 1, "free %u", fd->byte_count);
            ui_api->widget.label(ui, rect_size, ui->theme->foreground.color, TextAlignmentLeft,
                                 stringview(line_buffer));

            y += line.height + 1;
          }

          ++current_line;
        } break;
      }
    }

    ++block_idx;
  }

  ui_api->widget.scrollbar_vertical(ui, rect_scrollbar, visible_lines, total_entry_count, 1,
                                    &view_line_offset, __LINE__);
}

struct {
  dd_memory_alloc_data_t** alloc_data;
  uint32* total_allocation;
  uint32* allocation_count;
} hack_for_sorting_data;

int compare_size(const void* a_in, const void* b_in) {
  uint32 a = *(uint32*)a_in;
  uint32 b = *(uint32*)b_in;

  return (int)(hack_for_sorting_data.total_allocation[a] -
               hack_for_sorting_data.total_allocation[b]);
}

int compare_location(const void* a_in, const void* b_in) {
  uint32 a = *(uint32*)a_in;
  uint32 b = *(uint32*)b_in;

  int r = strcmp(hack_for_sorting_data.alloc_data[a]->file_name,
                 hack_for_sorting_data.alloc_data[b]->file_name);
  if (r == 0) {
    r = (int)(hack_for_sorting_data.alloc_data[a]->line_number -
              hack_for_sorting_data.alloc_data[b]->line_number);
  }
  return r;
}

static void draw_memory_table_combined(dd_ui_t* ui, ui_rect_t rect,
                                       struct dd_telemetry_memory_t* memory,
                                       struct memory_table_headers* headers, uint32 id) {
  // Find memory usage before active view
  uint32 block_count = (uint32)array_count(memory->stored_buffers);

  char line_buffer[1024] = {0};

  ui_rect_t line      = rect;
  line.height         = 8;
  ui_rect_t full_line = line;

  // const ui_rect_t original_line = line;
  ui_rect_t rect_timestamp = rect_split_off_left(&line, headers->timestamp_width, COLUMN_MARGIN);
  ui_rect_t rect_size      = rect_split_off_left(&line, headers->size_width, COLUMN_MARGIN);
  ui_rect_t rect_filename  = line;
  // rect_split_off_left(&line, headers->filename_width, COLUMN_MARGIN);

  float y     = line.y;
  float end_y = rect.y + rect.height;

  static uint32 view_line_offset = 0;
  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_api->interaction.set_next_hover(ui, ui->current_ui_layer, id);

    if (ui->hover_id == id) {
      if (ui->event.event_type == UIEventMouseScroll) {
        int32 delta = (*(int32*)&(ui->event.data));
        if (delta < 0) {
          view_line_offset++;
        } else {
          if (view_line_offset > 0) {
            view_line_offset--;
          }
        }
      } else if (ui->event.event_type == UIEventMouseButton0Up) {
        headers->selected_row = (uint32)(1 + (ui->mouse.y - rect.y) / (line.height + 1));
      }
    }
  }

  // Find active allocations
  dd_hash64_t allocation_to_alloc_data = {0};

  uint32 running_total = 0;
  for (uint32 block_idx = 0; block_idx < block_count; block_idx++) {
    const struct telemetry_memory_processed_t* block = memory->stored_buffers + block_idx;
    uint8* entry_it                                  = block->data;
    uint8* entry_end                                 = entry_it + block->byte_count;
    const uint32 entries_byte_count                  = *(uint32*)entry_it;
    entry_it += sizeof(uint32);
    entry_end = entry_it + entries_byte_count;
    while (entry_it < entry_end && rect_timestamp.y < end_y) {
      switch (*(entry_it++)) {
        case MEMORY_SENTINAL_THREAD_ID: {
          entry_it += sizeof(dd_memory_threaddata_t);
        } break;
        case MEMORY_SENTINAL_ALLOC: {
          dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)entry_it;
          entry_it += sizeof(dd_memory_alloc_data_t);

          running_total += ad->byte_count;

          uint64 key = (uint64)(ad->address_returned);
          // dd_devprintf("hash add %p\n", ad->address_returned);
          dd_hash64_add(&allocation_to_alloc_data, key, (uint64)ad, g_application_allocator);
        } break;
        case MEMORY_SENTINAL_FREE: {
          dd_memory_free_data_t* fd = (dd_memory_free_data_t*)entry_it;
          entry_it += sizeof(*fd);

          running_total -= fd->byte_count;

          uint64 key = (uint64)(fd->address_freed);
          // dd_devprintf("hash set %p\n", fd->address_freed);
          dd_hash64_remove(&allocation_to_alloc_data, key);
        } break;
      }
    }
  }

  // hash table now contains a list of all active allocations in the values array, interspersed
  // with 0 where it has been freed

  // Combine the data
  dd_memory_alloc_data_t** alloc_data  = {0};
  uint32* total_allocation             = {0};
  uint32* allocation_count             = {0};
  dd_hash64_t location_string_to_index = {0};

  uint32 value_count = allocation_to_alloc_data.bucket_count;
  for (uint32 a_idx = 0; a_idx < value_count; a_idx++) {
    dd_memory_alloc_data_t* ad =
        (dd_memory_alloc_data_t*)(uintptr_t)allocation_to_alloc_data.values[a_idx];
    if (ad == 0) continue;

    snprintf(line_buffer, sizeof(line_buffer) - 1, "%s (%u)", ad->file_name, ad->line_number);

    uint64 key   = murmur_hash_64(line_buffer, strlen(line_buffer), 0);
    uint64 index = dd_hash64_lookup(&location_string_to_index, key, array_count(total_allocation));
    if (index == array_count(total_allocation)) {
      dd_hash64_add(&location_string_to_index, key, array_count(total_allocation),
                    g_application_allocator);
      array_push(total_allocation, ad->byte_count, g_application_allocator);
      array_push(alloc_data, ad, g_application_allocator);
      array_push(allocation_count, 1, g_application_allocator);
    } else {
      total_allocation[index] += ad->byte_count;
      allocation_count[index]++;
    }
  }

  const uint32 viewed_allocation_count = (uint32)array_count(total_allocation);
  uint32* sorted_allocations           = {0};
  array_reserve(sorted_allocations, viewed_allocation_count, g_application_allocator);
  for (uint32 i = 0; i < viewed_allocation_count; i++) {
    sorted_allocations[i] = i;
  }

  hack_for_sorting_data.alloc_data       = alloc_data;
  hack_for_sorting_data.total_allocation = total_allocation;
  hack_for_sorting_data.allocation_count = allocation_count;
  if (headers->sort_by == 1) {
    qsort(sorted_allocations, viewed_allocation_count, sizeof(*sorted_allocations), &compare_size);
  } else if (headers->sort_by == 2) {
    qsort(sorted_allocations, viewed_allocation_count, sizeof(*sorted_allocations),
          &compare_location);
  }
  if (headers->sort_direction == -1) {
    uint32 t;
    for (uint32 i = 0; i < viewed_allocation_count / 2; i++) {
      t                     = sorted_allocations[i];
      sorted_allocations[i] = sorted_allocations[viewed_allocation_count - i - 1];
      sorted_allocations[viewed_allocation_count - i - 1] = t;
    }
  }

  uint32 total_memory_allocation_byte_count = 0;
  for (uint32 i = 0; i < viewed_allocation_count; i++) {
    uint32 idx = sorted_allocations[i];

    if (i + 1 == headers->selected_row) {
      full_line.y = y;
      g_draw2d_api_->rect(&ui->draw_data, full_line, ui->theme->status_bar.color_success);
    }
    total_memory_allocation_byte_count += total_allocation[idx];

    rect_timestamp.y = rect_size.y = rect_filename.y = y;
    snprintf(line_buffer, sizeof(line_buffer) - 1, "alloc %u", allocation_count[idx]);
    ui_api->widget.label(ui, rect_size, ui->theme->foreground.color, TextAlignmentLeft,
                         stringview(line_buffer));
    snprintf(line_buffer, sizeof(line_buffer) - 1, "%u B ", total_allocation[idx]);
    ui_api->widget.label(ui, rect_size, ui->theme->foreground.color, TextAlignmentRight,
                         stringview(line_buffer));
    snprintf(line_buffer, sizeof(line_buffer) - 1, "%s (%u)", alloc_data[idx]->file_name,
             alloc_data[idx]->line_number);
    ui_api->widget.label(ui, rect_filename, ui->theme->foreground.color, TextAlignmentLeft,
                         stringview(line_buffer));

    y += line.height + 1;
  }

  snprintf(line_buffer, sizeof(line_buffer) - 1, " total: %u B ",
           total_memory_allocation_byte_count);
  rect_size.y = y;
  ui_api->widget.label(ui, rect_size, ui->theme->foreground.color, TextAlignmentRight,
                       stringview(line_buffer));

  dd_hash64_free(&location_string_to_index, g_application_allocator);
  array_free(total_allocation, g_application_allocator);
  array_free(alloc_data, g_application_allocator);
}

void draw_tab_memory(dd_ui_t* ui, ui_rect_t rect, struct dd_telemetry_memory_t* memory) {
  static struct memory_table_headers headers = {.timestamp_width = 100, .size_width = 200};

  ui_rect_t rect_buttons = rect_split_off_top(&rect, 14, 1);
  draw_memory_buttons(ui, rect_buttons, memory, &headers);

  ui_rect_t rect_header = rect_split_off_top(&rect, 14, 1);
  draw_memory_table_headers(ui, rect_header, memory, &headers);

  if (headers.should_combine_locations) {
    draw_memory_table_combined(ui, rect, memory, &headers, __LINE__);
  } else {
    draw_memory_table(ui, rect, memory, &headers, __LINE__);
  }
}
