#include <assert.h>

#include "core/hid.h"
#include "core/profiler.h"
#include "core/types.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "hub.inl"
#include "ui/ui.h"

extern char log_messages[1024 * 1024 * 10];

extern void logs_get_lines(uint32 offset, uint32 count, const char** out_lines);

void draw_log_message_textarea(dd_ui_t* ui, ui_rect_t rect, uint32 id) {
  static uint32 line_offset = 0;

#if 0
  dd_ui_textarea(&g_ui, rect, log_messages, &line_offset, __LINE__);
#else
  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_api->interaction.set_next_hover(ui, ui->current_ui_layer, id);

    if (ui->hover_id == id) {
      if (ui->event.event_type == UIEventMouseScroll) {
        int32 delta = (*(int32*)&(ui->event.data));
        if (delta < 0) {
          line_offset++;
        } else {
          if (line_offset > 0) {
            line_offset--;
          }
        }
      }
    }
  }

  ui_rect_t line_rect    = rect;
  line_rect.height       = 7;
  const uint32 num_lines = (uint32)((rect.height / line_rect.height) + 1);

  const char* lines[1024];
  assert(num_lines < 1024);

  dd_prof_start("get lines");
  logs_get_lines(line_offset, num_lines, lines);
  dd_prof_end();

  dd_prof_start("draw lines");
  for (uint32 i = 0; (i < num_lines) && lines[i]; i++) {
    g_draw2d_api_->text(&ui->draw_data, line_rect, ui->theme->foreground.color, TextAlignmentLeft,
                        stringview(lines[i]));

    line_rect.y += 8;
  }
  dd_prof_end();
#endif
}

void draw_tab_logs(dd_ui_t* ui, ui_rect_t rect) {
  dd_prof_start("logs");
  draw_log_message_textarea(ui, rect, __LINE__);
  dd_prof_end();
}
