#include "core/types.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
#else
  #include <unistd.h>
static uint64 GetCurrentProcessId() {
  pid_t id = getpid();
  return (uint64)id;
}
#endif

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/containers.h"
#include "core/dd_math.h"
#include "core/log_remote.h"
#include "core/memory.inl"
#include "core/network.h"
#include "core/profiler.h"
#include "core/profiler.inl"
#include "core/thread.h"
#include "core/timer.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "hub.inl"
#include "telemetry/telemetry_server.h"
#include "telemetry/telemetry_types.inl"
#include "ui/ui.h"

enum { ZOOM_FRAME_COUNT = 60, time_length_to_store = 1000 * 1000 * 1000, MAX_STACK_DEPTH = 10 };

// Simply copy the data locally without processing
struct ProfileData {
  struct buf_and_size* buffers; /* array */
  uint64 thread_id;
  char thread_name[64];
  uint32 max_depth;
};

typedef struct dd_profile_dataset_t {
  double time_data_min;
  double time_data_max;

  struct {
    double time_min;
    double time_max;
    double time_min_target;
    double time_max_target;
  } view;
} dd_profile_dataset_t;

static dd_profile_dataset_t g_profile_active_dataset = {0};

static const int16 left_column_width = 50;

static uint64 g_latest_recorded_timestamp    = time_length_to_store;
static struct ProfileData* g_active_profiles = {0};
static dd_hash64_t g_hash_thread_id_to_index = {0};
static struct buf_and_size* incoming_buffers = {0}; /* array */
static dd_mutex_t incoming_buffers_mutex     = {0};

extern telemetry_server_t g_telemetry_server;

static uint8 remaining_animation_frame = 0;
static bool is_recording               = false;

void hub_profiler_process(const uint8* buffer, uint32 buffer_size) {
  if (incoming_buffers_mutex.opaque_[0] == 0) {
    incoming_buffers_mutex = dd_mutex_create();
  }

  uint8* data_copy = dd_alloc(g_application_allocator, buffer_size, 1);
  memcpy(data_copy, buffer, buffer_size);
  dd_mutex_lock(&incoming_buffers_mutex);
  struct buf_and_size bs = {.data = data_copy, .byte_count = buffer_size};
  array_push(incoming_buffers, bs, g_application_allocator);
  dd_mutex_release(&incoming_buffers_mutex);
}

void hub_memory_process(const uint8* buffer, uint32 buffer_size) {
  if (g_telemetry_server.memory.incoming_buffers_mutex.opaque_[0] == 0) {
    g_telemetry_server.memory.incoming_buffers_mutex = dd_mutex_create();
  }

  dd_devprintf("Telemetry server received memory data\n");

  uint8* data_copy = dd_alloc(g_application_allocator, buffer_size, 1);
  memcpy(data_copy, buffer, buffer_size);
  dd_mutex_lock(&g_telemetry_server.memory.incoming_buffers_mutex);

  // Fix the filename pointers
  uint8* entry_it                 = data_copy;
  const uint32 entries_byte_count = *(uint32*)entry_it;
  entry_it += sizeof(uint32);
  uint8* entry_end = entry_it + entries_byte_count;
  while (entry_it < entry_end) {
    switch (*(entry_it++)) {
      case MEMORY_SENTINAL_THREAD_ID: {
        entry_it += sizeof(dd_memory_threaddata_t);
      } break;
      case MEMORY_SENTINAL_ALLOC: {
        dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)entry_it;
        entry_it += sizeof(dd_memory_alloc_data_t);
        ad->file_name = (const char*)(data_copy + (uintptr_t)(ad->file_name));
      } break;
      case MEMORY_SENTINAL_FREE: {
        entry_it += sizeof(dd_memory_free_data_t);
      } break;
    }
  }

  struct buf_and_size bs = {.data = data_copy, .byte_count = buffer_size};
  array_push(g_telemetry_server.memory.incoming_buffers, bs, g_application_allocator);
  dd_mutex_release(&g_telemetry_server.memory.incoming_buffers_mutex);
}

void hub_profiler_process_in_thread(uint8* buffer, uint32 buffer_size) {
  uint32* offset_to_data = (uint32*)buffer;
  uint8* buf             = (uint8*)(buffer + *offset_to_data);
  uint64 thread_id       = ((struct dd_perf_threaddata_t*)(buf + 1))->thread_id;

  uint64 last_timestamp = profiler_last_timestamp_in_buffer(buffer, buffer_size);

  const uint64 time_length_to_store   = 1000 * 1000 * 1000;
  const uint64 earliest_time_to_store = (g_latest_recorded_timestamp > time_length_to_store)
                                            ? g_latest_recorded_timestamp - time_length_to_store
                                            : 0;
  if (is_recording || last_timestamp > earliest_time_to_store) {
    if (buffer_size == 0) {
      return;
    }

    g_latest_recorded_timestamp = max(g_latest_recorded_timestamp, last_timestamp);

    uint32 index = (uint32)dd_hash64_lookup(&g_hash_thread_id_to_index, thread_id,
                                            array_count(g_active_profiles));
    if (index == array_count(g_active_profiles)) {
      dd_hash64_add(&g_hash_thread_id_to_index, thread_id, index, g_application_allocator);
      struct ProfileData pd = {.thread_id = thread_id};
      array_push(g_active_profiles, pd, g_application_allocator);
    }

    struct ProfileData* this_profile = g_active_profiles + index;

    // uint8* old_profile = this_profile->data;

    struct buf_and_size bs = {.data = buffer, .byte_count = buffer_size};
    array_push(this_profile->buffers, bs, g_application_allocator);
    // free(old_profile);

    while (array_count(this_profile->buffers) > 0) {
      uint64 last_timestamp_in_buffer = profiler_last_timestamp_in_buffer(
          this_profile->buffers[0].data, this_profile->buffers[0].byte_count);
      if (last_timestamp_in_buffer < earliest_time_to_store) {
        free(this_profile->buffers[0].data);
        array_remove_at_index(this_profile->buffers, 0);
      } else {
        break;
      }
    }

    g_profile_active_dataset.view.time_min = g_profile_active_dataset.view.time_max = 0;
    g_profile_active_dataset.view.time_min_target = g_profile_active_dataset.view.time_max_target =
        0;
    remaining_animation_frame = 0;
  } else {
    free(buffer);
  }
}

static void draw_profiler_buttons(dd_ui_t* ui, ui_rect_t rect) {
  ui_rect_t rb            = rect_split_off_left(&rect, 100, 2);
  ui_rect_t button_around = rect_enlarge(rb, 1);
  if (is_recording) {
    g_draw2d_api_->rect_stroke(&ui->draw_data, button_around, 1, (color_rgba8_t){255, 0, 0, 255});
  }
  const dd_ui_button_t b = {.rect = rb, .id = __LINE__, .label = stringview("Record")};

  if (ui_api->widget.button(ui, &b)) {
    is_recording = !is_recording;
    dd_telemetry_server_enable_recording(is_recording);
  }
  char buff[128] = {0};
  snprintf(buff, sizeof(buff) - 1, "Num connected clients: %i",
           dd_telemetry_server_connected_client_count());
  ui_api->widget.label(ui, rect, ui->theme->foreground.color, TextAlignmentLeft, stringview(buff));
}

static inline double double_align_down(double value, double alignment) {
  double out = floor(value / alignment) * alignment;
  return out;
}

void draw_tab_profiler_time_track(dd_ui_t* ui, const ui_rect_t original_header,
                                  double time_view_min__us, double time_view_max__us) {
  double time_range__us = time_view_max__us - time_view_min__us;

  // Want to show some kind of legend every 100 or so pixels
  const uint32 minimum_pixels_between_ticks = 200;
  uint32 num_ticks               = (uint32)original_header.width / minimum_pixels_between_ticks;
  double range_between_ticks__us = time_range__us / num_ticks;
  const char* precision_string   = "%.0f ms";
  if (range_between_ticks__us < 5) {
    range_between_ticks__us = 5;
    precision_string        = "%.3f ms";
  } else if (range_between_ticks__us < 10) {
    range_between_ticks__us = 10;
    precision_string        = "%.3f ms";
  } else if (range_between_ticks__us < 20) {
    range_between_ticks__us = 20;
    precision_string        = "%.3f ms";
  } else if (range_between_ticks__us < 50) {
    range_between_ticks__us = 50;
    precision_string        = "%.2f ms";
  } else if (range_between_ticks__us < 100) {
    range_between_ticks__us = 100;
    precision_string        = "%.2f ms";
  } else if (range_between_ticks__us < 200) {
    range_between_ticks__us = 200;
    precision_string        = "%.2f ms";
  } else if (range_between_ticks__us < 500) {
    range_between_ticks__us = 500;
    precision_string        = "%.1f ms";
  } else if (range_between_ticks__us < 1000) {
    range_between_ticks__us = 1000;
    precision_string        = "%.0f ms";
  } else if (range_between_ticks__us < 2000) {
    range_between_ticks__us = 2000;
    precision_string        = "%.0f ms";
  } else if (range_between_ticks__us < 5000) {
    range_between_ticks__us = 5000;
    precision_string        = "%.0f ms";
  } else if (range_between_ticks__us < 10000) {
    range_between_ticks__us = 10000;
    precision_string        = "%.0f ms";
  } else if (range_between_ticks__us < 20000) {
    range_between_ticks__us = 20000;
    precision_string        = "%.0f ms";
  } else if (range_between_ticks__us < 50000) {
    range_between_ticks__us = 50000;
    precision_string        = "%.0f ms";
  }
  const double minor_ticks__us = range_between_ticks__us / 5;

  const double time_view_total     = time_view_max__us - time_view_min__us;
  const uint32 max_width_to_render = (uint32)original_header.width;
  double factor                    = (double)max_width_to_render / time_view_total;

  double time_view_tick__us       = time_view_min__us;
  double time_view_tick_start__us = time_view_tick__us;

  // Minor ticks
  time_view_tick__us = double_align_down(time_view_tick_start__us, range_between_ticks__us);
  for (; time_view_tick__us < time_view_max__us; time_view_tick__us += minor_ticks__us) {
    int16 offset     = (int16)(factor * (time_view_tick__us - time_view_tick_start__us));
    ui_rect_t header = original_header;
    if (offset < header.width) {
      (void)rect_split_off_left(&header, offset, 0);

      ui_rect_t line = header;
      line.y         = line.y + line.height - 2;
      line.height    = 2;
      line.width     = 1;
      g_draw2d_api_->rect(&ui->draw_data, line, ui->theme->foreground.color);
    }
  }

  time_view_tick__us = double_align_down(time_view_tick_start__us, range_between_ticks__us);
  for (; time_view_tick__us < time_view_max__us; time_view_tick__us += range_between_ticks__us) {
    char buffer[1024] = {0};

#if !defined(_MSC_VER)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wformat-nonliteral"
#endif
    sprintf(buffer, precision_string, (time_view_tick__us / 1000.0));
#if !defined(_MSC_VER)
  #pragma clang diagnostic pop
#endif

    int16 offset     = (int16)(factor * (time_view_tick__us - time_view_tick_start__us));
    ui_rect_t header = original_header;

    if (offset < header.width) {
      (void)rect_split_off_left(&header, offset, 0);

      ui_rect_t line = header;
      line.width     = 1;
      g_draw2d_api_->rect(&ui->draw_data, line, ui->theme->foreground.color);

      header.x += 2;
      ui_api->widget.label(ui, header, ui->theme->foreground.color, TextAlignmentLeft,
                           stringview(buffer));
    }
  }
}

void draw_memory_graph(dd_ui_t* ui, const ui_rect_t rect, double time_view_min__us,
                       double time_view_max__us) {
  // Find memory usage before active view
  uint32 total_memory_usage = 0;
  uint32 block_count        = (uint32)array_count(g_telemetry_server.memory.stored_buffers);
  uint32 block_idx          = 0;
  uint8* entry_it           = NULL;
  uint8* entry_end          = NULL;
  bool is_entry_in_view     = false;
  while (block_idx < block_count && !is_entry_in_view) {
    const struct telemetry_memory_processed_t* block =
        g_telemetry_server.memory.stored_buffers + block_idx;
    entry_it                        = block->data;
    entry_end                       = entry_it + block->byte_count;
    const uint32 entries_byte_count = *(uint32*)entry_it;
    entry_it += sizeof(uint32);
    entry_end = entry_it + entries_byte_count;
    while (entry_it < entry_end && !is_entry_in_view) {
      switch (*(entry_it++)) {
        case MEMORY_SENTINAL_THREAD_ID: {
          entry_it += sizeof(dd_memory_threaddata_t);
        } break;
        case MEMORY_SENTINAL_ALLOC: {
          dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)entry_it;
          entry_it += sizeof(dd_memory_alloc_data_t);
          if (ad->timestamp < time_view_min__us) {
            total_memory_usage += ad->byte_count;
          } else {
            is_entry_in_view = true;
          }
        } break;
        case MEMORY_SENTINAL_FREE: {
          dd_memory_free_data_t* fd = (dd_memory_free_data_t*)entry_it;
          entry_it += sizeof(dd_memory_free_data_t);
          if (fd->timestamp < g_profile_active_dataset.view.time_min) {
            total_memory_usage -= fd->byte_count;
          } else {
            is_entry_in_view = true;
          }
        } break;
      }
    }

    ++block_idx;
  }

  // Next part is in view
  const double time_view_total = time_view_max__us - time_view_min__us;
  uint32 max_width_to_render   = (uint32)rect.width;
  double factor                = (double)max_width_to_render / time_view_total;
  double vertical_scale_factor =
      (double)rect.height / g_telemetry_server.memory.maximum_total_allocation;

  ui_rect_t bar_rect = {rect.x, rect.y + rect.height, 0, 0};
  while (block_idx < block_count && is_entry_in_view) {
    const struct telemetry_memory_processed_t* block =
        g_telemetry_server.memory.stored_buffers + block_idx;
    if (entry_it == NULL) {
      entry_it                        = block->data;
      entry_end                       = entry_it + block->byte_count;
      const uint32 entries_byte_count = *(uint32*)entry_it;
      entry_it += sizeof(uint32);
      entry_end = entry_it + entries_byte_count;
    }
    while (entry_it < entry_end) {
      float offset     = bar_rect.x;
      uint64 timestamp = 0;
      switch (*(entry_it++)) {
        case MEMORY_SENTINAL_THREAD_ID: {
          entry_it += sizeof(dd_memory_threaddata_t);
        } break;
        case MEMORY_SENTINAL_ALLOC: {
          dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)entry_it;
          entry_it += sizeof(dd_memory_alloc_data_t);
          total_memory_usage += ad->byte_count;
          timestamp = ad->timestamp;
        } break;
        case MEMORY_SENTINAL_FREE: {
          dd_memory_free_data_t* fd = (dd_memory_free_data_t*)entry_it;
          entry_it += sizeof(dd_memory_free_data_t);
          total_memory_usage -= fd->byte_count;
          timestamp = fd->timestamp;
        } break;
      }
      offset         = (float)((timestamp - g_profile_active_dataset.view.time_min) * factor);
      bar_rect.width = floorf(offset - bar_rect.x);
      if (timestamp && bar_rect.width > 0) {
        bar_rect.height = -(float)(total_memory_usage * vertical_scale_factor);
        g_draw2d_api_->rect(&ui->draw_data, bar_rect, ui->theme->status_bar.color_success);
        bar_rect.x += bar_rect.width;
      }
      if (timestamp > g_profile_active_dataset.view.time_max) {
        break;
      }
    }

    ++block_idx;
    entry_it = NULL;
  }

  char buf[8] = {0};
  if (g_telemetry_server.memory.maximum_total_allocation < 1000) {
    snprintf(buf, sizeof(buf) - 1, "%u B", g_telemetry_server.memory.maximum_total_allocation);
  } else if (g_telemetry_server.memory.maximum_total_allocation < 1000 * 1000) {
    snprintf(buf, sizeof(buf) - 1, "%u KB",
             g_telemetry_server.memory.maximum_total_allocation / 1000);
  } else if (g_telemetry_server.memory.maximum_total_allocation < 1000 * 1000 * 1000) {
    snprintf(buf, sizeof(buf) - 1, "%u MB",
             g_telemetry_server.memory.maximum_total_allocation / (1000 * 1000));
  } else if (g_telemetry_server.memory.maximum_total_allocation < 1000u * 1000u * 1000u * 1000u) {
    snprintf(buf, sizeof(buf) - 1, "%u GB",
             g_telemetry_server.memory.maximum_total_allocation / (1000 * 1000 * 1000));
  }
  ui_rect_t rect_label = rect;
  rect_label.x -= left_column_width;
  rect_label.width  = left_column_width;
  rect_label.height = 8;
  ui_api->widget.label(ui, rect_label, ui->theme->foreground.color, TextAlignmentRight,
                       stringview(buf));
}

void draw_profiler(dd_ui_t* ui, ui_rect_t* remaining_graph_area, ui_rect_t* threads_column) {
  ui_rect_t box        = *remaining_graph_area;
  uint32 profile_count = (uint32)array_count(g_active_profiles);

  const double time_view_total =
      g_profile_active_dataset.view.time_max - g_profile_active_dataset.view.time_min;
  uint32 max_width_to_render = (uint32)remaining_graph_area->width;
  double factor              = (double)max_width_to_render / time_view_total;

  box.height = 11;
  for (uint32 i = 0; i < profile_count; i++) {
    color_rgba8_t color = ui->theme->background.color;
    color               = (color_rgba8_t){color.r + (1 + i % 2) * 20, color.g + (1 + i % 2) * 20,
                            color.b + (1 + i % 2) * 20, 255};
    g_draw2d_api_->rect(&ui->draw_data, *remaining_graph_area, color);
    ui_rect_t name_area = *remaining_graph_area;
    name_area.x         = threads_column->x;
    name_area.width     = threads_column->width;
    g_draw2d_api_->rect(&ui->draw_data, name_area, color);

    struct ProfileData* thread_profile = g_active_profiles + i;
    uint32 thread_profiles_count       = (uint32)array_count(thread_profile->buffers);
    for (uint32 profile_idx = 0; profile_idx < thread_profiles_count; profile_idx++) {
      if (thread_profile->buffers[profile_idx].byte_count == 0) continue;

      struct buf_and_size* profile = thread_profile->buffers + profile_idx;

      const char* labels     = (const char*)(profile->data);
      uint32* offset_to_data = (uint32*)profile->data;
      uint8* buf             = (uint8*)(profile->data + *offset_to_data);
      uint8* buf_end         = profile->data + profile->byte_count;

      const uint64 process_id = GetCurrentProcessId();

      const dd_perf_startdata* start_stack[MAX_STACK_DEPTH];
      uint32 current_stack_depth = 0;

      const dd_perf_threaddata_t* td = NULL;
      while (buf != buf_end) {
        uint8 sentinal = *buf;
        buf += 1;
        if (sentinal == SENTINAL_START) {
          const dd_perf_startdata* sd = (const dd_perf_startdata*)buf;
          buf += sizeof(dd_perf_startdata);

          start_stack[current_stack_depth++] = sd;
          assert(current_stack_depth <= MAX_STACK_DEPTH);

          //          sd->label, sd->category, process_id, sd->thread_id, (sd->timestamp -
          //          initial));
        } else if (sentinal == SENTINAL_END) {
          const dd_perf_enddata* ed = (const dd_perf_enddata*)buf;
          buf += sizeof(dd_perf_enddata);

          const dd_perf_startdata* sd = start_stack[--current_stack_depth];
          const char* label           = labels + (intptr_t)(sd->label);

          double offset =
              factor * ((double)(sd->timestamp) - g_profile_active_dataset.view.time_min);
          double width = factor * (double)(ed->timestamp - sd->timestamp);

          if (offset + width < 0) continue;
          if (offset > remaining_graph_area->width) continue;

          // Numbers can be quite large, and int16 won't have enough range, so clamp the box to
          // the view first
          if (offset < 0) {
            width  = width + offset;
            offset = 0;
          }

          if ((offset + width) > (remaining_graph_area->x + remaining_graph_area->width)) {
            width = (remaining_graph_area->x + remaining_graph_area->width) - offset;
          }

          uint32 profile_section_depth = current_stack_depth;
          thread_profile->max_depth    = max(thread_profile->max_depth, profile_section_depth);
          box.x                        = (int16)(remaining_graph_area->x + offset);
          box.width                    = (int16)(width);
          box.y =
              (float)(remaining_graph_area->y + (int)profile_section_depth * (box.height + 1)) + 1;
          if (width < 0) continue;

          // Always have at least one pixel width, and always leave a pixel gap to the next one
          box.width = max(1.f, box.width - 1);
          if (((box.x + box.width) > remaining_graph_area->x) &&
              (box.x < (remaining_graph_area->x + remaining_graph_area->width))) {
            g_draw2d_api_->rect(&ui->draw_data, box, ui->theme->status_bar.color_success);
            box.x += 2;
            box.width                = (box.width > 2) ? box.width - 2 : 0;
            char clipped_label[1024] = {0};
            float duration__ms       = ((float)(ed->timestamp - sd->timestamp)) / 1000.f;
            snprintf(clipped_label, sizeof(clipped_label) - 1, "%s { %.3f ms }", label,
                     duration__ms);
            int16 num_char_space          = (int16)(box.width / 6);
            clipped_label[num_char_space] = 0;
            if (clipped_label[0]) {
              ui_api->widget.label(ui, box, ui->theme->foreground.color, TextAlignmentLeft,
                                   stringview(clipped_label));
            }
            (void)process_id;
            // process_id,                   ed->thread_id, (ed->timestamp - initial));
          }
        } else if (sentinal == SENTINAL_THREAD_ID) {
          td                         = (const dd_perf_threaddata_t*)buf;
          char thread_id_buffer[128] = {0};
          if (thread_profile->thread_name[0]) {
            snprintf(thread_id_buffer, sizeof(thread_id_buffer) - 1, "%s",
                     thread_profile->thread_name);
          } else {
            snprintf(thread_id_buffer, sizeof(thread_id_buffer) - 1, "%llu", td->thread_id);
          }
          ui_rect_t tr = {.x      = threads_column->x,
                          .y      = remaining_graph_area->y,
                          .width  = threads_column->width,
                          .height = box.height};
          ui_api->widget.label(ui, tr, ui->theme->foreground.color, TextAlignmentLeft,
                               stringview(thread_id_buffer));
          buf += sizeof(dd_perf_threaddata_t);
        } else if (sentinal == SENTINAL_THREAD_NAME) {
          const dd_perf_threadname_t* tn = (const dd_perf_threadname_t*)buf;
          memcpy(thread_profile->thread_name, tn->name, sizeof(tn->name));
          buf += sizeof(dd_perf_threadname_t);
        } else {
          assert(false);  // Unknown id
        }
      }
    }

    float max_y = thread_profile->max_depth * (box.height + 1);

    rect_split_off_top(remaining_graph_area, max_y + box.height, 2);
  }
}

void draw_tab_profiler(dd_ui_t* ui, ui_rect_t rect, uint32 id) {
  if (incoming_buffers_mutex.opaque_[0] == 0) {
    incoming_buffers_mutex = dd_mutex_create();
  }

  dd_mutex_lock(&incoming_buffers_mutex);
  if (array_count(incoming_buffers) > 0) {
    for (uint32 i = 0; i < array_count(incoming_buffers); i++) {
      hub_profiler_process_in_thread(incoming_buffers[i].data, incoming_buffers[i].byte_count);
    }
    array_reset(incoming_buffers);
  }
  dd_mutex_release(&incoming_buffers_mutex);

  dd_telemetry_server_process_incoming_data(&g_telemetry_server);

  {
    ui_rect_t header = rect_split_off_top(&rect, 20, 1);
    draw_profiler_buttons(ui, header);
  }

  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_api->interaction.set_next_hover(ui, ui->current_ui_layer, id);
  }

  ui_rect_t header = rect_split_off_top(&rect, 10, 1);
  (void)rect_split_off_left(&header, left_column_width, 1);
  ui_rect_t threads_column = rect_split_off_left(&rect, left_column_width, 1);
  (void)threads_column;
  const ui_rect_t graph_area     = rect;
  ui_rect_t remaining_graph_area = graph_area;

  ui_rect_t memory_graph_area = rect_split_off_top(&remaining_graph_area, 100, 1);

  if (ui->hover_id == id) {
    if (ui->event.event_type == UIEventMouseScroll) {
      int32 data = ui->event.data;
      dd_devprintf("can zoom %i\n", data);
      const double zoom_percentage = 1.0 + (data > 0 ? -0.05 : 0.05);
      const double old_range =
          g_profile_active_dataset.view.time_max - g_profile_active_dataset.view.time_min;
      const double new_range = old_range * zoom_percentage;

      // Keep point under the mouse the same
      const float mouse_x_offset_in_widget = ui->mouse.x - graph_area.x;
      const float widget_width             = graph_area.width;
      const double mouse_factor = (double)mouse_x_offset_in_widget / (double)widget_width;
      const double time_under_mouse =
          g_profile_active_dataset.view.time_min +
          ((g_profile_active_dataset.view.time_max - g_profile_active_dataset.view.time_min) *
           mouse_factor);
      const double new_left_time             = time_under_mouse - new_range * mouse_factor;
      const double new_right_time            = time_under_mouse + new_range * (1.0 - mouse_factor);
      g_profile_active_dataset.view.time_min = new_left_time;
      g_profile_active_dataset.view.time_max = new_right_time;
    }
  }

  uint32 profile_count = (uint32)array_count(g_active_profiles);
  if (profile_count == 0) return;

  bool should_determine_extents = is_recording || (g_profile_active_dataset.view.time_min ==
                                                   g_profile_active_dataset.view.time_max);
  if (should_determine_extents) {
    g_profile_active_dataset.time_data_min = DBL_MAX;
    g_profile_active_dataset.time_data_max = 0;

    for (uint32 i = 0; i < profile_count; i++) {
      struct ProfileData* thread_profile = g_active_profiles + i;
      uint32 thread_profiles_count       = (uint32)array_count(thread_profile->buffers);
      for (uint32 profile_idx = 0; profile_idx < thread_profiles_count; profile_idx++) {
        struct buf_and_size* profile = thread_profile->buffers + profile_idx;
        uint32* offset_to_data       = (uint32*)profile->data;
        uint8* buf                   = (uint8*)(profile->data + *offset_to_data);
        uint8* buf_end               = profile->data + profile->byte_count;

        uint32 profile_data_byte_count = (uint32)(buf_end - buf);
        uint64 first_timestamp = profiler_first_timestamp_in_buffer(buf, profile_data_byte_count);
        uint64 last_timestamp  = profiler_last_timestamp_in_buffer(buf, profile_data_byte_count);
        if (should_determine_extents) {
          g_profile_active_dataset.time_data_min =
              min(g_profile_active_dataset.time_data_min, (double)first_timestamp);
          g_profile_active_dataset.time_data_max =
              max(g_profile_active_dataset.time_data_max, (double)last_timestamp);
        }
      }
    }

    g_profile_active_dataset.view.time_min = g_profile_active_dataset.time_data_min;
    g_profile_active_dataset.view.time_max = g_profile_active_dataset.time_data_max;
    // dd_devprintf("data extents %f %f\n", g_profile_active_dataset.time_data_min,
    //             g_profile_active_dataset.time_data_max);
  }

  if (remaining_animation_frame) {
    g_profile_active_dataset.view.time_min =
        g_profile_active_dataset.view.time_min +
        (g_profile_active_dataset.view.time_min_target - g_profile_active_dataset.view.time_min) /
            remaining_animation_frame;
    double dx =
        (g_profile_active_dataset.view.time_max_target - g_profile_active_dataset.view.time_max) /
        remaining_animation_frame;
    g_profile_active_dataset.view.time_max = g_profile_active_dataset.view.time_max + dx;
    remaining_animation_frame--;
  }

  draw_tab_profiler_time_track(
      ui, header, g_profile_active_dataset.view.time_min - g_profile_active_dataset.time_data_min,
      g_profile_active_dataset.view.time_max - g_profile_active_dataset.time_data_min);

  draw_memory_graph(ui, memory_graph_area, g_profile_active_dataset.view.time_min,
                    g_profile_active_dataset.view.time_max);

  draw_profiler(ui, &remaining_graph_area, &threads_column);

  g_draw2d_api_->rect(&ui->draw_data, remaining_graph_area, ui->theme->background.color);

  // If zooming with dragging, then render highlight over the top
  if (ui->hover_id == id) {
    static float x_first_down     = 0;
    static float pan_x_first_down = 0;
    if (ui->event.event_type == UIEventMouseButton1Down) {
      dd_devprintf("start drag\n");
      pan_x_first_down = ui->mouse.x;
    } else if (ui->event.event_type == UIEventMouseButton1Up) {
      dd_devprintf("end drag\n");
      pan_x_first_down = 0;
    }

    const double time_view_total =
        g_profile_active_dataset.view.time_max - g_profile_active_dataset.view.time_min;
    float max_width_to_render = remaining_graph_area.width;
    double factor             = (double)max_width_to_render / time_view_total;

    //  Panning
    if (pan_x_first_down != 0.f) {
      dd_devprintf("dragging %i\n", ui->mouse.dx);
      g_profile_active_dataset.view.time_min -= ui->mouse.dx / factor;
      g_profile_active_dataset.view.time_max -= ui->mouse.dx / factor;
    }

    if (ui->mouse.down) {
      if (!ui->mouse.down_prev) {
        x_first_down = ui->mouse.x;
      }

      float left_x  = (x_first_down < ui->mouse.x) ? x_first_down : ui->mouse.x;
      float right_x = (x_first_down > ui->mouse.x) ? x_first_down : ui->mouse.x;
      left_x        = max(left_x, graph_area.x);
      right_x       = min(right_x, graph_area.x + graph_area.width);

      ui_rect_t highlight = graph_area;
      ui_rect_t highlight_left =
          rect_split_off_left(&highlight, max(1.f, left_x - graph_area.x), 0);
      ui_rect_t highlight_right = rect_split_off_right(
          &highlight, max(1.f, (graph_area.x + graph_area.width) - right_x - 1), 0);

      g_draw2d_api_->rect(&ui->draw_data, highlight_left, (color_rgba8_t){0, 0, 0, 100});
      g_draw2d_api_->rect(&ui->draw_data, highlight_right, (color_rgba8_t){0, 0, 0, 100});
    } else if (ui->mouse.down_prev) {
      // Just stopped dragging
      float x_last_down       = ui->mouse.x;
      const float left_x      = (x_first_down < x_last_down) ? x_first_down : x_last_down;
      const float right_x     = (x_first_down > x_last_down) ? x_first_down : x_last_down;
      const double factor_min = (double)(left_x - graph_area.x) / (double)(graph_area.width);
      const double factor_max = (double)(right_x - graph_area.x) / (double)(graph_area.width);

      g_profile_active_dataset.view.time_min_target =
          g_profile_active_dataset.view.time_min + factor_min * time_view_total;
      g_profile_active_dataset.view.time_max_target =
          g_profile_active_dataset.view.time_min + factor_max * time_view_total;
      remaining_animation_frame = ZOOM_FRAME_COUNT;
      dd_devprintf("zooming in to %f %f\n", g_profile_active_dataset.view.time_min_target,
                   g_profile_active_dataset.view.time_max_target);
    }
    if (ui->event.event_type == UIEventMouseButton0DoubleClick) {
      g_profile_active_dataset.view.time_min_target = g_profile_active_dataset.time_data_min;
      g_profile_active_dataset.view.time_max_target = g_profile_active_dataset.time_data_max;
      remaining_animation_frame                     = ZOOM_FRAME_COUNT;
    }
  }
}
