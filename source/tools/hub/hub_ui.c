#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "core/api_registry.h"
#include "core/containers.h"
#include "core/hid.h"
#include "core/plugins.h"
#include "core/profiler.h"
#include "core/timer.h"
#include "core/types.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "graphics_api/graphics.h"
#include "telemetry/telemetry_client.h"
#include "telemetry/telemetry_server.h"
#include "telemetry/telemetry_types.inl"
#include "ui/ui.h"

extern struct dd_allocator* g_application_allocator;
extern telemetry_server_t g_telemetry_server;

static struct dd_plugins_o* g_plugins;

struct dd_ui_api* ui_api            = NULL;
struct dd_draw2d_api* g_draw2d_api_ = NULL;

// TODO: use plugin manager
#if defined(_WIN32)
extern dd_graphics_api_t dd_vulkan;
dd_graphics_api_t* g_graphics_api = &dd_vulkan;
#else
extern dd_graphics_api_t dd_metal;
dd_graphics_api_t* g_graphics_api = &dd_metal;
#endif

static dd_graphics_device_t g_graphics_device;
dd_graphics_buffer_t g_quad_details_buffer[3];
dd_graphics_buffer_t g_quad_buffer[3];
dd_graphics_buffer_t g_uniform_screen_size;
dd_graphics_material_prototype_t g_material_prototype = {0};

const uint32 quad_buffer_byte_count =
    500 * 500 * 16;  // Space for 500x500 rects, which means almost every other pixe
const uint32 quad_details_buffer_byte_count = 500000;
static dd_ui_t g_ui                         = {0};
static uint32 g_width                       = 0;
static uint32 g_height                      = 0;

void send_ui_buffers_to_device(uint32 index, const dd_draw2d_t* draw_data) {
  uint32 details_count = (uint32)array_count(draw_data->primitive_buffer);
  uint32 bytes_count   = sizeof(*draw_data->primitive_buffer) * details_count;
  assert(bytes_count <= quad_details_buffer_byte_count);
  g_graphics_api->buffer.update_data(g_graphics_device, g_quad_details_buffer[index], 0,
                                     (const void*)draw_data->primitive_buffer, bytes_count);

  uint32 quads_byte_count = (uint32)(sizeof(*draw_data->quads) * array_count(draw_data->quads));
  assert(quads_byte_count <= quad_buffer_byte_count);
  g_graphics_api->buffer.update_data(g_graphics_device, g_quad_buffer[index], 0,
                                     (const void*)draw_data->quads, quads_byte_count);
}

const dd_ui_style_t g_theme = {.background = {.color = {30, 30, 30, 255}},
                               .foreground = {.color = {255, 255, 255, 255}},
                               .button     = {.color = {14, 99, 156, 255},
                                          .hover = {16, 120, 188, 255},
                                          .down  = {255, 92, 145, 255}},
                               .status_bar = {.color_neutral = {30, 30, 30, 255},
                                              .color_error   = {192, 0, 50, 255},
                                              .color_success = {0, 122, 204, 255}}};

void draw_tab_logs(dd_ui_t* ui, ui_rect_t rect);
void draw_tab_profiler(dd_ui_t* ui, ui_rect_t rect, int32 id);
void draw_tab_memory(dd_ui_t* ui, ui_rect_t rect, struct dd_telemetry_memory_t* memory_recording);

void draw(ui_rect_t r) {
  static unsigned selected_tab = 0;
  {
    ui_rect_t status_r = rect_split_off_bottom(&r, 16, 1);
    {
      ui_api->widget.pane(&g_ui, status_r, g_theme.status_bar.color_neutral);
      ui_api->widget.label(&g_ui, rect_shrink(status_r, 1), (color_rgba8_t){255, 255, 255, 255},
                           TextAlignmentLeft, stringview("Error status"));
    }

    ui_rect_t tabs_r = rect_split_off_top(&r, 16, 1);
    {
      ui_api->widget.tab_bar(&g_ui, &tabs_r);
      const dd_stringview_t labels[] = {stringview("Logs"), stringview("Profiler"),
                                        stringview("Memory")};
      for (unsigned i = 0; i < countof(labels); i++) {
        if (ui_api->widget.tab_bar_item(&g_ui, &tabs_r, labels[i], (i == selected_tab))) {
          selected_tab = i;
        }
      }
    }

    ui_api->widget.pane(&g_ui, r, g_theme.background.color);

    if (selected_tab == 0) {
      draw_tab_logs(&g_ui, r);
    } else if (selected_tab == 1) {
      draw_tab_profiler(&g_ui, r, __LINE__);
    } else if (selected_tab == 2) {
      draw_tab_memory(&g_ui, r, &g_telemetry_server.memory);
    }
  }
}

void hub_draw_frame(dd_graphics_commandbuffer_t draw_commandbuffer,
                    const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor) {
  dd_telemetry_client_flush_threaddata();

  dd_telemetry_client_send_memory_data();

  DD_PROF();
  if (g_width != in_render_pass_descriptor->width ||
      g_height != in_render_pass_descriptor->height) {
    g_width  = in_render_pass_descriptor->width;
    g_height = in_render_pass_descriptor->height;

    uint32 screen_size[2] = {g_width, g_height};
    g_graphics_api->buffer.update_data(g_graphics_device, g_uniform_screen_size, 0,
                                       (const void*)screen_size, sizeof(screen_size));
  }

  static uint32 frame = 0;
  frame               = (frame + 1) % 3;

  dd_ui_event_t ui_event = ui_api->interaction.pop_event();

  if (ui_event.event_type == UIEventKeyInputChar && ui_event.data == 'p') {
    dd_telemetry_client_send_profiler_frame();
  }

  ui_api->begin(&g_ui, &g_theme, ui_event);
  ui_rect_t r = {.x      = 0,
                 .y      = 0,
                 .width  = (int16)in_render_pass_descriptor->width,
                 .height = (int16)in_render_pass_descriptor->height};
  draw(r);
  const dd_draw2d_t* draw_data = ui_api->end(&g_ui);

  dd_prof_start("sending buffers");
  send_ui_buffers_to_device(frame, draw_data);
  dd_prof_end();

  dd_prof_start("render pass");
  g_graphics_api->commandbuffer.begin_render_pass(draw_commandbuffer, in_render_pass_descriptor);

  g_graphics_api->commandbuffer.set_viewport(draw_commandbuffer, 0, 0, (float)g_width,
                                             (float)g_height, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(draw_commandbuffer, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers = {{.buffer = g_quad_details_buffer[frame],
                   .offset = 0,
                   .range  = quad_details_buffer_byte_count},
                  {.buffer = g_quad_buffer[frame], .offset = 0, .range = quad_buffer_byte_count},
                  {.buffer = g_uniform_screen_size, .offset = 0, .range = 2 * sizeof(uint32)}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(draw_commandbuffer, g_material_prototype,
                                                          &md);
  g_graphics_api->commandbuffer.draw(draw_commandbuffer, (uint32)array_count(draw_data->quads) * 6,
                                     1);

  g_graphics_api->commandbuffer.end_render_pass(draw_commandbuffer);
  dd_prof_end();

  DD_PROF_END();

  dd_prof_flush();

  dd_plugins_api.check_for_changes(g_plugins);
}

extern dd_graphics_window_t g_window;

void hub_init() {
  dd_api_registry.init(g_application_allocator);
  g_plugins                  = dd_plugins_api.create(g_application_allocator);
  const char* plugin_paths[] = {"e:\\daedalus_c\\build\\x64\\Debug\\*.dll"};
  dd_plugins_api.load_plugins(g_plugins, plugin_paths, countof(plugin_paths));

  ui_api        = (struct dd_ui_api*)dd_api_registry.get_api("ui", 1);
  g_draw2d_api_ = (struct dd_draw2d_api*)dd_api_registry.get_api("draw2d", 1);

  g_ui.draw_data.allocator = g_application_allocator;
  g_width                  = g_window.size.width;
  g_height                 = g_window.size.height;

  g_graphics_device = g_graphics_api->get_device();

  for (unsigned i = 0; i < 3; ++i) {
    g_graphics_api->buffer.allocate(g_graphics_device, 1, &quad_details_buffer_byte_count,
                                    g_quad_details_buffer + i);
    g_graphics_api->buffer.allocate(g_graphics_device, 1, &quad_buffer_byte_count,
                                    g_quad_buffer + i);
  }

  uint32 screen_size[2] = {g_width, g_height};
  uint32 bytes_count    = sizeof(screen_size);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &bytes_count, &g_uniform_screen_size);
  g_graphics_api->buffer.update_data(g_graphics_device, g_uniform_screen_size, 0,
                                     (const void*)screen_size, sizeof(screen_size));

#if defined(_WIN32)
  uint8* vs_code = read_file("../source/examples/ui/vulkan/ui.vs.spv", g_application_allocator);
  uint8* fs_code = read_file("../source/examples/ui/vulkan/ui.fs.spv", g_application_allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"quadsVertexShader",
    .fs_code       = (const uint8*)"quadsFragmentShader",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output =
        {// TODO: link formats with view
         .color_format = B8G8R8A8,
         .depth_format = DEPTH_U16_NORMALIZED},
    .bindings           = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeUniformBuffer},
                 {.type = BindingTypeUniformBuffer}},
    .enable_alpha_blend = true
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

#if defined(_WIN32)
  array_free(vs_code, g_application_allocator);
  array_free(fs_code, g_application_allocator);
#endif
}

void hub_deinit() {
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype);
  g_graphics_api->buffer.destroy(g_graphics_device, 3, g_quad_details_buffer);
  g_graphics_api->buffer.destroy(g_graphics_device, 3, g_quad_buffer);
  g_graphics_api->buffer.destroy(g_graphics_device, 1, &g_uniform_screen_size);

  dd_plugins_api.unload_all(g_plugins);
}
