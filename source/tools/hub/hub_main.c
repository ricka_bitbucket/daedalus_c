#include <stdio.h>

#include "core/profiler.h"
#include "graphics_api/graphics.h"
#include "telemetry/telemetry_client.h"
#include "telemetry/telemetry_server.h"

// test
#include "core/memory.h"
#include "core/thread.h"
#include "core/utils.h"

static uint8 g_alloc_buffer[2 * 1024];
dd_allocator* g_application_allocator;

extern void hub_init();
extern void hub_deinit();
extern void hub_draw_frame(dd_graphics_commandbuffer_t draw_commandbuffer,
                           const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor);
extern void dd_helper_set_functions(void (*init)(), void (*deinit)(),
                                    dd_graphics_api_helper_draw_callback draw_cb,
                                    dd_graphics_api_helper_compute_callback compute_cb);

void test_thread(void* arg) {
  (void)arg;
  while (true) {
    dd_sleep(500);
    DD_PROF();
    dd_sleep(50);
    DD_PROF_END();
  }
}

#if defined(_WIN32)

  #include <Windows.h>
extern dd_graphics_window_t g_window;
extern int dd_vk_main_loop(dd_graphics_window_t* window, dd_allocator* allocator);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
  dd_prof_thread_init("main");

  dd_allocator* allocator = (dd_allocator*)g_alloc_buffer;
  dd_heap_allocator_init(allocator, sizeof(g_alloc_buffer), "startup_allocator");

  dd_allocator* main_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(main_allocator, "main_allocator");

  #if 0
  dd_allocator* page_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(page_allocator, "telemetry_client_allocator");
  dd_telemetry_client_init(page_allocator);
  dd_allocator* reporting_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_telemetry_client_memory_allocator_init(reporting_allocator, main_allocator);
  g_application_allocator = reporting_allocator;
  #else
  g_application_allocator = main_allocator;
  #endif

  dd_telemetry_server_init(g_application_allocator);

  // dd_thread_start(&test_thread, NULL);
  dd_helper_set_functions(hub_init, hub_deinit, hub_draw_frame, NULL);

  const char* settings_file_path = "windows_pos.dat";
  FILE* f                        = fopen(settings_file_path, "rb");
  uint32 r[]                     = {0, 0, 800, 512, 0};
  if (f) {
    fread(r, sizeof(r), 1, f);
    fclose(f);
  }

  const char* name = "Daedalus Hub";
  // Don't adjust size, because the stored size is already including window chrome
  const uint32 flags          = (r[4] == 0) ? 0 : DD_WINDOW_FLAGS_MAXIMIZE;
  dd_graphics_window_t window = dd_create_window(hInstance, name, r[0], r[1], r[2], r[3], 200, 200,
                                                 flags, g_application_allocator);

  int out = dd_vk_main_loop(&window, g_application_allocator);

  f = fopen(settings_file_path, "wb");
  if (f) {
    r[0] = g_window.position.x;
    r[1] = g_window.position.y;
    r[2] = g_window.size.width;
    r[3] = g_window.size.height;
    r[4] = g_window.state;

    fwrite(r, sizeof(r), 1, f);
    fclose(f);
  };

  return out;
}

#elif defined(__APPLE__)

  #pragma clang diagnostic ignored "-Wnullability-extension"
extern int NSApplicationMain(int argc, const char* _Nonnull argv[_Nonnull]);
int main(int argc, const char* argv[]) {
  dd_prof_thread_init("main");

  dd_allocator* allocator = (dd_allocator*)g_alloc_buffer;
  dd_heap_allocator_init(allocator, sizeof(g_alloc_buffer), "startup_allocator");

  dd_allocator* main_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(main_allocator, "main_allocator");

  #if 0
  dd_allocator* page_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(page_allocator, "telemetry_client_allocator");
  dd_telemetry_client_init(page_allocator);
  dd_allocator* reporting_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_telemetry_client_memory_allocator_init(reporting_allocator, main_allocator);
  g_application_allocator = reporting_allocator;
  #else
  g_application_allocator = main_allocator;
  #endif

  dd_telemetry_server_init(g_application_allocator);

  dd_helper_set_functions(hub_init, hub_deinit, hub_draw_frame, NULL);
  return NSApplicationMain(argc, argv);
}

#endif
