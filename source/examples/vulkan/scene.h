#pragma once

#include "core/types.h"
#include "linmath.h"

typedef struct DemoScene {
  mat4x4 projection_matrix;
  mat4x4 view_matrix;
  mat4x4 model_matrix;

  float spin_angle;
  float spin_increment;
} DemoScene;
