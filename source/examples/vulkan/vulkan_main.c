#if defined(_WIN32)
  #include <Windows.h>
#endif  // _WIN32

#define _USE_MATH_DEFINES
#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "core/api_registry.h"
#include "core/application.h"
#include "core/containers.h"
#include "core/memory.h"
#include "core/plugins.h"
#include "core/profiler.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "graphics_api/graphics.h"
#include "scene.h"
#include "ui/ui.h"

extern dd_graphics_api_t dd_vulkan;
dd_graphics_api_t* g_graphics_api = &dd_vulkan;

static struct dd_plugins_o* g_plugins;
struct dd_ui_api* ui_api                   = NULL;
static struct dd_draw2d_api* g_draw2d_api_ = NULL;

static dd_graphics_device_t g_graphics_device;
static uint8 g_alloc_buffer[2 * 1024];

dd_allocator* g_application_allocator;

// TODO: Don't really need this, but the graphics submodule interacts with the GUI for events
static dd_ui_t g_ui = {0};

enum { NUM_CUBE_BUFFERS = 4 };

DemoScene g_scene = {0};
struct vktexcube_vs_uniform {
  // Must start with MVP
  float mvp[4][4];
  float position[12 * 3][4];
  float attr[12 * 3][4];
};
dd_graphics_buffer_t g_uniform_buffers[NUM_CUBE_BUFFERS];

dd_graphics_material_prototype_t g_material_prototype = {0};
dd_graphics_texture_t g_texture                       = {0};

static uint32 g_width                = 500;
static uint32 g_height               = 500;
static dd_graphics_window_t g_window = {0};

//--------------------------------------------------------------------------------------
// Mesh and VertexFormat Data
//--------------------------------------------------------------------------------------
// clang-format off
static const float g_vertex_buffer_data[] = {
    -1.0f,-1.0f,-1.0f,  // -X side
    -1.0f,-1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,

    -1.0f,-1.0f,-1.0f,  // -Z side
     1.0f, 1.0f,-1.0f,
     1.0f,-1.0f,-1.0f,
    -1.0f,-1.0f,-1.0f,
    -1.0f, 1.0f,-1.0f,
     1.0f, 1.0f,-1.0f,

    -1.0f,-1.0f,-1.0f,  // -Y side
     1.0f,-1.0f,-1.0f,
     1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f,-1.0f,
     1.0f,-1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,

    -1.0f, 1.0f,-1.0f,  // +Y side
    -1.0f, 1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f,-1.0f,
     1.0f, 1.0f, 1.0f,
     1.0f, 1.0f,-1.0f,

     1.0f, 1.0f,-1.0f,  // +X side
     1.0f, 1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f,-1.0f,-1.0f,
     1.0f, 1.0f,-1.0f,

    -1.0f, 1.0f, 1.0f,  // +Z side
    -1.0f,-1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
    -1.0f,-1.0f, 1.0f,
     1.0f,-1.0f, 1.0f,
     1.0f, 1.0f, 1.0f,
};

static const float g_uv_buffer_data[] = {
    0.0f, 1.0f,  // -X side
    1.0f, 1.0f,
    1.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f,

    1.0f, 1.0f,  // -Z side
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,

    1.0f, 0.0f,  // -Y side
    1.0f, 1.0f,
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 0.0f,

    1.0f, 0.0f,  // +Y side
    0.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,

    1.0f, 0.0f,  // +X side
    0.0f, 0.0f,
    0.0f, 1.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,

    0.0f, 0.0f,  // +Z side
    0.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 0.0f,
};
// clang-format on

static void scene_init() {
  g_scene.spin_angle     = 4.0f;
  g_scene.spin_increment = 0.2f;
  vec3 eye               = {0.0f, 3.0f, 5.0f};
  vec3 origin            = {0, 0, 0};
  vec3 up                = {0.0f, 1.0f, 0.0};
  mat4x4_perspective(g_scene.projection_matrix, (float)degreesToRadians(45.0f), 1.0f, 0.1f,
                     100.0f);
  mat4x4_look_at(g_scene.view_matrix, eye, origin, up);
  mat4x4_identity(g_scene.model_matrix);
  g_scene.projection_matrix[1][1] *= -1;  // Flip projection matrix from GL to Vulkan orientation.
}

#include "data/lunarg.ppm.inc"

/* Convert ppm image data from header file into DD_RGBA texture image */
static bool unpackPPMTexture(const uint8* in_texture_data, uint32 in_texture_data_length,
                             uint32 in_row_pitch, uint8* out_rgba_data) {
  char* cPtr;
  cPtr = (char*)in_texture_data;
  if ((unsigned char*)cPtr >= (in_texture_data + in_texture_data_length) ||
      strncmp(cPtr, "P6\n", 3)) {
    return false;
  }
  while (strncmp(cPtr++, "\n", 1))
    ;
  uint32 width, height;
  sscanf(cPtr, "%u %u", &width, &height);
  if (out_rgba_data == NULL) {
    return true;
  }
  while (strncmp(cPtr++, "\n", 1))
    ;
  if ((unsigned char*)cPtr >= (in_texture_data + in_texture_data_length) ||
      strncmp(cPtr, "255\n", 4)) {
    return false;
  }
  while (strncmp(cPtr++, "\n", 1))
    ;
  for (uint32 y = 0; y < height; y++) {
    uint8* rowPtr = out_rgba_data;
    for (uint32 x = 0; x < width; x++) {
      memcpy(rowPtr, cPtr, 3);
      rowPtr[3] = 255; /* Alpha of 1 */
      rowPtr += 4;
      cPtr += 3;
    }
    out_rgba_data += in_row_pitch;
  }

  return true;
}

void demo_update_data_buffer(dd_graphics_buffer_t buffer) {
  mat4x4 MVP, Model, VP;
  int matrixSize = sizeof(MVP);

  mat4x4_mul(VP, g_scene.projection_matrix, g_scene.view_matrix);

  // Rotate around the Y axis
  mat4x4_dup(Model, g_scene.model_matrix);
  mat4x4_rotate(g_scene.model_matrix, Model, 0.0f, 1.0f, 0.0f,
                (float)degreesToRadians(g_scene.spin_angle));
  mat4x4_mul(MVP, VP, g_scene.model_matrix);

  g_graphics_api->buffer.update_data(g_graphics_device, buffer, 0, (const void*)&MVP[0][0],
                                     matrixSize);
}

void non_vulkan_draw_frame(dd_graphics_commandbuffer_t draw_commandbuffer,
                           const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor) {
  float viewport_dimension;
  float x, y;
  x = y = 0;
  if (g_width < g_height) {
    viewport_dimension = (float)g_width;
    y                  = (g_height - g_width) / 2.0f;
  } else {
    viewport_dimension = (float)g_height;
    x                  = (g_width - g_height) / 2.0f;
  }

  g_graphics_api->commandbuffer.begin_render_pass(draw_commandbuffer, in_render_pass_descriptor);

  g_graphics_api->commandbuffer.set_viewport(draw_commandbuffer, x, y, viewport_dimension,
                                             viewport_dimension, 0, 1);
  static int buffer_index = 0;
  buffer_index            = (buffer_index + 1) % NUM_CUBE_BUFFERS;
  g_graphics_api->commandbuffer.bind_material_prototype(draw_commandbuffer, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .textures = {0, &g_texture},
      .buffers  = {{.buffer = g_uniform_buffers[buffer_index],
                   .offset = 0,
                   .range  = sizeof(struct vktexcube_vs_uniform)}}};

  g_graphics_api->commandbuffer.bind_buffers_and_textures(draw_commandbuffer, g_material_prototype,
                                                          &md);
  g_graphics_api->commandbuffer.draw(draw_commandbuffer, 12 * 3, 1);
  g_graphics_api->commandbuffer.end_render_pass(draw_commandbuffer);

  demo_update_data_buffer(g_uniform_buffers[buffer_index]);
}

void non_vulkan_init() {
  dd_api_registry.init(g_application_allocator);
  g_plugins = dd_plugins_api.create(g_application_allocator);

#if defined(_WIN32)
  dd_stringview_t app_folder = dd_application.executable_folder();
  array(char) path           = dd_strcat(app_folder, stringview("*.dll"), g_application_allocator);
  const char* plugin_paths[] = {path};
#else
  const char* plugin_paths[] = {"/Users/rickappleton/dev/daedalus_c/build/x64/Debug/"};
#endif
  dd_plugins_api.load_plugins(g_plugins, plugin_paths, countof(plugin_paths));

  ui_api        = (struct dd_ui_api*)dd_api_registry.get_api("ui", 1);
  g_draw2d_api_ = (struct dd_draw2d_api*)dd_api_registry.get_api("draw2d", 1);
#if defined(__APPLE__)
  chdir("/Users/rickappleton/dev/daedalus_c/build/x64/Debug/");
#endif

  g_ui.draw_data.allocator = g_application_allocator;

  // ui_api.init(&g_ui, &g_draw2d_api_);

  g_graphics_device = g_graphics_api->get_device();

  scene_init();

  // Vulkan has been inited sufficiently to initialize data

  uint32 bytes_count[NUM_CUBE_BUFFERS];
  for (unsigned i = 0; i < NUM_CUBE_BUFFERS; i++) {
    bytes_count[i] = sizeof(struct vktexcube_vs_uniform);
  }
  g_graphics_api->buffer.allocate(g_graphics_device, NUM_CUBE_BUFFERS, bytes_count,
                                  g_uniform_buffers);

  struct vktexcube_vs_uniform data;
  {
    mat4x4 MVP, VP;
    mat4x4_mul(VP, g_scene.projection_matrix, g_scene.view_matrix);
    mat4x4_mul(MVP, VP, g_scene.model_matrix);
    memcpy(data.mvp, MVP, sizeof(MVP));
    for (unsigned int i = 0; i < 12 * 3; i++) {
      data.position[i][0] = g_vertex_buffer_data[i * 3];
      data.position[i][1] = g_vertex_buffer_data[i * 3 + 1];
      data.position[i][2] = g_vertex_buffer_data[i * 3 + 2];
      data.position[i][3] = 1.0f;
      data.attr[i][0]     = g_uv_buffer_data[2 * i];
      data.attr[i][1]     = g_uv_buffer_data[2 * i + 1];
      data.attr[i][2]     = 0;
      data.attr[i][3]     = 0;
    }
  }
  for (unsigned i = 0; i < NUM_CUBE_BUFFERS; i++) {
    g_graphics_api->buffer.update_data(g_graphics_device, g_uniform_buffers[i], 0,
                                       (const void*)&data, sizeof(data));
  }

  const uint8* vs_code =
      read_file("../source/examples/vulkan/data/vert.inc", tmp_malloc_allocator);
  const uint8* fs_code =
      read_file("../source/examples/vulkan/data/frag.inc", tmp_malloc_allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
      .vs_code       = (const uint8*)vs_code,
      .fs_code       = (const uint8*)fs_code,
      .vs_byte_count = (uint32)array_count(vs_code),
      .fs_byte_count = (uint32)array_count(fs_code),
      .bindings      = {{BindingTypeUniformBuffer}, {BindingTypeTexture}}};
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  const uint32 texture_dimensions     = 256;  // knowledge of the texture, should be more flexible
  dd_graphics_texture_descriptor_t td = {.format          = R8G8B8A8,
                                         .width           = texture_dimensions,
                                         .height          = texture_dimensions,
                                         .layers          = 1,
                                         .mip_level_count = 1,
                                         .usage           = TEXTURE_USAGE_SHADER_READ,
                                         .storage_mode    = SHARED};
  g_texture                           = g_graphics_api->texture.create(g_graphics_device, &td);
  uint8* tex_data = (uint8*)malloc(texture_dimensions * texture_dimensions * 4);
  unpackPPMTexture(lunarg_ppm, lunarg_ppm_len, texture_dimensions * 4, tex_data);
  g_graphics_api->texture.set_pixels(g_graphics_device, g_texture,
                                     (dd_rect_t){0, 0, texture_dimensions, texture_dimensions}, 0,
                                     0, texture_dimensions * texture_dimensions * 4, tex_data);
  free(tex_data);
}

void non_vulkan_deinit() {
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype);
  g_graphics_api->texture.destroy(g_graphics_device, 1, &g_texture);
  g_graphics_api->buffer.destroy(g_graphics_device, NUM_CUBE_BUFFERS, g_uniform_buffers);
}

extern int dd_vk_main_loop(dd_graphics_window_t* window, struct dd_allocator* allocator);
extern void dd_helper_set_functions(void (*init)(), void (*deinit)(),
                                    dd_graphics_api_helper_draw_callback draw_cb,
                                    dd_graphics_api_helper_compute_callback compute_cb);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
  dd_prof_thread_init("main");
  dd_allocator* allocator = (dd_allocator*)g_alloc_buffer;
  dd_heap_allocator_init(allocator, sizeof(g_alloc_buffer), "startup_allocator");

  dd_allocator* main_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(main_allocator, "main_allocator");

  g_application_allocator = main_allocator;

  dd_helper_set_functions(non_vulkan_init, non_vulkan_deinit, non_vulkan_draw_frame, NULL);

  const char* name = "Vulkan Cube";
  g_window         = dd_create_window(hInstance, name, 100, 100, g_width, g_height, 200, 200,
                              DD_WINDOW_FLAGS_ADJUST_SIZE, main_allocator);

  int result = dd_vk_main_loop(&g_window, main_allocator);

  return result;
}
