echo on

call run.bat ignore 2> timing_ignore.txt
call run.bat stdout 2> timing_stdout.txt
call run.bat stdout > redirect_log.txt 2> timing_stdout_redirect.txt
call run.bat file 2> timing_file.txt
call run.bat remote_print 2> timing_remote_print.txt
call run.bat remote_combine_print 2> timing_remote_combine_print.txt
call run.bat remote_process 2> timing_remote_process.txt