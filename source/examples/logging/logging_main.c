#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif
#include <stdio.h>
#include <string.h>

#include "core/log.h"
#include "core/timer.h"
#include "core/utils.h"

extern void log_impl_init();
extern void log_impl_process(void* buffer, unsigned byte_count);
extern void log_impl_deinit();

int main() {
  log_impl_init();

  dd_log_set_process_callback(&log_impl_process);

  uint64 current_time = dd_time_microseconds();
  uint64 end_time     = current_time + 1000 * 1000 * 10;

  unsigned num_loops = 0;
  while (current_time < end_time) {
    DD_LOG(ELogSeverityDebug, "simple value %u\n", current_time);

    current_time = dd_time_microseconds();
    ++num_loops;
    dd_log_flush();
    dd_sleep(5);
  }

  dd_log_flush();
  dd_sleep(500);

  log_impl_deinit();

  fprintf(stderr, "Num logs in 1 second: %i\n", num_loops);
}
