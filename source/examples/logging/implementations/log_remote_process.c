/* Send data to remote connection on flush
 */
#include <string.h>

#include "core/containers.h"
#include "core/log.h"
#include "core/log_remote.h"
#include "core/log_structs.inl"
#include "core/memory.h"
#include "core/network.h"

extern void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                                dd_log_print_callback func);

static dd_network_connection_t* conn = {0};

void log_impl_init() { conn = dd_network_connect("localhost", 12345, tmp_malloc_allocator); }

void log_impl_process(void* buffer, unsigned byte_count) {
  // No processing on the client, simply send everything to the server to be processed there.
  // TODO: server doesn't have strings, still needs to be implemented
  // First collect all the strings needed
  uint8* entry_data = {0};
  uint8* it         = (uint8*)buffer;
  uint8* it_end     = (uint8*)(it + byte_count);
  while (it < it_end) {
    uint8* entry_start  = it;
    dd_log_entry* entry = *(dd_log_entry**)it;

    // Assume both process and remote are both 32 or 64bit
    if (entry->did_send_to_remote == 0) {
      array_append(entry_data, &entry, sizeof(entry), tmp_malloc_allocator);
      array_append(entry_data, entry, sizeof(*entry),
                   tmp_malloc_allocator);  // TODO: don't need full entry
      array_append(entry_data, entry->function, (unsigned)strlen(entry->function) + 1,
                   tmp_malloc_allocator);
      array_append(entry_data, entry->file, (unsigned)strlen(entry->file) + 1,
                   tmp_malloc_allocator);
      array_append(entry_data, entry->format, (unsigned)strlen(entry->format) + 1,
                   tmp_malloc_allocator);
      entry->did_send_to_remote = 1;
    }

#if defined(__x86_64)
    it = entry_start + sizeof(entry) +
         entry->count_args * (sizeof(int) + sizeof(double) + sizeof(void*));
#else
    it = entry_start + sizeof(entry) + entry->count_args * sizeof(double);
#endif
  }

  // May not have new data to send here
  if (entry_data) {
    dd_log_remote_message_header_t msg = {MSG_LOG_PROCESS_STRINGS,
                                          (uint32)array_count(entry_data)};
    dd_network_connection_send_data(conn, &msg, sizeof(msg));
    dd_network_connection_send_data(conn, entry_data, msg.payload_num_bytes);
  }
  {
    dd_log_remote_message_header_t msg = {MSG_LOG_PROCESS_DATA, byte_count};
    dd_network_connection_send_data(conn, &msg, sizeof(msg));
    dd_network_connection_send_data(conn, buffer, byte_count);
  }
}

void log_impl_deinit() { dd_network_close_connection(conn); }
