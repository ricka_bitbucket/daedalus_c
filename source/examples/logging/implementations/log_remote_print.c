/* Send data to remote connection on flush
 */
#include <string.h>

#include "core/log.h"
#include "core/log_remote.h"
#include "core/memory.h"
#include "core/network.h"

extern void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                                dd_log_print_callback func);

static dd_network_connection_t* conn = {0};

void log_impl_init() { conn = dd_network_connect("localhost", 12345, tmp_malloc_allocator); }

void log_impl_print(const char* file_path, unsigned line, const char* function_name,
                    ELogSeverity severity, const char* formatted_log) {
  dd_log_remote_message_header_t msg = {MSG_LOG_STRINGS, (unsigned)strlen(formatted_log) + 1};
  dd_network_connection_send_data(conn, &msg, sizeof(msg));
  dd_network_connection_send_data(conn, formatted_log, msg.payload_num_bytes);
}

void log_impl_process(void* buffer, unsigned byte_count) {
  dd_log_process_buffer_with_callback(buffer, byte_count, &log_impl_print);
}

void log_impl_deinit() { dd_network_close_connection(conn); }
