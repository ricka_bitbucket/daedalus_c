/* Print log to file on flush
 */
#include <stdio.h>

#include "core/log.h"

extern void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                                dd_log_print_callback func);

static FILE* log_file;

void log_impl_init() { log_file = fopen("log.txt", "wb"); }

void log_impl_print(const char* file_path, unsigned line, const char* function_name,
                    ELogSeverity severity, const char* formatted_log) {
  fprintf(log_file, formatted_log);
}

void log_impl_process(void* buffer, unsigned byte_count) {
  dd_log_process_buffer_with_callback(buffer, byte_count, &log_impl_print);
}

void log_impl_deinit() { fclose(log_file); }
