/* Send data to remote connection on flush as a single send
 */
#include <stdio.h>
#include <string.h>

#include "core/containers.h"
#include "core/log.h"
#include "core/log_remote.h"
#include "core/memory.h"
#include "core/network.h"
#include "core/types.h"

extern void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                                dd_log_print_callback func);

static dd_network_connection_t* conn = {0};
static uint8* flush_buffer           = {0};

void log_impl_init() { conn = dd_network_connect("localhost", 12345, tmp_malloc_allocator); }

void log_impl_print(const char* file_path, unsigned line, const char* function_name,
                    ELogSeverity severity, const char* formatted_log) {
  unsigned l = (unsigned)strlen(formatted_log) + 1;
  array_append(flush_buffer, formatted_log, l, tmp_malloc_allocator);
}

void log_impl_process(void* buffer, unsigned byte_count) {
  dd_log_process_buffer_with_callback(buffer, byte_count, &log_impl_print);

  // Send all the lines in one go
  dd_log_remote_message_header_t msg = {MSG_LOG_STRINGS, (uint32)array_count(flush_buffer)};
  // printf("Sending %i bytes (excl header %i)\n", msg.payload_num_bytes, (unsigned)sizeof(msg));
  dd_network_connection_send_data(conn, &msg, sizeof(msg));
  dd_network_connection_send_data(conn, flush_buffer, array_count(flush_buffer));

  // Empty buffer
  array_header(flush_buffer)->count_ = 0;
}

void log_impl_deinit() { dd_network_close_connection(conn); }
