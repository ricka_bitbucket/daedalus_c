
/* Print log to stdout on flush
 */
#include <stdio.h>

#include "core/log.h"

extern void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                                dd_log_print_callback func);

void log_impl_init() {
  // Do nothing
}

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
#pragma clang diagnostic ignored "-Wformat-security"
#endif
void log_impl_print(const char* file_path, unsigned line, const char* function_name,
                    ELogSeverity severity, const char* formatted_log) {
  printf(formatted_log);
}
#if defined(__clang__)
#pragma clang diagnostic pop
#endif

void log_impl_process(void* buffer, unsigned byte_count) {
  dd_log_process_buffer_with_callback(buffer, byte_count, &log_impl_print);
}

void log_impl_deinit() {
  // Do nothing
}
