//
//  GameViewController.m
//  metal
//
//  Created by Rick Appleton on 02/06/2020.
//  Copyright © 2020 Daedalus Development. All rights reserved.
//

#import "GameViewController.h"
#import "Renderer.h"

#include "core/hid.h"
#include "ui/ui.h"

@implementation GameViewController
{
  MTKView *_view;
  
  Renderer *_renderer;
  
  NSTrackingArea* trackingArea;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  _view = (MTKView *)self.view;
  
  _view.device = MTLCreateSystemDefaultDevice();
  
  if(!_view.device)
  {
    NSLog(@"Metal is not supported on this device");
    self.view = [[NSView alloc] initWithFrame:self.view.frame];
    return;
  }
  
  _renderer = [[Renderer alloc] initWithMetalKitView:_view];
  
  [_renderer mtkView:_view drawableSizeWillChange:_view.bounds.size];
  
  _view.delegate = _renderer;
  
  NSTrackingAreaOptions trackingOptions = // NSTrackingEnabledDuringMouseDrag | // don't track during drag
  NSTrackingMouseMoved |
  NSTrackingMouseEnteredAndExited |
  //NSTrackingActiveInActiveApp | NSTrackingActiveInKeyWindow | NSTrackingActiveWhenFirstResponder |
  NSTrackingActiveAlways;
  trackingArea = [[NSTrackingArea alloc] initWithRect:_view.bounds options:trackingOptions owner:_view userInfo:nil];
  [_view addTrackingArea:trackingArea];
}

@end

