//
//  GameViewController.h
//  metal
//
//  Created by Rick Appleton on 02/06/2020.
//  Copyright © 2020 Daedalus Development. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import "Renderer.h"

// Our macOS view controller.
@interface GameViewController : NSViewController

@end
