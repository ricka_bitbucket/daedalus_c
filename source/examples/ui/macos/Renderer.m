//
//  Renderer.m
//  metal
//
//  Created by Rick Appleton on 02/06/2020.
//  Copyright © 2020 Daedalus Development. All rights reserved.
//

#import <simd/simd.h>
#import <ModelIO/ModelIO.h>

#import "Renderer.h"

#include "core/hid.h"
#include "core/memory.h"
#include "core/types.h"
#include "graphics_api/graphics.h"
#include "metal_api/metal_types.inl"

#include "ui/ui.h"

static const NSUInteger kMaxBuffersInFlight = 3;

extern struct dd_ui_api* ui_api;

extern dd_graphics_window_t dd_create_window(void* in_handle, const char* name, uint32 left, uint32 top, uint32 width,
                                      uint32 height, uint32 minimum_width,
                                      uint32 minimum_height, uint32 flags, dd_allocator* allocator);

// TODO
extern void dd_mtl_set_device(id<MTLDevice> in_device);







static struct {
  void (*init)();
  void (*deinit)();
  dd_graphics_api_helper_draw_callback draw;
} g_mtl_helper = {0};

void dd_helper_set_functions(void (*init)(),
                                 void (*deinit)(),
                                 dd_graphics_api_helper_draw_callback draw) {
  g_mtl_helper.init = init;
  g_mtl_helper.deinit = deinit;
  g_mtl_helper.draw = draw;
}

@interface MyMTKView : MTKView
@end
@implementation MyMTKView

-(BOOL)acceptsFirstResponder
{
  return YES;
}
- (void)keyDown:(NSEvent *)event {
  int32 key_code = [event keyCode];
  NSEventModifierFlags flags = [event modifierFlags];
  
  switch( [event keyCode] ) {
    case 126:   // up arrow
    case 125:   // down arrow
    case 124:   // right arrow
    case 123:   // left arrow
      break;
    default:
      key_code = [[event characters] characterAtIndex:0];
      if( key_code=='v' && (flags & NSEventModifierFlagCommand)) {
        dd_ui_event_t e = {.event_type = UIEventPaste};
        ui_api->interaction.push_event(e, tmp_malloc_allocator);
      } else if( key_code=='c' && (flags & NSEventModifierFlagCommand)) {
        dd_ui_event_t e = {.event_type = UIEventCopy};
        ui_api->interaction.push_event(e, tmp_malloc_allocator);
      } else if( key_code=='x' && (flags & NSEventModifierFlagCommand)) {
        dd_ui_event_t e = {.event_type = UIEventCut};
        ui_api->interaction.push_event(e, tmp_malloc_allocator);
      } else {
        dd_ui_event_t e = {.event_type = UIEventKeyInputChar, .data = key_code};
        ui_api->interaction.push_event(e, tmp_malloc_allocator);
      }
      break;
  }
}

- (void)keyUp:(NSEvent *)event {
  // Empty for now
}

- (void)mouseDown:(NSEvent *)event {
  dd_ui_event_t e = {.event_type = UIEventMouseButton0Down};
  ui_api->interaction.push_event(e, tmp_malloc_allocator);
}

- (void)mouseUp:(NSEvent *)event {
  dd_ui_event_t e = {.event_type = UIEventMouseButton0Up};
  ui_api->interaction.push_event(e, tmp_malloc_allocator);
}

- (void)mouseMoved:(NSEvent *)event {
  NSPoint curPoint = [event locationInWindow];
  ui_api->interaction.update_mouse_pos((int16)curPoint.x, (int16)(self.bounds.size.height - curPoint.y));
}

- (void)scrollWheel:(NSEvent *)event {
  int32 delta     = (int32)(10*[event scrollingDeltaY]);
  dd_ui_event_t e = {.event_type = UIEventMouseScroll, .data = delta};
  ui_api->interaction.push_event(e, tmp_malloc_allocator);
}

@end

@implementation Renderer
{
  dispatch_semaphore_t _inFlightSemaphore;
  id <MTLDevice> _device;
  id <MTLCommandQueue> _commandQueue;
  
}

-(nonnull instancetype)initWithMetalKitView:(nonnull MTKView *)view
{
  self = [super init];
  if(self)
  {
    // Configure the view
    view.depthStencilPixelFormat = MTLPixelFormatDepth16Unorm;
    view.colorPixelFormat = MTLPixelFormatBGRA8Unorm;
    view.sampleCount = 1;
    view.clearColor = MTLClearColorMake(1,0,1,1);
    
    uint32 flags = 0;
    (void) dd_create_window( NULL, "Daedalus HUB", 50,50, (uint32)view.bounds.size.width,
                                           (uint32)view.bounds.size.height,  0,0, flags, tmp_malloc_allocator) ;

    _device = view.device;
    
    _commandQueue = [_device newCommandQueue];
    _inFlightSemaphore = dispatch_semaphore_create(kMaxBuffersInFlight);
    
    dd_mtl_set_device(_device);
    
    g_mtl_helper.init();
  }
  
  return self;
}


- (void)drawInMTKView:(nonnull MTKView *)view
{
  /// Per frame updates here
  dispatch_semaphore_wait(_inFlightSemaphore, DISPATCH_TIME_FOREVER);
  
  id <MTLCommandBuffer> commandBuffer = [_commandQueue commandBuffer];
  commandBuffer.label = @"MyCommand";
  
  __block dispatch_semaphore_t block_sema = _inFlightSemaphore;
  [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer)
   {
    dispatch_semaphore_signal(block_sema);
  }];
  
  
  
  /// Delay getting the currentRenderPassDescriptor until we absolutely need it to avoid
  ///   holding onto the drawable and blocking the display pipeline any longer than necessary
  MTLRenderPassDescriptor* renderPassDescriptor = view.currentRenderPassDescriptor;
  
  if(renderPassDescriptor != nil) {
    
    /// Final pass rendering code here
    
     
    struct metal_commandbuffer_t mtl_cmd_buf = {
      .cmd_buffer = commandBuffer,
    };
    dd_graphics_render_pass_descriptor_t rpb = {
      .mtl_render_pass_descriptor = renderPassDescriptor,
      .width = (uint32)view.bounds.size.width,
      .height = (uint32)view.bounds.size.height
    };
    
    g_mtl_helper.draw((dd_graphics_commandbuffer_t){&mtl_cmd_buf}, &rpb);
    
    [commandBuffer presentDrawable:view.currentDrawable];
  }
  
  [commandBuffer commit];
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size
{
  /// Respond to drawable size or orientation changes here
  //TODO: implement
}

@end


