//
//  AppDelegate.h
//  metal
//
//  Created by Rick Appleton on 02/06/2020.
//  Copyright © 2020 Daedalus Development. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
