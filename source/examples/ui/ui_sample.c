#include <assert.h>
#include <stdio.h>
#include <string.h>
#if !defined(_WIN32)
  #include <unistd.h>
#endif

#include "core/api_registry.h"
#include "core/application.h"
#include "core/containers.h"
#include "core/dd_string.h"
#include "core/dll.h"
#include "core/hid.h"
#include "core/http.h"
#include "core/memory.h"
#include "core/plugins.h"
#include "core/profiler.h"
#include "core/thread.h"
#include "core/types.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "graphics_api/graphics.h"
#include "telemetry/telemetry_client.h"
#include "ui/ui.h"

// TODO: use plugin manager
#if defined(_WIN32)
extern dd_graphics_api_t dd_vulkan;
dd_graphics_api_t* g_graphics_api = &dd_vulkan;
#else
extern dd_graphics_api_t dd_metal;
dd_graphics_api_t* g_graphics_api = &dd_metal;
#endif

static struct dd_plugins_o* g_plugins;

extern dd_allocator* g_application_allocator;
#if defined(_WIN32)
const char* d2d_dyn_path = "e:/daedalus_c/build/x64/Debug/draw2d.dll";
const char* ui_dyn_path  = "e:/daedalus_c/build/x64/Debug/ui.dll";
#else
const char* d2d_dyn_path = "/Users/rickappleton/dev/daedalus_c/build/x64/Debug/libdraw2d.dylib";
const char* ui_dyn_path  = "/Users/rickappleton/dev/daedalus_c/build/x64/Debug/libui.dylib";
#endif

static dd_graphics_device_t g_graphics_device;
dd_graphics_buffer_t g_quad_instance_buffer[3];
dd_graphics_buffer_t g_quad_buffer[3];
dd_graphics_buffer_t g_uniform_screen_size;
dd_graphics_material_prototype_t g_material_prototype = {0};

const uint32 quad_buffer_byte_count          = 500000;
const uint32 quad_instance_buffer_byte_count = 500000;
static dd_ui_t g_ui                          = {0};
static uint32 g_width                        = 512;
static uint32 g_height                       = 512;
static const char* g_large_text              = NULL;

struct dd_ui_api* ui_api                   = NULL;
static struct dd_draw2d_api* g_draw2d_api_ = NULL;

void send_ui_buffers_to_device(uint32 index, const dd_draw2d_t* draw_data) {
  uint32 bytes_count =
      (uint32)(sizeof(*draw_data->primitive_buffer) * array_count(draw_data->primitive_buffer));
  assert(bytes_count <= quad_instance_buffer_byte_count);
  g_graphics_api->buffer.update_data(g_graphics_device, g_quad_instance_buffer[index], 0,
                                     (const void*)draw_data->primitive_buffer, bytes_count);

  uint32 quads_byte_count = (uint32)(sizeof(*draw_data->quads) * array_count(draw_data->quads));
  assert(quads_byte_count <= quad_buffer_byte_count);
  g_graphics_api->buffer.update_data(g_graphics_device, g_quad_buffer[index], 0,
                                     (const void*)draw_data->quads, quads_byte_count);
}

const dd_ui_style_t g_theme = {
    .background = {.color = {30, 30, 30, 255}, .disabled = {50, 50, 50, 255}},
    .foreground = {.color = {255, 255, 255, 255}, .disabled = {200, 200, 200, 255}},
    .button     = {.color = {14, 99, 156, 255},
               .hover = {16, 120, 188, 255},
               .down  = {255, 92, 145, 255}},
    .status_bar = {.color_neutral = {30, 30, 30, 255},
                   .color_error   = {192, 0, 50, 255},
                   .color_success = {0, 122, 204, 255}}};

void draw_box() {
  color_rgba8_t box_color = {255, 100, 0, 255};
  ui_rect_t rc            = {260, 250, 200, 200};
  g_draw2d_api_->rect(&g_ui.draw_data, rc, box_color);

  ui_rect_t top = rect_split_off_top(&rc, 20, 0);

  rc.y -= 4;
  rc.height += 4;
  rc = rect_shrink(rc, 1);
  g_draw2d_api_->rect(&g_ui.draw_data, rc, g_theme.background.color);
  rc = rect_shrink(rc, 1);
  g_draw2d_api_->rect(&g_ui.draw_data, rc, box_color);
  rc = rect_shrink(rc, 1);
  g_draw2d_api_->rect(&g_ui.draw_data, rc, g_theme.background.color);

  g_draw2d_api_->rect(&g_ui.draw_data, top, box_color);
  ui_api->widget.label(&g_ui, top, (color_rgba8_t){255, 255, 255, 255}, TextAlignmentCenter,
                       stringview("label"));
}

void draw_triangles() {
  color_rgba8_t colors[] = {(color_rgba8_t){255, 0, 0, 255}, (color_rgba8_t){0, 255, 0, 255},
                            (color_rgba8_t){0, 0, 255, 255}, (color_rgba8_t){0, 255, 255, 255}};
  float4 triangle[]      = {(float4){100, 300, 0, 0}, (float4){150, 400, 0, 0},
                       (float4){200, 300, 0, 0}, (float4){100, 300, 0, 0},
                       (float4){150, 200, 0, 0}, (float4){200, 300, 0, 0}};
  for (uint32 i = 0; i < countof(triangle); i++) {
    color_rgba8_t color = colors[i % countof(colors)];
    triangle[i].w       = *(float*)&color;
  }

  g_draw2d_api_->triangles(&g_ui.draw_data, triangle, countof(triangle));
}

void draw_scroll_area(ui_rect_t r) {
  g_draw2d_api_->rect(&g_ui.draw_data, r, (color_rgba8_t){0, 0, 0, 255});

  ui_rect_t r_element        = r;
  r_element.height           = 60;
  const uint32 element_count = 8;
  // const uint32 visible_count = 2;

  ui_rect_t canvas              = {.height = (int16)(element_count * r_element.height),
                      .width  = r_element.width};
  dd_ui_scrollview_t scrollview = {.id = __LINE__, .rect = r, .canvas = canvas};

  float scroll_x        = 0;  // Going to ignore this anyway
  static float scroll_y = 0;
  ui_rect_t r_content;
  ui_api->widget.scrollview_begin(&g_ui, &scrollview, &scroll_x, &scroll_y, &r_content);

  r               = r_content;
  r_element.width = r.width;

  const ui_rect_t prev_clip_rect = g_draw2d_api_->set_clip_rect(&g_ui.draw_data, r_content);

  r_element.y = r_content.y - scroll_y;
  for (uint32 i = 0; i < element_count; i++, r_element.y += r_element.height) {
    uint32 element_id = (__LINE__ << 10) + i;

    g_draw2d_api_->rect(&g_ui.draw_data, r_element, (color_rgba8_t){255, 55, 0, 255});
    color_rgba8_t color = (color_rgba8_t){255, 0, 0, 255};
    if (g_ui.hover_id == element_id) {
      color = (color_rgba8_t){0, 0, 255, 255};
    }
    g_draw2d_api_->rect(&g_ui.draw_data, rect_shrink(r_element, 1), color);
    ui_rect_t label = {.x      = r_element.x + 10,
                       .y      = r_element.y + r_element.height / 2 - 4,
                       .width  = r_element.width - 20,
                       .height = 7};
    char buf[10];
    sprintf(buf, "element %i", i);
    ui_api->widget.label(&g_ui, label, (color_rgba8_t){0, 255, 0, 255}, TextAlignmentLeft,
                         stringview(buf));

    if (rect_contains_point(r_element, g_ui.mouse.x, g_ui.mouse.y)) {
      ui_api->interaction.set_next_hover(&g_ui, g_ui.current_ui_layer, element_id);
    }
  }

  ui_api->widget.scrollview_end(&g_ui, &scrollview, &scroll_x, &scroll_y);

  (void)g_draw2d_api_->set_clip_rect(&g_ui.draw_data, prev_clip_rect);
}

void draw_tab1(ui_rect_t r) {
  ui_rect_t r_left  = rect_split_off_left(&r, r.width / 2, 0);
  ui_rect_t r_right = r;

  ui_rect_t r_white_label = rect_split_off_top(&r_left, 7, 1);
  ui_rect_t r_red_label   = rect_split_off_top(&r_left, 7, 1);
  ui_rect_t r_green_label = rect_split_off_top(&r_left, 7, 1);
  r_green_label.x         = -3;
  ui_api->widget.label(&g_ui, r_white_label, (color_rgba8_t){255, 255, 255, 255},
                       TextAlignmentLeft, stringview("White label"));
  ui_api->widget.label(&g_ui, r_red_label, (color_rgba8_t){255, 0, 0, 255}, TextAlignmentCenter,
                       stringview("Red label"));
  ui_api->widget.label(&g_ui, r_green_label, (color_rgba8_t){0, 255, 0, 255}, TextAlignmentRight,
                       stringview("Green label"));

  static bool is_checked = true;

  ui_rect_t r_dropdown        = rect_split_off_top(&r_left, 13, 1);
  ui_rect_t r_dropdown_result = rect_split_off_top(&r_left, 13, 1);
  ui_rect_t rc                = rect_split_off_top(&r_left, 16, 1);
  ui_rect_t button1           = rect_split_off_top(&r_left, 16, 1);
  ui_rect_t button2           = rect_split_off_top(&r_left, 16, 1);
  ui_rect_t label             = rect_split_off_top(&r_left, 7, 1);

  const dd_stringview_t options[] = {
      stringview("Option 1"),  stringview("Option 2"),  stringview("Option 3"),
      stringview("Option 4"),  stringview("Option 5"),  stringview("Option 6"),
      stringview("Option 7"),  stringview("Option 8"),  stringview("Option 9"),
      stringview("Option 10"), stringview("Option 11"), stringview("Option 12")};
  const bool disabled_options[] = {false, false, false, false, false, false,
                                   false, true,  false, false, false, false};

  static uint32 selected_index = 0;
  dd_ui_dropdown_t dropdown    = {.id               = __LINE__,
                               .rect             = r_dropdown,
                               .options          = options,
                               .options_disabled = disabled_options,
                               .options_count    = countof(options)};
  if (ui_api->widget.dropdown(&g_ui, &dropdown, &selected_index)) {
    // User selected something
  }

  ui_api->widget.label(&g_ui, r_dropdown_result, (color_rgba8_t){255, 255, 255, 255},
                       TextAlignmentLeft, options[selected_index]);

  rc.width = rc.height;
  ui_api->widget.checkbox(&g_ui, rc, stringview("show buttons"), 156, &is_checked);
  if (is_checked) {
    static bool did_click       = false;
    ui_rect_t rb                = button1;
    rb.width                    = 32;
    const dd_ui_button_t b_show = {.rect = rb, .id = __LINE__, .label = stringview("show")};
    if (ui_api->widget.button(&g_ui, &b_show)) {
      did_click = true;
    }
    rb                          = button2;
    rb.width                    = 32;
    const dd_ui_button_t b_hide = {.rect = rb, .id = __LINE__, .label = stringview("hide")};
    if (ui_api->widget.button(&g_ui, &b_hide)) {
      did_click = false;
    }
    if (did_click) {
      ui_api->widget.label(&g_ui, label, (color_rgba8_t){255, 255, 255, 255}, TextAlignmentLeft,
                           stringview("Hello World!"));
    }
  }

  ui_rect_t r_scroll_area = rect_split_off_top(&r_right, 200, 1);
  draw_scroll_area(r_scroll_area);

  draw_box();

  draw_triangles();
}

void draw_tab2(ui_rect_t r) {
  DD_PROF();
  ui_rect_t rt                  = rect_split_off_top(&r, 16, 1);
  static char text[1024]        = "Editable text";
  dd_ui_inputfield_t inputfield = {.id = __LINE__, .rect = rt};
  ui_api->widget.inputfield(&g_ui, &inputfield, text, sizeof(text));

  static float line_offset = 0;
  ui_api->widget.textarea(&g_ui, r, g_large_text, &line_offset, __LINE__);
  DD_PROF_END();
}

void draw_tab3(ui_rect_t r) {
  ui_rect_t r_left  = rect_split_off_left(&r, r.width / 2, 0);
  ui_rect_t r_right = r;
  (void)r_right;

  static bool is_checked = true;

  ui_rect_t r_dropdowns        = rect_split_off_top(&r_left, 13, 1);
  ui_rect_t r_dropdowns_result = rect_split_off_top(&r_left, 13, 1);
  ui_rect_t r_dropdown1        = rect_split_off_left(&r_dropdowns, r_dropdowns.width / 2, 1);
  ui_rect_t r_dropdown1_result =
      rect_split_off_top(&r_dropdowns_result, r_dropdowns_result.width / 2, 1);
  ui_rect_t r_dropdown2        = r_dropdowns;
  ui_rect_t r_dropdown2_result = r_dropdowns_result;
  ui_rect_t rc                 = rect_split_off_top(&r_left, 16, 1);
  ui_rect_t button1            = rect_split_off_top(&r_left, 16, 1);
  ui_rect_t button2            = rect_split_off_top(&r_left, 16, 1);
  ui_rect_t label              = rect_split_off_top(&r_left, 7, 1);
  ui_rect_t r_input1           = rect_split_off_top(&r_left, 14, 2);
  ui_rect_t r_input2           = rect_split_off_top(&r_left, 14, 2);
  ui_rect_t r_input3           = rect_split_off_top(&r_left, 14, 2);

  const dd_stringview_t options[] = {
      stringview("Option 1"),  stringview("Option 2"),  stringview("Option 3"),
      stringview("Option 4"),  stringview("Option 5"),  stringview("Option 6"),
      stringview("Option 7"),  stringview("Option 8"),  stringview("Option 9"),
      stringview("Option 10"), stringview("Option 11"), stringview("Option 12")};
  static uint32 selected_index = 0;
  dd_ui_dropdown_t dropdown    = {.id               = __LINE__,
                               .rect             = r_dropdown1,
                               .options          = options,
                               .options_count    = countof(options)};
  if (ui_api->widget.dropdown(&g_ui, &dropdown, &selected_index)) {
    // User selected something
  }
  // Limit the option count here, to show that the box shrinks.
  dd_ui_dropdown_t dropdown2 = {
      .id = __LINE__, .rect = r_dropdown2, .options = options, .options_count = 3};
  if (ui_api->widget.dropdown(&g_ui, &dropdown2, &selected_index)) {    
    // User selected something
  }

  ui_api->widget.label(&g_ui, r_dropdown1_result, (color_rgba8_t){255, 255, 255, 255},
                       TextAlignmentLeft, options[selected_index]);
  ui_api->widget.label(&g_ui, r_dropdown2_result, (color_rgba8_t){255, 255, 255, 255},
                       TextAlignmentLeft, options[selected_index]);

  rc.width = rc.height;
  ui_api->widget.checkbox(&g_ui, rc, stringview("show buttons"), 156, &is_checked);
  if (is_checked) {
    static bool did_click       = false;
    ui_rect_t rb                = button1;
    rb.width                    = 32;
    const dd_ui_button_t b_show = {.rect = rb, .id = __LINE__, .label = stringview("show")};
    if (ui_api->widget.button(&g_ui, &b_show)) {
      did_click = true;
    }
    rb                          = button2;
    rb.width                    = 32;
    const dd_ui_button_t b_hide = {.rect = rb, .id = __LINE__, .label = stringview("hide")};
    if (ui_api->widget.button(&g_ui, &b_hide)) {
      did_click = false;
    }
    if (did_click) {
      ui_api->widget.label(&g_ui, label, (color_rgba8_t){255, 255, 255, 255}, TextAlignmentLeft,
                           stringview("Hello World!"));
    }
  }

  dd_ui_inputfield_t input1    = {.id = __LINE__, .rect = r_input1};
  static char input1_text[256] = {"Text1dd_os_api.clipboard.set_data(EClipboardText, text, len + 1);"};
  if (ui_api->widget.inputfield(&g_ui, &input1, input1_text, countof(input1_text))) {
  }
  dd_ui_inputfield_t input2    = {.id = __LINE__, .rect = r_input2};
  static char input2_text[256] = {"Text2"};
  if (ui_api->widget.inputfield(&g_ui, &input2, input2_text, countof(input2_text))) {
  }
  dd_ui_inputfield_t input3    = {.id = __LINE__, .rect = r_input3};
  static char input3_text[256] = {"Text3"};
  if (ui_api->widget.inputfield(&g_ui, &input3, input3_text, countof(input3_text))) {
  }
}

void draw() {
  static unsigned selected_tab = 0;

  DD_PROF();

  ui_rect_t r = {.x = 0, .y = 0, .width = (int16)g_width, .height = (int16)g_height};

  ui_rect_t status_r = rect_split_off_bottom(&r, 16, 1);
  {
    ui_api->widget.pane(&g_ui, status_r, g_theme.status_bar.color_neutral);
    ui_api->widget.label(&g_ui, rect_shrink(status_r, 1), (color_rgba8_t){255, 255, 255, 255},
                         TextAlignmentLeft, stringview("Error status"));
  }

  ui_rect_t tabs_r = rect_split_off_top(&r, 16, 1);
  {
    ui_api->widget.tab_bar(&g_ui, &tabs_r);
    const dd_stringview_t labels[] = {stringview("Widgets"), stringview("Scrollview"),
                                      stringview("Controls")};
    for (unsigned i = 0; i < countof(labels); i++) {
      if (ui_api->widget.tab_bar_item(&g_ui, &tabs_r, labels[i], (i == selected_tab))) {
        selected_tab = i;
      }
    }
  }

  ui_api->widget.pane(&g_ui, r, g_theme.background.color);

  if (selected_tab == 0) {
    draw_tab1(r);
  } else if (selected_tab == 1) {
    draw_tab2(r);
  } else if (selected_tab == 2) {
    draw_tab3(r);
  }

  DD_PROF_END();
}

/*
void reload_plugins() {
  if (dd_dll_api.load_plugin(&ui_plugin, ui_dyn_path)) {
    struct dd_ui_api* (*api)() =
        (struct dd_ui_api * (*)()) dd_dll_api.get_symbol(&ui_plugin, "get_api");
    memcpy(&ui_api, api(), sizeof(ui_api));
    dd_dll_api.unload_old_plugin(&ui_plugin);

    ui_api->init(&g_ui, &g_draw2d_api_);
  }
}*/

void sample_draw_frame(dd_graphics_commandbuffer_t draw_commandbuffer,
                       const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor) {
  DD_PROF();

  static uint32 frame = 0;
  frame               = (frame + 1) % 3;

  ui_api->begin(&g_ui, &g_theme, ui_api->interaction.pop_event());
  draw();

  const dd_draw2d_t* draw_data = ui_api->end(&g_ui);

  send_ui_buffers_to_device(frame, draw_data);

  float clear_color[4] = {0, 0, 0, 1};
  memcpy((dd_graphics_render_pass_descriptor_t*)in_render_pass_descriptor->clear_color,
         clear_color, sizeof(clear_color));
  g_graphics_api->commandbuffer.begin_render_pass(draw_commandbuffer, in_render_pass_descriptor);

  g_graphics_api->commandbuffer.set_viewport(draw_commandbuffer, 0, 0, (float)g_width,
                                             (float)g_height, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(draw_commandbuffer, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers = {{.buffer = g_quad_instance_buffer[frame],
                   .offset = 0,
                   .range  = quad_instance_buffer_byte_count},
                  {.buffer = g_quad_buffer[frame], .offset = 0, .range = quad_buffer_byte_count},
                  {.buffer = g_uniform_screen_size, .offset = 0, .range = 2 * sizeof(uint32)}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(draw_commandbuffer, g_material_prototype,
                                                          &md);
  g_graphics_api->commandbuffer.draw(draw_commandbuffer, (uint32)array_count(draw_data->quads), 1);

  g_graphics_api->commandbuffer.end_render_pass(draw_commandbuffer);

  // Send each frame
  dd_telemetry_client_send_profiler_frame();

  DD_PROF_END();
  dd_prof_flush();

  dd_plugins_api.check_for_changes(g_plugins);
}

void download() {
  dd_http_api.url_to_file(
      "http://advances.realtimerendering.com/s2020/Drobot-SIGGRAPH_2020_VRS_Final.pptx",
#if defined(_WIN32)
      "C:\\Users\\RickA\\Downloads\\otherfile.pptx"
#else
      "/Users/rickappleton/Documents/drobot.pptx"
#endif
  );
}

void sample_init() {
  download();
  dd_api_registry.init(g_application_allocator);
  g_plugins = dd_plugins_api.create(g_application_allocator);
    
#if defined(_WIN32)
  dd_stringview_t app_folder = dd_application.executable_folder();
  array(char) path           = dd_strcat(app_folder, stringview("*.dll"), g_application_allocator);
  const char* plugin_paths[] = {path};
#else
  const char* plugin_paths[] = {"/Users/rickappleton/dev/daedalus_c/build/x64/Debug/"};
#endif
  dd_plugins_api.load_plugins(g_plugins, plugin_paths, countof(plugin_paths));

  ui_api        = (struct dd_ui_api*)dd_api_registry.get_api("ui", 1);
  g_draw2d_api_ = (struct dd_draw2d_api*)dd_api_registry.get_api("draw2d", 1);
#if defined(__APPLE__)
  chdir("/Users/rickappleton/dev/daedalus_c/build/x64/Debug/");
#endif

  g_ui.draw_data.allocator = g_application_allocator;

#if defined(_WIN32)
  // VS debugs from location of project
  g_large_text = (const char*)read_file("../source/examples/ui/ui_sample.c", tmp_malloc_allocator);
#else
  // MacOS debugs from location of binary
  g_large_text =
      (const char*)read_file("../../../source/examples/ui/ui_sample.c", tmp_malloc_allocator);
#endif

  g_graphics_device = g_graphics_api->get_device();

  for (unsigned i = 0; i < 3; ++i) {
    g_graphics_api->buffer.allocate(g_graphics_device, 1, &quad_instance_buffer_byte_count,
                                    g_quad_instance_buffer + i);
    g_graphics_api->buffer.allocate(g_graphics_device, 1, &quad_buffer_byte_count,
                                    g_quad_buffer + i);
  }

  uint32 screen_size[2] = {g_width, g_height};
  uint32 bytes_count    = sizeof(screen_size);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &bytes_count, &g_uniform_screen_size);
  g_graphics_api->buffer.update_data(g_graphics_device, g_uniform_screen_size, 0,
                                     (const void*)screen_size, sizeof(screen_size));

#if defined(_WIN32)
  const uint8* vs_code = read_file("../source/examples/ui/vulkan/ui.vs.spv", tmp_malloc_allocator);
  const uint8* fs_code = read_file("../source/examples/ui/vulkan/ui.fs.spv", tmp_malloc_allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
      .vs_code       = (const uint8*)"quadsVertexShader",
      .fs_code       = (const uint8*)"quadsFragmentShader",
      .vs_byte_count = 0,
      .fs_byte_count = 0,
#endif
    .output =
        {// TODO: link formats with view
         .color_format = B8G8R8A8,
         .depth_format = DEPTH_U16_NORMALIZED},
    .bindings = {{.type = BindingTypeUniformBuffer},
                 {.type = BindingTypeUniformBuffer},
                 {.type = BindingTypeUniformBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);
}

void sample_deinit() {
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype);
  g_graphics_api->buffer.destroy(g_graphics_device, 3, g_quad_instance_buffer);
  g_graphics_api->buffer.destroy(g_graphics_device, 3, g_quad_buffer);
  g_graphics_api->buffer.destroy(g_graphics_device, 1, &g_uniform_screen_size);

  dd_plugins_api.unload_all(g_plugins);
}
