#define DD_DRAW2D_PRIMITIVE_TRIANGLE 0
#define DD_DRAW2D_PRIMITIVE_QUAD 1
#define DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH 2

struct QuadVert {
  vec4 position;
  uint type;
  int data0;
  int data1;
  vec4 color;
  vec4 clip;
};
