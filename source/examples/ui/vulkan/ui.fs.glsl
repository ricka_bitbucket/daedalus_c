#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(location = 0) out vec4 uFragColor;

#include "ui.inl"

layout(location = 0) flat in QuadVert in_quad;
layout(location = 12) in vec2 tex_coord;
layout(location = 13) in vec4 color;

void main() {
  if (in_quad.clip[2] != 0) {
    vec4 clip    = in_quad.clip;
    float left   = clip[0];
    float right  = clip[1];
    float bottom = clip[2];
    float top    = clip[3];
    if ((gl_FragCoord.x < left) || (gl_FragCoord.x > right) || (gl_FragCoord.y < bottom) ||
        (gl_FragCoord.y > top)) {
      discard;
    }
  }

  switch (in_quad.type) {
    case DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH: {
      ivec2 pixel_offset = ivec2(floor(tex_coord));
      pixel_offset.x     = 4 - pixel_offset.x;  // Flip correct way
      pixel_offset.y     = 6 - pixel_offset.y;
      uint bit_index     = uint(pixel_offset.y * 5 + pixel_offset.x);
      uint char_data;
      if (bit_index > 31) {
        char_data = in_quad.data1;
        bit_index -= 32;
      } else {
        char_data = in_quad.data0;
      }
      uint isPixel = (char_data >> bit_index) & 0x1;
      if (isPixel == 0) discard;
      uFragColor = color;
    } break;
    default:
      uFragColor = color;
      break;
  }
}