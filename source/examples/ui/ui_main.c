#include "core/memory.h"
#include "core/profiler.h"
#include "core/thread.h"
#include "graphics_api/graphics.h"
#include "telemetry/telemetry_client.h"

static uint8 g_alloc_buffer[2 * 1024];
dd_allocator* g_application_allocator;

extern void sample_init();
extern void sample_deinit();
extern void sample_draw_frame(
    dd_graphics_commandbuffer_t draw_commandbuffer,
    const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor);
extern void dd_helper_set_functions(void (*init)(), void (*deinit)(),
                                    dd_graphics_api_helper_draw_callback draw_cb,
                                    dd_graphics_api_helper_compute_callback compute_cb);

#if defined(_WIN32)

#include <Windows.h>
extern int dd_vk_main_loop(dd_graphics_window_t* window, struct dd_allocator* allocator);
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
  dd_prof_thread_init("main");
  dd_allocator* allocator = (dd_allocator*)g_alloc_buffer;
  dd_heap_allocator_init(allocator, sizeof(g_alloc_buffer), "startup_allocator");

  dd_allocator* main_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(main_allocator, "main_allocator");

  g_application_allocator = main_allocator;

  dd_helper_set_functions(sample_init, sample_deinit, sample_draw_frame, NULL);

  const char* name            = "Vulkan IMGui test";
  dd_graphics_window_t window = dd_create_window(hInstance, name, 1300, 100, 512, 512, 200, 200,
                                                 DD_WINDOW_FLAGS_ADJUST_SIZE, allocator);
  return dd_vk_main_loop(&window, main_allocator);
}

#elif defined(__APPLE__)

#pragma clang diagnostic ignored "-Wnullability-extension"
extern int NSApplicationMain(int argc, const char* _Nonnull argv[_Nonnull]);
int main(int argc, const char* argv[]) {
  dd_prof_thread_init("main");
  dd_allocator* allocator = (dd_allocator*)g_alloc_buffer;
  dd_heap_allocator_init(allocator, sizeof(g_alloc_buffer), "startup_allocator");

  dd_allocator* main_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(main_allocator, "main_allocator");

  g_application_allocator = main_allocator;

  dd_helper_set_functions(sample_init, sample_deinit, sample_draw_frame);
  return NSApplicationMain(argc, argv);
}

#endif
