//
//  quads.h
//  totexture
//
//  Created by Rick Appleton on 08/03/2020.
//  Copyright © 2020 Daedalus Development. All rights reserved.
//

#ifndef quads_h
#define quads_h

#import <simd/simd.h>

struct QuadInfo {
  packed_short4 geometry;
  unsigned int type : 5;
  unsigned int data_offset : 27;
};

#endif /* quads_h */
