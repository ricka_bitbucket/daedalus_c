
#define __STDC_FORMAT_MACROS
#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>
#include "quads.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include <inttypes.h>

#include "core/timer.h"
#include "core/containers.h"
#include "graphics_api/graphics.h"

// TODO: use plugin manager
#if defined(_WIN32)
extern dd_graphics_api_t dd_vulkan;
dd_graphics_api_t* g_graphics_api = &dd_vulkan;
#else
extern dd_graphics_api_t dd_metal;
dd_graphics_api_t* g_graphics_api = &dd_metal;
#endif

#include "metal_api/metal_types.inl"

typedef struct
{
  vector_float4 position;
  vector_float2 texCoord;
} VertexData;

#if 0
void primitiveTest(id <MTLDevice> device) {
  dispatch_semaphore_t inFlightSemaphore = dispatch_semaphore_create(0);
  
  id <MTLCommandQueue> commandQueue = [device newCommandQueue];
  
  const uint32 texture_dimension = 128;
  MTLTextureDescriptor* outputDesc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Uint
                                                                                        width:texture_dimension
                                                                                       height:texture_dimension
                                                                                    mipmapped:NO];
  outputDesc.usage = MTLTextureUsageRenderTarget | MTLTextureUsageShaderRead;
  id <MTLTexture> texture = [device newTextureWithDescriptor:outputDesc];
  
  VertexData vertexData[3] = {
    {{-1,  -1, 0, 1}, {0, 0}},
    {{ 1,  -1, 0, 1}, {1, 0}},
    {{ 0,   1, 0, 1}, {0, 1}}
  };
  uint32 vertex_size = sizeof(vertexData);
  
  
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertexShader",
    .fs_code       = (const uint8*)"fragmentShader",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
    .buffer_range  = vertex_size,
    .output =
    { .color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeUniformBuffer}}};
  
  dd_graphics_material_prototype_t material_prototype = g_graphics_api->material_prototype.create(&device, &mpd);
  
  // loadAssets
  dd_graphics_buffer_t myVB = {0};
  g_graphics_api->buffer.allocate(&device, 1, &vertex_size, &myVB);
  g_graphics_api->buffer.update_data(&device, myVB, vertexData, vertex_size);
  
  // drawInMTKView
  @autoreleasepool {
    //dd_graphics_commandbuffer_t cmd = g_graphics_api->queue.get_command_buffer(queue);
    id <MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];
    commandBuffer.label = @"MyCommand";
    
    __block dispatch_semaphore_t block_sema = inFlightSemaphore;
    [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> buffer) {
      dispatch_semaphore_signal(block_sema);
    }];
    
    MTLRenderPassDescriptor* renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
    renderPassDescriptor.colorAttachments[0].texture = texture;
    renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(1, 2, 3, 4);
    renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
    renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
    
    id <MTLRenderCommandEncoder> renderEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    if (!renderEncoder)
    {
      NSLog(@"No renderEncoder");
      abort();
    }
    /*
     struct metal_commandbuffer_t* cmd_metal = (struct metal_commandbuffer_t*)cmd.ptr;
     cmd_metal->render_encoder = renderEncoder;
     */
    
    struct metal_commandbuffer_t cmd_queue = {commandBuffer, renderEncoder};
    dd_graphics_commandbuffer_t cmd = (dd_graphics_commandbuffer_t){&cmd_queue};
    [renderEncoder pushDebugGroup:@"DrawBox"];
    
    g_graphics_api->commandbuffer.bind_material_prototype(cmd, material_prototype);
    
    //    [renderEncoder setRenderPipelineState:pipelineState];
    //[renderEncoder setVertexBuffer:vertexBuffer offset:0 atIndex:0];
    
    dd_graphics_material_descriptor_t md = {
      .buffers = {
        {.buffer = myVB,
          .offset =0,
          .range = sizeof(vertexData)
        }
      }
    };
    g_graphics_api->commandbuffer.bind_buffers_and_textures(cmd, NULL, &md);
    
    g_graphics_api->commandbuffer.set_viewport(cmd, 0,0, 64,64, 0.0, 1.0);
    [renderEncoder drawPrimitives:MTLPrimitiveTypeLineStrip vertexStart:0 vertexCount:3];
    g_graphics_api->commandbuffer.set_viewport(cmd, 64,0, 64,64, 0.0, 1.0);
    g_graphics_api->commandbuffer.draw(cmd, 3, 1);
    
    g_graphics_api->commandbuffer.set_viewport(cmd, 0,64, 64,64, 0.0, 1.0);
    g_graphics_api->commandbuffer.draw(cmd, 6, 1);
    
    g_graphics_api->commandbuffer.set_viewport(cmd, 64,64, 64,64, 0.0, 1.0);
    [renderEncoder drawPrimitives:MTLPrimitiveTypePoint vertexStart:0 vertexCount:6];
    
    
    [renderEncoder popDebugGroup];
    [renderEncoder endEncoding];
    [commandBuffer commit];
    
    dispatch_semaphore_wait(inFlightSemaphore, DISPATCH_TIME_FOREVER);
    
    uint8 *pixels = (uint8*)malloc(texture_dimension*texture_dimension * 4);
    memset(pixels, 0, texture_dimension*texture_dimension*4);
    [texture getBytes:pixels
          bytesPerRow:sizeof(uint8) * 4 * outputDesc.width
           fromRegion:MTLRegionMake2D(0, 0, outputDesc.width, outputDesc.height)
          mipmapLevel:0];
    
    stbi_write_png("test_draw_types.png", (int)outputDesc.width, (int)outputDesc.height, 4, pixels, 0);
    free(pixels);
  }
}

void quadsTest() {
  dd_graphics_device_t api_device = g_graphics_api->create_default_device();
  dd_graphics_queue_t queue = g_graphics_api->device.get_queue(api_device, QueueTypeGraphics);
  
  uint32 texture_dimension = 128;
  dd_graphics_texture_descriptor_t tc = {
    .width        = texture_dimension,
    .height       = texture_dimension,
    .format       = R8G8B8A8,
    .storage_mode = DEVICE_LOCAL,
    .usage        = TEXTURE_USAGE_RENDER_TARGET | TEXTURE_USAGE_TRANSFER_SOURCE};
  dd_graphics_texture_t color = g_graphics_api->texture.create(api_device, &tc);
  
  
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"quadsVertexShader",
    .fs_code       = (const uint8*)"quadsFragmentShader",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
    .buffer_range  = 200,
    .output =
    { .color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeUniformBuffer},
      {.type = BindingTypeUniformBuffer},
      {.type = BindingTypeUniformBuffer}}};
  
  dd_graphics_material_prototype_t material_prototype = g_graphics_api->material_prototype.create(api_device, &mpd);
  
  uint8_t* data = {0};  /* array */
  struct QuadInfo* quads = {0}; /* array */
  
  {
    uint32 offset = dd_time_microseconds() % 100;
    struct QuadInfo qi = {{10+(int16)offset,10,32,16}, 0, (unsigned int)array_count(data)};
    array_push(quads, qi);
    array_push(data,255);array_push(data,0);array_push(data,0);array_push(data,255);
  }
  {
    struct QuadInfo qi = {{10,30,32,16}, 0, (unsigned int)array_count(data)};
    array_push(quads, qi);
    array_push(data,255);array_push(data,255);array_push(data,0);array_push(data,255);
  }
  
  const char* s = "Hello World";
  {
    int16_t offset = 10;
    while(*s) {
      struct QuadInfo qi = {{offset,50,5,7}, 1, (unsigned int)*s};
      array_push(quads, qi);
      offset+=(5+1);
      ++s;
    }
  }
  
  dd_graphics_buffer_t myQuads = {0};
  uint32 buffer_size =array_count(quads)*sizeof(struct QuadInfo);
  g_graphics_api->buffer.allocate(api_device, 1, &buffer_size, &myQuads);
  g_graphics_api->buffer.update_data(api_device, myQuads, quads, buffer_size);
  dd_graphics_buffer_t myData = {0};
  buffer_size =array_count(data);
  g_graphics_api->buffer.allocate(api_device, 1, &buffer_size, &myData);
  g_graphics_api->buffer.update_data(api_device, myData, data, buffer_size);
  
  
  dd_graphics_commandbuffer_t cmd = g_graphics_api->queue.get_command_buffer(queue);
  struct metal_commandbuffer_t* cmd_metal = (struct metal_commandbuffer_t*)cmd.ptr;
  
  dd_graphics_render_pass_descriptor_t rpd = {.color = {color}, .width = texture_dimension, .height = texture_dimension};
  
  g_graphics_api->commandbuffer.begin_render_pass(cmd, &rpd);
  
  
  //TODO
  //[renderEncoder pushDebugGroup:@"DrawBox"];
  
  g_graphics_api->commandbuffer.bind_material_prototype(cmd, material_prototype);
  
  float size[2] = {(float)texture_dimension, (float)texture_dimension};
  //TODO
  [cmd_metal->render_encoder setVertexBytes:&size length:sizeof(size) atIndex:0];
  
  dd_graphics_material_descriptor_t md = {
    .buffers = {
      {.offset=0},
      {.buffer = myQuads,
        .offset =0,
        .range = buffer_size
      },
      {.buffer = myData,
        .offset =0,
        .range = buffer_size
      }
    }
  };
  g_graphics_api->commandbuffer.bind_buffers_and_textures(cmd, NULL, &md);
  
  g_graphics_api->commandbuffer.draw(cmd, 6*array_count(quads), 1);
  
  //TODO
  //[renderEncoder popDebugGroup];
  
  g_graphics_api->queue.submit_command_buffer(queue, cmd);
  
  g_graphics_api->device.wait_until_idle(api_device);
  
  uint8* pixels = (uint8*)malloc(texture_dimension * texture_dimension * 4);
  g_graphics_api->texture.get_pixels(api_device, color, 0, pixels);
  
  stbi_write_png("quads.png", (int)texture_dimension, (int)texture_dimension, 4, pixels, 0);
  free(pixels);
}
#endif

void Exec()
{
  // initWithMetalKitView
  //id <MTLDevice> device = MTLCreateSystemDefaultDevice();
  
  //primitiveTest(device);
  //quadsTest();
  
}


int main(int argc, const char * argv[])
{
  @autoreleasepool {
    Exec();
  }
  return 0;
}
