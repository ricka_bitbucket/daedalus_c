#if defined(_WIN32)
  #include <Windows.h>
#endif  // _WIN32

#define _USE_MATH_DEFINES
#include <assert.h>
#include <math.h>
#include <stdio.h>

#include "../vulkan/scene.h"
#include "core/api_registry.h"
#include "core/application.h"
#include "core/containers.h"
#include "core/dd_math.h"
#include "core/ddf.h"
#include "core/memory.h"
#include "core/plugins.h"
#include "core/profiler.h"
#include "core/utils.h"
#include "draw2d/draw_2d.h"
#include "formats/ply_reader/ply_reader.h"
#include "graphics_api/graphics.h"
#include "metis/dd_metis.h"
#include "ui/ui.h"

extern dd_graphics_api_t dd_vulkan;
dd_graphics_api_t* g_graphics_api = &dd_vulkan;

static dd_graphics_device_t g_graphics_device;
static uint8 g_alloc_buffer[2 * 1024];

static struct dd_plugins_o* g_plugins;
dd_allocator* g_application_allocator;

struct dd_ui_api* ui_api                   = NULL;
static struct dd_draw2d_api* g_draw2d_api_ = NULL;
static dd_ui_t g_ui                        = {0};

enum { NUM_CUBE_BUFFERS = 4 };

array(uint32) index_base_visible;

DemoScene g_scene = {0};
struct vktexcube_vs_uniform {
  float mvp[4][4];
};

dd_graphics_buffer_t g_uniform_buffers[NUM_CUBE_BUFFERS + 2];  // VTX and IDX

// Mesh data
dd_graphics_buffer_t g_mesh_buffers[3];
dd_graphics_buffer_binding_t g_mesh_buffer_binding[4];
uint32 g_mesh_index_count;

dd_graphics_material_prototype_t g_material_prototype_regular   = {0};
dd_graphics_material_prototype_t g_material_prototype_clustered = {0};

uint32 g_cluster_count = 0;
struct {
  struct {
    dd_graphics_buffer_t visible_clusters;
    dd_graphics_buffer_t draw_indirect;
  } buffers;
  struct {
    dd_graphics_buffer_binding_t input, output, draw_indirect;
  } bindings;
} g_compute;
dd_graphics_material_prototype_t g_compute_cull = {0};

static uint32 g_width                = 800;
static uint32 g_height               = 800;
static dd_graphics_window_t g_window = {0};
const bool do_clusters               = true;

// const char* model_file_path = "../sample_data/armadillo.ply";
// const char* model_file_path = "../sample_data/xyzrgb_dragon.ply";
/*
const char* model_file_path = "../sample_data/bunny/bun_zipper.ply";
vec3 eye                    = {0.0f, 0.125f, 0.0f};
vec3 origin                 = {0, 0.125f, -1};
vec3 up                     = {0.0f, 1.0f, 0.0};
float extents               = 1.5f;
/*/
const char* model_file_path = "../sample_data/lucy.ply";
vec3 eye                    = {700.0f, 1.f, 0.0f};
vec3 origin                 = {700, 0, 0};
vec3 up                     = {0.0f, 0.0f, 1.0f};
float extents               = 1200;
//*/

static void scene_init() {
  g_scene.spin_angle = 0.0f;
  /*mat4x4_perspective(g_scene.projection_matrix, (float)degreesToRadians(45.0f), 1.0f, 0.1f,
                      100.0f);//*/
  mat4x4 projection;
  // float extents = 0.1f;
  // float extents = 100;
  mat4x4_ortho(projection, -extents, extents, -extents, extents, -extents, extents);

  // Circumvent the fact that OpenGL NDC is [-1,1] and Vulkan NDC is [0,1] and that Vulkan has
  // y-axis flipped.
  mat4x4 vulkan_fix = {{1, 0, 0, 0}, {0, -1, 0, 0}, {0, 0, 0.5, 0}, {0, 0, 0.5, 1}};
  mat4x4_mul(g_scene.projection_matrix, vulkan_fix, projection);

  mat4x4_look_at(g_scene.view_matrix, eye, origin, up);
  mat4x4_identity(g_scene.model_matrix);
}

void demo_update_data_buffer(dd_graphics_buffer_t buffer) {
  mat4x4 MVP, Model, VP;
  int matrixSize = sizeof(MVP);

  mat4x4_mul(VP, g_scene.projection_matrix, g_scene.view_matrix);
  // mat4x4_identity(VP);
  // VP[1][1] *= -1;  // Flip projection matrix from GL to Vulkan orientation.

  // Rotate around the Y axis
  mat4x4_dup(Model, g_scene.model_matrix);
  mat4x4_rotate(g_scene.model_matrix, Model, up[0], up[1], up[2],
                (float)degreesToRadians(g_scene.spin_angle));  //*/
  mat4x4_mul(MVP, VP, g_scene.model_matrix);

  g_graphics_api->buffer.update_data(g_graphics_device, buffer, 0, (const void*)&MVP[0][0],
                                     matrixSize);
}

const dd_ui_style_t g_theme = {
    .background =
        {
#if defined(FINANCES_SAVE_FILES)
            .color = {250, 250, 250, 255},
#else
            .color = {255, 200, 200, 255},
#endif
            .disabled = {150, 150, 150, 255}},
    .foreground = {.color = {0, 0, 0, 255}, .disabled = {50, 50, 50, 255}},
    .button     = {.color = {14, 99, 156, 255},
               .hover = {16, 120, 188, 255},
               .down  = {255, 92, 145, 255}},
    .status_bar = {.color_neutral = {30, 30, 30, 255},
                   .color_error   = {192, 0, 50, 255},
                   .color_success = {0, 122, 204, 255}}};

void draw_ui(dd_ui_t* ui, ui_rect_t rect) {
  ui_rect_t r_status_bar = rect_split_off_bottom(&rect, 10, 0);

  uint64 te        = dd_time_microseconds();
  static uint64 ts = 0;
  dd_time_microseconds();
  {
    char buf[256] = {0};
    snprintf(buf, countof(buf) - 1, "%i tris, %i clusters, %.2fms ",
             (uint32)g_mesh_index_count / 3, (int)g_cluster_count, ((float)(te - ts)) / 1000.f);
    ui_api->widget.label(&g_ui, r_status_bar, (color_rgba8_t){255, 255, 255, 255},
                         TextAlignmentLeft, stringview(buf));
  }
  ts = te;
}

enum { NUM_FRAMES_DELAY = 3 };

struct {
  dd_graphics_material_prototype_t material_prototype;
  dd_graphics_buffer_t quad_instance_buffer[NUM_FRAMES_DELAY];
  dd_graphics_buffer_t quad_buffer[NUM_FRAMES_DELAY];
  dd_graphics_buffer_t uniform_screen_size;
} g_ui_render_data = {0};

const uint32 quad_buffer_byte_count          = 1000000;
const uint32 quad_instance_buffer_byte_count = 1000000;

void send_ui_buffers_to_device(uint32 index, const dd_draw2d_t* draw_data) {
  uint32 bytes_count =
      (uint32)(sizeof(*draw_data->primitive_buffer) * array_count(draw_data->primitive_buffer));
  assert(bytes_count <= quad_instance_buffer_byte_count);
  g_graphics_api->buffer.update_data(g_graphics_device,
                                     g_ui_render_data.quad_instance_buffer[index], 0,
                                     (const void*)draw_data->primitive_buffer, bytes_count);

  uint32 quads_byte_count = (uint32)(sizeof(*draw_data->quads) * array_count(draw_data->quads));
  assert(quads_byte_count <= quad_buffer_byte_count);
  g_graphics_api->buffer.update_data(g_graphics_device, g_ui_render_data.quad_buffer[index], 0,
                                     (const void*)draw_data->quads, quads_byte_count);
}

void ui_init() {
  {
    for (unsigned i = 0; i < 3; ++i) {
      g_graphics_api->buffer.allocate(g_graphics_device, 1, &quad_instance_buffer_byte_count,
                                      g_ui_render_data.quad_instance_buffer + i);
      g_graphics_api->buffer.allocate(g_graphics_device, 1, &quad_buffer_byte_count,
                                      g_ui_render_data.quad_buffer + i);
    }

    uint32 screen_size[2] = {g_width, g_height};
    uint32 bytes_count    = sizeof(screen_size);
    g_graphics_api->buffer.allocate(g_graphics_device, 1, &bytes_count,
                                    &g_ui_render_data.uniform_screen_size);
    g_graphics_api->buffer.update_data(g_graphics_device, g_ui_render_data.uniform_screen_size, 0,
                                       (const void*)screen_size, sizeof(screen_size));

    dd_stringview_t exe_path = dd_application.executable_folder();

    char file_path[512];
#if defined(_WIN32)
    file_path[0] = 0;
    strcat(file_path, exe_path.string);
    strcat(file_path, "../../../source/examples/ui/vulkan/ui.vs.spv");
    uint8* vs_code = read_file(file_path, g_application_allocator);
    file_path[0]   = 0;
    strcat(file_path, exe_path.string);
    strcat(file_path, "../../../source/examples/ui/vulkan/ui.fs.spv");
    uint8* fs_code = read_file(file_path, g_application_allocator);
    dd_graphics_material_prototype_descriptor_t mpd = {
      .vs_code       = (const uint8*)vs_code,
      .fs_code       = (const uint8*)fs_code,
      .vs_byte_count = (uint32)array_count(vs_code),
      .fs_byte_count = (uint32)array_count(fs_code),
#else
    dd_graphics_material_prototype_descriptor_t mpd = {
        .vs_code = (const uint8*)"quadsVertexShader",
        .fs_code = (const uint8*)"quadsFragmentShader",
        .vs_byte_count = 0,
        .fs_byte_count = 0,
#endif
      .output =
          {// TODO: link formats with view
           .color_format = B8G8R8A8,
           .depth_format = DEPTH_U16_NORMALIZED},
      .bindings = {{.type = BindingTypeStorageBuffer},
                   {.type = BindingTypeStorageBuffer},
                   {.type = BindingTypeUniformBuffer}}
    };
    g_ui_render_data.material_prototype =
        g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

#if defined(_WIN32)
    array_free(vs_code, g_application_allocator);
    array_free(fs_code, g_application_allocator);
#endif
  }
}

void render_ui(dd_graphics_commandbuffer_t draw_commandbuffer, uint32 frame_index,
               const dd_draw2d_t* draw_data) {
  g_graphics_api->commandbuffer.bind_material_prototype(draw_commandbuffer,
                                                        g_ui_render_data.material_prototype);
  dd_graphics_material_descriptor_t md = (dd_graphics_material_descriptor_t){
      .buffers  = {{.buffer = g_ui_render_data.quad_instance_buffer[frame_index],
                   .offset = 0,
                   .range  = quad_instance_buffer_byte_count},
                  {.buffer = g_ui_render_data.quad_buffer[frame_index],
                   .offset = 0,
                   .range  = quad_buffer_byte_count},
                  {.buffer = g_ui_render_data.uniform_screen_size,
                   .offset = 0,
                   .range  = sizeof(float) * 2}},
      .textures = {0}};

  g_graphics_api->commandbuffer.bind_buffers_and_textures(
      draw_commandbuffer, g_ui_render_data.material_prototype, &md);

  g_graphics_api->commandbuffer.draw(draw_commandbuffer, (uint32)array_count(draw_data->quads), 1);
}

// Example of using compute in vulkan:
// https://github.com/Glavnokoman/vulkan-compute-example/blob/master/src/example_filter.cpp

static uint32 frame_index = 0;

void compute_frame(dd_graphics_commandbuffer_t compute_commandbuffer) {
  frame_index = (frame_index + 1) % 3;

  if (g_cluster_count == 0) return;

  // Command buffer already ready to be filled with commands
  // g_graphics_api->commandbuffer.begin_compute(compute_commandbuffer);
  // g_graphics_api->commandbuffer.end_compute(compute_commandbuffer);
  g_graphics_api->commandbuffer.bind_material_prototype(compute_commandbuffer, g_compute_cull);

  dd_graphics_material_descriptor_t md = {.buffers = {g_compute.bindings.input,
                                                      g_compute.bindings.output,
                                                      g_compute.bindings.draw_indirect}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(compute_commandbuffer, g_compute_cull,
                                                          &md);

  g_graphics_api->commandbuffer.push_constants(compute_commandbuffer, sizeof(g_cluster_count),
                                               &g_cluster_count);

  const uint32 group_count = align_up(g_cluster_count, 64) / 64;
  g_graphics_api->commandbuffer.dispatch(compute_commandbuffer, group_count, 1, 1);
}

void non_vulkan_draw_frame(dd_graphics_commandbuffer_t draw_commandbuffer,
                           const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor) {
  float viewport_dimension;
  float x, y;
  x = y = 0;
  if (g_width < g_height) {
    viewport_dimension = (float)g_width;
    y                  = (g_height - g_width) / 2.0f;
  } else {
    viewport_dimension = (float)g_height;
    x                  = (g_width - g_height) / 2.0f;
  }

  dd_ui_event_t event = ui_api->interaction.pop_event();

  ui_rect_t rect = {.x = x, .y = y, .width = viewport_dimension, .height = viewport_dimension};
  do {
    // handle_event(event);

    ui_api->begin(&g_ui, &g_theme, event);
    draw_ui(&g_ui, rect);

    event = ui_api->interaction.pop_event();
  } while (event.event_type);
  const dd_draw2d_t* draw_data = ui_api->end(&g_ui);
  send_ui_buffers_to_device(frame_index, draw_data);

  g_graphics_api->commandbuffer.begin_render_pass(draw_commandbuffer, in_render_pass_descriptor);

  g_graphics_api->commandbuffer.set_viewport(draw_commandbuffer, x, y, viewport_dimension,
                                             viewport_dimension, 0, 1);
  static int buffer_index = 0;
  buffer_index            = (buffer_index + 1) % NUM_CUBE_BUFFERS;
  if (g_mesh_index_count > 0) {
    if (do_clusters) {
      g_graphics_api->commandbuffer.bind_material_prototype(draw_commandbuffer,
                                                            g_material_prototype_clustered);
      dd_graphics_material_descriptor_t md = {
          .buffers = {{.buffer = g_uniform_buffers[buffer_index],
                       .offset = 0,
                       .range  = sizeof(struct vktexcube_vs_uniform)},
                      g_compute.bindings.output,
                      g_mesh_buffer_binding[1],
                      g_mesh_buffer_binding[2],
                      g_mesh_buffer_binding[3]}};

      g_graphics_api->commandbuffer.bind_buffers_and_textures(draw_commandbuffer,
                                                              g_material_prototype_clustered, &md);
      /*g_graphics_api->commandbuffer.draw(draw_commandbuffer, g_mesh_index_count, 1);  //*/
      g_graphics_api->commandbuffer.draw_indirect(draw_commandbuffer,
                                                  g_compute.buffers.draw_indirect, 0, 1,
                                                  0);  //*/
    } else {
      g_graphics_api->commandbuffer.bind_material_prototype(draw_commandbuffer,
                                                            g_material_prototype_regular);
      dd_graphics_material_descriptor_t md = {
          .buffers = {{.buffer = g_uniform_buffers[buffer_index],
                       .offset = 0,
                       .range  = sizeof(struct vktexcube_vs_uniform)},
                      g_mesh_buffer_binding[0],
                      g_mesh_buffer_binding[1],
                      g_mesh_buffer_binding[2]}};

      g_graphics_api->commandbuffer.bind_buffers_and_textures(draw_commandbuffer,
                                                              g_material_prototype_regular, &md);
      g_graphics_api->commandbuffer.draw(draw_commandbuffer, g_mesh_index_count, 1);
    }

    render_ui(draw_commandbuffer, frame_index, draw_data);

    demo_update_data_buffer(g_uniform_buffers[buffer_index]);
    g_graphics_api->commandbuffer.end_render_pass(draw_commandbuffer);
  }
}

uint8* sample_grid(struct dd_allocator* allocator) {
  const size_t grid_count     = 4000;
  const size_t vertex_count   = grid_count * grid_count;
  const size_t triangle_count = (grid_count - 1) * (grid_count - 1) * 2;

  srand(0);

  array(uint8) out   = NULL;
  dd_mesh_t out_mesh = {
      .index_count   = (uint32)(triangle_count * 3),
      .vertex_count  = (uint32)(vertex_count),
      .index_offset  = (uint32)(sizeof(dd_mesh_t)),
      .vertex_offset = (uint32)(sizeof(dd_mesh_t) + triangle_count * 3 * sizeof(uint32))};
  array_append(out, &out_mesh, sizeof(out_mesh), allocator);
  size_t total_byte_count = out_mesh.vertex_offset + vertex_count * sizeof(float4);
  array_resize(out, total_byte_count, allocator);

  float4* out_vertexes = (float4*)(out + out_mesh.vertex_offset);
  uint32* out_indices  = (uint32*)(out + out_mesh.index_offset);

  float delta = 2.f / (grid_count - 1);
  for (size_t x = 0; x < grid_count; ++x) {
    for (size_t y = 0; y < grid_count; ++y) {
      float x_coord     = -1.f + delta * (float)x;
      float height      = 1.f - (float)fabs(x_coord);
      *(out_vertexes++) = (float4){x_coord, -1.f + delta * (float)y, height * height, 1.f};
    }
  }

  for (size_t y = 0; y < (grid_count - 1); ++y) {
    for (size_t x = 0; x < (grid_count - 1); ++x) {
      *(out_indices++) = (uint32)((y + 0) * grid_count + (x + 0));
      *(out_indices++) = (uint32)((y + 0) * grid_count + (x + 1));
      *(out_indices++) = (uint32)((y + 1) * grid_count + (x + 0));
      *(out_indices++) = (uint32)((y + 1) * grid_count + (x + 0));
      *(out_indices++) = (uint32)((y + 0) * grid_count + (x + 1));
      *(out_indices++) = (uint32)((y + 1) * grid_count + (x + 1));
    }
  }

  return out;
}

static void calculate_vertex_normals(const float4* vertexes, const uint32* indexes,
                              const size_t vertex_count, const size_t triangle_count,
                              float3* vertex_normals) {
  memset(vertex_normals, 0, sizeof(float3) * vertex_count);

  for (size_t it = 0; it < triangle_count; ++it) {
    const uint32* tris_indexes = indexes + it * 3;
    float4 vertex_a            = vertexes[tris_indexes[0]];
    float4 vertex_b            = vertexes[tris_indexes[1]];
    float4 vertex_c            = vertexes[tris_indexes[2]];
    float3 dir_a  = {vertex_b.x - vertex_a.x, vertex_b.y - vertex_a.y, vertex_b.z - vertex_a.z};
    float3 dir_b  = {vertex_c.x - vertex_a.x, vertex_c.y - vertex_a.y, vertex_c.z - vertex_a.z};
    dir_a         = float3_normalize(dir_a);
    dir_b         = float3_normalize(dir_b);
    float3 normal = float3_normalize(float3_cross(dir_a, dir_b));
    vertex_normals[tris_indexes[0]] = float3_add(vertex_normals[tris_indexes[0]], normal);
    vertex_normals[tris_indexes[1]] = float3_add(vertex_normals[tris_indexes[1]], normal);
    vertex_normals[tris_indexes[2]] = float3_add(vertex_normals[tris_indexes[2]], normal);
  }

  for (size_t iv = 0; iv < vertex_count; ++iv) {
    float3 v = float3_normalize(vertex_normals[iv]);
    /*assert(!isnan(v.x));
    assert(!isnan(v.y));
    assert(!isnan(v.z));*/
    vertex_normals[iv] = v;
  }
}

void non_vulkan_init() {
  dd_api_registry.init(g_application_allocator);
  g_plugins = dd_plugins_api.create(g_application_allocator);

#if defined(_WIN32)
  dd_stringview_t app_folder = dd_application.executable_folder();
  array(char) path           = dd_strcat(app_folder, stringview("*.dll"), g_application_allocator);
  const char* plugin_paths[] = {path};
#else
  const char* plugin_paths[] = {"/Users/rickappleton/dev/daedalus_c/build/x64/Debug/"};
#endif
  dd_plugins_api.load_plugins(g_plugins, plugin_paths, countof(plugin_paths));

  ui_api        = (struct dd_ui_api*)dd_api_registry.get_api("ui", 1);
  g_draw2d_api_ = (struct dd_draw2d_api*)dd_api_registry.get_api("draw2d", 1);
#if defined(__APPLE__)
  chdir("/Users/rickappleton/dev/daedalus_c/build/x64/Debug/");
#endif

  g_graphics_device = g_graphics_api->get_device();

  const struct dd_ply_reader_api* ply_api =
      (struct dd_ply_reader_api*)dd_api_registry.get_api("ply_reader", 1);
  const struct dd_metis_api* metis_api = (struct dd_metis_api*)dd_api_registry.get_api("metis", 1);
  assert(ply_api);
  assert(metis_api);
  (void)ply_api;

  ui_api->init(&g_ui, g_draw2d_api_);
  g_ui.draw_data.allocator = g_application_allocator;

  ui_init();
  scene_init();

  // Vulkan has been inited sufficiently to initialize data
  {
    array(uint8) mesh_data =
        ply_api->load_from_file_path(stringview(model_file_path), g_application_allocator);  //*/
    /*array(uint8) mesh_data = read_file("temp_result_slowprocessing.bin",
                                       g_application_allocator);  //*/
    /*array(uint8) mesh_data = sample_grid(g_application_allocator);  //*/

    if (mesh_data) {
      if (do_clusters) {
        array(uint8) ddf_data =
            metis_api->clusterize_mesh(mesh_data, g_application_allocator);  //*/
        /*dd_cluster_mesh_t* cluster_mesh = (dd_cluster_mesh_t*)mesh_data;//*/

        const ddf_block_descriptor_t cluster_bd =
            ddf_api.read.block_descriptor_with_label(ddf_data, "clusters");
        const ddf_block_descriptor_t indexes_bd =
            ddf_api.read.block_descriptor_with_label(ddf_data, "indexes");
        const ddf_block_descriptor_t positions_bd =
            ddf_api.read.block_descriptor_with_label(ddf_data, "positions");
        const ddf_block_descriptor_t normals_bd =
            ddf_api.read.block_descriptor_with_label(ddf_data, "normals");

        const uint32 data_start_offset = min(cluster_bd.data_offset, indexes_bd.data_offset);

        {
          uint32 buffer_byte_count[] = {(uint32)(array_count(ddf_data) - data_start_offset)};
          g_graphics_api->buffer.allocate(g_graphics_device, countof(buffer_byte_count),
                                          buffer_byte_count, g_mesh_buffers);

          g_mesh_buffer_binding[0] =
              (dd_graphics_buffer_binding_t){.buffer = g_mesh_buffers[0],
                                             .offset = cluster_bd.data_offset - data_start_offset,
                                             .range  = cluster_bd.data_byte_count};
          g_mesh_buffer_binding[1] =
              (dd_graphics_buffer_binding_t){.buffer = g_mesh_buffers[0],
                                             .offset = indexes_bd.data_offset - data_start_offset,
                                             .range  = indexes_bd.data_byte_count};
          g_mesh_buffer_binding[2] = (dd_graphics_buffer_binding_t){
              .buffer = g_mesh_buffers[0],
              .offset = positions_bd.data_offset - data_start_offset,
              .range  = positions_bd.data_byte_count};
          g_mesh_buffer_binding[3] =
              (dd_graphics_buffer_binding_t){.buffer = g_mesh_buffers[0],
                                             .offset = normals_bd.data_offset - data_start_offset,
                                             .range  = normals_bd.data_byte_count};
          g_cluster_count = cluster_bd.data_byte_count / sizeof(dd_mesh_cluster_data_t);

          if (0) {
            const dd_mesh_cluster_data_t* clusters =
                (dd_mesh_cluster_data_t*)(ddf_data + cluster_bd.data_offset);
            for (size_t i = 0; i < g_cluster_count; i++) {
              if (clusters[i].padding_A > 8850) {
                array_push(index_base_visible, clusters[i].base_vertex, g_application_allocator);
              }
            }
            dd_devprintf("Should be %i clusters visible, with %i indices\n",
                         (int)array_count(index_base_visible),
                         3 * cluster_size * (int)array_count(index_base_visible));
          }
          // TODO(Rick): support views to do this simpler
          g_graphics_api->buffer.update_data(g_graphics_device, g_mesh_buffers[0], 0,
                                             ddf_data + data_start_offset, buffer_byte_count[0]);
          g_mesh_index_count = 3 * indexes_bd.data_byte_count / sizeof(uint32);
        }

        {
          // Assume all clusters are visible
          dd_draw_indirect_command_t visible_cluster_count = {.vertex_count   = 0 * 3,
                                                              .instance_count = 0};
          uint32 buffer_byte_count[]                       = {cluster_bd.data_byte_count,
                                        sizeof(visible_cluster_count) * 3};
          dd_graphics_buffer_t buffers[countof(buffer_byte_count)];
          g_graphics_api->buffer.allocate(g_graphics_device, countof(buffer_byte_count),
                                          buffer_byte_count, buffers);
          g_compute.buffers.visible_clusters = buffers[0];
          g_compute.buffers.draw_indirect    = buffers[1];
          /*g_graphics_api->buffer.update_data(g_graphics_device, g_compute.buffers.draw_indirect,
             0, &visible_cluster_count, sizeof(visible_cluster_count));*/

          g_compute.bindings.input = g_mesh_buffer_binding[0];  // Default clusters
          g_compute.bindings.output =
              (dd_graphics_buffer_binding_t){.buffer = g_compute.buffers.visible_clusters,
                                             .offset = 0,
                                             .range  = buffer_byte_count[0]};
          g_compute.bindings.draw_indirect =
              (dd_graphics_buffer_binding_t){.buffer = g_compute.buffers.draw_indirect,
                                             .offset = 0,
                                             .range  = sizeof(visible_cluster_count)};

          const uint8* code = read_file("../source/examples/metis/data/cluster_cull_compute.inc",
                                        tmp_malloc_allocator);
          dd_graphics_compute_prototype_descriptor_t cpd = {
              .compute_shader_data = (const uint8*)code,
              .byte_count          = (uint32)array_count(code),
              .bindings            = {{BindingTypeStorageBuffer},
                           {BindingTypeStorageBuffer},
                           {BindingTypeStorageBuffer},
                           {.type  = BindingTypePushConstant,
                            .props = {.push_constant = {.offset = 0, .range = sizeof(uint32)}}}}};
          g_compute_cull = g_graphics_api->compute_prototype.create(g_graphics_device, &cpd);
        }

        array_free(ddf_data, g_application_allocator);
      } else {
        dd_mesh_t* mesh = (dd_mesh_t*)mesh_data;

        uint32 buffer_byte_count[3] = {(uint32)mesh->index_count * sizeof(uint32),
                                       (uint32)mesh->vertex_count * sizeof(float4),
                                       (uint32)mesh->vertex_count * sizeof(float)*3};
        g_graphics_api->buffer.allocate(g_graphics_device, countof(buffer_byte_count), buffer_byte_count, g_mesh_buffers);

        array(float) normals = NULL;
        array_resize(normals, 3 * mesh->vertex_count, g_application_allocator);

        calculate_vertex_normals((float4*)(mesh_data + mesh->vertex_offset),
                                 (uint32*)(mesh_data+mesh->index_offset), mesh->vertex_count,
                                 mesh->index_count / 3, (float3*)normals);

        g_graphics_api->buffer.update_data(g_graphics_device, g_mesh_buffers[0], 0,
                                           (const void*)(mesh_data + mesh->index_offset),
                                           buffer_byte_count[0]);
        g_graphics_api->buffer.update_data(g_graphics_device, g_mesh_buffers[1], 0,
                                           (const void*)(mesh_data + mesh->vertex_offset),
                                           buffer_byte_count[1]);
        g_graphics_api->buffer.update_data(g_graphics_device, g_mesh_buffers[2], 0,
                                           (const void*)(normals),
                                           buffer_byte_count[2]);

        array_free(normals, g_application_allocator);

        g_mesh_buffer_binding[0] = (dd_graphics_buffer_binding_t){
            .buffer = g_mesh_buffers[0], .offset = 0, .range = buffer_byte_count[0]};
        g_mesh_buffer_binding[1] = (dd_graphics_buffer_binding_t){
            .buffer = g_mesh_buffers[1], .offset = 0, .range = buffer_byte_count[1]};
        g_mesh_buffer_binding[2] = (dd_graphics_buffer_binding_t){
            .buffer = g_mesh_buffers[2], .offset = 0, .range = buffer_byte_count[2]};
        g_mesh_index_count = mesh->index_count;
        array_free(mesh_data, g_application_allocator);
      }
    }
  }

  uint32 bytes_count[NUM_CUBE_BUFFERS];
  for (unsigned i = 0; i < NUM_CUBE_BUFFERS; i++) {
    bytes_count[i] = sizeof(struct vktexcube_vs_uniform);
  }
  g_graphics_api->buffer.allocate(g_graphics_device, NUM_CUBE_BUFFERS, bytes_count,
                                  g_uniform_buffers);

  {
    const uint8* vs_code =
        read_file("../source/examples/metis/data/regular_vert.inc", tmp_malloc_allocator);
    const uint8* fs_code =
        read_file("../source/examples/metis/data/regular_frag.inc", tmp_malloc_allocator);
    dd_graphics_material_prototype_descriptor_t mpd = {
        .vs_code       = (const uint8*)vs_code,
        .fs_code       = (const uint8*)fs_code,
        .vs_byte_count = (uint32)array_count(vs_code),
        .fs_byte_count = (uint32)array_count(fs_code),
        .bindings      = {{BindingTypeUniformBuffer},
                     {BindingTypeStorageBuffer},
                     {BindingTypeStorageBuffer},
                     {BindingTypeStorageBuffer}}};
    g_material_prototype_regular =
        g_graphics_api->material_prototype.create(g_graphics_device, &mpd);
  }
  {
    const uint8* vs_code =
        read_file("../source/examples/metis/data/metis_cluster_vert.inc", tmp_malloc_allocator);
    const uint8* fs_code =
        read_file("../source/examples/metis/data/metis_cluster_frag.inc", tmp_malloc_allocator);
    dd_graphics_material_prototype_descriptor_t mpd = {
        .vs_code       = (const uint8*)vs_code,
        .fs_code       = (const uint8*)fs_code,
        .vs_byte_count = (uint32)array_count(vs_code),
        .fs_byte_count = (uint32)array_count(fs_code),
        .bindings      = {{BindingTypeUniformBuffer},
                     {BindingTypeStorageBuffer},
                     {BindingTypeStorageBuffer},
                     {BindingTypeStorageBuffer},
                     {BindingTypeStorageBuffer}}};
    g_material_prototype_clustered =
        g_graphics_api->material_prototype.create(g_graphics_device, &mpd);
  }
}

void non_vulkan_deinit() {
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype_regular);
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype_clustered);
  g_graphics_api->compute_prototype.destroy(g_graphics_device, g_compute_cull);
  g_graphics_api->buffer.destroy(g_graphics_device, NUM_CUBE_BUFFERS, g_uniform_buffers);
}

extern int dd_vk_main_loop(dd_graphics_window_t* window, struct dd_allocator* allocator);
extern void dd_helper_set_functions(void (*init)(), void (*deinit)(),
                                    dd_graphics_api_helper_draw_callback draw_cb,
                                    dd_graphics_api_helper_compute_callback compute_cb);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
  dd_prof_thread_init("main");
  dd_allocator* allocator = (dd_allocator*)g_alloc_buffer;
  dd_heap_allocator_init(allocator, sizeof(g_alloc_buffer), "startup_allocator");

  dd_allocator* main_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(main_allocator, "main_allocator");

  g_application_allocator = main_allocator;

  dd_helper_set_functions(non_vulkan_init, non_vulkan_deinit, non_vulkan_draw_frame,
                          compute_frame);

  const char* name = "Cluster tests";
  g_window         = dd_create_window(hInstance, name, 100, 100, g_width, g_height, 200, 200,
                              DD_WINDOW_FLAGS_ADJUST_SIZE, main_allocator);

  int result = dd_vk_main_loop(&g_window, main_allocator);

  return result;
}
