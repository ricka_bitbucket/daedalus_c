#version 430
#extension GL_ARB_compute_shader : enable

#define CLUSTER_SIZE 128

struct dd_mesh_cluster_data_t {
  uint base_vertex;
  uint base_index;
  int padding_A;
  int padding_B;
};

struct dd_draw_indirect_command_t {
  uint vertex_count;
  uint instance_count;
  uint vertex_offset;
  uint instance_offset;
};

layout(local_size_x = 64) in;
layout(local_size_y = 1) in;
layout(local_size_z = 1) in;

layout(push_constant) uniform
    Parameters {  // specify push constants. on cpp side its layout is fixed at PipelineLayout, and
                  // values are provided via vk::CommandBuffer::pushConstants()
  uint cluster_count;
}
params;

layout(std430, binding = 0) readonly buffer all_clusters_t {
  dd_mesh_cluster_data_t all_clusters[];
};
layout(std430, binding = 1) writeonly buffer visible_clusters_t {
  dd_mesh_cluster_data_t visible_clusters[];
};
layout(std430, binding = 2) volatile buffer visible_cluster_count_t {
  dd_draw_indirect_command_t cluster_draw_indirect;
};

void main() {
  // Reset the count.
  if (gl_GlobalInvocationID.x == 0) {
    cluster_draw_indirect.vertex_count    = CLUSTER_SIZE * 3;
    cluster_draw_indirect.instance_count  = 0;
    cluster_draw_indirect.vertex_offset   = 0;
    cluster_draw_indirect.instance_offset = 0;
  }

  memoryBarrierBuffer();

  const uint id           = gl_GlobalInvocationID.x;
    const bool keep_cluster = ((id < params.cluster_count));
  // const bool keep_cluster = ((id < params.cluster_count) && (id % 2 == 0));
  // const bool keep_cluster = ((id < params.cluster_count) && (all_clusters[id].padding_A > 0));

  uint output_cluster_index = 0;
  if (keep_cluster) {
    output_cluster_index = atomicAdd(cluster_draw_indirect.instance_count, 1);
  }

  if (keep_cluster) {
    visible_clusters[output_cluster_index].base_vertex = all_clusters[id].base_vertex;
    visible_clusters[output_cluster_index].base_index  = all_clusters[id].base_index;
  }
}
