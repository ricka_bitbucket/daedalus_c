#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
layout(std140, binding = 0) uniform buf { mat4 MVP; }
ubuf;

#define CLUSTER_SIZE 128

struct VTX {
  vec4 position;
};

struct dd_mesh_cluster_data_t {
  uint base_vertex;
  uint base_index;
  uint padding_A;
  uint padding_B;
};

layout(std430, binding = 1) readonly buffer clusters { dd_mesh_cluster_data_t cluster_datas[]; };
layout(std140, binding = 2) readonly buffer indexes { uvec4 idx[]; };
layout(std140, binding = 3) readonly buffer meshv { VTX vtx[]; };
layout(std430, binding = 4) readonly buffer normals_t { float normals[]; };

struct debug {
  uint patch_id;
  uint vertex_id;
};
layout(location = 0) out vec4 color;
layout(location = 1) out vec3 normal;
layout(location = 2) out debug id;

uint get_vertex_id(uint index) {
  uint vector_idx          = index / 8;
  uint vector_element      = (index % 8) / 2;
  uint vector_element_part = (index % 8) % 2;
  uint shift               = 16 * vector_element_part;
  uint mask                = 0xFFFF << shift;
  return (idx[vector_idx][vector_element] & mask) >> shift;
}

uvec3 unpack_uint10x3(uint in_value) {
  uvec3 unpacked;
  unpacked.x = (in_value >> 0) & 0x3FF;
  unpacked.y = (in_value >> 10) & 0x3FF;
  unpacked.z = (in_value >> 20) & 0x3FF;
  return unpacked;
}

uint get_cluster_index(uint index) { /*return index / (CLUSTER_SIZE * 3);*/
  return gl_InstanceIndex;
}
uint get_vertex_in_cluster_index(uint index) {
  uint cluster_vertex_count = CLUSTER_SIZE * 3;
  return index % cluster_vertex_count;
}

uint get_cluster_vertex_id(uint cluster_index, uint vertex_in_cluster_index) {
  uint base_vertex              = cluster_datas[cluster_index].base_vertex;
  uint triangle_index           = vertex_in_cluster_index / 3;
  uint vertex_in_triangle_index = vertex_in_cluster_index % 3;

  uint global_triangle_index   = triangle_index + cluster_datas[cluster_index].base_index;
  uint packed_triangle_indexes = idx[global_triangle_index / 4][global_triangle_index % 4];
  uvec3 triangle_indexes       = unpack_uint10x3(packed_triangle_indexes);

  return base_vertex + triangle_indexes[vertex_in_triangle_index];
}

void main() {
  uint cluster_idx             = get_cluster_index(gl_VertexIndex);
  uint vertex_in_cluster_index = get_vertex_in_cluster_index(gl_VertexIndex);

  uint vtx_idx = get_cluster_vertex_id(cluster_idx, vertex_in_cluster_index);
  color.rgb    = vec3(1, 0, 0);
  color.a      = vtx_idx;
  gl_Position  = ubuf.MVP * vec4(vtx[vtx_idx].position.rgb, 1.0);

  normal =
      vec3(normals[3 * vtx_idx + 0], normals[3 * vtx_idx + 1], normals[3 * vtx_idx + 2]);
  // normal = vec4(normals[vtx_idx].x+0.1,normals[vtx_idx].y,normals[vtx_idx].z,0);
  id.patch_id  = cluster_datas[cluster_idx].base_vertex;
  id.vertex_id = vtx_idx;
}
