#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(std140, binding = 0) uniform buf { mat4 MVP; }
ubuf;

struct VTX {
  vec4 position;
};

layout(std140, binding = 1) readonly buffer meshi { uvec4 idx[]; };
layout(std140, binding = 2) readonly buffer meshv { VTX vtx[]; };
layout(std430, binding = 3) readonly buffer normals_t { float normals[]; };

struct debug {
  uint patch_id;
  uint vertex_id;
};
layout(location = 0) out vec4 color;
layout(location = 1) out vec3 normal;
layout(location = 2) out debug id;

uint get_vertex_id(uint index) {
  uint vector_idx     = index / 4;
  uint vector_element = index % 4;
  return idx[vector_idx][vector_element];
}

void main() {
  uint vtx_idx = get_vertex_id(gl_VertexIndex);  // idx[gl_VertexIndex].x;
  color.rgb    = vec3(1, 0, 0);                  // vtx[vtx_idx].attr;
  color.a      = vtx_idx;
  gl_Position  = ubuf.MVP * vec4(vtx[vtx_idx].position.xyz, 1.0);

  normal =
      vec3(normals[3 * vtx_idx + 0], normals[3 * vtx_idx + 1], normals[3 * vtx_idx + 2]);
  
  // For now this is a debug aid.
  id.patch_id  = gl_VertexIndex / (128 * 3);
  id.vertex_id = vtx_idx;
}