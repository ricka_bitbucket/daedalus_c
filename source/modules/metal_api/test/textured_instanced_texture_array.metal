#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]];
  float2 texcoord;
  uint texture_index;
} Vertex;

vertex Vertex textured_instanced_texture_array_vs(uint vid [[vertex_id]], uint iid [[instance_id]],
                                                  constant float2* positions [[buffer(0)]],
                                                  constant float2* texcoords [[buffer(1)]]
                                                  )
{
  Vertex out;
  
  out.texture_index = iid;
  out.position = float4(positions[iid * 6 + vid], 0.5, 1);
  
  out.texcoord = texcoords[vid];
  
  return out;
}

constexpr sampler textureSampler (mag_filter::linear,
                                  min_filter::linear);

fragment uint4 textured_instanced_texture_array_fs(Vertex in [[stage_in]], texture2d_array<ushort> colorTexture [[ texture(2) ]])
{
  const ushort4 tex = colorTexture.sample(textureSampler, in.texcoord, in.texture_index);
  uint4 out = uint4(tex);
  return out;
}


