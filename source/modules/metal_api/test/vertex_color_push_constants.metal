#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]];
  float4 color;
} Vertex;

vertex Vertex vertex_color_push_constants_vs(uint vid [[vertex_id]],
                                        constant float2* positions [[buffer(0)]],
                                        constant float* data [[buffer(1)]]
                                        )
{
  Vertex out;
  
  float4 vert = float4(positions[vid], 0.5, 1);
  out.position = float4(data[0], data[1],0,0)+vert;
  
  out.color = float4(data[2], data[3], data[4], 1);
  
  return out;
}

fragment uint4 vertex_color_push_constants_fs(Vertex in [[stage_in]])
{
  uint4 out = uint4(256*in.color);
  return out;
}


