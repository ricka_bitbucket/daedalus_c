#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]];
  float4 color;
} Vertex;

vertex Vertex vertex_color_instanced_vs(uint vid [[vertex_id]], uint iid [[instance_id]],
                                        constant float2* positions [[buffer(0)]],
                                        constant float* colors [[buffer(1)]]
                                        )
{
  Vertex out;
  
  out.position = float4(positions[iid * 6 + vid], 0.5, 1);
  
  out.color = float4(colors[iid * 3 + 0], colors[iid * 3 + 1], colors[iid * 3 + 2], 1);
  
  return out;
}

fragment uint4 vertex_color_instanced_fs(Vertex in [[stage_in]])
{
  uint4 out = uint4(256*in.color);
  return out;
}


