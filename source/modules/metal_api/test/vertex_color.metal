#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]];
  float4 color;
} Vertex;

vertex Vertex vertex_color_vs(uint vid [[vertex_id]],
                     constant float2* positions [[buffer(0)]],
                     constant float* colors [[buffer(1)]]
                     )
{
  Vertex out;
  
  out.position = float4(positions[vid], 0.5, 1);
  
  out.color = float4(colors[vid * 3 + 0], colors[vid * 3 + 1], colors[vid * 3 + 2], 1);
  
  return out;
}

fragment uint4 vertex_color_fs(Vertex in [[stage_in]])
{
  uint4 out = uint4(256*in.color);
  return out;
}

