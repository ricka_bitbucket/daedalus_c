#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]]; 
  float2 texcoord;
  uint texture_index;
} Vertex;

vertex Vertex textured_array_of_textures_dynamic_indexed_vs(uint vid [[vertex_id]], uint iid [[instance_id]],
                                                  constant float2* positions [[buffer(0)]],
                                                  constant float2* texcoords [[buffer(1)]]
                                                  )
{
  Vertex out;
  
  out.texture_index = iid % 128;
  out.position = float4(positions[iid * 6 + vid], 0.5, 1);
  
  out.texcoord = texcoords[vid];
  
  return out;
}

constexpr sampler textureSampler (mag_filter::linear,
                                  min_filter::linear);

typedef struct FragmentShaderArguments {
  texture2d<ushort> colorTextures[128];
} FragmentShaderArguments;

//https://stackoverflow.com/questions/52849269/how-to-use-an-array-of-textures-in-metal
fragment uint4 textured_array_of_textures_dynamic_indexed_fs(Vertex in [[stage_in]], device FragmentShaderArguments& frag_args [[buffer(2)]])
{
  const ushort4 tex = frag_args.colorTextures[in.texture_index].sample(textureSampler, in.texcoord);
  uint4 out = uint4(tex);
  return out;
}


