#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]]; 
  float2 texcoord;
  uint texture_index;
} Vertex;

vertex Vertex textured_instanced_array_of_textures_vs(uint vid [[vertex_id]],
                                                  constant float2* positions [[buffer(0)]],
                                                  constant float2* texcoords [[buffer(1)]],
                                                  constant float* data [[buffer(3)]]
                                         )
{
  Vertex out;
  
  float4 vert = float4(positions[vid], 0.5, 1);
  out.position = float4(data[0], data[1],0,0)+vert;
  out.texture_index = (*(constant int*)(data+2))%128;
  
  out.texcoord = texcoords[vid];
  
  return out;
}

constexpr sampler textureSampler (mag_filter::linear,
                                  min_filter::linear);

typedef struct FragmentShaderArguments {
  texture2d<ushort> colorTextures[128];
} FragmentShaderArguments;

//https://stackoverflow.com/questions/52849269/how-to-use-an-array-of-textures-in-metal
fragment uint4 textured_instanced_array_of_textures_fs(Vertex in [[stage_in]], device FragmentShaderArguments& frag_args [[buffer(2)]])
{
  const ushort4 tex = frag_args.colorTextures[in.texture_index].sample(textureSampler, in.texcoord);
  uint4 out = uint4(tex);
  return out;
}


