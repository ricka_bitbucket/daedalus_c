#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]];
  float2 texcoord;
} Vertex;

vertex Vertex textured_vs(uint vid [[vertex_id]],
                     constant float2* positions [[buffer(0)]],
                     constant float2* texcoords [[buffer(1)]]
                     )
{
  Vertex out;
  
  out.position = float4(positions[vid], 0.5, 1);
  
  out.texcoord = texcoords[vid];
  
  return out;
}

constexpr sampler textureSampler (mag_filter::linear,
                                  min_filter::linear, mip_filter::nearest);

fragment uint4 textured_fs(Vertex in [[stage_in]], texture2d<ushort> colorTexture [[ texture(2) ]])
{
  const ushort4 tex = colorTexture.sample(textureSampler, in.texcoord);
  uint4 out = uint4(tex);
  return out;
}

