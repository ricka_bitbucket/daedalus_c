#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct  {
  float4 position [[position]];
  float2 texcoord;
} Vertex;

vertex Vertex textured_push_constants_vs(uint vid [[vertex_id]],
                                         constant float2* positions [[buffer(0)]],
                                         constant float2* texcoords [[buffer(1)]],
                                         constant float* data [[buffer(2)]]
                                         )
{
  Vertex out;
  
  float4 vert = float4(positions[vid], 0.5, 1);
  out.position = float4(data[0], data[1],0,0)+vert;
  
  out.texcoord = texcoords[vid];
  
  return out;
}

constexpr sampler textureSampler (mag_filter::linear,
                                  min_filter::linear);

fragment uint4 textured_push_constants_fs(Vertex in [[stage_in]], texture2d<ushort> colorTexture [[ texture(3) ]])
{
  const ushort4 tex = colorTexture.sample(textureSampler, in.texcoord);
  uint4 out = uint4(tex);
  return out;
}


