#include <metal_stdlib>
#include <simd/simd.h>
using namespace metal;

typedef struct {
  float4 position;
  float4 color;
} in_vertex;

typedef struct  {
  float4 position [[position]];
  float4 color;
} Vertex;

vertex Vertex vertex_color_interleaved_vs(uint vid [[vertex_id]],
                     constant in_vertex* data [[buffer(0)]]
                     )
{
  Vertex out;
  
  out.position = data[vid].position;
  
  out.color = data[vid].color;
  
  return out;
}

fragment uint4 vertex_color_interleaved_fs(Vertex in [[stage_in]])
{
  uint4 out = uint4(256*in.color);
  return out;
}

