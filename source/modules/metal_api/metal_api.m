/*
MTLQueue
  keep for the life time of application, so usually only need 1
MTLCommandBuffer
  A container that stores encoded commands for the GPU to execute. One per frame minimal, as can't touch while in flight. Are executed in order of submission to MTLQueue
MTLCommandEncoder
  An encoder that writes GPU commands into a MTLCommandBuffer.
      - enqueue this to set it's order in the queue (optional)
      - commit this to enqueue (if not done yet) and to mark ready for execution

To simplify the common API, the implementation assumes a command buffer is only ever encoded with either graphics or compute, but never both.

 */
#import <Metal/Metal.h>

#include "core/containers.h"
#include "core/utils.h"
#include "graphics_api/graphics.h"
#include "core/dd_math.h"
#include "metal_types.inl"




#pragma mark Local functions

static MTLPixelFormat metal_pixel_format(uint32 format) {
  if (format < PIXEL_FORMAT_MAX) {
    MTLPixelFormat out_formats[] = {
      MTLPixelFormatInvalid,
      // These values are normalized when read in shader
      MTLPixelFormatRGBA8Uint,
      MTLPixelFormatBGRA8Unorm,
      
      // Compressed formats
      MTLPixelFormatBC1_RGBA,
      
      // Depth formats
      MTLPixelFormatDepth16Unorm};
    static_assert(countof(out_formats) == PIXEL_FORMAT_MAX,  "Incorrect number of pixel formats");
    return out_formats[format];
  } else {
    return (MTLPixelFormat)
    format;  // Assume the format is some hardware supported one that we don't know about
  }
}

#pragma mark Device

static id<MTLDevice> g_mtl_device = NULL;

void dd_mtl_set_device(id<MTLDevice> in_device) {
  g_mtl_device = in_device;
}


dd_graphics_device_t dd_mtl_create_default_device() {
  id <MTLDevice> device = MTLCreateSystemDefaultDevice();
  g_mtl_device = device;
  printf("Device props\n        argumentBuffersSupport: Tier%u\n", 1u+(uint32)device.argumentBuffersSupport);
  return &g_mtl_device;
}

static dd_graphics_device_t dd_mtl_create_device() {
  //TODO: actually create the device here
  return &g_mtl_device;
}

static id<MTLCommandBuffer> g_last_commandbuffer_submitted = nil;
void dd_mtl_device_wait_until_idle(dd_graphics_device_t in_device) {
  if( g_last_commandbuffer_submitted ) {
    [g_last_commandbuffer_submitted waitUntilCompleted];
    if( @available(macOS 10.15, *)) {
      //printf("Last command buffer took %f ms\n", (float)(g_last_commandbuffer_submitted.GPUEndTime - g_last_commandbuffer_submitted.GPUStartTime)*1000);
    }
  }
}
    
dd_graphics_queue_t dd_mtl_device_get_queue(dd_graphics_device_t in_device, EQueueType in_type) {
  id<MTLDevice> device = (id<MTLDevice>)(*(void**)in_device);
  struct metal_queue_t* queue = (struct metal_queue_t*)malloc(sizeof(struct metal_queue_t));
  queue->queue = [device newCommandQueue];
  queue->device = device;
  return (dd_graphics_queue_t){queue};
}

#pragma mark Queue

dd_graphics_commandbuffer_t dd_mtl_queue_get_command_buffer(dd_graphics_queue_t in_queue) {
  struct metal_queue_t* queue = (struct metal_queue_t*)in_queue.ptr;
  struct metal_commandbuffer_t* cb = (struct metal_commandbuffer_t*)malloc(sizeof(struct metal_commandbuffer_t));
  id<MTLCommandQueue> mtl_queue = queue->queue;
  cb->cmd_buffer = [mtl_queue commandBuffer];
  cb->device = queue->device;
  return (dd_graphics_commandbuffer_t){cb};
}

void dd_mtl_queue_submit_command_buffer(dd_graphics_queue_t in_queue,
                                  dd_graphics_commandbuffer_t in_buffer) {
  (void)in_queue;
  struct metal_commandbuffer_t* cb = (struct metal_commandbuffer_t*)in_buffer.ptr;
  
  [cb->cmd_buffer commit];

  g_last_commandbuffer_submitted = cb->cmd_buffer;
}

#pragma mark Buffers

static void dd_mtl_allocate_buffers(dd_graphics_device_t in_device, uint32_t count, const uint32* in_sizes,
                                    dd_graphics_buffer_t* out_buffers) {
  
  id<MTLDevice> device = (id<MTLDevice>)(*(void**)in_device);
  for (uint32_t i = 0; i < count; i++) {
    id<MTLBuffer> buffer = [device newBufferWithLength:in_sizes[i] options:MTLResourceStorageModeShared];
    out_buffers[i] = buffer;
  }
}

void dd_mtl_buffer_destroy(dd_graphics_device_t device, uint32 count, dd_graphics_buffer_t* in_buffers) {
  //TODO implement
}


static void dd_mtl_update_buffer_data(dd_graphics_device_t device, dd_graphics_buffer_t in_buffer, uint32 in_offset_byte_count,
                                      const void* in_data, uint32 in_num_bytes) {
  id<MTLBuffer> mtl_buffer = (id<MTLBuffer>)in_buffer;
  memcpy((void*)((intptr_t)mtl_buffer.contents + in_offset_byte_count), in_data, in_num_bytes);
}

#pragma mark Materials

static dd_graphics_material_prototype_t dd_mtl_create_material_prototype(
                                                                         dd_graphics_device_t in_device,
                                                                         const dd_graphics_material_prototype_descriptor_t* in_material_descriptor) {
  id<MTLDevice> device = (id<MTLDevice>)(*(void**)in_device);
  id<MTLLibrary> defaultLibrary = [device newDefaultLibrary];
  
  MTLRenderPipelineDescriptor* psd = [[MTLRenderPipelineDescriptor alloc] init];
  psd.sampleCount = 1;
  psd.vertexFunction = [defaultLibrary newFunctionWithName:[NSString stringWithUTF8String:(const char*)in_material_descriptor->vs_code]];
  psd.fragmentFunction = [defaultLibrary newFunctionWithName:[NSString stringWithUTF8String:(const char*)in_material_descriptor->fs_code]];;
  psd.colorAttachments[0].pixelFormat = metal_pixel_format(in_material_descriptor->output.color_format);
  psd.depthAttachmentPixelFormat = metal_pixel_format(in_material_descriptor->output.depth_format);
  
  NSError *error = NULL;
  id <MTLRenderPipelineState> ps = [device newRenderPipelineStateWithDescriptor:psd error:&error];
  if (!ps || error)
  {
    fprintf(stderr, "Failed to create pipeline state, error %s\n", error.localizedDescription.UTF8String);
    exit(1);
  }
  
  MTLDepthStencilDescriptor *depthStateDesc = [[MTLDepthStencilDescriptor alloc] init];
  depthStateDesc = [[MTLDepthStencilDescriptor alloc] init];
  depthStateDesc.depthCompareFunction = MTLCompareFunctionAlways;
  depthStateDesc.depthWriteEnabled = NO;
  id<MTLDepthStencilState> depthStencil = [device newDepthStencilStateWithDescriptor:depthStateDesc];
  
  struct metal_material_prototype_t* mp = (struct metal_material_prototype_t*)malloc(sizeof(struct metal_material_prototype_t));
  mp->ps = ps;
  mp->depthStencil = depthStencil;
  memcpy(mp->bindings, in_material_descriptor->bindings, sizeof(mp->bindings));
  mp->fragment_function = psd.fragmentFunction;
  mp->topology = in_material_descriptor->topology;
  
  return mp;
}
void dd_mtl_material_prototype_destroy(dd_graphics_device_t in_device,
                dd_graphics_material_prototype_t in_material_prototype) {
  //TODO
}

#pragma mark Textures

dd_graphics_texture_t dd_mtl_texture_create(dd_graphics_device_t in_device,
                                    const dd_graphics_texture_descriptor_t* in_texture_descriptor) {
  id<MTLDevice> device = (id<MTLDevice>)(*(void**)in_device);
  MTLTextureDescriptor* outputDesc = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:metal_pixel_format(in_texture_descriptor->format)
                                                                                        width:in_texture_descriptor->width
                                                                                       height:in_texture_descriptor->height
                                                                                    mipmapped:(in_texture_descriptor->mip_level_count!=1) ? YES : NO];
  outputDesc.usage = 0;
  outputDesc.textureType = (in_texture_descriptor->layers>1) ? MTLTextureType2DArray : MTLTextureType2D;
  outputDesc.arrayLength = max(1u, in_texture_descriptor->layers);
  if( in_texture_descriptor->usage & TEXTURE_USAGE_RENDER_TARGET ) {
    outputDesc.usage |= MTLTextureUsageRenderTarget;
  }
  if( in_texture_descriptor->usage & TEXTURE_USAGE_RENDER_TARGET ) {
    outputDesc.usage |= TEXTURE_USAGE_SHADER_READ;
  }
  id <MTLTexture> texture = [device newTextureWithDescriptor:outputDesc];

  struct metal_texture_t* to = (struct metal_texture_t*)malloc(sizeof(struct metal_texture_t));
  to->texture = texture;
  return to;
}

void dd_mtl_texture_destroy(dd_graphics_device_t in_device, uint32 in_count, dd_graphics_texture_t *in_textures ) {
  //TODO: need to figure out how ARC is enabled/disabled and how to actually free a texture
}

void dd_mtl_texture_set_pixels(dd_graphics_device_t device, dd_graphics_texture_t in_texture,
                               dd_rect_t in_rect,uint32 in_layer_index,  uint32 in_mip_map_level, uint32 byte_count, uint8* in_data) {
  (void)byte_count;
  struct metal_texture_t* texture = (struct metal_texture_t*)in_texture;
  
  [texture->texture replaceRegion:MTLRegionMake2D(in_rect.x, in_rect.y, in_rect.width, in_rect.height) mipmapLevel:in_mip_map_level
                            slice:in_layer_index withBytes:in_data bytesPerRow:sizeof(uint8) * 4 * in_rect.width bytesPerImage:0];
}

void dd_mtl_texture_get_pixels(dd_graphics_device_t device, dd_graphics_texture_t in_texture,
                               dd_rect_t in_rect, uint32 in_mip_map_level, uint8* out_pixels) {
  struct metal_texture_t* texture = (struct metal_texture_t*)in_texture;
  
  [texture->texture getBytes:out_pixels
                 bytesPerRow:sizeof(uint8) * 4 * in_rect.width
                  fromRegion:MTLRegionMake2D(in_rect.x, in_rect.y, in_rect.width, in_rect.height)
                 mipmapLevel:in_mip_map_level];
}

#pragma mark Render state
static void dd_mtl_set_viewport(dd_graphics_commandbuffer_t in_commandbuffer, float origin_x, float origin_y,
                                float width, float height, float z_near, float z_far) {
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  MTLViewport viewports[] = {
    {origin_x, origin_y, width,height, z_near, z_far}
  };
  [re setViewports:viewports count:1];
  
  //TODO: access this elsewhere
  [re setFrontFacingWinding:MTLWindingCounterClockwise];
  [re setCullMode:MTLCullModeNone];
}


static void dd_mtl_bind_material_prototype(dd_graphics_commandbuffer_t in_commandbuffer,
                                           dd_graphics_material_prototype_t in_material_prototype) {
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  struct metal_material_prototype_t* mp = ((struct metal_material_prototype_t*)in_material_prototype);
  
  id<MTLRenderPipelineState> ps = mp->ps;
  id<MTLDepthStencilState> ds = mp->depthStencil;
  
  [re setRenderPipelineState:ps];
  [re setDepthStencilState:ds];
  
  cmd_queue->bound_material_prototype = mp;
}


static void dd_mtl_bind_buffers_and_textures(dd_graphics_commandbuffer_t in_commandbuffer,
                                             dd_graphics_material_prototype_t in_material_prototype,
                                             const dd_graphics_material_descriptor_t* in_material_descriptor) {
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  struct metal_material_prototype_t* material_prototype = (struct metal_material_prototype_t*)in_material_prototype;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  
  for( unsigned i=0; i<MaterialPrototypeMaxBindingCount; i++ ) {
    if( in_material_descriptor->buffers[i].buffer ) {
      [re setVertexBuffer:in_material_descriptor->buffers[i].buffer
                   offset:in_material_descriptor->buffers[i].offset
                  atIndex:i];
    }
    else if( in_material_descriptor->textures[i]) {
      struct metal_texture_t** textures = (struct metal_texture_t**)(in_material_descriptor->textures[i]);
      uint32 texture_count = material_prototype->bindings[i].props.array.count;
      if(texture_count>1){
        id<MTLBuffer> argument_buffer = [cmd_queue->device newBufferWithLength:128*16 options:MTLResourceStorageModeShared];
        id <MTLArgumentEncoder> argumentEncoder = [material_prototype->fragment_function newArgumentEncoderWithBufferIndex:i];
        [argumentEncoder setArgumentBuffer:argument_buffer offset:0];

        for( uint32 ti=0; ti<texture_count; ti++) {
          [argumentEncoder setTexture:textures[ti]->texture atIndex:ti];
          [re useResource:textures[ti]->texture usage:MTLResourceUsageSample];
        }
        [re setFragmentBuffer:argument_buffer
                                  offset:0
                                 atIndex:i];
      } else {
        id<MTLTexture> t = textures[0]->texture;
        [re setFragmentTexture:t
                       atIndex:i];
      }
    }
  }
  
  if( in_material_descriptor->index_buffer.buffer ) {
    cmd_queue->bound_index_buffer = in_material_descriptor->index_buffer.buffer;
    cmd_queue->index_type = (in_material_descriptor->index_type==IndexTypeUint16) ? MTLIndexTypeUInt16 : MTLIndexTypeUInt32;
  }
}

static void dd_vk_begin_render_pass(dd_graphics_commandbuffer_t in_commandbuffer,
                                    const dd_graphics_render_pass_descriptor_t* in_render_pass_begin) {
  struct metal_commandbuffer_t* commandbuffer = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  
  if( in_render_pass_begin->mtl_render_pass_descriptor ) {
    MTLRenderPassDescriptor* renderPassDescriptor = in_render_pass_begin->mtl_render_pass_descriptor;
    for( uint32 i=0; i<GRAPHICS_MAX_COLOR_ATTACHMENTS; i++) {
      if( renderPassDescriptor.colorAttachments[i].texture) {
        renderPassDescriptor.colorAttachments[i].clearColor = MTLClearColorMake(in_render_pass_begin->clear_color[0],in_render_pass_begin->clear_color[1],in_render_pass_begin->clear_color[2],in_render_pass_begin->clear_color[3]);
        renderPassDescriptor.colorAttachments[i].storeAction = MTLStoreActionStore;
        renderPassDescriptor.colorAttachments[i].loadAction = MTLLoadActionClear;
      }
    }
    id <MTLRenderCommandEncoder> renderEncoder =
    [commandbuffer->cmd_buffer renderCommandEncoderWithDescriptor:in_render_pass_begin->mtl_render_pass_descriptor];
    commandbuffer->render_encoder = renderEncoder;
  } else {
    MTLRenderPassDescriptor* renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
    for( uint32 i=0; i<GRAPHICS_MAX_COLOR_ATTACHMENTS; i++) {
      struct metal_texture_t* texture = (struct metal_texture_t*)in_render_pass_begin->color[i];
      if( texture ) {
        renderPassDescriptor.colorAttachments[i].texture = texture->texture;
        renderPassDescriptor.colorAttachments[i].clearColor = MTLClearColorMake(in_render_pass_begin->clear_color[0],in_render_pass_begin->clear_color[1],in_render_pass_begin->clear_color[2],in_render_pass_begin->clear_color[3]);
        renderPassDescriptor.colorAttachments[i].storeAction = MTLStoreActionStore;
        renderPassDescriptor.colorAttachments[i].loadAction = MTLLoadActionClear;
      }
    }

    id <MTLRenderCommandEncoder> renderEncoder = [commandbuffer->cmd_buffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    if (!renderEncoder)
    {
      fprintf(stderr, "No renderEncoder\n");
      exit(1);
    }
    
    commandbuffer->render_encoder = renderEncoder;
  }
}


static void dd_vk_end_render_pass(dd_graphics_commandbuffer_t in_commandbuffer) {
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  [re endEncoding];
}

void dd_mtl_commandbuffer_push_constants(dd_graphics_commandbuffer_t in_commandbuffer,
                                        uint32 bytes_count, void* values) {
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  struct metal_material_prototype_t* mp = cmd_queue->bound_material_prototype;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  
  for(uint32 i=0; i<MaterialPrototypeMaxBindingCount; i++) {
    if( mp->bindings[i].type==BindingTypePushConstant) {
      [re setVertexBytes:values length:bytes_count atIndex:i];
    }
  }
}

static MTLPrimitiveType metal_primitive_type(ETopologyType type) {
  const MTLPrimitiveType types[] = {MTLPrimitiveTypeTriangle, MTLPrimitiveTypePoint, MTLPrimitiveTypeLine, MTLPrimitiveTypeLineStrip, MTLPrimitiveTypeTriangleStrip};
  return types[(int)type];
}

static void dd_mtl_draw_with_offset(dd_graphics_commandbuffer_t in_commandbuffer, const uint32 in_primitive_count, const uint32 in_primitive_base,
                                    const uint32 in_instance_count, const uint32 in_instance_base) {
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  [re drawPrimitives:metal_primitive_type(cmd_queue->bound_material_prototype->topology) vertexStart:in_primitive_base vertexCount:in_primitive_count instanceCount:in_instance_count baseInstance:in_instance_base];
}
static void dd_mtl_draw(dd_graphics_commandbuffer_t in_commandbuffer, const uint32 in_primitive_count,const uint32 in_instance_count) {
  dd_mtl_draw_with_offset(in_commandbuffer, in_primitive_count, 0, in_instance_count, 0);
}


void dd_mtl_commandbuffer_draw_indexed_with_offsets(dd_graphics_commandbuffer_t in_commandbuffer,
                                                   const uint32 in_index_count,
                                                   const uint32 in_index_base,
                                                   const uint32 in_vertex_base,
                                                   const uint32 in_instance_count,
                                                   const uint32 in_instance_base) {
  
  struct metal_commandbuffer_t* cmd_queue = (struct metal_commandbuffer_t*) in_commandbuffer.ptr;
  id<MTLRenderCommandEncoder> re = cmd_queue->render_encoder;
  [re drawIndexedPrimitives:metal_primitive_type(cmd_queue->bound_material_prototype->topology) indexCount:in_index_count indexType:cmd_queue->index_type indexBuffer:cmd_queue->bound_index_buffer indexBufferOffset:in_index_base instanceCount:in_index_count baseVertex:in_vertex_base baseInstance:in_instance_base];
}

void dd_mtl_commandbuffer_draw_indexed(dd_graphics_commandbuffer_t in_commandbuffer,
                                      const uint32 in_index_count,
                                      const uint32 in_instance_count) {
  dd_mtl_commandbuffer_draw_indexed_with_offsets(in_commandbuffer, in_index_count, 0, 0,
                                                in_instance_count, 0);
}

dd_graphics_api_t dd_metal = {
  .create_default_device   = &dd_mtl_create_default_device,
  .get_device              = &dd_mtl_create_device,

  .device = {
    .get_queue          = &dd_mtl_device_get_queue,
    .wait_until_idle    = &dd_mtl_device_wait_until_idle
  },
  .queue = {
    .get_command_buffer = &dd_mtl_queue_get_command_buffer,
    .submit_command_buffer = &dd_mtl_queue_submit_command_buffer,
  },
  .buffer = {
    .allocate            = &dd_mtl_allocate_buffers,
    .destroy             = &dd_mtl_buffer_destroy,
    .update_data         = &dd_mtl_update_buffer_data,
  },
  
  .texture = {
    .create             = &dd_mtl_texture_create,
    .destroy            = &dd_mtl_texture_destroy,
    .set_pixels         = &dd_mtl_texture_set_pixels,
    .get_pixels         = &dd_mtl_texture_get_pixels,
  },
  .material_prototype = {
    .create  = &dd_mtl_create_material_prototype,
    .destroy = &dd_mtl_material_prototype_destroy,
  },
  .commandbuffer={
    .begin_render_pass          = &dd_vk_begin_render_pass,
    .end_render_pass            = &dd_vk_end_render_pass,
    .set_viewport               = &dd_mtl_set_viewport,
    .bind_material_prototype    = &dd_mtl_bind_material_prototype,
    .bind_buffers_and_textures  = &dd_mtl_bind_buffers_and_textures,
    .push_constants             = &dd_mtl_commandbuffer_push_constants,
    .draw                       = &dd_mtl_draw,
    .draw_with_offsets          = &dd_mtl_draw_with_offset,
    .draw_indexed               = &dd_mtl_commandbuffer_draw_indexed,
    .draw_indexed_with_offsets  = &dd_mtl_commandbuffer_draw_indexed_with_offsets,
  }
  };



