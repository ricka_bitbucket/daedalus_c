#pragma once

#include <Metal/Metal.h>

#pragma mark Structs

struct metal_material_prototype_t {
  id<MTLRenderPipelineState> ps;
  id<MTLDepthStencilState> depthStencil;
  id<MTLFunction> fragment_function;
  dd_graphics_material_prototype_binding_t bindings[MaterialPrototypeMaxBindingCount];
  ETopologyType topology;
};

struct metal_texture_t {
  id<MTLTexture> texture;
};

struct dd_mtl_material_t {
  id<MTLBuffer> uniforms[MaterialPrototypeMaxBindingCount];
};

struct metal_queue_t {
  id<MTLDevice> device;
  id<MTLCommandQueue> queue;
};

struct metal_commandbuffer_t {
  id<MTLDevice> device;
  id<MTLCommandBuffer> cmd_buffer;
  id<MTLRenderCommandEncoder> render_encoder;

  struct metal_material_prototype_t* bound_material_prototype;
  id<MTLBuffer> bound_index_buffer;
  MTLIndexType index_type;
};
