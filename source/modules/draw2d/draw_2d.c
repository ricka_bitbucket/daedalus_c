#include "draw_2d.h"

#include <assert.h>
#include <math.h>
#include <string.h>

#include "core/api_registry.h"
#include "core/containers.h"
#include "core/memory.h"

static_assert(sizeof(struct dd_ui_quad_info_t) == 8 * sizeof(float),
              "dd_ui_quad_info_t isn't expected size");

enum dd_draw2d_primitive_type_t {
  DD_DRAW2D_PRIMITIVE_TRIANGLE       = 0,
  DD_DRAW2D_PRIMITIVE_QUAD           = 1,
  DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH = 2
};

static uint32 encode_vertex_id(enum dd_draw2d_primitive_type_t type, uint32 corner,
                               uint32 primitive_buffer_offset) {
  return (type << 26) | (corner << 24) | primitive_buffer_offset;
}

static void draw2d_rect(dd_draw2d_t* ui, ui_rect_t rect, color_rgba8_t color) {
  uint32 offset = (uint32)array_count(ui->primitive_buffer);

  struct dd_ui_quad_info_t qi = {.rect        = rect,
                                 .data        = *(uint32*)&color,
                                 .data_offset = 0,
                                 .has_clip    = (ui->clip_rect.width > 0)};
  array_append(ui->primitive_buffer, &qi, sizeof(qi) / sizeof(*ui->primitive_buffer),
               ui->allocator);
  if (qi.has_clip) {
    array_append(ui->primitive_buffer, &ui->clip_rect,
                 sizeof(ui->clip_rect) / sizeof(*ui->primitive_buffer), ui->allocator);
  }
  array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_QUAD, 0, offset), ui->allocator);
  array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_QUAD, 2, offset), ui->allocator);
  array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_QUAD, 1, offset), ui->allocator);
  array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_QUAD, 1, offset), ui->allocator);
  array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_QUAD, 2, offset), ui->allocator);
  array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_QUAD, 3, offset), ui->allocator);
}

static void draw2d_rect_stroke(dd_draw2d_t* ui, ui_rect_t rect, float thickness,
                               color_rgba8_t color) {
  ui_rect_t stroke_rect = rect;

  // Left
  stroke_rect.width = thickness;
  draw2d_rect(ui, stroke_rect, color);
  // Right
  stroke_rect.x = rect.x + rect.width - thickness;
  draw2d_rect(ui, stroke_rect, color);
  // Top
  stroke_rect.x      = rect.x + thickness;
  stroke_rect.width  = rect.width - 2 * thickness;
  stroke_rect.height = thickness;
  draw2d_rect(ui, stroke_rect, color);
  // Bottom
  stroke_rect.y = rect.y + rect.height - thickness;
  draw2d_rect(ui, stroke_rect, color);
}

/*
 *    Shared glyph data:
 *
 *    --------------------------------------
 *    float4 clip [if needed]
 *    --------------------------------------
 *    int32 color
 *    int32 padding[3]
 *    --------------------------------------
 */
static void draw2d_text(dd_draw2d_t* ui, ui_rect_t rect, color_rgba8_t color, ETextAligment align,
                        const dd_stringview_t text) {
  if (text.length == 0) return;

  dd_allocator* allocator      = ui->allocator;
  const int16 spacing          = 1;
  const int16 character_width  = 5;
  const int16 character_height = 7;
  float offset                 = rect.x;

  float4* pb          = ui->primitive_buffer;
  ui_rect_t char_rect = {
      .x = rect.x, .y = rect.y, .width = character_width, .height = character_height};

  // Center vertically
  char_rect.y += floorf((rect.height - character_height) / 2);

  const float end_x = rect.x + rect.width - character_width + 1;

  struct dd_ui_quad_info_t qi = {.rect        = char_rect,
                                 .data_offset = (uint32)array_count(pb),
                                 .has_clip    = (ui->clip_rect.width > 0)};

  if (qi.has_clip) {
    array_append(pb, &ui->clip_rect, sizeof(ui->clip_rect) / sizeof(*pb), ui->allocator);
  }
  float4 shared_glyph_data      = {0};
  *(int32*)&(shared_glyph_data) = *(int32*)&color;
  array_push(pb, shared_glyph_data, ui->allocator);

  const uint32 pbo_before_chars = (uint32)array_count(pb);
  uint32 pbo                    = (uint32)array_count(pb);

  size_t char_index = 0;
  for (; char_index < text.length; char_index++ && offset < end_x) {
    qi.rect.x = offset;
    qi.data   = (uint32)text.string[char_index];
    pbo       = (uint32)array_count(pb);
    array_append(pb, &qi, sizeof(qi) / sizeof(*pb), allocator);
    array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH, 0, pbo), allocator);
    array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH, 2, pbo), allocator);
    array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH, 1, pbo), allocator);
    array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH, 1, pbo), allocator);
    array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH, 2, pbo), allocator);
    array_push(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_INTERNAL_GLYPH, 3, pbo), allocator);
    offset += (character_width + spacing);
  }

  float alignment_offset = 0;
  if (align == TextAlignmentCenter) {
    const float total_width = offset - spacing - rect.x;
    if (total_width < rect.width) {
      const float width_left = rect.width - total_width;
      alignment_offset       = floorf(width_left / 2);
    }
  } else if (align == TextAlignmentRight) {
    const float total_width = offset - spacing - rect.x;
    if (total_width < rect.width) {
      alignment_offset = floorf(rect.width - total_width);
    }
  }

  if (alignment_offset != 0) {
    struct dd_ui_quad_info_t* q = (struct dd_ui_quad_info_t*)(pb + pbo_before_chars);
    for (unsigned i = 0; i < char_index; i++, q++) {
      q->rect.x += alignment_offset;
    }
  }

  ui->primitive_buffer = pb;
}

static ui_rect_t draw2d_set_clip_rect(dd_draw2d_t* ui, ui_rect_t clip_rect) {
  ui_rect_t out = ui->clip_rect;
  ui->clip_rect = clip_rect;
  return out;
}

static void draw2d_triangles(dd_draw2d_t* ui, float4* vertices, uint32 vertex_count) {
  uint32 pbo = (uint32)array_count(ui->primitive_buffer);
  array_reserve(ui->primitive_buffer, pbo + vertex_count, ui->allocator);
  array_reserve(ui->quads, array_count(ui->quads) + vertex_count, ui->allocator);
  for (uint32 i = 0; i < vertex_count; i++) {
    array_push_no_alloc(ui->primitive_buffer, vertices[i]);
    array_push_no_alloc(ui->quads, encode_vertex_id(DD_DRAW2D_PRIMITIVE_TRIANGLE, 0, pbo++));
  }
}

const struct dd_draw2d_api dd_draw2d = {.triangles     = &draw2d_triangles,
                                        .rect          = &draw2d_rect,
                                        .rect_stroke   = &draw2d_rect_stroke,
                                        .text          = &draw2d_text,
                                        .set_clip_rect = &draw2d_set_clip_rect};

#if defined(_MSC_VER)
  #define DD_DLL_EXPORT __declspec(dllexport)
#else
  #define DD_DLL_EXPORT
#endif

DD_DLL_EXPORT const struct dd_draw2d_api* get_draw2d_api() { return &dd_draw2d; }
DD_DLL_EXPORT const struct dd_draw2d_api* get_api() { return &dd_draw2d; }

#include "core/plugins.h"
DD_DLL_EXPORT bool plugin_link_apis(const dd_api_registry_t* registry,
                                    struct dd_allocator* allocator) {
  // Don't depend on anything, so simply register draw2d
  registry->register_api("draw2d", 1, &dd_draw2d, sizeof(dd_draw2d));

  return true;
}
