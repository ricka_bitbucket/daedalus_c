#pragma once

#include "core/dd_string.h"
#include "core/types.h"

typedef enum { TextAlignmentLeft = 0, TextAlignmentCenter, TextAlignmentRight } ETextAligment;

typedef struct ui_rect_t {
  float x, y, width, height;
} ui_rect_t;

typedef struct dd_ui_quad_info_t {
  ui_rect_t rect;
  uint32 has_clip : 1;
  uint32 data_offset : 31;
  uint32 data;
  int32 padding[2];
} dd_ui_quad_info_t;

typedef struct dd_draw2d_t {
  float4* primitive_buffer; /* array */
  uint32* quads;            /* array */
  struct dd_allocator* allocator;

  ui_rect_t clip_rect;
} dd_draw2d_t;

/* 0 <= h < 360
 * 0 <= s <= 1.0
 * 0 <= l <= 1.0
 */
static inline void hsl2rgb(float hsl[3], float rgb[3]);

static inline float4 dd_unpack_color(color_rgba8_t color);

/* Functions for modifying ui_rects
 */
static inline ui_rect_t rect_shrink(ui_rect_t in, float pixels_to_shrink);
static inline ui_rect_t rect_enlarge(ui_rect_t in, float pixels_to_enlarge);
static inline ui_rect_t rect_split_off_top(ui_rect_t* r, float height, float margin);
static inline ui_rect_t rect_split_off_bottom(ui_rect_t* r, float height, float margin);
static inline ui_rect_t rect_split_off_left(ui_rect_t* r, float width, float margin);

struct dd_draw2d_api {
  /* Draws triangles
   */
  void (*triangles)(dd_draw2d_t* ui, float4* vertices, uint32 vertex_count);

  /* Draws a filled rectangle with the outermost limits given
   */
  void (*rect)(dd_draw2d_t* ui, ui_rect_t rect, color_rgba8_t color);

  /* Draws a non-filled rectangle with the outermost limits given
   */
  void (*rect_stroke)(dd_draw2d_t* ui, ui_rect_t rect, float thickness, color_rgba8_t color);

  /* Draws text with embedded font
   */
  void (*text)(dd_draw2d_t* ui, ui_rect_t rect, color_rgba8_t color, ETextAligment align,
               const dd_stringview_t text);

  /* Set clipping rect for shapes. Returns currently set rect. Store that so you can set it back
   * when you are done.
   */
  ui_rect_t (*set_clip_rect)(dd_draw2d_t* ui, ui_rect_t clip_rect);
};

// extern const struct dd_draw2d_api dd_draw2d;

// ================================================================================================
// Implementations of static inline functions
// ================================================================================================
static inline float HueToRGB(float v1, float v2, float vH) {
  if (vH < 0) vH += 1;

  if (vH > 1) vH -= 1;

  if ((6 * vH) < 1) return (v1 + (v2 - v1) * 6 * vH);

  if ((2 * vH) < 1) return v2;

  if ((3 * vH) < 2) return (v1 + (v2 - v1) * ((2.0f / 3) - vH) * 6);

  return v1;
}

/* 0 <= h < 360
 * 0 <= s <= 1.0
 * 0 <= l <= 1.0
 */
static inline void hsl2rgb(float hsl[3], float rgb[3]) {
  const float h = hsl[0];
  const float s = hsl[1];
  const float l = hsl[2];
  float r = 0, g = 0, b = 0;

  if (s == 0) {
    r = g = b = l;
  } else {
    float v1, v2;
    float hue = (float)h / 360;

    v2 = (l < 0.5) ? (l * (1 + s)) : ((l + s) - (l * s));
    v1 = 2 * l - v2;

    r = HueToRGB(v1, v2, hue + (1.0f / 3));
    g = HueToRGB(v1, v2, hue);
    b = HueToRGB(v1, v2, hue - (1.0f / 3));
  }

  rgb[0] = r;
  rgb[1] = g;
  rgb[2] = b;
}

static inline float4 dd_unpack_color(color_rgba8_t color) {
  float4 out;
  out.x = ((float)color.r) / 255.f;
  out.y = ((float)color.g) / 255.f;
  out.z = ((float)color.b) / 255.f;
  out.w = ((float)color.a) / 255.f;
  return out;
}

/* Functions for modifying ui_rects
 */
static inline ui_rect_t rect_shrink(ui_rect_t in, float pixels_to_shrink) {
  ui_rect_t out = {in.x + pixels_to_shrink, in.y + pixels_to_shrink,
                   in.width - 2 * pixels_to_shrink, in.height - 2 * pixels_to_shrink};
  out.width     = (out.width > 0) ? out.width : 0;
  out.height    = (out.height > 0) ? out.height : 0;
  return out;
}
static inline ui_rect_t rect_enlarge(ui_rect_t in, float pixels_to_enlarge) {
  ui_rect_t out = {in.x - pixels_to_enlarge, in.y - pixels_to_enlarge,
                   in.width + 2 * pixels_to_enlarge, in.height + 2 * pixels_to_enlarge};
  return out;
}
static inline ui_rect_t rect_split_off_top(ui_rect_t* r, float height, float margin) {
  const float needed_height = height + margin;

  ui_rect_t out = *r;
  out.height    = height;

  r->height -= needed_height;
  r->y += needed_height;

  return out;
}
static inline ui_rect_t rect_split_off_bottom(ui_rect_t* r, float height, float margin) {
  const float needed_height = height + margin;

  ui_rect_t out = *r;
  out.y         = out.y + out.height - height;
  out.height    = height;

  r->height -= needed_height;
  return out;
}
static inline ui_rect_t rect_split_off_left(ui_rect_t* r, float width, float margin) {
  const float needed_width = width + margin;

  ui_rect_t out = *r;
  out.width     = width;

  r->width -= needed_width;
  r->x += needed_width;

  return out;
}
static inline ui_rect_t rect_split_off_right(ui_rect_t* r, float width, float margin) {
  const float needed_width = width + margin;

  ui_rect_t out = *r;
  out.x         = out.x + out.width - width;
  out.width     = width;

  r->width -= needed_width;

  return out;
}

static inline bool rect_contains_point(ui_rect_t in, float point_x, float point_y) {
  const float left   = in.width > 0 ? in.x : in.x + in.width;
  const float right  = in.width > 0 ? in.x + in.width : in.x;
  const float top    = in.height > 0 ? in.y : in.y + in.height;
  const float bottom = in.height > 0 ? in.y + in.height : in.y;
  return ((left <= point_x) && (point_x < right) && (top <= point_y) && (point_y < bottom));
}
