#include <assert.h>
#include <vulkan/vk_sdk_platform.h>
#include <vulkan/vulkan.h>

#include "graphics_api/graphics.h"
#include "vulkan_types.inl"

#define U_ASSERT_ONLY

#define UNUSED

extern void memory_type_from_properties(VkPhysicalDeviceMemoryProperties* memory_properties,
                                        uint32_t typeBits, VkFlags requirements_mask,
                                        uint32_t* typeIndex);

static void dd_vk_set_image_layout(VkCommandBuffer cmd, VkImage image,
                                   VkImageAspectFlags aspectMask, VkImageLayout old_image_layout,
                                   VkImageLayout new_image_layout, VkAccessFlagBits srcAccessMask,
                                   VkPipelineStageFlags src_stages,
                                   VkPipelineStageFlags dest_stages) {
  VkImageMemoryBarrier image_memory_barrier = {.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                                               .pNext = NULL,
                                               .srcAccessMask       = srcAccessMask,
                                               .dstAccessMask       = 0,
                                               .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                               .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                               .oldLayout           = old_image_layout,
                                               .newLayout           = new_image_layout,
                                               .image               = image,
                                               .subresourceRange    = {aspectMask, 0, 1, 0, 1}};

  switch (new_image_layout) {
    case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
      /* Make sure anything that was copying from this image has completed */
      image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      break;

    case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
      image_memory_barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
      break;

    case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
      image_memory_barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
      break;

    case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
      image_memory_barrier.dstAccessMask =
          VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
      break;

    case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
      image_memory_barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
      break;

    case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
      image_memory_barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
      break;

    default:
      image_memory_barrier.dstAccessMask = 0;
      break;
  }

  VkImageMemoryBarrier* pmemory_barrier = &image_memory_barrier;

  vkCmdPipelineBarrier(cmd, src_stages, dest_stages, 0, 0, NULL, 0, NULL, 1, pmemory_barrier);
}

void vulkan_create_texture(vulkan_api_device_t* api_device, VkImageLayout initial_layout,
                           const dd_graphics_texture_descriptor_t* in_texture_descriptor,
                           struct vulkan_texture_t* out_texture) {
  const bool is_depth_format = (in_texture_descriptor->format == DEPTH_U16_NORMALIZED);

  VkFlags required_props =
      (in_texture_descriptor->storage_mode == DEVICE_LOCAL)
          ? VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
          : (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  VkImageTiling tiling = (in_texture_descriptor->usage == TEXTURE_USAGE_RENDER_TARGET)
                             ? VK_IMAGE_TILING_OPTIMAL
                             : VK_IMAGE_TILING_LINEAR;
  if (in_texture_descriptor->format == BC1_RGBA_UNORM) {
    tiling = VK_IMAGE_TILING_OPTIMAL;
  }

  VkImageUsageFlags usage = 0;
  if (is_depth_format) {
    if (in_texture_descriptor->usage & TEXTURE_USAGE_RENDER_TARGET) {
      usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    }
  } else {
    if (in_texture_descriptor->usage & TEXTURE_USAGE_SHADER_READ) {
      usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
    }
    if (in_texture_descriptor->usage & TEXTURE_USAGE_RENDER_TARGET) {
      usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    }
  }

  if (in_texture_descriptor->usage & TEXTURE_USAGE_TRANSFER_SOURCE) {
    usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
  }
  usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;

  const VkFormat tex_format = dd_vk_vulkan_pixel_format(in_texture_descriptor->format);
  out_texture->format       = tex_format;
  int32_t width             = in_texture_descriptor->width;
  int32_t height            = in_texture_descriptor->height;
  assert(in_texture_descriptor->layers > 0);
  assert(in_texture_descriptor->mip_level_count > 0);

  const VkImageCreateInfo ici = {
      .sType         = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
      .pNext         = NULL,
      .imageType     = VK_IMAGE_TYPE_2D,
      .format        = tex_format,
      .extent        = {width, height, 1},
      .mipLevels     = in_texture_descriptor->mip_level_count,
      .arrayLayers   = in_texture_descriptor->layers,
      .samples       = VK_SAMPLE_COUNT_1_BIT,
      .tiling        = tiling,
      .usage         = usage,
      .flags         = 0,
      .sharingMode   = VK_SHARING_MODE_EXCLUSIVE,
      .initialLayout = initial_layout,
  };
  VK_CHECK_RESULT(
      vkCreateImage(api_device->logical_device.device, &ici, NULL, &out_texture->image));

  VkMemoryRequirements mem_reqs;
  vkGetImageMemoryRequirements(api_device->logical_device.device, out_texture->image, &mem_reqs);

  out_texture->mem_alloc.sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  out_texture->mem_alloc.pNext           = NULL;
  out_texture->mem_alloc.allocationSize  = mem_reqs.size;
  out_texture->mem_alloc.memoryTypeIndex = 0;
  memory_type_from_properties(&api_device->memory_properties, mem_reqs.memoryTypeBits,
                              required_props, &out_texture->mem_alloc.memoryTypeIndex);

  VK_CHECK_RESULT(vkAllocateMemory(api_device->logical_device.device, &out_texture->mem_alloc,
                                   &api_device->vk_allocator_callbacks, &out_texture->mem));

  VK_CHECK_RESULT(vkBindImageMemory(api_device->logical_device.device, out_texture->image,
                                    out_texture->mem, 0));

  const bool is_view_needed = (in_texture_descriptor->usage & TEXTURE_USAGE_SHADER_READ) ||
                              (in_texture_descriptor->usage & TEXTURE_USAGE_SHADER_WRITE) ||
                              (in_texture_descriptor->usage & TEXTURE_USAGE_RENDER_TARGET);
  if (is_view_needed) {
    /* create image view if the image is used by shaders */
    const VkImageAspectFlags aspect_mask = (usage & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT)
                                               ? VK_IMAGE_ASPECT_DEPTH_BIT
                                               : VK_IMAGE_ASPECT_COLOR_BIT;
    VkImageViewCreateInfo view           = {
        .sType            = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext            = NULL,
        .image            = VK_NULL_HANDLE,
        .viewType         = (in_texture_descriptor->layers > 1) ? VK_IMAGE_VIEW_TYPE_2D_ARRAY
                                                                          : VK_IMAGE_VIEW_TYPE_2D,
        .format           = tex_format,
        .flags            = 0,
        .image            = out_texture->image,
        .subresourceRange = {.aspectMask     = aspect_mask,
                             .baseMipLevel   = 0,
                             .levelCount     = in_texture_descriptor->mip_level_count,
                             .baseArrayLayer = 0,
                             .layerCount     = in_texture_descriptor->layers}};
    VK_CHECK_RESULT(
        vkCreateImageView(api_device->logical_device.device, &view, NULL, &out_texture->view));
  }

  const bool is_sampler_needed = (in_texture_descriptor->usage & TEXTURE_USAGE_SHADER_READ) ||
                                 (in_texture_descriptor->usage & TEXTURE_USAGE_SHADER_WRITE);
  if (is_sampler_needed) {
    const VkSamplerCreateInfo sampler = {
        .sType                   = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext                   = NULL,
        .magFilter               = VK_FILTER_NEAREST,
        .minFilter               = VK_FILTER_NEAREST,
        .mipmapMode              = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU            = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV            = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW            = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .mipLodBias              = 0.0f,
        .anisotropyEnable        = VK_FALSE,
        .maxAnisotropy           = 1,
        .compareOp               = VK_COMPARE_OP_NEVER,
        .minLod                  = 0.0f,
        .maxLod                  = (float)in_texture_descriptor->mip_level_count,
        .borderColor             = VK_BORDER_COLOR_FLOAT_OPAQUE_WHITE,
        .unnormalizedCoordinates = VK_FALSE,
    };
    VK_CHECK_RESULT(
        vkCreateSampler(api_device->logical_device.device, &sampler, NULL, &out_texture->sampler));
  }
}

#if 0
/*  Removed this function when I added the texture_map/texture_unmap functions as it isn't really needed anymore.
 *  I'm not sure about the transition though, that may be needed, although it doesn't seem like it.
 */
void vulkan_create_texture_upload_data(
    VulkanAPIDevice* device, VkCommandBuffer init_commandbuffer,
    const dd_graphics_texture_descriptor_t* in_texture_descriptor, const uint8* in_texture_data,
    uint32 in_texture_data_length, struct texture_object* out_texture) {
  const VkFormat tex_format = VK_FORMAT_R8G8B8A8_UNORM;

  VkFormatProperties props;
  vkGetPhysicalDeviceFormatProperties(device->physicalDevice, tex_format, &props);

  if (!(props.linearTilingFeatures & VK_FORMAT_FEATURE_SAMPLED_IMAGE_BIT)) {
    /* Can't support VK_FORMAT_R8G8B8A8_UNORM !? */
    assert(!"No support for R8G8B8A8_UNORM as texture image format");
  }

  // Copy the data in
  {
    const VkImageSubresource subres = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .mipLevel   = 0,
        .arrayLayer = 0,
    };
    VkSubresourceLayout layout;
    void* data;

    vkGetImageSubresourceLayout(device->device, out_texture->image, &subres, &layout);
    (void)data;
    /*VK_CHECK_RESULT(vkMapMemory(device->device, out_texture->mem, 0,
                                out_texture->mem_alloc.allocationSize, 0, &data));

    memcpy(data, in_texture_data, in_texture_data_length);

    vkUnmapMemory(device->device, out_texture->mem);*/
  }

  out_texture->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  // Nothing in the pipeline needs to be complete to start, and don't allow fragment
  // shader to run until layout transition completes
  VkImageLayout old_layout = VK_IMAGE_LAYOUT_PREINITIALIZED;
  VkImageLayout new_layout = out_texture->imageLayout;
  dd_vk_set_image_layout(init_commandbuffer, out_texture->image, VK_IMAGE_ASPECT_COLOR_BIT,
                         old_layout, new_layout, 0, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT);
}
#endif