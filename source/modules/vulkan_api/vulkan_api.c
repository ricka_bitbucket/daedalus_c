/*
 * References:
 *  http://gpuopen.com/wp-content/uploads/2016/03/VulkanFastPaths.pdf
 *  https://ourmachinery.com/post/vulkan-descriptor-sets-management/
 *  https://ourmachinery.com/post/vulkan-pipelines-and-render-states/
 *  https://ourmachinery.com/post/vulkan-command-buffer-management/
 *  http://kylehalladay.com/blog/tutorial/vulkan/2018/01/28/Textue-Arrays-Vulkan.html
 *  http://kylehalladay.com/blog/tutorial/vulkan/2017/08/30/Vulkan-Uniform-Buffers-pt2.html

  VkQueue
    submit VkCommandBuffer to queue
  VkCommandBuffer
    add render or compute commands to this
    different VkCommandBuffers can be access from different threads, but this is only safe if they
 come from different VkCommandPools

  To simplify the common API, the implementation assumes a command buffer is only ever encoded
 with either graphics or compute, but never both. If the user requests both types, then multiple
 queues are created, possibly of the same family.

 Debugging Vulkan
 ================
 Need to set environment to enable debug layers: VK_INSTANCE_LAYERS=VK_LAYER_KHRONOS_validation
 */
#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vulkan/vk_sdk_platform.h>
#include <vulkan/vulkan.h>

#include "core/containers.h"
#include "core/dd_math.h"
#include "core/memory.h"
#include "core/profiler.h"
#include "core/profiler.inl"
#include "core/timer.h"
#include "core/utils.h"
#include "graphics_api/graphics.h"
#include "vk_window_surface.h"
#include "vulkan_types.inl"

#define APP_SHORT_NAME "vkcube"

// std::cout << "Fatal : VkResult is \"" << vks::tools::errorString(res) << "\" in " << __FILE__
//          << " at line " << __LINE__ << std::endl;
#define VK_CHECK_RESULT(f)       \
  {                              \
    VkResult res = (f);          \
    if (res != VK_SUCCESS) {     \
      assert(res == VK_SUCCESS); \
    }                            \
  }

#define U_ASSERT_ONLY

#define UNUSED

const char* g_validation_layers[] = {"VK_LAYER_KHRONOS_validation"};
static uint8 g_vulkan_alloc_buffer[2 * 1024 * 1024];

#if defined(NDEBUG)
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

// Globals

struct vulkan_commandbuffers_frame_t* g_frame_data_in_flight = {0}; /* array */
VkDescriptorPool g_active_descriptor_pool                    = 0;

#define ERR_EXIT(err_msg, err_class)             \
  do {                                           \
    MessageBox(NULL, err_msg, err_class, MB_OK); \
    exit(1);                                     \
  } while (0)

static PFN_vkGetDeviceProcAddr g_gdpa = NULL;

#include "demo.h"

struct demo g_demo = {0};
extern struct DefaultVulkanLoop g_loop;

dd_graphics_device_t dd_vulkan_create_device() {
  // TODO: actually create the device here
  return &g_demo.device;
}

// Forward declarations:
static VkDescriptorSet vulkan_prepare_descriptor_set(
    vulkan_api_device_t* device, VkDescriptorPool pool, struct vulkan_pipeline_t* in_out_pipeline,
    const dd_graphics_material_descriptor_t* in_material_descriptor);
extern void vulkan_create_texture(vulkan_api_device_t* device, VkImageLayout initial_layout,
                                  const dd_graphics_texture_descriptor_t* in_texture_descriptor,
                                  struct vulkan_texture_t* out_texture);
static void vulkan_create_descriptor_pools(vulkan_api_device_t* device);
static VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
              VkDebugUtilsMessageTypeFlagsEXT messageType,
              const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) {
  // std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

  // Ignore verbose messages
  // if ((messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) == 0)
  const char* severity = NULL;
  {
    if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) {
      severity = "VERBOSE";
      return VK_FALSE;
    } else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT) {
      severity = "INFO";
    } else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
      severity = "WARNING";
    } else if (messageSeverity & VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT) {
      severity = "ERROR";
    }
  }

  const char* type = NULL;
  {
    if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT) {
      type = "GENERAL";
    } else {
      if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT) {
        type = "VALIDATION";
      }
      if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT) {
        if (messageType & VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT) {
          type = "VALIDATION | PERFORMANCE";
        }
        type = "PERFORMANCE";
      }
    }
  }
  dd_devprintf("%s %s %s\n", severity, type, pCallbackData->pMessage);

  if (messageSeverity > VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
    return VK_FALSE;
  }
  return VK_FALSE;
}
VkResult CreateDebugUtilsMessengerEXT(VkInstance instance,
                                      const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo,
                                      const VkAllocationCallbacks* pAllocator,
                                      VkDebugUtilsMessengerEXT* pDebugMessenger) {
  PFN_vkCreateDebugUtilsMessengerEXT func =
      (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance,
                                                                "vkCreateDebugUtilsMessengerEXT");
  if (func != NULL) {
    return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
  } else {
    return VK_ERROR_EXTENSION_NOT_PRESENT;
  }
}
void DestroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
                                   const VkAllocationCallbacks* pAllocator) {
  PFN_vkDestroyDebugUtilsMessengerEXT func =
      (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(
          instance, "vkDestroyDebugUtilsMessengerEXT");
  if (func != NULL) {
    func(instance, debugMessenger, pAllocator);
  }
}
bool checkValidationLayerSupport(vulkan_api_device_t* api_device) {
  uint32_t available_layer_count;
  vkEnumerateInstanceLayerProperties(&available_layer_count, NULL);

  VkLayerProperties* available_layers = {0}; /* array */
  array_resize(available_layers, available_layer_count, api_device->allocator);
  vkEnumerateInstanceLayerProperties(&available_layer_count, available_layers);

  const uint32 vl_count = countof(g_validation_layers);
  for (unsigned vl_idx = 0; vl_idx < vl_count; vl_idx++) {
    bool layerFound       = false;
    const char* layerName = g_validation_layers[vl_idx];

    for (unsigned al_idx = 0; al_idx < array_count(available_layers); al_idx++) {
      if (strcmp(layerName, available_layers[al_idx].layerName) == 0) {
        layerFound = true;
        break;
      }
    }

    if (!layerFound) {
      return false;
    }
  }

  return true;
}
void vulkan_setup_debug_messenger(vulkan_api_device_t* api_device) {
  if (!enableValidationLayers) return;

  VkDebugUtilsMessengerCreateInfoEXT createInfo = {
      .sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
      .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
      .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
      .pfnUserCallback = debugCallback,
  };

  if (CreateDebugUtilsMessengerEXT(api_device->inst, &createInfo, NULL,
                                   &api_device->debug.messenger) != VK_SUCCESS) {
    ERR_EXIT("failed to set up debug messenger!", "DEBUG error");
  }
}

void memory_type_from_properties(VkPhysicalDeviceMemoryProperties* memory_properties,
                                 uint32_t typeBits, VkFlags requirements_mask,
                                 uint32_t* typeIndex) {
  // Search memtypes to find first index with those properties
  for (uint32_t i = 0; i < VK_MAX_MEMORY_TYPES; i++) {
    if ((typeBits & 1) == 1) {
      // Type is available, does it match user properties?
      if ((memory_properties->memoryTypes[i].propertyFlags & requirements_mask) ==
          requirements_mask) {
        *typeIndex = i;
        return;
      }
    }
    typeBits >>= 1;
  }
  // No memory types matched, failure
  assert(false);
}

/*
 * Currently waits until this buffer is fully executed
 */
static void dd_vk_submit_command_buffer_to_queue(vulkan_api_device_t* device,
                                                 struct vulkan_queue_t* queue,
                                                 VkCommandBuffer init_commandbuffer) {
  VkFence fence;
  VkFenceCreateInfo fence_ci = {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO, .pNext = NULL, .flags = 0};
  VK_CHECK_RESULT(vkCreateFence(device->logical_device.device, &fence_ci, NULL, &fence));

  const VkCommandBuffer cmd_bufs[] = {init_commandbuffer};
  VkSubmitInfo submit_info         = {.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                              .commandBufferCount = 1,
                              .pCommandBuffers    = cmd_bufs};

  VK_CHECK_RESULT(vkQueueSubmit(queue->handle, 1, &submit_info, fence));
  VK_CHECK_RESULT(vkWaitForFences(device->logical_device.device, 1, &fence, VK_TRUE, UINT64_MAX));

  vkDestroyFence(device->logical_device.device, fence, NULL);
}

void vulkan_build_frame_image_ownership_cmd(VkCommandBuffer graphics_to_present_cmd,
                                            uint32 graphics_queue_family_index,
                                            uint32 present_queue_family_index, VkImage image) {}

void dd_vk_commandbuffer_begin_render_pass(
    dd_graphics_commandbuffer_t in_commandbuffer,
    const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;

  const VkRenderPass rp =
      vulkan_prepare_render_pass_with_formats(commandbuffer->device, in_render_pass_descriptor);
  const VkFramebuffer fb = vulkan_prepare_framebuffer_with_formats(commandbuffer->device,
                                                                   in_render_pass_descriptor, rp);
  const uint32 width     = in_render_pass_descriptor->width;
  const uint32 height    = in_render_pass_descriptor->height;

  commandbuffer->render_pass = rp;

  const VkClearValue clear_values[2] = {
      [0] = {.color.float32 = {in_render_pass_descriptor->clear_color[0],
                               in_render_pass_descriptor->clear_color[1],
                               in_render_pass_descriptor->clear_color[2],
                               in_render_pass_descriptor->clear_color[3]}},
      [1] = {.depthStencil = {1.0f, 0}},
  };
  VkRenderPassBeginInfo rp_begin = {.sType       = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                                    .pNext       = NULL,
                                    .renderPass  = rp,
                                    .framebuffer = fb,
                                    .renderArea.offset.x      = 0,
                                    .renderArea.offset.y      = 0,
                                    .renderArea.extent.width  = width,
                                    .renderArea.extent.height = height,
                                    .clearValueCount = in_render_pass_descriptor->depth ? 2 : 1,
                                    .pClearValues    = clear_values};
  vkCmdBeginRenderPass(draw_commandbuffer, &rp_begin, VK_SUBPASS_CONTENTS_INLINE);

  VkRect2D scissor;
  memset(&scissor, 0, sizeof(scissor));
  scissor.extent.width  = width;
  scissor.extent.height = height;
  scissor.offset.x      = 0;
  scissor.offset.y      = 0;
  vkCmdSetScissor(draw_commandbuffer, 0, 1, &scissor);
}

static void dd_vk_commandbuffer_end_render_pass(dd_graphics_commandbuffer_t in_commandbuffer) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;
  // Note that ending the renderpass changes the image's layout from COLOR_ATTACHMENT_OPTIMAL to
  // PRESENT_SRC_KHR
  vkCmdEndRenderPass(draw_commandbuffer);

  commandbuffer->render_pass = VK_NULL_HANDLE;
}

void dd_vk_commandbuffer_begin_compute(dd_graphics_commandbuffer_t in_commandbuffer) {
  struct vulkan_commandbuffer_t* vulkan_commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer command_buffer = vulkan_commandbuffer->buffer;

  const VkCommandBufferBeginInfo cmd_buf_info = {
      .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .pNext            = NULL,
      .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
      .pInheritanceInfo = NULL,
  };
  VK_CHECK_RESULT(vkBeginCommandBuffer(command_buffer, &cmd_buf_info));
}

static void dd_vk_commandbuffer_end_compute(dd_graphics_commandbuffer_t in_commandbuffer) {
  // TODO: fix this, it's currently done in default_vulkan_window.c
  /*struct vulkan_commandbuffer_t* vulkan_commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer commandbuffer = vulkan_commandbuffer->buffer;
  vkEndCommandBuffer(commandbuffer);
  */
}

static void dd_vk_commandbuffer_set_viewport(dd_graphics_commandbuffer_t in_commandbuffer,
                                             float origin_x, float origin_y, float width,
                                             float height, float z_near, float z_far) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;

  VkViewport viewport = {.x        = origin_x,
                         .y        = origin_y,
                         .width    = width,
                         .height   = height,
                         .minDepth = z_near,
                         .maxDepth = z_far};
  vkCmdSetViewport(draw_commandbuffer, 0, 1, &viewport);
}

static void dd_vk_commandbuffer_begin(dd_graphics_commandbuffer_t in_commandbuffer) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer cb = commandbuffer->buffer;

  const VkCommandBufferBeginInfo cmd_buf_info = {
      .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .pNext            = NULL,
      .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
      .pInheritanceInfo = NULL,
  };
  VK_CHECK_RESULT(vkBeginCommandBuffer(cb, &cmd_buf_info));
}

static void dd_vk_commandbuffer_end(dd_graphics_commandbuffer_t in_commandbuffer) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer cb = commandbuffer->buffer;
  VK_CHECK_RESULT(vkEndCommandBuffer(cb));
}

void debug_print_command_buffers(vulkan_api_device_t* device) {
  for (uint32 q_idx = 0; q_idx < VULKAN_MAX_QUEUES; q_idx++) {
    struct vulkan_queue_t* q = device->logical_device.queues + q_idx;
    for (uint32 i = 0; i < array_count(q->command_pool.buffers_free); i++) {
      dd_devprintf("%i f  0x%p\n", q_idx, (void*)q->command_pool.buffers_free[i]);
    }
    for (uint32 i = 0; i < array_count(q->command_pool.pending_buffers); i++) {
      dd_devprintf("%i u  0x%p\n", q_idx, (void*)q->command_pool.pending_buffers[i]->buffer);
    }
  }
}

void vulkan_device_retire_commandbuffer(vulkan_api_device_t* device,
                                        struct vulkan_commandbuffer_t* commandbuffer) {
  // Get time results
  if (device->logical_device.query.timer_offset) {
#if 1
    uint64 timer_data[2] = {
        vulkan_query_read_results_and_retire(device, commandbuffer->timer_query[0]),
        vulkan_query_read_results_and_retire(device, commandbuffer->timer_query[1])};
    {
      // Hardcoded profiler input
      uint8* buffer       = profiler_store->block_alloc(512);
      uint8* buffer_alloc = buffer;

      *(buffer++) = SENTINAL_THREAD_ID;
      {
        dd_perf_threaddata_t td = {.thread_id = 0xFFFFFF};
        memcpy(buffer, &td, sizeof(td));
        buffer += sizeof(td);
      }

      *(buffer++) = SENTINAL_THREAD_NAME;
      {
        dd_perf_threadname_t tn = {.name = "GPU"};
        memcpy(buffer, &tn, sizeof(tn));
        buffer += sizeof(tn);
      }

      *(buffer++) = SENTINAL_START;
      {
        dd_perf_startdata sd = {
            .label     = "GPU",
            .category  = "GPU cat",
            .timestamp = timer_data[0],
        };
        memcpy(buffer, &sd, sizeof(sd));
        buffer += sizeof(sd);
      }

      *(buffer++) = SENTINAL_END;
      {
        dd_perf_enddata ed = {.timestamp = timer_data[1]};
        memcpy(buffer, &ed, sizeof(ed));
        buffer += sizeof(ed);
      }

      uint32 data_byte_count = (uint32)(buffer - buffer_alloc);
      (void)data_byte_count;
      profiler_store->block_retire(0xFFFFFF, buffer_alloc, data_byte_count, 128);
    }
    //   dd_devprintf("%llu us time %u us for %u us\n", commandbuffer->cpu_time_requested__us,
    //                timer_data[0], (timer_data[1] - timer_data[0]));
#endif
  }

  for (uint32 q_idx = 0; q_idx < VULKAN_MAX_QUEUES; q_idx++) {
    struct vulkan_queue_t* queue = device->logical_device.queues + q_idx;
    if (queue->handle == VK_NULL_HANDLE) {
      continue;
    }

    for (uint32 i = 0; i < array_count(queue->command_pool.pending_buffers); i++) {
      if (queue->command_pool.pending_buffers[i] == commandbuffer) {
        array_push(queue->command_pool.buffers_free, commandbuffer->buffer,
                   queue->device->allocator);
        array_remove_at_index(queue->command_pool.pending_buffers, i);
        dd_free(queue->device->allocator, commandbuffer);
        break;
      }
    }
  }
}

dd_graphics_commandbuffer_t dd_vk_queue_get_command_buffer(dd_graphics_queue_t in_queue) {
  struct vulkan_queue_t* queue           = (struct vulkan_queue_t*)in_queue.ptr;
  struct vulkan_commandpool_t* pool      = &(queue->command_pool);
  struct vulkan_api_device_t* api_device = queue->device;

  if (array_count(pool->buffers_free) == 0) {
    const uint32 additional_command_buffer_count = 10;
    const VkCommandBufferAllocateInfo ai         = {
        .sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext              = NULL,
        .commandPool        = pool->pool,
        .level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = additional_command_buffer_count,
    };
    array_resize(pool->buffers_free, additional_command_buffer_count, api_device->allocator);
    VK_CHECK_RESULT(
        vkAllocateCommandBuffers(api_device->logical_device.device, &ai, pool->buffers_free));
  }

  VkCommandBuffer cmd_buffer = pool->buffers_free[--(array_header(pool->buffers_free)->count_)];

  VK_CHECK_RESULT(vkResetCommandBuffer(cmd_buffer, 0));
  const VkCommandBufferBeginInfo cmd_buf_info = {
      .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .pNext            = NULL,
      .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
      .pInheritanceInfo = NULL,
  };
  VK_CHECK_RESULT(vkBeginCommandBuffer(cmd_buffer, &cmd_buf_info));

  if (array_count(api_device->descriptor_pools_free) == 0) {
    vulkan_create_descriptor_pools(api_device);
  }
  g_active_descriptor_pool =
      api_device
          ->descriptor_pools_free[--(array_header(api_device->descriptor_pools_free)->count_)];
  array_push(api_device->descriptor_pools_used, g_active_descriptor_pool, api_device->allocator);
  vkResetDescriptorPool(api_device->logical_device.device, g_active_descriptor_pool, 0);

  struct vulkan_commandbuffer_t* cmd =
      dd_alloc(queue->device->allocator, sizeof(struct vulkan_commandbuffer_t), 1);

  *cmd = (struct vulkan_commandbuffer_t){
      .device                 = api_device,
      .buffer                 = cmd_buffer,
      .cpu_time_requested__us = dd_time_microseconds(),
  };
  cmd->timer_query[0] = vulkan_commandbuffer_enqueue_query_timer(api_device, cmd);
  return (dd_graphics_commandbuffer_t){cmd};
}

void dd_vk_queue_submit_command_buffer(dd_graphics_queue_t in_queue,
                                       dd_graphics_commandbuffer_t in_commandbuffer) {
  struct vulkan_queue_t* queue = (struct vulkan_queue_t*)in_queue.ptr;
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;

  commandbuffer->timer_query[1] =
      vulkan_commandbuffer_enqueue_query_timer(queue->device, commandbuffer);

  VK_CHECK_RESULT(vkEndCommandBuffer(commandbuffer->buffer));

  VkSubmitInfo submit_info       = {0};
  submit_info.sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.commandBufferCount = 1;
  submit_info.pCommandBuffers    = &commandbuffer->buffer;
  VK_CHECK_RESULT(vkQueueSubmit(queue->handle, 1, &submit_info, 0));

  array_push(queue->command_pool.pending_buffers, commandbuffer, queue->device->allocator);
}

VkFormat dd_vk_vulkan_pixel_format(uint32 format) {
  if (format < PIXEL_FORMAT_MAX) {
    VkFormat out_formats[] = {VK_FORMAT_UNDEFINED,
                              // 32-bit formats
                              VK_FORMAT_R8G8B8A8_UNORM, VK_FORMAT_B8G8R8A8_UNORM,
                              // Compressed formats
                              VK_FORMAT_BC1_RGBA_UNORM_BLOCK,
                              // Depth formats
                              VK_FORMAT_D16_UNORM};
    static_assert(countof(out_formats) == PIXEL_FORMAT_MAX, "Incorrect number of pixel formats");
    return out_formats[format];
  } else {
    return (VkFormat)
        format;  // Assume the format is some hardware supported one that we don't know about
  }
}

VkFramebuffer vulkan_prepare_framebuffer_with_formats(
    vulkan_api_device_t* api_device, const dd_graphics_render_pass_descriptor_t* in_rpd,
    VkRenderPass in_render_pass) {
  struct vulkan_cached_framebuffers_t* cfb = &api_device->logical_device.cached_framebuffers;

  // TODO: currently calculating the hash twice, once for render pass, and again for framebuffer
  const uint64 h = murmur_hash_64(in_rpd, sizeof(*in_rpd), 0);
  for (uint32 i = 0; i < VULKAN_MAX_CACHED_FRAMEBUFFERS; i++) {
    if (h == cfb->hashes[i]) {
      return cfb->framebuffers[i];
    }
  }

  // Need to create a new frame buffer
  assert(cfb->count < VULKAN_MAX_CACHED_RENDERPASSES);

  VkImageView attachments[GRAPHICS_MAX_COLOR_ATTACHMENTS + 1] = {0};

  uint32 attachment_count = 0;
  for (uint32 i = 0; i < GRAPHICS_MAX_COLOR_ATTACHMENTS; i++) {
    if (in_rpd->color[i]) {
      const struct vulkan_texture_t* to = (const struct vulkan_texture_t*)in_rpd->color[i];
      attachments[attachment_count++]   = to->view;
    }
  }
  if (in_rpd->depth) {
    attachments[attachment_count++] = ((const struct vulkan_texture_t*)in_rpd->depth)->view;
  }
  const VkFramebufferCreateInfo fb_info = {
      .sType           = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
      .pNext           = NULL,
      .renderPass      = in_render_pass,
      .attachmentCount = attachment_count,
      .pAttachments    = attachments,
      .width           = in_rpd->width,
      .height          = in_rpd->height,
      .layers          = 1,
  };
  VkFramebuffer framebuffer;
  VK_CHECK_RESULT(vkCreateFramebuffer(((vulkan_api_device_t*)api_device)->logical_device.device,
                                      &fb_info, NULL, &framebuffer));

  cfb->framebuffers[cfb->count] = framebuffer;
  cfb->hashes[cfb->count]       = h;
  cfb->count++;

  return framebuffer;
}

void vulkan_renderpass_clear_cache(vulkan_api_device_t* device) {
  struct vulkan_cached_renderpasses_t* crp = &device->logical_device.cached_render_passes;
  crp->count                               = 0;
}

VkRenderPass vulkan_prepare_render_pass_with_formats(
    vulkan_api_device_t* device, const dd_graphics_render_pass_descriptor_t* in_rpd) {
  struct vulkan_cached_renderpasses_t* crp = &device->logical_device.cached_render_passes;

  const uint64 h = murmur_hash_64(in_rpd, sizeof(*in_rpd), 0);
  for (uint32 i = 0; i < VULKAN_MAX_CACHED_RENDERPASSES; i++) {
    if (h == crp->hashes[i]) {
      return crp->passes[i];
    }
  }

  // Need to create a new render pass
  assert(crp->count < VULKAN_MAX_CACHED_RENDERPASSES);

  // The initial layout for the color and depth attachments will be LAYOUT_UNDEFINED
  // because at the start of the renderpass, we don't care about their contents.
  // At the start of the subpass, the color attachment's layout will be transitioned
  // to LAYOUT_COLOR_ATTACHMENT_OPTIMAL and the depth stencil attachment's layout
  // will be transitioned to LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL.  At the end of
  // the renderpass, the color attachment's layout will be transitioned to
  // LAYOUT_PRESENT_SRC_KHR to be ready to present.  This is all done as part of
  // the renderpass, no barriers are necessary.

  VkAttachmentDescription attachments[GRAPHICS_MAX_COLOR_ATTACHMENTS + 1]        = {0};
  VkSubpassDependency attachmentDependencies[GRAPHICS_MAX_COLOR_ATTACHMENTS + 1] = {0};
  VkAttachmentReference color_references[GRAPHICS_MAX_COLOR_ATTACHMENTS + 1]     = {0};
  const VkAttachmentReference depth_reference                                    = {
      .attachment = 1,
      .layout     = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
  };
  VkSubpassDescription subpass = {
      .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,

  };

  uint32 attachment_count = 0;
  for (uint32 i = 0; i < GRAPHICS_MAX_COLOR_ATTACHMENTS; i++) {
    if (in_rpd->color[i]) {
      VkFormat color_format =
          dd_vk_vulkan_pixel_format(((const struct vulkan_texture_t*)(in_rpd->color[i]))->format);
      attachments[attachment_count] = (VkAttachmentDescription){
          .format         = color_format,
          .flags          = 0,
          .samples        = VK_SAMPLE_COUNT_1_BIT,
          .loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR,
          .storeOp        = VK_ATTACHMENT_STORE_OP_STORE,
          .stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
          .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
          .initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED,
          .finalLayout    = in_rpd->is_for_present ? VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
                                                : VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
      };
      color_references[attachment_count] = (VkAttachmentReference){
          .attachment = i,
          .layout     = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
      };
      attachmentDependencies[attachment_count] = (VkSubpassDependency){
          // Image Layout Transition
          .srcSubpass    = VK_SUBPASS_EXTERNAL,
          .dstSubpass    = 0,
          .srcStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
          .dstStageMask  = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
          .srcAccessMask = 0,
          .dstAccessMask =
              VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
          .dependencyFlags = 0,
      };

      attachment_count++;
    }
  }
  if (attachment_count) {
    subpass.colorAttachmentCount = attachment_count;
    subpass.pColorAttachments    = color_references;
  }

  if (in_rpd->depth) {
    VkFormat depth_format =
        dd_vk_vulkan_pixel_format(((const struct vulkan_texture_t*)(in_rpd->depth))->format);
    attachments[attachment_count] = (VkAttachmentDescription){
        .format         = depth_format,
        .flags          = 0,
        .samples        = VK_SAMPLE_COUNT_1_BIT,
        .loadOp         = VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp        = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .stencilLoadOp  = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .initialLayout  = VK_IMAGE_LAYOUT_UNDEFINED,
        .finalLayout    = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    };
    subpass.pDepthStencilAttachment          = &depth_reference;
    attachmentDependencies[attachment_count] = (VkSubpassDependency){
        // Depth buffer is shared between swapchain images
        .srcSubpass = VK_SUBPASS_EXTERNAL,
        .dstSubpass = 0,
        .srcStageMask =
            VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
        .dstStageMask =
            VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
        .srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT |
                         VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
        .dependencyFlags = 0,
    };

    attachment_count++;
  }

  const VkRenderPassCreateInfo rp_info = {
      .sType           = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
      .attachmentCount = attachment_count,
      .pAttachments    = attachments,
      .subpassCount    = 1,
      .pSubpasses      = &subpass,
      .dependencyCount = attachment_count,
      .pDependencies   = attachmentDependencies,
  };

  VkRenderPass render_pass;
  VK_CHECK_RESULT(vkCreateRenderPass(device->logical_device.device, &rp_info, NULL, &render_pass));

  assert(crp->count < VULKAN_MAX_CACHED_RENDERPASSES);
  crp->passes[crp->count] = render_pass;
  crp->hashes[crp->count] = h;
  crp->count++;
  return render_pass;
}

static void vulkan_create_descriptor_pools(vulkan_api_device_t* api_device) {
  if (array_count(api_device->descriptor_pools_free) == 0) {
    const VkDescriptorPoolSize type_counts[] = {
        {
            .type            = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 10000,
        },
        {
            .type            = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
            .descriptorCount = 10000,
        },
        {
            .type            = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 10000,
        },
    };
    const VkDescriptorPoolCreateInfo descriptor_pool = {
        .sType         = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext         = NULL,
        .maxSets       = 10000,
        .poolSizeCount = countof(type_counts),
        .pPoolSizes    = type_counts,
    };

    const uint32 descriptor_pool_count = 10;
    for (unsigned i = 0; i < descriptor_pool_count; i++) {
      VkDescriptorPool pool;
      VK_CHECK_RESULT(vkCreateDescriptorPool(api_device->logical_device.device, &descriptor_pool,
                                             NULL, &pool));
      array_push(api_device->descriptor_pools_free, pool, api_device->allocator);
    }
  }
}

static VkDescriptorSet vulkan_prepare_descriptor_set(
    vulkan_api_device_t* api_device, VkDescriptorPool pool,
    struct vulkan_pipeline_t* in_out_pipeline,
    const dd_graphics_material_descriptor_t* in_material_descriptor) {
  struct vulkan_materialprototype_t* in_material_prototype = &in_out_pipeline->vmp;

  VkDescriptorSetAllocateInfo alloc_info = {
      .sType              = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
      .pNext              = NULL,
      .descriptorPool     = pool,
      .descriptorSetCount = 1,
      .pSetLayouts        = &in_out_pipeline->desc_layout};
  VkDescriptorSet ds;
  VkResult result = vkAllocateDescriptorSets(api_device->logical_device.device, &alloc_info, &ds);
  if (result == VK_ERROR_OUT_OF_POOL_MEMORY) {
    dd_devprintf("Descriptor pool out of memory\n");
    // TODO: handle this case
    assert(false);
  }

  uint8 stack_buffer[5000];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));
  VkWriteDescriptorSet* writes = {0}; /* array */

  // TODO: dynamically allocate these so can support more than a single buffer and texture
  VkDescriptorBufferInfo buffer_info[MaterialPrototypeMaxBindingCount] = {0};
  VkDescriptorImageInfo tex_desc[32 * 32]                              = {0};

  for (unsigned i = 0; i < MaterialPrototypeMaxBindingCount; i++) {
    switch (in_material_prototype->bindings[i].type) {
      case BindingTypeUniformBuffer: {
        const struct VulkanBuffer* b =
            (const struct VulkanBuffer*)(in_material_descriptor->buffers[i].buffer);
        buffer_info[i].offset = in_material_descriptor->buffers[i].offset;
        buffer_info[i].range  = in_material_descriptor->buffers[i].range;
        buffer_info[i].buffer = b->buffer;
        {
          VkWriteDescriptorSet w = {w.sType          = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                                    .dstBinding      = i,
                                    .descriptorCount = 1,
                                    .descriptorType  = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                                    .pBufferInfo     = buffer_info + i,
                                    .dstSet          = ds};
          array_push(writes, w, stack_allocator);
        }
      } break;
      case BindingTypeStorageBuffer: {
        const struct VulkanBuffer* b =
            (const struct VulkanBuffer*)(in_material_descriptor->buffers[i].buffer);
        buffer_info[i].offset = in_material_descriptor->buffers[i].offset;
        buffer_info[i].range  = in_material_descriptor->buffers[i].range;
        buffer_info[i].buffer = b->buffer;
        {
          VkWriteDescriptorSet w = {w.sType          = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                                    .dstBinding      = i,
                                    .descriptorCount = 1,
                                    .descriptorType  = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                                    .pBufferInfo     = buffer_info + i,
                                    .dstSet          = ds};
          array_push(writes, w, stack_allocator);
        }
      } break;
      case BindingTypeTexture: {
        const struct vulkan_texture_t** all_textures =
            (const struct vulkan_texture_t**)(in_material_descriptor->textures[i]);
        uint32 texture_count = max(1, in_material_prototype->bindings[i].props.array.count);
        for (uint32 ti = 0; ti < texture_count; ti++) {
          const struct vulkan_texture_t* to = all_textures[ti];
          tex_desc[ti].sampler              = to->sampler;
          tex_desc[ti].imageView            = to->view;
          tex_desc[ti].imageLayout          = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        }
        {
          VkWriteDescriptorSet w = {w.sType          = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                                    .dstBinding      = i,
                                    .descriptorCount = texture_count,
                                    .descriptorType  = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                    .pImageInfo      = tex_desc,
                                    .dstSet          = ds};
          array_push(writes, w, stack_allocator);
        }
      } break;
      default:  // Do nothing
        break;
    }
  }

  vkUpdateDescriptorSets(api_device->logical_device.device, (uint32)array_count(writes), writes, 0,
                         NULL);

  return ds;
}

extern void vulkan_create_texture_with_data(
    vulkan_api_device_t* device, VkCommandBuffer init_commandbuffer,
    const dd_graphics_texture_descriptor_t* in_texture_descriptor, const uint8* in_texture_data,
    uint32 in_texture_data_length, struct vulkan_texture_t* out_texture);

static dd_graphics_material_prototype_t dd_vk_material_prototype_create(
    dd_graphics_device_t in_device,
    const dd_graphics_material_prototype_descriptor_t* in_material_descriptor) {
  vulkan_api_device_t* device = (vulkan_api_device_t*)in_device;

  struct vulkan_pipeline_t vp = {0};
  array_push(device->material_prototypes, vp, device->allocator);
  vulkan_create_material_prototype(
      device, in_material_descriptor,
      &device->material_prototypes[array_count(device->material_prototypes) - 1]);

  return (dd_graphics_material_prototype_t)(uintptr_t)(array_count(device->material_prototypes) -
                                                       1);
}

static dd_graphics_compute_prototype_t dd_vk_compute_prototype_create(
    dd_graphics_device_t in_device,
    const dd_graphics_compute_prototype_descriptor_t* in_compute_descriptor) {
  vulkan_api_device_t* device = (vulkan_api_device_t*)in_device;

  struct vulkan_pipeline_t vp = {0};
  array_push(device->material_prototypes, vp, device->allocator);
  vulkan_create_compute_prototype(
      device, in_compute_descriptor,
      &device->material_prototypes[array_count(device->material_prototypes) - 1]);

  return (dd_graphics_compute_prototype_t)(uintptr_t)(array_count(device->material_prototypes) -
                                                      1);
}

static dd_graphics_texture_t dd_vk_texture_create(
    dd_graphics_device_t in_device,
    const dd_graphics_texture_descriptor_t* in_texture_descriptor) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;
  struct vulkan_texture_t* t = dd_alloc(api_device->allocator, sizeof(struct vulkan_texture_t), 1);
  memset(t, 0, sizeof(*t));
  vulkan_create_texture(api_device, VK_IMAGE_LAYOUT_UNDEFINED, in_texture_descriptor, t);
  return t;
}

#if 0
/*  Removed this function when I added the texture_map/texture_unmap functions as it isn't really needed anymore.
 *  I'm not sure about the transition though, that may be needed, although it doesn't seem like it.
 */
static dd_graphics_texture_t dd_vk_create_texture_with_data(
    dd_graphics_device_t in_device, const dd_graphics_texture_descriptor_t* in_texture_descriptor,
    const uint8* in_pixel_data, const uint32 in_pixel_byte_count) {
  VulkanAPIDevice* api_device = (VulkanAPIDevice*)in_device;

  struct texture_object* t = dd_alloc(api_device->allocator, sizeof(struct texture_object), 1);
  memset(t, 0, sizeof(*t));
  vulkan_create_texture(api_device, VK_IMAGE_LAYOUT_PREINITIALIZED, in_texture_descriptor, t);

  VkCommandBuffer init_commandbuffer;
  {
    const VkCommandBufferAllocateInfo cmd = {
        .sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext              = NULL,
        .commandPool        = api_device->cmd.pool,
        .level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1,
    };
    VK_CHECK_RESULT(vkAllocateCommandBuffers(api_device->logical_device.device, &cmd, &init_commandbuffer));
  }
  VkCommandBufferBeginInfo cmd_buf_info = {
      .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .pNext            = NULL,
      .flags            = 0,
      .pInheritanceInfo = NULL,
  };
  VK_CHECK_RESULT(vkBeginCommandBuffer(init_commandbuffer, &cmd_buf_info));

  vulkan_create_texture_upload_data(api_device, init_commandbuffer, in_texture_descriptor,
                                    in_pixel_data, in_pixel_byte_count, t);

  dd_vk_submit_command_buffer_to_queue(api_device, &g_demo.graphics_queue, init_commandbuffer);

  return t;
}
#endif

void vulkan_destroy_logical_device(vulkan_api_device_t* device) {
  for (unsigned i = 0; i < array_count(device->descriptor_pools_free); i++) {
    vkDestroyDescriptorPool(device->logical_device.device, device->descriptor_pools_free[i], NULL);
  }
  array_reset(device->descriptor_pools_free);
  for (unsigned i = 0; i < array_count(device->descriptor_pools_used); i++) {
    vkDestroyDescriptorPool(device->logical_device.device, device->descriptor_pools_used[i], NULL);
  }
  array_reset(device->descriptor_pools_used);

  for (unsigned i = 0; i < array_count(device->logical_device.query.pools); i++) {
    vkDestroyQueryPool(device->logical_device.device, device->logical_device.query.pools[i], NULL);
  }
  array_reset(device->logical_device.query.pools);

  array_header(device->descriptor_pools_free)->count_ = 0;
  array_header(device->descriptor_pools_used)->count_ = 0;
}

static void dd_vk_material_prototype_destroy(
    dd_graphics_device_t in_device, dd_graphics_material_prototype_t in_material_prototype) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;

  struct vulkan_pipeline_t* vp =
      api_device->material_prototypes + (uintptr_t)in_material_prototype;
  struct vulkan_materialprototype_t* mp = &vp->vmp;

  VkDevice device = api_device->logical_device.device;
  for (uint32 i = 0; i < ShaderTypeMax; i++) {
    if (vp->shaders[i]) {
      vkDestroyShaderModule(device, vp->shaders[i], NULL);
    }
  }
  vkDestroyPipeline(device, vp->pipeline, NULL);
  vkDestroyPipelineLayout(device, vp->pipeline_layout, NULL);
  vkDestroyDescriptorSetLayout(device, vp->desc_layout, NULL);
  memset(mp, 0, sizeof(*mp));
}

static void dd_vk_compute_prototype_destroy(dd_graphics_device_t in_device,
                                            dd_graphics_compute_prototype_t in_compute_prototype) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;

  struct vulkan_pipeline_t* vp = api_device->material_prototypes + (uintptr_t)in_compute_prototype;
  struct vulkan_materialprototype_t* mp = &vp->vmp;

  VkDevice device = api_device->logical_device.device;
  for (uint32 i = 0; i < ShaderTypeMax; i++) {
    if (vp->shaders[i]) {
      vkDestroyShaderModule(device, vp->shaders[i], NULL);
    }
  }
  vkDestroyPipeline(device, vp->pipeline, NULL);
  vkDestroyPipelineLayout(device, vp->pipeline_layout, NULL);
  vkDestroyDescriptorSetLayout(device, vp->desc_layout, NULL);
  memset(mp, 0, sizeof(*mp));
}

extern LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

/*
 * Return 1 (true) if all layer names specified in check_names
 * can be found in given layer properties.
 */
static VkBool32 demo_check_layers(uint32_t check_count, char** check_names, uint32_t layer_count,
                                  VkLayerProperties* layers) {
  for (uint32_t i = 0; i < check_count; i++) {
    VkBool32 found = 0;
    for (uint32_t j = 0; j < layer_count; j++) {
      if (!strcmp(check_names[i], layers[j].layerName)) {
        found = 1;
        break;
      }
    }
    if (!found) {
      fprintf(stderr, "Cannot find layer: %s\n", check_names[i]);
      return 0;
    }
  }
  return 1;
}

uint32 vulkan_commandbuffer_enqueue_query_timer(vulkan_api_device_t* api_device,
                                                struct vulkan_commandbuffer_t* cmd_buffer) {
  struct vulkan_logical_device_t* ld = &api_device->logical_device;

  uint32 pool_index = 0xFFFFFFFF;
  if (array_count(ld->query.pools) == 0 ||
      ld->query.next_free_index == VULKAN_MAX_QUERIES_PER_POOL) {
    // Find a pool that has no outstanding queries
    for (uint32 i = 0; i < array_count(ld->query.pools); i++) {
      if (ld->query.outstanding_queries[i] == 0) {
        pool_index = i;
      }
    }

    if (pool_index == 0xFFFFFFFF) {
      VkQueryPoolCreateInfo createInfo = {
          .sType      = VK_STRUCTURE_TYPE_QUERY_POOL_CREATE_INFO,
          .queryType  = VK_QUERY_TYPE_TIMESTAMP,
          .queryCount = VULKAN_MAX_QUERIES_PER_POOL,
      };

      VkQueryPool pool;
      VK_CHECK_RESULT(vkCreateQueryPool(ld->device, &createInfo, NULL, &pool));

      array_push(ld->query.pools, pool, api_device->allocator);
      array_push(ld->query.outstanding_queries, 0, api_device->allocator);
      pool_index = ld->query.next_free_pool_index = (uint32)(array_count(ld->query.pools) - 1);
    } else {
      ld->query.next_free_pool_index = pool_index;
    }

    VkQueryPool pool = ld->query.pools[pool_index];
    vkCmdResetQueryPool(cmd_buffer->buffer, pool, 0, VULKAN_MAX_QUERIES_PER_POOL);
    ld->query.next_free_index = 0;
  } else {
    pool_index = ld->query.next_free_pool_index;
  }

  VkQueryPool query_pool = ld->query.pools[pool_index];
  uint32 query           = ld->query.next_free_index++;

  vkCmdWriteTimestamp(cmd_buffer->buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, query_pool, query);

  BIT_SET(ld->query.outstanding_queries[pool_index], query);

  uint32 query_index = (pool_index << 24) | query;
  return query_index;
}

uint64 vulkan_query_read_results_and_retire(vulkan_api_device_t* api_device, uint32 query_index) {
  struct vulkan_logical_device_t* ld = &api_device->logical_device;

  uint32 pool_index = query_index >> 24;
  uint32 query      = query_index & 0xFFFFFF;

  VkQueryPool query_pool = ld->query.pools[pool_index];
  uint64 data__ticks;
  VK_CHECK_RESULT(vkGetQueryPoolResults(ld->device, query_pool, query, 1, sizeof(uint64),
                                        &data__ticks, 0, VK_QUERY_RESULT_64_BIT));

  BIT_CLEAR(ld->query.outstanding_queries[pool_index], query);

  uint32 nano_seconds_timestamp = (uint32)api_device->gpu_props.limits.timestampPeriod;
  uint64 data__us               = data__ticks * nano_seconds_timestamp / 1000;

  // Adjust for estimated difference between GPU and CPU
  data__us += api_device->logical_device.query.timer_offset;

  return data__us;
}

static void vk_create_logical_device_and_queues(VkPhysicalDevice in_physicalDevice,
                                                const char* const* in_device_extension_names,
                                                unsigned in_device_extensions_count,
                                                unsigned in_num_queues,
                                                VkDevice* out_logical_device,
                                                struct vulkan_queue_t* in_out_queues) {
  // All queues are created with equal priority
  float queue_priorities[4] = {0};
  VkDeviceQueueCreateInfo queues[2];
  assert(in_num_queues <= 2);

  uint32 total_queues_created = 0;
  for (unsigned i = 0; i < in_num_queues; i++) {
    uint32_t num_queues_of_type = 1;
    // TODO: allow creation of multiple of the same queue type
    //__popcnt(in_out_queues[i].type);
    queues[i].sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queues[i].pNext            = NULL;
    queues[i].queueFamilyIndex = in_out_queues[i].family_index;
    queues[i].queueCount       = num_queues_of_type;
    queues[i].pQueuePriorities = queue_priorities + total_queues_created;
    queues[i].flags            = 0;
    total_queues_created += num_queues_of_type;
  }

  VkDeviceCreateInfo dci = {
      .sType                   = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext                   = NULL,
      .queueCreateInfoCount    = in_num_queues,
      .pQueueCreateInfos       = queues,
      .enabledLayerCount       = 0,
      .ppEnabledLayerNames     = NULL,
      .enabledExtensionCount   = in_device_extensions_count,
      .ppEnabledExtensionNames = in_device_extension_names,
      .pEnabledFeatures        = NULL,  // If specific features are required, pass them in here
  };
  // See http://roar11.com/2019/06/vulkan-textures-unbound/
  VkPhysicalDeviceDescriptorIndexingFeaturesEXT indexingFeatures = {
      .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES_EXT,
      .descriptorBindingPartiallyBound           = VK_TRUE,
      .runtimeDescriptorArray                    = VK_TRUE,
      .shaderSampledImageArrayNonUniformIndexing = VK_TRUE};
  dci.pNext = &indexingFeatures;
  VK_CHECK_RESULT(vkCreateDevice(in_physicalDevice, &dci, NULL, out_logical_device));
  for (unsigned i = 0; i < in_num_queues; i++) {
    vkGetDeviceQueue(*out_logical_device, in_out_queues[i].family_index, 0,
                     &in_out_queues[i].handle);
  }
}

dd_graphics_queue_t dd_vk_device_get_queue(dd_graphics_device_t in_device, EQueueType in_type) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;
  for (uint32 i = 0; i < VULKAN_MAX_QUEUES; i++) {
    if (api_device->logical_device.queues[i].type & in_type) {
      void* q = api_device->logical_device.queues + i;
      return (dd_graphics_queue_t){q};
    }
  }
  ERR_EXIT("No queue available of requested type.", "Setup error");
}

uint32 vulkan_determine_queue_families(struct demo* demo, EQueueType in_required_queue_types,
                                       struct vulkan_queue_t* out_queues) {
  vulkan_api_device_t* api_device = &demo->device;

  uint32 max_queues_needed = __popcnt((int32)in_required_queue_types);
  memset(out_queues, 0, max_queues_needed * sizeof(out_queues));

  struct vulkan_physical_device_t* pd = &api_device->physical_device;
  // Check if there is a single queue that supports all requested operations
  uint32 single_queue_family_index = UINT32_MAX;
  for (uint32_t i = 0; (i < pd->queue_family.count) && (single_queue_family_index == UINT32_MAX);
       i++) {
    bool is_queue_acceptable = true;
    if (in_required_queue_types & QueueTypePresent) {
      VkBool32 supported = false;
      demo->fpGetPhysicalDeviceSurfaceSupportKHR(pd->device, i, demo->surface, &supported);
      is_queue_acceptable |= (supported == VK_TRUE);
    }
    if (in_required_queue_types & QueueTypeGraphics) {
      is_queue_acceptable |= ((pd->queue_family.props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) != 0);
    }
    if (in_required_queue_types & QueueTypeCompute) {
      is_queue_acceptable |= ((pd->queue_family.props[i].queueFlags & VK_QUEUE_COMPUTE_BIT) != 0);
    }
    if (in_required_queue_types & QueueTypeTransfer) {
      is_queue_acceptable |= ((pd->queue_family.props[i].queueFlags & VK_QUEUE_TRANSFER_BIT) != 0);
    }
    if (is_queue_acceptable) {
      single_queue_family_index = i;
    }
  }
  const bool has_single_acceptable_queue = (single_queue_family_index == UINT32_MAX);

  uint8 stack_buffer[5000];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));
  uint32_t* queueFamilyIndices = {0}; /* array */
  EQueueType* queue_types      = {0}; /* array */
  if (!has_single_acceptable_queue) {
    // Iterate over each queue to learn whether it supports presenting:
    VkBool32* supportsPresent =
        dd_alloc(api_device->allocator, pd->queue_family.count * sizeof(VkBool32), 1);
    if (in_required_queue_types & QueueTypePresent) {
      for (uint32_t i = 0; i < pd->queue_family.count; i++) {
        demo->fpGetPhysicalDeviceSurfaceSupportKHR(pd->device, i, demo->surface,
                                                   &supportsPresent[i]);
      }
    }

    // Search for a graphics and a present queue in the array of queue
    // families, try to find one that supports both
    uint32_t graphicsQueueFamilyIndex = UINT32_MAX;
    uint32_t presentQueueFamilyIndex  = UINT32_MAX;
    for (uint32_t i = 0; i < api_device->physical_device.queue_family.count; i++) {
      if ((api_device->physical_device.queue_family.props[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) !=
          0) {
        if (graphicsQueueFamilyIndex == UINT32_MAX) {
          graphicsQueueFamilyIndex = i;
        }

        if (in_required_queue_types & QueueTypePresent) {
          if (supportsPresent[i] == VK_TRUE) {
            graphicsQueueFamilyIndex = i;
            presentQueueFamilyIndex  = i;
            break;
          }
        }
      }
    }

    if (in_required_queue_types & QueueTypePresent) {
      if (presentQueueFamilyIndex == UINT32_MAX) {
        // If didn't find a queue that supports both graphics and present, then
        // find a separate present queue.
        for (uint32_t i = 0; i < api_device->physical_device.queue_family.count; ++i) {
          if (supportsPresent[i] == VK_TRUE) {
            presentQueueFamilyIndex = i;
            break;
          }
        }
      }
    }

    if (in_required_queue_types & QueueTypePresent) {
      // Generate error if could not find both a graphics and a present queue
      if (graphicsQueueFamilyIndex == UINT32_MAX || presentQueueFamilyIndex == UINT32_MAX) {
        ERR_EXIT("Could not find both graphics and present queues\n",
                 "Swapchain Initialization Failure");
      }
      demo->separate_present_queue = (graphicsQueueFamilyIndex != presentQueueFamilyIndex);
    } else {
      if (graphicsQueueFamilyIndex == UINT32_MAX) {
        ERR_EXIT("Could not find graphics queues\n", "Initialization Failure");
      }
    }

    dd_free(api_device->allocator, supportsPresent);
    array_push(queueFamilyIndices, graphicsQueueFamilyIndex, stack_allocator);

    if (demo->separate_present_queue) {
      array_push(queueFamilyIndices, presentQueueFamilyIndex, stack_allocator);
      array_push(queue_types, QueueTypeGraphics, stack_allocator);
      array_push(queue_types, QueueTypePresent, stack_allocator);

    } else {
      array_push(queue_types, QueueTypeGraphics | QueueTypePresent, stack_allocator);
    }
  } else {
    array_push(queueFamilyIndices, single_queue_family_index, stack_allocator);
    array_push(queue_types, in_required_queue_types, stack_allocator);
  }

  for (uint32 i = 0; i < array_count(queue_types); i++) {
    out_queues[i].type         = queue_types[i];
    out_queues[i].family_index = queueFamilyIndices[i];
  }

  return (uint32)array_count(queue_types);
}

static void vulkan_create_command_buffers_for_pool(
    vulkan_api_device_t* api_device, struct vulkan_logical_device_t* in_logical_device,
    uint32 family_index, struct vulkan_commandpool_t* out_vkp) {
  out_vkp->queue_family_index        = family_index;
  const VkCommandPoolCreateInfo cpci = {
      .sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .pNext            = NULL,
      .queueFamilyIndex = family_index,
      .flags            = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
  };
  VK_CHECK_RESULT(vkCreateCommandPool(in_logical_device->device, &cpci, NULL, &out_vkp->pool));

  const uint32 initial_command_buffer_count = 10;
  const VkCommandBufferAllocateInfo ai      = {
      .sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .pNext              = NULL,
      .commandPool        = out_vkp->pool,
      .level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = initial_command_buffer_count,
  };
  array_resize(out_vkp->buffers_free, initial_command_buffer_count, api_device->allocator);
  assert(array_capacity(out_vkp->pending_buffers) == 0);
  array_reset(out_vkp->pending_buffers);
  VK_CHECK_RESULT(vkAllocateCommandBuffers(in_logical_device->device, &ai, out_vkp->buffers_free));
}

void vulkan_calibrate_timestamps(struct vulkan_api_device_t* api_device) {
  // Calibrate timestamps
#if 0
  // My GPU doesn't support the needed extension: VK_EXT_calibrated_timestamps
  {
    VkCalibratedTimestampInfoEXT infos[2] = {
        {.sType      = VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT,
         .timeDomain = VK_TIME_DOMAIN_QUERY_PERFORMANCE_COUNTER_EXT},
        {.sType      = VK_STRUCTURE_TYPE_CALIBRATED_TIMESTAMP_INFO_EXT,
         .timeDomain = VK_TIME_DOMAIN_DEVICE_EXT}};
    uint64 timestamps[2] = {0};
    uint64 max_deviation = {0};
    PFN_vkGetCalibratedTimestampsEXT func =
        (PFN_vkGetCalibratedTimestampsEXT)vkGetInstanceProcAddr(api_device->inst,
                                                                "vkGetCalibratedTimestampsEXT");
    func(logical_device->device, 2, infos, timestamps, &max_deviation);

    LARGE_INTEGER frequency = {0};
    QueryPerformanceFrequency(&frequency);
    frequency.QuadPart /= 1000000;
    uint64 cpu_us = (uint64)(timestamps[1] / frequency.QuadPart);

    dd_devprintf("cpu time %llu\n", dd_time_microseconds());
    dd_devprintf("vulkan cpu time %llu\n", cpu_us);
    dd_devprintf("vulkan gpu time %llu\n", timestamps[0]);
  }
#else
  uint64 cpu_begin__us = dd_time_microseconds();

  // The following sequence shouldn't do much on the GPU, so should finish quickly ...
  dd_graphics_queue_t queue       = dd_vk_device_get_queue(api_device, QueueTypeGraphics);
  dd_graphics_commandbuffer_t cmd = dd_vk_queue_get_command_buffer(queue);
  dd_vk_queue_submit_command_buffer(queue, cmd);

  uint64 cpu_end__us = dd_time_microseconds();

  VK_CHECK_RESULT(vkDeviceWaitIdle(api_device->logical_device.device));
  struct vulkan_commandbuffer_t* vulkan_cmd = (struct vulkan_commandbuffer_t*)cmd.ptr;
  uint64 timer_data[2]                      = {
      vulkan_query_read_results_and_retire(api_device, vulkan_cmd->timer_query[0]),
      vulkan_query_read_results_and_retire(api_device, vulkan_cmd->timer_query[1])};
  vulkan_device_retire_commandbuffer(api_device, vulkan_cmd);

  // Now offset the GPU values by half the CPU time, just a guess
  api_device->logical_device.query.timer_offset =
      (cpu_begin__us / 2) + (cpu_end__us / 2) - timer_data[0];
#endif
}

void vulkan_create_logical_device(struct demo* demo, EQueueType required_queue_types) {
  vulkan_api_device_t* api_device                = &demo->device;
  struct vulkan_logical_device_t* logical_device = &api_device->logical_device;

  memset(logical_device->queues, 0, sizeof(logical_device->queues));
  uint32 queue_count =
      vulkan_determine_queue_families(demo, required_queue_types, logical_device->queues);
  for (uint32 i = 0; i < queue_count; i++) {
    logical_device->queues[i].device = api_device;
  }
  vk_create_logical_device_and_queues(api_device->physical_device.device,
                                      api_device->device_extensions.names,
                                      api_device->device_extensions.count, queue_count,
                                      &logical_device->device, logical_device->queues);

  api_device->graphics_queue = logical_device->queues + 0;
  if (required_queue_types & QueueTypeCompute) {
    assert(queue_count == 1);
    api_device->compute_queue = logical_device->queues + 0;
  }
  demo->present_queue = &logical_device->queues[(demo->separate_present_queue) ? 1 : 0];

  assert(logical_device->queues[0].command_pool.pool == VK_NULL_HANDLE);

  vulkan_create_descriptor_pools(api_device);

  for (uint32 i = 0; i < queue_count; i++) {
    vulkan_create_command_buffers_for_pool(api_device, logical_device,
                                           logical_device->queues[i].family_index,
                                           &logical_device->queues[i].command_pool);
  }

  vulkan_calibrate_timestamps(api_device);
}

void dd_vk_buffer_allocate(dd_graphics_device_t in_device, uint32_t count, const uint32* in_sizes,
                           dd_graphics_buffer_t* out_buffers) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;
  for (uint32_t i = 0; i < count; i++) {
    VulkanBuffer* vk_buffer = dd_alloc(api_device->allocator, sizeof(VulkanBuffer), 1);

    VkMemoryRequirements mem_reqs;
    VkMemoryAllocateInfo mem_alloc;
    VkBufferCreateInfo buf_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
                 VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT |
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        .size = in_sizes[i]};

    VK_CHECK_RESULT(
        vkCreateBuffer(api_device->logical_device.device, &buf_info, NULL, &vk_buffer->buffer));

    vkGetBufferMemoryRequirements(api_device->logical_device.device, vk_buffer->buffer, &mem_reqs);

    mem_alloc.sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    mem_alloc.pNext           = NULL;
    mem_alloc.allocationSize  = mem_reqs.size;
    mem_alloc.memoryTypeIndex = 0;

    memory_type_from_properties(
        &api_device->memory_properties, mem_reqs.memoryTypeBits,
        VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
        &mem_alloc.memoryTypeIndex);

    VK_CHECK_RESULT(vkAllocateMemory(api_device->logical_device.device, &mem_alloc,
                                     &api_device->vk_allocator_callbacks, &vk_buffer->memory));

    VK_CHECK_RESULT(vkMapMemory(api_device->logical_device.device, vk_buffer->memory, 0,
                                VK_WHOLE_SIZE, 0, &vk_buffer->memory_ptr));

    VK_CHECK_RESULT(vkBindBufferMemory(api_device->logical_device.device, vk_buffer->buffer,
                                       vk_buffer->memory, 0));

    out_buffers[i] = vk_buffer;
  }
}

void dd_vk_buffer_destroy(dd_graphics_device_t in_device, uint32_t count,
                          dd_graphics_buffer_t* out_buffers) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;
  VulkanBuffer** vk_buffers       = (VulkanBuffer**)out_buffers;
  for (unsigned i = 0; i < count; i++) {
    vkDestroyBuffer(api_device->logical_device.device, vk_buffers[i]->buffer, NULL);
    vkUnmapMemory(api_device->logical_device.device, vk_buffers[i]->memory);
    vkFreeMemory(api_device->logical_device.device, vk_buffers[i]->memory,
                 &api_device->vk_allocator_callbacks);
    dd_free(api_device->allocator, out_buffers[i]);
  }
}

void dd_vk_texture_destroy(dd_graphics_device_t in_device, const uint32_t in_count,
                           dd_graphics_texture_t* in_textures) {
  vulkan_api_device_t* api_device       = (vulkan_api_device_t*)in_device;
  struct vulkan_texture_t** vk_textures = (struct vulkan_texture_t**)in_textures;
  for (unsigned i = 0; i < in_count; i++) {
    struct vulkan_texture_t* texture = vk_textures[i];
    vkDestroyImageView(api_device->logical_device.device, texture->view, NULL);
    vkDestroyImage(api_device->logical_device.device, texture->image, NULL);
    vkFreeMemory(api_device->logical_device.device, texture->mem,
                 &api_device->vk_allocator_callbacks);
    vkDestroySampler(api_device->logical_device.device, texture->sampler, NULL);
    memset(texture, 0, sizeof(*texture));
  }
}

void dd_vk_buffer_update_data(dd_graphics_device_t* device, dd_graphics_buffer_t in_buffer,
                              uint32 in_offset_byte_count, const void* in_data,
                              uint32 in_num_bytes) {
  (void)device;
  VulkanBuffer* vk_buffer = (VulkanBuffer*)in_buffer;
  memcpy((void*)((intptr_t)vk_buffer->memory_ptr + in_offset_byte_count), in_data, in_num_bytes);
}

void dd_vk_buffer_copy(dd_graphics_commandbuffer_t in_commandbuffer,
                       const dd_graphics_buffer_t src_buffer, uint32 src_offset_bytes,
                       dd_graphics_buffer_t dst_buffer, uint32 dst_offset_bytes,
                       uint32 byte_count) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer active_commandbuffer = commandbuffer->buffer;
  VulkanBuffer* src_vk_buffer          = (VulkanBuffer*)src_buffer;
  VulkanBuffer* dst_vk_buffer          = (VulkanBuffer*)dst_buffer;
  VkBufferCopy buffer_copy             = {
      .srcOffset = src_offset_bytes, .dstOffset = dst_offset_bytes, .size = byte_count};
  vkCmdCopyBuffer(active_commandbuffer, src_vk_buffer->buffer, dst_vk_buffer->buffer, 1,
                  &buffer_copy);
}

void* VKAPI_PTR vulkan_AllocationFunction(void* pUserData, size_t size, size_t alignment,
                                          VkSystemAllocationScope allocationScope) {
  (void)allocationScope;
  void* out = dd_alloc((dd_allocator*)pUserData, (uint32)size, (uint32)alignment);
  assert(((uintptr_t)out) % alignment == 0);
  return out;
}

void* VKAPI_PTR vulkan_ReallocationFunction(void* pUserData, void* pOriginal, size_t size,
                                            size_t alignment,
                                            VkSystemAllocationScope allocationScope) {
  (void)allocationScope;
  void* out = dd_alloc((dd_allocator*)pUserData, (uint32)size, (uint32)alignment);
  assert(((uintptr_t)out) % alignment == 0);

  assert(false);  // TODO: implement copy
  return out;
}

void VKAPI_PTR vulkan_FreeFunction(void* pUserData, void* pMemory) {
  if (pMemory != NULL) {
    dd_free((dd_allocator*)pUserData, pMemory);
  }
}

void vulkan_init(vulkan_api_device_t* api_device, struct dd_allocator* allocator,
                 const char** in_required_instance_extensions /* array */,
                 const char** in_required_device_extensions /* array */) {
  api_device->allocator = allocator;

  api_device->vk_allocator_callbacks =
      (VkAllocationCallbacks){.pUserData       = allocator,
                              .pfnAllocation   = &vulkan_AllocationFunction,
                              .pfnReallocation = &vulkan_ReallocationFunction,
                              .pfnFree         = &vulkan_FreeFunction};

  uint32_t instance_extension_count     = 0;
  api_device->instance_extensions.count = 0;
  api_device->device_extensions.count   = 0;

  // Add empty pipeline, so material prototypes get non-null value
  struct vulkan_pipeline_t vp = {0};
  array_push(api_device->material_prototypes, vp, api_device->allocator);

  /* Look for instance extensions */
  memset(api_device->instance_extensions.names, 0, sizeof(api_device->instance_extensions.names));
  memset(api_device->device_extensions.names, 0, sizeof(api_device->device_extensions.names));

  if (enableValidationLayers && !checkValidationLayerSupport(api_device)) {
    ERR_EXIT("validation layers requested, but not available!", "validation layer failure");
  }

  VK_CHECK_RESULT(vkEnumerateInstanceExtensionProperties(NULL, &instance_extension_count, NULL));

  if (instance_extension_count > 0) {
    VkExtensionProperties* instance_extensions = dd_alloc(
        api_device->allocator, sizeof(VkExtensionProperties) * instance_extension_count, 1);
    VK_CHECK_RESULT(vkEnumerateInstanceExtensionProperties(NULL, &instance_extension_count,
                                                           instance_extensions));

    for (uint32_t re_idx = 0; re_idx < array_count(in_required_instance_extensions); re_idx++) {
      int32 index = -1;
      for (uint32 i = 0; i < instance_extension_count; i++) {
        if (strcmp(in_required_instance_extensions[re_idx],
                   instance_extensions[i].extensionName) == 0)
          index = (int32)i;
      }
      if (index != -1) {
        api_device->instance_extensions.names[api_device->instance_extensions.count++] =
            instance_extensions[index].extensionName;
      } else {
        dd_devprintf(
            "vkEnumerateInstanceExtensionProperties failed to find "
            "the %s extension.\n\n"
            "Do you have a compatible Vulkan installable client driver (ICD) installed?\n"
            "Please look at the Getting Started guide for additional information.\n",
            "vkCreateInstance Failure", in_required_instance_extensions[re_idx]);
        exit(1);
      }
    }

    if (enableValidationLayers) {
      int32 index = -1;
      for (uint32 i = 0; i < instance_extension_count; i++) {
        if (strcmp(VK_EXT_DEBUG_UTILS_EXTENSION_NAME, instance_extensions[i].extensionName) == 0)
          index = (int32)i;
      }
      if (index != -1) {
        api_device->instance_extensions.names[api_device->instance_extensions.count++] =
            VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
      }
    }

    assert(api_device->instance_extensions.count < 64);

    // TODO: the requested extensions stored in the struct reference this array, so can't free it
    // free(instance_extensions);
  }

  const VkApplicationInfo app = {
      .sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO,
      .pNext              = NULL,
      .pApplicationName   = APP_SHORT_NAME,
      .applicationVersion = 0,
      .pEngineName        = APP_SHORT_NAME,
      .engineVersion      = 0,
      .apiVersion         = VK_API_VERSION_1_0,
  };
  VkDebugUtilsMessengerCreateInfoEXT createInfo = {
      .sType           = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
      .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                         VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
      .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                     VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
      .pfnUserCallback = debugCallback,
  };
  VkInstanceCreateInfo inst_info = {
      .sType                   = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pNext                   = &createInfo,
      .pApplicationInfo        = &app,
      .enabledExtensionCount   = api_device->instance_extensions.count,
      .ppEnabledExtensionNames = (const char* const*)api_device->instance_extensions.names,
  };
  if (enableValidationLayers) {
    inst_info.enabledLayerCount   = (uint32_t)(countof(g_validation_layers));
    inst_info.ppEnabledLayerNames = g_validation_layers;
  } else {
    inst_info.enabledLayerCount = 0;
  }

  uint32_t gpu_count;

  VkInstance vkInstance;
  VkResult err = vkCreateInstance(&inst_info, NULL, &vkInstance);
  if (err == VK_ERROR_INCOMPATIBLE_DRIVER) {
    ERR_EXIT(
        "Cannot find a compatible Vulkan installable client driver (ICD).\n\n"
        "Please look at the Getting Started guide for additional information.\n",
        "vkCreateInstance Failure");
  } else if (err == VK_ERROR_EXTENSION_NOT_PRESENT) {
    ERR_EXIT(
        "Cannot find a specified extension library.\n"
        "Make sure your layers path is set appropriately.\n",
        "vkCreateInstance Failure");
  } else if (err) {
    ERR_EXIT(
        "vkCreateInstance failed.\n\n"
        "Do you have a compatible Vulkan installable client driver (ICD) installed?\n"
        "Please look at the Getting Started guide for additional information.\n",
        "vkCreateInstance Failure");
  }
  api_device->inst = vkInstance;

  vulkan_setup_debug_messenger(api_device);

  /* Make initial call to query gpu_count, then second call for gpu info*/
  VK_CHECK_RESULT(vkEnumeratePhysicalDevices(vkInstance, &gpu_count, NULL));

  VkPhysicalDevice vkGpu;
  if (gpu_count > 0) {
    VkPhysicalDevice* physical_devices =
        dd_alloc(api_device->allocator, sizeof(VkPhysicalDevice) * gpu_count, 4);

    VK_CHECK_RESULT(vkEnumeratePhysicalDevices(vkInstance, &gpu_count, physical_devices));
    /* For cube demo we just grab the first physical device */
    vkGpu = physical_devices[0];
    dd_free(api_device->allocator, physical_devices);
  } else {
    ERR_EXIT(
        "vkEnumeratePhysicalDevices reported zero accessible devices.\n\n"
        "Do you have a compatible Vulkan installable client driver (ICD) installed?\n"
        "Please look at the Getting Started guide for additional information.\n",
        "vkEnumeratePhysicalDevices Failure");
  }

  /* Look for device extensions */
  uint32_t device_extension_count = 0;

  VK_CHECK_RESULT(
      vkEnumerateDeviceExtensionProperties(vkGpu, NULL, &device_extension_count, NULL));

  if (device_extension_count > 0) {
    VkExtensionProperties* device_extensions =
        dd_alloc(api_device->allocator, sizeof(VkExtensionProperties) * device_extension_count, 1);
    VK_CHECK_RESULT(vkEnumerateDeviceExtensionProperties(vkGpu, NULL, &device_extension_count,
                                                         device_extensions));

    for (uint32_t re_idx = 0; re_idx < array_count(in_required_device_extensions); re_idx++) {
      int32 index                   = -1;
      const char* extension_to_find = in_required_device_extensions[re_idx];
      for (uint32 i = 0; i < device_extension_count; i++) {
        if (strcmp(extension_to_find, device_extensions[i].extensionName) == 0) {
          index = (int32)i;
        }
      }
      if (index != -1) {
        api_device->device_extensions.names[api_device->device_extensions.count++] =
            device_extensions[index].extensionName;
      } else {
        dd_devprintf(
            "vkEnumerateDeviceExtensionProperties failed to find "
            "the %s extension.\n\n",
            extension_to_find);
        dd_devprintf(
            "Do you have a compatible Vulkan installable client driver (ICD) installed?\n"
            "Please look at the Getting Started guide for additional information.\n",
            "vkCreateInstance Failure");
        exit(1);
      }
      assert(api_device->device_extensions.count < 64);
    }

    // TODO: requested extensions references this array, so can't free
    // free(device_extensions);
  }

  vkGetPhysicalDeviceProperties(vkGpu, &api_device->gpu_props);
  dd_devprintf("Vulkan device: %s\n", api_device->gpu_props.deviceName);
  dd_devprintf("Vulkan version: %u.%u.%u\n", VK_VERSION_MAJOR(api_device->gpu_props.apiVersion),
               VK_VERSION_MINOR(api_device->gpu_props.apiVersion),
               VK_VERSION_PATCH(api_device->gpu_props.apiVersion));
  dd_devprintf("Device props:\n");
  dd_devprintf("    maxImageArrayLayers %i\n", api_device->gpu_props.limits.maxImageArrayLayers);
  dd_devprintf("    maxDescriptorSetSamplers: %i\n",
               api_device->gpu_props.limits.maxDescriptorSetSamplers);
  dd_devprintf("    timestampPeriod: %f\n", api_device->gpu_props.limits.timestampPeriod);

  /* Call with NULL data to get count */
  vkGetPhysicalDeviceQueueFamilyProperties(vkGpu, &api_device->physical_device.queue_family.count,
                                           NULL);
  assert(api_device->physical_device.queue_family.count >= 1);

  api_device->physical_device.queue_family.props = dd_alloc(
      api_device->allocator,
      api_device->physical_device.queue_family.count * sizeof(VkQueueFamilyProperties), 1);
  vkGetPhysicalDeviceQueueFamilyProperties(vkGpu, &api_device->physical_device.queue_family.count,
                                           api_device->physical_device.queue_family.props);

  // Query fine-grained feature support for this device.
  //  If app has specific feature requirements it should check supported
  //  features based on this query
  VkPhysicalDeviceFeatures physDevFeatures;
  vkGetPhysicalDeviceFeatures(vkGpu, &physDevFeatures);

  api_device->physical_device.device = vkGpu;

  vkGetPhysicalDeviceMemoryProperties(api_device->physical_device.device,
                                      &api_device->memory_properties);
}

void vulkan_instance_destroy(vulkan_api_device_t* api_device) {
  if (enableValidationLayers) {
    DestroyDebugUtilsMessengerEXT(api_device->inst, api_device->debug.messenger, NULL);
  }

  vkDestroyInstance(api_device->inst, NULL);
}

void dd_vk_commandbuffer_bind_material_prototype(
    dd_graphics_commandbuffer_t in_commandbuffer,
    dd_graphics_material_prototype_t in_material_prototype) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  struct vulkan_pipeline_t* vp =
      commandbuffer->device->material_prototypes + (uintptr_t)in_material_prototype;

  commandbuffer->active_pipeline = vp;
}

void dd_vk_commandbuffer_bind_buffers_and_textures(
    dd_graphics_commandbuffer_t in_commandbuffer,
    dd_graphics_material_prototype_t in_material_prototype,
    const dd_graphics_material_descriptor_t* in_material_descriptor) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;
  struct vulkan_pipeline_t* vp =
      commandbuffer->device->material_prototypes + (uintptr_t)in_material_prototype;
  vp->bound_descriptors = in_material_descriptor;

  const VkPipelineBindPoint pipeline_bind_point = (vp->shaders[ShaderTypeCompute] != NULL)
                                                      ? VK_PIPELINE_BIND_POINT_COMPUTE
                                                      : VK_PIPELINE_BIND_POINT_GRAPHICS;
  VkDescriptorSet ds = vulkan_prepare_descriptor_set(
      commandbuffer->device, g_active_descriptor_pool, vp, in_material_descriptor);
  vkCmdBindDescriptorSets(draw_commandbuffer, pipeline_bind_point, vp->pipeline_layout, 0, 1, &ds,
                          0, NULL);
  if (pipeline_bind_point == VK_PIPELINE_BIND_POINT_GRAPHICS) {
    if (in_material_descriptor->index_buffer.buffer) {
      const struct VulkanBuffer* b =
          (const struct VulkanBuffer*)(in_material_descriptor->index_buffer.buffer);
      vkCmdBindIndexBuffer(
          draw_commandbuffer, b->buffer, in_material_descriptor->index_buffer.offset,
          (in_material_descriptor->index_type == IndexTypeUint16) ? VK_INDEX_TYPE_UINT16
                                                                  : VK_INDEX_TYPE_UINT32);
    }
  }
}

void dd_vk_commandbuffer_push_constants(dd_graphics_commandbuffer_t in_commandbuffer,
                                        uint32 bytes_count, void* values) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  struct vulkan_pipeline_t* vp = commandbuffer->active_pipeline;

  vkCmdPushConstants(commandbuffer->buffer, vp->pipeline_layout, VK_SHADER_STAGE_ALL, 0,
                     bytes_count, values);
}

void dd_vk_commandbuffer_draw_with_offsets(dd_graphics_commandbuffer_t in_commandbuffer,
                                           const uint32 in_primitive_count,
                                           const uint32 in_primitive_base,
                                           const uint32 in_instance_count,
                                           const uint32 in_instance_base) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  struct vulkan_pipeline_t* vp = commandbuffer->active_pipeline;

  if (vp->pipeline == 0) {
    // Late creation of pipeline, this isn't ideal. It's better to create pipelines ahead of time
    // if you can.
    vulkan_create_graphics_pipeline(commandbuffer->device, commandbuffer->render_pass, vp);
  }
  vkCmdBindPipeline(commandbuffer->buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vp->pipeline);

  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;
  vkCmdDraw(draw_commandbuffer, in_primitive_count, in_instance_count, in_primitive_base,
            in_instance_base);

#if 0
  // Debug code to check that drawing is happening
  uint64 t                     = dd_time_microseconds();
  VkClearAttachment attachment = {.aspectMask      = VK_IMAGE_ASPECT_COLOR_BIT,
                                  .colorAttachment = 0,
                                  .clearValue      = {.color.float32 = {0.0f, 1.0f, 1.0f, 1}}};
  VkClearRect rect = {.rect       = {.offset = {t % 100, (t / 100) % 100}, .extent = {50, 50}},
                      .layerCount = 1};
  vkCmdClearAttachments(draw_commandbuffer, 1, &attachment, 1, &rect);
#endif
}

void dd_vk_commandbuffer_draw(dd_graphics_commandbuffer_t in_commandbuffer,
                              const uint32 in_vertex_count, const uint32 in_instance_count) {
  dd_vk_commandbuffer_draw_with_offsets(in_commandbuffer, in_vertex_count, 0, in_instance_count,
                                        0);
}

void dd_vk_commandbuffer_draw_indirect(dd_graphics_commandbuffer_t in_commandbuffer,
                                       dd_graphics_buffer_t in_buffer, uint32 offset_bytes,
                                       uint32 draw_count, uint32 stride_bytes) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  struct vulkan_pipeline_t* vp = commandbuffer->active_pipeline;
  VulkanBuffer* vk_buffer      = (VulkanBuffer*)in_buffer;

  if (vp->pipeline == 0) {
    // Late creation of pipeline, this isn't ideal. It's better to create pipelines ahead of time
    // if you can.
    vulkan_create_graphics_pipeline(commandbuffer->device, commandbuffer->render_pass, vp);
  }
  vkCmdBindPipeline(commandbuffer->buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vp->pipeline);

  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;
  vkCmdDrawIndirect(draw_commandbuffer, vk_buffer->buffer, offset_bytes, draw_count, stride_bytes);
}

void dd_vk_commandbuffer_draw_indexed_with_offsets(dd_graphics_commandbuffer_t in_commandbuffer,
                                                   const uint32 in_index_count,
                                                   const uint32 in_index_base,
                                                   const uint32 in_vertex_base,
                                                   const uint32 in_instance_count,
                                                   const uint32 in_instance_base) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  struct vulkan_pipeline_t* vp = commandbuffer->active_pipeline;

  if (vp->pipeline == 0) {
    // Late creation of pipeline, this isn't ideal. It's better to create pipelines ahead of time
    // if you can.
    vulkan_create_graphics_pipeline(commandbuffer->device, commandbuffer->render_pass, vp);
  }
  vkCmdBindPipeline(commandbuffer->buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, vp->pipeline);

  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;
  vkCmdDrawIndexed(draw_commandbuffer, in_index_count, in_instance_count, in_index_base,
                   in_vertex_base, in_instance_base);

#if 0
  // Debug code to check that drawing is happening
  uint64 t                     = dd_time_microseconds();
  VkClearAttachment attachment = {.aspectMask      = VK_IMAGE_ASPECT_COLOR_BIT,
                                  .colorAttachment = 0,
                                  .clearValue      = {.color.float32 = {0.0f, 1.0f, 1.0f, 1}}};
  VkClearRect rect = {.rect       = {.offset = {t % 100, (t / 100) % 100}, .extent = {50, 50}},
                      .layerCount = 1};
  vkCmdClearAttachments(draw_commandbuffer, 1, &attachment, 1, &rect);
#endif
}

void dd_vk_commandbuffer_dispatch(dd_graphics_commandbuffer_t in_commandbuffer,
                                  const uint32 in_group_count_x, const uint32 in_group_count_y,
                                  const uint32 in_group_count_z) {
  struct vulkan_commandbuffer_t* commandbuffer =
      (struct vulkan_commandbuffer_t*)in_commandbuffer.ptr;
  struct vulkan_pipeline_t* vp = commandbuffer->active_pipeline;

  assert(in_group_count_x < commandbuffer->device->gpu_props.limits.maxComputeWorkGroupCount[0]);
  assert(in_group_count_y < commandbuffer->device->gpu_props.limits.maxComputeWorkGroupCount[1]);
  assert(in_group_count_z < commandbuffer->device->gpu_props.limits.maxComputeWorkGroupCount[2]);

  if (vp->pipeline == 0) {
    // Late creation of pipeline, this isn't ideal. It's better to create pipelines ahead of time
    // if you can.
    vulkan_create_compute_pipeline(commandbuffer->device, vp);
  }
  vkCmdBindPipeline(commandbuffer->buffer, VK_PIPELINE_BIND_POINT_COMPUTE, vp->pipeline);

  VkCommandBuffer draw_commandbuffer = commandbuffer->buffer;
  vkCmdDispatch(draw_commandbuffer, in_group_count_x, in_group_count_y, in_group_count_z);

  // TODO(Rick): Remove hardcoded barrier
  {
    VulkanBuffer* vk_buffer = (VulkanBuffer*)(vp->bound_descriptors->buffers[2].buffer);

    VkBufferMemoryBarrier barrier = {.sType         = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                                     .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
                                     .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
                                     // uint32_t srcQueueFamilyIndex;
                                     // uint32_t dstQueueFamilyIndex;
                                     .buffer = vk_buffer->buffer,
                                     .size   = VK_WHOLE_SIZE};
    (void)barrier;
    /*vkCmdPipelineBarrier(draw_commandbuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                         VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, 0, 0, NULL, 1, &barrier, 0, NULL);*/
    vkCmdPipelineBarrier(draw_commandbuffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 0, NULL);
  }
}

void dd_vk_commandbuffer_draw_indexed(dd_graphics_commandbuffer_t in_commandbuffer,
                                      const uint32 in_index_count,
                                      const uint32 in_instance_count) {
  dd_vk_commandbuffer_draw_indexed_with_offsets(in_commandbuffer, in_index_count, 0, 0,
                                                in_instance_count, 0);
}

uint8* dd_vk_texture_map_memory(dd_graphics_device_t in_device, dd_graphics_texture_t in_texture) {
  vulkan_api_device_t* device         = (vulkan_api_device_t*)in_device;
  struct vulkan_texture_t* vk_texture = *(struct vulkan_texture_t**)&in_texture;

  uint8* imagedata;
  VkImageSubresource subResource        = {.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT};
  VkSubresourceLayout subResourceLayout = {0};
  vkGetImageSubresourceLayout(device->logical_device.device, vk_texture->image, &subResource,
                              &subResourceLayout);
  vkMapMemory(device->logical_device.device, vk_texture->mem, 0, VK_WHOLE_SIZE, 0,
              (void**)&imagedata);
  imagedata += subResourceLayout.offset;

  return imagedata;
}

void dd_vk_texture_unmap_memory(dd_graphics_device_t in_device, dd_graphics_texture_t in_texture) {
  vulkan_api_device_t* device         = (vulkan_api_device_t*)in_device;
  struct vulkan_texture_t* vk_texture = *(struct vulkan_texture_t**)&in_texture;

  vkUnmapMemory(device->logical_device.device, vk_texture->mem);
}

void dd_vk_device_wait_until_idle(dd_graphics_device_t in_device) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_device;

  VK_CHECK_RESULT(vkDeviceWaitIdle(api_device->logical_device.device));

  // Can now release all pending data
  for (uint32 i = 0; i < array_count(api_device->descriptor_pools_used); i++) {
    array_push(api_device->descriptor_pools_free, api_device->descriptor_pools_used[i],
               api_device->allocator);
  }
  array_reset(api_device->descriptor_pools_used);

  for (uint32 queue_idx = 0; queue_idx < VULKAN_MAX_QUEUES; queue_idx++) {
    struct vulkan_queue_t* queue = api_device->logical_device.queues + queue_idx;
    for (uint32 buffer_idx = 0; buffer_idx < array_count(queue->command_pool.pending_buffers);
         buffer_idx++) {
      vulkan_device_retire_commandbuffer(api_device,
                                         queue->command_pool.pending_buffers[buffer_idx]);
    }
    array_reset(queue->command_pool.pending_buffers);

// Get time results
#if 0
    uint32 nano_seconds_timestamp = (uint32)device->gpu_props.limits.timestampPeriod;
    uint64 timer_data[10]         = {0};
    for (uint32 query_idx = 0; query_idx < array_count(queue->query.used); query_idx++) {
      VK_CHECK_RESULT(vkGetQueryPoolResults(device->logical_device.device, queue->query.pool,
                                            queue->query.used[query_idx], 1, sizeof(uint64),
                                            timer_data + query_idx, 0, VK_QUERY_RESULT_64_BIT));
      array_push(queue->query.free, queue->query.used[query_idx], api_device->allocator);
    }
    if (array_count(queue->query.used) >= 2) {
      dd_devprintf("took %u ns\n", (timer_data[1] - timer_data[0]) / nano_seconds_timestamp);
    }
    array_reset(queue->query.used);
#endif
  }
}

void dd_vk_texture_set_pixels(dd_graphics_device_t in_api_device, dd_graphics_texture_t in_texture,
                              dd_rect_t in_rect, uint32 in_layer_index, uint32 in_mip_map_level,
                              uint32 bytes_count, uint8* in_data) {
  vulkan_api_device_t* api_device = (vulkan_api_device_t*)in_api_device;
  VkDevice logical_device         = api_device->logical_device.device;
  struct vulkan_texture_t* to     = (struct vulkan_texture_t*)in_texture;

  VkMemoryRequirements mem_reqs;
  VkMemoryAllocateInfo mem_alloc;
  VkBufferCreateInfo buf_info = {.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                                 .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                                 .size  = bytes_count};
  if (in_rect.width == 4) {
    buf_info.size = 256;
  }
  VkBuffer buffer;
  VK_CHECK_RESULT(vkCreateBuffer(logical_device, &buf_info, NULL, &buffer));

  vkGetBufferMemoryRequirements(logical_device, buffer, &mem_reqs);

  mem_alloc.sType           = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  mem_alloc.pNext           = NULL;
  mem_alloc.allocationSize  = mem_reqs.size;
  mem_alloc.memoryTypeIndex = 0;

  memory_type_from_properties(
      &api_device->memory_properties, mem_reqs.memoryTypeBits,
      VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
      &mem_alloc.memoryTypeIndex);

  VkDeviceMemory memory;
  VK_CHECK_RESULT(
      vkAllocateMemory(logical_device, &mem_alloc, &api_device->vk_allocator_callbacks, &memory));
  VK_CHECK_RESULT(vkBindBufferMemory(logical_device, buffer, memory, 0));

  void* ptr;
  VK_CHECK_RESULT(
      vkMapMemory(api_device->logical_device.device, memory, 0, VK_WHOLE_SIZE, 0, &ptr));

  memcpy(ptr, in_data, (size_t)bytes_count);
  vkUnmapMemory(api_device->logical_device.device, memory);

  struct vulkan_queue_t* queue =
      (struct vulkan_queue_t*)dd_vk_device_get_queue(in_api_device, QueueTypeGraphics).ptr;

  VkCommandBufferAllocateInfo allocInfo = {.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                                           .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                                           .commandPool        = queue->command_pool.pool,
                                           .commandBufferCount = 1};

  VkCommandBuffer commandBuffer;
  vkAllocateCommandBuffers(api_device->logical_device.device, &allocInfo, &commandBuffer);

  VkCommandBufferBeginInfo beginInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
      .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
  };

  vkBeginCommandBuffer(commandBuffer, &beginInfo);

  VkImageMemoryBarrier barrier            = {0};
  barrier.sType                           = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
  barrier.oldLayout                       = VK_IMAGE_LAYOUT_UNDEFINED;
  barrier.newLayout                       = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  barrier.srcQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
  barrier.image                           = to->image;
  barrier.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
  barrier.subresourceRange.baseMipLevel   = in_mip_map_level;
  barrier.subresourceRange.levelCount     = 1;
  barrier.subresourceRange.baseArrayLayer = in_layer_index;
  barrier.subresourceRange.layerCount     = 1;
  barrier.srcAccessMask                   = 0;
  barrier.dstAccessMask                   = VK_ACCESS_TRANSFER_WRITE_BIT;
  vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                       VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1, &barrier);

  VkBufferImageCopy region               = {0};
  region.bufferOffset                    = 0;
  region.bufferRowLength                 = 0;
  region.bufferImageHeight               = 0;
  region.imageSubresource.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
  region.imageSubresource.mipLevel       = in_mip_map_level;
  region.imageSubresource.baseArrayLayer = in_layer_index;
  region.imageSubresource.layerCount     = 1;
  region.imageOffset                     = (VkOffset3D){in_rect.x, in_rect.y, 0};
  region.imageExtent                     = (VkExtent3D){in_rect.width, in_rect.height, 1};
  vkCmdCopyBufferToImage(commandBuffer, buffer, to->image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
                         &region);

  barrier.oldLayout                       = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
  barrier.newLayout                       = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
  barrier.srcQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
  barrier.dstQueueFamilyIndex             = VK_QUEUE_FAMILY_IGNORED;
  barrier.image                           = to->image;
  barrier.subresourceRange.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT;
  barrier.subresourceRange.baseMipLevel   = in_mip_map_level;
  barrier.subresourceRange.levelCount     = 1;
  barrier.subresourceRange.baseArrayLayer = in_layer_index;
  barrier.subresourceRange.layerCount     = 1;
  barrier.srcAccessMask                   = VK_ACCESS_TRANSFER_WRITE_BIT;
  barrier.dstAccessMask                   = VK_ACCESS_SHADER_READ_BIT;
  vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                       VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0, 0, NULL, 0, NULL, 1, &barrier);

  vkEndCommandBuffer(commandBuffer);
  VkSubmitInfo submitInfo = {
      .sType              = VK_STRUCTURE_TYPE_SUBMIT_INFO,
      .commandBufferCount = 1,
      .pCommandBuffers    = &commandBuffer,
  };
  vkQueueSubmit(queue->handle, 1, &submitInfo, VK_NULL_HANDLE);
  vkQueueWaitIdle(queue->handle);

  vkFreeCommandBuffers(api_device->logical_device.device, queue->command_pool.pool, 1,
                       &commandBuffer);
}

void dd_vk_texture_get_pixels(dd_graphics_device_t in_api_device, dd_graphics_texture_t in_texture,
                              dd_rect_t in_rect, uint32 in_mip_map_level, uint8* out_pixels) {
  struct vulkan_texture_t* to = (struct vulkan_texture_t*)in_texture;

  dd_graphics_queue_t graphics_queue = dd_vk_device_get_queue(in_api_device, QueueTypeGraphics);
  dd_graphics_commandbuffer_t gg_copy_cmd = dd_vk_queue_get_command_buffer(graphics_queue);

  dd_graphics_texture_descriptor_t tcd = {.width           = in_rect.width,
                                          .height          = in_rect.height,
                                          .layers          = 1,
                                          .mip_level_count = 1,
                                          .format          = R8G8B8A8,
                                          .storage_mode    = SHARED,
                                          .usage           = TEXTURE_USAGE_TRANSFER_DESTINATION};
  struct vulkan_texture_t* color_dst =
      (struct vulkan_texture_t*)dd_vk_texture_create(in_api_device, &tcd);

  VkCommandBuffer copyCmd = ((struct vulkan_commandbuffer_t*)gg_copy_cmd.ptr)->buffer;
  VkImageMemoryBarrier image_memory_barrier = {
      .sType               = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext               = NULL,
      .srcAccessMask       = 0,
      .dstAccessMask       = VK_ACCESS_TRANSFER_WRITE_BIT,
      .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .oldLayout           = VK_IMAGE_LAYOUT_UNDEFINED,
      .newLayout           = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      .image               = color_dst->image,
      .subresourceRange    = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}};
  vkCmdPipelineBarrier(copyCmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                       0, NULL, 0, NULL, 1, &image_memory_barrier);

  VkImageCopy imageCopyRegion = {
      .srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .srcSubresource.layerCount = 1,
      .dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .dstSubresource.layerCount = 1,
      .srcOffset                 = {.x = in_rect.x, .y = in_rect.y},
      .extent                    = {.width = in_rect.width, .height = in_rect.height, .depth = 1}};

  vkCmdCopyImage(copyCmd, to->image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, color_dst->image,
                 VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &imageCopyRegion);

  VkImageMemoryBarrier image_memory_barrier2 = {
      .sType               = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .pNext               = NULL,
      .srcAccessMask       = VK_ACCESS_TRANSFER_WRITE_BIT,
      .dstAccessMask       = VK_ACCESS_MEMORY_READ_BIT,
      .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .oldLayout           = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      .newLayout           = VK_IMAGE_LAYOUT_GENERAL,
      .image               = color_dst->image,
      .subresourceRange    = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}};
  vkCmdPipelineBarrier(copyCmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
                       0, NULL, 0, NULL, 1, &image_memory_barrier2);

  dd_vk_queue_submit_command_buffer(graphics_queue, gg_copy_cmd);

  dd_vk_device_wait_until_idle(in_api_device);

  const uint8* imagedata = dd_vk_texture_map_memory(in_api_device, color_dst);
  memcpy(out_pixels, imagedata, in_rect.width * in_rect.height * 4);
  dd_vk_texture_unmap_memory(in_api_device, color_dst);

  dd_vk_texture_destroy(in_api_device, 1, &color_dst);
}

dd_graphics_device_t dd_vk_create_default_device(struct dd_allocator* allocator) {
  uint8 stack_buffer[5000];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));
  const char** required_instance_extensions = {0};
  const char** required_device_extensions   = {0};

  /* VK_EXT_descriptor_indexing
   * https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_EXT_descriptor_indexing.html
   * This extension is needed to allow a single drawcall to index into for example different
   * textures in an array of textures in the shader.
   * If this extension isn't enabled, then trying to index like that won't give any error or
   * warning, but the output will silently be incorrect.
   */
  array_push(required_instance_extensions, VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
             stack_allocator);
  array_push(required_device_extensions, VK_KHR_MAINTENANCE3_EXTENSION_NAME, stack_allocator);
  array_push(required_device_extensions, VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME,
             stack_allocator);

  // Not supported on my card
  // array_push(required_device_extensions, VK_EXT_CALIBRATED_TIMESTAMPS_EXTENSION_NAME,
  // stack_allocator);

  vulkan_init(&g_demo.device, allocator, required_instance_extensions, required_device_extensions);
  vulkan_create_logical_device(&g_demo, QueueTypeGraphics);

  return (dd_graphics_device_t)&g_demo.device;
}

dd_graphics_api_t dd_vulkan = {
    .create_default_device = &dd_vk_create_default_device,
    .get_device            = &dd_vulkan_create_device,
    .device =
        {
            .wait_until_idle = &dd_vk_device_wait_until_idle,
            .get_queue       = &dd_vk_device_get_queue,
        },
    .queue =
        {
            .get_command_buffer    = &dd_vk_queue_get_command_buffer,
            .submit_command_buffer = &dd_vk_queue_submit_command_buffer,
        },
    .buffer = {.allocate    = &dd_vk_buffer_allocate,
               .destroy     = &dd_vk_buffer_destroy,
               .update_data = &dd_vk_buffer_update_data,
               .copy        = &dd_vk_buffer_copy},
    .texture =
        {
            .create     = &dd_vk_texture_create,
            .destroy    = &dd_vk_texture_destroy,
            .set_pixels = &dd_vk_texture_set_pixels,
            .get_pixels = &dd_vk_texture_get_pixels,
        },
    .material_prototype =
        {
            .create  = &dd_vk_material_prototype_create,
            .destroy = &dd_vk_material_prototype_destroy,
        },
    .compute_prototype =
        {
            .create  = &dd_vk_compute_prototype_create,
            .destroy = &dd_vk_compute_prototype_destroy,
        },
    .commandbuffer = {
        .begin_render_pass         = &dd_vk_commandbuffer_begin_render_pass,
        .end_render_pass           = &dd_vk_commandbuffer_end_render_pass,
        .begin_compute             = &dd_vk_commandbuffer_begin_compute,
        .end_compute               = &dd_vk_commandbuffer_end_compute,
        .set_viewport              = &dd_vk_commandbuffer_set_viewport,
        .bind_material_prototype   = &dd_vk_commandbuffer_bind_material_prototype,
        .bind_buffers_and_textures = &dd_vk_commandbuffer_bind_buffers_and_textures,
        .push_constants            = &dd_vk_commandbuffer_push_constants,
        .draw                      = &dd_vk_commandbuffer_draw,
        .draw_indirect             = &dd_vk_commandbuffer_draw_indirect,
        .draw_with_offsets         = &dd_vk_commandbuffer_draw_with_offsets,
        .draw_indexed              = &dd_vk_commandbuffer_draw_indexed,
        .draw_indexed_with_offsets = &dd_vk_commandbuffer_draw_indexed_with_offsets,
        .dispatch                  = &dd_vk_commandbuffer_dispatch,
    }};
