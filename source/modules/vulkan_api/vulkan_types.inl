#pragma once

#include "core/memory.h"
#include "core/types.h"
#include "graphics_api/graphics.h"
#include "vulkan/vulkan.h"

#define VK_CHECK_RESULT(f)       \
  {                              \
    VkResult res = (f);          \
    if (res != VK_SUCCESS) {     \
      assert(res == VK_SUCCESS); \
    }                            \
  }

// TODO: assuming this is enough
enum {
  VULKAN_MAX_QUEUES              = 4,
  VULKAN_MAX_CACHED_RENDERPASSES = 4,
  VULKAN_MAX_CACHED_FRAMEBUFFERS = 4,
  VULKAN_MAX_QUERIES_PER_POOL    = 64
};

struct vulkan_commandpool_t {
  uint32 queue_family_index;
  VkCommandPool pool;
  VkCommandBuffer* buffers_free;                   /* array */
  struct vulkan_commandbuffer_t** pending_buffers; /* takes ownership of data */
};

struct vulkan_queue_t {
  struct vulkan_api_device_t* device;
  VkQueue handle;
  uint32 family_index;
  EQueueType type;

  struct vulkan_commandpool_t command_pool;
};

struct vulkan_physical_device_t {
  VkPhysicalDevice device;
  struct {
    VkQueueFamilyProperties* props;
    uint32_t count;
  } queue_family;
};

struct vulkan_logical_device_t {
  VkDevice device;
  struct vulkan_queue_t queues[VULKAN_MAX_QUEUES];

  struct {
    VkQueryPool* pools;          /* array */
    uint64* outstanding_queries; /* array */
    uint32 next_free_pool_index;
    uint32 next_free_index;
    uint64 timer_offset;
  } query;

  struct vulkan_cached_renderpasses_t {
    uint64 hashes[VULKAN_MAX_CACHED_RENDERPASSES];
    VkRenderPass passes[VULKAN_MAX_CACHED_RENDERPASSES];
    uint32 count;
  } cached_render_passes;
  struct vulkan_cached_framebuffers_t {
    uint64 hashes[VULKAN_MAX_CACHED_FRAMEBUFFERS];
    VkFramebuffer framebuffers[VULKAN_MAX_CACHED_FRAMEBUFFERS];
    uint32 count;
  } cached_framebuffers;
};

struct vulkan_materialprototype_t {
  bool enable_alpha_blend;
  ETopologyType topology;
  dd_graphics_material_prototype_binding_t bindings[MaterialPrototypeMaxBindingCount];
};

struct vulkan_pipeline_t {
  // Data needed to create pipeline, static for all instances of this pipeline
  VkPipelineShaderStageCreateInfo shaderStages[2];
  VkGraphicsPipelineCreateInfo ci;
  VkPipelineLayout pipeline_layout;
  VkShaderModule shaders[ShaderTypeMax];
  VkDescriptorSetLayout desc_layout;

  // Data needed to create pipeline, but may change between instances of this pipeline
  VkPipelineColorBlendAttachmentState att_state;

  // Legacy
  struct vulkan_materialprototype_t vmp;

  // TODO(Rick): hack needed to create memory barrier
  const dd_graphics_material_descriptor_t* bound_descriptors;

  // Actual useable pipeline
  VkPipeline pipeline;
};

struct vulkan_commandbuffer_t {
  struct vulkan_api_device_t* device;
  struct vulkan_pipeline_t* active_pipeline;

  VkRenderPass render_pass;
  VkCommandBuffer buffer;

  uint64 cpu_time_requested__us;
  uint32 timer_query[2];
};

typedef struct vulkan_api_device_t {
  VkInstance inst;
  struct vulkan_physical_device_t physical_device;
  struct vulkan_logical_device_t logical_device;
  VkPhysicalDeviceMemoryProperties memory_properties;

  VkDescriptorPool* descriptor_pools_free; /* array*/
  VkDescriptorPool* descriptor_pools_used; /* array*/

  dd_allocator* allocator;
  VkAllocationCallbacks vk_allocator_callbacks;

  struct {
    uint32_t count;
    char* names[64];
  } instance_extensions, device_extensions;

  VkPhysicalDeviceProperties gpu_props;
  struct vulkan_queue_t* graphics_queue;
  struct vulkan_queue_t* compute_queue;

  struct {
    VkDebugUtilsMessengerEXT messenger;
  } debug;

  struct vulkan_pipeline_t* material_prototypes; /* array */

} vulkan_api_device_t;

/*
 * structure to track all objects related to a texture.
 */
struct vulkan_texture_t {
  VkFormat format;
  VkImage image;
  VkMemoryAllocateInfo mem_alloc;
  VkDeviceMemory mem;
  VkImageView view;

  VkSampler sampler;
  VkBuffer buffer;
  VkImageLayout imageLayout;
};

struct vulkan_framebuffer_t {
  VkFramebuffer framebuffer;
  uint32 width;
  uint32 height;
};

struct vulkan_renderpass_t {
  VkRenderPass render_pass;
  uint32 width;
  uint32 height;
};

struct vulkan_graphics_renderpass_t {
  VkFramebuffer framebuffer;
  VkRenderPass render_pass;
};

struct vulkan_commandbuffers_frame_t {
  VkFence fence;
  struct vulkan_commandbuffer_t* buffer;
  VkCommandBuffer buffer_present;
  VkDescriptorPool descriptor_pool;
};

void vulkan_create_material_prototype(
    vulkan_api_device_t* device,
    const dd_graphics_material_prototype_descriptor_t* in_material_descriptor,
    struct vulkan_pipeline_t* out_pipeline);
void vulkan_create_compute_prototype(
    vulkan_api_device_t* device,
    const dd_graphics_compute_prototype_descriptor_t* in_compute_descriptor,
    struct vulkan_pipeline_t* out_pipeline);

struct demo;
void vulkan_create_logical_device(struct demo* demo, EQueueType required_queue_types);
dd_graphics_device_t dd_vulkan_create_device();
void vulkan_destroy_logical_device(vulkan_api_device_t* device);
VkRenderPass vulkan_prepare_render_pass_with_formats(
    vulkan_api_device_t* device, const dd_graphics_render_pass_descriptor_t* in_rpd);
VkFramebuffer vulkan_prepare_framebuffer_with_formats(
    vulkan_api_device_t* device, const dd_graphics_render_pass_descriptor_t* in_rpd,
    VkRenderPass in_render_pass);

void vulkan_init(vulkan_api_device_t* api_device, struct dd_allocator* allocator,
                 const char** in_required_instance_extensions /* array */,
                 const char** in_required_device_extensions /* array */);
void vulkan_instance_destroy(vulkan_api_device_t* api_device);
void vulkan_create_graphics_pipeline(vulkan_api_device_t* device, VkRenderPass render_pass,
                                     struct vulkan_pipeline_t* in_out_pipeline);
void vulkan_create_compute_pipeline(vulkan_api_device_t* device,
                                    struct vulkan_pipeline_t* in_out_pipeline);

uint32 vulkan_commandbuffer_enqueue_query_timer(vulkan_api_device_t* api_device,
                                                struct vulkan_commandbuffer_t* cmd_buffer);
uint64 vulkan_query_read_results_and_retire(vulkan_api_device_t* api_device, uint32 query_index);
void vulkan_device_retire_commandbuffer(vulkan_api_device_t* device,
                                        struct vulkan_commandbuffer_t* commandbuffer);
void vulkan_renderpass_clear_cache(vulkan_api_device_t* device);
VkFormat dd_vk_vulkan_pixel_format(uint32 format);