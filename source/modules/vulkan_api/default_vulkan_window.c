#include <assert.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vulkan/vk_sdk_platform.h>
#include <vulkan/vulkan.h>

#include "core/containers.h"
#include "core/memory.h"
#include "core/profiler.h"
#include "core/timer.h"
#include "core/utils.h"
#include "graphics_api/graphics.h"
#include "vk_window_surface.h"
#include "vulkan_types.inl"
#define APP_SHORT_NAME "vkcube"

// std::cout << "Fatal : VkResult is \"" << vks::tools::errorString(res) << "\" in " << __FILE__
//          << " at line " << __LINE__ << std::endl;
#define VK_CHECK_RESULT(f)       \
  {                              \
    VkResult res = (f);          \
    if (res != VK_SUCCESS) {     \
      assert(res == VK_SUCCESS); \
    }                            \
  }

#define U_ASSERT_ONLY

#define UNUSED

#define ERR_EXIT(err_msg, err_class)                                         \
  do {                                                                       \
    if (!demo->suppress_popups) MessageBox(NULL, err_msg, err_class, MB_OK); \
    exit(1);                                                                 \
  } while (0)

#define GET_INSTANCE_PROC_ADDR(inst, entrypoint)                                              \
  {                                                                                           \
    demo->fp##entrypoint = (PFN_vk##entrypoint)vkGetInstanceProcAddr(inst, "vk" #entrypoint); \
    if (demo->fp##entrypoint == NULL) {                                                       \
      ERR_EXIT("vkGetInstanceProcAddr failed to find vk" #entrypoint,                         \
               "vkGetInstanceProcAddr Failure");                                              \
    }                                                                                         \
  }

static PFN_vkGetDeviceProcAddr g_gdpa = NULL;

#define GET_DEVICE_PROC_ADDR(dev, entrypoint)                                         \
  {                                                                                   \
    if (!g_gdpa)                                                                      \
      g_gdpa = (PFN_vkGetDeviceProcAddr)vkGetInstanceProcAddr(demo->device.inst,      \
                                                              "vkGetDeviceProcAddr"); \
    demo->fp##entrypoint = (PFN_vk##entrypoint)g_gdpa(dev, "vk" #entrypoint);         \
    if (demo->fp##entrypoint == NULL) {                                               \
      ERR_EXIT("vkGetDeviceProcAddr failed to find vk" #entrypoint,                   \
               "vkGetDeviceProcAddr Failure");                                        \
    }                                                                                 \
  }

#include "demo.h"

extern struct demo g_demo;
struct DefaultVulkanLoop g_loop = {0};

static struct {
  void (*init)();
  void (*deinit)();
  dd_graphics_api_helper_draw_callback draw;
  dd_graphics_api_helper_compute_callback compute;
} g_vulkan_helper = {0};

extern dd_graphics_api_t dd_vulkan;
extern struct vulkan_commandbuffers_frame_t* g_frame_data_in_flight; /* array */
extern VkDescriptorPool g_active_descriptor_pool;

// Forward declarations:
static void vulkan_default_loop_resize(struct demo* demo);
static void vulkan_default_loop_draw(struct demo* demo, struct DefaultVulkanLoop* loop);
static void vulkan_default_loop_prepare_swapchain_buffers(struct demo* demo);
static void vulkan_default_loop_destroy_swapchain_resources(struct demo* demo);

void vulkan_frame_start(vulkan_api_device_t* device) {
  static uint32 frame_count = 0;
  frame_count++;
  // Check for any finished frames
  uint32 frame = 0;
  while (frame < array_count(g_frame_data_in_flight)) {
    VkFence f  = g_frame_data_in_flight[frame].fence;
    VkResult r = vkGetFenceStatus(device->logical_device.device, f);
    if (r != VK_NOT_READY) {
      struct vulkan_commandbuffer_t* buffer = g_frame_data_in_flight[frame].buffer;
      vulkan_device_retire_commandbuffer(device, buffer);

      VkDescriptorPool dp = g_frame_data_in_flight[frame].descriptor_pool;
      for (uint32 i = 0; i < array_count(device->descriptor_pools_used); i++) {
        if (device->descriptor_pools_used[i] == dp) {
          array_remove_at_index(device->descriptor_pools_used, i);
          array_push(device->descriptor_pools_free, dp, device->allocator);
          break;
        }
      }
      ++frame;
    } else {
      break;
    }
  }

  // Remove frames
  if (frame > 0) {
    memmove(g_frame_data_in_flight, g_frame_data_in_flight + frame,
            sizeof(*g_frame_data_in_flight) * (array_count(g_frame_data_in_flight) - frame));
    array_header(g_frame_data_in_flight)->count_ -= frame;
  }
}
void vulkan_frame_end() {}

// Create semaphores to synchronize acquiring presentable buffers before
// rendering and waiting for drawing to be complete before presenting
static void vulkan_default_loop_create_semaphores(struct vulkan_api_device_t* device,
                                                  struct demo* demo,
                                                  struct DefaultVulkanLoop* loop) {
  VkSemaphoreCreateInfo semaphoreCreateInfo = {
      .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
      .pNext = NULL,
      .flags = 0,
  };

  // Create fences that we can use to throttle if we get too far
  // ahead of the image presents
  VkFenceCreateInfo fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
                                .pNext = NULL,
                                .flags = VK_FENCE_CREATE_SIGNALED_BIT};
  for (uint32_t i = 0; i < FRAME_LAG; i++) {
    VK_CHECK_RESULT(
        vkCreateFence(device->logical_device.device, &fence_ci, NULL, &demo->fences[i]));
    VK_CHECK_RESULT(vkCreateSemaphore(device->logical_device.device, &semaphoreCreateInfo, NULL,
                                      &loop->image_acquired_semaphores[i]));
    VK_CHECK_RESULT(vkCreateSemaphore(device->logical_device.device, &semaphoreCreateInfo, NULL,
                                      &loop->draw_complete_semaphores[i]));
    if (demo->separate_present_queue) {
      VK_CHECK_RESULT(vkCreateSemaphore(device->logical_device.device, &semaphoreCreateInfo, NULL,
                                        &loop->image_ownership_semaphores[i]));
    }
  }
}

void demo_run() {
  if (!g_demo.prepared) return;

  vulkan_default_loop_draw(&g_demo, &g_loop);
  g_demo.curFrame++;
}

void demo_extern_resize(uint32 width, uint32 height) {
  if (width != g_loop.width || height != g_loop.height) {
    g_loop.width  = width;
    g_loop.height = height;
    vulkan_default_loop_resize(&g_demo);
  }
}

static void demo_prepare(struct demo* demo) {
  vulkan_default_loop_prepare_swapchain_buffers(demo);

  if (demo->is_minimized) {
    demo->prepared = false;
    return;
  }
}

static void vulkan_default_loop_destroy_swapchain_resources(struct demo* demo) {
  for (uint32 i = 0; i < demo->device.logical_device.cached_framebuffers.count; i++) {
    vkDestroyFramebuffer(demo->device.logical_device.device,
                         demo->device.logical_device.cached_framebuffers.framebuffers[i], NULL);
  }
  demo->device.logical_device.cached_framebuffers.count = 0;

  for (unsigned i = 0; i < demo->swapchainImageCount; i++) {
    vkDestroyImageView(demo->device.logical_device.device, demo->swapchain_image_resources[i].view,
                       NULL);
  }
  dd_free(demo->device.allocator, demo->swapchain_image_resources);
  demo->swapchain_image_resources = NULL;
}

// TODO: change depth to use standard vulkan functions
static void demo_destroy_depth(struct demo* demo) {
  vkDestroyImageView(demo->device.logical_device.device, demo->depth->view, NULL);
  vkDestroyImage(demo->device.logical_device.device, demo->depth->image, NULL);
  vkFreeMemory(demo->device.logical_device.device, demo->depth->mem,
               &demo->device.vk_allocator_callbacks);
  dd_free(demo->device.allocator, demo->depth);
  demo->depth = NULL;
}

static void vulkan_default_loop_resize(struct demo* demo) {
  // Don't react to resize until after first initialization.
  if (!demo->prepared) {
    if (demo->is_minimized) {
      demo_prepare(demo);
    }
    return;
  }
  // In order to properly resize the window, we must re-create the swapchain
  // AND redo the command buffers, etc.
  //
  // First, perform part of the demo_cleanup() function:
  demo->prepared = false;
  vkDeviceWaitIdle(demo->device.logical_device.device);

  // Assume the format for the swapchain doesn't change, only the size, in which case renderpass
  // can stay valid
  // vkDestroyRenderPass(demo->device.logical_device.device, demo->render_pass, NULL);

  demo_destroy_depth(demo);
  vulkan_default_loop_destroy_swapchain_resources(demo);
  vulkan_renderpass_clear_cache(&demo->device);

  // Second, re-perform the demo_prepare() function, which will re-create the
  // swapchain:
  dd_graphics_texture_descriptor_t td = {.width           = g_loop.width,
                                         .height          = g_loop.height,
                                         .layers          = 1,
                                         .mip_level_count = 1,
                                         .format          = DEPTH_U16_NORMALIZED,
                                         .storage_mode    = DEVICE_LOCAL,
                                         .usage           = TEXTURE_USAGE_RENDER_TARGET};
  dd_graphics_device_t d              = dd_vulkan_create_device();
  g_demo.depth                        = (struct vulkan_texture_t*)dd_vulkan.texture.create(d, &td);

  demo_prepare(demo);
  demo->prepared = true;
}

int WINAPI demo_init_vulkan(dd_graphics_window_t* in_window, struct dd_allocator* allocator) {
  memset(&g_demo, 0, sizeof(g_demo));
  g_demo.is_minimized = false;
  g_loop.width        = in_window->size.width;
  g_loop.height       = in_window->size.height;

  g_demo.window      = *in_window;
  g_demo.quit        = false;
  g_demo.curFrame    = 0;
  g_demo.frame_index = 0;

  uint8 stack_buffer[5000];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));
  const char** required_instance_extensions = {0};
  const char** required_device_extensions   = {0};

  /* Extensions needed to be able to create a swapchain
   */
  array_push(required_instance_extensions, VK_KHR_SURFACE_EXTENSION_NAME, stack_allocator);
  array_push(required_instance_extensions, VK_KHR_WIN32_SURFACE_EXTENSION_NAME, stack_allocator);
  array_push(required_device_extensions, VK_KHR_SWAPCHAIN_EXTENSION_NAME, stack_allocator);

  /* VK_EXT_descriptor_indexing
   * https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VK_EXT_descriptor_indexing.html
   * http://roar11.com/2019/06/vulkan-textures-unbound/
   * This extension is needed to allow a single drawcall to index into for example different
   * textures in an array of textures in the shader.
   * If this extension isn't enabled, then trying to index like that won't give any error or
   * warning, but the output will silently be incorrect.
   */
  array_push(required_instance_extensions, VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME,
             stack_allocator);
  array_push(required_device_extensions, VK_KHR_MAINTENANCE3_EXTENSION_NAME, stack_allocator);
  array_push(required_device_extensions, VK_EXT_DESCRIPTOR_INDEXING_EXTENSION_NAME,
             stack_allocator);

  vulkan_init(&g_demo.device, allocator, required_instance_extensions, required_device_extensions);

  struct demo* demo = &g_demo;
  GET_INSTANCE_PROC_ADDR(g_demo.device.inst, GetPhysicalDeviceSurfaceSupportKHR);
  GET_INSTANCE_PROC_ADDR(g_demo.device.inst, GetPhysicalDeviceSurfaceCapabilitiesKHR);
  GET_INSTANCE_PROC_ADDR(g_demo.device.inst, GetPhysicalDeviceSurfacePresentModesKHR);
  GET_INSTANCE_PROC_ADDR(g_demo.device.inst, GetSwapchainImagesKHR);

  g_demo.surface = dd_vk_window_surface_create_present_surface(&g_demo.device, &g_demo.window);
  g_demo.surface_format = dd_vk_window_surface_choose_color_format(&g_demo.device, g_demo.surface,
                                                                   VK_FORMAT_B8G8R8A8_UNORM);

  vulkan_create_logical_device(&g_demo, QueueTypePresent | QueueTypeGraphics | QueueTypeCompute);
  vulkan_default_loop_create_semaphores(&g_demo.device, &g_demo, &g_loop);

  GET_DEVICE_PROC_ADDR(demo->device.logical_device.device, CreateSwapchainKHR);
  GET_DEVICE_PROC_ADDR(demo->device.logical_device.device, DestroySwapchainKHR);
  GET_DEVICE_PROC_ADDR(demo->device.logical_device.device, GetSwapchainImagesKHR);
  GET_DEVICE_PROC_ADDR(demo->device.logical_device.device, AcquireNextImageKHR);
  GET_DEVICE_PROC_ADDR(demo->device.logical_device.device, QueuePresentKHR);

  dd_graphics_texture_descriptor_t td = {.width           = g_loop.width,
                                         .height          = g_loop.height,
                                         .layers          = 1,
                                         .mip_level_count = 1,
                                         .format          = DEPTH_U16_NORMALIZED,
                                         .storage_mode    = DEVICE_LOCAL,
                                         .usage           = TEXTURE_USAGE_RENDER_TARGET};
  dd_graphics_device_t d              = dd_vulkan_create_device();
  g_demo.depth                        = (struct vulkan_texture_t*)dd_vulkan.texture.create(d, &td);

  demo_prepare(&g_demo);

  return 0;
}

static void vulkan_default_loop_prepare_swapchain_buffers(struct demo* demo) {
  VkSwapchainKHR oldSwapchain = demo->swapchain;

  // Check the surface capabilities and formats
  VkSurfaceCapabilitiesKHR surfCapabilities;
  VK_CHECK_RESULT(demo->fpGetPhysicalDeviceSurfaceCapabilitiesKHR(
      demo->device.physical_device.device, demo->surface, &surfCapabilities));

  uint32_t presentModeCount;
  VK_CHECK_RESULT(demo->fpGetPhysicalDeviceSurfacePresentModesKHR(
      demo->device.physical_device.device, demo->surface, &presentModeCount, NULL));
  VkPresentModeKHR* presentModes =
      dd_alloc(demo->device.allocator, presentModeCount * sizeof(VkPresentModeKHR), 1);
  assert(presentModes);
  VK_CHECK_RESULT(demo->fpGetPhysicalDeviceSurfacePresentModesKHR(
      demo->device.physical_device.device, demo->surface, &presentModeCount, presentModes));

  VkExtent2D swapchainExtent;
  // width and height are either both 0xFFFFFFFF, or both not 0xFFFFFFFF.
  if (surfCapabilities.currentExtent.width == 0xFFFFFFFF) {
    // If the surface size is undefined, the size is set to the size
    // of the images requested, which must fit within the minimum and
    // maximum values.
    swapchainExtent.width  = g_loop.width;
    swapchainExtent.height = g_loop.height;

    if (swapchainExtent.width < surfCapabilities.minImageExtent.width) {
      swapchainExtent.width = surfCapabilities.minImageExtent.width;
    } else if (swapchainExtent.width > surfCapabilities.maxImageExtent.width) {
      swapchainExtent.width = surfCapabilities.maxImageExtent.width;
    }

    if (swapchainExtent.height < surfCapabilities.minImageExtent.height) {
      swapchainExtent.height = surfCapabilities.minImageExtent.height;
    } else if (swapchainExtent.height > surfCapabilities.maxImageExtent.height) {
      swapchainExtent.height = surfCapabilities.maxImageExtent.height;
    }
  } else {
    // If the surface size is defined, the swap chain size must match
    swapchainExtent = surfCapabilities.currentExtent;
    g_loop.width    = surfCapabilities.currentExtent.width;
    g_loop.height   = surfCapabilities.currentExtent.height;
  }

  if (g_loop.width == 0 || g_loop.height == 0) {
    demo->is_minimized = true;
    return;
  } else {
    demo->is_minimized = false;
  }

  // The FIFO present mode is guaranteed by the spec to be supported
  // and to have no tearing.  It's a great default present mode to use.
  VkPresentModeKHR swapchainPresentMode = VK_PRESENT_MODE_FIFO_KHR;

  // Determine the number of VkImages to use in the swap chain.
  // Application desires to acquire 3 images at a time for triple
  // buffering
  uint32_t desiredNumOfSwapchainImages = 3;
  if (desiredNumOfSwapchainImages < surfCapabilities.minImageCount) {
    desiredNumOfSwapchainImages = surfCapabilities.minImageCount;
  }
  // If maxImageCount is 0, we can ask for as many images as we want;
  // otherwise we're limited to maxImageCount
  if ((surfCapabilities.maxImageCount > 0) &&
      (desiredNumOfSwapchainImages > surfCapabilities.maxImageCount)) {
    // Application must settle for fewer images than desired:
    desiredNumOfSwapchainImages = surfCapabilities.maxImageCount;
  }

  VkSurfaceTransformFlagsKHR preTransform;
  if (surfCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
    preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
  } else {
    preTransform = surfCapabilities.currentTransform;
  }

  // Find a supported composite alpha mode - one of these is guaranteed to be set
  VkCompositeAlphaFlagBitsKHR compositeAlpha         = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
  VkCompositeAlphaFlagBitsKHR compositeAlphaFlags[4] = {
      VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
      VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR,
      VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR,
      VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
  };
  for (uint32_t i = 0; i < 4; i++) {
    if (surfCapabilities.supportedCompositeAlpha & compositeAlphaFlags[i]) {
      compositeAlpha = compositeAlphaFlags[i];
      break;
    }
  }

  VkSwapchainCreateInfoKHR swapchain_ci = {
      .sType           = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
      .pNext           = NULL,
      .surface         = demo->surface,
      .minImageCount   = desiredNumOfSwapchainImages,
      .imageFormat     = demo->surface_format.format,
      .imageColorSpace = demo->surface_format.colorSpace,
      .imageExtent =
          {
              .width  = swapchainExtent.width,
              .height = swapchainExtent.height,
          },
      .imageUsage            = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
      .preTransform          = preTransform,
      .compositeAlpha        = compositeAlpha,
      .imageArrayLayers      = 1,
      .imageSharingMode      = VK_SHARING_MODE_EXCLUSIVE,
      .queueFamilyIndexCount = 0,
      .pQueueFamilyIndices   = NULL,
      .presentMode           = swapchainPresentMode,
      .oldSwapchain          = oldSwapchain,
      .clipped               = true,
  };
  uint32_t i;
  VK_CHECK_RESULT(demo->fpCreateSwapchainKHR(demo->device.logical_device.device, &swapchain_ci,
                                             NULL, &demo->swapchain));

  // If we just re-created an existing swapchain, we should destroy the old
  // swapchain at this point.
  // Note: destroying the swapchain also cleans up all its associated
  // presentable images once the platform is done with them.
  if (oldSwapchain != VK_NULL_HANDLE) {
    demo->fpDestroySwapchainKHR(demo->device.logical_device.device, oldSwapchain, NULL);
  }

  VK_CHECK_RESULT(demo->fpGetSwapchainImagesKHR(
      demo->device.logical_device.device, demo->swapchain, &demo->swapchainImageCount, NULL));
  assert(demo->swapchainImageCount <= NUM_CUBE_BUFFERS);

  VkImage* swapchainImages =
      dd_alloc(demo->device.allocator, demo->swapchainImageCount * sizeof(VkImage), 1);
  assert(swapchainImages);
  VK_CHECK_RESULT(demo->fpGetSwapchainImagesKHR(demo->device.logical_device.device,
                                                demo->swapchain, &demo->swapchainImageCount,
                                                swapchainImages));

  demo->swapchain_image_resources = dd_alloc(
      demo->device.allocator, sizeof(SwapchainImageResources) * demo->swapchainImageCount, 1);
  assert(demo->swapchain_image_resources);
  memset(demo->swapchain_image_resources, 0,
         sizeof(SwapchainImageResources) * demo->swapchainImageCount);

  array_reset(demo->swapchain_images);

  for (i = 0; i < demo->swapchainImageCount; i++) {
    VkImageViewCreateInfo color_image_view = {
        .sType  = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext  = NULL,
        .format = demo->surface_format.format,
        .components =
            {
                .r = VK_COMPONENT_SWIZZLE_R,
                .g = VK_COMPONENT_SWIZZLE_G,
                .b = VK_COMPONENT_SWIZZLE_B,
                .a = VK_COMPONENT_SWIZZLE_A,
            },
        .subresourceRange = {.aspectMask     = VK_IMAGE_ASPECT_COLOR_BIT,
                             .baseMipLevel   = 0,
                             .levelCount     = 1,
                             .baseArrayLayer = 0,
                             .layerCount     = 1},
        .viewType         = VK_IMAGE_VIEW_TYPE_2D,
        .flags            = 0,
    };

    demo->swapchain_image_resources[i].image = swapchainImages[i];

    color_image_view.image = demo->swapchain_image_resources[i].image;

    VK_CHECK_RESULT(vkCreateImageView(demo->device.logical_device.device, &color_image_view, NULL,
                                      &demo->swapchain_image_resources[i].view));

    // Copy into Daedalus structures
    struct vulkan_texture_t to = {.format = demo->surface_format.format,
                                  .image  = demo->swapchain_image_resources[i].image,
                                  .view   = demo->swapchain_image_resources[i].view};
    array_push(demo->swapchain_images, to, demo->device.allocator);
  }

  if (NULL != swapchainImages) {
    dd_free(demo->device.allocator, swapchainImages);
  }

  if (NULL != presentModes) {
    dd_free(demo->device.allocator, presentModes);
  }
}

static void vulkan_default_loop_build_draw_commandbuffer(
    struct demo* demo, struct vulkan_commandbuffer_t* draw_commandbuffer, unsigned buffer_index) {
  vulkan_api_device_t* api_device = &demo->device;
  // Allow debug labels
  // VkDebugUtilsLabelEXT label  = {0};

  dd_graphics_render_pass_descriptor_t rpd = {.color  = {demo->swapchain_images + buffer_index},
                                              .depth  = demo->depth,
                                              .width  = g_loop.width,
                                              .height = g_loop.height,
                                              .is_for_present = true};
  if (g_vulkan_helper.draw) {
    g_vulkan_helper.draw((dd_graphics_commandbuffer_t){draw_commandbuffer}, &rpd);
  }

  if (demo->separate_present_queue) {
    struct vulkan_commandpool_t* command_pool = &api_device->logical_device.queues[1].command_pool;
    VkCommandBuffer cmd_present =
        command_pool->buffers_free[array_count(command_pool->buffers_free) - 1];
    array_pop(command_pool->buffers_free);

    const VkCommandBufferBeginInfo cmd_buf_info = {
        .sType            = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext            = NULL,
        .flags            = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = NULL,
    };
    VK_CHECK_RESULT(vkBeginCommandBuffer(cmd_present, &cmd_buf_info));

    VkImageMemoryBarrier image_ownership_barrier = {
        .sType               = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext               = NULL,
        .srcAccessMask       = 0,
        .dstAccessMask       = 0,
        .oldLayout           = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        .newLayout           = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
        .srcQueueFamilyIndex = api_device->graphics_queue->family_index,
        .dstQueueFamilyIndex = demo->present_queue->family_index,
        .image               = demo->swapchain_image_resources[buffer_index].image,
        .subresourceRange    = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1}};

    vkCmdPipelineBarrier(cmd_present, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1,
                         &image_ownership_barrier);
    VK_CHECK_RESULT(vkEndCommandBuffer(cmd_present));

    // We have to transfer ownership from the graphics queue family to the
    // present queue family to be able to present.  Note that we don't have
    // to transfer from present queue family back to graphics queue family at
    // the start of the next frame because we don't care about the image's
    // contents at that point.

    vkCmdPipelineBarrier(draw_commandbuffer->buffer, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, NULL, 0, NULL, 1,
                         &image_ownership_barrier);
  }
}

static void vulkan_default_loop_draw(struct demo* demo, struct DefaultVulkanLoop* loop) {
  VkResult err;
  dd_prof_thread_init("main");

  vulkan_api_device_t* api_device = &demo->device;

  // Ensure no more than FRAME_LAG renderings are outstanding
  vkWaitForFences(demo->device.logical_device.device, 1, &demo->fences[demo->frame_index], VK_TRUE,
                  UINT64_MAX);

  vulkan_frame_start(&demo->device);

  vkResetFences(demo->device.logical_device.device, 1, &demo->fences[demo->frame_index]);

  uint32_t buffer_index;
  do {
    // Get the index of the next available swapchain image:
    err = demo->fpAcquireNextImageKHR(
        demo->device.logical_device.device, demo->swapchain, UINT64_MAX,
        loop->image_acquired_semaphores[demo->frame_index], VK_NULL_HANDLE, &buffer_index);

    if (err == VK_ERROR_OUT_OF_DATE_KHR) {
      // demo->swapchain is out of date (e.g. the window was resized) and
      // must be recreated:
      vulkan_default_loop_resize(demo);
    } else if (err == VK_SUBOPTIMAL_KHR) {
      // demo->swapchain is not as optimal as it could be, but the platform's
      // presentation engine will still present the image correctly.
      break;
    } else if (err == VK_ERROR_SURFACE_LOST_KHR) {
      vkDestroySurfaceKHR(demo->device.inst, demo->surface, NULL);
      demo->surface = dd_vk_window_surface_create_present_surface(&demo->device, &demo->window);
      vulkan_default_loop_resize(demo);
    } else {
      assert(!err);
    }
  } while (err != VK_SUCCESS);

  dd_graphics_queue_t graphics_queue = dd_vulkan.device.get_queue(api_device, QueueTypeGraphics);

  // TODO(Rick): Don't hard code this, need to re-architect the whole thing.
  if (g_vulkan_helper.compute) {
    dd_graphics_commandbuffer_t compute_cmd   = dd_vulkan.queue.get_command_buffer(graphics_queue);
    struct vulkan_commandbuffer_t* vulkan_cmd = ((struct vulkan_commandbuffer_t*)compute_cmd.ptr);
    g_vulkan_helper.compute(compute_cmd);
    // Manually end, submit and manage
    vkEndCommandBuffer(vulkan_cmd->buffer);

    VkSubmitInfo submit_info;
    submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext                = NULL;
    submit_info.waitSemaphoreCount   = 0;
    submit_info.commandBufferCount   = 1;
    submit_info.pCommandBuffers      = &vulkan_cmd->buffer;
    submit_info.signalSemaphoreCount = 0;
    VK_CHECK_RESULT(vkQueueSubmit(api_device->compute_queue->handle, 1, &submit_info, NULL));
  }

  dd_graphics_commandbuffer_t graphics_cmd  = dd_vulkan.queue.get_command_buffer(graphics_queue);
  struct vulkan_commandbuffer_t* vulkan_cmd = ((struct vulkan_commandbuffer_t*)graphics_cmd.ptr);

  vulkan_default_loop_build_draw_commandbuffer(
      demo, (struct vulkan_commandbuffer_t*){graphics_cmd.ptr}, buffer_index);

  struct vulkan_queue_t* queue = (struct vulkan_queue_t*)graphics_queue.ptr;

  vulkan_cmd->timer_query[1] = vulkan_commandbuffer_enqueue_query_timer(queue->device, vulkan_cmd);

  // Manually end, submit and manage
  vkEndCommandBuffer(vulkan_cmd->buffer);

  // Wait for the image acquired semaphore to be signaled to ensure
  // that the image won't be rendered to until the presentation
  // engine has fully released ownership to the application, and it is
  // okay to render to the image.
  VkPipelineStageFlags pipe_stage_flags;
  VkSubmitInfo submit_info;
  submit_info.sType                = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submit_info.pNext                = NULL;
  submit_info.pWaitDstStageMask    = &pipe_stage_flags;
  pipe_stage_flags                 = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  submit_info.waitSemaphoreCount   = 1;
  submit_info.pWaitSemaphores      = &loop->image_acquired_semaphores[demo->frame_index];
  submit_info.commandBufferCount   = 1;
  submit_info.pCommandBuffers      = &vulkan_cmd->buffer;
  submit_info.signalSemaphoreCount = 1;
  submit_info.pSignalSemaphores    = &loop->draw_complete_semaphores[demo->frame_index];
  VK_CHECK_RESULT(vkQueueSubmit(api_device->graphics_queue->handle, 1, &submit_info,
                                demo->fences[demo->frame_index]));

  array_push(api_device->graphics_queue->command_pool.pending_buffers, vulkan_cmd,
             api_device->allocator);

  struct vulkan_commandbuffers_frame_t cbf = {.fence           = demo->fences[demo->frame_index],
                                              .buffer          = vulkan_cmd,
                                              .descriptor_pool = g_active_descriptor_pool};

  if (demo->separate_present_queue) {
    struct vulkan_commandpool_t* command_pool = &api_device->logical_device.queues[1].command_pool;
    VkCommandBuffer cmd_buffer_present =
        command_pool->buffers_free[--(array_header(command_pool->buffers_free)->count_)];
    cbf.buffer_present = cmd_buffer_present;

    // If we are using separate queues, change image ownership to the
    // present queue before presenting, waiting for the draw complete
    // semaphore and signalling the ownership released semaphore when finished
    VkFence nullFence                = VK_NULL_HANDLE;
    pipe_stage_flags                 = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    submit_info.waitSemaphoreCount   = 1;
    submit_info.pWaitSemaphores      = &loop->draw_complete_semaphores[demo->frame_index];
    submit_info.commandBufferCount   = 1;
    submit_info.pCommandBuffers      = &cmd_buffer_present;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores    = &loop->image_ownership_semaphores[demo->frame_index];
    VK_CHECK_RESULT(vkQueueSubmit(demo->present_queue->handle, 1, &submit_info, nullFence));
  }

  array_push(g_frame_data_in_flight, cbf, demo->device.allocator);

  // If we are using separate queues we have to wait for image ownership,
  // otherwise wait for draw complete
  VkPresentInfoKHR present = {
      .sType              = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
      .pNext              = NULL,
      .waitSemaphoreCount = 1,
      .pWaitSemaphores    = (demo->separate_present_queue)
                             ? &loop->image_ownership_semaphores[demo->frame_index]
                             : &loop->draw_complete_semaphores[demo->frame_index],
      .swapchainCount = 1,
      .pSwapchains    = &demo->swapchain,
      .pImageIndices  = &buffer_index,
  };

  err = demo->fpQueuePresentKHR(demo->present_queue->handle, &present);
  demo->frame_index += 1;
  demo->frame_index %= FRAME_LAG;

  if (err == VK_ERROR_OUT_OF_DATE_KHR) {
    // demo->swapchain is out of date (e.g. the window was resized) and
    // must be recreated:
    vulkan_default_loop_resize(demo);
  } else if (err == VK_SUBOPTIMAL_KHR) {
    // demo->swapchain is not as optimal as it could be, but the platform's
    // presentation engine will still present the image correctly.
  } else if (err == VK_ERROR_SURFACE_LOST_KHR) {
    vkDestroySurfaceKHR(demo->device.inst, demo->surface, NULL);
    demo->surface = dd_vk_window_surface_create_present_surface(&demo->device, &demo->window);
    vulkan_default_loop_resize(demo);
  } else {
    assert(!err);
  }

  vulkan_frame_end();
}

static void demo_cleanup(struct demo* demo, struct DefaultVulkanLoop* loop) {
  vulkan_api_device_t* api_device = &demo->device;

  demo->prepared = false;
  vkDeviceWaitIdle(demo->device.logical_device.device);

  // Wait for fences from present operations
  for (uint32 i = 0; i < FRAME_LAG; i++) {
    vkWaitForFences(demo->device.logical_device.device, 1, &demo->fences[i], VK_TRUE, UINT64_MAX);
    vkDestroyFence(demo->device.logical_device.device, demo->fences[i], NULL);
    vkDestroySemaphore(demo->device.logical_device.device, loop->image_acquired_semaphores[i],
                       NULL);
    vkDestroySemaphore(demo->device.logical_device.device, loop->draw_complete_semaphores[i],
                       NULL);
    if (demo->separate_present_queue) {
      vkDestroySemaphore(demo->device.logical_device.device, loop->image_ownership_semaphores[i],
                         NULL);
    }
  }

  g_vulkan_helper.deinit();

  // If the window is currently minimized, demo_resize has already done some cleanup for us.
  if (!demo->is_minimized) {
    vulkan_default_loop_destroy_swapchain_resources(demo);

    vulkan_destroy_logical_device(&demo->device);

    for (uint32 i = 0; i < demo->device.logical_device.cached_render_passes.count; i++) {
      vkDestroyRenderPass(demo->device.logical_device.device,
                          demo->device.logical_device.cached_render_passes.passes[i], NULL);
    }

    demo->fpDestroySwapchainKHR(demo->device.logical_device.device, demo->swapchain, NULL);

    demo_destroy_depth(demo);

    dd_free(api_device->allocator, api_device->physical_device.queue_family.props);

    for (uint32 i = 0; i < VULKAN_MAX_QUEUES; i++) {
      if (api_device->logical_device.queues[i].command_pool.pool) {
        vkDestroyCommandPool(api_device->logical_device.device,
                             api_device->logical_device.queues[i].command_pool.pool, NULL);
      }
    }
  }
  vkDeviceWaitIdle(api_device->logical_device.device);
  vkDestroyDevice(api_device->logical_device.device, NULL);
  vkDestroySurfaceKHR(api_device->inst, demo->surface, NULL);

  vulkan_instance_destroy(api_device);
}

#if defined(_WIN32)

void dd_helper_set_functions(void (*init)(), void (*deinit)(),
                             dd_graphics_api_helper_draw_callback draw_cb,
                             dd_graphics_api_helper_compute_callback compute_cb) {
  g_vulkan_helper.init    = init;
  g_vulkan_helper.deinit  = deinit;
  g_vulkan_helper.draw    = draw_cb;
  g_vulkan_helper.compute = compute_cb;
}

#endif

extern dd_graphics_window_t g_window;
int dd_vk_main_loop(dd_graphics_window_t* window, struct dd_allocator* allocator) {
  demo_init_vulkan(window, allocator);
  g_vulkan_helper.init();
  g_demo.prepared = true;

  MSG msg;            // message
  bool done = false;  // flag saying when app is complete

  // Ensure wParam is initialized.
  msg.wParam = 0;

  // main message loop
  while (!done) {
    PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);
    if (msg.message == WM_QUIT)  // check for a quit message
    {
      WINDOWPLACEMENT placement;
      BOOL res = GetWindowPlacement(window->handle, &placement);
      if (res) {
        g_window.position.x = placement.rcNormalPosition.left;
        g_window.position.y = placement.rcNormalPosition.top;
        g_window.size.width = (placement.rcNormalPosition.right - placement.rcNormalPosition.left);
        g_window.size.height =
            (placement.rcNormalPosition.bottom - placement.rcNormalPosition.top);
      } else {
        // Some error getting the placement
        char err[256];
        memset(err, 0, 256);
        FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
        dd_devprintf("Unable to GetWindowPlacement : %s\n", err);
      }
      done = true;  // if found, quit app
    } else {
      /* Translate and dispatch to event queue*/
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }
    RedrawWindow(window->handle, NULL, NULL, RDW_INTERNALPAINT);
  }

  demo_cleanup(&g_demo, &g_loop);

  return (int)msg.wParam;
}
