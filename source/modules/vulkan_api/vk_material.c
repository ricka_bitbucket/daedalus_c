#include <assert.h>

#include "core/containers.h"
#include "vulkan_types.inl"

enum { MAX_DYNAMIC_STATES = 4 };

VkPrimitiveTopology vulkan_topology(ETopologyType type) {
  const VkPrimitiveTopology topologies[] = {
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, VK_PRIMITIVE_TOPOLOGY_POINT_LIST,
      VK_PRIMITIVE_TOPOLOGY_LINE_LIST, VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP};
  return topologies[(uint32)type];
}

void vulkan_create_graphics_pipeline(vulkan_api_device_t* device, VkRenderPass render_pass,
                                     struct vulkan_pipeline_t* in_out_pipeline) {
  VkGraphicsPipelineCreateInfo pipeline_ci;
  VkPipelineVertexInputStateCreateInfo vi;
  VkPipelineInputAssemblyStateCreateInfo ia;
  VkPipelineRasterizationStateCreateInfo rs;
  VkPipelineColorBlendStateCreateInfo cb;
  VkPipelineDepthStencilStateCreateInfo ds;
  VkPipelineViewportStateCreateInfo vp;
  VkPipelineMultisampleStateCreateInfo ms;
  VkDynamicState dynamicStateEnables[MAX_DYNAMIC_STATES];
  VkPipelineDynamicStateCreateInfo dynamicState;

  memset(dynamicStateEnables, 0, sizeof dynamicStateEnables);
  memset(&dynamicState, 0, sizeof dynamicState);
  dynamicState.sType          = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
  dynamicState.pDynamicStates = dynamicStateEnables;

  memset(&pipeline_ci, 0, sizeof(pipeline_ci));
  pipeline_ci.sType  = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
  pipeline_ci.layout = in_out_pipeline->pipeline_layout;

  memset(&vi, 0, sizeof(vi));
  vi.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

  memset(&ia, 0, sizeof(ia));
  ia.sType    = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
  ia.topology = vulkan_topology(in_out_pipeline->vmp.topology);

  memset(&rs, 0, sizeof(rs));
  rs.sType                   = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
  rs.polygonMode             = VK_POLYGON_MODE_FILL;
  rs.cullMode                = VK_CULL_MODE_NONE;
  rs.frontFace               = VK_FRONT_FACE_COUNTER_CLOCKWISE;
  rs.depthClampEnable        = VK_FALSE;
  rs.rasterizerDiscardEnable = VK_FALSE;
  rs.depthBiasEnable         = VK_FALSE;
  rs.lineWidth               = 1.0f;

  memset(&cb, 0, sizeof(cb));
  cb.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
  VkPipelineColorBlendAttachmentState att_state[1];
  memset(att_state, 0, sizeof(att_state));
  att_state[0].colorWriteMask      = 0xf;
  att_state[0].blendEnable         = in_out_pipeline->vmp.enable_alpha_blend;
  att_state[0].srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
  att_state[0].dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
  att_state[0].colorBlendOp        = VK_BLEND_OP_ADD;

  cb.attachmentCount = 1;
  cb.pAttachments    = att_state;

  memset(&vp, 0, sizeof(vp));
  vp.sType         = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
  vp.viewportCount = 1;
  dynamicStateEnables[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_VIEWPORT;
  vp.scissorCount                                       = 1;
  dynamicStateEnables[dynamicState.dynamicStateCount++] = VK_DYNAMIC_STATE_SCISSOR;

  memset(&ds, 0, sizeof(ds));
  ds.sType                 = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
  ds.depthTestEnable       = VK_TRUE;
  ds.depthWriteEnable      = VK_TRUE;
  ds.depthCompareOp        = VK_COMPARE_OP_LESS_OR_EQUAL;
  ds.depthBoundsTestEnable = VK_FALSE;
  ds.back.failOp           = VK_STENCIL_OP_KEEP;
  ds.back.passOp           = VK_STENCIL_OP_KEEP;
  ds.back.compareOp        = VK_COMPARE_OP_ALWAYS;
  ds.stencilTestEnable     = VK_FALSE;
  ds.front                 = ds.back;

  memset(&ms, 0, sizeof(ms));
  ms.sType                = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
  ms.pSampleMask          = NULL;
  ms.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

  // Two stages: vs and fs
  VkPipelineShaderStageCreateInfo shaderStages[2];
  memset(&shaderStages, 0, 2 * sizeof(VkPipelineShaderStageCreateInfo));

  shaderStages[0].sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shaderStages[0].stage  = VK_SHADER_STAGE_VERTEX_BIT;
  shaderStages[0].module = in_out_pipeline->shaders[ShaderTypeVertex];
  shaderStages[0].pName  = "main";

  shaderStages[1].sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shaderStages[1].stage  = VK_SHADER_STAGE_FRAGMENT_BIT;
  shaderStages[1].module = in_out_pipeline->shaders[ShaderTypeFragment];
  shaderStages[1].pName  = "main";

  pipeline_ci.pVertexInputState   = &vi;
  pipeline_ci.pInputAssemblyState = &ia;
  pipeline_ci.pRasterizationState = &rs;
  pipeline_ci.pColorBlendState    = &cb;
  pipeline_ci.pMultisampleState   = &ms;
  pipeline_ci.pViewportState      = &vp;
  pipeline_ci.pDepthStencilState  = &ds;
  pipeline_ci.stageCount          = countof(shaderStages);
  pipeline_ci.pStages             = shaderStages;
  pipeline_ci.renderPass          = render_pass;
  pipeline_ci.pDynamicState       = &dynamicState;
  assert(dynamicState.dynamicStateCount <= MAX_DYNAMIC_STATES);

  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device->logical_device.device, VK_NULL_HANDLE, 1,
                                            &pipeline_ci, NULL, &in_out_pipeline->pipeline));
}

void vulkan_create_compute_pipeline(vulkan_api_device_t* device,
                                    struct vulkan_pipeline_t* in_out_pipeline) {
  VkComputePipelineCreateInfo pipeline_ci = {
      .sType  = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
      .stage  = {.sType  = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                .stage  = VK_SHADER_STAGE_COMPUTE_BIT,
                .module = in_out_pipeline->shaders[ShaderTypeCompute],
                .pName  = "main"},
      .layout = in_out_pipeline->pipeline_layout};

  VK_CHECK_RESULT(vkCreateComputePipelines(device->logical_device.device, VK_NULL_HANDLE, 1,
                                           &pipeline_ci, NULL, &in_out_pipeline->pipeline));
}

static VkShaderModule dd_vk_prepare_shader_module(vulkan_api_device_t* device, const uint8* code,
                                                  uint32 size) {
  VkShaderModule module;
  VkShaderModuleCreateInfo moduleCreateInfo;

  moduleCreateInfo.sType    = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
  moduleCreateInfo.pNext    = NULL;
  moduleCreateInfo.flags    = 0;
  moduleCreateInfo.codeSize = size;
  moduleCreateInfo.pCode    = (const uint32_t*)code;

  VK_CHECK_RESULT(
      vkCreateShaderModule(device->logical_device.device, &moduleCreateInfo, NULL, &module));

  return module;
}

void vulkan_create_material_prototype(
    vulkan_api_device_t* device,
    const dd_graphics_material_prototype_descriptor_t* in_material_descriptor,
    struct vulkan_pipeline_t* out_pipeline) {
  struct vulkan_materialprototype_t* out_material_prototype = &out_pipeline->vmp;
  const VkDescriptorType vulkan_type[]                      = {
      /* use sample as unknown, it shouldn't be used anyway */
      VK_DESCRIPTOR_TYPE_SAMPLER,

      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,

      /* use sample for push constant as that isn't an descriptor type */
      VK_DESCRIPTOR_TYPE_SAMPLER,
  };
  static_assert(countof(vulkan_type) == BindingTypeCount, "Missing binding type conversions");
  VkPushConstantRange pcr[4] = {0};
  uint32 pcr_count           = 0;

  uint8 stack_buffer[5000];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));
  VkDescriptorSetLayoutBinding* layout_bindings = {0}; /* array */
  for (unsigned i = 0; i < MaterialPrototypeMaxBindingCount; i++) {
    const EMaterialPrototypeBindingType binding_type = in_material_descriptor->bindings[i].type;
    if (binding_type != BindingTypeUnknown) {
      if (binding_type == BindingTypePushConstant) {
        VkPushConstantRange p = {
            .stageFlags = VK_SHADER_STAGE_ALL,
            .offset     = in_material_descriptor->bindings[i].props.push_constant.offset,
            .size       = in_material_descriptor->bindings[i].props.push_constant.range};
        pcr[pcr_count++] = p;
      } else {
        uint32 count = max(1, in_material_descriptor->bindings[i].props.array.count);
        VkDescriptorSetLayoutBinding binding = {
            .binding         = i,
            .descriptorType  = vulkan_type[binding_type],
            .descriptorCount = count,
            .stageFlags      = VK_SHADER_STAGE_ALL,
        };
        array_push(layout_bindings, binding, stack_allocator);
      }
    }
  };
  VkDescriptorSetLayoutCreateInfo descriptor_layout = {
      .sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = (uint32_t)array_count(layout_bindings),
      .pBindings    = layout_bindings};

  memcpy(out_material_prototype->bindings, in_material_descriptor->bindings,
         sizeof(in_material_descriptor->bindings));

  VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device->logical_device.device, &descriptor_layout,
                                              NULL, &out_pipeline->desc_layout));

  const VkPipelineLayoutCreateInfo pipeline_layout_ci = {
      .sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount         = 1,
      .pSetLayouts            = &out_pipeline->desc_layout,
      .pushConstantRangeCount = pcr_count,
      .pPushConstantRanges    = pcr};

  VK_CHECK_RESULT(vkCreatePipelineLayout(device->logical_device.device, &pipeline_layout_ci, NULL,
                                         &out_pipeline->pipeline_layout));

  VkShaderModule vertex_shader = dd_vk_prepare_shader_module(
      device, in_material_descriptor->vs_code, in_material_descriptor->vs_byte_count);
  VkShaderModule fragment_shader = dd_vk_prepare_shader_module(
      device, in_material_descriptor->fs_code, in_material_descriptor->fs_byte_count);

  out_pipeline->shaders[ShaderTypeVertex]   = vertex_shader;
  out_pipeline->shaders[ShaderTypeFragment] = fragment_shader;
  out_pipeline->vmp.topology                = in_material_descriptor->topology;
  out_pipeline->vmp.enable_alpha_blend      = in_material_descriptor->enable_alpha_blend;
}

void vulkan_create_compute_prototype(
    vulkan_api_device_t* device,
    const dd_graphics_compute_prototype_descriptor_t* in_compute_descriptor,
    struct vulkan_pipeline_t* out_pipeline) {
  struct vulkan_materialprototype_t* out_material_prototype = &out_pipeline->vmp;
  const VkDescriptorType vulkan_type[]                      = {
      /* use sample as unknown, it shouldn't be used anyway */
      VK_DESCRIPTOR_TYPE_SAMPLER,

      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
      VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,

      /* use sample for push constant as that isn't an descriptor type */
      VK_DESCRIPTOR_TYPE_SAMPLER,
  };
  static_assert(countof(vulkan_type) == BindingTypeCount, "Missing binding type conversions");
  VkPushConstantRange pcr[4] = {0};
  uint32 pcr_count           = 0;

  uint8 stack_buffer[5000];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));
  VkDescriptorSetLayoutBinding* layout_bindings = {0}; /* array */
  for (unsigned i = 0; i < MaterialPrototypeMaxBindingCount; i++) {
    const EMaterialPrototypeBindingType binding_type = in_compute_descriptor->bindings[i].type;
    if (binding_type != BindingTypeUnknown) {
      if (binding_type == BindingTypePushConstant) {
        VkPushConstantRange p = {
            .stageFlags = VK_SHADER_STAGE_ALL,
            .offset     = in_compute_descriptor->bindings[i].props.push_constant.offset,
            .size       = in_compute_descriptor->bindings[i].props.push_constant.range};
        pcr[pcr_count++] = p;
      } else {
        uint32 count = max(1, in_compute_descriptor->bindings[i].props.array.count);
        VkDescriptorSetLayoutBinding binding = {
            .binding         = i,
            .descriptorType  = vulkan_type[binding_type],
            .descriptorCount = count,
            .stageFlags      = VK_SHADER_STAGE_ALL,
        };
        array_push(layout_bindings, binding, stack_allocator);
      }
    }
  };
  VkDescriptorSetLayoutCreateInfo descriptor_layout = {
      .sType        = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
      .bindingCount = (uint32_t)array_count(layout_bindings),
      .pBindings    = layout_bindings};

  memcpy(out_material_prototype->bindings, in_compute_descriptor->bindings,
         sizeof(in_compute_descriptor->bindings));

  VK_CHECK_RESULT(vkCreateDescriptorSetLayout(device->logical_device.device, &descriptor_layout,
                                              NULL, &out_pipeline->desc_layout));

  const VkPipelineLayoutCreateInfo pipeline_layout_ci = {
      .sType                  = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
      .setLayoutCount         = 1,
      .pSetLayouts            = &out_pipeline->desc_layout,
      .pushConstantRangeCount = pcr_count,
      .pPushConstantRanges    = pcr};

  VK_CHECK_RESULT(vkCreatePipelineLayout(device->logical_device.device, &pipeline_layout_ci, NULL,
                                         &out_pipeline->pipeline_layout));

  VkShaderModule compute_shader = dd_vk_prepare_shader_module(
      device, in_compute_descriptor->compute_shader_data, in_compute_descriptor->byte_count);

  out_pipeline->shaders[ShaderTypeCompute] = compute_shader;
}
