#pragma once

#include <vulkan/vulkan.h>

struct vulkan_api_device_t;
struct dd_graphics_window_t;

struct VkSurfaceFormatKHR* /* array */ dd_vk_window_surface_get_supported_framebuffer_formats(
    struct vulkan_api_device_t* device, VkSurfaceKHR surface);

VkSurfaceKHR dd_vk_window_surface_create_present_surface(struct vulkan_api_device_t* in_device,
                                                         struct dd_graphics_window_t* in_window);
VkSurfaceFormatKHR dd_vk_window_surface_choose_color_format(struct vulkan_api_device_t* device,
                                                            VkSurfaceKHR surface,
                                                            VkFormat preferredFormat);
