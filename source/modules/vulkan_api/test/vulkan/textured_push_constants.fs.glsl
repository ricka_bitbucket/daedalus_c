#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// Constants
layout(binding = 3) uniform sampler2D tex;

// Inputs
layout(location = 0) in vec2 texcoord;

// Outputs
layout(location = 0) out vec4 uFragColor;

void main() {
  vec4 tex   = texture(tex, texcoord.xy);
  uFragColor = vec4(tex.rgb, 1);
}
