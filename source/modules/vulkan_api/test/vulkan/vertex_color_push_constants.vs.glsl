#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(std430, binding = 0) readonly buffer buf { vec2 positions[]; };
layout(push_constant) uniform data_t { float data[6]; }
data;

layout(location = 0) out vec3 frag_color;

void main() {
  uint vid = gl_VertexIndex;

  frag_color = vec3(data.data[2], data.data[3], data.data[4]);

  vec4 vert   = vec4(positions[vid], 0.5, 1);
  gl_Position = vec4(data.data[0], data.data[1], 0, 0) + vert;

  // Note that Vulkan NDC has +Y going down, so correct that here
  gl_Position.y = -gl_Position.y;
}
