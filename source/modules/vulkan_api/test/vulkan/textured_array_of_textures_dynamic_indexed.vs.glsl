#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#extension VK_EXT_descriptor_indexing : enable

layout(std430, binding = 0) readonly buffer buf { vec2 positions[]; };
layout(std430, binding = 1) readonly buffer buf2 { vec2 texcoords[]; };

// Outputs
layout(location = 0) out vec2 texcoord;
layout(location = 1) out uint texture_index;

void main() {
  uint vid = gl_VertexIndex;
  uint iid = gl_InstanceIndex;

  texture_index = iid % 128;
  texcoord      = texcoords[vid];

  gl_Position = vec4(positions[(iid * 6 + vid)], 0.5, 1);

  // Note that Vulkan NDC has +Y going down, so correct that here
  gl_Position.y = -gl_Position.y;
}
