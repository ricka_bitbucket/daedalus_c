#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(std430, binding = 0) readonly buffer buf { vec2 positions[]; };
layout(std430, binding = 1) readonly buffer buf2 { vec2 texcoords[]; };

// Outputs
layout(location = 0) out vec4 texcoord;

void main() {
  uint vid = gl_VertexIndex;

  texcoord = vec4(texcoords[vid], 0, 1);

  gl_Position = vec4(positions[vid], 0.5, 1);

  // Note that Vulkan NDC has +Y going down, so correct that here
  gl_Position.y = -gl_Position.y;
}
