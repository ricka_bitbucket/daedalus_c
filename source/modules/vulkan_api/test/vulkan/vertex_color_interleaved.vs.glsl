#version 430
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#extension GL_EXT_nonuniform_qualifier : enable

struct vertex {
  vec4 pos;
  vec4 color;
};

layout(std140, binding = 0) readonly buffer buf { vertex data[]; };

layout(location = 0) out vec4 frag_color;

void main() {
  uint vid = gl_VertexIndex;

  frag_color = data[vid].color;

  gl_Position = data[vid].pos;

  // Note that Vulkan NDC has +Y going down, so correct that here
  gl_Position.y = -gl_Position.y;
}
