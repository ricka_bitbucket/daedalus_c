#version 400
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

// Constants
layout(binding = 2) uniform sampler2D tex[128];

// Inputs
layout(location = 0) in vec2 texcoord;
layout(location = 1) flat in uint texture_index;

// Outputs
layout(location = 0) out vec4 uFragColor;

void main() {
  vec4 tex   = texture(tex[texture_index], texcoord.xy);
  uFragColor = vec4(tex.rgb, 1);
}
