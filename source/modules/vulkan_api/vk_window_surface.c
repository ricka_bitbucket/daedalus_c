/*
 * Copyright (c) 2015-2019 The Khronos Group Inc.
 * Copyright (c) 2015-2019 Valve Corporation
 * Copyright (c) 2015-2019 LunarG, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Chia-I Wu <olv@lunarg.com>
 * Author: Courtney Goeltzenleuchter <courtney@LunarG.com>
 * Author: Ian Elliott <ian@LunarG.com>
 * Author: Ian Elliott <ianelliott@google.com>
 * Author: Jon Ashburn <jon@lunarg.com>
 * Author: Gwan-gyeong Mun <elongbug@gmail.com>
 * Author: Tony Barbour <tony@LunarG.com>
 * Author: Bill Hollings <bill.hollings@brenwill.com>
 */
#define _GNU_SOURCE
#define _USE_MATH_DEFINES
#include "vk_window_surface.h"

#include <assert.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vulkan/vk_sdk_platform.h>

#include "core/containers.h"
#include "graphics_api/graphics.h"
#include "vulkan/vulkan.h"
#include "vulkan_types.inl"

PFN_vkGetPhysicalDeviceSurfaceSupportKHR fpGetPhysicalDeviceSurfaceSupportKHR           = NULL;
PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR fpGetPhysicalDeviceSurfaceCapabilitiesKHR = NULL;
PFN_vkGetPhysicalDeviceSurfaceFormatsKHR fpGetPhysicalDeviceSurfaceFormatsKHR           = NULL;
PFN_vkGetPhysicalDeviceSurfacePresentModesKHR fpGetPhysicalDeviceSurfacePresentModesKHR = NULL;

#define GET_INSTANCE_PROC_ADDR(inst, entrypoint)                                        \
  {                                                                                     \
    fp##entrypoint = (PFN_vk##entrypoint)vkGetInstanceProcAddr(inst, "vk" #entrypoint); \
    if (fp##entrypoint == NULL) {                                                       \
      ERR_EXIT("vkGetInstanceProcAddr failed to find vk" #entrypoint,                   \
               "vkGetInstanceProcAddr Failure");                                        \
    }                                                                                   \
  }

#define ERR_EXIT(err_msg, err_class)           \
  MessageBox(NULL, err_msg, err_class, MB_OK); \
  exit(1);

VkSurfaceKHR dd_vk_window_surface_create_present_surface(vulkan_api_device_t* in_device,
                                                         dd_graphics_window_t* in_window) {
  if (fpGetPhysicalDeviceSurfaceCapabilitiesKHR == NULL) {
    GET_INSTANCE_PROC_ADDR(in_device->inst, GetPhysicalDeviceSurfaceSupportKHR);
    GET_INSTANCE_PROC_ADDR(in_device->inst, GetPhysicalDeviceSurfaceCapabilitiesKHR);
    GET_INSTANCE_PROC_ADDR(in_device->inst, GetPhysicalDeviceSurfaceFormatsKHR);
    GET_INSTANCE_PROC_ADDR(in_device->inst, GetPhysicalDeviceSurfacePresentModesKHR);
  }

  // Create a WSI surface for the window:
  VkWin32SurfaceCreateInfoKHR createInfo;
  createInfo.sType     = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
  createInfo.pNext     = NULL;
  createInfo.flags     = 0;
  createInfo.hinstance = in_window->instance;
  createInfo.hwnd      = in_window->handle;

  VkSurfaceKHR surface;
  VK_CHECK_RESULT(vkCreateWin32SurfaceKHR(in_device->inst, &createInfo, NULL, &surface));

  return surface;
}

VkSurfaceFormatKHR* /* array */ dd_vk_window_surface_get_supported_framebuffer_formats(
    vulkan_api_device_t* api_device, VkSurfaceKHR surface) {
  // Get the list of VkFormat's that are supported:
  uint32_t formatCount;
  VK_CHECK_RESULT(fpGetPhysicalDeviceSurfaceFormatsKHR(api_device->physical_device.device, surface,
                                                       &formatCount, NULL));

  VkSurfaceFormatKHR* out_formats = {0}; /* array */
  array_resize(out_formats, formatCount, api_device->allocator);

  VK_CHECK_RESULT(fpGetPhysicalDeviceSurfaceFormatsKHR(api_device->physical_device.device, surface,
                                                       &formatCount, out_formats));

  return out_formats;
}

VkSurfaceFormatKHR dd_vk_window_surface_choose_color_format(vulkan_api_device_t* device,
                                                            VkSurfaceKHR surface,
                                                            VkFormat preferredFormat) {
  VkSurfaceFormatKHR* supported_formats; /* array */
  supported_formats = dd_vk_window_surface_get_supported_framebuffer_formats(device, surface);

  VkSurfaceFormatKHR out = {0};

  // If the format list includes just one entry of VK_FORMAT_UNDEFINED,
  // the surface has no preferred format.  Otherwise, at least one
  // supported format will be returned.
  const size_t formatCount = array_count(supported_formats);
  if (formatCount == 1 && supported_formats[0].format == VK_FORMAT_UNDEFINED) {
    out.format     = preferredFormat;
    out.colorSpace = supported_formats[0].colorSpace;
  } else {
    assert(formatCount >= 1);
    // Check if the preferred format is supported
    for (size_t i = 0; i < formatCount; i++) {
      if (supported_formats[i].format == preferredFormat) {
        out = supported_formats[i];
      }
    }
    const bool unable_to_find_preferred_format = (out.format == 0);
    if (unable_to_find_preferred_format) {
      out = supported_formats[0];
    }
  }
  array_free(supported_formats, device->allocator);

  return out;
}