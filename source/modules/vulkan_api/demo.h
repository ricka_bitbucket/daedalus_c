#include "core/types.h"

enum {
  // Allow a maximum of two outstanding presentation operations.
  FRAME_LAG        = 2,
  NUM_CUBE_BUFFERS = 4
};

typedef struct VulkanBuffer {
  VkBuffer buffer;
  VkDeviceMemory memory;
  void* memory_ptr;
} VulkanBuffer;

typedef struct {
  VkImage image;
  VkImageView view;
} SwapchainImageResources;

struct demo {
  char* name;  // Name to put on the window/icon

  dd_graphics_window_t window;

  VkSurfaceKHR surface;
  bool prepared;
  bool separate_present_queue;
  bool is_minimized;

  bool syncd_with_actual_presents;
  uint64_t refresh_duration;
  uint64_t refresh_duration_multiplier;
  uint64_t target_IPD;  // image present duration (inverse of frame rate)
  uint64_t prev_desired_present_time;
  uint32_t next_present_id;
  uint32_t last_early_id;  // 0 if no early images
  uint32_t last_late_id;   // 0 if no late images

  vulkan_api_device_t device;
  struct vulkan_queue_t* present_queue;

  char* enabled_layers[64];

  VkSurfaceFormatKHR surface_format;

  PFN_vkGetPhysicalDeviceSurfaceSupportKHR fpGetPhysicalDeviceSurfaceSupportKHR;
  PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR fpGetPhysicalDeviceSurfaceCapabilitiesKHR;
  PFN_vkGetPhysicalDeviceSurfacePresentModesKHR fpGetPhysicalDeviceSurfacePresentModesKHR;
  PFN_vkCreateSwapchainKHR fpCreateSwapchainKHR;
  PFN_vkDestroySwapchainKHR fpDestroySwapchainKHR;
  PFN_vkGetSwapchainImagesKHR fpGetSwapchainImagesKHR;
  PFN_vkAcquireNextImageKHR fpAcquireNextImageKHR;
  PFN_vkQueuePresentKHR fpQueuePresentKHR;
  PFN_vkGetRefreshCycleDurationGOOGLE fpGetRefreshCycleDurationGOOGLE;
  PFN_vkGetPastPresentationTimingGOOGLE fpGetPastPresentationTimingGOOGLE;
  uint32_t swapchainImageCount;
  VkSwapchainKHR swapchain;
  SwapchainImageResources* swapchain_image_resources;

  VkFence fences[FRAME_LAG];
  int frame_index;

  struct vulkan_texture_t* depth;
  struct vulkan_texture_t* swapchain_images; /* array */

  bool quit;
  int32_t curFrame;
  bool use_break;
  bool suppress_popups;
};

struct DefaultVulkanLoop {
  uint32 width, height;
  VkSemaphore image_acquired_semaphores[FRAME_LAG];
  VkSemaphore draw_complete_semaphores[FRAME_LAG];
  VkSemaphore image_ownership_semaphores[FRAME_LAG];
};