#include "os.h"

#include "core/containers.h"
#include "core/types.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>

static const int windows_types[] = {0, CF_TEXT};

static void os_clipboard_set_data(dd_clipboard_t type, const void* data, size_t data_byte_count) {
  OpenClipboard(NULL);
  EmptyClipboard();

  // Allocate a global memory object for the text.
  HGLOBAL hglbCopy = GlobalAlloc(GMEM_MOVEABLE, data_byte_count);
  if (hglbCopy == NULL) {
    CloseClipboard();
    return;
  }

  switch (type) {
    case EClipboardText: {
      char* strCopy = GlobalLock(hglbCopy);
      if (strCopy) {
        memcpy(strCopy, data, data_byte_count);
        GlobalUnlock(hglbCopy);
      }
    } break;
  }
  GlobalUnlock(hglbCopy);

  SetClipboardData(windows_types[type], hglbCopy);

  CloseClipboard();
}

static char* os_clipboard_get_data(dd_clipboard_t type, struct dd_allocator* allocator) {
  if (!IsClipboardFormatAvailable(windows_types[type])) return NULL;
  if (!OpenClipboard(NULL)) return NULL;

  char* out = NULL;

  HGLOBAL hglb = GetClipboardData(CF_TEXT);
  if (hglb != NULL) {
    switch (type) {
      case EClipboardText: {
        const char* lptstr = GlobalLock(hglb);
        if (lptstr) {
          const size_t len = strlen(lptstr) + 1;
          array_resize(out, len, allocator);
          memcpy(out, lptstr, len);
          GlobalUnlock(hglb);
        }
      } break;
    }
  }

  CloseClipboard();

  return out;
}

/// This function changes the cursor.
///
/// It only works properly if WM_SETCURSOR is also handled manually (currently in graphics.c
void os_set_cursor(dd_cursor_t cursor_type) {
  // TODO(Rick): properly destroy this handle during close.
  static HCURSOR cursor_handle = NULL;
  if (cursor_handle != NULL) {
    (void)DestroyCursor(cursor_handle);
  }

  const LPCSTR cursors[] = {IDC_ARROW, IDC_HAND, IDC_IBEAM, IDC_HELP};
  static_assert(countof(cursors) == ECursorTypeCount, "Missing cursor definitions.");

  cursor_handle = LoadCursor(NULL, cursors[(int)cursor_type]);
  (void)SetCursor(cursor_handle);
}

static const struct dd_os_api dd_os_api = {
    .clipboard = {.set_data = &os_clipboard_set_data, .get_data = &os_clipboard_get_data},
    .window    = {.set_cursor = &os_set_cursor}};

  #if defined(_MSC_VER)
    #define DD_DLL_EXPORT __declspec(dllexport)
  #else
    #define DD_DLL_EXPORT
  #endif

  #include "core/api_registry.h"
  #include "core/plugins.h"
DD_DLL_EXPORT bool plugin_link_apis(const dd_api_registry_t* registry,
                                    struct dd_allocator* allocator) {
  // Don't depend on anything, so simply register.
  registry->register_api("os", 1, &dd_os_api, sizeof(dd_os_api));

  return true;
}

#endif
