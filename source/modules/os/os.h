#pragma once

#include "core/types.h"

struct dd_allocator;

typedef enum { EClipboardEmpty = 0, EClipboardText = 1 } dd_clipboard_t;

typedef enum {
  ECursorTypeDefault = 0,
  ECursorTypeClickable,
  ECursorTypeIBeam,
  ECursorTypeHelp,

  ECursorTypeCount
} dd_cursor_t;

struct dd_os_api {
  struct {
    // Set data onto the OS clipboard for the passed in type of data.
    void (*set_data)(dd_clipboard_t type, const void* data, size_t data_byte_count);

    // Get data from the OS clipboard for the passed in type of data.
    //
    // return: NULL if no data is registered for the given type, else array(char) of data.
    char* /* array */ (*get_data)(dd_clipboard_t type, struct dd_allocator* allocator);
  } clipboard;

  struct {
    void (*set_cursor)(dd_cursor_t cursor_type);
  } window;
};
