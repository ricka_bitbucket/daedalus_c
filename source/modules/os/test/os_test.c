#include "os/os.h"

#define DD_TEST_GENERATE_MAIN

#include "core/test.h"

static struct dd_os_api* os_api_ = NULL;

TEST_CASE(clipboard_text) {
  char stack_buffer[2048];
  dd_heap_allocator_init(stack_buffer, sizeof(stack_buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)stack_buffer;

  const char* text = "My text to put on clipboard!!";
  size_t len       = strlen(text) + 1;

  os_api_->clipboard.set_data(EClipboardText, text, len);

  char* retrieved_text = os_api_->clipboard.get_data(EClipboardText, allocator);
  DD_TEST_IS_TRUE(retrieved_text != NULL);
  if (retrieved_text) {
    DD_TEST_IS_TRUE(strcmp(text, retrieved_text) == 0);
  }
}

TEST_SUITE(os, test_data) { TEST_ADD_CASE(clipboard_text, test_data); }

#include "core/api_registry.h"
#include "core/application.h"
#include "core/plugins.h"

static uint8 stack_buffer[100000];

int main(int argc, char** args) {
  dd_test_data_t test_d     = {0};
  dd_test_data_t* test_data = &test_d;

  dd_heap_allocator_init(stack_buffer, sizeof(stack_buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)stack_buffer;
  dd_api_registry.init(allocator);
  dd_stringview_t app_folder   = dd_application.executable_folder();
  struct dd_plugins_o* plugins = dd_plugins_api.create(allocator);
  const char* paths[]          = {app_folder.string};
  dd_plugins_api.load_plugins(plugins, paths, 1);
  os_api_ = (struct dd_os_api*)dd_api_registry.get_api("os", 1);

  TEST_ADD_SUITE(os, test_data);

  return dd_perform_tests(test_data, argc, args);
}
