#pragma once

#include "../core/thread.h"
#include "../core/types.h"

enum { TELEMETRY_CLIENT_SAVED_BUFFER_SIZE = 1024 * 1024 };
enum { MEMORY_SENTINAL_THREAD_ID = 0xAA, MEMORY_SENTINAL_ALLOC, MEMORY_SENTINAL_FREE };

typedef struct dd_memory_threaddata_t {
  uint64 thread_id;
} dd_memory_threaddata_t;

typedef struct dd_memory_alloc_data_t {
  uint64 timestamp;
  uint64 allocator_id;
  uint64 address_returned;
  uint32 byte_count;
  const char* file_name;
  uint32 line_number;
} dd_memory_alloc_data_t;

typedef struct dd_memory_free_data_t {
  uint64 timestamp;
  uint64 allocator_id;
  uint64 address_freed;
  uint32 byte_count;
} dd_memory_free_data_t;

struct buf_and_size {
  uint8* data;
  uint32 byte_count;
};

struct telemetry_memory_processed_t {
  uint8* data;
  uint32 byte_count;

  uint32 entry_count;
  int32 memory_delta_byte_count;
};

struct dd_telemetry_memory_t {
  struct buf_and_size* incoming_buffers;               /* array */
  struct telemetry_memory_processed_t* stored_buffers; /* array */

  dd_mutex_t incoming_buffers_mutex;
  uint32 current_total_mem_allocated;
  uint32 maximum_total_allocation;
};

struct telemetry_client_t {
  // As reports come in, if recording is not enabled store them in this buffer.
  // dd_log_remote_message_header_t payload_description
  // byte_count payload_bytes
  // dd_log_remote_message_header_t payload_description
  // ...
  struct {
    uint8 data[TELEMETRY_CLIENT_SAVED_BUFFER_SIZE];
    uint8* ptr;
    uint8* range_end;
    dd_mutex_t mutex;
  } stored_reports;

  struct dd_allocator* allocator;
};

typedef struct telemetry_server_t {
  struct dd_allocator* allocator;

  struct dd_telemetry_memory_t memory;

} telemetry_server_t;
