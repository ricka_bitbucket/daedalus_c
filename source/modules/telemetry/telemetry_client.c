#include "telemetry_client.h"

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "core/containers.h"
#include "core/dd_math.h"
#include "core/log_remote.h"
#include "core/memory.h"
#include "core/memory.inl"
#include "core/network.h"
#include "core/profiler.h"
#include "core/profiler.inl"
#include "core/thread.h"
#include "core/utils.h"
#include "telemetry_types.inl"

static void telemetry_profiler_report(uint8* buffer, unsigned buffer_size);

static uint32 should_save_profile                = 0;
static dd_network_connection_t* g_telemetry_conn = NULL;
static volatile bool isRunning                   = true;

struct telemetry_client_t g_telemetry_client = {0};

static void dd_profiler_client_gather_data(void* buffer, unsigned buffer_size, uint8** out_data) {
  dd_mutex_lock(&g_telemetry_client.stored_reports.mutex);

  // Gather labels
  dd_hash64_t index_from_label = {0};
  uint8* labels                = {0};

  // Add space for offset count to actual data to skip over labels being added. Will be filled in
  // later
  uint32 offset_to_data = 0;
  array_append(labels, (uint8*)&offset_to_data, (unsigned)(sizeof(offset_to_data)),
               g_telemetry_client.allocator);

  {
    uint8* buf     = (uint8*)buffer;
    uint8* buf_end = buf + buffer_size;
    while (buf != buf_end) {
      uint8 sentinal = *buf;
      buf += 1;
      if (sentinal == SENTINAL_START) {
        dd_perf_startdata* sd = (dd_perf_startdata*)buf;
        buf += sizeof(dd_perf_startdata);

        uint64 offset_into_labels =
            dd_hash64_lookup(&index_from_label, (uint64)sd->label, array_count(labels));
        if (offset_into_labels == array_count(labels)) {
          dd_hash64_add(&index_from_label, (uint64)sd->label, offset_into_labels,
                        g_telemetry_client.allocator);
          array_append(labels, (uint8*)sd->label, (unsigned)(strlen(sd->label) + 1),
                       g_telemetry_client.allocator);
        }

        // For now change the actual data
        sd->label = (const char*)(intptr_t)offset_into_labels;
      } else if (sentinal == SENTINAL_END) {
        buf += sizeof(dd_perf_enddata);
      } else if (sentinal == SENTINAL_THREAD_ID) {
        buf += sizeof(dd_perf_threaddata_t);
      } else if (sentinal == SENTINAL_THREAD_NAME) {
        buf += sizeof(dd_perf_threadname_t);
      } else {
        assert(false);  // Unknown id
      }
    }
  }

  // Now fill in offset to actual data
  *(uint32*)labels = (uint32)array_count(labels);

  array_append(labels, buffer, buffer_size, g_telemetry_client.allocator);

  // TODO: remove this extra copy
  array_append(*out_data, labels, array_count(labels), g_telemetry_client.allocator);
  array_free(labels, g_telemetry_client.allocator);

  dd_mutex_release(&g_telemetry_client.stored_reports.mutex);
}

static void dd_telemetry_client_gather_memory_data(const uint8* buffer, uint32 buffer_size,
                                                   uint8** out_data) {
  dd_mutex_lock(&g_telemetry_client.stored_reports.mutex);

  array_append(*out_data, &buffer_size, sizeof(buffer_size), g_telemetry_client.allocator);
  array_append(*out_data, buffer, buffer_size, g_telemetry_client.allocator);

  {
    uint32 buf_offset    = sizeof(buffer_size);
    const uint32 buf_end = buf_offset + buffer_size;
    while (buf_offset < buf_end) {
      uint8 sentinal = (*out_data)[buf_offset];
      buf_offset += 1;

      switch (sentinal) {
        case MEMORY_SENTINAL_THREAD_ID: {
          buf_offset += sizeof(dd_memory_threaddata_t);
        } break;
        case MEMORY_SENTINAL_ALLOC: {
          dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)((*out_data) + buf_offset);
          buf_offset += sizeof(dd_memory_alloc_data_t);

          const char* file_name = ad->file_name;

          ad->file_name =
              (void*)(uintptr_t)(array_count(*out_data) - sizeof(dd_log_remote_message_header_t));

          array_append(*out_data, (uint8*)file_name, (unsigned)(strlen(file_name) + 1),
                       g_telemetry_client.allocator);
        } break;
        case MEMORY_SENTINAL_FREE: {
          buf_offset += sizeof(dd_memory_free_data_t);
        } break;
      }
    }
  }

  dd_devprintf("gathered memory data %u and %u\n", buffer_size, array_count(*out_data));
  dd_mutex_release(&g_telemetry_client.stored_reports.mutex);
}

void telemetry_client_store_report(enum ELogRemoteMessageType message_type, const uint8* buffer,
                                   uint32 byte_count) {
  dd_mutex_lock(&g_telemetry_client.stored_reports.mutex);

  // TODO: for now if the buffer is full, simply discard it and start anew
  dd_log_remote_message_header_t msg = {message_type, byte_count};
  uint8* new_ptr = g_telemetry_client.stored_reports.ptr + sizeof(msg) + byte_count;
  if (new_ptr > g_telemetry_client.stored_reports.range_end) {
    dd_datetime_t dt = dd_datetime();
    dd_devprintf(
        "%02i:%02i:%02i Discarded %u bytes of stored telemetry data as no connection available\n",
        dt.hour, dt.minute, dt.second, TELEMETRY_CLIENT_SAVED_BUFFER_SIZE);
    g_telemetry_client.stored_reports.ptr = g_telemetry_client.stored_reports.data;
  }
  memcpy(g_telemetry_client.stored_reports.ptr, &msg, sizeof(msg));
  g_telemetry_client.stored_reports.ptr += sizeof(msg);
  memcpy(g_telemetry_client.stored_reports.ptr, buffer, byte_count);
  g_telemetry_client.stored_reports.ptr += byte_count;

  dd_mutex_release(&g_telemetry_client.stored_reports.mutex);
}

void telemetry_profiler_report(uint8* buffer, unsigned byte_count) {
  if (should_save_profile > 0 && g_telemetry_conn) {
    uint8* data                        = {0}; /* array */
    dd_log_remote_message_header_t msg = {MSG_PROFILER, 0};
    array_append(data, &msg, sizeof(msg), g_telemetry_client.allocator);
    dd_profiler_client_gather_data(buffer, byte_count, &data);
    ((dd_log_remote_message_header_t*)data)->payload_num_bytes =
        (uint32)(array_count(data) - sizeof(msg));
    dd_network_connection_send_data(g_telemetry_conn, data, array_count(data));

    array_free(data, g_telemetry_client.allocator);

    should_save_profile--;
  } else {
    telemetry_client_store_report(MSG_PROFILER, buffer, byte_count);
  }
}

void telemetry_client_send_stored_reports() {
  dd_mutex_lock(&g_telemetry_client.stored_reports.mutex);

  // dd_devprintf("start sending stored reports\n");
  uint8* buffer_it        = g_telemetry_client.stored_reports.data;
  const uint8* buffer_end = g_telemetry_client.stored_reports.ptr;
  while (buffer_it < buffer_end) {
    dd_log_remote_message_header_t* msg = (dd_log_remote_message_header_t*)buffer_it;
    uint32 stored_byte_count            = msg->payload_num_bytes;
    uint8* stored_data                  = (uint8*)(msg + 1);

    switch (msg->message_type) {
      case MSG_PROFILER: {
        // Send
        uint8* data = {0}; /* array */
        array_append(data, msg, sizeof(*msg), g_telemetry_client.allocator);
        dd_profiler_client_gather_data(stored_data, stored_byte_count, &data);
        ((dd_log_remote_message_header_t*)data)->payload_num_bytes =
            (uint32)(array_count(data) - sizeof(*msg));
        dd_network_connection_send_data(g_telemetry_conn, data, array_count(data));
        array_free(data, g_telemetry_client.allocator);

        /*
        uint64 ts_first = profiler_first_timestamp_in_buffer(stored_data, stored_byte_count);
        uint64 ts_last  = profiler_last_timestamp_in_buffer(stored_data, stored_byte_count);
        dd_devprintf("Sending profile with %i bytes %llu - %llu\n", msg->payload_num_bytes,
                     ts_first, ts_last);
                     */
      } break;
      case MSG_MEMORY: {
        // TODO: gather strings etc
        dd_devprintf("sending memory to server\n");
        // Send
        uint8* data = {0}; /* array */
        array_append(data, msg, sizeof(*msg), g_telemetry_client.allocator);
        dd_telemetry_client_gather_memory_data(stored_data, stored_byte_count, &data);
        ((dd_log_remote_message_header_t*)data)->payload_num_bytes =
            (uint32)(array_count(data) - sizeof(*msg));
        dd_network_connection_send_data(g_telemetry_conn, data, array_count(data));
        array_free(data, g_telemetry_client.allocator);
      } break;
    }

    buffer_it = stored_data + stored_byte_count;
  }
  g_telemetry_client.stored_reports.ptr = g_telemetry_client.stored_reports.data;

  // dd_devprintf("end sending stored reports\n");
  dd_mutex_release(&g_telemetry_client.stored_reports.mutex);
}

void telemetry_client_main(void* arg) {
  (void)arg;

  g_telemetry_conn = dd_network_connect("localhost", 12345, tmp_malloc_allocator);

  if (g_telemetry_conn == NULL) {
    // TODO: allow late connection
    return;
  }

  while (isRunning) {
    size_t num_bytes_waiting = dd_network_connection_bytes_received(g_telemetry_conn);
    if (num_bytes_waiting > 0) {
      uint8 msg_buffer[1024];
      assert(sizeof(msg_buffer) >= num_bytes_waiting);
      dd_network_connection_read_bytes(g_telemetry_conn, msg_buffer, num_bytes_waiting);
      dd_log_remote_message_header_t* header = (dd_log_remote_message_header_t*)msg_buffer;

      if (header->message_type == MSG_PROFILER_RECORD) {
        bool should_record = *(bool*)(header + 1);
        if (should_record) {
          should_save_profile = 0xFFFFFFFF;
        } else {
          should_save_profile = 0;
        }
        dd_devprintf("Should record %i\n", should_record);
        if (g_telemetry_client.stored_reports.ptr != g_telemetry_client.stored_reports.data) {
          telemetry_client_send_stored_reports();
        }
      }
    }

    dd_sleep(1);
  };
}

void dd_telemetry_client_send_profiler_frame() {
  if (should_save_profile == 0) {
    should_save_profile = 1;
  }
}

struct telemetry_memory_allocator_impl_t {
  dd_allocator_impl allocator;

  // Concrete types of allocator can store data after these functions
  dd_allocator* wrapped_allocator;
};
static_assert(sizeof(struct telemetry_memory_allocator_impl_t) <= sizeof(dd_allocator),
              "Allocator doesn't fit in standard allocator size");

// Forward declarations
void dd_memory_log_alloc(uint64 allocator_id, uint64 address_returned, uint32 byte_count,
                         const struct allocation_location* alloc_location);
void dd_memory_log_free(uint64 allocator_id, uint64 address_freed, uint32 byte_count);

// Tracks the allocation for telemetry, and forwards request to wrapped allocator
void* telemetry_memory_alloc(dd_allocator* allocator, uint32 count__bytes, uint32 align,
                             const struct allocation_location* alloc_location) {
  struct telemetry_memory_allocator_impl_t* alloc_impl =
      (struct telemetry_memory_allocator_impl_t*)allocator;

  dd_allocator_impl* wrapper_alloc_impl = (dd_allocator_impl*)(alloc_impl->wrapped_allocator);
  void* out = wrapper_alloc_impl->alloc(alloc_impl->wrapped_allocator, count__bytes, align,
                                        alloc_location);

  dd_memory_log_alloc((uint64)(uintptr_t)allocator, (uint64)(uintptr_t)out, count__bytes,
                      alloc_location);

  return out;
}

// Tracks the free actions for telemetry, and forwards request to wrapped allocator
uint32 telemetry_memory_free(dd_allocator* allocator, void* p,
                             const struct allocation_location* alloc_location) {
  struct telemetry_memory_allocator_impl_t* alloc_impl =
      (struct telemetry_memory_allocator_impl_t*)allocator;

  uint32 byte_count = dd_free(alloc_impl->wrapped_allocator, p);

  dd_memory_log_free((uint64)(uintptr_t)allocator, (uint64)(uintptr_t)p, byte_count);

  return byte_count;
}

void dd_telemetry_client_memory_allocator_init(dd_allocator* telemetry_allocator,
                                               dd_allocator* wrapped_allocator) {
  struct telemetry_memory_allocator_impl_t* alloc_impl =
      (struct telemetry_memory_allocator_impl_t*)telemetry_allocator;
  memset(alloc_impl, 0, sizeof(*alloc_impl));

  alloc_impl->allocator.name    = "telemetry_allocator_wrapper";
  alloc_impl->allocator.alloc   = &telemetry_memory_alloc;
  alloc_impl->allocator.free    = &telemetry_memory_free;
  alloc_impl->wrapped_allocator = wrapped_allocator;
}

void memory_callback(const uint8* buffer, uint32 byte_count) {
  if (should_save_profile > 0 && g_telemetry_conn) {
    // TODO: this mutex is so that an incoming buffer isn't sent while stored reports are being
    // sent. Should add a proper queue to avoid this.
    dd_mutex_lock(&g_telemetry_client.stored_reports.mutex);

    dd_devprintf("sending memory to server\n");
    // Send
    uint8* data                        = {0}; /* array */
    dd_log_remote_message_header_t msg = {MSG_MEMORY, byte_count};
    array_append(data, &msg, sizeof(msg), g_telemetry_client.allocator);
    dd_telemetry_client_gather_memory_data(buffer, byte_count, &data);
    ((dd_log_remote_message_header_t*)data)->payload_num_bytes =
        (uint32)(array_count(data) - sizeof(msg));
    dd_network_connection_send_data(g_telemetry_conn, data, array_count(data));
    array_free(data, g_telemetry_client.allocator);
    // TODO: Free buffer?

    dd_mutex_release(&g_telemetry_client.stored_reports.mutex);
  } else {
    telemetry_client_store_report(MSG_MEMORY, buffer, byte_count);
  }
}

void dd_telemetry_client_send_memory_data() { dd_memory_report_changes(&memory_callback); }

enum { PROFILER_BUFFER_SIZE = 2048, MAX_NUM_THREADS = 128 };

struct dd_memory_thread_data {
  uint8* buffer_start;
  uint8* buffer_ptr;
  uint8* buffer_end;
  uint32 buffer_size;
};

struct memory_retired_block {
  uint8* data;
  uint32 used_byte_count;
  uint32 allocation_byte_count;
  uint64 thread_id;
};

struct memory_central_t {
  dd_mutex_t mutex;

  uint8** free_blocks;                         /* array */
  struct memory_retired_block* retired_blocks; /* array */
} g_memory_central = {0};

THREAD_LOCAL struct dd_memory_thread_data memory_thread_data = {0};

uint8* memory_central_block_allocate(uint32 byte_count) {
  uint8* out = NULL;
  if (g_memory_central.mutex.opaque_[0] == 0) {
    g_memory_central.mutex = dd_mutex_create();
  }

  dd_mutex_lock(&g_memory_central.mutex);
  for (int32 i = (int32)array_count(g_memory_central.free_blocks) - 1; i >= 0; i--) {
    uint8* a = g_memory_central.free_blocks[i];
    if (array_count(a) >= byte_count) {
      out = a;
      array_remove_at_index(g_memory_central.free_blocks, (uint32)i);
    }
  }

  if (out == NULL) {
    out = dd_alloc(g_telemetry_client.allocator, sizeof(dd_array_header_t) + byte_count, 1);
    out += sizeof(dd_array_header_t);
    array_header(out)->capacity_ = byte_count;
    array_header(out)->count_    = byte_count;
    // dd_devprintf("alloc a new memory block %u bytes\n", byte_count);
  }
  dd_mutex_release(&g_memory_central.mutex);

  return out;
}

void memory_central_block_discard(uint8* data, uint32 byte_count) {
  dd_mutex_lock(&g_memory_central.mutex);
  array_reset(data);
  array_push(g_memory_central.free_blocks, data, g_telemetry_client.allocator);
  dd_mutex_release(&g_memory_central.mutex);
}

void memory_central_block_retire(uint64 thread_id, void* data, uint32 data_byte_count,
                                 uint32 byte_count) {
  struct memory_retired_block rb = {.data                  = data,
                                    .thread_id             = thread_id,
                                    .used_byte_count       = data_byte_count,
                                    .allocation_byte_count = byte_count};
  // TODO: could possibly also allocate the next block here already
  dd_mutex_lock(&g_memory_central.mutex);
  array_push(g_memory_central.retired_blocks, rb, g_telemetry_client.allocator);
  dd_mutex_release(&g_memory_central.mutex);
}

void dd_telemetry_client_flush_threaddata() {
  const uint32 byte_count =
      (uint32)(memory_thread_data.buffer_ptr - memory_thread_data.buffer_start);
  if (byte_count > 0) {
    memory_central_block_retire(dd_thread_id(), memory_thread_data.buffer_start, byte_count,
                                memory_thread_data.buffer_size);
    memory_thread_data.buffer_start   = memory_thread_data.buffer_end =
        memory_thread_data.buffer_ptr = NULL;
  }
}

void dd_memory_report_changes(dd_allocator_report_changes_callback cb) {
  if (g_memory_central.mutex.opaque_[0] == 0) {
    g_memory_central.mutex = dd_mutex_create();
  }

  dd_mutex_lock(&g_memory_central.mutex);
  for (uint32 i = 0; i < array_count(g_memory_central.retired_blocks); i++) {
    cb(g_memory_central.retired_blocks[i].data,
       g_memory_central.retired_blocks[i].used_byte_count);
    array_push(g_memory_central.free_blocks, g_memory_central.retired_blocks[i].data,
               g_telemetry_client.allocator);
  }
  array_reset(g_memory_central.retired_blocks);
  dd_mutex_release(&g_memory_central.mutex);
}

void dd_memory_log_alloc(uint64 allocator_id, uint64 address_returned, uint32 byte_count,
                         const struct allocation_location* alloc_location) {
  dd_memory_alloc_data_t ad = {.timestamp        = dd_time_microseconds(),
                               .allocator_id     = allocator_id,
                               .address_returned = address_returned,
                               .byte_count       = byte_count,
                               .file_name        = alloc_location->file_name,
                               .line_number      = alloc_location->line_number};
  // dd_devprintf("log allo %p\n", address_returned);

  // 1 byte for sentinal to indicate this is an allocation block, then the size of the block
  uint8* new_offset = memory_thread_data.buffer_ptr + 1 + sizeof(ad);
  if (new_offset > memory_thread_data.buffer_end) {
    const uint32 new_buffer_size =
        (memory_thread_data.buffer_end == 0)
            ? (memory_thread_data.buffer_size > 0 ? memory_thread_data.buffer_size : 5 * 1024)
            : memory_thread_data.buffer_size + memory_thread_data.buffer_size / 2;
    uint32 used_byte_count =
        (uint32)(memory_thread_data.buffer_ptr - memory_thread_data.buffer_start);
    uint8* new_block                = memory_central_block_allocate(new_buffer_size);
    uint8* old_block                = memory_thread_data.buffer_start;
    uint32 old_buffer_size          = memory_thread_data.buffer_size;
    memory_thread_data.buffer_start = new_block;
    memory_thread_data.buffer_ptr   = memory_thread_data.buffer_start + used_byte_count;
    memory_thread_data.buffer_end   = memory_thread_data.buffer_start + new_buffer_size;
    memory_thread_data.buffer_size  = new_buffer_size;

    // dd_devprintf("rotate buff\n");

    if (old_block) {
      memcpy(new_block, old_block, used_byte_count);
      memory_central_block_discard(old_block, old_buffer_size);
    } else {
      // Insert the thread ID when a memory block is first returned
      *(memory_thread_data.buffer_ptr++) = MEMORY_SENTINAL_THREAD_ID;
      dd_memory_threaddata_t td          = {.thread_id = dd_thread_id()};
      memcpy(memory_thread_data.buffer_ptr, &td, sizeof(td));
      memory_thread_data.buffer_ptr += sizeof(td);
    }
  }

  *memory_thread_data.buffer_ptr = MEMORY_SENTINAL_ALLOC;
  ++memory_thread_data.buffer_ptr;
  memcpy(memory_thread_data.buffer_ptr, &ad, sizeof(ad));
  memory_thread_data.buffer_ptr += sizeof(ad);
}

void dd_memory_log_free(uint64 allocator_id, uint64 address_freed, uint32 byte_count) {
  dd_memory_free_data_t fd = {.timestamp     = dd_time_microseconds(),
                              .allocator_id  = allocator_id,
                              .address_freed = address_freed,
                              .byte_count    = byte_count};
  // dd_devprintf("log free %p\n", address_freed);
  uint8* new_offset = memory_thread_data.buffer_ptr + 1 + sizeof(fd);
  if (new_offset > memory_thread_data.buffer_end) {
    const uint32 new_buffer_size =
        (memory_thread_data.buffer_end == 0)
            ? (memory_thread_data.buffer_size > 0 ? memory_thread_data.buffer_size : 5 * 1024)
            : memory_thread_data.buffer_size + memory_thread_data.buffer_size / 2;
    void* block = memory_central_block_allocate(new_buffer_size);

    // dd_devprintf("rotate buff\n");

    const uint32 used_byte_count =
        (uint32)(memory_thread_data.buffer_ptr - memory_thread_data.buffer_start);
    memcpy(block, memory_thread_data.buffer_start, used_byte_count);
    memory_central_block_discard(memory_thread_data.buffer_start, memory_thread_data.buffer_size);
    memory_thread_data.buffer_start = block;
    memory_thread_data.buffer_ptr   = memory_thread_data.buffer_start + used_byte_count;
    memory_thread_data.buffer_end   = memory_thread_data.buffer_start + new_buffer_size;
    memory_thread_data.buffer_size  = new_buffer_size;
  }

  *memory_thread_data.buffer_ptr = MEMORY_SENTINAL_FREE;
  ++memory_thread_data.buffer_ptr;
  memcpy(memory_thread_data.buffer_ptr, &fd, sizeof(fd));
  memory_thread_data.buffer_ptr += sizeof(fd);
}

struct profiler_retired_block {
  uint8* data;
  uint32 byte_count;
  uint32 allocation_byte_count;
  uint64 thread_id;
};
struct profiler_central_t {
  dd_mutex_t mutex;

  uint8** free_blocks;                           /* array */
  struct profiler_retired_block* retired_blocks; /* array */
} g_profiler_central = {0};

uint8* telemetry_profiler_central_block_allocate(uint32 byte_count) {
  uint8* out = NULL;

  dd_mutex_lock(&g_profiler_central.mutex);
  for (int32 i = (int32)array_count(g_profiler_central.free_blocks) - 1; i >= 0; i--) {
    uint8* a = g_profiler_central.free_blocks[i];
    if (array_count(a) >= byte_count) {
      out = a;
      array_remove_at_index(g_profiler_central.free_blocks, (uint32)i);
    }
  }

  if (out == NULL) {
    assert(g_telemetry_client.allocator);
    out = dd_alloc(g_telemetry_client.allocator, sizeof(dd_array_header_t) + byte_count, 1);
    out += sizeof(dd_array_header_t);
    array_header(out)->capacity_ = byte_count;
    array_header(out)->count_    = byte_count;
    // dd_devprintf("alloc a new memory block %u bytes\n", byte_count);
  }
  dd_mutex_release(&g_profiler_central.mutex);

  return out;
}

uint8* telemetry_profiler_central_block_reallocate(uint8* ptr, uint32 old_byte_count,
                                                   uint32 new_byte_count) {
  uint8* out = NULL;

  dd_mutex_lock(&g_profiler_central.mutex);
  for (int32 i = (int32)array_count(g_profiler_central.free_blocks) - 1; i >= 0; i--) {
    uint8* a = g_profiler_central.free_blocks[i];
    if (array_count(a) >= new_byte_count) {
      out = a;
      array_remove_at_index(g_profiler_central.free_blocks, (uint32)i);
    }
  }

  if (out == NULL) {
    assert(g_telemetry_client.allocator);
    out = dd_alloc(g_telemetry_client.allocator, sizeof(dd_array_header_t) + new_byte_count, 1);
    out += sizeof(dd_array_header_t);
    array_header(out)->capacity_ = new_byte_count;
    array_header(out)->count_    = new_byte_count;
    // dd_devprintf("alloc a new memory block %u bytes\n", byte_count);
  }

  uint32 min_byte_count = min(old_byte_count, new_byte_count);
  memcpy(out, ptr, min_byte_count);
  array_free(ptr, g_telemetry_client.allocator);  // The old one was too small, so just discard it

  dd_mutex_release(&g_profiler_central.mutex);

  return out;
}

void telemetry_profiler_central_block_retire(uint64 thread_id, void* data, uint32 data_byte_count,
                                             uint32 byte_count) {
  struct profiler_retired_block rb = {.data                  = data,
                                      .thread_id             = thread_id,
                                      .byte_count            = data_byte_count,
                                      .allocation_byte_count = byte_count};
  // TODO: could possibly also allocate the next block here already
  dd_mutex_lock(&g_profiler_central.mutex);
  // Don't need to store if sending directly
  // array_push(g_profiler_central.retired_blocks, rb, g_telemetry_client.allocator);
  (void)rb;
  telemetry_profiler_report(data, data_byte_count);
  if (g_telemetry_client.allocator) {
    array_push(g_profiler_central.free_blocks, data, g_telemetry_client.allocator);
  }
  dd_mutex_release(&g_profiler_central.mutex);
}

struct dd_profiler_store g_telemetry_store = {&telemetry_profiler_central_block_allocate,
                                              &telemetry_profiler_central_block_reallocate,
                                              &telemetry_profiler_central_block_retire};

void dd_telemetry_client_init(dd_allocator* allocator) {
  dd_prof_thread_init("telemetry_client");

  g_telemetry_client.allocator = allocator;

  // First create the mutex ...
  g_telemetry_client.stored_reports.mutex = dd_mutex_create();
  g_telemetry_client.stored_reports.ptr   = g_telemetry_client.stored_reports.data;
  g_telemetry_client.stored_reports.range_end =
      g_telemetry_client.stored_reports.data + sizeof(g_telemetry_client.stored_reports.data);
  // ... only then register the callback, since the callback may use the mutex
  dd_prof_set_process_callback(telemetry_profiler_report);

  dd_thread_api.start(&telemetry_client_main, NULL, "DD Telemetry Client");

  g_profiler_central.mutex = dd_mutex_create();
  profiler_store           = &g_telemetry_store;
}
