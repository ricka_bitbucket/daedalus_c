#include "telemetry_server.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "core/containers.h"
#include "core/dd_math.h"
#include "core/log_remote.h"
#include "core/log_structs.inl"
#include "core/memory.h"
#include "core/network.h"
#include "core/profiler.h"
#include "core/profiler.inl"
#include "core/thread.h"
#include "core/utils.h"
#include "telemetry_types.inl"

#if defined(__clang__)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wformat-nonliteral"
  #pragma clang diagnostic ignored "-Wformat-security"
#endif

static volatile bool isRunning = true;

typedef struct dd_hub_client_t {
  uint8* waiting_data;
} dd_hub_client_t;

dd_hub_client_t* hub_clients = {0};

// TODO: make better
char string_buffer[1024 * 1024];
unsigned current_offset = 0;

dd_log_entry* log_remote_entries2 = {0};
dd_hash64_t log_remote_entry_hash = {0};

const char* log_messages_strings[1024 * 1024] = {0};
char log_messages[1024 * 1024 * 100]          = {0};
uint32 log_messages_offset                    = 0;
uint32 log_messages_count                     = 0;

telemetry_server_t g_telemetry_server = {0};

void logs_get_lines(uint32 offset, uint32 count, const char** out_lines) {
  for (uint32 i = 0; i < count; i++) {
    out_lines[i] = log_messages_strings[offset + i];
  }
}

static void log_process_buffer_strings(void* buffer, unsigned byte_count) {
  uint8* it     = (uint8*)buffer;
  uint8* it_end = it + byte_count;
  while (it < it_end) {
    const dd_log_entry* address = *(const dd_log_entry**)it;
    it += sizeof(void*);
    dd_log_entry entry = *(const dd_log_entry*)it;
    it += sizeof(entry);
    const char* function = string_buffer + current_offset;
    strcpy(string_buffer + current_offset, (const char*)it);
    current_offset += (unsigned)strlen(function) + 1;
    while (*it) {
      it++;
    }
    it++;
    const char* file = string_buffer + current_offset;
    strcpy(string_buffer + current_offset, (const char*)it);
    current_offset += (unsigned)strlen(file) + 1;
    while (*it) {
      it++;
    }
    it++;
    const char* format = string_buffer + current_offset;
    strcpy(string_buffer + current_offset, (const char*)it);
    current_offset += (unsigned)strlen(format) + 1;
    while (*it) {
      it++;
    }
    it++;
    entry.file     = file;
    entry.function = function;
    entry.format   = format;

    uint64 entry_index = array_count(log_remote_entries2);
    array_push(log_remote_entries2, entry, g_telemetry_server.allocator);
    dd_hash64_add(&log_remote_entry_hash, (uint64)address, entry_index,
                  g_telemetry_server.allocator);
  }
}

static void log_process_buffer_data(void* buffer, unsigned byte_count) {
  char line_buffer[2048];
  char* line_buffer_end = line_buffer + sizeof(line_buffer);
  char single_specifier_format_buffer[256];

  uint8* it    = (uint8*)buffer;
  uint8* itEnd = it + byte_count;
  while (it != itEnd) {
    uint8* entry_start  = it;
    void* entry_address = *(void**)it;
    it += sizeof(entry_address);

    uint64 entry_index  = dd_hash64_lookup(&log_remote_entry_hash, (uint64)entry_address, 0);
    dd_log_entry* entry = log_remote_entries2 + entry_index;

    char* next_in_line_buffer = line_buffer;

    const unsigned arg_count = entry->count_args;
#if defined(__x86_64)
    void** line_next_pointer_arg = (void**)it;
    int* line_next_int_arg       = (int*)(line_next_pointer_arg + arg_count);
    double* line_next_double_arg = (double*)(line_next_int_arg + arg_count);
#endif
    const char* file             = entry->file;
    const char* function_name    = entry->function;
    const char* dissected_format = entry->format;
    const char* prev_processed   = dissected_format;
    const char* next_specifier   = prev_processed;
    next_specifier               = strchr(next_specifier, '%');
    while (next_specifier) {
      ++next_specifier;

      // find end character
      while (!((*next_specifier >= 'a' && *next_specifier <= 'z') ||
               (*next_specifier >= 'A' && *next_specifier <= 'Z') || (*next_specifier == '%'))) {
        ++next_specifier;
      }

      strncpy(single_specifier_format_buffer, prev_processed,
              (unsigned)(next_specifier - prev_processed + 1));
      single_specifier_format_buffer[next_specifier - prev_processed + 1] = 0;

      switch (*next_specifier) {
        case 'i':  // Intentional fallthrough
        case 'd':  // Intentional fallthrough
        case 'u':  // Intentional fallthrough
        case 'o':  // Intentional fallthrough
        case 'x':  // Intentional fallthrough
        case 'X':  // Intentional fallthrough
        case 'c': {
#if defined(__x86_64)
          int v = *line_next_int_arg;
          ++line_next_int_arg;
#elif defined(_WIN64)
          int v = *(int*)it;
          it += sizeof(double);
#else
          int v = *(int*)it;
          it += sizeof(int);
#endif
          snprintf(next_in_line_buffer, (unsigned)(line_buffer_end - next_in_line_buffer),
                   single_specifier_format_buffer, v);
          next_in_line_buffer = strchr(next_in_line_buffer, 0);
        } break;
        case 'f':  // Intentional fallthrough
        case 'F':  // Intentional fallthrough
        case 'e':  // Intentional fallthrough
        case 'E':  // Intentional fallthrough
        case 'g':  // Intentional fallthrough
        case 'G':  // Intentional fallthrough
        case 'a':  // Intentional fallthrough
        case 'A': {
#if defined(__x86_64)
          double v = *line_next_double_arg;
          ++line_next_double_arg;
#else
          double v = *(double*)it;
          it += sizeof(double);
#endif
          snprintf(next_in_line_buffer, (unsigned)(line_buffer_end - next_in_line_buffer),
                   single_specifier_format_buffer, v);
          next_in_line_buffer = strchr(next_in_line_buffer, 0);
        } break;
        case 'p': {
#if defined(__x86_64)
          void* v = *line_next_pointer_arg;
          ++line_next_pointer_arg;
#elif defined(_WIN64)
          void* v = *(void**)it;
          it += sizeof(double);
#else
          void* v = *(void**)it;
          it += sizeof(void*);
#endif
          snprintf(next_in_line_buffer, (unsigned)(line_buffer_end - next_in_line_buffer),
                   single_specifier_format_buffer, v);
          next_in_line_buffer = strchr(next_in_line_buffer, 0);
        } break;
        case '%': {
          next_in_line_buffer[0] = '%';
          next_in_line_buffer[1] = '\0';
          ++next_in_line_buffer;
        } break;
        default: {
          assert(FALSE && "Unknown specifier");
        }
      }

      // Do stuff
      prev_processed = next_specifier + 1;
      next_specifier = strchr(prev_processed, '%');
    }
    strcpy(next_in_line_buffer, prev_processed);
    next_in_line_buffer = strchr(next_in_line_buffer, 0);

    log_messages_strings[log_messages_count++] = log_messages + log_messages_offset;
    uint32 count = (uint32)sprintf(log_messages + log_messages_offset, "%s(%i) | %s | %s", file,
                                   entry->line, function_name, line_buffer);
    //(void)file;
    // int count = sprintf(log_messages + log_messages_offset, "(%i) | %s | %s", entry->line,
    //                    function_name, line_buffer);
    // TODO: choose if each individual message should be NULL-terminated or not. It currently
    // depends on how the strings are rendered: in a textarea or manually line by line.
    log_messages_offset += (count + 1);

#if defined(__x86_64)
    it = entry_start + sizeof(entry) + arg_count * (sizeof(int) + sizeof(double) + sizeof(void*));
#else
    it = entry_start + sizeof(entry) + arg_count * sizeof(double);
#endif
  }
}

extern void hub_profiler_process(const uint8* buffer, uint32 buffer_size);
extern void hub_memory_process(const uint8* buffer, uint32 buffer_size);

void handle_log_data(uint8* data) {
  uint8* end_data = data + array_count(data);
  if (end_data == data) return;

  dd_log_remote_message_header_t* header = (dd_log_remote_message_header_t*)data;
  bool is_message_complete               = (unsigned)(end_data - data) >=
                             (sizeof(dd_log_remote_message_header_t) + header->payload_num_bytes);

  if (!is_message_complete) {
    // Need to wait for more data
    return;
  }

  switch (header->message_type) {
    case MSG_LOG_STRINGS: {
      // It's just strings
      const uint8* string_begin       = (uint8*)(header + 1);
      const uint8* string_message_end = string_begin + header->payload_num_bytes;
      const uint8* string_end         = string_begin;
      while (string_end <= string_message_end) {
        while (string_end <= string_message_end) {
          if (*string_end == 0) break;
          ++string_end;
        }
        if (string_end <= string_message_end) {
          printf("%s", string_begin);
          string_end++;
          string_begin = string_end;
        }
      }
      break;
    }
    case MSG_LOG_PROCESS_STRINGS: {
      log_process_buffer_strings(header + 1, header->payload_num_bytes);
    } break;
    case MSG_LOG_PROCESS_DATA: {
      log_process_buffer_data(header + 1, header->payload_num_bytes);
    } break;
    case MSG_PROFILER: {
      hub_profiler_process((uint8*)(header + 1), header->payload_num_bytes);
    } break;
    case MSG_MEMORY: {
      hub_memory_process((uint8*)(header + 1), header->payload_num_bytes);
    } break;
    default: {
      dd_devprintf("Telemetry server received unknown data\n");
    } break;
  }

  // TODO: this only handles a single message at a time
  // dd_devprintf("handled %i with %i bytes\n", header->message_type, header->payload_num_bytes);

  uint8* start_of_unhandled_data =
      data + (sizeof(dd_log_remote_message_header_t) + header->payload_num_bytes);
  unsigned count_unhandled_data = (unsigned)(end_data - start_of_unhandled_data);
  memmove(data, start_of_unhandled_data, count_unhandled_data);
  array_header(data)->count_ = count_unhandled_data;
}

bool should_record      = false;
bool should_record_prev = false;
void telemetry_server_thread_main(void* arg) {
  (void)arg;
  uint16 port                     = 12345;
  dd_network_connection_t* server = dd_network_server_start(port, g_telemetry_server.allocator);
  dd_devprintf("Started telemetry server on port %i\n", port);

  while (isRunning) {
    const size_t num_known_clients       = array_count(hub_clients);
    const unsigned num_connected_clients = dd_network_server_num_connected_clients(server);
    assert(num_known_clients <= num_connected_clients ||
           num_connected_clients == 0);  // TODO: not handling disconnects correctly yet

    while (array_count(hub_clients) < num_connected_clients) {
      dd_hub_client_t hc = {0};
      array_push(hub_clients, hc, g_telemetry_server.allocator);
    }

    for (unsigned i = 0; i < num_connected_clients; ++i) {
      dd_hub_client_t* phc                 = hub_clients + i;
      dd_network_connection_t* client_conn = dd_network_server_get_client_connection(server, i);
      size_t num_bytes_waiting             = dd_network_connection_bytes_received(client_conn);
      if (num_bytes_waiting) {
        size_t current_count = array_count(phc->waiting_data);
        size_t new_count     = current_count + num_bytes_waiting;
        array_resize(phc->waiting_data, new_count, g_telemetry_server.allocator);
        dd_network_connection_read_bytes(client_conn, phc->waiting_data + current_count,
                                         num_bytes_waiting);
      }
      size_t num_bytes_cached = array_count(phc->waiting_data);
      if (num_bytes_cached > 0) {
        handle_log_data(phc->waiting_data);
        //         dd_devprintf("Num bytes waiting: %i after handle %i\n", num_bytes_cached,
        //                      array_count(phc->waiting_data));
      }

      bool did_record_change = should_record != should_record_prev;
      bool is_client_new     = i >= num_known_clients;
      if (did_record_change || (is_client_new && should_record)) {
        dd_log_remote_message_header_t msg[2] = {{MSG_PROFILER_RECORD, 1}};
        *((uint8*)(msg + 1))                  = should_record;
        dd_network_connection_send_data(
            client_conn, msg, sizeof(dd_log_remote_message_header_t) + msg[0].payload_num_bytes);
      }
    }

    should_record_prev = should_record;

    while (array_count(hub_clients) > num_connected_clients) {
      array_pop(hub_clients);  // TODO: No guarantee it's the correct child if there are multiple
                               // connections
      dd_devprintf("client removed\n");
    }

    dd_sleep(1);
  };
}

#if defined(__clang__)
  #pragma clang diagnostic pop
#endif

void dd_telemetry_server_init(struct dd_allocator* allocator) {
  g_telemetry_server.allocator                     = allocator;
  g_telemetry_server.memory.incoming_buffers_mutex = dd_mutex_create();
  dd_thread_api.start(&telemetry_server_thread_main, NULL, "DD Telemetry Server");
}
void dd_telemetry_server_destroy() {
  dd_mutex_destroy(&g_telemetry_server.memory.incoming_buffers_mutex);
}

void dd_telemetry_server_register_profiler_data(const uint8* data, uint32 byte_count) {}
uint8* dd_telemetry_server_get_last_frame() { return NULL; }
uint32 dd_telemetry_server_connected_client_count() { return (uint32)array_count(hub_clients); }
void dd_telemetry_server_enable_recording(bool enable) { should_record = enable; }

void dd_telemetry_server_process_incoming_data(telemetry_server_t* server) {
  struct dd_telemetry_memory_t* memory = &server->memory;

  uint8 stack_buffer[1024];
  dd_allocator* stack_allocator =
      dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));

  struct buf_and_size* buffers_to_process = {0}; /* array */

  // Copy incoming buffer pointers, thread safe
  size_t buffer_count = 0;
  {
    dd_mutex_lock(&memory->incoming_buffers_mutex);
    buffer_count = array_count(memory->incoming_buffers);
    array_resize(buffers_to_process, buffer_count, stack_allocator);
    memcpy(buffers_to_process, memory->incoming_buffers,
           buffer_count * sizeof(*buffers_to_process));
    array_reset(memory->incoming_buffers);
    dd_mutex_release(&memory->incoming_buffers_mutex);
  }

  // Process all the ready buffers
  if (buffer_count > 0) {
    for (uint32 i = 0; i < buffer_count; i++) {
      struct telemetry_memory_processed_t mp = {.data       = buffers_to_process[i].data,
                                                .byte_count = buffers_to_process[i].byte_count};

      uint8* buffer                   = buffers_to_process[i].data;
      uint32 buffer_size              = buffers_to_process[i].byte_count;
      uint8* buffer_it                = buffer;
      uint8* buffer_end               = buffer + buffer_size;
      const uint32 entries_byte_count = *(uint32*)buffer_it;
      dd_devprintf("received memory data %u and %u\n", entries_byte_count, buffer_size);
      buffer_it += sizeof(uint32);
      buffer_end = buffer_it + entries_byte_count;

      uint32 entry_count            = 0;
      int32 memory_delta_byte_count = 0;
      while (buffer_it < buffer_end) {
        uint8 sentinal = *(buffer_it++);
        switch (sentinal) {
          case MEMORY_SENTINAL_THREAD_ID: {
            buffer_it += sizeof(dd_memory_threaddata_t);
          } break;
          case MEMORY_SENTINAL_ALLOC: {
            dd_memory_alloc_data_t* ad = (dd_memory_alloc_data_t*)buffer_it;
            buffer_it += sizeof(dd_memory_alloc_data_t);

            memory->maximum_total_allocation =
                max(memory->maximum_total_allocation, memory->current_total_mem_allocated);

            memory_delta_byte_count += ad->byte_count;
            ++entry_count;
          } break;
          case MEMORY_SENTINAL_FREE: {
            dd_memory_free_data_t* fd = (dd_memory_free_data_t*)buffer_it;
            buffer_it += sizeof(dd_memory_free_data_t);
            memory_delta_byte_count -= fd->byte_count;
            ++entry_count;
          } break;
          default: {
            assert(false && "Unknown memory sentinal");
          } break;
        }
      }

      memory->current_total_mem_allocated += (uint32)memory_delta_byte_count;

      mp.entry_count             = entry_count;
      mp.memory_delta_byte_count = memory_delta_byte_count;
      array_push(memory->stored_buffers, mp, g_telemetry_server.allocator);
    }
  }
}
