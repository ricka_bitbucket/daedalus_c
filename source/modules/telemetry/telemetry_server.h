#pragma once

#include "core/types.h"

// Forward declarations
struct dd_allocator;
typedef struct telemetry_server_t telemetry_server_t;

void dd_telemetry_server_init(struct dd_allocator* allocator);
void dd_telemetry_server_destroy();

void dd_telemetry_server_register_profiler_data(const uint8* data, uint32 byte_count);
uint8* dd_telemetry_server_get_last_frame();
uint32 dd_telemetry_server_connected_client_count();
void dd_telemetry_server_enable_recording(bool enable);

// Call one a frame or so
void dd_telemetry_server_process_incoming_data(telemetry_server_t* server);
