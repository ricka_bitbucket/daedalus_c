#pragma once

#include "core/types.h"

// Need to distinguish between the different types of telemetry:
// time, memory, log
struct dd_allocator;

void dd_telemetry_client_init(struct dd_allocator* allocator);

void dd_telemetry_client_send_profiler_frame();
void dd_telemetry_client_send_memory_data();

// Need to call this regularly (once per frame for example) on each thread.
// It will flush pending data from that thread to the central location
void dd_telemetry_client_flush_threaddata();

void dd_telemetry_client_memory_allocator_init(struct dd_allocator* telemetry_allocator,
                                               struct dd_allocator* wrapped_allocator);
