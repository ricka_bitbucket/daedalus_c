#ifdef __cplusplus
extern "C" {
#endif

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu-zero-variadic-macro-arguments"
#endif

#if defined(_MSC_VER)
__declspec(dllimport) void __cdecl _wassert(_In_z_ wchar_t const* _Message,
                                            _In_z_ wchar_t const* _File, _In_ unsigned _Line);

/**
 * Custom assert that is always on (for now at least).
 */
//#define DD_ASSERT(expression) \
//  (void)((!!(expression)) ||  \
//         (_wassert(_CRT_WIDE(#expression), _CRT_WIDE(__FILE__), (unsigned)(__LINE__)), 0))

#define DD_ASSERT(test, format, ...) ((test) || (dd_devprintf("" format "\n", ##__VA_ARGS__), false))

#else
// Since print statement is void, need to return something, hence the 'false' at the end
#define DD_ASSERT(test, format, ...) ((test) || (dd_devprintf("" format "\n", ##__VA_ARGS__), false))
#endif

#if defined(__clang__)
#pragma clang diagnostic pop
#endif

#ifdef __cplusplus
}
#endif
