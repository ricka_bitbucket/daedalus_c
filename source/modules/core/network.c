#include "network.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "containers.h"
#include "memory.h"
#include "thread.h"
#include "types.h"

#if defined(_WIN32)
#include <winsock2.h>
#include <ws2tcpip.h>
// Include last
#include <Iphlpapi.h>
#pragma comment(lib, "ws2_32.lib")
#else
#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

typedef int SOCKET;
static const int INVALID_SOCKET = -1;
static const int SD_SEND        = 1;
static const int SOCKET_ERROR   = -1;  // TODO: doesn't work, error can be anything smaller than 0

#endif

#define DD_LOG_DISABLE
#include "log.h"

static bool dd_network_is_initialized = false;

dd_mutex_t g_network_mutex;

dd_allocator* g_server_allocator = NULL;
// TODO: need to allow multiple clients, with their own allocators
dd_allocator* g_client_allocator = NULL;

typedef struct dd_network_connection_impl_t {
  // Temporarily made smaller so it fits into void*
  uint16 connection_index;
  uint16 server_index;
  SOCKET socket;
  uint8* incoming_data; /* array */
} dd_network_connection_impl_t;

typedef struct dd_network_client_impl_t {
  dd_network_connection_impl_t connection;
  // Connection needs to be first
  dd_allocator* allocator;
} dd_network_client_impl_t;

typedef struct dd_network_server_impl_t {
  // Have the connection first
  dd_network_connection_impl_t connection;
  // Then other members
  unsigned* connected_clients;                       /* array */
  dd_network_connection_impl_t** client_connections; /* array */
  unsigned num_connected_clients;
} dd_network_server_impl_t;

static dd_network_server_impl_t* server_data; /* array */

void dd_network_close_connection(dd_network_connection_t* connection) {
  dd_network_connection_impl_t* cc = (dd_network_connection_impl_t*)connection;
  const unsigned conn_index        = ((dd_network_connection_impl_t*)connection)->connection_index;

  SOCKET s = cc->socket;
  DD_LOG(ELogSeverityDebug, "reset connection %i\n", conn_index);

  // shutdown the send half of the connection since no more data will be sent
  int result = shutdown(s, SD_SEND);
#ifdef _WIN32
  if (result == SOCKET_ERROR) {
    int last_error = WSAGetLastError();
    if (last_error != WSAENOTCONN) {
      DD_LOG(ELogSeverityError, "shutdown failed: %d\n", WSAGetLastError());
    }
  }

  closesocket(s);
#else
  (void)result;
  close(s);
#endif
  cc->socket = 0;
}

size_t dd_network_connection_bytes_received(dd_network_connection_t* connection) {
  dd_network_connection_impl_t* cc = (dd_network_connection_impl_t*)connection;

  dd_mutex_lock(&g_network_mutex);
  size_t out = array_count(cc->incoming_data);
  dd_mutex_release(&g_network_mutex);
  // TODO: note that it may change between this and getting the data, although it should only
  // increase
  return out;
}

void dd_network_connection_read_bytes(dd_network_connection_t* connection, uint8* out_data,
                                      size_t in_count_bytes) {
  dd_network_connection_impl_t* cc = (dd_network_connection_impl_t*)connection;

  dd_mutex_lock(&g_network_mutex);

  uint8* data_store = cc->incoming_data;
  assert(array_count(data_store) >= in_count_bytes);

  memcpy(out_data, data_store, in_count_bytes);
  // In error cases, the requested data amount is more than the store. Shouldn't crash in that case
  if (in_count_bytes >= array_count(data_store)) {
    const size_t count_bytes_left    = array_count(data_store) - in_count_bytes;
    array_header(data_store)->count_ = count_bytes_left;
    memmove(data_store, data_store + in_count_bytes, count_bytes_left);
  }
  dd_mutex_release(&g_network_mutex);
}

void network_server_process_thread_main(void* connection) {
  dd_network_connection_impl_t* cc = (dd_network_connection_impl_t*)connection;
  unsigned connection_index        = cc->connection_index;

#define DEFAULT_BUFLEN 4096
  SOCKET client_socket = cc->socket;

  char recvbuf[DEFAULT_BUFLEN];
  int iResult;
  int recvbuflen = DEFAULT_BUFLEN;

  // Receive until the peer shuts down the connection
  do {
    // DD_LOG(ELogSeverityDebug, "server waiting to receive data from client connection %i\n",
    //       connection_index);
    iResult = (int)recv(client_socket, recvbuf, (size_t)recvbuflen, 0);
    if (iResult > 0) {
      dd_mutex_lock(&g_network_mutex);

      array_append(cc->incoming_data, recvbuf, (unsigned)iResult, g_server_allocator);
      dd_mutex_release(&g_network_mutex);

      DD_LOG(ELogSeverityDebug, "Bytes received: %d, stored %d\n", iResult,
             array_count(cc->incoming_data));

    } else if (iResult == 0) {
      DD_LOG(ELogSeverityDebug, "Connection closing...\n");
    } else {
#ifdef _WIN32
      int err = WSAGetLastError();
      if (err != WSAEINTR) {
        DD_LOG(ELogSeverityError, "server receive failed: %d\n", err);
        closesocket(client_socket);
      } else {
        // Do nothing. The blocking call was interrupted, perhaps because the socket was
        // closed?
      }
#else
      int err = errno;
      DD_LOG(ELogSeverityError, "server receive failed: %d\n", err);
      close(client_socket);
#endif
    }

  } while (iResult > 0);

  // TODO: Only works as long as no more servers have been made
  bool found_client  = false;
  size_t num_servers = array_count(server_data);
  for (size_t server_idx = 0; server_idx < num_servers && !found_client; server_idx++) {
    dd_network_server_impl_t* serv = &server_data[server_idx];
    size_t num_clients             = array_count(serv->connected_clients);
    for (size_t client_idx = 0; client_idx < num_clients && !found_client; client_idx++) {
      if (serv->connected_clients[client_idx] == connection_index) {
        array_remove_at_index(serv->connected_clients, client_idx);
        serv->num_connected_clients--;
        found_client = true;
      }
    }
  }

  DD_LOG(ELogSeverityDebug, "exiting\n");
}

void network_server_listen_thread_main(void* in_server) {
  dd_network_connection_impl_t* server = (dd_network_connection_impl_t*)in_server;
  SOCKET incoming_client_socket        = INVALID_SOCKET;

  // TODO: Only works as long as no more servers have been made
  dd_network_server_impl_t* serv = &server_data[server->server_index];
  // Accept a client socket
  // This blocks
  SOCKET listen_socket = server->socket;

  do {
    incoming_client_socket = accept(listen_socket, NULL, NULL);
    if (incoming_client_socket == INVALID_SOCKET) {
#if defined(_WIN32)
      int err = WSAGetLastError();
      if (err != WSAEINTR) {
        DD_LOG(ELogSeverityError, "accept failed: %d\n", err);
        closesocket(listen_socket);
      } else {
        // Do nothing. The blocking call was interrupted, perhaps because the server socked was
        // closed?
      }
#else
      if (listen_socket != INVALID_SOCKET) {
        int err = errno;
        DD_LOG(ELogSeverityError, "accept failed: %d\n", err);
      } else {
        // Do nothing, it looks like it was already closed
      }
#endif
    } else {
      dd_mutex_lock(&g_network_mutex);
      size_t client_index = array_count(serv->client_connections);

      dd_network_connection_impl_t* cc =
          dd_alloc(g_server_allocator, sizeof(dd_network_connection_impl_t), 1);
      memset(cc, 0, sizeof(*cc));
      cc->socket           = incoming_client_socket;
      cc->connection_index = (uint16)client_index;
      array_push(serv->client_connections, cc, g_server_allocator);

      array_push(serv->connected_clients, (uint32)client_index, g_server_allocator);
      dd_mutex_release(&g_network_mutex);
      serv->num_connected_clients++;
      DD_LOG(ELogSeverityDebug, "A client connected, now have %i clients\n",
             serv->num_connected_clients);
      dd_thread_api.start(&network_server_process_thread_main, cc, "DD Network Server");
    }
  } while (incoming_client_socket != INVALID_SOCKET);

  DD_LOG(ELogSeverityDebug, "Ending threaded server\n");
}

bool dd_network_initialize_if_needed() {
  if (!dd_network_is_initialized) {
#if defined(_WIN32)
    WSADATA wsa_data;

    // Initialize Winsock
    int result = WSAStartup(MAKEWORD(2, 2), &wsa_data);
    if (result != 0) {
      DD_LOG(ELogSeverityError, "WSAStartup failed: %d\n", result);
      return FALSE;
    }
#endif

    g_network_mutex = dd_mutex_create();

    // Add the zero entry that will be ignored
    if (g_server_allocator) {
      dd_network_server_impl_t s = {0};
      array_push(server_data, s, g_server_allocator);
    }
  }
  return TRUE;
}
dd_network_connection_t* dd_network_server_start(uint16 port, dd_allocator* allocator) {
  g_server_allocator        = allocator;
  dd_network_is_initialized = dd_network_initialize_if_needed();
  if (!dd_network_is_initialized) {
    return NULL;
  }

  struct addrinfo hints;
  struct addrinfo* address;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family   = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;  // For now, only TCP/IP is supported
  hints.ai_flags    = AI_PASSIVE;

  // Resolve the local address and port to be used by the server
  char port_string[64] = {0};
  sprintf(port_string, "%i", port);
  int result = getaddrinfo(NULL, port_string, &hints, &address);
  if (result != 0) {
    DD_LOG(ELogSeverityError, "getaddrinfo failed: %d\n", result);
#if defined(_WIN32)
    WSACleanup();
#else
    assert(false);
#endif
    return NULL;
  }

  SOCKET listen_socket = socket(address->ai_family, address->ai_socktype, address->ai_protocol);
  if (listen_socket == INVALID_SOCKET) {
    freeaddrinfo(address);
#if defined(_WIN32)
    DD_LOG(ELogSeverityError, "Error at socket(): %ld\n", WSAGetLastError());
    WSACleanup();
#else
    assert(false);
#endif
    return NULL;
  }

  // Setup the TCP listening socket
  result = bind(listen_socket, address->ai_addr, (socklen_t)address->ai_addrlen);
  if (result == SOCKET_ERROR) {
    freeaddrinfo(address);
#if defined(_WIN32)
    DD_LOG(ELogSeverityError, "bind failed with error: %d\n", WSAGetLastError());
    WSACleanup();
    closesocket(listen_socket);
#else
    int err = errno;
    DD_LOG(ELogSeverityError, "bind failed with error: %d\n", err);
    close(listen_socket);
#endif
    return NULL;
  }

  freeaddrinfo(address);

  if (listen(listen_socket, SOMAXCONN) == SOCKET_ERROR) {
#if defined(_WIN32)
    DD_LOG(ELogSeverityError, "Listen failed with error: %ld\n", WSAGetLastError());
    closesocket(listen_socket);
    WSACleanup();
#else
    assert(false);
#endif
    return NULL;
  }

  dd_mutex_lock(&g_network_mutex);

  dd_network_server_impl_t s = {0};
  array_push(server_data, s, g_server_allocator);

  dd_network_connection_impl_t* out = dd_alloc(allocator, sizeof(dd_network_connection_impl_t), 1);
  memset(out, 0, sizeof(*out));
  out->socket           = listen_socket;
  out->connection_index = (uint16)0;
  dd_mutex_release(&g_network_mutex);

  out->server_index = (uint16)array_count(server_data) - 1;

  dd_thread_api.start(&network_server_listen_thread_main, (void*)out, "DD Network");

  return (dd_network_connection_t*)out;
}

void network_client_thread_main(void* client_connection) {
  dd_network_client_impl_t* cc = (dd_network_client_impl_t*)client_connection;
  unsigned connection_index    = cc->connection.connection_index;

#define DEFAULT_BUFLEN 4096
  SOCKET client_socket = cc->connection.socket;

  char recvbuf[DEFAULT_BUFLEN];
  int iResult;
  int recvbuflen = DEFAULT_BUFLEN;

  DD_LOG(ELogSeverityDebug, "Client thread started\n");

  // Receive until the peer shuts down the connection
  do {
    DD_LOG(ELogSeverityDebug, "client waiting to receive data %i\n", connection_index);
    iResult = (int)recv(client_socket, recvbuf, (size_t)recvbuflen, 0);
    if (iResult > 0) {
      DD_LOG(ELogSeverityDebug, "Bytes received: %d\n", iResult);
      array_append(cc->connection.incoming_data, recvbuf, (unsigned)iResult, cc->allocator);
    } else if (iResult == 0) {
      DD_LOG(ELogSeverityDebug, "Connection closing...\n");
    } else {
#if defined(_WIN32)
      int err = WSAGetLastError();
      if (err != WSAEINTR) {
        DD_LOG(ELogSeverityError, "client receive failed: %d\n", err);
        closesocket(client_socket);
      } else {
        // Do nothing. The blocking call was interrupted, perhaps because the socket was
        // closed?
      }
#else
      int err = errno;
      DD_LOG(ELogSeverityError, "client receive failed: %d\n", err);
      close(client_socket);
#endif
    }

  } while (iResult > 0);

  DD_LOG(ELogSeverityDebug, "Client thread ended\n");
}

dd_network_connection_t* dd_network_connect(const char* destination, uint16 port,
                                            dd_allocator* allocator) {
  g_client_allocator        = allocator;
  dd_network_is_initialized = dd_network_initialize_if_needed();
  if (!dd_network_is_initialized) {
    return NULL;
  }

  struct addrinfo hints;
  struct addrinfo* address;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family   = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;  // For now, only TCP/IP is supported

  // Resolve the local address and port to be used by the server
  char port_string[64] = {0};
  sprintf(port_string, "%i", port);
  int result = getaddrinfo(destination, port_string, &hints, &address);
  if (result != 0) {
#if defined(_WIN32)
    DD_LOG(ELogSeverityError, "getaddrinfo failed: %d\n", result);
    WSACleanup();
#else
    assert(false);
#endif
    return NULL;
  }

  SOCKET connect_socket = socket(address->ai_family, address->ai_socktype, address->ai_protocol);
  if (connect_socket == INVALID_SOCKET) {
#if defined(_WIN32)
    DD_LOG(ELogSeverityError, "Error at socket(): %ld\n", WSAGetLastError());
    freeaddrinfo(address);
    WSACleanup();
#else
    assert(false);
#endif
    return NULL;
  }

  // Connect to server.
  result = connect(connect_socket, address->ai_addr, (socklen_t)address->ai_addrlen);
  if (result == SOCKET_ERROR) {
#if defined(_WIN32)
    closesocket(connect_socket);
#else
    int err = errno;
    DD_LOG(ELogSeverityError, "client connect failed: %d\n", err);
    close(connect_socket);
#endif
    connect_socket = INVALID_SOCKET;
  }

  // Should really try the next address returned by getaddrinfo
  // if the connect call failed
  // But for this simple example we just free the resources
  // returned by getaddrinfo and print an error message

  freeaddrinfo(address);

  if (connect_socket == INVALID_SOCKET) {
    DD_LOG(ELogSeverityError, "Unable to connect to server!\n");
#if defined(_WIN32)
    WSACleanup();
#endif
    return NULL;
  }

  dd_mutex_lock(&g_network_mutex);
  dd_network_client_impl_t* out = dd_alloc(allocator, sizeof(dd_network_client_impl_t), 1);
  memset(out, 0, sizeof(*out));
  out->connection.socket = connect_socket;
  out->allocator         = allocator;
  dd_mutex_release(&g_network_mutex);

  dd_thread_api.start(&network_client_thread_main, out, "DD Network Client");

  return (dd_network_connection_t*)out;
}

// why don 't  network allocation show in the hub profiling itself?

bool dd_network_is_connection_connected(const dd_network_connection_t* connection) {
  dd_network_connection_impl_t* cc = (dd_network_connection_impl_t*)connection;
  return cc->socket;
}

void dd_network_connection_send_data(dd_network_connection_t* connection, const void* in_data,
                                     const size_t in_count_bytes) {
  dd_network_connection_impl_t* cc = (dd_network_connection_impl_t*)connection;

  SOCKET s = cc->socket;
  if (s != INVALID_SOCKET) {
    int result = (int)send(s, (const char*)in_data, (int)in_count_bytes, 0);
    if (result == SOCKET_ERROR) {
#if defined(_WIN32)
      DD_LOG(ELogSeverityError, "send failed: %d\n", WSAGetLastError());
#else
      assert(false);
#endif
      dd_network_close_connection(connection);
      // closesocket(ClientSocket);
      // WSACleanup();
      // return 1;
    } else {
      DD_LOG(ELogSeverityDebug, "data sent on %i\n", cc->connection_index);
    }
  }
}

unsigned dd_network_server_num_connected_clients(const dd_network_connection_t* server) {
  const unsigned serv_index = ((dd_network_connection_impl_t*)server)->server_index;
  assert(serv_index != 0);
  const dd_network_server_impl_t* serv = &server_data[serv_index];

  return serv->num_connected_clients;
}

dd_network_connection_t* dd_network_server_get_client_connection(
    const dd_network_connection_t* server, const unsigned index) {
  const unsigned serv_index = ((dd_network_connection_impl_t*)server)->server_index;
  assert(serv_index != 0);
  const dd_network_server_impl_t* serv = &server_data[serv_index];

  return (dd_network_connection_t*)(serv->client_connections[index]);
}
