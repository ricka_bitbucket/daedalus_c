// Daedalus Development: code taken from https://arstechnica.com/civis/viewtopic.php?f=20&t=240425
#include "timer.h"

//*******************************************************************************
//*  YOU MUST SUPPLY A SYSTEM SPECIFIC TIMING FUNCTION WHICH RETURNS MICROSECS  *
//* This number need only be a correctly incrementing value, its absolute value *
//* is irrelevant,  because it is never used for anything except differences.   *
//* But this number must increment correctly across all larger time-boundaries, *
//* and arithmetic must be correctly modulo (TIMER_INT +1) ... although that    *
//* rollover should be rare!                                                    *
//*******************************************************************************
//
// IN GENERAL, MOST SYSTEMS HAVE SPECIAL SYSTEM CALLS WHICH ARE BEST USED.  THE
// DEFAULT DEFINITION BELOW CALLS THE POSIX FUNCTION gettimeofday() ... which
// may be adequate, or may be the best you can do!
//
// See the function TestTimer (below) it is called by main.c ... which checks that
// this function is running and (reasonably) well-behaved, before a program using
// it continues too far.
//
#if (defined(__APPLE__) && defined(IF_APPLE_USE_CARBON_API))

  //  Build dd_time_microseconds() on MacOS systems ..............
  //
  // CoreServices supplies Microsecond for Carbon interface on Mac.  This
  // little chunk here is all that is os-system specific.
  //
  // This is just a little wrapper function to give nicer interface behavior to
  // the CoreServices function which returns an "unsigned wide", only the lower
  // word of which do we need. It returns a TIMER_INT which is "time" in microsec;
  // it is used only to compute differences, so absolute time is irrelevant.
  //
  // NOTE: this routine is provided here for Macs, in the even that the code
  // is being compiled on compilers which support Apple's libaries, but not
  // POSIX (e.g. for os9, etc).  The call to a standard POSIX routine below
  // also works on osX Macs.
  //
  // This routine is a trivial modification of a routine in the Apple
  // documentation, and is public domain.
  #include <CoreServices/CoreServices.h>
// Used to be getMuSec
uint64 dd_time_microseconds(void) {
  unsigned long t[2];  // apple defines it this way
  Microseconds((UnsignedWide*)&t);
  return t[1];
}

#elif defined(_MSC_VER)

  //****************************************************************************
  // _MSC_VER is the defined identifier for Microsoft Visual C++ .. this version
  // intended for Windows applications.
  // ArsTechnica pseudnoymous donor "PeterB" contributed this routine, which
  // links to a Windows system call.  We presume it to be donated to the public
  // domain.  The divides in this routine may be a peformance difficulty.
  // This version has corrections made by "bash666" Feb 3, 2005
  #include <Windows.h>

// Used to be getMuSec
uint64 dd_time_microseconds() {
  static LARGE_INTEGER timestamp = {0};
  LARGE_INTEGER frequency        = {0};
  if (frequency.QuadPart == 0) {
    QueryPerformanceFrequency(&frequency);
    frequency.QuadPart /= 1000000;
  }
  QueryPerformanceCounter(&timestamp);
  return (uint64)(timestamp.QuadPart / frequency.QuadPart);
}

dd_datetime_t dd_datetime() {
  SYSTEMTIME time;
  GetLocalTime(&time);

  return (dd_datetime_t){.year   = (uint16)time.wYear,
                         .month  = (uint8)time.wMonth,
                         .day    = (uint8)time.wDay,
                         .hour   = (uint8)time.wHour,
                         .minute = (uint8)time.wMinute,
                         .second = (uint8)time.wSecond};
}

#else

// ************************* UNIX/ POSIX COMPLIANT DEFAULT ***********************
// * This definition uses the POSIX function gettimeofday().  It may or may not  *
// * be satisfactory, depending on the system implementation, particularly how   *
// * frequently the tp.usec value is updated.                                    *
// *                                                                             *
// * This needs to be carefully tested on each system if used (due to issues of  *
// * the tzp usage, and also 'tick' granularity).  If 'ticks' are larger than 10 *
// * microseconds (POSIX only requires they be 0.01 sec or finer) then the bench *
// * will be compromised unless run times are increased commensurately.          *
// *                                                                             *
// *******************************************************************************
//
// The structures pointed to by tp and tzp are defined in <sys/time.h> as:
// struct timeval {
// long tv_sec; /* seconds since Jan. 1, 1970 */
// long tv_usec; /* and microseconds */
//
// note that tzp is archaic and has been derogated ...  Some very old
// implementations require a value of tzp be set or they will not return a time.
//
// also note the routine doesn't attempt to use the returned error/no-error,
// again archaic implementations varied .. if it isn't returning good data,
// you'll know quickly!
//
// This routine is  public domain.

  #include <sys/time.h>

// Used to be getMuSec
uint64 dd_time_microseconds(void) {
  struct timeval tp;
  struct timezone tzp;
  tzp.tz_minuteswest = 0;
  (void)gettimeofday(&tp, &tzp);
  return 1000000u * (uint64)tp.tv_sec + (uint64)tp.tv_usec;
}

dd_datetime_t dd_datetime() {
  time_t t;
  struct timeval tv;
  struct tm* info;
  char buffer[64];

  gettimeofday(&tv, NULL);
  t = tv.tv_sec;

  info = localtime(&t);

  return (dd_datetime_t){.year   = (uint16)info->tm_year + 1900,
                         .month  = (uint8)info->tm_mon + 1,
                         .day    = (uint8)info->tm_mday,
                         .hour   = (uint8)info->tm_hour,
                         .minute = (uint8)info->tm_min,
                         .second = (uint8)info->tm_sec};
}

#endif

#if 0
// TestTimer returns the timer tick increment in microseconds, and generally
// guards against functionless timers.  It will return zero if the timer is
// not running (running too slowly to be useful), or has erratic properties.
//
// Note that "erratic" includes being too slow in comparison to tasker-swaps,
// and a fairly arbitrary criterion is set below that 1/2 of the observed
// time-steps must be the minimum or the minimum plus 1.
//
// If the struct TimerAnalysis * ta variable is passed != NULL, it will return
// the specified analysis variables
//
// Routine written by Lee Harrison, and contributed to the public domain.
// TICS_TO_SAMPLE should be _significantly_ bigger than 256

// NOTE THAT THESE DEFINITIONS ARE FOR STANDARD 32-bit addressing C, where
// sizeof(int) = sizeof(char *) = 4, and sizeof(long long) = 8
//
// You may need to redefine these so that they FORCE the stated
// storage sizes which are their intent
  #define INT64BITS long long  // 64 bit integer ... etc as follows
  #define INT32BITS int
  #define INT16BITS short
  #define INT08BITS char
  #define FLOAT32BITS float
  #define FLOAT64BITS double
// warning -- this may be "long long" on 64bit hardware...
  #define POINTERSIZEINT int
// TIMER_INT is defined solely so that it is easy to find and change all
// references to timer values being passed, if in the future there is need
// to change this, or need to fiddle with it to accomodate particular ports.
// If you are simply porting code without changing getMUSec() etc, there
// should be no reason to change this.
  #define TIMER_INT uint64

  #ifndef TICS_TO_SAMPLE
    #define TICS_TO_SAMPLE 10000
struct TimerAnalysis {
  TIMER_INT minimum_tic;
  float minimum_tic_fraction_observed;
  float minimum_tic_plus1_fraction_observed;
};
  #endif

TIMER_INT TestTimer(TIMER_INT bailout_microseconds,
                    struct TimerAnalysis* ta) {  // can be NULL if desired
  register TIMER_INT t1, t2, diff_t, min_t;
  register unsigned INT32BITS i, count_min_t, count_min_tp1, count_LTmin_t;

  //
  if (ta != NULL) {
    ta->minimum_tic                   = 0;
    ta->minimum_tic_fraction_observed = ta->minimum_tic_plus1_fraction_observed = 0.0;
  }
  // first test that timer is running.... Note that if it
  // ISN'T running, this loop will trap for 2^^32 -1 loop
  // cycles ...
  i  = 0xFFFFFFFF;
  t1 = dd_time_microseconds();

  do {
    t2 = dd_time_microseconds();
    if (t2 != t1) break;
  }

  while (--i);
  if (i == 0) return i;  // timer doesn't run or runs FAR too slowly to be useful

  // this is first estimate of min_t ...
  min_t = t2 - t1;

  // run a loop of 1000 observations of time-ticks to catch any
  // "reasonably frequent" smaller minimum difference... if
  // time-ticks were very slow this could take awhile too...

  i  = 1000;
  t1 = dd_time_microseconds();
  do {
    diff_t = dd_time_microseconds() - t1;
    if (diff_t) {  // found a time-step

      if (diff_t < min_t) min_t = diff_t;
      --i;
      t1 += diff_t;
    }
  } while (i);

  // if min_t detected here is "unreasonably large" by the
  // caller's expectations... bailout here because the next
  // loop would get really tedious...  note that in this case
  // ta->minimum_tic returns the tic observed, but the fractions
  // will be zero.

  if (min_t > bailout_microseconds) {
    if (ta != NULL) ta->minimum_tic = min_t;

    return 0;
  }

  // at this point min_t is probably the tick size, but might not
  // be for a variety of reasons.  The following test watches 10000
  // timer  transitions, and counts the number which match a minimum
  // transition, which had better be a LARGE fraction of the
  // transitions, but won't be all in any multitasking environment.
  // Some minimum-tick transitions will be missed due to interrupts;
  // this will be more likely in single-processor systems and those
  // with unpleasant hoggy drivers etc.
  //
  // There is ALSO the possibility that the underlying timer function
  // is a timer running from a time-base which doesn't produce a tidy
  // modulo with microseconds ... such that minimum time ticks might
  // "beat" between two minimums, different by 1.
  //
  // And there is a possibility... of finding rare ticks which are
  // a smaller time-step.  What to do about these if found is a
  // problem, because the loop shouldn't become endless.  The scheme
  // taken here will retry the loop if more than 1/8th of the detected
  // transitions are below min_t ... this is very unlikely to be possible
  // given the sampling loop above which "found" min_t
retry:

  count_min_t = count_min_tp1 = count_LTmin_t = 0;
  i                                           = TICS_TO_SAMPLE;
  t1                                          = dd_time_microseconds();

  do {
    t2 = dd_time_microseconds();

    if (t2 != t1) {
      i--;
      diff_t = t2 - t1;
      t1     = t2;

      if (diff_t == min_t)
        count_min_t++;
      else if (diff_t == min_t + 1)
        count_min_tp1++;
      else if (diff_t < min_t)
        count_LTmin_t++;

      if ((count_LTmin_t > 3) && ((count_LTmin_t << 3) > count_min_t)) {
        min_t = diff_t;

        goto retry;
      }

      t1 = dd_time_microseconds();  // so that an interrupt in the above code is not counted
    }
  } while (i);

  if (ta != NULL) {
    ta->minimum_tic                         = min_t;
    ta->minimum_tic_fraction_observed       = (float)count_min_t / (float)TICS_TO_SAMPLE;
    ta->minimum_tic_plus1_fraction_observed = (float)count_min_tp1 / (float)TICS_TO_SAMPLE;
  }

  if (count_min_t + count_min_tp1 < (TICS_TO_SAMPLE >> 1))
    return 0;
  else
    return min_t;
}
#endif
