#pragma once

#ifdef __cplusplus
extern "C" {
#else
  #include <stdbool.h>
  #include <stdio.h>
#endif

#ifndef NULL
  #define NULL 0
#endif
#ifndef TRUE
  #define TRUE 1
  #define FALSE 0
#endif

// Sized types
typedef unsigned char uint8;
typedef signed char int8;
typedef unsigned short uint16;
typedef signed short int16;
typedef unsigned int uint32;
typedef signed int int32;
typedef unsigned long long uint64;
typedef signed long long int64;

// Math types
typedef struct float2 {
  float x, y;
} float2;
typedef struct float3 {
  float x, y, z;
} float3;
typedef struct float4 {
  float x, y, z, w;
} float4;

// 8-bit per channel, 4 channel color
typedef struct color_rgba8_t {
  uint8 r, g, b, a;
} color_rgba8_t;

/// A struct that wraps a string combining it with the length of the string.
///
/// Can be used to view a part of a string, or the entire thing.
///
/// Does NOT own the memory pointed at.
typedef struct dd_stringview_t {
  const char* string;
  size_t length;
} dd_stringview_t;

/// \brief Type to clearly indicate when an array is used vs a pointer
///        Relates to the functionality in containers.h
#define array(type) type*

/************************************************************************/
/* Helper/implementation functions                                      */
/************************************************************************/
#ifndef macro_var
  #define macro_concat_impl(a, b) a##b
  #define macro_concat(a, b) macro_concat_impl(a, b)
  #define macro_var(name) macro_concat(name, __LINE__)
#endif

#define defer(start, end) \
  for (int macro_var(i) = (start, 0); !macro_var(i); (macro_var(i) += 1), end)

#ifdef __cplusplus
}
#endif
