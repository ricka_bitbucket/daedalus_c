#include "filesystem.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
#else
  #include <dirent.h>
  #include <errno.h>
  #include <sys/stat.h>
  #include <unistd.h>
#endif

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "containers.h"
#include "memory.h"
#include "utils.h"

#if !defined(MAX_PATH)
  #define MAX_PATH 512
#endif

#if defined(_WIN32)
static uint64 filesystem_file_last_modified(const char* path) {
  HANDLE hFile = CreateFile(path, 0, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
  if (hFile == INVALID_HANDLE_VALUE) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to create file '%s':\n %s\n", path, err);
    return 0;
  }

  FILETIME time;
  GetFileTime(hFile, NULL, NULL, &time);

  CloseHandle(hFile);

  return *(uint64*)&time;
}

static bool filesystem_remove_file(const char* path) {
  BOOL result = DeleteFileA(path);
  if (result == 0) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to remove file '%s':\n %s\n", path, err);
  }

  return (result != 0);
}

static bool filesystem_copy_file(const char* from_path, const char* to_path,
                                 enum ECopyFileFlag flags) {
  const BOOL failIfExists = (flags & ECopyFileFlagOverwriteExisting) ? FALSE : TRUE;
  BOOL result             = CopyFile(from_path, to_path, failIfExists);
  if (result == 0) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Couldn't copy '%s' to '%s':\n %s\n", from_path, to_path, err);
    printf("Couldn't copy '%s' to '%s':\n %s\n", from_path, to_path, err);
    return false;
  }

  return true;
}

static bool filesystem_move_file(const char* from_path, const char* to_path,
                                 enum ECopyFileFlag flags) {
  const DWORD mf_flags = (flags & ECopyFileFlagOverwriteExisting) ? MOVEFILE_REPLACE_EXISTING : 0;
  BOOL result          = MoveFileEx(from_path, to_path, mf_flags);
  if (result == 0) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Couldn't move '%s' to '%s':\n %s\n", from_path, to_path, err);
    printf("Couldn't move '%s' to '%s':\n %s\n", from_path, to_path, err);
    return false;
  }

  return true;
}

static dd_stringlist_t filesystem_directory_contents(const char* path,
                                                     struct dd_allocator* allocator) {
  uint32 count      = 0;
  array(char) names = NULL;

  // Copy the folder into dll_path
  WIN32_FIND_DATAA fd;
  HANDLE h = FindFirstFileA(path, &fd);
  if (h != INVALID_HANDLE_VALUE) {
    do {
      if (strcmp(".", (char*)fd.cFileName) == 0 || strcmp("..", (char*)fd.cFileName) == 0) {
        continue;
      }
      const bool is_folder = (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
      if (!is_folder) {
        array_append(names, fd.cFileName, strlen(fd.cFileName) + 1, allocator);
        ++count;
      }
    } while (FindNextFileA(h, &fd));
  }
  FindClose(h);
  (void)GetLastError();  // Clear the ERROR_NO_MORE_FILES

  size_t offset_to_pointers = array_count(names);
  array_resize(names, offset_to_pointers + count * sizeof(void*), allocator);
  char** pointers = (char**)(names + offset_to_pointers);
  char* p         = names;
  size_t i        = 0;
  while ((void*)p < (void*)pointers) {
    pointers[i] = p;
    i++;
    p += strlen(p) + 1;
  }

  return (dd_stringlist_t){.count = count, .payload = names, .strings = pointers};
}
#else
static uint64 filesystem_file_last_modified(const char* path) {
  // Check time
  struct stat attrib;
  stat(path, &attrib);
  uint64 last_modified =
  #if defined(_POSIX_C_SOURCE)
      attrib.st_mtime.tv_sec;
  #else
      attrib.st_mtimespec.tv_sec;
  #endif
  return last_modified;
}

static bool filesystem_remove_file(const char* path) {
  if (!unlink(path)) {
    if (errno != ENOENT) {
      dd_devprintf("couldn't unlink '%s', error %s\n", path, strerror(errno));
    }
  }
}

static bool filesystem_copy_file(const char* from_path, const char* to_path,
                                 enum ECopyFileFlag flags) {
  if (flags & ECopyFileFlagOverwriteExisting) {
    (void)remove(to_path);
    // Ignore errors here
  }

  if (!link(from_path, to_path)) {
    dd_devprintf("Couldn't copy '%s' to '%s', error %s\n", from_path, to_path, strerror(errno));
    return false;
  }
  return true;
}

static int dll_select(const struct dirent* entry) {
  return (strcmp(entry->d_name, ".") != 0) && (strcmp(entry->d_name, "..") != 0) &&
         (strstr(entry->d_name, ".dylib") != NULL);
}
static dd_stringlist_t filesystem_directory_contents(const char* path,
                                                     struct dd_allocator* allocator) {
  uint32 count      = 0;
  array(char) names = NULL;

  struct dirent** namelist;
  int numEntries = scandir(path, &namelist, &dll_select, 0);
  struct stat filestat;
  char full_path[MAX_PATH];
  bool isDir;

  if (numEntries > 0) {
    for (size_t f = 0; f < (size_t)numEntries; ++f) {
      const char* file_name = namelist[f]->d_name;
      snprintf(full_path, sizeof(full_path), "%s%s", path, file_name);
      lstat(full_path, &filestat);

      isDir = ((filestat.st_mode & S_IFDIR) != 0);
      if (isDir) {
      } else {
        array_append(names, file_name, strlen(file_name) + 1, allocator);
        ++count;
      }
    }

    for (size_t f = 0; f < (size_t)numEntries; ++f) {
      free(namelist[f]);
    }
    free(namelist);
  }

  size_t offset_to_pointers = array_count(names);
  array_resize(names, offset_to_pointers + count * sizeof(void*), allocator);
  char** pointers = (char**)(names + offset_to_pointers);
  char* p         = names;
  size_t i        = 0;
  while ((void*)p < (void*)pointers) {
    pointers[i] = p;
    i++;
    p += strlen(p) + 1;
  }

  return (dd_stringlist_t){.count = count, .payload = names, .strings = pointers};
}
#endif



#if defined(_WIN32)
static bool filesystem_file_exists(const char* file_path) {
  bool exists = false;

  WIN32_FIND_DATA fd;
  HANDLE hnd = FindFirstFile(file_path, &fd);
  if (hnd != INVALID_HANDLE_VALUE) {
    exists = true;
    FindClose(hnd);
  }

  return exists;
}
#else
static bool filesystem_file_exists(const char* file_path) {
  bool exists = false;

  FILE* f = fopen(file_path, "rb");
  if (f) {
    fclose(f);
    exists = true;
  }

  return exists;
}
#endif

const struct dd_filesystem_api dd_filesystem_api = {
    .file_exists        = &filesystem_file_exists,
    .remove_file        = &filesystem_remove_file,
    .copy_file          = &filesystem_copy_file,
    .move_file          = &filesystem_move_file,
    .directory_contents = &filesystem_directory_contents,
    .file_last_modified = filesystem_file_last_modified};
