/*
Dynamic library must implement this function (with same exact name)

bool plugin_link_apis(const struct dd_api_registry_t* registry,
                      struct dd_allocator* allocator);

*/

#pragma once

#include "types.h"

#define API_NAME_MAX_LENGTH 32

/*
Make a unique PDB filename, so linking doesn't want to overwrite while debugging is happening
/PDB:$(OutDir)$(TargetName)_$([System.Guid]::NewGuid()).pdb
/PDB:$(OutDir)$(TargetName)_$([System.DateTime]::Now.ToString("yyyyMMdd_HHmmss")).pdb
*/

typedef struct dd_plugin_t {
  void* new_dll_handle;
  void* old_dll_handle;
  uint64 modification_time;
  uint8 reload_count;

  uint8 old_revision;
  char new_path[512];
} dd_plugin_t;

struct dd_allocator;
struct dd_plugins_o;

struct dd_plugins_api {
  struct dd_plugins_o* (*create)(struct dd_allocator* allocator);

  void (*load_plugins)(struct dd_plugins_o* obj, const char** paths, uint32 path_count);

  void (*check_for_changes)(struct dd_plugins_o* obj);

  void (*unload_all)(struct dd_plugins_o* obj);
};

extern const struct dd_plugins_api dd_plugins_api;
