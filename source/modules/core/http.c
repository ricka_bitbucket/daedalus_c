#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
  #include <urlmon.h>
  #pragma comment(lib, "Urlmon.lib")
#else
#endif

#include <stdlib.h>
#include <string.h>

// TODO: In the future want to reuse the Background download thread
#include "http.h"

#include "memory.h"
#include "thread.h"
#include "utils.h"

struct file_download_settings {
  char url[512];
  char file_path[512];
};

#if defined(_WIN32)

static void http_background_download_(struct file_download_settings* fd) {
  HRESULT res = URLDownloadToFile(NULL, fd->url, fd->file_path, 0, NULL);
  dd_devprintf("result from download: %i\n", res);
  free(fd);
}

#else

extern void http_background_download_(struct file_download_settings* fd);

#endif


static void http_url_to_file(const char* url, const char* file_path) {
  struct file_download_settings* fd = malloc(sizeof(struct file_download_settings));
  strcpy(fd->url, url);
  strcpy(fd->file_path, file_path);

  dd_thread_api.start((dd_thread_entry_f)http_background_download_, fd, "DD Background Downloader");
}

const struct dd_http_api dd_http_api = {.url_to_file = http_url_to_file};
