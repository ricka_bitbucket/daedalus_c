/*
Dynamic library must implement this function (with same exact name)

bool plugin_link_apis(const struct dd_plugins_api* plugins_api,
                      struct dd_plugins_o* plugins, struct dd_allocator* allocator);

*/
#pragma once

#include "types.h"

#define API_NAME_MAX_LENGTH 32

struct dd_allocator;

typedef struct dd_api_registry_t {
  void (*init)(struct dd_allocator* allocator);
  void (*deinit)(void);

  void (*register_api)(const char* name, uint32 version, const void* api, uint32 api_byte_count);

  void* (*get_api)(const char* name, uint32 version);
} dd_api_registry_t;

extern const dd_api_registry_t dd_api_registry;
