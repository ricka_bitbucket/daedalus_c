#pragma once

#include "types.h"

typedef struct {
  uint8 opaque_[64];
} dd_mutex_t;

dd_mutex_t dd_mutex_create();
void dd_mutex_lock(dd_mutex_t* mutex);
void dd_mutex_release(dd_mutex_t* mutex);
void dd_mutex_destroy(dd_mutex_t* mutex);

uint64 dd_thread_id();

#if defined(_WIN32)
  #define THREAD_LOCAL __declspec(thread)
#else
  #define THREAD_LOCAL _Thread_local
#endif

typedef void (*dd_thread_entry_f)(void* user_data);

typedef struct dd_thread_t {
  uint32 opaque_[32];
} dd_thread_t;

struct dd_thread_api {
  dd_thread_t (*start)(dd_thread_entry_f entry_func, void* user_data, const char* debug_name);
};

extern const struct dd_thread_api dd_thread_api;
