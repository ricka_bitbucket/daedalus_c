
#pragma once

#include "types.h"

struct dd_allocator;

enum ECopyFileFlag { ECopyFileFlagOverwriteExisting = 1 << 0 };

typedef struct dd_stringlist_t {
  uint32 count;
  char* payload;
  char** strings;
} dd_stringlist_t;

struct dd_filesystem_api {
  bool (*file_exists)(const char* file_path);

  bool (*remove_file)(const char* path);

  bool (*copy_file)(const char* from_path, const char* to_path, enum ECopyFileFlag flags);

  bool (*move_file)(const char* from_path, const char* to_path, enum ECopyFileFlag flags);

  dd_stringlist_t (*directory_contents)(const char* path, struct dd_allocator* allocator);

  uint64 (*file_last_modified)(const char* path);
};

extern const struct dd_filesystem_api dd_filesystem_api;
