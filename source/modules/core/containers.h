#pragma once

#include "memory.inl"
#include "types.h"

// Forward declarations
struct dd_allocator;
struct allocation_location;

#define countof(a) (sizeof(a) / sizeof(a[0]))

typedef struct dd_array_header_t {
  size_t count_;
  size_t capacity_;
} dd_array_header_t;

void* array_grow(void* a, size_t element_size, struct dd_allocator* allocator,
                 const char* file_path, const char* function_name, size_t line_number);
void* array_reserve_impl(void* a, size_t element_size, size_t new_capacity,
                         struct dd_allocator* allocator, const char* file_path,
                         const char* function_name, size_t line_number);
void array_free(void*, struct dd_allocator* allocator);


#define array_header(a) ((a!=0) ? ((dd_array_header_t*)((char*)(a) - sizeof(dd_array_header_t))) : 0)

#define array_count(a) ((a!=0) ? array_header(a)->count_ : 0)
#define array_capacity(a) ((a!=0) ? array_header(a)->capacity_ : 0)
#define array_reset(a) ((a!=0) ? array_header(a)->count_ = 0 : 0)
#define array_is_full(a) ((a!=0) ? (array_header(a)->count_ == array_header(a)->capacity_) : true)

#define array_reserve(a, new_capacity, allocator)                                              \
  *((void**)&(a)) = array_reserve_impl(a, sizeof(*(a)), (new_capacity), (allocator), __FILE__, \
                                       __FUNCTION__, __LINE__)

#define array_push(a, item, allocator)                                                   \
  (array_is_full(a) ? (*((void**)&(a)) = array_grow((void*)(a), sizeof(*(a)), allocator, \
                                                    __FILE__, __FUNCTION__, __LINE__))   \
                    : 0,                                                                 \
   ((a)[array_header(a)->count_++] = (item)))

#define array_push_no_alloc(a, item) \
  (assert(!array_is_full(a)), ((a)[array_header(a)->count_++] = (item)))

#define array_pop(a) array_header(a)->count_--

#define array_remove_at_index(a, idx)                                               \
  ((a!=0) ? (memmove(a + idx, a + idx + 1, (array_count(a) - idx - 1) * sizeof(*(a))), \
          array_header(a)->count_--)                                                \
       : 0)

#define array_append(a, data, count, allocator)                                               \
  (((array_count(a) + (count)) > array_capacity(a))                                           \
       ? array_reserve((a),                                                                   \
                       (array_count(a) + (count)) > (array_count(a) * 3 / 2)                  \
                           ? (array_count(a) + (count))                                       \
                           : (array_count(a) * 3 / 2),                                        \
                       allocator)                                                             \
       : 0,                                                                                   \
   (((count) > 0) ? (memcpy(a + array_header(a)->count_, data, ((count) + 0) * sizeof(*(a)))) \
                  : 0),                                                                       \
   (((count) > 0) ? array_header(a)->count_ = array_header(a)->count_ + (count) : 0))

#define array_resize(a, count, allocator)                                   \
  (((count) > array_count(a)) ? array_reserve((a), (count), allocator) : 0, \
   ((array_header(a) != NULL) ? (array_header(a)->count_ = (count)) : 0))

/**
 *  Hash map
 */
typedef struct dd_hash64_t {
  uint64* keys;
  uint64* values;
  uint32 value_count;
  uint32 bucket_count;
} dd_hash64_t;

uint64 dd_hash64_lookup(const dd_hash64_t* hash, const uint64 key, uint64 default_value);
void dd_hash64_set(const dd_hash64_t* hash, const uint64 key, uint64 new_value);
void dd_hash64_add_impl(dd_hash64_t* h, const uint64 key, uint64 value,
                        struct dd_allocator* allocator, const char* file_path,
                        const char* function_name, size_t line_number);
#define dd_hash64_add(hash, key, value, allocator) \
  dd_hash64_add_impl(hash, key, value, allocator, __FILE__, __FUNCTION__, __LINE__)
void dd_hash64_remove(dd_hash64_t* h, const uint64 key);
void dd_hash64_free(dd_hash64_t* hash, struct dd_allocator* allocator);

uint64 murmur_hash_64(const void* key, uint64 len, uint64 seed);

/**
 *  Algorithm like functions
 */

// Find a string 'needle' in the list of strings 'haystack' returning the
// index found, or -1 if not found
int32 dd_is_string_in_list(const char* needle, const char** haystack, uint32 haystack_count);

// Some comments on the code
//
// ((count)+0)  For some array functions it is useful to call them with sizeof(something). VS
//              Analyze comments on that, because that argument is sometimes multiplied by a
//              sizeof(*a). Adding 0 circumvents that warning.

// Watching an array in Visual Studio is easy if you know how many elements it has:
//
// array(type) my_array;    --> "my_array, 5" if it has 5 elements
//
// If you don't know the count you can view that with the following watch:
//
// (((dd_array_header_t*)(my_array))-1)->count_

