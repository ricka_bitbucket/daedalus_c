// Daedalus headers
#include "application.h"

#include "containers.h"
#include "dd_string.h"

#if defined(_WIN32)

  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>

static dd_stringview_t application_executable_folder() {
  static char buf[256] = {0};
  GetModuleFileNameA(0, buf, countof(buf) - 1);

  char* it             = buf;
  char* last_separator = NULL;

  // Replace all \ with /
  while (*it) {
    if (*it == '\\') {
      *it            = '/';
      last_separator = it;
    }
    it++;
  }

  // Cut the binary name so only a path is left, ending with a slash
  last_separator[1] = 0;

  return (dd_stringview_t){.string = buf, .length = (last_separator-buf+1)};
}

#else
static dd_stringview_t application_executable_folder() {
  static char buf[256] = {0};
  return (dd_stringview_t){.string = buf, .length=0};
}
#endif

const struct dd_application_api dd_application = {.executable_folder = &application_executable_folder};
