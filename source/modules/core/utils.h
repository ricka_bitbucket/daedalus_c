#pragma once

#include "dd_string.h"

typedef struct dd_allocator dd_allocator;

void dd_sleep(unsigned count_milliseconds);
void dd_devprintf(const char* format, ...);

#if defined(_WIN32)
  #if defined(_WIN64)
extern __declspec(dllimport) void __cdecl DebugBreak();
  #else
extern __declspec(dllimport) void __stdcall DebugBreak();
  #endif
  #define dd_debug_break() DebugBreak()
#else
  #if PLATFORM_IOS || PLATFORM_TVOS
    #define dd_debug_break() assert(false)  // Unimplemented
  #else
    #define dd_debug_break() __asm__("int $3")
  #endif
#endif

uint8* read_file(const char* file_path, dd_allocator* allocator);
char* string_trim(char* in);

static inline uint32 pack_uint10x3(uint32 a, uint32 b, uint32 c) {
  return ((a & 0x3FF) << 0) | ((b & 0x3FF) << 10) | ((c & 0x3FF) << 20);
}

static inline void unpack_uint10x3(const uint32 in_value, uint32* a, uint32* b, uint32* c) {
  *a = (in_value >> 0) & 0x3FF;
  *b = (in_value >> 10) & 0x3FF;
  *c = (in_value >> 20) & 0x3FF;
}