#import <AppKit/AppKit.h>

#include "containers.h"
#include "os.h"
#include "types.h"

static const int macos_types[] = {0, 0};

static void os_clipboard_set_data(dd_clipboard_t type, const void* data, size_t data_byte_count) {
  [NSPasteboard.generalPasteboard declareTypes:[NSArray arrayWithObject:NSPasteboardTypeString]
                                         owner:nil];
  (void)[NSPasteboard.generalPasteboard setString:[NSString stringWithUTF8String:(const char*)data]
                                          forType:NSPasteboardTypeString];
}

static char* os_clipboard_get_data(dd_clipboard_t type, struct dd_allocator* allocator) {
  char* out_data = NULL;

  switch (type) {
    case EClipboardText: {
      NSString* str = [NSPasteboard.generalPasteboard stringForType:NSPasteboardTypeString];
      if (str) {
        size_t len = str.length + 1;
        array_resize(out_data, len, allocator);
        memcpy(out_data, str.UTF8String, len);
      }
    } break;
    case EClipboardEmpty:
      break;
  }

  return out_data;
}

const struct dd_os_api dd_os_api = {
    .clipboard = {.set_data = &os_clipboard_set_data, .get_data = &os_clipboard_get_data}};
