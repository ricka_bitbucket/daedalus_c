#pragma once

#include "types.h"

//////////////////////////////////////////////////////////////////////////
// Daedalus Data Format
//
// Never store things twice. Store an array of something once in the file, and reference it with
// indexes.
//////////////////////////////////////////////////////////////////////////

struct dd_allocator;

typedef struct ddf_header_t {
  char identifier[4];  // Should contain 'D','D','F', '\0'
  uint32 version;
  uint32 block_count;
} ddf_header_t;

typedef struct ddf_block_descriptor_t {
  const char* label;
  const char* format;
  uint32 data_offset;
  uint32 data_byte_count;
} ddf_block_descriptor_t;

struct ddf_api {
  struct {
    ddf_header_t (*header)(const uint8* ddf_data);
    ddf_block_descriptor_t (*block_descriptor)(const uint8* ddf_data, size_t block_index);
    ddf_block_descriptor_t (*block_descriptor_with_label)(const uint8* ddf_data,
                                                          const char* label);
  } read;

  struct {
    void (*init_header)(array(uint8) * ddf_data, struct dd_allocator* allocator);

    /// \return id of block
    size_t (*add_block)(array(uint8) * ddf_data, struct dd_allocator* allocator, const char* label,
                        const char* format);
    void (*set_block_data)(array(uint8) * ddf_data, struct dd_allocator* allocator,
                           size_t block_index, const void* data, uint32 byte_count);
    void (*reserve_block_data)(array(uint8) * ddf_data, struct dd_allocator* allocator,
                               size_t block_index, uint32 byte_count);
  } write;
};

extern const struct ddf_api ddf_api;
