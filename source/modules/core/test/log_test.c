#include "core/log.h"

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <string.h>

#include "core/test.h"
#include "core/types.h"

static char log_test_buffer[2048] = {0};
static void output_checker(const char* file_path, unsigned line, const char* function_name,
                           ELogSeverity severity, const char* formatted_string) {
  (void)file_path;
  (void)line;
  (void)function_name;
  size_t length = strlen(formatted_string);
  (void)length;
  assert(length < (sizeof(log_test_buffer) - 1));
  strcpy(log_test_buffer, formatted_string);
}
static void output_reset() { log_test_buffer[0] = 0; }

TEST_CASE(log_argument_counts_compile) {
  DD_LOG(ELogSeverityDebug, "0 args\n");
  DD_LOG(ELogSeverityDebug, "1 args\n", "arg");
  DD_LOG(ELogSeverityDebug, "2 args\n", "arg1", "arg2");
  DD_LOG(ELogSeverityDebug, "3 args\n", "arg1", "arg2", "arg3");
  DD_LOG(ELogSeverityDebug, "20 args\n", "arg1", "arg2", "arg3", "arg4", "arg5", "arg6", "arg7",
         "arg8", "arg9", "arg10", "arg11", "arg12", "arg13", "arg14", "arg15", "arg16", "arg17",
         "arg18", "arg19", "arg20");
  /* Only support up to 20 arguments
  DD_LOG(ELogSeverityDebug, "21 args\n", "arg1", "arg2", "arg3", "arg4", "arg5", "arg6", "arg7",
         "arg8", "arg9", "arg10", "arg11", "arg12", "arg13", "arg14", "arg15", "arg16", "arg17",
         "arg18", "arg19", "arg20", "arg21");
         */
}

TEST_CASE(log_small_no_output_until_flush) {
  dd_log_set_output_callback(output_checker);
  output_reset();

  DD_TEST_EQUAL_INT(0, log_test_buffer[0]);
  DD_LOG(ELogSeverityDebug, "pre %u post", 101);
  DD_TEST_EQUAL_INT(0, log_test_buffer[0]);

  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("pre 101 post", log_test_buffer);
  dd_log_set_output_callback(NULL);
}

TEST_CASE(log_large_automatically_flushes) {
  dd_log_set_output_callback(output_checker);
  output_reset();

  uint32 val = 0xFFFFFFFF;

  DD_TEST_EQUAL_INT(0, log_test_buffer[0]);
  for (unsigned i = 0; i < 1000; ++i) {
    DD_LOG(ELogSeverityDebug, "%i", val);
  }
  DD_TEST_IS_TRUE(0 != log_test_buffer[0]);

  dd_log_flush();

  dd_log_set_output_callback(NULL);
}

TEST_CASE(test_log_specifier) {
  dd_log_set_output_callback(output_checker);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%i: %i", 101);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%i: 101", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%i: %i", -1);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%i: -1", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%d: %d", 101);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%d: 101", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%d: %d", -1);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%d: -1", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%u: %u", 101);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%u: 101", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%u: %u", -1);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%u: 4294967295", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%o: %o", 101);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%o: 145", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%o: %o", -1);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%o: 37777777777", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%x: %x", 101);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%x: 65", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%x: %x", -1);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%x: ffffffff", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%X: %X", 101);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%X: 65", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%X: %X", -1);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%X: FFFFFFFF", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%f: %.1f", 101.1f);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%f: 101.1", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%f: %.1f", -1.5);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%f: -1.5", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%F: %.1F", -1.5);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%F: -1.5", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%e: %e", 101.1f);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%e: 1.011000e+02", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%E: %E", -1.5);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%E: -1.500000E+00", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%g: %g", 101.1f);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%g: 101.1", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%G: %G", -1.5);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%G: -1.5", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%a: %.5a", -1.5);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%a: -0x1.80000p+0", log_test_buffer);
  output_reset();
  DD_LOG(ELogSeverityDebug, "%%A: %.5A", -1.5);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%A: -0X1.80000P+0", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%c: %c", 'a');
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%c: a", log_test_buffer);

  /*
  TODO: %s specifier
  output_reset();
  DD_LOG(ELogSeverityDebug,"%s", "bla");
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("bla", log_test_buffer);
  */

  output_reset();
#if defined(__x86_64) || defined(_WIN64)
  DD_LOG(ELogSeverityDebug, "%p", (void*)0xBAAABCDEF);
#else
  DD_LOG(ELogSeverityDebug, "%p", (void*)0xBAABCDEF);
#endif

  dd_log_flush();
  // The casing of %p is not specified by the standard
  for (char* b = log_test_buffer; *b != '\0'; ++b) {
    *b = (char)tolower(*b);
  }
#if defined(__x86_64) || defined(_WIN64)
  const char* result = strstr(log_test_buffer, "baaabcdef");
  DD_TEST_EQUAL_STRINGS("baaabcdef", result);
#else
  const char* result = strstr(log_test_buffer, "baabcdef");
  DD_TEST_EQUAL_STRINGS("baabcdef", result);
#endif

  output_reset();
  DD_LOG(ELogSeverityDebug, "%%");
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("%", log_test_buffer);

  dd_log_set_output_callback(NULL);
}

TEST_CASE(log_multiple_arguments) {
  dd_log_set_output_callback(output_checker);

  output_reset();
  DD_LOG(ELogSeverityDebug, "multiple args %i %.2f", 101, 102.0);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("multiple args 101 102.00", log_test_buffer);

  output_reset();
  DD_LOG(ELogSeverityDebug, "multiple args %.2f %i", 101.0, 102);
  dd_log_flush();
  DD_TEST_EQUAL_STRINGS("multiple args 101.00 102", log_test_buffer);
}

TEST_SUITE(logging, test_data) {
  TEST_ADD_CASE(log_argument_counts_compile, test_data);
  TEST_ADD_CASE(log_small_no_output_until_flush, test_data);
  TEST_ADD_CASE(log_large_automatically_flushes, test_data);
  TEST_ADD_CASE(test_log_specifier, test_data);
  TEST_ADD_CASE(log_multiple_arguments, test_data);
}
