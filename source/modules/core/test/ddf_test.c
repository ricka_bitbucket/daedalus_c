#include "core/ddf.h"

#include "core/test.h"

uint8 buffer[4 * 1024 * 1024];

TEST_CASE(empty_header) {
  dd_heap_allocator_init(buffer, sizeof(buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)buffer;

  array(uint8) ddf_data = NULL;

  ddf_api.write.init_header(&ddf_data, allocator);

  ddf_header_t header = ddf_api.read.header(ddf_data);

  DD_TEST_EQUAL_INT(header.version, 1);
  DD_TEST_EQUAL_STRINGS(header.identifier, "DDF");
  DD_TEST_EQUAL_INT(header.block_count, 0);
}

TEST_CASE(add_empty_block) {
  dd_heap_allocator_init(buffer, sizeof(buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)buffer;

  array(uint8) ddf_data = NULL;

  ddf_api.write.init_header(&ddf_data, allocator);
  const size_t block_index = ddf_api.write.add_block(&ddf_data, allocator, "positions", "vec3");
  DD_TEST_EQUAL_INT((int)block_index, 0);

  ddf_block_descriptor_t bd = ddf_api.read.block_descriptor(ddf_data, block_index);
  DD_TEST_EQUAL_STRINGS(bd.label, "positions");
  DD_TEST_EQUAL_STRINGS(bd.format, "vec3");
  DD_TEST_EQUAL_INT(bd.data_offset, 0);
  DD_TEST_EQUAL_INT(bd.data_byte_count, 0);
}

TEST_CASE(set_block_data) {
  dd_heap_allocator_init(buffer, sizeof(buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)buffer;

  array(uint8) ddf_data = NULL;

  ddf_api.write.init_header(&ddf_data, allocator);
  const size_t block_index = ddf_api.write.add_block(&ddf_data, allocator, "positions", "vec3");
  const char* test_string  = "test_me";
  uint32 len               = (uint32)strlen(test_string) + 1;
  ddf_api.write.set_block_data(&ddf_data, allocator, block_index, (const uint8*)test_string, len);

  ddf_block_descriptor_t bd = ddf_api.read.block_descriptor(ddf_data, block_index);
  DD_TEST_EQUAL_STRINGS((const char*)(ddf_data + bd.data_offset), test_string);
}

TEST_SUITE(ddf, test_data) {
  TEST_ADD_CASE(empty_header, test_data);
  TEST_ADD_CASE(add_empty_block, test_data);
  TEST_ADD_CASE(set_block_data, test_data);
}
