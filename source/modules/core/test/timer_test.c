#include "core/timer.h"

#include "core/test.h"
#include "core/types.h"

TEST_CASE(timer_resolution_better_than_2_microsecond) {
  uint64 start_time   = dd_time_microseconds();
  uint64 end_time     = start_time + 1000000;
  uint64 current_time = start_time;
  uint64 prev_time    = current_time;

  int num_changes = 0;
  while (current_time < end_time) {
    current_time = dd_time_microseconds();
    if (current_time != prev_time) {
      prev_time = current_time;
      ++num_changes;
    }
  }

  DD_TEST_IS_TRUE(num_changes > 500000);
}

TEST_SUITE(timer, test_data) {
  TEST_ADD_CASE(timer_resolution_better_than_2_microsecond, test_data);
}
