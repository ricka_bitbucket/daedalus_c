#include <assert.h>
#include <string.h>

#include "core/memory.inl"

#define DD_TEST_GENERATE_MAIN

#include "core/test.h"

int main(int argc, char** args) {
  dd_test_data_t test_d     = {0};
  dd_test_data_t* test_data = &test_d;

  TEST_ADD_SUITE(containers, test_data);
  TEST_ADD_SUITE(logging, test_data);
  TEST_ADD_SUITE(math, test_data);
  TEST_ADD_SUITE(memory, test_data);
  TEST_ADD_SUITE(network, test_data);
  TEST_ADD_SUITE(plugin, test_data);
  TEST_ADD_SUITE(profiler, test_data);
  TEST_ADD_SUITE(strings, test_data);
  TEST_ADD_SUITE(timer, test_data);
  TEST_ADD_SUITE(ddf, test_data);

  return dd_perform_tests(test_data, argc, args);
}
