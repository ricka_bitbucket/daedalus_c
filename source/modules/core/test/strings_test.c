#include "core/types.h"
#include "core/dd_string.h"

#include "core/test.h"

static uint8 stack_buffer[100000];

TEST_CASE(strings_split) {
  dd_heap_allocator_init(stack_buffer, sizeof(stack_buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)stack_buffer;

  {
    const char* original = "||ab|cd||";
    array(dd_stringview_t) splits =
        stringview_split(stringview(original), stringview("|"), allocator);
    // Discards splitters at start and end
    DD_TEST_EQUAL_INT((int)array_count(splits), 4);
    DD_TEST_EQUAL_INT((int)splits[0].length, 0);
    DD_TEST_IS_TRUE(are_strings_equal(splits[1], stringview("ab")));
    DD_TEST_IS_TRUE(are_strings_equal(splits[2], stringview("cd")));
    DD_TEST_EQUAL_INT((int)splits[3].length, 0);
  }
}

TEST_CASE(strings_pop) {
  {
     dd_stringview_t original =stringview("abcde");
    const dd_stringview_t front    = stringview_pop_front(&original, 2);
    DD_TEST_IS_TRUE(are_strings_equal(front, stringview("a")));
    DD_TEST_IS_TRUE(are_strings_equal(original, stringview("cde")));
  }
}

TEST_SUITE(strings, test_data) {
  TEST_ADD_CASE(strings_split, test_data);
}
