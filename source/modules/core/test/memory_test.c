#include "core/memory.inl"

#include <assert.h>
#include <string.h>


#include "core/test.h"

uint8 buffer[4 * 1024 * 1024];
uint8* buffer_end = buffer + sizeof(buffer);

int allocation_count = 0;

struct allocs_t {
  const char* label;
  size_t line_number;
};
struct allocs_t reported_allocation_names[10] = {0};
uint32 total_allocated                        = 0;
void count_allocations(const struct dd_allocation_header* alloc_data, uint32 byte_count) {
  reported_allocation_names[allocation_count] =
      (struct allocs_t){alloc_data->label, alloc_data->line_number};
  ++allocation_count;
  total_allocated += byte_count;
}

void reset_stats() {
  allocation_count = 0;
  total_allocated  = 0;
}

TEST_CASE(stack_memory) {
  DD_STACK_ALLOCATOR(allocator, 2048);

  void* data = dd_alloc(allocator, 20, 1);
  DD_TEST_IS_TRUE(data != NULL);
}

TEST_CASE(test_memory_tracker) {
  dd_heap_allocator_init(buffer, sizeof(buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)buffer;

  void* result          = dd_alloc(allocator, sizeof(float), 4);
  const int alloc1_line = __LINE__ - 1;
  {
    assert(result != NULL);
    assert((size_t)buffer <= (size_t)result);
    assert((size_t)result < (size_t)buffer_end);

    reset_stats();
    dd_report_allocations(allocator, &count_allocations);
    DD_TEST_EQUAL_STRINGS(__FILE__, reported_allocation_names[0].label);
    DD_TEST_EQUAL_INT(alloc1_line, (int)reported_allocation_names[0].line_number);
    DD_TEST_EQUAL_INT(1, allocation_count);
  }
  void* result2         = dd_alloc(allocator, sizeof(float), 4);
  const int alloc2_line = __LINE__ - 1;
  {
    assert(result2 != NULL);
    assert((size_t)buffer <= (size_t)result2);
    assert((size_t)result2 < (size_t)buffer_end);

    reset_stats();
    dd_report_allocations(allocator, &count_allocations);
    DD_TEST_EQUAL_STRINGS(__FILE__, reported_allocation_names[0].label);
    DD_TEST_EQUAL_STRINGS(__FILE__, reported_allocation_names[1].label);
    DD_TEST_EQUAL_INT(alloc1_line, (int)reported_allocation_names[0].line_number);
    DD_TEST_EQUAL_INT(alloc2_line, (int)reported_allocation_names[1].line_number);
    DD_TEST_EQUAL_INT(2, allocation_count);
  }

  dd_free(allocator, result);
  dd_free(allocator, result2);

  reset_stats();
  dd_report_allocations(allocator, &count_allocations);
  DD_TEST_EQUAL_INT(0, allocation_count);

  // Test too large allocation returns NULL
  // void* result2 = dd_allocate(&allocator, 10*1024*1024, 4);
  // assert(result2 == NULL);
}

TEST_CASE(test_memory_page_allocator) {
  dd_allocator* allocator = (dd_allocator*)buffer;
  dd_heap_allocator_init(allocator, sizeof(buffer), "test_startup_allocator");

  dd_allocator* page_allocator = (dd_allocator*)dd_alloc(allocator, sizeof(dd_allocator), 1);
  dd_page_allocator_init(page_allocator, "test_page_allocator");

  void* result         = dd_alloc(page_allocator, 10 * 1024 * 1024, 4);
  const int alloc_line = __LINE__ - 1;
  assert(result != NULL);

  reset_stats();
  dd_report_allocations(page_allocator, &count_allocations);
  DD_TEST_EQUAL_STRINGS(__FILE__, reported_allocation_names[0].label);
  DD_TEST_EQUAL_INT(alloc_line, (int)reported_allocation_names[0].line_number);
  DD_TEST_EQUAL_INT(1, allocation_count);

  dd_free(page_allocator, result);

  reset_stats();
  dd_report_allocations(page_allocator, &count_allocations);
  DD_TEST_EQUAL_INT(0, allocation_count);
}

TEST_SUITE(memory, test_data) {
  TEST_ADD_CASE(stack_memory, test_data);

  // TODO: these tests check for logging of memory, not actual allocations
  TEST_ADD_CASE(test_memory_tracker, test_data);
  // TEST_ADD_CASE(test_memory_page_allocator, test_data);
  (void)test_memory_page_allocator;
}
