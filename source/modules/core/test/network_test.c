#include "core/network.h"

#include "core/test.h"
#include "core/utils.h"

const uint16 kPort = 13880;  // Just chose something. 8080 is in use by Jenkins, so don't use that

// TODO: reusing the same port doesn't seem to work correctly on MacOS
TEST_CASE(network_connect) {
  dd_network_connection_t* server = dd_network_server_start(kPort, tmp_malloc_allocator);
  DD_TEST_IS_TRUE(server != NULL);
  DD_TEST_EQUAL_UINT(0u, dd_network_server_num_connected_clients(server));
  dd_network_connection_t* client = dd_network_connect("127.0.0.1", kPort, tmp_malloc_allocator);
  DD_TEST_IS_TRUE(client != NULL);
  dd_sleep(5);
  DD_TEST_EQUAL_UINT(1u, dd_network_server_num_connected_clients(server));
  DD_TEST_IS_TRUE(dd_network_is_connection_connected(client));

  dd_network_close_connection(client);
  DD_TEST_IS_FALSE(dd_network_is_connection_connected(client));
  dd_sleep(5);  // Give server some time to make sure the disconnect arrived
  DD_TEST_EQUAL_UINT(0u, dd_network_server_num_connected_clients(server));
  DD_LOG(ELogSeverityDebug, "Shutdown server\n");
  dd_network_close_connection(server);
  DD_TEST_IS_FALSE(dd_network_is_connection_connected(server));

  dd_sleep(50);
}

TEST_CASE(network_connect_multiple_clients) {
  dd_network_connection_t* server = dd_network_server_start(kPort + 1, tmp_malloc_allocator);
  DD_TEST_IS_TRUE(server != NULL);
  DD_TEST_EQUAL_UINT(0u, dd_network_server_num_connected_clients(server));

  dd_network_connection_t* client1 =
      dd_network_connect("127.0.0.1", kPort + 1, tmp_malloc_allocator);
  DD_TEST_IS_TRUE(client1 != NULL);
  dd_network_connection_t* client2 =
      dd_network_connect("127.0.0.1", kPort + 1, tmp_malloc_allocator);
  DD_TEST_IS_TRUE(client2 != NULL);
  dd_sleep(5);
  DD_TEST_EQUAL_UINT(2u, dd_network_server_num_connected_clients(server));
  DD_TEST_IS_TRUE(dd_network_is_connection_connected(client1));
  DD_TEST_IS_TRUE(dd_network_is_connection_connected(client2));

  dd_network_close_connection(client1);
  DD_TEST_IS_FALSE(dd_network_is_connection_connected(client1));
  dd_network_close_connection(client2);
  DD_TEST_IS_FALSE(dd_network_is_connection_connected(client2));
  dd_sleep(5);  // Give server some time to make sure the disconnect arrived
  DD_TEST_EQUAL_UINT(0u, dd_network_server_num_connected_clients(server));

  dd_network_close_connection(server);
  DD_TEST_IS_FALSE(dd_network_is_connection_connected(server));

  dd_sleep(50);
}

TEST_CASE(network_send_small_data_client_to_server) {
  dd_network_connection_t* server = dd_network_server_start(kPort + 2, tmp_malloc_allocator);
  dd_network_connection_t* client =
      dd_network_connect("localhost", kPort + 2, tmp_malloc_allocator);
  dd_sleep(5);

  dd_network_connection_t* client_on_server = dd_network_server_get_client_connection(server, 0);
  DD_TEST_EQUAL_UINT(0u, (uint32)dd_network_connection_bytes_received(client_on_server));

  {
    const char* data = __TIME__;
    dd_network_connection_send_data(client, (const uint8*)data, (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client_on_server);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client_on_server, (uint8*)received_data,
                                     received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  {
    const char* data = __DATE__;
    dd_network_connection_send_data(client, (const uint8*)data, (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client_on_server);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client_on_server, (uint8*)received_data,
                                     received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  dd_network_close_connection(client);
  dd_network_close_connection(server);

  dd_sleep(5);
}

TEST_CASE(network_send_small_data_server_to_client) {
  dd_network_connection_t* server = dd_network_server_start(kPort + 3, tmp_malloc_allocator);
  dd_network_connection_t* client =
      dd_network_connect("127.0.0.1", kPort + 3, tmp_malloc_allocator);
  dd_sleep(5);

  DD_TEST_EQUAL_UINT(0u, (uint32)dd_network_connection_bytes_received(client));

  dd_network_connection_t* client_on_server = dd_network_server_get_client_connection(server, 0);

  {
    const char* data = __TIME__;
    dd_network_connection_send_data(client_on_server, (const uint8*)data,
                                    (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client, (uint8*)received_data, received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  {
    const char* data = __DATE__;
    dd_network_connection_send_data(client_on_server, (const uint8*)data,
                                    (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client, (uint8*)received_data, received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  dd_network_close_connection(client);
  dd_network_close_connection(server);
}

TEST_CASE(network_send_small_data_alternating) {
  dd_network_connection_t* server = dd_network_server_start(kPort + 4, tmp_malloc_allocator);
  dd_network_connection_t* client =
      dd_network_connect("127.0.0.1", kPort + 4, tmp_malloc_allocator);
  dd_sleep(5);

  DD_TEST_EQUAL_UINT(0u, (uint32)dd_network_connection_bytes_received(client));

  dd_network_connection_t* client_on_server = dd_network_server_get_client_connection(server, 0);

  {
    const char* data = __TIME__;
    dd_network_connection_send_data(client_on_server, (const uint8*)data,
                                    (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client, (uint8*)received_data, received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  {
    const char* data = "test";
    dd_network_connection_send_data(client, (const uint8*)data, (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client_on_server);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client_on_server, (uint8*)received_data,
                                     received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  {
    const char* data = __DATE__;
    dd_network_connection_send_data(client_on_server, (const uint8*)data,
                                    (unsigned)strlen(data) + 1);
    dd_sleep(5);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client);
    DD_TEST_EQUAL_UINT((uint32)(strlen(data) + 1), (uint32)received_count_bytes);
    char received_data[32];
    dd_network_connection_read_bytes(client, (uint8*)received_data, received_count_bytes);
    DD_TEST_EQUAL_STRINGS(data, received_data);
  }

  dd_network_close_connection(client);
  dd_network_close_connection(server);
}

TEST_CASE(network_send_large_data_client_to_server) {
  dd_network_connection_t* server = dd_network_server_start(kPort + 5, tmp_malloc_allocator);
  dd_network_connection_t* client =
      dd_network_connect("localhost", kPort + 5, tmp_malloc_allocator);
  dd_sleep(5);

  dd_network_connection_t* client_on_server = dd_network_server_get_client_connection(server, 0);
  DD_TEST_EQUAL_UINT(0u, (uint32)dd_network_connection_bytes_received(client_on_server));

  const size_t NUM_ITEMS = 1024 * 1024;
  int* data_send         = (int*)malloc(NUM_ITEMS * sizeof(int));
  for (unsigned i = 0; i < NUM_ITEMS; ++i) {
    data_send[i] = rand();
  }

  {
    dd_network_connection_send_data(client, (const uint8*)data_send, NUM_ITEMS * sizeof(int));
    dd_sleep(500);  // Ensure there is time for the data to have been sent
    size_t received_count_bytes = dd_network_connection_bytes_received(client_on_server);
    DD_TEST_EQUAL_UINT((uint32)(NUM_ITEMS * sizeof(int)), (uint32)received_count_bytes);

    int* data_recv = (int*)malloc(NUM_ITEMS * sizeof(int));
    dd_network_connection_read_bytes(client_on_server, (uint8*)data_recv, received_count_bytes);

    bool is_data_equal = true;
    for (unsigned i = 0; i < NUM_ITEMS; ++i) {
      is_data_equal = is_data_equal && (data_send[i] == data_recv[i]);
    }
    DD_TEST_IS_TRUE(is_data_equal);
  }

  dd_network_close_connection(client);
  dd_network_close_connection(server);

  dd_sleep(500);
}

TEST_SUITE(network, test_data) {
  TEST_ADD_CASE(network_connect, test_data);
  TEST_ADD_CASE(network_connect_multiple_clients, test_data);
  TEST_ADD_CASE(network_send_small_data_client_to_server, test_data);
  TEST_ADD_CASE(network_send_small_data_server_to_client, test_data);
  TEST_ADD_CASE(network_send_small_data_alternating, test_data);
  TEST_ADD_CASE(network_send_large_data_client_to_server, test_data);
}
