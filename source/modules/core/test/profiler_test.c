#include "core/profiler.h"

#if defined(_WIN32)
#include <Windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>

#include "core/profiler.inl"
#include "core/test.h"
#include "core/thread.h"
#include "core/types.h"
#include "core/utils.h"

FILE* profile_log_file;
uint64 first_timestamp    = 0;
dd_mutex_t log_file_mutex = {0};

static void prof_report_chrome_to_file(uint8* buffer, unsigned buffer_size) {
  if (profile_log_file == NULL) return;

  dd_mutex_lock(&log_file_mutex);

  uint8* buf     = (uint8*)buffer;
  uint8* buf_end = buf + buffer_size;

#if defined(_WIN32)
  const uint64 process_id = GetCurrentProcessId();
#else
  const uint64 process_id = 0;
#endif
  if (first_timestamp == 0) {
    first_timestamp = ((const dd_perf_startdata*)(buf + 1))->timestamp;
  }

  dd_perf_threaddata_t* td = NULL;
  while (buf != buf_end) {
    uint8 sentinal = *buf;
    buf += 1;
    if (sentinal == SENTINAL_START) {
      const dd_perf_startdata* sd = (const dd_perf_startdata*)buf;
      buf += sizeof(dd_perf_startdata);

      fprintf(profile_log_file,
              "{\"ts\" : %lld, \"ph\" : \"B\", \"pid\" : %llu, \"tid\" : %llu, "
              "\"name\":\"%s\",\"cat\":\"%s\"}, \n",
              (sd->timestamp - first_timestamp), process_id, td->thread_id, sd->label,
              sd->category);
    } else if (sentinal == SENTINAL_END) {
      const dd_perf_enddata* ed = (const dd_perf_enddata*)buf;
      buf += sizeof(dd_perf_enddata);

      fprintf(profile_log_file,
              "{\"ts\" : %lld, \"ph\" : \"E\", \"pid\" : %llu, \"tid\" : %llu},\n",
              (ed->timestamp - first_timestamp), process_id, td->thread_id);
    } else if (sentinal == SENTINAL_THREAD_ID) {
      td = (dd_perf_threaddata_t*)buf;
      buf += sizeof(dd_perf_threaddata_t);
    }
  }

  dd_mutex_release(&log_file_mutex);
}

TEST_CASE(profiler) {
  dd_prof_thread_init("");

  DD_PROF();
  dd_prof_start("a");
  unsigned long total = 0;
  for (unsigned i = 0; i < 1000000; ++i) {
    total += (unsigned long)rand();
  }
  DD_PROF_END();
  dd_sleep(10);
  dd_prof_start("b");
  unsigned long total2 = 0;
  for (unsigned i = 0; i < 1000000; ++i) {
    total2 += (unsigned long)rand();
  }
  DD_PROF_END();
  DD_PROF_END();

  dd_prof_flush();

  dd_prof_thread_destroy();
}

TEST_CASE(profiler_many_entries) {
  dd_prof_thread_init("");

  profile_log_file = fopen("profile.json", "wb");
  fprintf(profile_log_file, "[\n");
  log_file_mutex = dd_mutex_create();

  dd_prof_set_process_callback(&prof_report_chrome_to_file);

  int total = rand();
  for (unsigned i = 0; i < 100; ++i) {
    dd_prof_start("a");
    total = total ^ rand();
    dd_sleep(1);
    dd_prof_end();
    dd_sleep(1);
  }

  dd_prof_flush();
  if (profile_log_file != NULL) {
    fclose(profile_log_file);
  }

  // TODO: destroy mutex

  dd_prof_thread_destroy();
}

void prof_test_small_thread_main(void* arg) {
  dd_prof_thread_init("");

  const char* name = (const char*)arg;
  int total        = rand();
  for (unsigned i = 0; i < 10; ++i) {
    dd_prof_start(name);
    total = total ^ rand();
    dd_sleep(1);
    dd_prof_end();
    dd_sleep(1);
  }
  dd_sleep(1000);

  dd_prof_thread_destroy();
}

bool test_prof_reported = false;
unsigned entry_count    = 0;
void prof_report_count_entries(void* buffer, unsigned buffer_size) {
  test_prof_reported = true;

  uint8* buf     = (uint8*)buffer;
  uint8* buf_end = buf + buffer_size;
  while (buf != buf_end) {
    uint8 sentinal = *buf;
    buf += 1;
    if (sentinal == SENTINAL_START) {
      buf += sizeof(dd_perf_startdata);
      entry_count++;
    } else if (sentinal == SENTINAL_END) {
      buf += sizeof(dd_perf_enddata);
      entry_count++;
    } else if (sentinal == SENTINAL_THREAD_ID) {
      buf += sizeof(dd_perf_threaddata_t);
    }
  }
}

void prof_test_large_thread_main(void* arg) {
  dd_prof_thread_init("");

  const char* name = (const char*)arg;
  int total        = rand();
  for (unsigned i = 0; i < 500; ++i) {
    dd_prof_start(name);
    total = total ^ rand();
    dd_prof_end();
    dd_prof_start(name);
    total = total ^ rand();
    dd_prof_end();
    dd_sleep(1);
  }
  dd_sleep(1000);

  dd_prof_thread_destroy();
}

void prof_report_count_and_print(void* buffer, unsigned buffer_size) {
  prof_report_count_entries(buffer, buffer_size);
  prof_report_chrome_to_file(buffer, buffer_size);
}

#if 0
// The default profiler discards data
TEST_CASE(profiler_threaded_large_automatic_flush) {
  dd_prof_thread_init("");

  test_prof_reported = false;
  entry_count        = 0;
  profile_log_file   = fopen("profile.json", "wb");
  fprintf(profile_log_file, "[\n");
  log_file_mutex = dd_mutex_create();

  dd_prof_set_process_callback(&prof_report_count_and_print);

  dd_thread_start(&prof_test_large_thread_main, (void*)"threadA");
  dd_sleep(5);
  dd_thread_start(&prof_test_large_thread_main, (void*)"threadB");

  // TODO: wait until both threads are finished, instead of 1 second
  dd_sleep(1000);

  DD_TEST_IS_TRUE(test_prof_reported);

  dd_prof_flush();
  if (profile_log_file != NULL) {
    fclose(profile_log_file);
  }

  // TODO: destroy mutex
  dd_prof_thread_destroy();
}
#endif

TEST_SUITE(profiler, test_data) {
  TEST_ADD_CASE(profiler, test_data);
  TEST_ADD_CASE(profiler_many_entries, test_data);
  // TEST_ADD_CASE(profiler_threaded_large_automatic_flush, test_data);
}
