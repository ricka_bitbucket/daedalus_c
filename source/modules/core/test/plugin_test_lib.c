#include "core/api_registry.h"

const size_t g_value = TEST_PLUGIN_VALUE;

static size_t test_func() { return g_value; }

const struct test_api { size_t (*func)(); } test_api = {.func = &test_func};

#if defined(_MSC_VER)
  #define DD_DLL_EXPORT __declspec(dllexport)
#else
  #define DD_DLL_EXPORT
#endif

DD_DLL_EXPORT bool plugin_link_apis(const dd_api_registry_t* registry,
                                    struct dd_allocator* allocator) {
  // Check dependencies
  {
    // dd_draw2d_api draw2d_api_ = (struct dd_draw2d_api*)registry->get_api("draw2d", 1);

    // if (draw2d_api_ == NULL) return false;
  }

  // Register own apis
  registry->register_api("test_api", 1, &test_api, sizeof(test_api));

  return true;
}
