#include "core/containers.h"

#include <assert.h>

#include "core/memory.h"
#include "core/test.h"
#include "core/types.h"

static uint8 stack_buffer[100000];
dd_allocator* g_stack_allocator;

TEST_CASE(hash_lookup_empty_returns_default) {
  dd_hash64_t hash_table = {0};
  uint64 val             = dd_hash64_lookup(&hash_table, 1, 12345);
  DD_TEST_EQUAL_INT((int)val, 12345);
}

TEST_CASE(hash_add_and_lookup_returns_inserted_value) {
  dd_hash64_t hash_table = {0};
  const uint64 key       = 123;
  const uint64 value     = 12345;
  dd_hash64_add(&hash_table, key, value, g_stack_allocator);
  uint64 val = dd_hash64_lookup(&hash_table, key, 0);
  DD_TEST_EQUAL_INT((int)val, (int)value);
}

TEST_CASE(hash_add_multiple_and_lookup_return_inserted_values) {
  dd_hash64_t hash_table = {0};

  const uint64 key1   = 123;
  const uint64 value1 = 12345;
  const uint64 key2   = 124;
  const uint64 value2 = 12344;
  dd_hash64_add(&hash_table, key1, value1, g_stack_allocator);
  dd_hash64_add(&hash_table, key2, value2, g_stack_allocator);
  uint64 ret1 = dd_hash64_lookup(&hash_table, key1, 0);
  DD_TEST_EQUAL_INT((int)ret1, (int)value1);
  uint64 ret2 = dd_hash64_lookup(&hash_table, key2, 0);
  DD_TEST_EQUAL_INT((int)ret2, (int)value2);
}

TEST_CASE(hash_add_multiple_and_lookup_return_inserted_values_forcing_same_bucket) {
  // This test uses the inner knowledge that the hash inserts elements into bucket key %
  // hash->bucket_count to ensure the two keys would go into the same bucket.
  dd_hash64_t hash_table = {0};

  const uint64 key1   = 1;
  const uint64 value1 = 12345;
  dd_hash64_add(&hash_table, key1, value1, g_stack_allocator);
  const uint64 key2   = 1 + hash_table.bucket_count;
  const uint64 value2 = 12344;
  dd_hash64_add(&hash_table, key2, value2, g_stack_allocator);
  uint64 ret1 = dd_hash64_lookup(&hash_table, key1, 0);
  DD_TEST_EQUAL_INT((int)ret1, (int)value1);
  uint64 ret2 = dd_hash64_lookup(&hash_table, key2, 0);
  DD_TEST_EQUAL_INT((int)ret2, (int)value2);
}

TEST_CASE(hash_force_rehash) {
  dd_hash64_t hash_table = {0};

  const uint64 key1   = 1;
  const uint64 value1 = 12345;
  dd_hash64_add(&hash_table, key1, value1, g_stack_allocator);

  // Get the number of buckets, and add that many more entries, with simple keys
  unsigned num_key_to_add = hash_table.bucket_count;
  for (unsigned i = 2; i < num_key_to_add; i++) {
    dd_hash64_add(&hash_table, (uint64)i, (uint64)i, g_stack_allocator);
  }

  uint64 ret1 = dd_hash64_lookup(&hash_table, key1, 0);
  DD_TEST_EQUAL_INT((int)ret1, (int)value1);
}

TEST_CASE(hash_strings) {
  dd_hash64_t hash_table = {0};

  const char* s1      = "test string1";
  const uint64 key1   = murmur_hash_64(s1, strlen(s1), 0);
  const uint64 value1 = 12345;
  dd_hash64_add(&hash_table, key1, value1, g_stack_allocator);
  const char* s2      = "test string2";
  const uint64 key2   = murmur_hash_64(s2, strlen(s2), 0);
  const uint64 value2 = 12344;
  dd_hash64_add(&hash_table, key2, value2, g_stack_allocator);
  uint64 ret1 = dd_hash64_lookup(&hash_table, key1, 0);
  DD_TEST_EQUAL_INT((int)ret1, (int)value1);
  uint64 ret2 = dd_hash64_lookup(&hash_table, key2, 0);
  DD_TEST_EQUAL_INT((int)ret2, (int)value2);
}

TEST_SUITE(containers, test_data) {
  g_stack_allocator = dd_memory_stack_allocator_init(stack_buffer, sizeof(stack_buffer));

  TEST_ADD_CASE(hash_lookup_empty_returns_default, test_data);
  TEST_ADD_CASE(hash_add_and_lookup_returns_inserted_value, test_data);
  TEST_ADD_CASE(hash_add_multiple_and_lookup_return_inserted_values, test_data);
  TEST_ADD_CASE(hash_add_multiple_and_lookup_return_inserted_values_forcing_same_bucket,
                test_data);
  TEST_ADD_CASE(hash_force_rehash, test_data);
  TEST_ADD_CASE(hash_strings, test_data);
}
