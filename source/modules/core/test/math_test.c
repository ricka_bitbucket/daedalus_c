#include "core/dd_math.h"
#include "core/test.h"

TEST_CASE(alignments) {
  DD_TEST_EQUAL_INT(align_up((int32)5, 3), 6);
  DD_TEST_EQUAL_INT(align_up((int32)6, 3), 6);
  DD_TEST_EQUAL_INT(align_up((int32)7, 3), 9);
  DD_TEST_EQUAL_INT(align_down((int32)5, 3), 3);
  DD_TEST_EQUAL_INT(align_down((int32)6, 3), 6);
  DD_TEST_EQUAL_INT(align_down((int32)7, 3), 6);
  DD_TEST_EQUAL_INT(align_up((int32)-5, 3), -3);
  DD_TEST_EQUAL_INT(align_up((int32)-6, 3), -6);
  DD_TEST_EQUAL_INT(align_up((int32)-7, 3), -6);
  DD_TEST_EQUAL_INT(align_down((int32)-5, 3), -6);
  DD_TEST_EQUAL_INT(align_down((int32)-6, 3), -6);
  DD_TEST_EQUAL_INT(align_down((int32)-7, 3), -9);

  DD_TEST_EQUAL_INT(align_up((uint32)5, 3), 6);
  DD_TEST_EQUAL_INT(align_up((uint32)6, 3), 6);
  DD_TEST_EQUAL_INT(align_up((uint32)7, 3), 9);
  DD_TEST_EQUAL_INT(align_down((uint32)5, 3), 3);
  DD_TEST_EQUAL_INT(align_down((uint32)6, 3), 6);
  DD_TEST_EQUAL_INT(align_down((uint32)7, 3), 6);
}

TEST_SUITE(math, test_data) { TEST_ADD_CASE(alignments, test_data); }
