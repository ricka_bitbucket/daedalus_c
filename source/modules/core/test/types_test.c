#include "core/types.h"

#include <assert.h>

#include "core/test.h"

static_assert(sizeof(bool) == 1, "Datatype has incorrect size");

static_assert(sizeof(uint8) == 1, "Datatype has incorrect size");
static_assert(sizeof(int8) == 1, "Datatype has incorrect size");
static_assert(sizeof(uint16) == 2, "Datatype has incorrect size");
static_assert(sizeof(int16) == 2, "Datatype has incorrect size");
static_assert(sizeof(uint32) == 4, "Datatype has incorrect size");
static_assert(sizeof(int32) == 4, "Datatype has incorrect size");
static_assert(sizeof(uint64) == 8, "Datatype has incorrect size");
static_assert(sizeof(int64) == 8, "Datatype has incorrect size");

static_assert(sizeof(float2) == 8, "Datatype has incorrect size");
static_assert(sizeof(float3) == 12, "Datatype has incorrect size");
static_assert(sizeof(float4) == 16, "Datatype has incorrect size");
