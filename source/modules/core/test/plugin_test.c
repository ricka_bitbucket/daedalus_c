#include "core/api_registry.h"
#include "core/application.h"
#include "core/dd_string.h"
#include "core/filesystem.h"
#include "core/plugins.h"
#include "core/test.h"

struct test_api {
  size_t (*func)();
};

#if defined(_WIN32)
  #define DYN_LIB_PREFIX ""
  #define DYN_LIB_EXT "dll"
  #define LIB_FOLDER "e:/daedalus_c/"
#else
  #define DYN_LIB_PREFIX "lib"
  #define DYN_LIB_EXT "dylib"
  #define LIB_FOLDER "/Users/rickappleton/dev/daedalus_c/"
#endif

static uint8 stack_buffer[100000];

TEST_CASE(plugin_load_from_folder) {
  dd_heap_allocator_init(stack_buffer, sizeof(stack_buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)stack_buffer;

  dd_api_registry.init(allocator);

  dd_stringview_t app_folder = dd_application.executable_folder();
  printf("%i %s\n", __LINE__, app_folder.string);
  fflush(stdout);
  {
    array(char) old_path =
        dd_strcat(app_folder, stringview(DYN_LIB_PREFIX "test_plugin1.tmp"), allocator);
    array(char) new_path =
        dd_strcat(app_folder, stringview(DYN_LIB_PREFIX "test_plugin." DYN_LIB_EXT), allocator);
    dd_filesystem_api.copy_file(old_path, new_path, ECopyFileFlagOverwriteExisting);
    printf("%s(%i) %s -> %s\n", __FILE__, __LINE__, old_path, new_path);
    fflush(stdout);
  }

  struct dd_plugins_o* plugins = dd_plugins_api.create(allocator);
  const char* paths[]          = {app_folder.string};
  dd_plugins_api.load_plugins(plugins, paths, 1);

  struct test_api* tapi = (struct test_api*)dd_api_registry.get_api("test_api", 1);
  DD_TEST_IS_TRUE(tapi != NULL);

  printf("%i %p\n", __LINE__, tapi);
  fflush(stdout);
  size_t value = tapi->func();
  DD_TEST_EQUAL_INT((int)value, (int)5);

  dd_plugins_api.unload_all(plugins);
  dd_api_registry.deinit();
}

TEST_CASE(plugin_reload_from_folder) {
  dd_heap_allocator_init(stack_buffer, sizeof(stack_buffer), "test_allocator");
  dd_allocator* allocator = (dd_allocator*)stack_buffer;

  dd_api_registry.init(allocator);

  dd_stringview_t app_folder = dd_application.executable_folder();
  {
    array(char) old_path =
        dd_strcat(app_folder, stringview(DYN_LIB_PREFIX "test_plugin1.tmp"), allocator);
    array(char) new_path =
        dd_strcat(app_folder, stringview(DYN_LIB_PREFIX "test_plugin." DYN_LIB_EXT), allocator);
    dd_filesystem_api.copy_file(old_path, new_path, ECopyFileFlagOverwriteExisting);
  }

  struct dd_plugins_o* plugins = dd_plugins_api.create(allocator);
  const char* paths[]          = {app_folder.string};
  dd_plugins_api.load_plugins(plugins, paths, 1);

  struct test_api* tapi = (struct test_api*)dd_api_registry.get_api("test_api", 1);
  DD_TEST_IS_TRUE(tapi != NULL);

  size_t value = tapi->func();
  DD_TEST_EQUAL_INT((int)value, (int)5);

  {
    array(char) old_path =
        dd_strcat(app_folder, stringview(DYN_LIB_PREFIX "test_plugin2.tmp"), allocator);
    array(char) new_path =
        dd_strcat(app_folder, stringview(DYN_LIB_PREFIX "test_plugin." DYN_LIB_EXT), allocator);
    dd_filesystem_api.copy_file(old_path, new_path, ECopyFileFlagOverwriteExisting);
  }

  dd_plugins_api.check_for_changes(plugins);

  value = tapi->func();
  DD_TEST_EQUAL_INT((int)value, (int)16);

  dd_plugins_api.unload_all(plugins);
  dd_api_registry.deinit();
}

TEST_SUITE(plugin, test_data) {
  TEST_ADD_CASE(plugin_load_from_folder, test_data);
  TEST_ADD_CASE(plugin_reload_from_folder, test_data);
}
