#pragma once

#include "types.h"

typedef struct dd_datetime_t {
  uint16 year;
  uint8 month;  /* 1-12 */
  uint8 day;    /* 1-31 */
  uint8 hour;   /* 0-23 */
  uint8 minute; /* 0-59 */
  uint8 second; /* 0-59 */
} dd_datetime_t;

// Return type needs to be 64-bit. 32-bit only gives us about 70 minutes of run time
uint64 dd_time_microseconds();

dd_datetime_t dd_datetime();
