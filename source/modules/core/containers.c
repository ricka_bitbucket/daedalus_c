#include "containers.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "memory.h"
#include "memory.inl"

void* array_grow(void* a, size_t element_size, dd_allocator* allocator, const char* file_path,
                 const char* function_name, size_t line_number) {
  size_t prev_capacity           = 0;
  size_t prev_count              = 0;
  dd_array_header_t* prev_header = array_header(a);
  if (prev_header) {
    prev_capacity = prev_header->capacity_;
    prev_count    = prev_header->count_;
  }

  size_t new_capacity = (size_t)(prev_capacity * 1.75);
  if (new_capacity == prev_capacity) {
    new_capacity = prev_capacity + 8;
  }

  dd_allocator_impl* alloc_impl = (dd_allocator_impl*)allocator;
  dd_array_header_t* new_header = alloc_impl->alloc(
      allocator, (uint32)(element_size * new_capacity + sizeof(dd_array_header_t)), 1,
      &(struct allocation_location){__FILE__, __FUNCTION__, __LINE__});
  void* out             = new_header + 1;
  new_header->count_    = (uint32)prev_count;
  new_header->capacity_ = (uint32)new_capacity;
  memcpy(out, a, element_size * prev_count);

  if (prev_header) {
    dd_free(allocator, prev_header);
  }

  return out;
}

void* array_reserve_impl(void* a, size_t element_size, size_t new_capacity,
                         dd_allocator* allocator, const char* file_path, const char* function_name,
                         size_t line_number) {
  size_t prev_capacity           = 0;
  size_t prev_count              = 0;
  dd_array_header_t* prev_header = array_header(a);
  if (prev_header) {
    prev_capacity = prev_header->capacity_;
    prev_count    = prev_header->count_;
  }

  if (new_capacity > prev_capacity) {
    dd_allocator_impl* alloc_impl = (dd_allocator_impl*)allocator;
    dd_array_header_t* new_header = alloc_impl->alloc(
        allocator, (uint32)(element_size * new_capacity + sizeof(dd_array_header_t)), 1,
        &(struct allocation_location){__FILE__, __FUNCTION__, __LINE__});
    void* out             = new_header + 1;
    new_header->count_    = (uint32)prev_count;
    new_header->capacity_ = (uint32)new_capacity;
    memcpy(out, a, element_size * prev_count);

    if (prev_header) {
      dd_free(allocator, prev_header);
    }

    return out;
  } else {
    return a;
  }
}

void array_free(void* a, dd_allocator* allocator) {
  void* head_of_allocation = array_header(a);
  if (head_of_allocation) {
    dd_free(allocator, head_of_allocation);
  }
}

/*  Hash table
 *
 */
enum { HASH_UNUSED = 0, HASH_REMOVED = (uint64)-1 };

void dd_hash64_rehash(dd_hash64_t* hash, uint32 new_bucket_count, struct dd_allocator* allocator,
                      const struct allocation_location* alloc_location) {
  dd_allocator_impl* alloc_impl = (dd_allocator_impl*)allocator;

  dd_hash64_t nh      = {0};
  nh.bucket_count     = new_bucket_count;
  nh.value_count      = hash->value_count;
  unsigned byte_count = new_bucket_count * sizeof(uint64);
  nh.keys             = alloc_impl->alloc(allocator, byte_count, 1, alloc_location);
  nh.values           = alloc_impl->alloc(allocator, byte_count, 1, alloc_location);
  memset(nh.keys, 0, byte_count);
  memset(nh.values, 0, byte_count);

  // Perform actual rehash here
  for (uint32 i = 0; i < hash->bucket_count; i++) {
    if (hash->keys[i] != HASH_UNUSED && hash->keys[i] != HASH_REMOVED) {
      dd_hash64_add_impl(&nh, hash->keys[i], hash->values[i], allocator, alloc_location->file_name,
                         alloc_location->function_name, alloc_location->line_number);
    }
  }

  if (hash->keys) {
    dd_free(allocator, hash->keys);
    dd_free(allocator, hash->values);
  }

  *hash = nh;
}

uint64 dd_hash64_lookup(const dd_hash64_t* h, const uint64 key, uint64 default_value) {
  assert(key != HASH_UNUSED);

  if (h->bucket_count == 0) {
    return default_value;
  }

  uint32 b             = key % h->bucket_count;
  const uint32 b_start = b;
  (void)b_start;
  while (h->keys[b] != key && h->keys[b] != HASH_UNUSED) {
    b = (b + 1) % h->bucket_count;
    assert(b != b_start);
    // TODO: could be looking up a non-existant key in a completely full hash table
  }

  if (h->keys[b] == key) {
    return h->values[b];
  } else {
    return default_value;
  }
}

void dd_hash64_set(const dd_hash64_t* h, const uint64 key, uint64 new_value) {
  assert(key != HASH_UNUSED);
  assert(h->bucket_count > 0);

  uint32 b             = key % h->bucket_count;
  const uint32 b_start = b;
  (void)b_start;
  while (h->keys[b] != key && h->keys[b] != HASH_UNUSED) {
    b = (b + 1) % h->bucket_count;
    assert(b != b_start);
    // TODO: check that we aren't going all the way around
  }

  assert(h->keys[b] == key);
  h->values[b] = new_value;
}

void dd_hash64_add_impl(dd_hash64_t* h, const uint64 key, uint64 value,
                        struct dd_allocator* allocator, const char* file_path,
                        const char* function_name, size_t line_number) {
  assert(key != HASH_UNUSED);

  h->value_count++;

  bool should_rehash = h->value_count > (3 * h->bucket_count / 4);
  if (should_rehash) {
    uint32 new_count = 2 * h->value_count;
    new_count        = (new_count < 256) ? 256 : new_count;
    dd_hash64_rehash(h, new_count, allocator,
                     &(struct allocation_location){__FILE__, __FUNCTION__, __LINE__});
  }

  unsigned b = key % h->bucket_count;
  while (h->keys[b] != key && h->keys[b] != HASH_UNUSED && h->keys[b] != HASH_REMOVED) {
    b = (b + 1) % h->bucket_count;
  }
  h->keys[b]   = key;
  h->values[b] = value;
}

void dd_hash64_remove(dd_hash64_t* h, const uint64 key) {
  assert(key != HASH_UNUSED);

  h->value_count--;

  unsigned b = key % h->bucket_count;
  while (h->keys[b] != key && h->keys[b] != HASH_UNUSED) {
    b = (b + 1) % h->bucket_count;
  }
  assert(h->keys[b] == key);

  h->keys[b]   = (uint64)HASH_REMOVED;
  h->values[b] = 0;
}

void dd_hash64_free(dd_hash64_t* hash, struct dd_allocator* allocator) {
  if (hash->keys) {
    dd_free(allocator, hash->keys);
    dd_free(allocator, hash->values);
  }
}

// Ref: http://bitsquid.blogspot.com/2011/08/code-snippet-murmur-hash-inverse-pre.html
uint64 murmur_hash_64(const void* key, uint64 len, uint64 seed) {
  const uint64 m = 0xc6a4a7935bd1e995ULL;
  const int r    = 47;

  uint64 h = seed ^ (len * m);

  const uint64* data = (const uint64*)key;
  const uint64* end  = data + (len / 8);

  while (data != end) {
#ifdef PLATFORM_BIG_ENDIAN
    uint64 k = *data++;
    char* p  = (char*)&k;
    char c;
    c    = p[0];
    p[0] = p[7];
    p[7] = c;
    c    = p[1];
    p[1] = p[6];
    p[6] = c;
    c    = p[2];
    p[2] = p[5];
    p[5] = c;
    c    = p[3];
    p[3] = p[4];
    p[4] = c;
#else
    uint64 k = *data++;
#endif

    k *= m;
    k ^= k >> r;
    k *= m;

    h ^= k;
    h *= m;
  }

  const unsigned char* data2 = (const unsigned char*)data;

  switch (len & 7) {
    case 7:
      h ^= (uint64)(data2[6]) << 48;
    case 6:
      h ^= (uint64)(data2[5]) << 40;
    case 5:
      h ^= (uint64)(data2[4]) << 32;
    case 4:
      h ^= (uint64)(data2[3]) << 24;
    case 3:
      h ^= (uint64)(data2[2]) << 16;
    case 2:
      h ^= (uint64)(data2[1]) << 8;
    case 1:
      h ^= (uint64)(data2[0]);
      h *= m;
  };

  h ^= h >> r;
  h *= m;
  h ^= h >> r;

  return h;
}

int32 dd_is_string_in_list(const char* needle, const char** haystack, uint32 haystack_count) {
  for (uint32 i = 0; i < haystack_count; i++) {
    if (strcmp(haystack[i], needle) == 0) return (int32)i;
  }
  return -1;
}
