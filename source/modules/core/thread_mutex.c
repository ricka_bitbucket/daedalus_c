#include "thread.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "dd_assert.h"
#include "utils.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>
#else
typedef void*  HANDLE;
#endif


#if defined(_WIN32)

  // Criticial sections are more lightweight than mutexes
  // https://preshing.com/20111124/always-use-a-lightweight-mutex/
  #if 0
dd_mutex_t dd_mutex_create() {
  dd_mutex_t out;
  HANDLE mutex = CreateMutex(NULL,   // default security attributes
                             FALSE,  // initially not owned
                             NULL);
  assert(mutex != NULL);
  static_assert(sizeof(out) >= sizeof(mutex), "Mutex doesn't fix");
  memcpy(&out, &mutex, sizeof(mutex));
  return out;
}

void dd_mutex_lock(dd_mutex_t *in_mutex) {
  HANDLE mutex;
  memcpy(&mutex, &in_mutex, sizeof(mutex));
  DWORD result = WaitForSingleObject(mutex,      // handle to mutex
                                     INFINITE);  // no time-out interval
  if (result == WAIT_FAILED) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to lock mutex: %s\n", err);
  }
  assert(result == WAIT_OBJECT_0);  // TODO: error handling
}

void dd_mutex_release(dd_mutex_t* in_mutex) {
  HANDLE mutex;
  memcpy(&mutex, &in_mutex, sizeof(mutex));
  DWORD result = ReleaseMutex(mutex);
  (void)result;
  assert(result != 0);  // TODO: error handling
}

void dd_mutex_destroy(dd_mutex_t *in_mutex) {
  HANDLE mutex;
  memcpy(&mutex, &in_mutex, sizeof(mutex));
  CloseHandle(mutex);
}
  #else

dd_mutex_t dd_mutex_create() {
  dd_mutex_t out;
  CRITICAL_SECTION cs;
  InitializeCriticalSection(&cs);
  static_assert(sizeof(out) >= sizeof(cs), "Mutex doesn't fix");
  memcpy(&out, &cs, sizeof(cs));
  return out;
}

void dd_mutex_lock(dd_mutex_t* in_mutex) {
  CRITICAL_SECTION* cs = (CRITICAL_SECTION*)in_mutex;
  EnterCriticalSection(cs);
}

void dd_mutex_release(dd_mutex_t* in_mutex) {
  CRITICAL_SECTION* cs = (CRITICAL_SECTION*)in_mutex;
  LeaveCriticalSection(cs);
}

void dd_mutex_destroy(dd_mutex_t* in_mutex) {
  CRITICAL_SECTION* cs = (CRITICAL_SECTION*)in_mutex;
  DeleteCriticalSection(cs);
}
  #endif

#else
  #include <pthread.h>

dd_mutex_t dd_mutex_create() {
  dd_mutex_t out;
  pthread_mutex_t mutex;
  pthread_mutex_init(&mutex, NULL);

  static_assert(sizeof(out) >= sizeof(mutex), "Mutex doesn't fix");
  memcpy(&out, &mutex, sizeof(mutex));
  return out;
}

void dd_mutex_lock(dd_mutex_t* in_mutex) {
  pthread_mutex_t mutex;
  memcpy(&mutex, in_mutex, sizeof(mutex));
  pthread_mutex_lock(&mutex);
}

void dd_mutex_release(dd_mutex_t* in_mutex) {
  pthread_mutex_t mutex;
  memcpy(&mutex, in_mutex, sizeof(mutex));
  pthread_mutex_unlock(&mutex);
}

void dd_mutex_destroy(dd_mutex_t* in_mutex) {
  pthread_mutex_t mutex;
  memcpy(&mutex, in_mutex, sizeof(mutex));
  pthread_mutex_destroy(&mutex);
}

#endif
