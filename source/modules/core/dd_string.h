#pragma once

#include "types.h"

typedef struct dd_allocator dd_allocator;

/// Create a string view from a standard char*
dd_stringview_t stringview(const char* in_string);

array(char)
    dd_strcat(const dd_stringview_t in_a, const dd_stringview_t in_b, dd_allocator* allocator);

int strtoint(const dd_stringview_t in_str);
double strtodouble(const dd_stringview_t in_str);
bool are_strings_equal(const dd_stringview_t lhs, const dd_stringview_t rhs);

/// \return string from needle until end of haystack, or empty string view if not found.
dd_stringview_t stringviewstr(const dd_stringview_t haystack, const dd_stringview_t needle);

dd_stringview_t stringview_chr(const dd_stringview_t haystack, char needle);
dd_stringview_t stringview_trim(const dd_stringview_t s);

/// \return string before the split, and modifies \in_out_string to contain after the split.
dd_stringview_t stringview_pop_front(dd_stringview_t* in_out_string, size_t split_index);

dd_stringview_t* stringview_split(dd_stringview_t s, const dd_stringview_t splitter,
                                       dd_allocator* allocator);
