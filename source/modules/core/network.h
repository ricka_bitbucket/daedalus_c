/* Not going to wait until a connection is actually connected, it should be asynchronous.
 */
#pragma once

#include "types.h"

typedef struct dd_allocator dd_allocator;
typedef uint64 dd_network_connection_t;

dd_network_connection_t* dd_network_server_start(uint16 port, struct dd_allocator* allocator);
unsigned dd_network_server_num_connected_clients(const dd_network_connection_t* server);
dd_network_connection_t* dd_network_server_get_client_connection(
    const dd_network_connection_t* server, const unsigned index);

dd_network_connection_t* dd_network_connect(const char* destination, uint16 port,
                                            struct dd_allocator* allocator);

void dd_network_close_connection(dd_network_connection_t* connection);

// Is this connection connected?
bool dd_network_is_connection_connected(const dd_network_connection_t*);

// This data is not explicitly copied. Caller is responsible for keeping it around until the send
// is completed.
void dd_network_connection_send_data(dd_network_connection_t*, const void* in_data,
                                     const size_t in_count_bytes);

size_t dd_network_connection_bytes_received(dd_network_connection_t* connection);
void dd_network_connection_read_bytes(dd_network_connection_t* connection, uint8* out_data,
                                      size_t in_count_bytes);
