#pragma once

#include "types.h"

void dd_prof_start(const char* label);
void dd_prof_end();
void dd_prof_flush();

typedef void (*dd_prof_process_callback_t)(uint8* buffer, unsigned byte_count);

/**
 * Once per thread, before any profiling happens, call this function.
 * This will ensure the system knows about this thread, and will accurately report profiling blocks
 * from this thread.
 */
void dd_prof_thread_init(const char* thread_name);

void dd_prof_thread_destroy();

/**
 * To perform all processing of profiler buffers, set this callback.
 */
void dd_prof_set_process_callback(dd_prof_process_callback_t func);

#define DD_PROF() dd_prof_start(__FUNCTION__)

#define DD_PROF_END() dd_prof_end()
