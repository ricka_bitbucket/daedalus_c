#pragma once

#include "types.h"

struct dd_application_api {
  dd_stringview_t (*executable_folder)();
};

extern const struct dd_application_api dd_application;
