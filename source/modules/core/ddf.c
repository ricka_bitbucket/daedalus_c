#include "ddf.h"

#include <assert.h>

#include "containers.h"
#include "dd_math.h"

/*
ddf_header_t
ddf_block_description_t[header.block_count];
char strings[*]
header.block_count data blocks of varying size
*/

enum { DATA_ALIGNMENT = 256 };

typedef struct ddf_internal_header_t {
  char identifier[4];  // Should contain 'D','D','F', '\0'
  uint32 version;
  uint32 block_count;
  uint32 string_char_count;
} ddf_internal_header_t;

typedef struct ddf_internal_block_description_t {
  uint32 label_offset;   // Offset after the last block descriptor
  uint32 format_offset;  // Offset after the last block descriptor
  uint32 data_offset;    // Offset after the strings
  uint32 data_byte_count;
} ddf_internal_block_description_t;

static ddf_header_t get_header(const uint8* ddf_data) {
  const ddf_header_t* header = (const ddf_header_t*)(ddf_data);
  return *header;
}

static ddf_block_descriptor_t get_block_descriptor(const uint8* ddf_data, size_t block_index) {
  ddf_block_descriptor_t out = {0};

  const ddf_internal_header_t* header = (ddf_internal_header_t*)ddf_data;
  if (block_index >= header->block_count) return out;

  const ddf_internal_block_description_t* block_descriptor =
      ((ddf_internal_block_description_t*)(header + 1)) + block_index;

  out.label  = (char*)(ddf_data + block_descriptor->label_offset + sizeof(ddf_internal_header_t) +
                      header->block_count * sizeof(ddf_internal_block_description_t));
  out.format = (char*)(ddf_data + block_descriptor->format_offset + sizeof(ddf_internal_header_t) +
                       header->block_count * sizeof(ddf_internal_block_description_t));
  out.data_byte_count = block_descriptor->data_byte_count;
  if (out.data_byte_count != 0) {
    out.data_offset = block_descriptor->data_offset + sizeof(ddf_internal_header_t) +
                      header->block_count * sizeof(ddf_internal_block_description_t) +
                      header->string_char_count;
  }

  return out;
}

ddf_block_descriptor_t get_block_descriptor_with_label(const uint8* ddf_data, const char* label) {
  const ddf_internal_header_t* header = (ddf_internal_header_t*)ddf_data;

  for (size_t ib = 0; ib < header->block_count; ++ib) {
    const ddf_internal_block_description_t* block_descriptor =
        ((ddf_internal_block_description_t*)(header + 1)) + ib;
    const char* block_label =
        (char*)(ddf_data + block_descriptor->label_offset + sizeof(ddf_internal_header_t) +
                header->block_count * sizeof(ddf_internal_block_description_t));
    if (strcmp(block_label, label) != 0) continue;

    return get_block_descriptor(ddf_data, ib);
  }

  return (ddf_block_descriptor_t){0};
}

void init_header(array(uint8) * ddf_data, struct dd_allocator* allocator) {
  ddf_internal_header_t header = {
      .identifier = {'D', 'D', 'F', 0}, .version = 1, .block_count = 0, .string_char_count = 0};
  array_append(*ddf_data, &header, sizeof(header), allocator);
}

size_t add_block(array(uint8) * ddf_data, struct dd_allocator* allocator, const char* label,
                 const char* format) {
  assert(label != NULL);
  assert(format != NULL);

  const size_t label_len  = strlen(label) + 1;
  const size_t format_len = strlen(format) + 1;

  const size_t old_byte_count = array_count(*ddf_data);
  const size_t new_byte_count =
      old_byte_count + sizeof(ddf_internal_block_description_t) + (uint32)(label_len + format_len);

  array_resize(*ddf_data, new_byte_count, allocator);

  // Move all the data after the block descriptors back to make space
  ddf_internal_header_t* header = (ddf_internal_header_t*)*ddf_data;
  ddf_internal_block_description_t* block_descriptors =
      (ddf_internal_block_description_t*)(header + 1);

  const size_t old_string_offset = sizeof(ddf_internal_header_t) +
                                   header->block_count * sizeof(ddf_internal_block_description_t);
  const size_t new_string_offset = old_string_offset + sizeof(ddf_internal_block_description_t);

  const size_t old_block_data_offset = old_string_offset + header->string_char_count;
  const size_t new_block_data_offset =
      new_string_offset + header->string_char_count + format_len + label_len;

  memmove(*ddf_data + new_block_data_offset, *ddf_data + old_block_data_offset,
          old_byte_count - old_block_data_offset);
  memmove(*ddf_data + new_string_offset, *ddf_data + old_string_offset, header->string_char_count);

  const uint32 label_offset                        = header->string_char_count;
  const uint32 format_offset                       = (uint32)(label_offset + label_len);
  const ddf_internal_block_description_t new_block = {.label_offset  = label_offset,
                                                      .format_offset = format_offset};
  memcpy(*ddf_data + new_string_offset + label_offset, label, label_len);
  memcpy(*ddf_data + new_string_offset + format_offset, format, format_len);

  block_descriptors[header->block_count] = new_block;
  header->block_count++;
  header->string_char_count += (uint32)(label_len + format_len);

  return (size_t)(header->block_count - 1);
}

void reserve_block_data(array(uint8) * ddf_data, struct dd_allocator* allocator,
                        size_t block_index, uint32 byte_count) {
  if (byte_count == 0) return;

  const size_t aligned_byte_count = align_up_uint32(byte_count, DATA_ALIGNMENT);

  ddf_internal_header_t* header = (ddf_internal_header_t*)*ddf_data;
  assert(block_index < header->block_count);

  ddf_internal_block_description_t* block_descriptor =
      ((ddf_internal_block_description_t*)(header + 1)) + block_index;

  assert(block_descriptor->data_byte_count == 0 ||
         block_descriptor->data_byte_count == byte_count);

  if (block_descriptor->data_byte_count == 0) {
    const size_t string_offset = sizeof(ddf_internal_header_t) +
                                 header->block_count * sizeof(ddf_internal_block_description_t);

    const size_t block_data_offset = string_offset + header->string_char_count;

    block_descriptor->data_offset     = (uint32)(array_count(*ddf_data) - block_data_offset);
    block_descriptor->data_byte_count = (uint32)byte_count;

    array_resize(*ddf_data, array_count(*ddf_data) + aligned_byte_count, allocator);
  }
}

void set_block_data(array(uint8) * ddf_data, struct dd_allocator* allocator, size_t block_index,
                    const void* vdata, uint32 byte_count) {
  assert(vdata != NULL);
  if (byte_count == 0) return;

  const uint8* data = (const uint8*)vdata;

  reserve_block_data(ddf_data, allocator, block_index, byte_count);

  ddf_internal_header_t* header = (ddf_internal_header_t*)*ddf_data;
  assert(block_index < header->block_count);

  ddf_internal_block_description_t* block_descriptor =
      ((ddf_internal_block_description_t*)(header + 1)) + block_index;

  const size_t string_offset = sizeof(ddf_internal_header_t) +
                               header->block_count * sizeof(ddf_internal_block_description_t);
  const size_t block_data_offset = string_offset + header->string_char_count;

  memcpy(*ddf_data + block_data_offset + block_descriptor->data_offset, data, byte_count);
}

const struct ddf_api ddf_api = {
    .read  = {.header                      = &get_header,
             .block_descriptor            = &get_block_descriptor,
             .block_descriptor_with_label = &get_block_descriptor_with_label},
    .write = {.init_header        = &init_header,
              .add_block          = &add_block,
              .set_block_data     = &set_block_data,
              .reserve_block_data = &reserve_block_data}};
