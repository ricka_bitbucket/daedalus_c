#pragma once

#include "types.h"

enum EMouseButton {
  MouseButtonNone   = 0x00,
  MouseButtonLeft   = 0x01,
  MouseButtonMiddle = 0x02,
  MouseButtonRight  = 0x04
};

typedef struct dd_mouse_t {
  uint16 down;
  uint16 down_prev;

  int16 x, y;
  int16 dx, dy;
  int dscroll;
} dd_mouse_t;

typedef enum EKeyCode {
  DD_KEY_BACKSPACE = 8,
  DD_KEY_RETURN    = 13,
  DD_KEY_SHIFT     = 16,
  DD_KEY_CTRL      = 17,
  DD_KEY_ALT       = 18,
  DD_KEY_ESCAPE    = 27,
  DD_KEY_PAGEUP    = 33,
  DD_KEY_PAGEDOWN  = 34,

  DD_KEY_END  = 35,
  DD_KEY_HOME = 36,

  DD_KEY_LEFT  = 37,
  DD_KEY_UP    = 38,
  DD_KEY_RIGHT = 39,
  DD_KEY_DOWN  = 40,

  DD_KEY_DELETE = 46

} EKeyCode;
