/**
 * Testing
 * ===
 *
 * Include this file wherever you want to make test cases. In one file in your project
 *    #define DD_TEST_GENERATE_MAIN
 * before including test->h. That file will then automatically generate test_var
 * main(..) function to run the tests.
 */
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "core/memory.h"
#include "log.h"

#ifdef __cplusplus
extern "C" {
#endif

struct dd_test_data_;
typedef void (*dd_test_func)(struct dd_test_data_* test_data);
void dd_test_add(struct dd_test_data_* test_data, const char*, dd_test_func);

typedef struct dd_test_data_ {
  dd_test_func* runner_funcs;
  const char** runner_suites;
  const char** runner_names;
  dd_test_func* next_free_func;
  const char** next_free_suite;
  const char** next_free_name;

  unsigned num_funcs;

  const char* active_suite;

  unsigned error_count;
} dd_test_data_t;

#define DD_TEST_IS_FALSE(test_var)                    \
  {                                                   \
    bool at = (test_var);                             \
    if (at != 0) {                                    \
      DD_TEST_REPORT_ERROR();                         \
      printf("\t%s != 0\n", #test_var);               \
      printf("\t%i != 0\n", at); /*assert(FALSE);  */ \
    }                                                 \
  }
#define DD_TEST_IS_TRUE(test_var)                     \
  {                                                   \
    bool at = (test_var);                             \
    if (at == 0) {                                    \
      DD_TEST_REPORT_ERROR();                         \
      printf("\t(%s) != 0\n", #test_var);             \
      printf("\t%i != 0\n", at); /*assert(FALSE);  */ \
    }                                                 \
  }

static inline void dd_test_equal_strings(const char* file, unsigned line,
                                         struct dd_test_data_* test_data, const char* test_var,
                                         const char* expected_value, const char* a_lit,
                                         const char* b_lit) {
  if (test_var == NULL || expected_value == NULL || strcmp((test_var), (expected_value)) != 0) {
    printf("%s(%u): Failed:\n", file, line);
    ++(test_data->error_count);
    printf("\t%s != %s\n", a_lit, b_lit);                     /*assert(FALSE); */
    printf("\t\"%s\" != \"%s\"\n", test_var, expected_value); /*assert(FALSE); */
  }
}

#define DD_TEST_EQUAL_STRINGS(test_var, expected_value)                                     \
  dd_test_equal_strings(__FILE__, __LINE__, test_data, test_var, expected_value, #test_var, \
                        #expected_value);

#define DD_TEST_EQUAL_INT(test_var, expected_value)        \
  {                                                        \
    int at = (test_var);                                   \
    int bt = (expected_value);                             \
    if (at != bt) {                                        \
      DD_TEST_REPORT_ERROR();                              \
      printf("\t%s != %s\n", #test_var, #expected_value);  \
      printf("\t%i != %i\n", at, bt); /*assert(FALSE);  */ \
    }                                                      \
  }

#define DD_TEST_EQUAL_UINT(test_var, expected_value)       \
  {                                                        \
    unsigned at = (test_var);                              \
    unsigned bt = (expected_value);                        \
    if (at != bt) {                                        \
      DD_TEST_REPORT_ERROR();                              \
      printf("\t%s != %s\n", #test_var, #expected_value);  \
      printf("\t%u != %u\n", at, bt); /*assert(FALSE);  */ \
    }                                                      \
  }

#define DD_TEST_EQUAL_DOUBLE(test_var, expected_value)       \
  {                                                          \
    double at = (test_var);                                  \
    double bt = (expected_value);                            \
    if (at != bt) {                                          \
      DD_TEST_REPORT_ERROR();                                \
      printf("\t%s != %s\n", #test_var, #expected_value);    \
      printf("\t%lf != %lf\n", at, bt); /*assert(FALSE);  */ \
    }                                                        \
  }

#define DD_TEST_EQUAL_FLOAT(test_var, expected_value)      \
  {                                                        \
    float at = (test_var);                                 \
    float bt = (expected_value);                           \
    if (at != bt) {                                        \
      DD_TEST_REPORT_ERROR();                              \
      printf("\t%s != %s\n", #test_var, #expected_value);  \
      printf("\t%f != %f\n", at, bt); /*assert(FALSE);  */ \
    }                                                      \
  }

/**
 * If you implement some test functions of your own, be sure to add test_var call to this so the
 * error is properly accounted for in the test runner.
 */
#define DD_TEST_REPORT_ERROR()                       \
  {                                                  \
    printf("%s(%u): Failed:\n", __FILE__, __LINE__); \
    ++(test_data->error_count);                      \
  }

/**
 * Code to implement test case automatic registration is below. This relies on some platform
 * specific trickery to automatically run test_var functions before main() starts.
 *
 * See https://stackoverflow.com/questions/1113409/attribute-constructor-equivalent-in-vc for
 * reference.
 */
#ifdef __cplusplus
  #define INITIALIZER(f)    \
    static void f(void);    \
    struct f##_t_ {         \
      f##_t_(void) { f(); } \
    };                      \
    static f##_t_ f##_;     \
    static void f(void)
#elif defined(_MSC_VER)

#else
  #define INITIALIZER(f)                              \
    static void f(void) __attribute__((constructor)); \
    static void f(void)
#endif

// The following pragma can be used to put stuff after main
//#pragma data_seg(".CRT$XPU")
#if defined(_MSC_VER)
  #define TEST_CASE_IMPL(test_func, unique_count) static void test_func(dd_test_data_t* test_data)
#else
  #define TEST_CASE_IMPL(test_func, unique_count) static void test_func(dd_test_data_t* test_data)
#endif
#define TEST_CASE_EXPAND(test_var, expected_value) TEST_CASE_IMPL(test_var, expected_value)
#define TEST_CASE(test_var) TEST_CASE_EXPAND(test_var, __COUNTER__)

#define TEST_ADD_CASE(func_name, test_data) dd_test_add(test_data, #func_name, &func_name);

#define TEST_SUITE(func_name, test_data) void suite_##func_name(dd_test_data_t* test_data)

#define TEST_ADD_SUITE(func_name, test_data)                            \
  (test_data)->active_suite = #func_name, suite_##func_name(test_data), \
  (test_data)->active_suite = NULL

/**
 * Disable warnings about unused variables. If test_var test case has no test checks this will
 * fire, with no way to avoid that with test_var single macro for test_var test case. Enabling this
 * warning isn't too bad, as test cases should be easy to write, so disabling that rather strict
 * warning is acceptable.
 */
#if defined(_MSC_VER)
  #pragma warning(disable : 4100)
#elif defined(__clang__)
  #pragma clang diagnostic ignored "-Wunused-parameter"
#endif

/**
 * Disable warnings about unknown functions. There is no way to have both test_var call and
 * test_var declaration with test_var single macro. By disabling the warning about unknown
 * functions everything works as expected.
 */
#if defined(_MSC_VER)
  #pragma warning(disable : 4013)
#elif defined(__clang__)
  #pragma clang diagnostic ignored "-Wimplicit-function-declaration"
#endif

/**
 * When this macro is defined the code automatically generates test_var main() function which calls
 * the test runner, and the code to manage the test functions.
 */

#include "containers.h"

/**
 * Tests are stored in groups of (DD_NUM_TEST_SIZE+1), in which the last entry will point at the
 * next block.
 *
 * The memory allocated this way is not freed manually, as it is assumed the OS will release the
 * memory when ending the program.
 */
#define DD_NUM_TEST_SIZE 50

#if defined(DD_TEST_GENERATE_MAIN)
void dd_test_add(dd_test_data_t* test, const char* name, dd_test_func f) {
  if ((test->num_funcs % DD_NUM_TEST_SIZE) == 0) {
    // Need to allocate more
    if (test->num_funcs == 0) {
      test->runner_names    = (const char**)malloc((DD_NUM_TEST_SIZE + 1) * sizeof(const char*));
      test->runner_suites   = (const char**)malloc((DD_NUM_TEST_SIZE + 1) * sizeof(const char*));
      test->runner_funcs    = (dd_test_func*)malloc((DD_NUM_TEST_SIZE + 1) * sizeof(dd_test_func));
      test->next_free_name  = test->runner_names;
      test->next_free_suite = test->runner_suites;
      test->next_free_func  = test->runner_funcs;
    } else {
      *test->next_free_name  = (const char*)malloc((DD_NUM_TEST_SIZE + 1) * sizeof(const char*));
      *test->next_free_suite = (const char*)malloc((DD_NUM_TEST_SIZE + 1) * sizeof(const char*));
      *test->next_free_func  = (dd_test_func)malloc((DD_NUM_TEST_SIZE + 1) * sizeof(dd_test_func));
      test->next_free_name   = (const char**)*test->next_free_name;
      test->next_free_suite  = (const char**)*test->next_free_suite;
      test->next_free_func   = (dd_test_func*)*test->next_free_func;
    }
  }

  *test->next_free_suite = test->active_suite;
  *test->next_free_name  = name;
  *test->next_free_func  = f;

  ++test->num_funcs;
  ++test->next_free_func;
  ++test->next_free_name;
  ++test->next_free_suite;
}

int dd_perform_tests(dd_test_data_t* test, int argc, char** args) {
  uint8* stack_buffer           = malloc(5000);
  dd_allocator* stack_allocator = dd_memory_stack_allocator_init(stack_buffer, 5000);

  const char** add_filter    = {0};
  const char** remove_filter = {0};
  // No need to test the last argument as the actual filter string is later on
  printf("Running tests\n");
  for (int i = 0; i < argc - 1; ++i) {
    if (strstr(args[i], "--filter-in") != NULL) {
      array_push(add_filter, args[i + 1], stack_allocator);
      printf("\tfilter-in: %s\n", args[i + 1]);
    }
    if (strstr(args[i], "--filter-out") != NULL) {
      array_push(remove_filter, args[i + 1], stack_allocator);
      printf("\tfilter-out: %s\n", args[i + 1]);
    }
  }
  const size_t num_add_filters    = array_count(add_filter);
  const size_t num_remove_filters = array_count(remove_filter);

  if (num_add_filters == 0 && num_remove_filters == 0) {
    array_push(add_filter, "", stack_allocator);
  }

  printf("[===========] Running %u tests\n", test->num_funcs);

  const char** test_suite_it = test->runner_suites;
  const char** test_name_it  = test->runner_names;
  dd_test_func* test_func_it = test->runner_funcs;

  size_t tests_skipped = 0;

  for (uint32 i = 0; i < test->num_funcs; ++i, ++test_name_it, ++test_suite_it, ++test_func_it) {
    if ((i % DD_NUM_TEST_SIZE) == 0 && i != 0) {
      test_suite_it = *(const char***)test_suite_it;
      test_name_it  = *(const char***)test_name_it;
      test_func_it  = *(dd_test_func**)test_func_it;
    }

    const char* test_suite = *test_suite_it;
    const char* test_name  = *test_name_it;
    dd_test_func test_func = *test_func_it;

    bool should_perform_test = (num_add_filters == 0);
    for (size_t filter_idx = 0; (filter_idx < num_add_filters) && !should_perform_test;
         ++filter_idx) {
      if ((strstr(test_name, add_filter[filter_idx]) != NULL) ||
          (strstr(test_suite, add_filter[filter_idx]) != NULL)) {
        should_perform_test = true;
      }
    }
    for (size_t filter_idx = 0; (filter_idx < num_remove_filters) && should_perform_test;
         ++filter_idx) {
      if ((strstr(test_name, remove_filter[filter_idx]) != NULL) ||
          (strstr(test_suite, remove_filter[filter_idx]) != NULL)) {
        should_perform_test = false;
      }
    }

    if (!should_perform_test) {
      tests_skipped++;
      continue;
    }

    size_t error_count_before = test->error_count;

    char suite[1024] = {0};
    if (test_suite) {
      snprintf(suite, sizeof(suite) - 2, "%s.", test_suite);
      suite[sizeof(suite) - 1] = 0;
    }
    printf("[ RUN       ] %s%s\n", suite, test_name);
    fflush(stdout);
    test_func(test);

    dd_log_flush();

    if (error_count_before == test->error_count) {
      printf("[        OK ] %s%s\n", suite, test_name);
    } else {
      printf("[    FAILED ] %s%s\n", suite, test_name);
    }
  }

  printf("[===========] Finished running %u/%u tests, with %u errors\n",
         (uint32)(test->num_funcs - tests_skipped), (uint32)(test->num_funcs),
         (uint32)(test->error_count));

  return (int)(test->error_count);
}
#endif

// int main(int argc, char** args) { return (int)dd_perform_tests(argc, args); }

#ifdef __cplusplus
}
#endif
