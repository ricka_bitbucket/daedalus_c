#include "api_registry.h"

#include <assert.h>
#include <string.h>

#include "containers.h"
#include "memory.h"
#include "utils.h"

// Type declarations
// ====================
struct api_entry {
  void* api;
  char name[API_NAME_MAX_LENGTH];
  uint32 version;
  uint32 size_byte_count;
};

// Data for APIs
static struct {
  struct dd_allocator* allocator;
  struct api_entry* apis; /* array */
} api_registry_ = {NULL};

// Function definitions
// ====================
static void registry_init(struct dd_allocator* allocator) {
  if (api_registry_.apis) {
    array_free(api_registry_.apis, api_registry_.allocator);
  }

  api_registry_.allocator = allocator;
  api_registry_.apis      = NULL;
}

static void registry_deinit(void) {
  if (api_registry_.apis) {
    array_free(api_registry_.apis, api_registry_.allocator);
  }

  api_registry_.allocator = NULL;
  api_registry_.apis      = NULL;
}

static void registry_register_api(const char* name, uint32 version, const void* api,
                                  uint32 api_byte_count) {
  // Check if this is api is already registered
  size_t api_count = array_count(api_registry_.apis);
  for (size_t ia = 0; ia < api_count; ia++) {
    struct api_entry* stored_api = api_registry_.apis + ia;
    const bool is_same_name      = strcmp(stored_api->name, name) == 0;
    const bool is_same_version   = (stored_api->version == version);
    if (is_same_name && is_same_version) {
      assert(api_byte_count <= stored_api->size_byte_count);
      memcpy(stored_api->api, api, api_byte_count);
      return;
    }
  }

  // It's a new api
  struct api_entry stored_api = {.api     = dd_alloc(api_registry_.allocator, api_byte_count, 4),
                                 .version = version,
                                 .size_byte_count = api_byte_count};
  memcpy(stored_api.api, api, api_byte_count);
  strncpy(stored_api.name, name, API_NAME_MAX_LENGTH);
  array_push(api_registry_.apis, stored_api, api_registry_.allocator);

  dd_devprintf("  Api '%s' registered\n", name);
}

static void* registry_get_api(const char* name, uint32 version) {
  size_t api_count = array_count(api_registry_.apis);
  for (size_t ia = 0; ia < api_count; ia++) {
    struct api_entry* stored_api = api_registry_.apis + ia;
    const bool is_same_name      = strcmp(stored_api->name, name) == 0;
    const bool is_same_version   = (stored_api->version == version);
    if (is_same_name && is_same_version) {
      return stored_api->api;
    }
  }

  return NULL;
}

const dd_api_registry_t dd_api_registry = {.init         = &registry_init,
                                           .deinit       = &registry_deinit,
                                           .register_api = &registry_register_api,
                                           .get_api      = &registry_get_api};
