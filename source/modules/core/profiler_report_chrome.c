#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "profiler.h"
#include "profiler.inl"
#include "types.h"

#if !defined(_WIN32)
static uint64 GetCurrentProcessId() {
  pid_t id = getpid();
  return (uint64)id;
}
#endif

// 1. Save the report to a file
// 2. Go to chrome://tracing
// 3. Load the saved file
void dd_prof_report_chrome(void* buffer, unsigned buffer_size) {
  uint8* buf     = (uint8*)buffer;
  uint8* buf_end = buf + buffer_size;

  const uint64 process_id = GetCurrentProcessId();
  const uint64 initial    = profiler_first_timestamp_in_buffer(buffer, buffer_size);
  printf("[");
  const dd_perf_threaddata_t* td = NULL;
  while (buf != buf_end) {
    uint8 sentinal = *buf;
    buf += 1;
    if (sentinal == SENTINAL_START) {
      const dd_perf_startdata* sd = (const dd_perf_startdata*)buf;
      buf += sizeof(dd_perf_startdata);

      printf(
          "{\"name\":\"%s\",\"cat\":\"%s\", \"ph\" : \"B\", \"pid\" : %llu, \"tid\" : %llu, "
          "\"ts\" "
          ": "
          "%lld},",
          sd->label, sd->category, process_id, td->thread_id, (sd->timestamp - initial));
    } else if (sentinal == SENTINAL_END) {
      const dd_perf_enddata* ed = (const dd_perf_enddata*)buf;
      buf += sizeof(dd_perf_enddata);

      printf("{\"ph\" : \"E\", \"pid\" : %llu, \"tid\" : %llu, \"ts\" : %lld},", process_id,
             td->thread_id, (ed->timestamp - initial));
    } else if (sentinal == SENTINAL_THREAD_ID) {
      td = (const dd_perf_threaddata_t*)buf;
      buf += sizeof(dd_perf_threaddata_t);
    } else if (sentinal == SENTINAL_THREAD_NAME) {
      buf += sizeof(dd_perf_threadname_t);
    } else {
      assert(false);  // Unknown id
    }
  }
  // Don't end with a ], chrome://tracing can handle it without, but can't handle the trailing ,
  // after the last item
  printf("\n");
}
