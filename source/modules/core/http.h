#pragma once

#include "types.h"

struct dd_http_api {
  void (*url_to_file)(const char* url, const char* file_path);
};

extern const struct dd_http_api dd_http_api;
