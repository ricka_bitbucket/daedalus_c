/**
 * Logging
 * ====
 *
 * Requirements
 * ----
 * Logging should be easy to add, with a low performance cost. The API should be flexible enough to
 * allow for different backends (at compile-time, not run-time). One such backend would simply log
 * to a file. Another backend would log to a service (this will be the main implementation).
 * Another backend might remove logging completely (for optimized release builds for example).
 *
 * Considerations for out-of-process logging
 * ----
 * - Since PCs have many cores, and most programs dont't use them all, it is beneficial to perform
 * as little processing as possible in the main process, and more in another process running on a
 * different core.
 * - See the following slide-deck for basic ideas (from the assert section):
 * https://www.slideshare.net/younggikim16/developing-imperfect-software
 * - Most logging will not log arbitrary strings from the stack. Most things will be numeric
 * values, or fixed strings.
 * - Having fixed information for all logged items is beneficial: source file, source line,
 * function name, time of logging, thread id
 *
 * Design for out-of-process logging
 * ----
 * Assuming that in most of the cases a log statement only contains fixed strings and numeric
 * values, the logging system could store the char* and the actual values of the arguments in a
 * buffer, and periodically send this buffer to the out-of-process service. The service can then do
 * the processing to assemble the logged string into what is actually logged. Any missing char*
 * info will be requested as a batch from the logged process. Since multiple logs from the same
 * location in the code will use the same char* any subsequent logging will have minimal cost in
 * the main process.
 *
 * Assuming a printf-style log macro that looks like:
 *      DD_DEBUG_LOG("my vec3: %f,%f,%f", 1.f, 0.f, 1.f);
 * that will be expanded to something like:
 *      dd_log_printf(
 *          __FILE__, __LINE__, __FUNCTION__, ELogSeverityDebug, __thread_id, time,
 *          "my vec3: %f,%f,%f", 1.f, 0.f, 1.f);
 * the logging system can push 11 (1 count + 10 values) values (=11*8=88 bytes) into a buffer.
 * Assuming the buffer is then flushed to the server, the server will reply back with a request for
 * 4 strings (the 3 fixed ones, and 1 actual log char*). The logging system then replies with the
 * null-terminated strings for each of those. Since they are all static strings, the pointers will
 * still be valid.
 * On a second call from the same calling site, again 11 values are pushed into the buffer. When
 * sending this to the server, it will have cached the strings from the first call, so it has
 * enough information from just the 88 bytes sent to create the full log message.
 *
 * Implementation tip: https://p99.gforge.inria.fr/p99-html/group__variadic.html to get the number
 * of arguments without needing to parse the format string.
 *
 * Only in the case of %s specifier in the format-string does the system need to do more
 * processing. It will need to copy the full input string at that position into the buffer. This is
 * expected to a relatively rare occurance, so is allowed to be more expensive processing wise.
 * There is another simpler option though: when this case is encountered, immediately force a flush
 * of the buffer to the server, and wait for the requests for strings (which could be implemented
 * as a return value from the server to the buffer flush). These requests can be serviced
 * immediately, and might include a flag indicating that a specific string is volatile and so
 * should not be cached on the server.
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "log_structs.inl"

#ifdef _MSC_VER  // Microsoft compilers

  #define GET_ARG_COUNT(...) INTERNAL_EXPAND_ARGS_PRIVATE(INTERNAL_ARGS_AUGMENTER(__VA_ARGS__))

  #define INTERNAL_ARGS_AUGMENTER(...) unused, __VA_ARGS__
  #define INTERNAL_EXPAND_ARGS_PRIVATE(...)                                                      \
    INTERNAL_EXPAND(INTERNAL_GET_ARG_COUNT_PRIVATE(                                              \
        __VA_ARGS__, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, \
        50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29,  \
        28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6,  \
        5, 4, 3, 2, 1, 0))
  #define INTERNAL_GET_ARG_COUNT_PRIVATE(                                                       \
      _1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_, _11_, _12_, _13_, _14_, _15_, _16_,    \
      _17_, _18_, _19_, _20_, _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_, _31_, \
      _32_, _33_, _34_, _35_, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48,  \
      _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66, \
      _67, _68, _69, _70, count, ...)                                                           \
    count

#else  // Non-Microsoft compilers

  #define GET_ARG_COUNT(...)                                                                   \
    INTERNAL_GET_ARG_COUNT_PRIVATE(0, __VA_ARGS__, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, \
                                   59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, \
                                   44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, \
                                   29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, \
                                   14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
  #define INTERNAL_GET_ARG_COUNT_PRIVATE(                                                        \
      _0, _1_, _2_, _3_, _4_, _5_, _6_, _7_, _8_, _9_, _10_, _11_, _12_, _13_, _14_, _15_, _16_, \
      _17_, _18_, _19_, _20_, _21_, _22_, _23_, _24_, _25_, _26_, _27_, _28_, _29_, _30_, _31_,  \
      _32_, _33_, _34_, _35_, _36, _37, _38, _39, _40, _41, _42, _43, _44, _45, _46, _47, _48,   \
      _49, _50, _51, _52, _53, _54, _55, _56, _57, _58, _59, _60, _61, _62, _63, _64, _65, _66,  \
      _67, _68, _69, _70, count, ...)                                                            \
    count

#endif
/*
static_assert(GET_ARG_COUNT() == 0, "GET_ARG_COUNT() failed for 0 arguments");
static_assert(GET_ARG_COUNT(1) == 1, "GET_ARG_COUNT() failed for 1 argument");
static_assert(GET_ARG_COUNT(1, 2) == 2, "GET_ARG_COUNT() failed for 2 arguments");
static_assert(GET_ARG_COUNT(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
                            39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56,
                            57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70) == 70,
              "GET_ARG_COUNT() failed for 70 arguments");
*/

typedef enum {
  ELogSeverityFatal = 0,
  ELogSeverityError,
  ELogSeverityWarning,
  ELogSeverityInfo,
  ELogSeverityDebug
} ELogSeverity;

/**
 * Use DD_LOG macro to fill in the details of for this function automatically.
 */
void dd_log(const dd_log_entry* entry, ...);

// ref:
// https://stackoverflow.com/questions/4750688/how-to-single-out-the-first-parameter-sent-to-a-macro-taking-only-a-variadic-par
// https:// stackoverflow.com/questions/20818800/variadic-macro-and-trailing-comma
#define FIRST_ARG_SINGLE(N) N
#define FIRST_ARG(N, ...) N
#define NOT_FIRST_ARG(N, ...) __VA_ARGS__
#define CONCAT_IMPL(a, b) a##b
#define CONCAT(a, b) CONCAT_IMPL(a, b)
#define LINE_ENTRY CONCAT(entry, __LINE__)

#define FORWARD_SINGLE_ARG(_1, ...) _1
#define FORWARD_MULTIPLE_ARGS(_1, ...) _1, __VA_ARGS__

#define GET_ARG(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, \
                _19, _20, _21, _22, ...)                                                         \
  _22
#define INTERNAL_EXPAND(a) a

#if defined(_MSC_VER)
  #define ARG_CHOOSER(s, m, ...) \
    INTERNAL_EXPAND(             \
        GET_ARG(__VA_ARGS__, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, s))
#else
  #define ARG_CHOOSER(s, m, ...) \
    GET_ARG(__VA_ARGS__, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, m, s, )
#endif

#define ARG_CHOOSE(...) ARG_CHOOSER(FORWARD_SINGLE_ARG, FORWARD_MULTIPLE_ARGS, __VA_ARGS__)

#if !defined(DD_LOG_DISABLE)
  // Note that the C11-standard specifies in 6.7.9.10 that static values are initialized to 0
  #define DD_LOG(severity, ...)                                                                \
    {                                                                                          \
      static dd_log_entry LINE_ENTRY = {                                                       \
          __FUNCTION__,                                                                        \
          __FILE__,                                                                            \
          INTERNAL_EXPAND(ARG_CHOOSER(FIRST_ARG_SINGLE, FIRST_ARG, __VA_ARGS__)(__VA_ARGS__)), \
          GET_ARG_COUNT(__VA_ARGS__) - 1,                                                      \
          severity,                                                                            \
          __LINE__,                                                                            \
          0};                                                                                  \
      dd_log(ARG_CHOOSE(__VA_ARGS__)(&LINE_ENTRY, NOT_FIRST_ARG(__VA_ARGS__)));                \
    }
#else
static inline void no_log(const char* format, ...) { (void)format; }

  #define DD_LOG(severity, ...) no_log(__VA_ARGS__)
#endif

/**
 * Flush everything in the log buffer to output (whatever that is set as.)
 */
void dd_log_flush();

typedef void (*dd_log_process_callback_t)(void* buffer, unsigned byte_count);
typedef void (*dd_log_print_callback)(const char* file_path, unsigned line,
                                      const char* function_name, ELogSeverity severity,
                                      const char* formatted_log);

/**
 * Set the function that will be called to print the log.
 *
 * @param func may be NULL
 */
void dd_log_set_output_callback(dd_log_print_callback func);

/**
 * To perform all processing of log buffers, set this callback.
 */
void dd_log_set_process_callback(dd_log_process_callback_t func);

#ifdef __cplusplus
}
#endif
