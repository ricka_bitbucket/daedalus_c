#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define PLATFORM_IOS 0
#define PLATFORM_OSX 0
#define PLATFORM_TVOS 0
#define PLATFORM_WATCHOS 0
#define PLATFORM_AGB 0
#define PLATFORM_NDS 0
#define PLATFORM_NITRO 0
#define PLATFORM_WINDOWS 0
#define PLATFORM_WEB 0

#if defined(_WIN32) || defined(_WIN64)
#undef PLATFORM_WINDOWS
#define PLATFORM_WINDOWS 1
#define PLATFORM_PRODUCER 1
#endif

#if defined(__APPLE__)
#include "TargetConditionals.h"
#if TARGET_OS_IPHONE

#define PLATFORM_VERSION_MINIMUM(ver) \
  ([[NSProcessInfo processInfo] operatingSystemVersion].majorVersion >= ver)

#if TARGET_OS_IOS
#undef PLATFORM_IOS
#define PLATFORM_IOS 1
#elif TARGET_OS_TV
#undef PLATFORM_TVOS
#define PLATFORM_TVOS 1
#elif TARGET_OS_WATCH
#undef PLATFORM_WATCHOS
#define PLATFORM_WATCHOS 1
#endif
#else
#undef PLATFORM_OSX
#define PLATFORM_OSX 1
#define PLATFORM_PRODUCER 1
#endif
#endif

#if defined(__EMSCRIPTEN__) || defined(EMSCRIPTEN)
#undef PLATFORM_WEB
#define PLATFORM_WEB 1
#endif

#ifndef PLATFORM_PRODUCER
#define PLATFORM_PRODUCER 0
#endif

#ifdef __cplusplus
}
#endif
