#include "log.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "log_structs.inl"
#include "thread.h"
#include "types.h"

static uint8 tmp_buffer[10 * 1024];
static uint8* tmp_buffer_end = tmp_buffer + sizeof(tmp_buffer);
static uint8* tmp_buffer_ptr = tmp_buffer;
dd_mutex_t g_log_mutex       = {0};
static bool dd_log_is_inited = false;

void dd_log_append_to_buffer_(const uint8* in_buffer, const unsigned bytes_count) {
  if (!dd_log_is_inited) {
    dd_log_is_inited = true;
    g_log_mutex      = dd_mutex_create();
  }

  dd_mutex_lock(&g_log_mutex);
  uint8* next_entry_ptr = tmp_buffer_ptr + bytes_count;

  if (next_entry_ptr > tmp_buffer_end) {
    dd_log_flush();
    assert(tmp_buffer_ptr == tmp_buffer);
    next_entry_ptr = tmp_buffer_ptr + bytes_count;
  }

  memcpy(tmp_buffer_ptr, in_buffer, bytes_count);

  tmp_buffer_ptr = next_entry_ptr;

  assert(tmp_buffer_ptr <= tmp_buffer_end);

  dd_mutex_release(&g_log_mutex);
}

enum { MAX_LOG_ARGUMENTS = 32 };
void dd_log(const dd_log_entry* entry, ...) {
  uint8 local_buffer[sizeof(entry) +
                     MAX_LOG_ARGUMENTS * (sizeof(int) + sizeof(double) + sizeof(void*))];

  const unsigned count = entry->count_args;

  memcpy(local_buffer, &entry, sizeof(entry));

  va_list ap;

#if defined(__x86_64)
  /**
   * 64-bit stores the arguments separately, so if the first double argument is at position 4 it
   * will still be the first argument returned from va_arg(ap,double). For now simply get the
   * maximum count of ints, double, and void*, and sort it out during processing.
   */
  void** ptr_buffer = (void**)(local_buffer + sizeof(entry));
  va_start(ap, entry);
  for (unsigned i = 0; i < count; ++i) {
    *(ptr_buffer++) = va_arg(ap, void*);
  }
  va_end(ap);
  va_start(ap, entry);
  int* int_buffer = (int*)ptr_buffer;
  for (unsigned i = 0; i < count; ++i) {
    *(int_buffer++) = va_arg(ap, int);
  }
  va_end(ap);
  double* double_buffer = (double*)int_buffer;
  va_start(ap, entry);
  for (unsigned i = 0; i < count; ++i) {
    *(double_buffer++) = va_arg(ap, double);
  }
  va_end(ap);
#else
  va_start(ap, entry);
  double* double_buffer = (double*)(local_buffer + sizeof(entry));
  for (unsigned i = 0; i < count; ++i) {
    *(double_buffer++) = va_arg(ap, double);
  }
  va_end(ap);
#endif

  const unsigned total_entry_size = (unsigned)(((uint8*)double_buffer) - local_buffer);

  dd_log_append_to_buffer_(local_buffer, total_entry_size);

  if (entry->severity <= ELogSeverityError) {
    dd_log_flush();
  }
}

extern void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                                dd_log_print_callback func);

static void default_print(const char* file_path, unsigned line, const char* function_name,
                          ELogSeverity severity, const char* formatted_string) {
  printf("%s(%u) %s | %i | %s", file_path, line, function_name, severity, formatted_string);
}

static dd_log_print_callback dd_log_output_callback = &default_print;

static void dd_log_process_default(void* buffer, unsigned byte_count) {
  dd_log_process_buffer_with_callback(buffer, byte_count, dd_log_output_callback);
}
static dd_log_process_callback_t dd_log_process_active = &dd_log_process_default;

void dd_log_set_output_callback(dd_log_print_callback func) {
  dd_log_output_callback = func;
  if (func == NULL) {
    dd_log_output_callback = &default_print;
  }
  dd_log_process_active = &dd_log_process_default;
}

void dd_log_set_process_callback(dd_log_process_callback_t func) {
  dd_log_process_active = func;
  if (func == NULL) {
    dd_log_process_active  = &dd_log_process_default;
    dd_log_output_callback = &default_print;
  }
}

// TODO: replace this temporary flush function
void dd_log_flush() {
  if (!dd_log_is_inited) {
    dd_log_is_inited = true;
    g_log_mutex      = dd_mutex_create();
  }

  dd_mutex_lock(&g_log_mutex);
  dd_log_process_active(tmp_buffer, (unsigned)(tmp_buffer_ptr - tmp_buffer));

  // Reset the data pointer
  tmp_buffer_ptr = tmp_buffer;
  dd_mutex_release(&g_log_mutex);
}
