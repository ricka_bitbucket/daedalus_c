#import <Foundation/Foundation.h>

struct file_download_settings {
  char url[512];
  char file_path[512];
};

void http_background_download_(struct file_download_settings* fd) {
  NSURL* url = [NSURL URLWithString:[NSString stringWithUTF8String:fd->url]];

  NSURLSessionConfiguration* defaultConfigObject =
      [NSURLSessionConfiguration defaultSessionConfiguration];
  NSURLSession* defaultSession = [NSURLSession sessionWithConfiguration:defaultConfigObject];

  NSURLSessionDownloadTask* downloadTask = [defaultSession
      downloadTaskWithURL:url
        completionHandler:^(NSURL* location, NSURLResponse* response, NSError* error) {
          if (error == nil) {
            NSURL* final_location =
                [NSURL fileURLWithPath:[NSString stringWithUTF8String:fd->file_path]];
            NSLog(@"Temporary file =%@", location);

            NSError* err               = nil;
            NSFileManager* fileManager = [NSFileManager defaultManager];
            NSString* docsDir          = [NSSearchPathForDirectoriesInDomains(
                NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

            if ([fileManager moveItemAtURL:location toURL:final_location error:&err]) {
              NSLog(@"File is saved to =%@", docsDir);
            } else {
              NSLog(@"failed to move: %@", [err userInfo]);
            }
          }
        }];
  [downloadTask resume];
}
