#include "memory.h"

#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "dd_math.h"
#include "memory.inl"
#include "platform.h"
#include "thread.h"
#include "types.h"

#if defined(_WIN32)
  #include <Windows.h>
#else
#endif

#include "dlmalloc.h"

// Temporary default allocator
// ====================================================================================
static void* memory_malloc(dd_allocator* allocator, uint32 byte_count, uint32 align,
                           const struct allocation_location* alloc_location) {
#if defined(_WIN32)
  return _aligned_malloc(byte_count, align);  // malloc ok
#else
  if (align > 1) {
    return aligned_alloc(align, byte_count);  // malloc ok
  } else {
    return malloc(byte_count);  // malloc ok
  }
#endif
}

static uint32 memory_free(dd_allocator* allocator, void* p,
                          const struct allocation_location* alloc_location) {
#if defined(_WIN32)
  _aligned_free(p);  // free ok
#else
  free(p);                                        // free ok
#endif
  return 0;
}

dd_allocator_impl tmp_malloc_allocator_impl = {
    .name = "temporary std alloc", .alloc = &memory_malloc, .free = &memory_free};
dd_allocator* tmp_malloc_allocator = (dd_allocator*)&tmp_malloc_allocator_impl;

// Helper functions
// ====================================================================================
void* dd_alloc_impl(dd_allocator* allocator, uint32 count__bytes, uint32 align,
                    const struct allocation_location* alloc_location) {
  assert(align != 0);

  dd_allocator_impl* alloc_impl = (dd_allocator_impl*)allocator;
  void* out = alloc_impl->alloc(allocator, count__bytes, align, alloc_location);

  return out;
}

uint32 dd_free_impl(dd_allocator* allocator, void* p,
                    const struct allocation_location* alloc_location) {
  assert(p);
  return ((dd_allocator_impl*)allocator)->free(allocator, p, alloc_location);
}

void dd_report_allocations(dd_allocator* allocator, dd_allocator_report_callback callback) {
  dd_allocator_impl* allocator_impl = (dd_allocator_impl*)allocator;

  dd_allocation_header* allocation = allocator_impl->first_header.p_next;
  while (allocation != &allocator_impl->first_header) {
    callback(allocation, 0);
    allocation = allocation->p_next;
  }

  // Don't need to forward to the individual allocators at the moment, since they all have the same
  // info
  //((dd_allocator_impl*)allocator)->report_allocations(allocator, callback);
}

// Page Allocator
// ====================================================================================
typedef struct dd_page_allocator_impl {
  dd_allocator_impl allocator;
  dd_mutex_t mutex;
} dd_page_allocator_impl;

void* dd_page_allocator_alloc(dd_allocator* allocator, uint32 byte_count, uint32 alignment__bytes,
                              const struct allocation_location* alloc_location) {
  dd_page_allocator_impl* page_allocator = (dd_page_allocator_impl*)allocator;

  dd_mutex_lock(&page_allocator->mutex);

  // TODO: implement alignment

  void* p_alloc = NULL;

  const uint32 space_for_size_info = align_up(sizeof(dd_allocation_header), alignment__bytes);

  const uint32 total_allocation_byte_count = byte_count + space_for_size_info;
#if defined(_WIN32)
  p_alloc =
      VirtualAlloc(NULL, total_allocation_byte_count, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
  if (p_alloc == NULL) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to lock mutex: %s\n", err);
  }
#else
  p_alloc = malloc(total_allocation_byte_count);  // malloc ok
#endif

  void* p = (void*)(((intptr_t)p_alloc) + space_for_size_info);

  // Store the size right before the actual memory
  dd_allocation_header* header = ((dd_allocation_header*)p) - 1;
  dd_allocation_header headerd = {.p_original = p_alloc,
                                  .p_next = page_allocator->allocator.first_header.p_prev->p_next,
                                  .p_prev = page_allocator->allocator.first_header.p_prev,
                                  .count__bytes = byte_count,
                                  .label        = alloc_location->file_name,
                                  .line_number  = alloc_location->line_number};
  *header                      = headerd;

  page_allocator->allocator.first_header.p_prev->p_next = header;
  page_allocator->allocator.first_header.p_prev         = header;

  // Make sure the memory is aligned to the request
  assert(((size_t)p) % alignment__bytes == 0);

  dd_mutex_release(&page_allocator->mutex);

  return p;
}

uint32 dd_page_allocator_free(dd_allocator* allocator, void* p,
                              const struct allocation_location* alloc_location) {
  dd_page_allocator_impl* page_allocator = (dd_page_allocator_impl*)allocator;
  dd_mutex_lock(&page_allocator->mutex);

  dd_allocation_header* header = ((dd_allocation_header*)p) - 1;

  uint32 out_byte_count = header->count__bytes;

  // Fix up the linked list
  header->p_prev->p_next = header->p_next;
  header->p_next->p_prev = header->p_prev;

#if defined(_WIN32)
  BOOL result = VirtualFree(header->p_original, 0, MEM_DECOMMIT);
  (void)result;
  assert(result != 0);
#else
  free(header->p_original);                       // free ok
#endif

  dd_mutex_release(&page_allocator->mutex);

  return out_byte_count;
}

void dd_page_allocator_report_allocations(dd_allocator* allocator,
                                          dd_allocator_report_callback callback) {
  dd_page_allocator_impl* page_allocator = (dd_page_allocator_impl*)allocator;

  const dd_allocation_header* allocation = page_allocator->allocator.first_header.p_next;
  while (allocation != &page_allocator->allocator.first_header) {
    callback(allocation, 0);
    allocation = allocation->p_next;
  }
}

void dd_page_allocator_init(dd_allocator* allocator, const char* allocator_debug_name) {
  dd_page_allocator_impl* out     = (dd_page_allocator_impl*)allocator;
  dd_page_allocator_impl out_impl = {{.name               = allocator_debug_name,
                                      .alloc              = dd_page_allocator_alloc,
                                      .free               = dd_page_allocator_free,
                                      .report_allocations = dd_page_allocator_report_allocations},
                                     .mutex = dd_mutex_create()};
  memcpy(out, &out_impl, sizeof(dd_page_allocator_impl));

  dd_allocation_header* fh = &out->allocator.first_header;
  fh->p_next = fh->p_prev = fh;
}

// Heap Allocator
// ====================================================================================
typedef struct dd_heap_allocator_impl {
  dd_allocator_impl allocator;
  void* mspace_;
} dd_heap_allocator_impl;

static_assert(sizeof(dd_allocator) >= sizeof(dd_heap_allocator_impl), "Not enough size");

void* dd_heap_allocator_alloc(dd_allocator* allocator, uint32 count__bytes,
                              uint32 alignment__bytes,
                              const struct allocation_location* alloc_location) {
  dd_heap_allocator_impl* heap_allocator = (dd_heap_allocator_impl*)allocator;

  uint32 space_for_size_info = (alignment__bytes > sizeof(dd_allocation_header))
                                   ? alignment__bytes
                                   : sizeof(dd_allocation_header);
  void* p_alloc = mspace_malloc(heap_allocator->mspace_, count__bytes + space_for_size_info);
  assert(p_alloc != NULL);
  if (p_alloc == NULL) {
    return p_alloc;
  }

  void* p = (void*)(((intptr_t)p_alloc) + space_for_size_info);

  // Store the size right before the actual memory
  dd_allocation_header* header = ((dd_allocation_header*)p) - 1;
  dd_allocation_header headerd = {.p_original = p_alloc,
                                  .p_next = heap_allocator->allocator.first_header.p_prev->p_next,
                                  .p_prev = heap_allocator->allocator.first_header.p_prev,
                                  .count__bytes = count__bytes,
                                  .label        = alloc_location->file_name,
                                  .line_number  = alloc_location->line_number};
  *header                      = headerd;

  heap_allocator->allocator.first_header.p_prev->p_next = header;
  heap_allocator->allocator.first_header.p_prev         = header;

  // Make sure the memory is aligned to the request
  assert(((intptr_t)p) % alignment__bytes == 0);
  return p;
}

uint32 dd_heap_allocator_free(dd_allocator* allocator, void* p,
                              const struct allocation_location* alloc_location) {
  dd_heap_allocator_impl* heap_allocator = (dd_heap_allocator_impl*)allocator;
  dd_allocation_header* header           = ((dd_allocation_header*)p) - 1;

  uint32 out_byte_count = header->count__bytes;

  // Fix up the linked list
  header->p_prev->p_next = header->p_next;
  header->p_next->p_prev = header->p_prev;

  mspace_free(heap_allocator->mspace_, header->p_original);

  return out_byte_count;
}

void dd_heap_allocator_report_allocations(dd_allocator* allocator,
                                          dd_allocator_report_callback callback) {
  dd_heap_allocator_impl* heap_allocator = (dd_heap_allocator_impl*)allocator;

  const dd_allocation_header* allocation = heap_allocator->allocator.first_header.p_next;
  while (allocation != &heap_allocator->allocator.first_header) {
    callback(allocation, 0);
    allocation = allocation->p_next;
  }
}

void dd_heap_allocator_init(void* buffer, uint32 bytes_count, const char* allocator_debug_name) {
  dd_heap_allocator_impl* out = (dd_heap_allocator_impl*)buffer;

  const size_t bytes_left_for_data = bytes_count - sizeof(dd_heap_allocator_impl);
  void* mspace =
      create_mspace_with_base(((dd_heap_allocator_impl*)buffer) + 1, bytes_left_for_data, 1);
  if (mspace == NULL) {
    dd_devprintf("Couldn't create dlmalloc space. Maybe increase buffer size?\n");
    exit(0);
  }

  dd_heap_allocator_impl out_impl = {{.alloc              = dd_heap_allocator_alloc,
                                      .free               = dd_heap_allocator_free,
                                      .report_allocations = dd_heap_allocator_report_allocations,
                                      .name               = allocator_debug_name},
                                     .mspace_ = mspace};
  memcpy(buffer, &out_impl, sizeof(out_impl));

  dd_allocation_header* fh = &out->allocator.first_header;
  fh->p_next = fh->p_prev = fh;
}

#if 0
RingAllocator::RingAllocator(dd_allocator& backing, const char* name,
                             size_t size_bytes)
    : dd_allocator(name), backing_(backing) {
  begin_ = (uintptr_t)backing_.allocate(size_bytes, 1);
  end_   = begin_ + size_bytes;

  current_ = begin_;

  beginOfNotUsed_ = begin_;
  endOfNotUsed_   = begin_ + size_bytes;
  ;
}

RingAllocator::~RingAllocator(void) {
  backing_.deallocate((void*)begin_);
}

void* RingAllocator::allocate(size_t size, size_t align) {
  Header* h   = reinterpret_cast<Header*>(current_);
  uintptr_t p = current_ + sizeof(Header);
  current_    = current_ + size + sizeof(Header);
  if (current_ > end_) {
    assert(false);
    current_ = begin_;
    h        = reinterpret_cast<Header*>(current_);
    p        = current_ + sizeof(Header);
    current_ = current_ + size + sizeof(Header);
  }

  h->isInUse = true;
  h->size    = (uint32_t)size;

  beginOfNotUsed_ = current_;

  assert(((uintptr_t)p) % align ==
         0);  // Make sure the memory is aligned to the request
  return (void*)p;
}

void RingAllocator::deallocate(void* /*p*/) {
  /*Header *h = reinterpret_cast<Header*>((uintptr_t)p - sizeof(Header));
  h->isInUse = false;

  h = reinterpret_cast<Header*>(endOfNotUsed_);
  while( !h->isInUse && endOfNotUsed_<end_ ) {
          endOfNotUsed_ += h->size + sizeof(Header);
          h = reinterpret_cast<Header*>(endOfNotUsed_);
  }
  if( endOfNotUsed_ >= end_ )
          endOfNotUsed_ = begin_;*/
}
#endif

void dd_memory_startup(uint32 sizeOfScratchHeap_bytes) {
  (void)sizeOfScratchHeap_bytes;
#if 0
  memoryGlobals_.page =
      new (memoryGlobals_.globalsMemoryBuffer) PageAllocator("global_page");
  memoryGlobals_.heap =
      new (memoryGlobals_.globalsMemoryBuffer + sizeof(PageAllocator))
          HeapAllocator(*memoryGlobals_.page, "global_heap");
  memoryGlobals_.scratch = new (memoryGlobals_.globalsMemoryBuffer +
                                sizeof(PageAllocator) + sizeof(HeapAllocator))
      RingAllocator(*memoryGlobals_.page, "global_scratch",
                    sizeOfScratchHeap_bytes);

  memoryGlobals_.debugPage =
      new (memoryGlobals_.globalsMemoryBuffer + sizeof(PageAllocator) +
           sizeof(HeapAllocator) + sizeof(RingAllocator))
          PageAllocator("global_debug_page");
  memoryGlobals_.debugHeap = new (
      memoryGlobals_.globalsMemoryBuffer + sizeof(PageAllocator) +
      sizeof(HeapAllocator) + sizeof(RingAllocator) + sizeof(PageAllocator))
      HeapAllocator(*memoryGlobals_.page, "global_debug_heap");
#endif
}
void dd_memory_shutdown() {
#if 0
  memoryGlobals_.debugHeap->~HeapAllocator();
  memoryGlobals_.debugPage->~PageAllocator();
  memoryGlobals_.scratch->~RingAllocator();
  memoryGlobals_.heap->~HeapAllocator();
  memoryGlobals_.page->~PageAllocator();
  memoryGlobals_ = MemoryGlobals();
#endif
}

#if 0
dd_allocator dd_create_default_allocator() {
  dd_allocator out = {.alloc = &dd_page_allocator_alloc,
                      .free  = &dd_page_allocator_free,
                      .name  = "default_allocator"};
  return out;
}
#endif

#if 0
dd_allocator& default_allocator() {
  return *memoryGlobals_.heap;
}
dd_allocator& default_scratch_allocator() {
  return *memoryGlobals_.scratch;
}
dd_allocator& default_debug_allocator() {
  return *memoryGlobals_.debugHeap;
}
#endif

// Stack Allocator
// ====================================================================================
struct memory_stack_allocator_impl {
  dd_allocator_impl allocator;

  uint8* ptr;
  uint8* range_end;
};

void* memory_stack_alloc(dd_allocator* allocator, uint32 byte_count, uint32 align,
                         const struct allocation_location* alloc_location) {
  struct memory_stack_allocator_impl* alloc_impl = (struct memory_stack_allocator_impl*)allocator;

  assert((alloc_impl->ptr + byte_count) < alloc_impl->range_end &&
         "Not enough space in stack allocator");

  uint8* out = alloc_impl->ptr;
  alloc_impl->ptr += byte_count;
  return out;
}

void* memory_stack_realloc(dd_allocator* allocator, void* ptr, uint32 old_count, uint32 new_count,
                           uint32 align, const struct allocation_location* alloc_location) {
  struct memory_stack_allocator_impl* alloc_impl = (struct memory_stack_allocator_impl*)allocator;

  const bool is_last_allocation = (((uint8*)ptr + new_count) == alloc_impl->ptr);
  if (is_last_allocation) {
    assert(((uint8*)ptr + new_count) < alloc_impl->range_end);
    alloc_impl->ptr += (new_count - old_count);
    return ptr;
  } else {
    if (new_count < old_count) {
      // Don't do anything, it's fine. Won't be able to increase the size again, so the difference
      // in memory is effectively lost.
      return ptr;
    } else {
      assert((alloc_impl->ptr + new_count) < alloc_impl->range_end);

      uint8* out = alloc_impl->ptr;
      memcpy(out, ptr, old_count);
      alloc_impl->ptr += new_count;
      return out;
    }
  }
}

uint32 memory_stack_free(dd_allocator* allocator, void* p,
                         const struct allocation_location* alloc_location) {
  // When using stack allocator for a small array, it may grow, thus freeing data.
  // That would hit this assert.
  // assert(false && "Don't free memory from a stack allocator");
  return 0;
}

dd_allocator* dd_memory_stack_allocator_init(uint8* buffer, uint32 byte_count) {
  struct memory_stack_allocator_impl* alloc_impl = (struct memory_stack_allocator_impl*)buffer;
  memset(alloc_impl, 0, sizeof(*alloc_impl));
  alloc_impl->allocator.name  = "stack allocator";
  alloc_impl->allocator.alloc = &memory_stack_alloc;
  alloc_impl->allocator.free  = &memory_stack_free;
  alloc_impl->ptr             = (uint8*)(alloc_impl + 1);
  alloc_impl->range_end       = buffer + byte_count;
  assert(alloc_impl->ptr < alloc_impl->range_end);
  return (dd_allocator*)buffer;
}

// Stack allocator
// ====================================================================================
