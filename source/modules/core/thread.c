#include "thread.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "dd_assert.h"
#include "utils.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>
#else
  #include <pthread.h>
typedef void* HANDLE;
#endif

struct ThreadParams {
  HANDLE handle;
  void (*func)(void* arg);
  void* arg;
  const char* debug_name;
};

#if defined(_WIN32)

struct dd_thread_win32 {
  HANDLE handle;
  struct ThreadParams* data;
};
static_assert(sizeof(struct dd_thread_win32) <= sizeof(dd_thread_t),
              "Can't put Win32 thread into opaque dd_thread_t");

uint64 dd_thread_id() { return (uint64)GetCurrentThreadId(); }

DWORD WINAPI MyThreadFunction(LPVOID lpParam) {
  struct ThreadParams* tp = (struct ThreadParams*)lpParam;
  dd_thread_entry_f f     = tp->func;
  void* arg               = tp->arg;

  wchar_t desc[256];
  int convert_result =
      MultiByteToWideChar(CP_UTF8, 0, tp->debug_name, (int)strlen(tp->debug_name), desc, 256);
  if (convert_result > 0) {
    desc[convert_result] = 0;
    SetThreadDescription(tp->handle, desc);
  }

  (*f)(arg);
  free(tp);  // free ok
  return 0;
}

dd_thread_t thread_start(dd_thread_entry_f entry_func, void* user_data, const char* debug_name) {
  struct ThreadParams* tp =
      (struct ThreadParams*)malloc(sizeof(struct ThreadParams));  // malloc ok

  tp->func                   = entry_func;
  tp->arg                    = user_data;
  tp->debug_name             = debug_name;
  struct dd_thread_win32 out = {.data = tp};

  HANDLE h   = CreateThread(NULL, 0, MyThreadFunction, tp, 0, NULL);
  tp->handle = h;
  if (h == NULL) {
    // error
  }
  out.handle = h;

  return *(dd_thread_t*)&out;
}

#else

struct dd_thread_posix {
  pthread_t thread;
  struct ThreadParams* data;
};
static_assert(sizeof(struct dd_thread_posix) <= sizeof(dd_thread_t),
              "Can't put Posix thread into opaque dd_thread_t");

uint64 dd_thread_id() {
  uint64 id;
  pthread_threadid_np(NULL, &id);
  return id;
}

void* MyThreadFunction(void* lpParam) {
  struct ThreadParams* tp = (struct ThreadParams*)lpParam;
  dd_thread_entry_f f     = tp->func;
  void* arg               = tp->arg;

  (*f)(arg);
  free(tp);  // free ok
  return 0;
}

dd_thread_t thread_start(dd_thread_entry_f entry_func, void* user_data, const char* debug_name) {
  struct ThreadParams* tp =
      (struct ThreadParams*)malloc(sizeof(struct ThreadParams));  // malloc ok

  tp->func                   = entry_func;
  tp->arg                    = user_data;
  tp->debug_name             = debug_name;
  struct dd_thread_posix out = {.data = tp};

  int h = pthread_create(&out.thread, NULL, MyThreadFunction, tp);
  if (h != 0) {
    assert(false && "Couldn't create thread");
  }

  return *(dd_thread_t*)&out;
}

#endif

const struct dd_thread_api dd_thread_api = {.start = thread_start};
