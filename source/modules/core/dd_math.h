#pragma once

#include "types.h"

#if 0  // defined(_WIN32)
// Including <math.h> caused vkCreateInstance to crash, not sure why.
// Manually declaring sqrtf works on windows.
//_Check_return_ _ACRTIMP float __cdecl sqrtf(float);
  #if defined _M_X64 || defined _M_ARM || defined _M_ARM64 || defined _M_HYBRID_X86_ARM64 || \
      defined _CORECRT_BUILD_APISET

_Check_return_ _ACRTIMP float __cdecl sqrtf(_In_ float _X);

  #else
_Check_return_ __inline float __CRTDECL sqrtf(_In_ float _X);
  #endif
#else
  #include <math.h>
#endif

#if defined(__APPLE__)
  #pragma GCC diagnostic ignored "-Wgnu-statement-expression"
#endif

// min and max purposefully use the type of a only, not the type of b.
// Users should ensure types are identical.
#ifndef max
  #if defined(_WIN32)
    #define max(a, b) (((a) > (b)) ? (a) : (b))
  #else
    #define max(a, b)                  \
      (__typeof__(a))({                \
        __typeof__(a) _amax = (a);     \
        __typeof__(a) _bmax = (b);     \
        _amax > _bmax ? _amax : _bmax; \
      })
  #endif
#endif

#ifndef min
  #if defined(_WIN32)
    #define min(a, b) (((a) < (b)) ? (a) : (b))
  #else
    #define min(a, b)           \
      (__typeof__(a))({         \
        __typeof__(a) _a = (a); \
        __typeof__(a) _b = (b); \
        _a < _b ? _a : _b;      \
      })
  #endif
#endif

static inline uint32 align_down_uint32(uint32 value, uint32 alignment) {
  return (value / alignment) * alignment;
}
static inline uint32 align_up_uint32(uint32 value, uint32 alignment) {
  return ((value - 1) / alignment) * alignment + alignment;
}

static inline int32 align_down_int32(int32 value, int32 alignment) {
  if (value >= 0) {
    return (value / alignment) * alignment;
  } else {
    return ((value + 1) / alignment) * alignment - alignment;
  }
}
static inline int32 align_up_int32(int32 value, int32 alignment) {
  if (value >= 0) {
    return ((value - 1) / alignment) * alignment + alignment;
  } else {
    return (value / alignment) * alignment;
  }
}

#define align_down(v, a)               \
  _Generic((v), int32                  \
           : align_down_int32, uint32  \
           : align_down_uint32, size_t \
           : align_down_uint32)(v, a)
#define align_up(v, a) \
  _Generic((v), int32 : align_up_int32, uint32 : align_up_uint32, size_t : align_up_uint32)(v, a)

/* Bit operations */
/* a=target variable, b=bit number to act upon 0-n */
#define BIT_SET(a, b) ((a) |= (1ULL << (b)))
#define BIT_CLEAR(a, b) ((a) &= ~(1ULL << (b)))
#define BIT_FLIP(a, b) ((a) ^= (1ULL << (b)))
#define BIT_CHECK(a, b) (!!((a) & (1ULL << (b))))  // '!!' to make sure this returns 0 or 1

static inline float float2_dot(float2 a, float2 b);
static inline float2 float2_normalize(float2 a);
static inline float2 float2_add(float2 a, float2 b);
static inline float2 float2_mul(float2 a, float2 b);
static inline float2 float2_madd(float2 a, float2 x, float2 b);

static inline float float3_dot(float3 a, float3 b);
static inline float3 float3_cross(float3 a, float3 b);
static inline float3 float3_normalize(float3 a);
static inline float3 float3_add(float3 a, float3 b);
static inline float3 float3_mul(float3 a, float3 b);
static inline float3 float3_madd(float3 a, float3 x, float3 b);

static inline float float4_dot(float4 a, float4 b);
static inline float4 float4_normalize(float4 a);
static inline float4 float4_add(float4 a, float4 b);
static inline float4 float4_mul(float4 a, float4 b);
static inline float4 float4_madd(float4 a, float4 x, float4 b);

// ----------------------------------------------------------------------------
// float2
// ----------------------------------------------------------------------------
static inline float float2_dot(float2 a, float2 b) { return a.x * b.x + a.y * b.y; }
static inline float2 float2_normalize(float2 a) {
  float len = sqrtf(float2_dot(a, a));
  return (float2){a.x / len, a.y / len};
}
static inline float2 float2_mul(float2 a, float2 b) { return (float2){a.x * b.x, a.y * b.y}; }
static inline float2 float2_add(float2 a, float2 b) { return (float2){a.x + b.x, a.y + b.y}; }
static inline float2 float2_madd(float2 a, float2 x, float2 b) {
  return (float2){a.x * x.x + b.x, a.y * x.y + b.y};
}

// ----------------------------------------------------------------------------
// float3
// ----------------------------------------------------------------------------
static inline float float3_dot(float3 a, float3 b) { return a.x * b.x + a.y * b.y + a.z * b.z; }
static inline float3 float3_cross(float3 a, float3 b) {
  return (float3){a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x};
}
static inline float3 float3_normalize(float3 a) {
  float len = sqrtf(float3_dot(a, a));
  return (float3){a.x / len, a.y / len, a.z / len};
}
static inline float3 float3_mul(float3 a, float3 b) {
  return (float3){a.x * b.x, a.y * b.y, a.z * b.z};
}
static inline float3 float3_add(float3 a, float3 b) {
  return (float3){a.x + b.x, a.y + b.y, a.z + b.z};
}
static inline float3 float3_madd(float3 a, float3 x, float3 b) {
  return (float3){a.x * x.x + b.x, a.y * x.y + b.y, a.z * x.z + b.z};
}

// ----------------------------------------------------------------------------
// float4
// ----------------------------------------------------------------------------
static inline float float4_dot(float4 a, float4 b) {
  return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}
static inline float4 float4_normalize(float4 a) {
  float len = sqrtf(float4_dot(a, a));
  return (float4){a.x / len, a.y / len, a.z / len, a.w / len};
}
static inline float4 float4_mul(float4 a, float4 b) {
  return (float4){a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w};
}
static inline float4 float4_add(float4 a, float4 b) {
  return (float4){a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w};
}
static inline float4 float4_madd(float4 a, float4 x, float4 b) {
  return (float4){a.x * x.x + b.x, a.y * x.y + b.y, a.z * x.z + b.z, a.w * x.w + b.w};
}
