#pragma once

#include "types.h"

// Default alignment of allocations
// const uint32 DEFAULT_ALIGNMENT = 4;
struct dd_allocation_header;

typedef void (*dd_allocator_report_callback)(const struct dd_allocation_header* alloc_data,
                                             uint32 byte_count);

// Store information about where the allocation happened
struct allocation_location {
  const char* file_name;
  const char* function_name;
  uint32 line_number;
};

typedef struct dd_allocator {
  uint8 opaque_[128];
} dd_allocator;

void dd_report_allocations(dd_allocator* allocator, dd_allocator_report_callback callback);

typedef void (*dd_allocator_report_changes_callback)(const uint8* buffer, uint32 byte_count);
void dd_memory_report_changes(dd_allocator_report_changes_callback cb);

// Helper functions
// ====================================================================================
void* dd_alloc_impl(dd_allocator* allocator, uint32 count__bytes, uint32 align,
                    const struct allocation_location* alloc_location);
uint32 dd_free_impl(dd_allocator* allocator, void* p,
                    const struct allocation_location* alloc_location);

#define dd_alloc(allocator, byte_count, align) \
  dd_alloc_impl(allocator, byte_count, align,  \
                &(struct allocation_location){__FILE__, __FUNCTION__, __LINE__})
#define dd_free(allocator, p) \
  dd_free_impl(allocator, p, &(struct allocation_location){__FILE__, __FUNCTION__, __LINE__})

#if 0
class RingAllocator : public dd_allocator {
 public:
  RingAllocator(dd_allocator& backing, const char* name, size_t size_bytes);
  virtual ~RingAllocator(void);

  virtual void* allocate(size_t size, size_t align = 1);
  virtual void deallocate(void* p);

 private:
  dd_allocator& backing_;

  // Start and end of ring buffer
  uintptr_t begin_, end_;
  uintptr_t beginOfNotUsed_, endOfNotUsed_;

  uintptr_t current_;
};
#endif

extern dd_allocator* tmp_malloc_allocator;

// Initializes the global memory allocators. scratch_buffer_size is the size of the memory buffer
// used by the scratch allocators.
void dd_memory_startup(uint32 sizeOfScratchHeap_bytes);

// Shuts down the global memory allocators created by startup().
void dd_memory_shutdown();

// Returns a default memory allocator that can be used for most allocations.
//
// You need to call dd_memory_startup() for this allocator to be available.
dd_allocator dd_create_default_allocator();

void dd_heap_allocator_init(void* data, uint32 bytes_count, const char* allocator_debug_name);

void dd_page_allocator_init(dd_allocator* page_allocator, const char* allocator_debug_name);

// Returns a "scratch" allocator that can be used for temporary short-lived memory allocations. The
// scratch allocator uses a ring buffer of size scratch_buffer_size to service the allocations.
//
// If there is not enough memory in the buffer to match requests for scratch memory, memory from
// the default_allocator will be returned instead.
dd_allocator* default_scratch_allocator();

dd_allocator* default_debug_allocator();

// A simple linear stack allocator for small amounts of data, no need to free individual
// allocations
dd_allocator* dd_memory_stack_allocator_init(uint8* buffer, uint32 byte_count);

// Create a stack allocator of the indicated size.
// This does not need to be freed as that automatically happens at cleanup.
#define DD_STACK_ALLOCATOR(var_name, byte_count)            \
  uint8 macro_concat(var_name, __LINE__)[byte_count] = {0}; \
  dd_allocator* var_name =                                  \
      dd_memory_stack_allocator_init(macro_concat(var_name, __LINE__), byte_count)
