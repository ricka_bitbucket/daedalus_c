#include "dll.h"

#if defined(_WIN32)
#include <Windows.h>
#endif
#include "types.h"
#include "utils.h"

#if defined(_WIN32)

static dd_dll_handle_t dll_load(const char* full_dll_path) {
  return (dd_dll_handle_t)LoadLibrary(full_dll_path);
}

static void dll_unload(dd_dll_handle_t dll_handle) {
  if (dll_handle) {
    if (FreeLibrary(dll_handle) == 0) {
      char err[256];
      memset(err, 0, 256);
      FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                    MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
      dd_devprintf("Unable to unload DLL: %s\n", err);
    }
  }
}

static void* dll_get_symbol(dd_dll_handle_t dll_handle, const char* symbol_name) {
  if (dll_handle) {
    return (void*)GetProcAddress(dll_handle, symbol_name);
  } else {
    return NULL;
  }
}

#else

#include <dlfcn.h>

static dd_dll_handle_t dll_load(const char* full_dll_path) {
  void* handle = dlopen(full_dll_path, RTLD_LAZY);
  if (!handle) {
    dd_devprintf("Unable to Load dynamic library '%s'\n", full_dll_path);
  }

  return handle;
}

static void dll_unload(dd_dll_handle_t dll_handle) {
  if (dll_handle) {
    dlclose(dll_handle);
  }
}

static void* dll_get_symbol(dd_dll_handle_t dll_handle, const char* symbol_name) {
  if (dll_handle) {
    return dlsym(dll_handle, symbol_name);
  } else {
    return NULL;
  }
}

#endif

const struct dd_dll_api dd_dll_api = {
    .load = &dll_load, .unload = &dll_unload, .get_symbol = &dll_get_symbol};
