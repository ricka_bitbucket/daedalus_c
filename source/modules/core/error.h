#pragma once

extern void dd_devprintf(const char* format, ...);

#if 0
#if PLATFORM_WINDOWS
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#define DASSERT(a, text)                                           \
  if (!(a)) {                                                      \
    if (IsDebuggerPresent()) {                                     \
      DebugBreak();                                                \
    } else {                                                       \
      MessageBox(NULL, text, "ERROR", MB_OK | MB_ICONEXCLAMATION); \
    }                                                              \
  }
#else
#include <cassert>
#define DASSERT(a, text)                     \
  {                                          \
    if (!(a)) {                              \
      daedalus::LogContext::printContexts(); \
      printf("%s\n", text);                  \
    }                                        \
    assert(a);                               \
  }
#endif
#endif
