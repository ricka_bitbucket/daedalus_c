#pragma once

#ifdef __cplusplus
extern "C" {
#endif
#include "types.h"

enum ELogRemoteMessageType {
  MSG_LOG_STRINGS = 0,
  MSG_LOG_PROCESS_DATA,
  MSG_LOG_PROCESS_STRINGS,

  // Memory messages
  MSG_MEMORY,

  // Profiler messages
  MSG_PROFILER,
  MSG_PROFILER_RECORD
};

typedef struct dd_log_remote_message_header_t {
  uint32 message_type;
  uint32 payload_num_bytes;

} dd_log_remote_message_header_t;

#ifdef __cplusplus
}
#endif
