#pragma once

#include <string.h>

#include "containers.h"
#include "memory.h"
#include "thread.h"
#include "timer.h"
#include "types.h"
#include "utils.h"

uint8* memory_central_block_allocate(uint32 byte_count);
void memory_central_block_discard(uint8* data, uint32 byte_count);
void memory_central_block_retire(uint64 thread_id, void* data, uint32 data_byte_count,
                                 uint32 byte_count);

typedef struct dd_allocation_header dd_allocation_header;

typedef struct dd_allocation_header {
  dd_allocation_header* p_next;
  dd_allocation_header* p_prev;
  void* p_original;
  uint32 count__bytes;
  const char* label;  // Can be read at any time, so should be pointed at static text
  size_t line_number;
} dd_allocation_header;

// Base allocator implementation
// ====================================================================================
typedef struct dd_allocator_impl {
  // Must point to a literal as can be read at any time for debugging purposes
  const char* name;

  void* (*alloc)(dd_allocator* allocator, uint32 count__bytes, uint32 align,
                 const struct allocation_location* alloc_location);
  void* (*realloc)(dd_allocator* allocator, void* ptr, uint32 old_count, uint32 new_count,
                   uint32 align, const struct allocation_location* alloc_location);
  uint32 (*free)(dd_allocator* allocator, void* p,
                 const struct allocation_location* alloc_location);
  void (*report_allocations)(dd_allocator* allocator, dd_allocator_report_callback callback);

  // All allocators will need to track allocations
  dd_allocation_header first_header;

  // Concrete types of allocator can store data after these functions
} dd_allocator_impl;
