Memory profiling should be possible in two ways:

1.  An ongoing highwater mark (possibly per domain or subsystem)
2.  A detailed analysis of all memory used.

Preferably option 2 could be done after the fact. This means that all allocations and
de-allocations need to be logged, so such an analysis can be built for each frame. Since actual
allocations from higher level allocators are expected to be rare, the amount of data for this
should be manageable. The question is how does the level of usage within an allocator change?

Reporting once a frame would be ok, but how does that work on threads that may not have a clean
delineated frame period?
--> require the user to explicitly flush memory reporting once per frame on each thread. The
user can decide an appropriate location. Each thread can then store it's own memory profiling
data locally, and only during flush sync that centrally and send to daedalus_hub.

Should allocations be reported per allocator, or per thread?

- per allocator makes more logical sense and groups allocations per subsystem probably (as each subsystem would likely have it's own sub-allocator)
- per thread shows how allocations are made over time, and would integrate better with code time profiling
- probably both could be done. since profiling data would be buffered per thread, and allocations/frees are assumed to be few, can included thread_id and allocator_id for each alloc/free

Sending only changes should be less data than sending full allocation list each time (effectively each frame). So will store changes per thread.

For each allocation also want to store allocation location (file:line). This also needs to be viewable in the hub when looking at the detailed view.
