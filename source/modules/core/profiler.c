/* Profiler
 *
 * A central location stores blocks of data that represent a single root-level recording.
 * Different threads request a block of data from this central location, write data into that, then
 * register it back when complete. If data doesn't fit a new larger block is requested, data is
 * copied, and the older allocation is discarded. For the next iteration, the thread requests the
 * size needed previously plus some extra for headroom. This ensures that a single profile trace
 * from a single thread is always a single block in memory.
 *
 * The central location stores the data blocks until a fixed size is reached, then throws them
 * away. If the hub requests the data then all stored blocks are sent, and then discarded.
 */

#include "profiler.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../telemetry/telemetry_types.inl"
#include "containers.h"
#include "memory.h"
#include "profiler.inl"
#include "thread.h"
#include "timer.h"
#include "types.h"
#include "utils.h"

enum { PROFILER_BUFFER_SIZE = 2048, MAX_NUM_THREADS = 128 };

struct dd_profiler_thread_data {
  uint8* buffer;
  uint8* buffer_end;
  uint8* buffer_offset;
  uint32 buffer_size;
  uint32 call_depth;
  char name[64];
};

extern struct telemetry_client_t g_telemetry_client;

static dd_prof_process_callback_t dd_prof_process_active = NULL;

static THREAD_LOCAL struct dd_profiler_thread_data profile_thread_data = {0};

static uint8 g_static_profile_buffer[10 * 1024];

// Default store for profiler. This doesn't do much, it's expected you replace the store with a
// different one (for example the telemetry one).
static uint8* profiler_central_block_allocate(uint32 byte_count) { return malloc(byte_count); }
static uint8* profiler_central_block_reallocate(uint8* ptr, uint32 obc, uint32 new_byte_count) {
  return realloc(ptr, new_byte_count);
}
static void profiler_central_block_retire(uint64 t, void* data, uint32 b, uint32 c) { free(data); }

static struct dd_profiler_store g_default_profiler_store = {&profiler_central_block_allocate,
                                                            &profiler_central_block_reallocate,
                                                            &profiler_central_block_retire};
struct dd_profiler_store* profiler_store                 = &g_default_profiler_store;

static struct dd_profiler_thread_data* threaded_buffers[MAX_NUM_THREADS] = {0};
static unsigned num_registered_threads                                   = 0;

static void dd_prof_flush_thread_data();

void dd_prof_thread_init(const char* thread_name) {
  (void)thread_name;
  if (profile_thread_data.buffer_size == 0) {
    profile_thread_data.buffer_size = PROFILER_BUFFER_SIZE;

    // TODO: serialize access to buffer
    threaded_buffers[num_registered_threads++] = &profile_thread_data;

    strncpy(profile_thread_data.name, thread_name, sizeof(profile_thread_data.name));
  }
}

void dd_prof_thread_destroy() {
  memset(&profile_thread_data, 0, sizeof(profile_thread_data));

  // TODO: serialize access to buffer
  for (unsigned i = 0; i < num_registered_threads; i++) {
    if (threaded_buffers[i] == &profile_thread_data) {
      const size_t num_bytes =
          (num_registered_threads - i - 1) * sizeof(struct dd_profiler_thread_data*);
      memmove(threaded_buffers + i, threaded_buffers + i + 1, num_bytes);
      num_registered_threads--;
      threaded_buffers[num_registered_threads] = 0;
      return;
    }
  }
}

void dd_prof_start(const char* label) {
  if (profile_thread_data.buffer_size == 0) {
    //"Call dd_prof_thread_init on thread before profiling");
    dd_prof_thread_init("<unknown>");
  }

  dd_perf_startdata s;
  s.label     = label;
  s.category  = "my_category";
  s.timestamp = dd_time_microseconds();

  // 1 byte for sentinal to indicate this is a start block, then the size of the block
  uint8* new_offset = profile_thread_data.buffer_offset + 1 + sizeof(s);
  if (new_offset > profile_thread_data.buffer_end) {
    uint8* old_block       = profile_thread_data.buffer;
    uint32 old_buffer_size = profile_thread_data.buffer_size;

    uint32 new_buffer_size =
        (profile_thread_data.buffer_end == 0)
            ? profile_thread_data.buffer_size
            : profile_thread_data.buffer_size + profile_thread_data.buffer_size / 2;

    uint32 used_byte_count =
        (uint32)(profile_thread_data.buffer_offset - profile_thread_data.buffer);
    uint8* new_block = NULL;

    bool is_new_alloc = false;
    if (old_block) {
      new_block = profiler_store->block_realloc(old_block, old_buffer_size, new_buffer_size);
    } else {
      new_block    = profiler_store->block_alloc(new_buffer_size);
      is_new_alloc = true;
    }
    profile_thread_data.buffer        = new_block;
    profile_thread_data.buffer_offset = profile_thread_data.buffer + used_byte_count;
    profile_thread_data.buffer_end    = profile_thread_data.buffer + new_buffer_size;
    profile_thread_data.buffer_size   = new_buffer_size;

    if (is_new_alloc) {
      // Insert the thread ID
      *(profile_thread_data.buffer_offset++) = SENTINAL_THREAD_ID;
      dd_perf_threaddata_t td                = {.thread_id = dd_thread_id()};
      memcpy(profile_thread_data.buffer_offset, &td, sizeof(td));
      profile_thread_data.buffer_offset += sizeof(td);

      *(profile_thread_data.buffer_offset++) = SENTINAL_THREAD_NAME;
      dd_perf_threadname_t tn                = {0};
      strncpy(tn.name, profile_thread_data.name, sizeof(tn.name));
      memcpy(profile_thread_data.buffer_offset, &tn, sizeof(tn));
      profile_thread_data.buffer_offset += sizeof(tn);
    }
  }

  *profile_thread_data.buffer_offset = SENTINAL_START;
  ++profile_thread_data.buffer_offset;
  memcpy(profile_thread_data.buffer_offset, &s, sizeof(s));
  profile_thread_data.buffer_offset += sizeof(s);
  profile_thread_data.call_depth++;

  // Attempt to catch ummatched starts/end pairs
  assert(profile_thread_data.call_depth < 256);
  assert((uint32)(profile_thread_data.buffer_offset - profile_thread_data.buffer) <
         profile_thread_data.buffer_size);
}

void dd_prof_end() {
  // TODO: Forgetting one of these causes crashes. Need to find out why
  dd_perf_enddata e;
  e.timestamp = dd_time_microseconds();

  uint8* new_offset = profile_thread_data.buffer_offset + 1 + sizeof(e);
  if (new_offset > profile_thread_data.buffer_end) {
    uint32 new_buffer_size = profile_thread_data.buffer_size + profile_thread_data.buffer_size / 2;
    uint32 used_byte_count =
        (uint32)(profile_thread_data.buffer_offset - profile_thread_data.buffer);
    void* block = profiler_store->block_realloc(profile_thread_data.buffer,
                                                profile_thread_data.buffer_size, new_buffer_size);

    profile_thread_data.buffer        = block;
    profile_thread_data.buffer_offset = profile_thread_data.buffer + used_byte_count;
    profile_thread_data.buffer_end    = profile_thread_data.buffer + new_buffer_size;
    profile_thread_data.buffer_size   = new_buffer_size;
  }

  *profile_thread_data.buffer_offset = SENTINAL_END;
  ++profile_thread_data.buffer_offset;
  memcpy(profile_thread_data.buffer_offset, &e, sizeof(e));
  profile_thread_data.buffer_offset += sizeof(e);

  profile_thread_data.call_depth--;
  if (profile_thread_data.call_depth == 0) {
    uint32 data_byte_count =
        (uint32)(profile_thread_data.buffer_offset - profile_thread_data.buffer);
    profiler_store->block_retire(dd_thread_id(), profile_thread_data.buffer, data_byte_count,
                                 profile_thread_data.buffer_size);
    profile_thread_data.buffer            = profile_thread_data.buffer_end =
        profile_thread_data.buffer_offset = NULL;
  }
}

void dd_prof_set_process_callback(dd_prof_process_callback_t func) {
  dd_prof_process_active = func;
}

void dd_prof_flush_thread_data(struct dd_profiler_thread_data* data) {
  // TODO: serialize access to buffer
  uint32 byte_count = (uint32)(data->buffer_offset - data->buffer);
  if (byte_count > 0) {
    if (dd_prof_process_active) {
      (*dd_prof_process_active)(data->buffer, byte_count);
    }
    data->buffer_offset = data->buffer;
  }
}

void dd_prof_flush() {
  for (unsigned i = 0; i < num_registered_threads; i++) {
    // dd_prof_flush_thread_data(threaded_buffers[i]);
  }
}

uint64 profiler_first_timestamp_in_buffer(const uint8* buffer, uint32 buffer_size) {
  const uint8* buf_end = buffer + buffer_size;
  while (buffer != buf_end) {
    uint8 sentinal = *(buffer++);
    if (sentinal == SENTINAL_START) {
      const dd_perf_startdata* first_start = (const dd_perf_startdata*)buffer;
      return first_start->timestamp;
    } else if (sentinal == SENTINAL_END) {
      buffer += sizeof(dd_perf_enddata);
    } else if (sentinal == SENTINAL_THREAD_ID) {
      buffer += sizeof(dd_perf_threaddata_t);
    } else if (sentinal == SENTINAL_THREAD_NAME) {
      buffer += sizeof(dd_perf_threadname_t);
    } else {
      assert(false);  // Unknown id
    }
  }

  assert(false);
  exit(0);
}

uint64 profiler_last_timestamp_in_buffer(const uint8* buffer, uint32 buffer_size) {
  const uint8* buffer_end = buffer + buffer_size;
  // Get last entry with ending time
  const dd_perf_enddata* last_end = ((const dd_perf_enddata*)buffer_end) - 1;
  return last_end->timestamp;
}
