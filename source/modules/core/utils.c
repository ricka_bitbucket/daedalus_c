#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>
#else
  #include <unistd.h>
#endif
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "containers.h"
#include "dd_assert.h"
#include "memory.h"
#include "types.h"

#if !defined(_MSC_VER)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wformat-nonliteral"
  #pragma clang diagnostic ignored "-Wformat-security"
#endif

void dd_sleep(unsigned count_milliseconds) {
#if defined(_WIN32)
  Sleep(count_milliseconds);
#else
  usleep(1000 * count_milliseconds);
#endif
}

void dd_devprintf(const char* format, ...) {
  char buf[4096];
  char* p = buf;
  va_list args;
  int n;

  va_start(args, format);
  n = vsnprintf(p, sizeof(buf) - 3, format, args);  // buf-3 is room for CR/LF/NUL
  va_end(args);

  p += (n < 0) ? sizeof(buf) - 3 : (size_t)n;

  *p = '\0';
#if defined(_WIN32)
  OutputDebugString(buf);
#else
  printf("%s", buf);
#endif
}

uint8* read_file(const char* file_path, dd_allocator* allocator) {
  FILE* f = fopen(file_path, "rb");
  DD_ASSERT(f, "Couldn't open '%s'", file_path);

  fseek(f, 0, SEEK_END);
  unsigned size = ftell(f);
  fseek(f, 0, SEEK_SET);
  uint8* out = {NULL}; /* array */
  array_resize(out, size, allocator);
  fread(out, 1, size, f);
  fclose(f);

  return out;
}

char* string_trim(char* in) {
  while ((*in == ' ') || (*in == '\t') || (*in == '\n') || (*in == '\r')) {
    in++;
  }

  char* end = in + strlen(in) - 1;
  while ((*end == ' ') || (*end == '\t') || (*end == '\n') || (*end == '\r')) {
    end--;
  }
  end[1] = 0;
  return in;
}
