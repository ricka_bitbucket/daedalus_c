#pragma once

#include "types.h"

enum { SENTINAL_THREAD_ID = 0xAA, SENTINAL_THREAD_NAME, SENTINAL_START, SENTINAL_END };

typedef struct dd_perf_threaddata_t {
  uint64 thread_id;
} dd_perf_threaddata_t;

typedef struct dd_perf_threadname_t {
  char name[64];
} dd_perf_threadname_t;

typedef struct dd_perf_startdata {
  uint64 timestamp;
  const char* label;
  const char* category;
} dd_perf_startdata;

typedef struct dd_perf_enddata {
  uint64 timestamp;
} dd_perf_enddata;

uint64 profiler_first_timestamp_in_buffer(const uint8* buffer, uint32 buffer_size);
uint64 profiler_last_timestamp_in_buffer(const uint8* buffer, uint32 buffer_size);

struct dd_profiler_store {
  uint8* (*block_alloc)(uint32 byte_count);
  uint8* (*block_realloc)(uint8* ptr, uint32 old_byte_count, uint32 new_byte_count);
  void (*block_retire)(uint64 thread_id, void* data, uint32 data_byte_count,
                       uint32 block_byte_count);
};
extern struct dd_profiler_store* profiler_store;
