#include "dd_string.h"

#include <stdlib.h>
#include <string.h>

#include "containers.h"
#include "memory.h"

dd_stringview_t stringview(const char* in_string) {
  dd_stringview_t out = {0, 0};
  if (in_string) {
    out.string = in_string;
    out.length = strlen(in_string);
  }

  return out;
}

array(char)
    dd_strcat(const dd_stringview_t in_a, const dd_stringview_t in_b, dd_allocator* allocator) {
  array(char) out = NULL;
  array_resize(out, in_a.length + in_b.length + 1, allocator);

  memcpy(out, in_a.string, in_a.length);
  memcpy(out + in_a.length, in_b.string, in_b.length);
  out[in_a.length + in_b.length] = 0;

  return out;
}

int strtoint(const dd_stringview_t in_str) {
  char buf[64];
  size_t len = (countof(buf) - 1) < in_str.length ? (countof(buf) - 1) : in_str.length;
  memcpy(buf, in_str.string, len);
  buf[len] = 0;
  return (int)strtol(buf, NULL, 10);
}

double strtodouble(const dd_stringview_t in_str) {
  char buf[64];
  size_t len = (countof(buf) - 1) < in_str.length ? (countof(buf) - 1) : in_str.length;
  memcpy(buf, in_str.string, len);
  buf[len] = 0;
  return strtod(buf, NULL);
}

bool are_strings_equal(const dd_stringview_t lhs, const dd_stringview_t rhs) {
  if (lhs.length != rhs.length) return false;

  return strncmp(lhs.string, rhs.string, lhs.length) == 0;
}

dd_stringview_t stringview_chr(const dd_stringview_t haystack, char needle) {
  const char* search = haystack.string;
  for (int i = 0; i < haystack.length; i++, search++) {
    if (*search == needle) {
      const dd_stringview_t out = {search, haystack.length - i};
      return out;
    }
  }

  return (dd_stringview_t){0, 0};
}

dd_stringview_t stringviewstr(const dd_stringview_t haystack, const dd_stringview_t needle) {
  int latest_position = (int)haystack.length - (int)needle.length;
  const char* search  = haystack.string;
  for (int i = 0; i <= latest_position; i++, search++) {
    if (strncmp(search, needle.string, needle.length) == 0) {
      const dd_stringview_t out = {search, haystack.length - i};
      return out;
    }
  }

  return (dd_stringview_t){0, 0};
}

dd_stringview_t stringview_trim(dd_stringview_t in) {
  const char* start = in.string;
  const char* end   = in.string + in.length - 1;

  while ((*start == ' ') || (*start == '\t') || (*start == '\n') || (*start == '\r')) {
    if (start == end) break;
    start++;
  }

  while ((*end == ' ') || (*end == '\t') || (*end == '\n') || (*end == '\r')) {
    if (start == end) break;
    end--;
  }

  dd_stringview_t out = {start, end - start + 1};
  return out;
}

dd_stringview_t stringview_pop_front(dd_stringview_t* in_out_string, size_t split_index) {
  dd_stringview_t out = (dd_stringview_t){0, 0};

  if (split_index >= in_out_string->length) {
    out            = *in_out_string;
    *in_out_string = (dd_stringview_t){0, 0};
  } else if (split_index > 0) {
    out            = (dd_stringview_t){in_out_string->string, split_index};
    *in_out_string = (dd_stringview_t){in_out_string->string + split_index,
                                       in_out_string->length - split_index};
  }

  return out;
}

array(dd_stringview_t) stringview_split(const dd_stringview_t haystack, const dd_stringview_t splitter,
                                             dd_allocator* allocator) {
  array(dd_stringview_t) out = NULL;

  dd_stringview_t s = haystack;
  dd_stringview_t f;
  do {
    f = stringviewstr(s, splitter);
    if (f.length > 0) {
      const dd_stringview_t part = stringview_pop_front(&s, f.string - s.string);
      // Ignore opening splitter, but not empty parts after that
      if (f.string != haystack.string || part.length > 0) {
        array_push(out, part, allocator);
      }

      // Discard found splitter
      (void)stringview_pop_front(&s, splitter.length);   
    }
  } while (f.length > 0);

  if (s.length > 0) {
    array_push(out, s, allocator);
  }

  return out;
}
