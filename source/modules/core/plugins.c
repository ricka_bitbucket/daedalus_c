#include "plugins.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
  #include <shlwapi.h>
  #pragma comment(lib, "shlwapi.lib")
#else
#endif

#include <assert.h>

#include "api_registry.h"
#include "containers.h"
#include "dll.h"
#include "filesystem.h"
#include "memory.h"
#include "utils.h"

#ifndef MAX_PATH
  #define MAX_PATH 512
#endif

// Type declarations
// ====================
typedef bool (*plugin_link_apis)(const struct dd_api_registry_t* registry,
                                 struct dd_allocator* allocator);

struct dd_plugins_o {
  struct dd_allocator* allocator;

  struct {
    array(char) paths;
    array(size_t) path_offsets;
    array(dd_plugin_t) plugins;
  } libs;
};

// Function definitions
// ====================

#if defined(_WIN32)
static_assert(sizeof(((dd_plugin_t*)0)->modification_time) >= sizeof(FILETIME), "Doesn't fit");

  // RSDS Debug Information for PDB files
  #define CR_RSDS_SIGNATURE 'SDSR'
typedef struct cr_rsds_hdr {
  DWORD signature;
  GUID guid;
  long version;
  char filename[1];
} cr_rsds_hdr;

static char* cr_pdb_find(LPBYTE imageBase, PIMAGE_DEBUG_DIRECTORY debugDir) {
  LPBYTE debugInfo          = imageBase + debugDir->PointerToRawData;
  const DWORD debugInfoSize = debugDir->SizeOfData;
  if (debugInfo == 0 || debugInfoSize == 0) {
    return NULL;
  }

  if (IsBadReadPtr(debugInfo, debugInfoSize)) {
    return NULL;
  }

  if (debugInfoSize < sizeof(DWORD)) {
    return NULL;
  }

  if (debugDir->Type == IMAGE_DEBUG_TYPE_CODEVIEW) {
    DWORD signature = *(DWORD*)debugInfo;
    if (signature == CR_RSDS_SIGNATURE) {
      cr_rsds_hdr* info = (cr_rsds_hdr*)(debugInfo);
      if (IsBadReadPtr(debugInfo, sizeof(cr_rsds_hdr))) {
        return NULL;
      }

      if (IsBadStringPtrA((const char*)info->filename, UINT_MAX)) {
        return NULL;
      }

      return info->filename;
    }
  }

  return NULL;
}

static bool move_dynamic_library(const char* dll_source_path, int revision) {
  char extension[5];

  char dest_path[MAX_PATH];
  strcpy(dest_path, dll_source_path);
  sprintf(extension, ".d%02i", revision);
  PathRenameExtension(dest_path, extension);
  BOOL result = CopyFile(dll_source_path, dest_path, FALSE);
  if (result == 0) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to copy DLL file from '%s' to '%s':\n %s\n", dll_source_path, dest_path,
                 err);
    return false;
  }

  // Also do PDB
  char pdb_source_path[MAX_PATH];

  {
    HANDLE fp      = NULL;
    HANDLE filemap = NULL;
    LPVOID mem     = 0;
    result         = false;
    do {
      fp = CreateFile(dest_path, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, NULL,
                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
      if ((fp == INVALID_HANDLE_VALUE) || (fp == NULL)) {
        char err[256];
        memset(err, 0, 256);
        FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
        dd_devprintf("Unable to create file '%s':\n %s\n", dest_path, err);

        break;
      }

      filemap = CreateFileMapping(fp, NULL, PAGE_READWRITE, 0, 0, NULL);
      if (filemap == NULL) {
        break;
      }

      mem = MapViewOfFile(filemap, FILE_MAP_ALL_ACCESS, 0, 0, 0);
      if (mem == NULL) {
        break;
      }

      PIMAGE_DOS_HEADER dosHeader = (PIMAGE_DOS_HEADER)(mem);
      if (dosHeader == 0) {
        break;
      }
      if (IsBadReadPtr(dosHeader, sizeof(IMAGE_DOS_HEADER))) {
        break;
      }

      if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE) {
        break;
      }

      PIMAGE_NT_HEADERS ntHeaders =
          (PIMAGE_NT_HEADERS)(((intptr_t)dosHeader) + dosHeader->e_lfanew);
      if (ntHeaders == 0) {
        break;
      }

      if (IsBadReadPtr(ntHeaders, sizeof(ntHeaders->Signature))) {
        break;
      }

      if (ntHeaders->Signature != IMAGE_NT_SIGNATURE) {
        break;
      }

      if (IsBadReadPtr(&ntHeaders->FileHeader, sizeof(IMAGE_FILE_HEADER))) {
        break;
      }

      if (IsBadReadPtr(&ntHeaders->OptionalHeader, ntHeaders->FileHeader.SizeOfOptionalHeader)) {
        break;
      }

      if (ntHeaders->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC &&
          ntHeaders->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC) {
        break;
      }

      PIMAGE_SECTION_HEADER sectionHeaders = IMAGE_FIRST_SECTION(ntHeaders);
      if (IsBadReadPtr(sectionHeaders,
                       ntHeaders->FileHeader.NumberOfSections * sizeof(IMAGE_SECTION_HEADER))) {
        break;
      }

      DWORD debugDirRva  = 0;
      DWORD debugDirSize = 0;
      if (ntHeaders->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC) {
        PIMAGE_OPTIONAL_HEADER64 optionalHeader64 =
            (PIMAGE_OPTIONAL_HEADER64)(&ntHeaders->OptionalHeader);
        debugDirRva  = optionalHeader64->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress;
        debugDirSize = optionalHeader64->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size;
      } else {
        PIMAGE_OPTIONAL_HEADER32 optionalHeader32 =
            (PIMAGE_OPTIONAL_HEADER32)(&ntHeaders->OptionalHeader);
        debugDirRva  = optionalHeader32->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress;
        debugDirSize = optionalHeader32->DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].Size;
      }

      if (debugDirRva == 0 && debugDirSize == 0) {
      } else if (debugDirRva == 0 || debugDirSize == 0) {
        break;
      }

      if (debugDirRva == 0 || debugDirSize == 0) {
        break;
      }

      DWORD debugDirOffset                = 0;
      bool found                          = false;
      PIMAGE_SECTION_HEADER sectionHeader = IMAGE_FIRST_SECTION(ntHeaders);
      for (int i = 0; i < ntHeaders->FileHeader.NumberOfSections; i++, sectionHeader++) {
        DWORD sectionSize = sectionHeader->Misc.VirtualSize;
        if ((debugDirRva >= sectionHeader->VirtualAddress) &&
            (debugDirRva < sectionHeader->VirtualAddress + sectionSize)) {
          found = true;
          break;
        }
      }

      if (!found) {
        break;
      }

      const int diff = (int)(sectionHeader->VirtualAddress - sectionHeader->PointerToRawData);
      debugDirOffset = debugDirRva - diff;

      PIMAGE_DEBUG_DIRECTORY debugDir = (PIMAGE_DEBUG_DIRECTORY)(((intptr_t)mem) + debugDirOffset);
      if (debugDir == 0) {
        break;
      }

      if (IsBadReadPtr(debugDir, debugDirSize)) {
        break;
      }

      if (debugDirSize < sizeof(IMAGE_DEBUG_DIRECTORY)) {
        break;
      }

      int numEntries = debugDirSize / sizeof(IMAGE_DEBUG_DIRECTORY);
      if (numEntries == 0) {
        break;
      }

      for (int i = 1; i <= numEntries; i++, debugDir++) {
        char* pdb = cr_pdb_find((LPBYTE)mem, debugDir);
        if (pdb) {
          size_t len = strlen(pdb);
          if (len >= strlen(dest_path)) {
            sprintf(extension, ".p%02i", revision);
            PathRenameExtension(dest_path, extension);

            strncpy(pdb_source_path, pdb, sizeof(pdb_source_path));
            memcpy_s(pdb, len, dest_path, strlen(dest_path));
            pdb[strlen(dest_path)] = 0;
            result                 = true;
          } else {
            dd_devprintf("skipping fix of pdb name due to storage too short\n");
          }
        }
      }

    } while (0);  // While loop as goto construct

    if (mem != NULL) {
      UnmapViewOfFile(mem);
    }

    if (filemap != NULL) {
      CloseHandle(filemap);
    }

    if ((fp != NULL) && (fp != INVALID_HANDLE_VALUE)) {
      CloseHandle(fp);
    }
  }

  result = CopyFile(pdb_source_path, dest_path, FALSE);
  // result = MoveFileEx(pdb_source_path, pdb_dest_path, MOVEFILE_REPLACE_EXISTING);
  if (result == 0) {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to copy PDB file from '%s' to '%s':\n %s\n", pdb_source_path, dest_path,
                 err);
  }

  return true;
}

static bool dynamic_library_load_plugin(dd_plugin_t* plugin, const char* full_dll_path) {
  /*
  char file_path[MAX_PATH];
  if (GetModuleFileName(handle, file_path, sizeof(file_path)) > 0) {
    dd_devprintf("Module path '%s'\n", file_path);
  } else {
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unable to get module file path: %s\n", err);
  }
  */

  dd_devprintf("Loading DLL '%s'\n", full_dll_path);

  // Need to load the DLL
  plugin->old_dll_handle = plugin->new_dll_handle;

  // Only have 2 characters to store the revision in the filename extension, so wrap around after
  // 99
  const uint8 new_revision = (plugin->reload_count + 1) % 100;

  if (!move_dynamic_library(full_dll_path, (int)new_revision)) {
    // Failed
    // Set the count back
    plugin->reload_count = (plugin->reload_count + (100 - 1)) % 100;
    return false;
  }

  char extension[5];
  char target_path[MAX_PATH];
  strcpy(target_path, full_dll_path);
  sprintf(extension, ".d%02i", new_revision);
  PathRenameExtension(target_path, extension);
  HMODULE handle = LoadLibrary(target_path);
  if (handle) {
    plugin->old_dll_handle = plugin->new_dll_handle;
    plugin->old_revision   = plugin->reload_count;
    plugin->new_dll_handle = handle;
    plugin->reload_count   = new_revision;
    dd_devprintf("Loaded dynamic library '%s' v%i\n", full_dll_path, new_revision);
    return true;
  } else {
    return false;
  }
}

#else

  #include <dlfcn.h>
  #include <errno.h>
  #include <string.h>
  #include <sys/stat.h>
  #include <unistd.h>

static bool dynamic_library_load_plugin(dd_plugin_t* plugin, const char* full_dll_path) {
  uint8 new_revision = plugin->reload_count + 1;

  char target_path[512];
  strcpy(target_path, full_dll_path);
  char* extension = strstr(target_path, ".dylib");
  sprintf(extension, ".d%02i", new_revision);
  if (!remove(target_path)) {
    if (errno != ENOENT) {
      dd_devprintf("Couldn't remove '%s', error %s\n", target_path, strerror(errno));
    }
  }
  if (!link(full_dll_path, target_path)) {
    dd_devprintf("Couldn't link '%s' to '%s', error %s\n", full_dll_path, target_path,
                 strerror(errno));
    // Failed
    // return false;
  }

  void* handle = dlopen(target_path, RTLD_LAZY);

  if (handle) {
    plugin->old_dll_handle = plugin->new_dll_handle;
    plugin->old_revision   = plugin->reload_count;
    plugin->new_dll_handle = handle;
    plugin->reload_count   = new_revision;
    if (plugin->new_path[0] == 0) {
      strcpy(plugin->new_path, full_dll_path);
    }
    dd_devprintf("Loaded dynamic library '%s' v%i\n", target_path, plugin->reload_count);
    return true;
  }
  return false;
}
static void dl_unload_plugin(dd_plugin_t* plugin) {}

#endif

static struct dd_plugins_o* plugins_create(struct dd_allocator* allocator) {
  struct dd_plugins_o* out = dd_alloc(allocator, sizeof(struct dd_plugins_o), 1);
  out->allocator           = allocator;
  return out;
}

static void plugins_load_plugins(struct dd_plugins_o* obj, const char** paths, uint32 path_count) {
  char* lib_paths      = NULL;
  size_t* path_offsets = NULL;
  dd_plugin_t* plugins = NULL;

  for (uint32 ip = 0; ip < path_count; ip++) {
#if defined(_WIN32)
    static char dll_path[MAX_PATH];
    strcpy(dll_path, paths[ip]);
    if (strstr(dll_path, "*.dll") == NULL) {
      strcat(dll_path, "*.dll");
    }
#else
    static char dll_path[512];
    strcpy(dll_path, paths[ip]);
#endif

    // Find all plugins in this folder
    dd_stringlist_t files = dd_filesystem_api.directory_contents(dll_path, obj->allocator);

    dd_devprintf("Found %i plugins in folder '%s'\n", files.count, paths[ip]);
    // Copy the folder into dll_path
    strcpy(dll_path, paths[ip]);
#if defined(_WIN32)
    char* insertion = strrchr(dll_path, '\\');
    if (insertion == NULL) {
      insertion = strrchr(dll_path, '/');
    }
#else
    char* insertion = strrchr(dll_path, '/');
#endif
    if (insertion) {
      insertion++;
      *insertion = 0;
    }

    for (uint32 i = 0; i < files.count; i++) {
      // Create full path to the DLL
      snprintf(insertion, sizeof(dll_path), "%s", files.strings[i]);
      array_push(path_offsets, array_count(lib_paths), obj->allocator);
      array_append(lib_paths, dll_path, strlen(dll_path) + 1, obj->allocator);
      dd_plugin_t p = {.modification_time = dd_filesystem_api.file_last_modified(dll_path)};

      dynamic_library_load_plugin(&p, dll_path);
      strcpy(p.new_path, dll_path);
      array_push(plugins, p, obj->allocator);
    }

    array_free(files.payload, obj->allocator);

    obj->libs.paths        = lib_paths;
    obj->libs.path_offsets = path_offsets;
    obj->libs.plugins      = plugins;
  }

  // Try to load everything
  struct dd_allocator* temp_allocator = obj->allocator;
  uint32* plugins_to_init             = NULL;
  array_resize(plugins_to_init, array_count(obj->libs.plugins), temp_allocator);
  for (uint32 i = 0; i < array_count(obj->libs.plugins); i++) {
    plugins_to_init[i] = i;
  }

  size_t plugins_to_init_count = 0;
  do {
    plugins_to_init_count = array_count(plugins_to_init);

    uint32 error_plugins_index = 0;

    for (uint32 ip = 0; ip < array_count(plugins_to_init); ip++) {
      dd_plugin_t* p = obj->libs.plugins + plugins_to_init[ip];
      plugin_link_apis f =
          (plugin_link_apis)dd_dll_api.get_symbol(p->new_dll_handle, "plugin_link_apis");
      if (f) {
        dd_devprintf("linking plugin '%s'\n", p->new_path);
        // TODO: This isn't one of our plugins, remove from the list
        bool r = (*f)(&dd_api_registry, obj->allocator);
        if (!r) {
          plugins_to_init[error_plugins_index++] = ip;
        }
      }
    }

    array_resize(plugins_to_init, error_plugins_index, NULL);  // Decreasing size

  } while ((array_count(plugins_to_init) != plugins_to_init_count) &&
           (array_count(plugins_to_init) > 0));
}

static void plugins_check_for_changes(struct dd_plugins_o* obj) {
  size_t plugin_count = array_count(obj->libs.plugins);
  for (size_t ip = 0; ip < plugin_count; ip++) {
    struct dd_plugin_t* p = obj->libs.plugins + ip;
    uint64 lmt            = dd_filesystem_api.file_last_modified(p->new_path);
    if (lmt != p->modification_time) {
      dd_devprintf("need to reload %s\n", p->new_path);
      if (dynamic_library_load_plugin(p, p->new_path)) {
        dd_devprintf("Reload %s\n", p->new_path);
        p->modification_time = lmt;
        plugin_link_apis f =
            (plugin_link_apis)dd_dll_api.get_symbol(p->new_dll_handle, "plugin_link_apis");
        if (f) {
          // TODO: This isn't one of our plugins, remove from the list
          bool r = (*f)(&dd_api_registry, obj->allocator);
          if (!r) {
            dd_devprintf("error with reloaded DLL '%s'\n", p->new_dll_handle);
          }

          dd_dll_api.unload(p->old_dll_handle);
          p->old_dll_handle = 0;
        } else {
          dd_dll_api.unload(p->new_dll_handle);
        }
      } else {
        dd_devprintf("Couldn't (re)load %s\n", p->new_path);
      }
    }
  }
}

void plugins_unload_all(struct dd_plugins_o* obj) {
  size_t plugin_count = array_count(obj->libs.plugins);
  for (size_t ip = 0; ip < plugin_count; ip++) {
    struct dd_plugin_t* p = obj->libs.plugins + ip;
    dd_dll_api.unload(p->old_dll_handle);
    dd_dll_api.unload(p->new_dll_handle);
#if defined(_WIN32)
    if (p->old_dll_handle) {
      char extension[5];
      char target_path[MAX_PATH];
      strcpy(target_path, p->new_path);
      sprintf(extension, ".d%02i", (int)p->old_revision);
      PathRenameExtension(target_path, extension);
      dd_filesystem_api.remove_file(target_path);
      sprintf(extension, ".p%02i", (int)p->old_revision);
      PathRenameExtension(target_path, extension);
      dd_filesystem_api.remove_file(target_path);
    }
    if (p->new_dll_handle) {
      char extension[5];
      char target_path[MAX_PATH];
      strcpy(target_path, p->new_path);
      sprintf(extension, ".d%02i", (int)p->reload_count);
      PathRenameExtension(target_path, extension);
      dd_filesystem_api.remove_file(target_path);
      sprintf(extension, ".p%02i", (int)p->reload_count);
      PathRenameExtension(target_path, extension);
      dd_filesystem_api.remove_file(target_path);
    }
#else
#endif
  }
}

const struct dd_plugins_api dd_plugins_api = {.create            = &plugins_create,
                                              .load_plugins      = &plugins_load_plugins,
                                              .check_for_changes = &plugins_check_for_changes,
                                              .unload_all        = plugins_unload_all};
