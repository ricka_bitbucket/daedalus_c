#pragma once

#include "types.h"

enum ELogEntryBitfieldSizes {
  ELogEntryArgCountSize   = 5,
  ELogEntrySeveritySize   = 3,
  ELogEntryLineSize       = 23,
  ELogEntrySentRemoteSize = 1
};
typedef struct dd_log_entry {
  const char* function;
  const char* file;
  const char* format;
  uint32 count_args : ELogEntryArgCountSize;
  uint32 severity : ELogEntrySeveritySize;
  uint32 line : ELogEntryLineSize;
  uint32 did_send_to_remote : ELogEntrySentRemoteSize;
} dd_log_entry;
