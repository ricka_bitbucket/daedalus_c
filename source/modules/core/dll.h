#pragma once

typedef void* dd_dll_handle_t;

struct dd_dll_api {
  dd_dll_handle_t (*load)(const char* path);

  void (*unload)(dd_dll_handle_t dll_handle);

  void* (*get_symbol)(dd_dll_handle_t dll_handle, const char* symbol_name);
};

extern const struct dd_dll_api dd_dll_api;
