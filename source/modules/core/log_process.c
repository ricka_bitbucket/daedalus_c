#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "log.h"
#include "log_structs.inl"
#include "types.h"

#if !defined(_MSC_VER)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wformat-nonliteral"
  #pragma clang diagnostic ignored "-Wformat-security"
#endif

void dd_log_process_buffer_with_callback(void* buffer, unsigned byte_count,
                                         dd_log_print_callback func) {
  char line_buffer[2048];
  char* line_buffer_end = line_buffer + sizeof(line_buffer);
  char single_specifier_format_buffer[256];

  uint8* it    = (uint8*)buffer;
  uint8* itEnd = it + byte_count;
  while (it != itEnd) {
    uint8* entry_start  = it;
    dd_log_entry* entry = *(dd_log_entry**)it;
    it += sizeof(dd_log_entry*);

    char* next_in_line_buffer = line_buffer;

    unsigned arg_count = entry->count_args;

#if defined(__x86_64)
    void** line_next_pointer_arg = (void**)it;
    int* line_next_int_arg       = (int*)(line_next_pointer_arg + arg_count);
    double* line_next_double_arg = (double*)(line_next_int_arg + arg_count);
#endif
    const char* dissected_format = entry->format;
    const char* prev_processed   = dissected_format;
    const char* next_specifier   = prev_processed;
    next_specifier               = strchr(next_specifier, '%');
    while (next_specifier) {
      ++next_specifier;

      // find end character
      while (!((*next_specifier >= 'a' && *next_specifier <= 'z') ||
               (*next_specifier >= 'A' && *next_specifier <= 'Z') || (*next_specifier == '%'))) {
        ++next_specifier;
      }

      strncpy(single_specifier_format_buffer, prev_processed, next_specifier - prev_processed + 1);
      single_specifier_format_buffer[next_specifier - prev_processed + 1] = 0;

      switch (*next_specifier) {
        case 'i':  // Intentional fallthrough
        case 'd':  // Intentional fallthrough
        case 'u':  // Intentional fallthrough
        case 'o':  // Intentional fallthrough
        case 'x':  // Intentional fallthrough
        case 'X':  // Intentional fallthrough
        case 'c': {
#if defined(__x86_64)
          int v = *line_next_int_arg;
          ++line_next_int_arg;
#elif defined(_WIN64)
          int v = *(int*)it;
          it += sizeof(double);
#else
          int v = *(int*)it;
          it += sizeof(int);
#endif
          snprintf(next_in_line_buffer, line_buffer_end - next_in_line_buffer,
                   single_specifier_format_buffer, v);
          next_in_line_buffer = strchr(next_in_line_buffer, 0);
        } break;
        case 'f':  // Intentional fallthrough
        case 'F':  // Intentional fallthrough
        case 'e':  // Intentional fallthrough
        case 'E':  // Intentional fallthrough
        case 'g':  // Intentional fallthrough
        case 'G':  // Intentional fallthrough
        case 'a':  // Intentional fallthrough
        case 'A': {
#if defined(__x86_64)
          double v = *line_next_double_arg;
          ++line_next_double_arg;
#else
          double v = *(double*)it;
          it += sizeof(double);
#endif
          snprintf(next_in_line_buffer, line_buffer_end - next_in_line_buffer,
                   single_specifier_format_buffer, v);
          next_in_line_buffer = strchr(next_in_line_buffer, 0);
        } break;
        case 'p': {
#if defined(__x86_64)
          void* v = *line_next_pointer_arg;
          ++line_next_pointer_arg;
#elif defined(_WIN64)
          void* v = *(void**)it;
          it += sizeof(double);
#else
          void* v = *(void**)it;
          it += sizeof(void*);
#endif
          snprintf(next_in_line_buffer, line_buffer_end - next_in_line_buffer,
                   single_specifier_format_buffer, v);
          next_in_line_buffer = strchr(next_in_line_buffer, 0);
        } break;
        case '%': {
          next_in_line_buffer[0] = '%';
          next_in_line_buffer[1] = '\0';
          ++next_in_line_buffer;
        } break;
        default: {
          assert(FALSE && "Unknown specifier");
        }
      }

      // Do stuff
      prev_processed = next_specifier + 1;
      next_specifier = strchr(prev_processed, '%');
    }
    strcpy(next_in_line_buffer, prev_processed);
    next_in_line_buffer = strchr(next_in_line_buffer, 0);

    func(entry->file, entry->line, entry->function, (ELogSeverity)entry->severity, line_buffer);

#if defined(__x86_64)
    it = entry_start + sizeof(entry) + arg_count * (sizeof(int) + sizeof(double) + sizeof(void*));
#else
    it = entry_start + sizeof(entry) + arg_count * sizeof(double);
#endif
  }
}

#if !defined(_MSC_VER)
  #pragma clang diagnostic pop
#endif
