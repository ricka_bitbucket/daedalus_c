#include <assert.h>
#include <stdio.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../../../examples/metal_render_to_file/stb_image_write.h"
#include "core/containers.h"
#include "core/test.h"
#include "core/types.h"
#include "graphics_test.inl"

bool compare_images(const uint8* ref, const uint8* test, uint32 dimension) {
  const uint8* pr = ref;
  const uint8* pt = test;
  for (uint32 y = 0; y < dimension; y++) {
    for (uint32 x = 0; x < dimension; x++, pr += 4, pt += 4) {
      if ((pr[0] != pt[0]) || (pr[1] != pt[1]) || (pr[2] != pt[2]) || (pr[3] != pt[3])) {
        fprintf(stderr, "Pixel %u,%u is different ref(%u,%u,%u,%u) vs test(%u,%u,%u,%u)\n", x, y,
                pr[0], pr[1], pr[2], pr[3], pt[0], pt[1], pt[2], pt[3]);
        return false;
      }
    }
  }
  return true;
}

uint8 g_reference_data[RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4]            = {0};
static uint8 g_render_output_data[RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4] = {0};

void check_render_output(const char* test_name, dd_test_data_t* test_data, uint32 dimension) {
  g_graphics_api->device.wait_until_idle(g_graphics_device);

  memset(g_render_output_data, 0, sizeof(g_render_output_data));
  g_graphics_api->texture.get_pixels(g_graphics_device, test_color_render_target,
                                     (dd_rect_t){0, 0, dimension, dimension}, 0,
                                     g_render_output_data);

  bool are_equal = compare_images(g_reference_data, g_render_output_data, dimension);
  if (!are_equal) {
    char test_file_path[1024] = {0};
    snprintf(test_file_path, sizeof(test_file_path) - 2, "%s.png", test_name);
    stbi_write_png(test_file_path, (int)dimension, (int)dimension, 4, g_render_output_data, 0);
    stbi_write_png("reference.png", (int)dimension, (int)dimension, 4, g_reference_data, 0);
  }
  DD_TEST_IS_TRUE(are_equal);
}
