#pragma once

#include "graphics_api/graphics.h"

enum {
  RENDER_TEXTURE_DIMENSION = 128,
};

extern dd_graphics_device_t g_graphics_device;
extern dd_graphics_render_pass_descriptor_t rpd;
extern dd_graphics_queue_t graphics_queue;
extern dd_graphics_texture_t test_color_render_target;
extern dd_graphics_api_t* g_graphics_api;

bool compare_images(const uint8* ref, const uint8* test, uint32 dimension);
void check_render_output(const char* test_name, dd_test_data_t* test_data, uint32 dimension);
