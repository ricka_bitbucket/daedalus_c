/*  Create coloured grid of squares

 No buffers
 - Using only vertex id, calculate info in shader
 - Using vertex id and instance id, calculate info in shader

 Buffers, no textures
 - Single buffer with interleaved positions and colours for all squares
 1 drawcall
 - Two buffers, one with positions and one with colours, for all squares
 1 drawcall
 - A single buffer with position and colour for each square individually
 drawcall per square
 - Two buffers, one with position and one with colour, for each square individually
 drawcall per square

 Single texture
 - Single texture with all colours, size of screen, squares index into pixels they render to
 effectively
 - Single texture with single pixel per square, each square indexes into a single pixel
 - A texture per square
 drawcall per square

 Array of textures in shader
 - An array of textures with one entry per square, and index into array in shader from a buffer
 - An array of textures with one entry per square, and index into array in shader with a push
 constant
 - An array of textures with one entry per square, and index into array in shader with instancing

 Texture array type (vs 2D or 3D texture)
 - A 2d array-texture with one entry per square, and index into texture level in shader from a
 buffer
 - A 2d array-texture with one entry per square, and index into texture level in shader with a push
 constant
 - An 2d array-texture with one entry per square, and index into texture level in shader with
 instancing

 */

#include "graphics_api/graphics.h"

#include <assert.h>

#include "../../../examples/metal_render_to_file/stb_image_write.h"
#include "core/containers.h"
#include "core/memory.h"
#include "core/types.h"
#include "core/utils.h"

#define DD_TEST_GENERATE_MAIN
#include "core/test.h"

//
#include "core/dd_math.h"

// TODO: use plugin manager
#if defined(_WIN32)
extern dd_graphics_api_t dd_vulkan;
dd_graphics_api_t* g_graphics_api = &dd_vulkan;
#else
extern dd_graphics_api_t dd_metal;
dd_graphics_api_t* g_graphics_api = &dd_metal;
#endif

#include "graphics_test.inl"
static uint8 global_mem_buffer[500 * 1024];

enum {
  SQUARE_DIMENSION      = 4,
  SQUARE_COUNT_PER_SIDE = RENDER_TEXTURE_DIMENSION / SQUARE_DIMENSION,
  TOTAL_SQUARE_COUNT    = SQUARE_COUNT_PER_SIDE * SQUARE_COUNT_PER_SIDE
};

dd_graphics_device_t g_graphics_device;
dd_graphics_render_pass_descriptor_t rpd       = {0};
dd_graphics_queue_t graphics_queue             = {0};
dd_graphics_texture_t test_color_render_target = {0};

dd_graphics_buffer_t g_vertex_buffer;
static dd_graphics_buffer_t g_buffer_texcoords;
dd_graphics_buffer_t g_buffer_colors;
static dd_graphics_texture_t g_texture;
static dd_graphics_texture_t g_tiny_textures[TOTAL_SQUARE_COUNT];
static dd_graphics_material_prototype_t g_material_prototype = {0};

extern uint8 g_reference_data[RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4];

static float quads_vertices[TOTAL_SQUARE_COUNT * 6 * 2]  = {0};
static float quads_texcoords[TOTAL_SQUARE_COUNT * 6 * 2] = {0};
static float quads_colors[TOTAL_SQUARE_COUNT * 3]        = {0};
void calculate_quad_vertices() {
  uint32 i = 0;
  float dx = 2 * (float)SQUARE_DIMENSION / ((float)RENDER_TEXTURE_DIMENSION);
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++) {
      quads_vertices[i++] = -1.f + x * dx;
      quads_vertices[i++] = -1.f + y * dx;

      quads_vertices[i++] = -1.f + (x + 1) * dx;
      quads_vertices[i++] = -1.f + y * dx;

      quads_vertices[i++] = -1.f + x * dx;
      quads_vertices[i++] = -1.f + (y + 1) * dx;

      quads_vertices[i++] = -1.f + (x + 1) * dx;
      quads_vertices[i++] = -1.f + y * dx;

      quads_vertices[i++] = -1.f + (x + 1) * dx;
      quads_vertices[i++] = -1.f + (y + 1) * dx;

      quads_vertices[i++] = -1.f + x * dx;
      quads_vertices[i++] = -1.f + (y + 1) * dx;
    }
  }
}
void calculate_quad_texcoords() {
  uint32 i = 0;
  float dx = (float)SQUARE_DIMENSION / ((float)RENDER_TEXTURE_DIMENSION);
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++) {
      quads_texcoords[i++] = (x + 1) * dx;
      quads_texcoords[i++] = y * dx;
      quads_texcoords[i++] = x * dx;
      quads_texcoords[i++] = y * dx;
      quads_texcoords[i++] = x * dx;
      quads_texcoords[i++] = (y + 1) * dx;
      quads_texcoords[i++] = (x + 1) * dx;
      quads_texcoords[i++] = y * dx;
      quads_texcoords[i++] = (x + 1) * dx;
      quads_texcoords[i++] = (y + 1) * dx;
      quads_texcoords[i++] = x * dx;
      quads_texcoords[i++] = (y + 1) * dx;
    }
  }
}
void calculate_quad_colors() {
  uint32 i = 0;
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++) {
      float r           = (((float)x) / SQUARE_COUNT_PER_SIDE) * 256.f / 255.f;
      float g           = (((float)y) / SQUARE_COUNT_PER_SIDE) * 256.f / 255.f;
      quads_colors[i++] = r;
      quads_colors[i++] = g;
      quads_colors[i++] = 0;
    }
  }
}

void create_tiny_textures() {
  dd_graphics_texture_descriptor_t td = {.format          = R8G8B8A8,
                                         .width           = SQUARE_DIMENSION,
                                         .height          = SQUARE_DIMENSION,
                                         .layers          = 1,
                                         .mip_level_count = 1,
                                         .storage_mode    = SHARED,
                                         .usage           = TEXTURE_USAGE_SHADER_READ};
  for (uint32 sy = 0; sy < SQUARE_COUNT_PER_SIDE; sy++) {
    for (uint32 sx = 0; sx < SQUARE_COUNT_PER_SIDE; sx++) {
      uint32 square_index           = sy * SQUARE_COUNT_PER_SIDE + sx;
      dd_graphics_texture_t texture = g_graphics_api->texture.create(g_graphics_device, &td);
      g_tiny_textures[square_index] = texture;

      uint8 tex_data[SQUARE_DIMENSION * SQUARE_DIMENSION * 4];

      uint8 square_color[3] = {(uint8)(quads_colors[square_index * 3 + 0] * 255),
                               (uint8)(quads_colors[square_index * 3 + 1] * 255),
                               (uint8)(quads_colors[square_index * 3 + 2] * 255)};
      for (uint32 y = 0; y < 4; y++) {
        uint32 row_offset = y * SQUARE_DIMENSION * 4;
        for (uint32 x = 0; x < 4; x++) {
          uint32 offset        = row_offset + x * 4;
          tex_data[offset + 0] = square_color[0];
          tex_data[offset + 1] = square_color[1];
          tex_data[offset + 2] = square_color[2];
          tex_data[offset + 3] = 0xFF;
        }
      }
      g_graphics_api->texture.set_pixels(g_graphics_device, texture,
                                         (dd_rect_t){0, 0, SQUARE_DIMENSION, SQUARE_DIMENSION}, 0,
                                         0, SQUARE_DIMENSION * SQUARE_DIMENSION * 4, tex_data);
    }
  }

  g_graphics_api->device.wait_until_idle(g_graphics_device);
}

static void fill_reference_image() {
  uint32 factor = 256 / RENDER_TEXTURE_DIMENSION;
  for (uint32 y = 0; y < RENDER_TEXTURE_DIMENSION; y++) {
    for (uint32 x = 0; x < RENDER_TEXTURE_DIMENSION; x++) {
      const uint32 pixel_offset =
          ((RENDER_TEXTURE_DIMENSION - y - 1) * RENDER_TEXTURE_DIMENSION + x) * 4;
      g_reference_data[pixel_offset + 0] =
          (uint8)(factor * SQUARE_DIMENSION * (x / SQUARE_DIMENSION));
      g_reference_data[pixel_offset + 1] =
          (uint8)(factor * SQUARE_DIMENSION * (y / SQUARE_DIMENSION));
      g_reference_data[pixel_offset + 2] = 0;
      g_reference_data[pixel_offset + 3] = 0xFF;
    }
  }
}

static void deinit_all_render_resources() {
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype);
  if (g_buffer_texcoords) {
    g_graphics_api->buffer.destroy(g_graphics_device, 1, &g_buffer_texcoords);
    g_buffer_texcoords = 0;
  }
  if (g_buffer_colors) {
    g_graphics_api->buffer.destroy(g_graphics_device, 1, &g_buffer_colors);
    g_buffer_colors = 0;
  }
  if (g_texture) {
    g_graphics_api->texture.destroy(g_graphics_device, 1, &g_texture);
    g_texture = 0;
  }
}

TEST_CASE(quads_vertex_color_single_call_indexed) {
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  float* colors = {0};
  array_reserve(colors, TOTAL_SQUARE_COUNT * 6 * 3, allocator);
  uint32 quad_index = 0;
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++, quad_index++) {
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
    }
  }
  uint32 size = (uint32)(sizeof(float) * array_count(colors));
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_colors);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_colors, 0, (const void*)colors,
                                     size);

#if defined(_WIN32)
  const uint8* vs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/vertex_color.vs.spv", allocator);
  const uint8* fs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/vertex_color.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_vs",
    .fs_code       = (const uint8*)"vertex_color_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}, {.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  dd_graphics_buffer_t index_buffer;
  const uint32 index_count = TOTAL_SQUARE_COUNT * 6;
  uint32 sizes[1]          = {index_count * sizeof(uint16)};
  g_graphics_api->buffer.allocate(g_graphics_device, 1, sizes, &index_buffer);
  uint16* ib_data = (uint16*)malloc((size_t)sizes[0]);
  for (uint16 i = 0; i < (uint16)TOTAL_SQUARE_COUNT * 6; i += 6) {
    ib_data[i + 0] = i;
    ib_data[i + 1] = i + 1;
    ib_data[i + 2] = i + 2;

    ib_data[i + 3] = i + 3;
    ib_data[i + 4] = i + 4;
    ib_data[i + 5] = i + 5;
  }
  g_graphics_api->buffer.update_data(g_graphics_device, index_buffer, 0, ib_data, sizes[0]);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .index_buffer = {.buffer = index_buffer, .offset = 0, .range = sizes[0]},
      .buffers      = {
          {.buffer = g_vertex_buffer, .offset = 0, .range = TOTAL_SQUARE_COUNT * 6 * 2 * 4},
          {.buffer = g_buffer_colors, .offset = 0, .range = TOTAL_SQUARE_COUNT * 6 * 3 * 4}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw_indexed(graphics_cmd, TOTAL_SQUARE_COUNT * 6, 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->buffer.destroy(g_graphics_device, 1, &index_buffer);
  deinit_all_render_resources();
}

TEST_CASE(quads_vertex_color_single_call) {
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  float* colors = {0};
  array_reserve(colors, TOTAL_SQUARE_COUNT * 6 * 3, allocator);
  uint32 quad_index = 0;
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++, quad_index++) {
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
    }
  }
  uint32 size = (uint32)(sizeof(float) * array_count(colors));
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_colors);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_colors, 0, (const void*)colors,
                                     size);

#if defined(_WIN32)
  const uint8* vs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/vertex_color.vs.spv", allocator);
  const uint8* fs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/vertex_color.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_vs",
    .fs_code       = (const uint8*)"vertex_color_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}, {.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers = {
          {.buffer = g_vertex_buffer, .offset = 0, .range = TOTAL_SQUARE_COUNT * 6 * 2 * 4},
          {.buffer = g_buffer_colors, .offset = 0, .range = TOTAL_SQUARE_COUNT * 6 * 3 * 4}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw(graphics_cmd, TOTAL_SQUARE_COUNT * 6, 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

TEST_CASE(quads_vertex_color_multiple_calls) {
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  float* colors = {0};
  array_reserve(colors, TOTAL_SQUARE_COUNT * 6 * 3, allocator);
  uint32 quad_index = 0;
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++, quad_index++) {
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 0]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 1]);
      array_push_no_alloc(colors, quads_colors[quad_index * 3 + 2]);
    }
  }
  uint32 size = (uint32)(sizeof(float) * array_count(colors));
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_colors);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_colors, 0, (const void*)colors,
                                     size);

#if defined(_WIN32)
  const uint8* vs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/vertex_color.vs.spv", allocator);
  const uint8* fs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/vertex_color.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_vs",
    .fs_code       = (const uint8*)"vertex_color_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}, {.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  for (uint32 i = 0; i < TOTAL_SQUARE_COUNT; i++) {
    dd_graphics_material_descriptor_t md = {
        .buffers = {{.buffer = g_vertex_buffer, .offset = i * 6 * 2 * 4, .range = 6 * 2 * 4},
                    {.buffer = g_buffer_colors, .offset = i * 6 * 3 * 4, .range = 6 * 3 * 4}}};
    g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype,
                                                            &md);
    g_graphics_api->commandbuffer.draw(graphics_cmd, 6, 1);
  }

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

TEST_CASE(quads_vertex_color_instanced) {
  uint32 size = sizeof(quads_colors);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_colors);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_colors, 0,
                                     (const void*)quads_colors, size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_instanced.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_instanced.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_instanced_vs",
    .fs_code       = (const uint8*)"vertex_color_instanced_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}, {.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers = {
          {.buffer = g_vertex_buffer, .offset = 0, .range = TOTAL_SQUARE_COUNT * 6 * 2 * 4},
          {.buffer = g_buffer_colors, .offset = 0, .range = TOTAL_SQUARE_COUNT * 3 * 4}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw(graphics_cmd, 6, TOTAL_SQUARE_COUNT);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

TEST_CASE(quads_vertex_color_push_constants) {
#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));
  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_push_constants.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_push_constants.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_push_constants_vs",
    .fs_code       = (const uint8*)"vertex_color_push_constants_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypePushConstant, .props.push_constant.range = 8 * sizeof(float)}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  uint32 i = 0;
  float dx = 2 * (float)SQUARE_DIMENSION / ((float)RENDER_TEXTURE_DIMENSION);
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++, i++) {
      dd_graphics_material_descriptor_t md = {
          .buffers = {{.buffer = g_vertex_buffer, .offset = 0, .range = 6 * 2 * 4}}};
      g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype,
                                                              &md);
      float data[6] = {x * dx,
                       y * dx,
                       quads_colors[3 * i + 0],
                       quads_colors[3 * i + 1],
                       quads_colors[3 * i + 2],
                       0};
      g_graphics_api->commandbuffer.push_constants(graphics_cmd, sizeof(data), data);
      g_graphics_api->commandbuffer.draw(graphics_cmd, 6, 1);
    }
  }

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

// Render the quads with a single large texture where each quad references its own piece of the
// texture.
TEST_CASE(quads_textured_single_texture) {
  uint32 size = sizeof(quads_texcoords);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_texcoords);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_texcoords, 0,
                                     (const void*)quads_texcoords, size);

  dd_graphics_texture_descriptor_t td = {.format          = R8G8B8A8,
                                         .width           = RENDER_TEXTURE_DIMENSION,
                                         .height          = RENDER_TEXTURE_DIMENSION,
                                         .layers          = 1,
                                         .mip_level_count = 1,
                                         .storage_mode    = SHARED,
                                         .usage           = TEXTURE_USAGE_SHADER_READ};
  g_texture                           = g_graphics_api->texture.create(g_graphics_device, &td);
  uint8* tex_data = (uint8*)malloc(RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4);
  for (uint32 y = 0; y < RENDER_TEXTURE_DIMENSION; y++) {
    memcpy(tex_data + y * RENDER_TEXTURE_DIMENSION * 4,
           g_reference_data + ((RENDER_TEXTURE_DIMENSION - y - 1) * RENDER_TEXTURE_DIMENSION * 4),
           RENDER_TEXTURE_DIMENSION * 4);
  }
  g_graphics_api->texture.set_pixels(
      g_graphics_device, g_texture,
      (dd_rect_t){0, 0, RENDER_TEXTURE_DIMENSION, RENDER_TEXTURE_DIMENSION}, 0, 0,
      RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4, tex_data);
  free(tex_data);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/textured.vs.spv", allocator);
  const uint8* fs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/textured.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"textured_vs",
    .fs_code       = (const uint8*)"textured_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeTexture}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers  = {{.buffer = g_vertex_buffer, .offset = 0, .range = sizeof(quads_vertices)},
                  {.buffer = g_buffer_texcoords, .offset = 0, .range = sizeof(quads_texcoords)}},
      .textures = {NULL, NULL, &g_texture}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw(graphics_cmd, TOTAL_SQUARE_COUNT * 6, 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

// Render the quads with small textures where where each quad references its own texture.
// Each quad is rendered one at a time
TEST_CASE(quads_textured_multiple_independent_textures) {
  uint32 size = sizeof(quads_texcoords);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_texcoords);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_texcoords, 0,
                                     (const void*)quads_texcoords, size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/textured_push_constants.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/textured_push_constants.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"textured_push_constants_vs",
    .fs_code       = (const uint8*)"textured_push_constants_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeStorageBuffer},
                 {.type = BindingTypePushConstant, .props.push_constant.range = 2 * sizeof(float)},
                 {.type = BindingTypeTexture}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers  = {{.buffer = g_vertex_buffer, .offset = 0, .range = 6 * sizeof(float2)},
                  {.buffer = g_buffer_texcoords, .offset = 0, .range = 6 * sizeof(float2)}},
      .textures = {NULL, NULL, NULL, NULL}};

  uint32 i = 0;
  float dx = 2 * (float)SQUARE_DIMENSION / ((float)RENDER_TEXTURE_DIMENSION);
  for (uint32 y = 0; y < SQUARE_COUNT_PER_SIDE; y++) {
    for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++, i++) {
      md.textures[3] = &(g_tiny_textures[y * SQUARE_COUNT_PER_SIDE + x]);
      g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype,
                                                              &md);
      float data[2] = {x * dx, y * dx};
      g_graphics_api->commandbuffer.push_constants(graphics_cmd, sizeof(data), data);
      g_graphics_api->commandbuffer.draw(graphics_cmd, 6, 1);
    }
  }

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

TEST_CASE(quads_textured_array_of_textures) {
  uint32 size = sizeof(quads_texcoords);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_texcoords);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_texcoords, 0,
                                     (const void*)quads_texcoords, size);
  const uint32 bound_texture_count = 128;
#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/textured_instanced_array_of_textures.vs.spv",
      allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/textured_instanced_array_of_textures.fs.spv",
      allocator);
  dd_graphics_material_prototype_descriptor_t mpd =
  {.vs_code       = (const uint8*)vs_code,
   .fs_code       = (const uint8*)fs_code,
   .vs_byte_count = (uint32)array_count(vs_code),
   .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
      .vs_code       = (const uint8*)"textured_instanced_array_of_textures_vs",
      .fs_code       = (const uint8*)"textured_instanced_array_of_textures_fs",
      .vs_byte_count = 0,
      .fs_byte_count = 0,
#endif
   .output   = {.color_format = R8G8B8A8},
   .bindings = {
       {.type = BindingTypeStorageBuffer},
       {.type = BindingTypeStorageBuffer},
       {.type = BindingTypeTexture, .props.array.count = bound_texture_count},
       {.type = BindingTypePushConstant, .props.push_constant = {.offset = 0, .range = 12}},
   } };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  float dx = 2 * (float)SQUARE_DIMENSION / ((float)RENDER_TEXTURE_DIMENSION);
  for (uint32 c = 0; c < TOTAL_SQUARE_COUNT; c += bound_texture_count) {
    dd_graphics_material_descriptor_t md = {
        .buffers  = {{.buffer = g_vertex_buffer, .offset = 0, .range = 6 * 2 * sizeof(float)},
                    {.buffer = g_buffer_texcoords, .offset = 0, .range = 6 * 2 * sizeof(float)}},
        .textures = {NULL, NULL, g_tiny_textures + c}};
    g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype,
                                                            &md);

    uint32 y_start = c / SQUARE_COUNT_PER_SIDE;
    uint32 y_end   = y_start + bound_texture_count / SQUARE_COUNT_PER_SIDE;
    for (uint32 y = y_start; y < y_end; y++) {
      for (uint32 x = 0; x < SQUARE_COUNT_PER_SIDE; x++) {
        float data[3]     = {x * dx, y * dx};
        *(int*)&(data[2]) = (int)((y * SQUARE_COUNT_PER_SIDE + x) % bound_texture_count);
        g_graphics_api->commandbuffer.push_constants(graphics_cmd, sizeof(data), data);
        g_graphics_api->commandbuffer.draw(graphics_cmd, 6, 1);
      }
    }
  }

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

TEST_CASE(quads_textured_array_of_textures_dynamic_indexing) {
  uint32 size = sizeof(quads_texcoords);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_texcoords);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_texcoords, 0,
                                     (const void*)quads_texcoords, size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/"
      "textured_array_of_textures_dynamic_indexed.vs.spv",
      allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/"
      "textured_array_of_textures_dynamic_indexed.fs.spv",
      allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"textured_array_of_textures_dynamic_indexed_vs",
    .fs_code       = (const uint8*)"textured_array_of_textures_dynamic_indexed_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeTexture, .props.array.count = 128}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  for (uint32 c = 0; c < TOTAL_SQUARE_COUNT; c += mpd.bindings[2].props.array.count) {
    dd_graphics_material_descriptor_t md = {
        .buffers  = {{.buffer = g_vertex_buffer, .offset = 0, .range = sizeof(quads_vertices)},
                    {.buffer = g_buffer_texcoords, .offset = 0, .range = 6 * 2 * sizeof(float)}},
        .textures = {NULL, NULL, g_tiny_textures + c}};
    g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype,
                                                            &md);

    g_graphics_api->commandbuffer.draw_with_offsets(graphics_cmd, 6, 0, 128, c);
  }

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  deinit_all_render_resources();
}

TEST_CASE(quads_textured_texture_array) {
  uint32 size = sizeof(quads_texcoords);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_texcoords);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_texcoords, 0,
                                     (const void*)quads_texcoords, size);

  dd_graphics_texture_descriptor_t tc = {.width           = SQUARE_DIMENSION,
                                         .height          = SQUARE_DIMENSION,
                                         .layers          = TOTAL_SQUARE_COUNT,
                                         .mip_level_count = 1,
                                         .format          = R8G8B8A8,
                                         .storage_mode    = SHARED,
                                         .usage           = TEXTURE_USAGE_SHADER_READ};
  dd_graphics_texture_t array_texture = g_graphics_api->texture.create(g_graphics_device, &tc);

  uint8 tex_data[SQUARE_DIMENSION * SQUARE_DIMENSION * 4];
  for (uint32 sy = 0; sy < SQUARE_COUNT_PER_SIDE; sy++) {
    for (uint32 sx = 0; sx < SQUARE_COUNT_PER_SIDE; sx++) {
      uint32 square_index   = sy * SQUARE_COUNT_PER_SIDE + sx;
      uint8 square_color[3] = {(uint8)(quads_colors[square_index * 3 + 0] * 255),
                               (uint8)(quads_colors[square_index * 3 + 1] * 255),
                               (uint8)(quads_colors[square_index * 3 + 2] * 255)};
      for (uint32 y = 0; y < 4; y++) {
        uint32 row_offset = y * SQUARE_DIMENSION * 4;
        for (uint32 x = 0; x < 4; x++) {
          uint32 offset        = row_offset + x * 4;
          tex_data[offset + 0] = square_color[0];
          tex_data[offset + 1] = square_color[1];
          tex_data[offset + 2] = square_color[2];
          tex_data[offset + 3] = 0xFF;
        }
      }
      g_graphics_api->texture.set_pixels(
          g_graphics_device, array_texture, (dd_rect_t){0, 0, SQUARE_DIMENSION, SQUARE_DIMENSION},
          square_index, 0, SQUARE_DIMENSION * SQUARE_DIMENSION * 4, tex_data);
    }
  }

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/textured_instanced_texture_array.vs.spv",
                allocator);
  const uint8* fs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/textured_instanced_texture_array.fs.spv",
                allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"textured_instanced_texture_array_vs",
    .fs_code       = (const uint8*)"textured_instanced_texture_array_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeTexture}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);
  dd_graphics_material_descriptor_t md = {
      .buffers  = {{.buffer = g_vertex_buffer, .offset = 0, .range = sizeof(quads_vertices)},
                  {.buffer = g_buffer_texcoords, .offset = 0, .range = 6 * 2 * sizeof(float)}},
      .textures = {NULL, NULL, &array_texture}};

  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw(graphics_cmd, 6, TOTAL_SQUARE_COUNT);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->texture.destroy(g_graphics_device, 1, &array_texture);
  deinit_all_render_resources();
}

TEST_SUITE(graphics_colored_squares, test_data) {
  fill_reference_image();
  calculate_quad_vertices();
  calculate_quad_texcoords();
  calculate_quad_colors();

  g_graphics_device = g_graphics_api->create_default_device(tmp_malloc_allocator);
  dd_graphics_texture_descriptor_t tc = {
      .width           = RENDER_TEXTURE_DIMENSION,
      .height          = RENDER_TEXTURE_DIMENSION,
      .layers          = 1,
      .mip_level_count = 1,
      .format          = R8G8B8A8,
      .storage_mode    = DEVICE_LOCAL,
      .usage           = TEXTURE_USAGE_RENDER_TARGET | TEXTURE_USAGE_TRANSFER_SOURCE};
  test_color_render_target = g_graphics_api->texture.create(g_graphics_device, &tc);

  rpd = (dd_graphics_render_pass_descriptor_t){.color  = {test_color_render_target},
                                               .width  = RENDER_TEXTURE_DIMENSION,
                                               .height = RENDER_TEXTURE_DIMENSION};

  graphics_queue = g_graphics_api->device.get_queue(g_graphics_device, QueueTypeGraphics);

  uint32 size = sizeof(quads_vertices);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_vertex_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, g_vertex_buffer, 0,
                                     (const void*)quads_vertices, size);

  create_tiny_textures();

  TEST_ADD_CASE(quads_vertex_color_single_call, test_data);
  TEST_ADD_CASE(quads_vertex_color_single_call_indexed, test_data);
#if defined(__APPLE__)
  // Individual buffer offsets need to be aligned to 256 bytes in MacOS, which isn't the case
  (void)quads_vertex_color_multiple_calls;
#else
  TEST_ADD_CASE(quads_vertex_color_multiple_calls, test_data);
#endif
  TEST_ADD_CASE(quads_vertex_color_instanced, test_data);
  TEST_ADD_CASE(quads_vertex_color_push_constants, test_data);

  TEST_ADD_CASE(quads_textured_single_texture, test_data);
  TEST_ADD_CASE(quads_textured_multiple_independent_textures, test_data);
  TEST_ADD_CASE(quads_textured_array_of_textures, test_data);
  TEST_ADD_CASE(quads_textured_array_of_textures_dynamic_indexing, test_data);
  TEST_ADD_CASE(quads_textured_texture_array, test_data);
}

TEST_CASE(mipmap) {
  memset(g_reference_data, 0, RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4);

  // clang-format off
  float2 vert_uvs[] = {
    {0,0}, {1,0}, {0,1},
    {1,0}, {1,1}, {0,1}
  };
  uint32 size = sizeof(vert_uvs);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &g_buffer_texcoords);
  g_graphics_api->buffer.update_data(g_graphics_device, g_buffer_texcoords, 0,
                                     (const void*)vert_uvs, size);

  // clang-format off
  float2 vert_positions[] = {
    {-1, -1}, {0, -1}, {-1, 0},
    {0, -1}, {0, 0}, {-1, 0},

    {0, 0}, {0.5f, 0}, {0, 0.5f},
    {0.5f, 0}, {0.5f, 0.5f}, {0, 0.5f},

    {0.5f, 0.5f}, {0.75f, 0.5f}, {0.5f, 0.75f},
    {0.75f, 0.5f}, {0.75f, 0.75f}, {0.5f, 0.75f},

    {0.75f, 0.75f}, {0.875f, 0.75f}, {0.75f, 0.875f},
    {0.875f, 0.75f}, {0.875f, 0.875f}, {0.75f, 0.875f},

    {0.875f, 0.875f}, {0.9375f, 0.875f}, {0.875f, 0.9375f},
    {0.9375f, 0.875f}, {0.9375f, 0.9375f}, {0.875f, 0.9375f}
  };
  // clang-format on
  dd_graphics_buffer_t pos_buffer;
#if defined(_WIN32)
  size = sizeof(vert_positions);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &pos_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, pos_buffer, 0, (const void*)vert_positions,
                                     size);
  uint32 quad_offset = 6 * sizeof(float2);
#else
  const uint32 num_quads      = countof(vert_positions) / 6;
  const uint32 byte_alignment = 256;
  float2 aligned_positions[num_quads * byte_alignment / sizeof(float2)];
  for (uint32 i = 0; i < num_quads; i++) {
    memcpy(aligned_positions + i * byte_alignment / sizeof(float2), vert_positions + 6 * i,
           6 * sizeof(float2));
  }
  size = (uint32)sizeof(aligned_positions);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &pos_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, pos_buffer, 0,
                                     (const void*)aligned_positions, size);
  uint32 quad_offset = byte_alignment;
  assert(byte_alignment >= 6 * sizeof(float2));
#endif

  dd_graphics_texture_descriptor_t tc = {.width           = RENDER_TEXTURE_DIMENSION / 2,
                                         .height          = RENDER_TEXTURE_DIMENSION / 2,
                                         .layers          = 1,
                                         .mip_level_count = 5,
                                         .format          = R8G8B8A8,
                                         .storage_mode    = SHARED,
                                         .usage           = TEXTURE_USAGE_SHADER_READ};
  dd_graphics_texture_t texture       = g_graphics_api->texture.create(g_graphics_device, &tc);

  uint8 tex_data[RENDER_TEXTURE_DIMENSION / 2 * RENDER_TEXTURE_DIMENSION / 2 * 4];
  dd_rect_t tex_rect = {0, 0, tc.width, tc.height};
  uint8 colors[5][4] = {
      {0xFF, 0, 0, 0xFF},    {0, 0xFF, 0, 0xFF},    {0, 0, 0xFF, 0xFF},
      {0xFF, 0xFF, 0, 0xFF}, {0xFF, 0, 0xFF, 0xFF},
  };
  uint32 offset = 0;
  for (uint32 level = 0; level < 5; ++level) {
    for (uint32 p = 0; p < tex_rect.width * tex_rect.height * 4; p += 4) {
      tex_data[p + 0] = colors[level][0];
      tex_data[p + 1] = colors[level][1];
      tex_data[p + 2] = colors[level][2];
      tex_data[p + 3] = colors[level][3];
    }
    g_graphics_api->texture.set_pixels(g_graphics_device, texture, tex_rect, 0, level,
                                       tex_rect.width * tex_rect.height * 4, tex_data);

    // Fill reference data also
    for (uint32 y = offset; y < offset + tex_rect.height; y++) {
      for (uint32 x = offset; x < offset + tex_rect.width; x++) {
        uint32 row_offset = (RENDER_TEXTURE_DIMENSION - y - 1) * RENDER_TEXTURE_DIMENSION * 4;
        g_reference_data[row_offset + x * 4 + 0] = colors[level][0];
        g_reference_data[row_offset + x * 4 + 1] = colors[level][1];
        g_reference_data[row_offset + x * 4 + 2] = colors[level][2];
        g_reference_data[row_offset + x * 4 + 3] = colors[level][3];
      }
    }

    offset += tex_rect.width;
    tex_rect.width = tex_rect.height = tex_rect.width / 2;
  }

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/textured.vs.spv", allocator);
  const uint8* fs_code =
      read_file("../source/modules/vulkan_api/test/vulkan/textured.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"textured_vs",
    .fs_code       = (const uint8*)"textured_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeStorageBuffer},
                 {.type = BindingTypeTexture}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  for (uint32 i = 0; i < 5; i++) {
    dd_graphics_material_descriptor_t md = {
        .buffers = {{.buffer = pos_buffer, .offset = i * quad_offset, .range = 6 * sizeof(float2)},
                    {.buffer = g_buffer_texcoords, .offset = 0, .range = 6 * sizeof(float2)}},
        .textures = {NULL, NULL, &texture}};

    g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype,
                                                            &md);
    g_graphics_api->commandbuffer.draw(graphics_cmd, 6, 1);
  }

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->buffer.destroy(g_graphics_device, 1, &pos_buffer);
  g_graphics_api->texture.destroy(g_graphics_device, 1, &texture);
  deinit_all_render_resources();
}

TEST_SUITE(textures, test_data) { TEST_ADD_CASE(mipmap, test_data); }

int main(int argc, char** args) {
  dd_prof_thread_init("");

  dd_test_data_t test_d = {0};

  TEST_ADD_SUITE(graphics_colored_squares, &test_d);
  TEST_ADD_SUITE(graphics_draw_types, &test_d);
  TEST_ADD_SUITE(textures, &test_d);
  return dd_perform_tests(&test_d, argc, args);
}
