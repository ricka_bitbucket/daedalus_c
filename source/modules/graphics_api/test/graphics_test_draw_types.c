#include <assert.h>
#include <stdio.h>

#include "../../../examples/metal_render_to_file/stb_image_write.h"
#include "core/containers.h"
#include "core/test.h"
#include "core/types.h"
#include "core/utils.h"
#include "graphics_api/graphics.h"

// TODO: use plugin manager
extern dd_graphics_api_t* g_graphics_api;

#include "graphics_test.inl"
#if defined(_WIN32)
static uint8 global_mem_buffer[50 * 1024];
#endif

extern dd_graphics_device_t g_graphics_device;
extern dd_graphics_render_pass_descriptor_t rpd;
extern dd_graphics_queue_t graphics_queue;
extern dd_graphics_texture_t test_color_render_target;

static dd_graphics_material_prototype_t g_material_prototype = {0};

extern uint8 g_reference_data[RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4];

static uint8 g_draw_type_reference_data[RENDER_TEXTURE_DIMENSION * RENDER_TEXTURE_DIMENSION * 4];

enum { SQUARES_IN_STRIP = 2 };
struct vertex {
  float4 position, color;
};

static void fill_reference_image() {
  uint32 y = 0;
  for (; y < RENDER_TEXTURE_DIMENSION / 4; y++) {
    for (uint32 x = 0; x < RENDER_TEXTURE_DIMENSION; x++) {
      const uint32 pixel_offset =
          ((RENDER_TEXTURE_DIMENSION - y - 1) * RENDER_TEXTURE_DIMENSION + x) * 4;
      g_draw_type_reference_data[pixel_offset + 0] = 255u;
      g_draw_type_reference_data[pixel_offset + 1] = 0;
      g_draw_type_reference_data[pixel_offset + 2] = 0;
      g_draw_type_reference_data[pixel_offset + 3] = 0;
    }
  }
  for (; y < RENDER_TEXTURE_DIMENSION; y++) {
    for (uint32 x = 0; x < RENDER_TEXTURE_DIMENSION; x++) {
      const uint32 pixel_offset =
          ((RENDER_TEXTURE_DIMENSION - y - 1) * RENDER_TEXTURE_DIMENSION + x) * 4;
      g_draw_type_reference_data[pixel_offset + 0] = 0;
      g_draw_type_reference_data[pixel_offset + 1] = 0;
      g_draw_type_reference_data[pixel_offset + 2] = 0;
      g_draw_type_reference_data[pixel_offset + 3] = 0;
    }
  }
}

static void deinit_all_render_resources() {
  g_graphics_api->material_prototype.destroy(g_graphics_device, g_material_prototype);
}

TEST_CASE(draw_triangles) {
  memcpy(g_reference_data, g_draw_type_reference_data, sizeof(g_draw_type_reference_data));
  // clang-format off
  struct vertex triangles_data[] = {
    {{-1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{-1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
  };
  // clang-format on

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  dd_graphics_buffer_t vertex_buffer;
  uint32 size = (uint32)sizeof(triangles_data);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &vertex_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, vertex_buffer, 0,
                                     (const void*)triangles_data, size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_interleaved_vs",
    .fs_code       = (const uint8*)"vertex_color_interleaved_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  dd_graphics_material_descriptor_t md = {
      .buffers = {{.buffer = vertex_buffer, .offset = 0, .range = size}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw(graphics_cmd, 6 * 4, 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->buffer.destroy(g_graphics_device, 1, &vertex_buffer);
  deinit_all_render_resources();
}

TEST_CASE(draw_triangles_indexed_u16) {
  memcpy(g_reference_data, g_draw_type_reference_data, sizeof(g_draw_type_reference_data));
  // clang-format off
  struct vertex triangles_data[] = {
    {{-1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
  };

  uint16 index_data[] = {
    0+0,0+1,0+2,0+1,0+3,0+2,
    4+0,4+1,4+2,4+1,4+3,4+2,
    8+0,8+1,8+2,8+1,8+3,8+2,
    12+0,12+1,12+2,12+1,12+3,12+2,
  };
  // clang-format on

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  dd_graphics_buffer_t vertex_buffer;
  uint32 size = (uint32)sizeof(triangles_data);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &vertex_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, vertex_buffer, 0,
                                     (const void*)triangles_data, size);

  dd_graphics_buffer_t index_buffer;
  size = (uint32)sizeof(index_data);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &index_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, index_buffer, 0, (const void*)index_data,
                                     size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_interleaved_vs",
    .fs_code       = (const uint8*)"vertex_color_interleaved_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  dd_graphics_material_descriptor_t md = {
      .index_buffer = index_buffer,
      .buffers      = {{.buffer = vertex_buffer, .offset = 0, .range = sizeof(triangles_data)}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw_indexed(graphics_cmd, 6 * 4, 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->buffer.destroy(g_graphics_device, 1, &vertex_buffer);
  deinit_all_render_resources();
}

TEST_CASE(draw_triangles_indexed_u32) {
  memcpy(g_reference_data, g_draw_type_reference_data, sizeof(g_draw_type_reference_data));
  // clang-format off
  struct vertex triangles_data[] = {
    {{-1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},

    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
  };

  uint32 index_data[] = {
    0+0,0+1,0+2,0+1,0+3,0+2,
    4+0,4+1,4+2,4+1,4+3,4+2,
    8+0,8+1,8+2,8+1,8+3,8+2,
    12+0,12+1,12+2,12+1,12+3,12+2,
  };
  // clang-format on

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  dd_graphics_buffer_t vertex_buffer;
  uint32 size = (uint32)sizeof(triangles_data);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &vertex_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, vertex_buffer, 0,
                                     (const void*)triangles_data, size);

  dd_graphics_buffer_t index_buffer;
  size = (uint32)sizeof(index_data);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &index_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, index_buffer, 0, (const void*)index_data,
                                     size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_interleaved_vs",
    .fs_code       = (const uint8*)"vertex_color_interleaved_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}}
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  dd_graphics_material_descriptor_t md = {
      .index_buffer = index_buffer,
      .index_type   = IndexTypeUint32,
      .buffers      = {{.buffer = vertex_buffer, .offset = 0, .range = sizeof(triangles_data)}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw_indexed(graphics_cmd, 6 * 4, 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->buffer.destroy(g_graphics_device, 1, &vertex_buffer);
  deinit_all_render_resources();
}

TEST_CASE(draw_triangle_strip) {
  memcpy(g_reference_data, g_draw_type_reference_data, sizeof(g_draw_type_reference_data));
  // clang-format off
  struct vertex triangle_strip_data[] = {
    {{-1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    
    {{-0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{-0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    
    {{ 0.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{ 0.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
    
    {{0.5f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{0.5f, -0.5f, 0.5, 1}, {1,0,0,0}},
    
    {{1.0f, -1.0f, 0.5, 1}, {1,0,0,0}},
    {{1.0f, -0.5f, 0.5, 1}, {1,0,0,0}},
  };
  // clang-format on

  dd_graphics_commandbuffer_t graphics_cmd =
      g_graphics_api->queue.get_command_buffer(graphics_queue);

  g_graphics_api->commandbuffer.begin_render_pass(graphics_cmd, &rpd);
  g_graphics_api->commandbuffer.set_viewport(graphics_cmd, 0, 0, (float)RENDER_TEXTURE_DIMENSION,
                                             (float)RENDER_TEXTURE_DIMENSION, 0, 1);

  dd_graphics_buffer_t vertex_buffer;
  uint32 size = (uint32)sizeof(triangle_strip_data);
  g_graphics_api->buffer.allocate(g_graphics_device, 1, &size, &vertex_buffer);
  g_graphics_api->buffer.update_data(g_graphics_device, vertex_buffer, 0,
                                     (const void*)triangle_strip_data, size);

#if defined(_WIN32)
  dd_allocator* allocator =
      dd_memory_stack_allocator_init(global_mem_buffer, sizeof(global_mem_buffer));

  const uint8* vs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.vs.spv", allocator);
  const uint8* fs_code = read_file(
      "../source/modules/vulkan_api/test/vulkan/vertex_color_interleaved.fs.spv", allocator);
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)vs_code,
    .fs_code       = (const uint8*)fs_code,
    .vs_byte_count = (uint32)array_count(vs_code),
    .fs_byte_count = (uint32)array_count(fs_code),
#else
  dd_graphics_material_prototype_descriptor_t mpd = {
    .vs_code       = (const uint8*)"vertex_color_interleaved_vs",
    .fs_code       = (const uint8*)"vertex_color_interleaved_fs",
    .vs_byte_count = 0,
    .fs_byte_count = 0,
#endif
    .output   = {.color_format = R8G8B8A8},
    .bindings = {{.type = BindingTypeStorageBuffer}},
    .topology = TopologyTypeTriangleStrip
  };
  g_material_prototype = g_graphics_api->material_prototype.create(g_graphics_device, &mpd);

  g_graphics_api->commandbuffer.bind_material_prototype(graphics_cmd, g_material_prototype);

  dd_graphics_material_descriptor_t md = {
      .buffers = {{.buffer = vertex_buffer, .offset = 0, .range = size}}};
  g_graphics_api->commandbuffer.bind_buffers_and_textures(graphics_cmd, g_material_prototype, &md);
  g_graphics_api->commandbuffer.draw(graphics_cmd, countof(triangle_strip_data), 1);

  g_graphics_api->commandbuffer.end_render_pass(graphics_cmd);

  g_graphics_api->queue.submit_command_buffer(graphics_queue, graphics_cmd);

  check_render_output(__FUNCTION__, test_data, RENDER_TEXTURE_DIMENSION);

  g_graphics_api->buffer.destroy(g_graphics_device, 1, &vertex_buffer);
  deinit_all_render_resources();
}

TEST_SUITE(graphics_draw_types, test_data) {
  fill_reference_image();
  TEST_ADD_CASE(draw_triangles, test_data);
  TEST_ADD_CASE(draw_triangles_indexed_u16, test_data);
  TEST_ADD_CASE(draw_triangles_indexed_u32, test_data);
  TEST_ADD_CASE(draw_triangle_strip, test_data);
}
