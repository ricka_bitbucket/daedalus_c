#include "graphics.h"

#if defined(_WIN32)
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
#endif
#include <stdio.h>
#include <stdlib.h>

#include "core/types.h"
#include "core/utils.h"
#include "ui/ui.h"

static uint32 g_width  = 1;
static uint32 g_height = 1;

extern void demo_run();
extern void demo_extern_resize();
extern struct dd_ui_api* ui_api;

dd_graphics_window_t g_window;
static struct dd_allocator* g_window_allocator = NULL;

#if defined(_WIN32)
int32 convert_modifiers(WPARAM wParam) {
  dd_mouse_event_data_t d = {0};
  d.ctrl                  = (wParam & MK_CONTROL) ? 1 : 0;
  d.shift                 = (wParam & MK_SHIFT) ? 1 : 0;

  int32 out;
  out = *(int32*)&d;
  return out;
}

// These values have been found just by testing. Also mentioned on internet, but no exact reference
// was found. Don't know what happens in other languages.
enum {
  WM_CHAR_SELECT_ALL = 1,
  WM_CHAR_COPY  = 3,
  WM_CHAR_PASTE = 22,
  WM_CHAR_CUT   = 24,
};

// MS-Windows event handling function:
LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
  switch (uMsg) {
    case WM_CLOSE:
      // Need to use GetWindowPlacement since GetWindowRect returns size when maximized, rather
      // than normal size. So during quitting, if maximized we would have lost the original size.
      WINDOWPLACEMENT placement = {.length = sizeof(WINDOWPLACEMENT)};
      BOOL res                  = GetWindowPlacement(hWnd, &placement);
      if (res) {
        g_window.position.x = placement.rcNormalPosition.left;
        g_window.position.y = placement.rcNormalPosition.top;
        g_window.size.width = (placement.rcNormalPosition.right - placement.rcNormalPosition.left);
        g_window.size.height =
            (placement.rcNormalPosition.bottom - placement.rcNormalPosition.top);
        g_window.state = (placement.showCmd == SW_MAXIMIZE) ? DD_GRAPHICS_WINDOW_STATE_MAXIMIZED
                                                            : DD_GRAPHICS_WINDOW_STATE_RESTORED;
      } else {
        // Some error getting the placement
      }
      PostQuitMessage(0);
      break;
    case WM_LBUTTONDBLCLK: {
      dd_ui_event_t e = {.event_type = UIEventMouseButton0DoubleClick,
                         .data       = convert_modifiers(wParam)};
      ui_api->interaction.push_event(e, g_window_allocator);
    } break;
    case WM_LBUTTONUP: {
      dd_ui_event_t e = {.event_type = UIEventMouseButton0Up, .data = convert_modifiers(wParam)};
      ui_api->interaction.push_event(e, g_window_allocator);
    } break;
    case WM_LBUTTONDOWN: {
      dd_ui_event_t e = {.event_type = UIEventMouseButton0Down, .data = convert_modifiers(wParam)};
      ui_api->interaction.push_event(e, g_window_allocator);
    } break;
    case WM_RBUTTONUP: {
      dd_ui_event_t e = {.event_type = UIEventMouseButton1Up, .data = convert_modifiers(wParam)};
      ui_api->interaction.push_event(e, g_window_allocator);
    } break;
    case WM_RBUTTONDOWN: {
      dd_ui_event_t e = {.event_type = UIEventMouseButton1Down, .data = convert_modifiers(wParam)};
      ui_api->interaction.push_event(e, g_window_allocator);
    } break;
    case WM_CHAR: {
      switch (wParam) {
        case WM_CHAR_SELECT_ALL: {
          dd_ui_event_t pe = {.event_type = UIEventSelectAll};
          ui_api->interaction.push_event(pe, g_window_allocator);
        } break;
        case WM_CHAR_CUT: {
          dd_ui_event_t pe = {.event_type = UIEventCut};
          ui_api->interaction.push_event(pe, g_window_allocator);
        } break;
        case WM_CHAR_COPY: {
          dd_ui_event_t pe = {.event_type = UIEventCopy};
          ui_api->interaction.push_event(pe, g_window_allocator);
        } break;
        case WM_CHAR_PASTE: {
          dd_ui_event_t pe = {.event_type = UIEventPaste};
          ui_api->interaction.push_event(pe, g_window_allocator);
        } break;
        default: {
          dd_ui_key_event_t e = {.event_type = UIEventKeyInputChar, .key = (int32)wParam};
          if (GetKeyState(VK_SHIFT) & 0x8000) e.modifier = e.modifier | EUIEventKeyModifierShift;
          if (GetKeyState(VK_CONTROL) & 0x8000) e.modifier = e.modifier | EUIEventKeyModifierCtrl;
          ui_api->interaction.push_event(*(dd_ui_event_t*)&e, g_window_allocator);
        }
      }
    } break;
    case WM_MOUSEWHEEL: {
      int32 delta     = GET_WHEEL_DELTA_WPARAM(wParam);
      dd_ui_event_t e = {.event_type = UIEventMouseScroll, .data = delta};
      ui_api->interaction.push_event(e, g_window_allocator);
    } break;
    case WM_MOUSEMOVE: {
      POINTS pos = MAKEPOINTS(lParam);
      ui_api->interaction.update_mouse_pos((int16)pos.x, (int16)pos.y);
    } break;
    case WM_PAINT:
      demo_run();
      break;
    case WM_GETMINMAXINFO:  // set window's minimum size
    {
      POINT p                               = {g_window.minimum_size.x, g_window.minimum_size.y};
      ((MINMAXINFO*)lParam)->ptMinTrackSize = p;

      return 0;
    }
    case WM_ERASEBKGND:
      return 1;

    case WM_SETCURSOR:      
      // To allow programs to change the cursor, need to control this, instead of forwarding to the default handler.
      return 0;

    case WM_SIZE:
      // Resize the application to the new window size, except when
      // it was minimized. Vulkan doesn't support images or swapchains
      // with width=0 and height=0.
      if (wParam != SIZE_MINIMIZED) {
        uint32 new_width  = (uint32)(lParam & 0xffff);
        uint32 new_height = (uint32)((lParam & 0xffff0000) >> 16);
        if (new_width != g_width || new_height != g_width) {
          g_width  = new_width;
          g_height = new_height;

          demo_extern_resize(new_width, new_height);
        }
      }
      break;
    case WM_KEYDOWN: {
      dd_ui_key_event_t e = {.event_type = UIEventKeyInputNonChar, .key = (int32)wParam};
      if (GetKeyState(VK_SHIFT) & 0x8000) e.modifier = e.modifier | EUIEventKeyModifierShift;
      if (GetKeyState(VK_CONTROL) & 0x8000) e.modifier = e.modifier | EUIEventKeyModifierCtrl;
      ui_api->interaction.push_event(*(dd_ui_event_t*)&e, g_window_allocator);
      switch (wParam) {
        case VK_ESCAPE:
          // PostQuitMessage(0);
          break;
      }
      return 0;
    }
    default:
      break;
  }
  return (DefWindowProc(hWnd, uMsg, wParam, lParam));
}

dd_graphics_window_t dd_create_window(void* in_handle, const char* name, uint32 left, uint32 top,
                                      uint32 width, uint32 height, uint32 minimum_width,
                                      uint32 minimum_height, uint32 flags,
                                      struct dd_allocator* allocator) {
  g_window_allocator = allocator;
  g_width            = width;
  g_height           = height;
  WNDCLASSEX win_class;
  static_assert(sizeof(HINSTANCE) <= sizeof(in_handle), "HINSTANCE doesn't fit in handle");
  HINSTANCE hInstance = *(HINSTANCE*)&in_handle;

  // Initialize the window class structure:
  win_class.cbSize        = sizeof(WNDCLASSEX);
  win_class.style         = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
  win_class.lpfnWndProc   = (WNDPROC)WndProc;
  win_class.cbClsExtra    = 0;
  win_class.cbWndExtra    = 0;
  win_class.hInstance     = hInstance;
  win_class.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
  win_class.hCursor       = LoadCursor(NULL, IDC_ARROW);
  win_class.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
  win_class.lpszMenuName  = NULL;
  win_class.lpszClassName = name;
  win_class.hIconSm       = LoadIcon(NULL, IDI_WINLOGO);
  // Register window class:
  if (!RegisterClassEx(&win_class)) {
    // It didn't work, so try to give a useful error:
    char err[256];
    memset(err, 0, 256);
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), err, 255, NULL);
    dd_devprintf("Unexpected error trying to start the application (RegisterClassEx:%s)!\n", err);
    fflush(stdout);
    exit(1);
  }
  // Create window with the registered class:
  RECT wr = {.left = left, .top = top, .right = left + width, .bottom = top + height};
  if (flags & DD_WINDOW_FLAGS_ADJUST_SIZE) {
    // Adjust the size to account for window chrome. However, some applications store their window
    // size which will already include the chrome.
    AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);
  }
  DWORD style = WS_OVERLAPPEDWINDOW |  // window style
                WS_VISIBLE | WS_SYSMENU;

  // Even though we maximize the window later, need this to ensure the content of the window is
  // also maximized.
  if (flags & DD_WINDOW_FLAGS_MAXIMIZE) {
    style = style | WS_MAXIMIZE;
  }
  HWND window = CreateWindowEx(0,
                               name,                // class name
                               name,                // app name
                               style, 0, 0,         // x/y coords
                               wr.right - wr.left,  // width
                               wr.bottom - wr.top,  // height
                               NULL,                // handle to parent
                               NULL,                // handle to menu
                               hInstance, NULL);    // no extra parameters
  if (!window) {
    // It didn't work, so try to give a useful error:
    printf("Cannot create a window in which to draw!\n");
    fflush(stdout);
    exit(1);
  } else {
    RECT rect;
    if (GetWindowRect(window, &rect)) {
      width  = rect.right - rect.left;
      height = rect.bottom - rect.top;
    }
  }

  // Need to use SetWindowPlacement to get the location and maximization correct.
  WINDOWPLACEMENT placement = {
      .rcNormalPosition = wr,
      .length           = sizeof(WINDOWPLACEMENT),
      .showCmd          = (flags & DD_WINDOW_FLAGS_MAXIMIZE) ? SW_MAXIMIZE : SW_NORMAL};
  SetWindowPlacement(window, &placement);

  // Window client area size must be at least 1 pixel high, to prevent crash.
  dd_graphics_window_t w = {.instance     = hInstance,
                            .handle       = window,
                            .size         = {.width = width, .height = height},
                            .minimum_size = {.x = GetSystemMetrics(SM_CXMINTRACK),
                                             .y = (GetSystemMetrics(SM_CYMINTRACK) + 1)}};
  g_window               = w;
  return w;
}
#else

dd_graphics_window_t dd_create_window(void* in_handle, const char* name, uint32 left, uint32 top,
                                      uint32 width, uint32 height, uint32 minimum_width,
                                      uint32 minimum_height, uint32 flags,
                                      struct dd_allocator* allocator) {
  // TODO handle the flags
  (void)flags;

  g_window_allocator = allocator;

  g_width  = width;
  g_height = height;
  // Window client area size must be at least 1 pixel high, to prevent crash.
  dd_graphics_window_t w = {.instance     = 0,
                            .handle       = 0,
                            .size         = {.x = width, .y = height},
                            .minimum_size = {.x = 1, .y = 1}};
  g_window               = w;
  return w;
}

#endif
