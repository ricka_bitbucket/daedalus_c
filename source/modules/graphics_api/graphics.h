#pragma once

#include "core/types.h"

/*
- Do you want to present or not?
- Do you want to use compute or graphics (or both)

For helper windows implementation provide callbacks for
- format changed
- size changed
- draw


Both Metal and Vulkan can specialize shaders
  Vulkan: VkSpecializationInfo
  Metal: MTLFunctionConstantValues
*/
enum { GRAPHICS_MAX_COLOR_ATTACHMENTS = 4 };

typedef struct dd_allocator dd_allocator;

typedef enum ECreateWindowFlags {
  DD_WINDOW_FLAGS_ADJUST_SIZE = 1 << 0,
  DD_WINDOW_FLAGS_MAXIMIZE    = 1 << 1,
} ECreateWindowFlags;

typedef enum EQueueType {
  QueueTypePresent  = 1 << 0,
  QueueTypeGraphics = 1 << 1,
  QueueTypeCompute  = 1 << 2,
  QueueTypeTransfer = 1 << 3
} EQueueType;

typedef enum EPixelFormat {
  UNDEFINED = 0,

  // 32-bit colors
  R8G8B8A8,
  B8G8R8A8,

  // Compressed,
  BC1_RGBA_UNORM,

  // Depth only
  DEPTH_U16_NORMALIZED,

  PIXEL_FORMAT_MAX
} EPixelFormat;

typedef enum EStorageMode { SHARED = 0, DEVICE_LOCAL, STORAGE_MODE_MAX } EStorageMode;

typedef enum ETextureUsageFlags {
  TEXTURE_USAGE_SHADER_READ          = 1 << 0,  // Shader reads or samples texture
  TEXTURE_USAGE_SHADER_WRITE         = 1 << 1,  // Shader writes to texture explicitly
  TEXTURE_USAGE_RENDER_TARGET        = 1 << 2,  // Pipeline renders to this texture
  TEXTURE_USAGE_TRANSFER_SOURCE      = 1 << 3,  // Texture is used as source for copy operation
  TEXTURE_USAGE_TRANSFER_DESTINATION = 1 << 4  // Texture is used as destination for copy operation
} ETextureUsageFlags;

typedef struct dd_graphics_texture_descriptor_t {
  EPixelFormat format;
  uint32 width;
  uint32 height;
  uint32 layers;
  uint32 mip_level_count;
  EStorageMode storage_mode;
  ETextureUsageFlags usage;
} dd_graphics_texture_descriptor_t;

enum {
  DD_GRAPHICS_WINDOW_STATE_RESTORED,
  DD_GRAPHICS_WINDOW_STATE_MINIMIZED,
  DD_GRAPHICS_WINDOW_STATE_MAXIMIZED
};

typedef struct dd_graphics_window_t {
  void* instance; /* platform specific */
  void* handle;   /* platform specific */
  struct {
    uint32 x, y;
  } minimum_size;
  struct {
    int32 x, y;  // Can be negative if left of main monitor
  } position;
  struct {
    uint32 width, height;
  } size;
  uint32 state; /* minimized/maximized/restored */
} dd_graphics_window_t;

typedef void* dd_graphics_device_t;
typedef struct dd_graphics_queue_t {
  void* ptr;
} dd_graphics_queue_t;
typedef struct dd_graphics_commandbuffer_t {
  void* ptr;
} dd_graphics_commandbuffer_t;
typedef void* dd_graphics_buffer_t;
typedef void* dd_graphics_texture_t;
typedef void* dd_graphics_material_prototype_t;
typedef void* dd_graphics_compute_prototype_t;
typedef void* dd_graphics_render_pass_t;

typedef struct dd_graphics_render_pass_descriptor_t {
  dd_graphics_texture_t color[GRAPHICS_MAX_COLOR_ATTACHMENTS];
  dd_graphics_texture_t depth;
  uint32 width, height;

  float clear_color[4];

#ifdef __APPLE__
  void* mtl_render_pass_descriptor;
#else
  bool is_for_present;
  // A hash is created from this struct, and without explicit padding the automatic padding isn't
  // initialized.
  bool padding[7];
#endif
} dd_graphics_render_pass_descriptor_t;

typedef enum EMaterialPrototypeBindingType {
  BindingTypeUnknown = 0,
  BindingTypeUniformBuffer,
  BindingTypeStorageBuffer,
  BindingTypeTexture,
  BindingTypePushConstant,

  BindingTypeCount
} EMaterialPrototypeBindingType;

typedef enum EShaderType {
  ShaderTypeVertex = 0,
  ShaderTypeFragment,
  ShaderTypeCompute,

  ShaderTypeMax
} EShaderType;

typedef enum ETopologyType {
  TopologyTypeTriangles = 0,  // Set the default to value 0
  TopologyTypePoints,
  TopologyTypeLines,
  TopologyTypeLineStrip,
  TopologyTypeTriangleStrip
} ETopologyType;

typedef enum EIndexType { IndexTypeUint16 = 0, IndexTypeUint32 } EIndexType;

/* Contains all info needed to create a material prototype. You'll need to create a material
 * instance to supply the specific data
 */
enum { MaterialPrototypeMaxBindingCount = 16 };
typedef struct dd_graphics_material_prototype_binding_t {
  enum EMaterialPrototypeBindingType type;
  union {
    struct {
      uint32 count;
    } array;
    struct {
      uint32 offset;
      uint32 range;
    } push_constant;
  } props;
} dd_graphics_material_prototype_binding_t;

typedef struct dd_graphics_material_prototype_descriptor_t {
  const uint8* vs_code;
  const uint8* fs_code;
  uint32 vs_byte_count;
  uint32 fs_byte_count;

  dd_graphics_material_prototype_binding_t bindings[MaterialPrototypeMaxBindingCount];

  struct {
    EPixelFormat color_format;
    EPixelFormat depth_format;
  } output;

  ETopologyType topology;
  bool enable_alpha_blend;
} dd_graphics_material_prototype_descriptor_t;

typedef struct dd_graphics_compute_prototype_descriptor_t {
  const uint8* compute_shader_data;
  uint32 byte_count;

  dd_graphics_material_prototype_binding_t bindings[MaterialPrototypeMaxBindingCount];
} dd_graphics_compute_prototype_descriptor_t;

typedef struct dd_rect_t {
  uint32 x, y, width, height;
} dd_rect_t;

typedef struct dd_draw_indirect_command_t {
  uint32 vertex_count;
  uint32 instance_count;
  uint32 vertex_offset;
  uint32 instance_offset;
} dd_draw_indirect_command_t;

typedef struct dd_draw_indexed_indirect_command_t {
  uint32 index_count;
  uint32 instance_count;
  uint32 index_offset;
  int32 vertex_offset;
  uint32 instance_offset;
} dd_draw_indexed_indirect_command_t;

/* Contains all info needed to bind buffers
 */
typedef struct dd_graphics_buffer_binding_t {
  dd_graphics_buffer_t buffer;
  uint32 offset;
  uint32 range;
} dd_graphics_buffer_binding_t;

typedef struct dd_graphics_material_descriptor_t {
  dd_graphics_texture_t* textures[MaterialPrototypeMaxBindingCount];
  dd_graphics_buffer_binding_t buffers[MaterialPrototypeMaxBindingCount];
  dd_graphics_buffer_binding_t index_buffer;
  EIndexType index_type;  // Only valid if index_buffer is also set
} dd_graphics_material_descriptor_t;

typedef struct dd_graphics_api_t {
  dd_graphics_window_t (*create_window)(const char* name, void* handle);
  dd_graphics_device_t (*create_default_device)(dd_allocator* allocator);
  dd_graphics_device_t (*get_device)();

  struct {
    void (*wait_until_idle)(dd_graphics_device_t in_device);
    dd_graphics_queue_t (*get_queue)(dd_graphics_device_t in_device, EQueueType in_type);
  } device;

  struct {
    dd_graphics_commandbuffer_t (*get_command_buffer)(dd_graphics_queue_t in_queue);
    void (*submit_command_buffer)(dd_graphics_queue_t in_queue,
                                  dd_graphics_commandbuffer_t in_buffer);
  } queue;

  struct {
    void (*allocate)(dd_graphics_device_t device, uint32 count, const uint32* in_sizes,
                     dd_graphics_buffer_t* out_buffers);
    void (*destroy)(dd_graphics_device_t device, uint32 count, dd_graphics_buffer_t* in_buffers);
    void (*update_data)(dd_graphics_device_t device, dd_graphics_buffer_t buffer,
                        uint32 in_offset_byte_count, const void* data, uint32 num_bytes);

    void (*copy)(dd_graphics_commandbuffer_t in_commandbuffer,
                 const dd_graphics_buffer_t src_buffer, uint32 src_offset_bytes,
                 dd_graphics_buffer_t dst_buffer, uint32 dst_offset_bytes, uint32 byte_count);
  } buffer;

  struct {
    dd_graphics_texture_t (*create)(dd_graphics_device_t in_device,
                                    const dd_graphics_texture_descriptor_t* in_texture_descriptor);
    void (*destroy)(dd_graphics_device_t device, const uint32 count,
                    dd_graphics_texture_t* in_textures);
#if 0
    // The following functions cannot currently be done in Metal (2020-06-20)
    // They do give more explicit control in Vulkan, as the set_pixels/get_pixels just map memory
    // under the hood.
    uint8* (*map_memory)(dd_graphics_device_t device, dd_graphics_texture_t in_texture);
    void (*unmap_memory)(dd_graphics_device_t device, dd_graphics_texture_t in_texture);
#endif
    void (*set_pixels)(dd_graphics_device_t device, dd_graphics_texture_t in_texture,
                       dd_rect_t in_rect, uint32 in_layer_index, uint32 in_mip_map_level,
                       uint32 bytes_count, uint8* in_data);
    void (*get_pixels)(dd_graphics_device_t device, dd_graphics_texture_t in_texture,
                       dd_rect_t in_rect, uint32 in_mip_map_level, uint8* out_data);
  } texture;

  struct {
    dd_graphics_material_prototype_t (*create)(
        dd_graphics_device_t in_device,
        const dd_graphics_material_prototype_descriptor_t* in_material_prototype_descriptor);
    void (*destroy)(dd_graphics_device_t in_device,
                    dd_graphics_material_prototype_t in_material_prototype);
  } material_prototype;

  struct {
    dd_graphics_compute_prototype_t (*create)(
        dd_graphics_device_t in_device,
        const dd_graphics_compute_prototype_descriptor_t* in_prototype_descriptor);
    void (*destroy)(dd_graphics_device_t in_device, dd_graphics_compute_prototype_t in_prototype);
  } compute_prototype;

  struct {
    void (*begin_render_pass)(
        dd_graphics_commandbuffer_t in_commandbuffer,
        const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor);
    void (*end_render_pass)(dd_graphics_commandbuffer_t in_commandbuffer);

    // TODO(Rick): Not sure need these
    void (*begin_compute)(dd_graphics_commandbuffer_t in_commandbuffer);
    void (*end_compute)(dd_graphics_commandbuffer_t in_commandbuffer);
    // TODO(Rick): Not sure need these

    void (*set_viewport)(dd_graphics_commandbuffer_t cmd_qin_commandbufferueue, float origin_x,
                         float origin_y, float width, float height, float z_near, float z_far);
    void (*bind_material_prototype)(dd_graphics_commandbuffer_t in_commandbuffer,
                                    dd_graphics_material_prototype_t in_material_prototype);
    void (*bind_buffers_and_textures)(dd_graphics_commandbuffer_t in_commandbuffer,
                                      dd_graphics_material_prototype_t in_material_prototype,
                                      const dd_graphics_material_descriptor_t* in_material);
    void (*push_constants)(dd_graphics_commandbuffer_t in_commandbuffer, uint32 bytes_count,
                           void* values);
    void (*draw)(dd_graphics_commandbuffer_t in_commandbuffer, const uint32 in_primitive_count,
                 const uint32 in_instance_count);
    void (*draw_indirect)(dd_graphics_commandbuffer_t in_commandbuffer,
                          dd_graphics_buffer_t buffer, uint32 offset_bytes, uint32 draw_count,
                          uint32 stride_bytes);
    void (*draw_with_offsets)(dd_graphics_commandbuffer_t in_commandbuffer,
                              const uint32 in_primitive_count, const uint32 in_primitive_base,
                              const uint32 in_instance_count, const uint32 in_instance_base);
    void (*draw_indexed)(dd_graphics_commandbuffer_t in_commandbuffer, const uint32 in_index_count,
                         const uint32 in_instance_count);
    void (*draw_indexed_with_offsets)(dd_graphics_commandbuffer_t in_commandbuffer,
                                      const uint32 in_index_count, const uint32 in_index_base,
                                      const uint32 in_vertex_base, const uint32 in_instance_count,
                                      const uint32 in_instance_base);

    void (*dispatch)(dd_graphics_commandbuffer_t in_commandbuffer, const uint32 in_group_count_x,
                     const uint32 in_group_count_y, const uint32 in_group_count_z);
  } commandbuffer;
} dd_graphics_api_t;

typedef void (*dd_graphics_api_helper_draw_callback)(
    dd_graphics_commandbuffer_t draw_commandbuffer,
    const dd_graphics_render_pass_descriptor_t* in_render_pass_descriptor);

typedef void (*dd_graphics_api_helper_compute_callback)(
    dd_graphics_commandbuffer_t compute_commandbuffer);

dd_graphics_window_t dd_create_window(void* in_handle, const char* name, uint32 left, uint32 top,
                                      uint32 width, uint32 height, uint32 minimum_width,
                                      uint32 minimum_height, uint32 flags,
                                      dd_allocator* allocator);
