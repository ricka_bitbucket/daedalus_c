#include "ui.h"

#include <assert.h>
#include <math.h>
#include <string.h>

#include "core/api_registry.h"
#include "core/containers.h"
#include "core/dd_math.h"
#include "core/memory.h"
#include "core/timer.h"
#include "core/utils.h"
#include "os/os.h"

static struct {
  dd_ui_event_t* data;             /* array */
  uint32 offset_begin, offset_end; /* contents are between start and end */
} event_ringbuffer;

static_assert(sizeof(dd_ui_event_t) == sizeof(dd_ui_key_event_t),
              "Event struct sizes aren't equal");

// Initialize mouse coordinates to be offscreen
static dd_mouse_t g_mouse = {.x = -1, .y = -1};

struct dd_draw2d_api* g_draw2d_api_ = NULL;
struct dd_os_api* g_os_api_         = NULL;

// Function declarations
// ====================
static void ui_scrollview_begin(dd_ui_t* ui, const dd_ui_scrollview_t* view,
                                float* in_out_scroll_x, float* in_out_scroll_y,
                                ui_rect_t* out_content_rect);
static void ui_scrollview_end(dd_ui_t* ui, const dd_ui_scrollview_t* view, float* in_out_scroll_x,
                              float* in_out_scroll_y);
static void ui_interaction_tab_focus_register(dd_ui_t* ui, uint32 id);

// Function definitions
// ====================
static void ui_clear_event(dd_ui_t* ui) {
  ui->event.event_type = UIEventNone;
  ui->event.data       = 0;
}

static void ui_set_next_hover(dd_ui_t* ui, uint32 next_hover_layer, uint32 next_hover_id) {
  /*
  Can only override the next_hover_id if the next_hover_layer is equal or lower.
  This way later rows in a table for example won't overwrite a dropdown in an earlier row.
  */
  if (next_hover_layer >= ui->next_hover_layer) {
    ui->next_hover_layer = next_hover_layer;
    ui->next_hover_id    = next_hover_id;
  }
}

static void ui_push_event(dd_ui_event_t e, dd_allocator* allocator) {
  uint32 capacity = (uint32)array_capacity(event_ringbuffer.data);
  uint32 oe       = event_ringbuffer.offset_end;
  uint32 ob       = event_ringbuffer.offset_begin;

  if ((capacity == 0) || ((oe + 1) % capacity == ob)) {
    // There is no space, so allocate more
    uint32 new_capacity     = capacity ? capacity * 2 : 10;
    dd_ui_event_t* new_data = {0};
    array_reserve(new_data, new_capacity, allocator);
    // Take this chance to also reorder
    if (ob > oe) {
      uint32 count_at_end = capacity - ob;
      memcpy(new_data, event_ringbuffer.data + event_ringbuffer.offset_begin,
             sizeof(*new_data) * count_at_end);
      memcpy(new_data + count_at_end, event_ringbuffer.data, sizeof(*new_data) * oe);
      ob = event_ringbuffer.offset_begin = 0;
      oe = event_ringbuffer.offset_end = capacity;
      array_free(event_ringbuffer.data, allocator);
      capacity              = new_capacity;
      event_ringbuffer.data = new_data;
    } else {
      if (event_ringbuffer.data) {
        memcpy(new_data, event_ringbuffer.data + event_ringbuffer.offset_begin,
               sizeof(*new_data) * (oe - ob));
        array_free(event_ringbuffer.data, allocator);
      }
      capacity              = new_capacity;
      event_ringbuffer.data = new_data;
    }
  }

  event_ringbuffer.data[oe]   = e;
  event_ringbuffer.offset_end = (oe + 1) % capacity;
}

static dd_ui_event_t ui_pop_event() {
  uint32 capacity = (uint32)array_capacity(event_ringbuffer.data);
  uint32 oe       = event_ringbuffer.offset_end;
  uint32 ob       = event_ringbuffer.offset_begin;

  dd_ui_event_t out = {0};

  if (oe != ob) {
    out                           = event_ringbuffer.data[ob];
    event_ringbuffer.offset_begin = (ob + 1) % capacity;
  }

  return out;
}

static void ui_update_mouse_pos(int16 x, int16 y) {
  g_mouse.dx = x - g_mouse.x;
  g_mouse.dy = y - g_mouse.y;
  g_mouse.x  = x;
  g_mouse.y  = y;
}

static void ui_begin(dd_ui_t* ui, const dd_ui_style_t* theme, dd_ui_event_t event) {
  ui->theme = theme;
  ui->event = event;

  g_mouse.down_prev = g_mouse.down;
  if (ui->event.event_type == UIEventMouseButton0Down) {
    g_mouse.down = true;
  }
  if (ui->event.event_type == UIEventMouseButton0Up) {
    g_mouse.down = false;
  }
  ui->mouse  = g_mouse;
  g_mouse.dx = g_mouse.dy = 0;

  if (!ui->hover_id_locked) {
    ui->hover_id            = ui->next_hover_id;
    ui->current_hover_layer = ui->next_hover_layer;
    ui->next_hover_layer    = 0;
    ui->next_hover_id       = 0;
  }

  array_reset(ui->draw_data.primitive_buffer);
  array_reset(ui->quads_normal);
  array_reset(ui->overlay_quads);
  ui->draw_data.quads = ui->quads_normal;

  // If nothing changes the mouse cursor, we want the default type.
  ui->out.cursor_type = ECursorTypeDefault;
}

static const dd_draw2d_t* ui_end(dd_ui_t* ui) {
  // If nothing is selected, then nothing will actively check for the tab stop, so handle that case
  // here.
  if (ui->event.event_type == UIEventKeyInputNonChar) {
    dd_ui_key_event_t e = *(dd_ui_key_event_t*)&(ui->event);
    if (e.key == '\t') {
      if (e.modifier & EUIEventKeyModifierShift)
        ui->active_id = ui->tab_focus.previous_control_id;
      else
        ui->tab_focus.focus_on_next = true;
    }
    ui_clear_event(ui);
  }

  memset(&ui->event, 0, sizeof(ui->event));

  ui->quads_normal = ui->draw_data.quads;

  const uint32 quads_overlay_count = (uint32)array_count(ui->overlay_quads);
  if (quads_overlay_count > 0) {
    array_append(ui->quads_normal, ui->overlay_quads, quads_overlay_count,
                 ui->draw_data.allocator);
  }

  ui->draw_data.quads = ui->quads_normal;

  {
    static dd_cursor_t previous_cursor = ECursorTypeDefault;
    if (ui->out.cursor_type != previous_cursor) {
      g_os_api_->window.set_cursor(ui->out.cursor_type);

      previous_cursor = ui->out.cursor_type;
    }
  }

  return &(ui->draw_data);
}

static void ui_label(dd_ui_t* ui, ui_rect_t rect, color_rgba8_t color, ETextAligment align,
                     const dd_stringview_t text) {
  g_draw2d_api_->text(&(ui->draw_data), rect, color, align, text);
  // g_draw2d_api_->text(&(ui->draw_data), rect, DD_RGB(0, 123, 123), align, text);
}

static void ui_pane(dd_ui_t* ui, ui_rect_t rect, color_rgba8_t color) {
  g_draw2d_api_->rect(&(ui->draw_data), rect, color);
}

static bool ui_button(dd_ui_t* ui, const dd_ui_button_t* button) {
  if (!button->is_disabled) {
    if (rect_contains_point(button->rect, ui->mouse.x, ui->mouse.y)) {
      ui_set_next_hover(ui, ui->current_ui_layer, button->id);

      if (ui->mouse.down & MouseButtonLeft) {
        g_draw2d_api_->rect(&ui->draw_data, button->rect, ui->theme->button.down);
        ui->active_id = button->id;
      } else {
        g_draw2d_api_->rect(&ui->draw_data, button->rect, ui->theme->button.hover);
      }

      if (ui->event.event_type == UIEventMouseButton0Up) {
        ui_clear_event(ui);
        return true;
      }
    } else {
      g_draw2d_api_->rect(&ui->draw_data, button->rect, ui->theme->button.color);
    }

    ui_interaction_tab_focus_register(ui, button->id);
  } else {
    g_draw2d_api_->rect(&ui->draw_data, button->rect, ui->theme->background.color);
  }

  const bool is_active = ui->active_id == button->id;
  if (is_active) {
    g_draw2d_api_->rect_stroke(&ui->draw_data, button->rect, 1, ui->theme->foreground.color);
  }
  g_draw2d_api_->text(&ui->draw_data, rect_shrink(button->rect, 2), ui->theme->foreground.color,
                      TextAlignmentCenter, button->label);

  if (ui->active_id == button->id) {
    if (ui->event.event_type == UIEventKeyInputChar) {
      dd_ui_key_event_t e = *(dd_ui_key_event_t*)&(ui->event);
      if ((e.key == DD_KEY_RETURN) || (e.key == ' ')) {
        ui_clear_event(ui);
        return true;
      }
    }
  }

  return false;
}

static bool ui_checkbox(dd_ui_t* ui, ui_rect_t rect, const dd_stringview_t label, uint32 id,
                        bool* in_out_is_checked) {
  bool is_changed = false;

  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_set_next_hover(ui, ui->current_ui_layer, id);

    if (ui->event.event_type == UIEventMouseButton0Up) {
      *in_out_is_checked = !*in_out_is_checked;
      ui_clear_event(ui);
      is_changed = true;
    }
    g_draw2d_api_->rect(&ui->draw_data, rect, (color_rgba8_t){255, 255, 0, 255});

  } else {
    g_draw2d_api_->rect(&ui->draw_data, rect, (color_rgba8_t){0, 255, 0, 255});
  }

  const bool is_active         = ui->active_id == id;
  const float border_thickness = is_active ? 2.f : 1.f;
  g_draw2d_api_->rect(&ui->draw_data, rect_shrink(rect, border_thickness),
                      ui->theme->background.color);

  if (*in_out_is_checked) {
    g_draw2d_api_->rect(&ui->draw_data, rect_shrink(rect, 3), (color_rgba8_t){0, 255, 0, 255});
  }

  rect.x += rect.width;
  g_draw2d_api_->text(&ui->draw_data, rect_shrink(rect, border_thickness),
                      ui->theme->foreground.color, TextAlignmentLeft, label);

  ui_interaction_tab_focus_register(ui, id);

  if (is_active) {
    if (ui->event.event_type == UIEventKeyInputChar) {
      dd_ui_key_event_t e = *(dd_ui_key_event_t*)&(ui->event);
      if ((e.key == DD_KEY_RETURN) || (e.key == ' ')) {
        *in_out_is_checked = !*in_out_is_checked;
        ui_clear_event(ui);
        is_changed = true;
      }
    }
  }

  return is_changed;
}

static bool ui_inputfield(dd_ui_t* ui, const dd_ui_inputfield_t* inputfield, char* text,
                          uint32 max_chars) {
  ui_rect_t rect       = inputfield->rect;
  uint32 id            = inputfield->id;
  const bool is_active = (ui->active_id == id);

  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_set_next_hover(ui, ui->current_ui_layer, id);
    ui->out.cursor_type = ECursorTypeIBeam;
  }

  const uint32 char_width = 5;

  static int32 caret_position                   = 0;
  static int32 caret_position_selection_started = -1;
  static uint32 last_active_inputfield_id       = 0;

  bool is_changed              = false;
  const float border_thickness = is_active ? 2.f : 1.f;
  const ui_rect_t r_border     = rect;
  const ui_rect_t r_background = rect_shrink(rect, border_thickness);
  const ui_rect_t r_text       = rect_shrink(r_background, 1);
  g_draw2d_api_->rect_stroke(&ui->draw_data, r_border, border_thickness,
                             ui->theme->foreground.color);
  g_draw2d_api_->rect(&ui->draw_data, r_background, ui->theme->background.color);
  if ((caret_position_selection_started != -1) && (ui->active_id == id)) {
    ui_rect_t selected_rect = r_text;
    selected_rect.x +=
        (char_width + 1) * (float)min(caret_position_selection_started, caret_position) - 1;
    selected_rect.width =
        (char_width + 1) * (float)(max(caret_position_selection_started, caret_position) -
                                   min(caret_position_selection_started, caret_position));
    g_draw2d_api_->rect(&ui->draw_data, selected_rect, ui->theme->button.hover);
  }
  dd_stringview_t render_text = stringview(text);
  size_t max_visible_chars    = (size_t)(r_text.width / (char_width + 1));
  render_text.length =
      render_text.length < max_visible_chars ? render_text.length : max_visible_chars;
  g_draw2d_api_->text(&ui->draw_data, r_text, ui->theme->foreground.color, TextAlignmentLeft,
                      render_text);

  if (ui->active_id == id) {
    ui_rect_t r_caret = r_text;
    r_caret.width     = 1;
    r_caret.height += 2;
    r_caret.y -= 1;
    // Caret is located in center of glyph, so offset left slightly
    int32 offset = ((int32)(caret_position * (char_width + 1)) - 1);
    r_caret.x += offset;

    uint64 t__s = dd_time_microseconds() / 500000;
    if (t__s & 1) {
      g_draw2d_api_->rect(&ui->draw_data, r_caret, ui->theme->foreground.color);
    }
  }

  if (ui->hover_id == id) {
    if (ui->event.event_type == UIEventMouseButton0Up) {
      ui->active_id = id;
    }
  }

  ui_interaction_tab_focus_register(ui, inputfield->id);

  if (ui->active_id == id) {
    if (last_active_inputfield_id != id) {
      // TODO: need to reset when something else becomes active
      // When switching input fields, reset caret position
      last_active_inputfield_id = id;
      caret_position            = 0;
    }

    int32 len = (int32)strlen(text);

    if (ui->event.event_type == UIEventMouseButton0Up) {
      float offset = ui->mouse.x - r_text.x;
      if (offset > 0) {
        caret_position = min(len, (int32)((offset + char_width / 2) / (char_width + 1)));
      }
    }

    caret_position = min(len, caret_position);

    // Process input key
    if (ui->event.event_type == UIEventKeyInputNonChar) {
      dd_ui_key_event_t key_event = *(dd_ui_key_event_t*)&ui->event;

      if (caret_position_selection_started == -1 &&
          (key_event.modifier & EUIEventKeyModifierShift) != 0) {
        caret_position_selection_started = caret_position;
      }

      switch (key_event.key) {
        case DD_KEY_LEFT:
          caret_position = max(0, caret_position - 1);
          if ((key_event.modifier & EUIEventKeyModifierShift) == 0) {
            caret_position_selection_started = -1;
          }
          ui_clear_event(ui);
          break;
        case DD_KEY_RIGHT: {
          caret_position = min(len, caret_position + 1);
          if ((key_event.modifier & EUIEventKeyModifierShift) == 0) {
            caret_position_selection_started = -1;
          }
          ui_clear_event(ui);
        } break;
        case DD_KEY_HOME:
          caret_position = 0;
          if ((key_event.modifier & EUIEventKeyModifierShift) == 0) {
            caret_position_selection_started = -1;
          }
          ui_clear_event(ui);
          break;
        case DD_KEY_END:
          caret_position = len;
          if ((key_event.modifier & EUIEventKeyModifierShift) == 0) {
            caret_position_selection_started = -1;
          }
          ui_clear_event(ui);
          break;
        case DD_KEY_DELETE: {
          int32 begin_delete = caret_position;
          int32 end_delete   = caret_position + 1;
          if (caret_position_selection_started != -1) {
            begin_delete = min(caret_position, caret_position_selection_started);
            end_delete   = max(caret_position, caret_position_selection_started);
            caret_position_selection_started = -1;
          }
          if ((begin_delete != end_delete) && (end_delete <= len)) {
            memmove(text + begin_delete, text + end_delete, len - end_delete);
            len -= (end_delete - begin_delete);
            text[len] = 0;
          }

          is_changed = true;
          ui_clear_event(ui);
        } break;
        case DD_KEY_RETURN:
          is_changed                       = true;
          caret_position_selection_started = -1;
          // Don't clear this event, so the caller can act on it
          break;
        case DD_KEY_BACKSPACE: {
          int32 begin_delete = max(0, caret_position - 1);
          int32 end_delete   = caret_position;
          if (caret_position_selection_started != -1) {
            begin_delete = min(caret_position, caret_position_selection_started);
            end_delete   = max(caret_position, caret_position_selection_started);
            caret_position_selection_started = -1;
          }
          if (begin_delete != end_delete) {
            memmove(text + begin_delete, text + end_delete, len - end_delete);
            len -= (end_delete - begin_delete);
            text[len] = 0;
          }

          caret_position = begin_delete;
          is_changed     = true;
          ui_clear_event(ui);
        } break;
      }
    } else if (ui->event.event_type == UIEventKeyInputChar) {
      dd_ui_key_event_t e = *(dd_ui_key_event_t*)&ui->event;
      if ((e.key == '\t') || (e.key == '\r') || (e.key == '\n') || (e.key == DD_KEY_BACKSPACE)) {
        // Do nothing
      } else if ((len + 1 + 1) <= (int)max_chars) {
        if (caret_position_selection_started != -1) {
          int32 begin_delete               = min(caret_position, caret_position_selection_started);
          int32 end_delete                 = max(caret_position, caret_position_selection_started);
          caret_position                   = begin_delete;
          caret_position_selection_started = -1;

          if (begin_delete != end_delete) {
            memmove(text + begin_delete, text + end_delete, len - end_delete);
            len -= (end_delete - begin_delete);
            text[len] = 0;
          }
        }

        // Add a character to the buffer
        memmove(text + caret_position + 1, text + caret_position, len - caret_position + 1);
        text[caret_position++] = (char)e.key;
        is_changed             = true;
        ui_clear_event(ui);
      }
    } else if (ui->event.event_type == UIEventSelectAll) {
      // Select all
      caret_position_selection_started = 0;
      caret_position                   = len;
    } else if (ui->event.event_type == UIEventCopy) {
      size_t start  = min(caret_position_selection_started, caret_position);
      size_t end    = max(caret_position_selection_started, caret_position);
      char end_char = text[end];
      text[end]     = 0;
      g_os_api_->clipboard.set_data(EClipboardText, text + start, end - start + 1);
      text[end] = end_char;
    } else if (ui->event.event_type == UIEventPaste) {
      dd_stringview_t pasted_string =
          stringview(g_os_api_->clipboard.get_data(EClipboardText, tmp_malloc_allocator));
      if (len + pasted_string.length + 1 < max_chars) {
        memmove(text + caret_position + pasted_string.length, text + caret_position,
                len - caret_position + 1);
        memmove(text + caret_position, pasted_string.string, pasted_string.length);
        caret_position += (uint32)pasted_string.length;
        is_changed = true;
        ui_clear_event(ui);
      } else {
        // It doesn't all fit
      }
    }
  }

  return is_changed;
}
/*
Draws a scrollbar. The gutter on each side of the thumb is always at least 1 pixel, to indicate the
gutter is there. The buttons as each end decrement/increment 1 element, clicking in the gutter
decrements/increments by a full page of elements. The thumb can be dragged.

If visible elements >= total elements the thumb won't be shown.
+---+-+--------------------+-------------------------------------------------+---+
| B | |        THUMB       |     GUTTER                                      | B |
+---+-+--------------------+-------------------------------------------------+---+

*/
static void ui_scrollbar_vertical(dd_ui_t* ui, ui_rect_t rect_scrollbar, float visible_lines,
                                  float total_line_count, float delta, float* in_out_line_offset,
                                  uint32 id) {
  if (rect_contains_point(rect_scrollbar, ui->mouse.x, ui->mouse.y)) {
    ui_set_next_hover(ui, ui->current_ui_layer, id);
  }

  char p[2] = {14, 0};
  char n[2] = {15, 0};

  // Draw scrollbar
  ui_rect_t rect_prev          = rect_split_off_top(&rect_scrollbar, UIScrollbarThickness, 0);
  ui_rect_t rect_next          = rect_split_off_bottom(&rect_scrollbar, UIScrollbarThickness, 0);
  ui_rect_t rect_scroll_gutter = rect_scrollbar;
  if (visible_lines >= total_line_count) {
    *in_out_line_offset = 0;
    g_draw2d_api_->rect(&ui->draw_data, rect_prev, ui->theme->button.hover);
    g_draw2d_api_->rect(&ui->draw_data, rect_next, ui->theme->button.hover);
    g_draw2d_api_->rect(&ui->draw_data, rect_scroll_gutter, ui->theme->button.color);
    ui_label(ui, rect_prev, ui->theme->foreground.color, TextAlignmentCenter, stringview(p));
    ui_label(ui, rect_next, ui->theme->foreground.color, TextAlignmentCenter, stringview(n));
    // Everything fits, so no interaction needed
    return;
  }

  // It's possible that the amount of content has changed, so make sure the scroll is never too far
  // down.
  *in_out_line_offset =
      max(0.f, min(*in_out_line_offset, (float)(total_line_count - visible_lines)));

  const float usable_scroll_length = rect_scroll_gutter.height - 2;
  const float percentage_visible   = (float)visible_lines / (float)total_line_count;

  // Prevent the thumb from getting too small since the user needs something to grab
  const float thumb_pixel_length =
      floorf(max((float)UIScrollbarThickness, percentage_visible * rect_scroll_gutter.height));

  const float maximum_element_offset = total_line_count - visible_lines;

  const float maximum_pixel_offset = usable_scroll_length - thumb_pixel_length;

  const float scroll_thumb_offset =
      floorf(min(maximum_pixel_offset, maximum_pixel_offset * (float)*in_out_line_offset /
                                           (float)maximum_element_offset));

  ui_rect_t rect_before_thumb =
      rect_split_off_top(&rect_scroll_gutter, scroll_thumb_offset + 1, 0);
  ui_rect_t rect_thumb       = rect_split_off_top(&rect_scroll_gutter, thumb_pixel_length, 0);
  ui_rect_t rect_after_thumb = rect_scroll_gutter;

  g_draw2d_api_->rect(&ui->draw_data, rect_prev, ui->theme->button.hover);
  g_draw2d_api_->rect(&ui->draw_data, rect_next, ui->theme->button.hover);
  g_draw2d_api_->rect(&ui->draw_data, rect_before_thumb, ui->theme->button.color);
  g_draw2d_api_->rect(&ui->draw_data, rect_thumb, ui->theme->button.hover);
  g_draw2d_api_->rect(&ui->draw_data, rect_after_thumb, ui->theme->button.color);
  ui_label(ui, rect_prev, ui->theme->foreground.color, TextAlignmentCenter, stringview(p));
  ui_label(ui, rect_next, ui->theme->foreground.color, TextAlignmentCenter, stringview(n));

  static float offset_to_top_of_thumb = 0;
  if (ui->hover_id == id) {
    if (ui->event.event_type == UIEventMouseButton0Up) {
      ui->hover_id_locked = false;
      if (rect_contains_point(rect_prev, ui->mouse.x, ui->mouse.y)) {
        *in_out_line_offset = (*in_out_line_offset > delta) ? *in_out_line_offset - delta : 0;
        ui_clear_event(ui);
      } else if (rect_contains_point(rect_before_thumb, ui->mouse.x, ui->mouse.y)) {
        *in_out_line_offset =
            (*in_out_line_offset > visible_lines) ? *in_out_line_offset - visible_lines : 0;
        ui_clear_event(ui);
      } else if (rect_contains_point(rect_after_thumb, ui->mouse.x, ui->mouse.y)) {
        *in_out_line_offset = min(maximum_element_offset, *in_out_line_offset + visible_lines);
        ui_clear_event(ui);
      } else if (rect_contains_point(rect_next, ui->mouse.x, ui->mouse.y)) {
        *in_out_line_offset = min(maximum_element_offset, *in_out_line_offset + delta);
        ui_clear_event(ui);
      }
    } else if (ui->event.event_type == UIEventMouseButton0Down) {
      if (rect_contains_point(rect_thumb, ui->mouse.x, ui->mouse.y)) {
        ui->hover_id_locked    = true;
        offset_to_top_of_thumb = rect_thumb.y - ui->mouse.y;
        ui_clear_event(ui);
      }
    }

    if (ui->hover_id_locked) {
      if (ui->mouse.dy != 0) {
        float new_thumb_top_y   = ui->mouse.y + offset_to_top_of_thumb;
        float new_thumb_offset  = new_thumb_top_y - rect_before_thumb.y - 1;
        new_thumb_offset        = max(0.f, min(maximum_pixel_offset, new_thumb_offset));
        float percentage_offset = (float)new_thumb_offset / (float)maximum_pixel_offset;
        *in_out_line_offset     = percentage_offset * (total_line_count - visible_lines);
      }
    }
  }
}

static void ui_textarea(dd_ui_t* ui, ui_rect_t rect, const char* text, float* in_out_line_offset,
                        uint32 id) {
  const char* line = text;
  const char* nl   = strchr(text, '\n');
  char local_buf[2000];
  float end_y = rect.y + rect.height;

  float total_line_count = 1;
  {
    const char* first = nl;
    while (nl) {
      nl = strchr(nl + 1, '\n');
      total_line_count++;
    }
    nl = first;
  }

  ui_rect_t rect_scrollbar = rect_split_off_right(&rect, UIScrollbarThickness, 1);

  ui_rect_t line_rect = rect;
  line_rect.height    = 7;

  const float visible_lines = (0 + rect.height / (line_rect.height + 1));

  if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
    ui_set_next_hover(ui, ui->current_ui_layer, id);

    if (ui->hover_id == id) {
      if (ui->event.event_type == UIEventMouseScroll) {
        int32 delta = (*(int32*)&(ui->event.data));
        if (delta < 0) {
          *in_out_line_offset = min(total_line_count - visible_lines, *in_out_line_offset + 3);
        } else {
          if (*in_out_line_offset > 3) {
            *in_out_line_offset -= 3;
          } else {
            *in_out_line_offset = 0;
          }
        }
      }
    }
  }

  // Skip first lines
  uint32 lines_skipped = 0;
  while (nl && (lines_skipped < *in_out_line_offset)) {
    lines_skipped++;
    line = nl + 1;
    nl   = strchr(line, '\n');
  }

  while (nl && (line_rect.y + line_rect.height) < end_y) {
    strncpy(local_buf, line, nl - line);
    local_buf[nl - line] = 0;
    g_draw2d_api_->text(&ui->draw_data, line_rect, ui->theme->foreground.color, TextAlignmentLeft,
                        stringview(local_buf));

    line_rect.y += 8;
    line = nl + 1;
    nl   = strchr(line, '\n');
  }

  ui_scrollbar_vertical(ui, rect_scrollbar, visible_lines, total_line_count, 1, in_out_line_offset,
                        __LINE__);
}

// TODO: handle if not all tab bar items fit
static void ui_tab_bar(dd_ui_t* ui, ui_rect_t* rect) {
  ui_rect_t horizontal_line_r = rect_split_off_bottom(rect, 1, 0);
  g_draw2d_api_->rect(&ui->draw_data, horizontal_line_r, ui->theme->status_bar.color_success);
}
static bool ui_tab_bar_item(dd_ui_t* ui, ui_rect_t* rect, const dd_stringview_t label,
                            bool is_active_tab) {
  ui_rect_t tab_background = rect_split_off_left(rect, 100, 0);
  if (!is_active_tab) {
    if (rect_contains_point(tab_background, ui->mouse.x, ui->mouse.y)) {
      ui->hover_id =
          0;  // Tab has changed, so we can't be said to be hovering over the old item anymore

      ui_rect_t tab_hover_background = tab_background;
      tab_hover_background.height--;
      g_draw2d_api_->rect(&ui->draw_data, tab_hover_background, ui->theme->button.hover);
      g_draw2d_api_->text(&ui->draw_data, rect_shrink(tab_background, 1),
                          ui->theme->foreground.color, TextAlignmentCenter, label);

      if (ui->event.event_type == UIEventMouseButton0Up) {
        return true;
      }
    } else {
      g_draw2d_api_->text(&ui->draw_data, rect_shrink(tab_background, 1),
                          ui->theme->foreground.color, TextAlignmentCenter, label);
    }
  } else {
    g_draw2d_api_->rect(&ui->draw_data, tab_background, ui->theme->status_bar.color_success);
    g_draw2d_api_->text(&ui->draw_data, rect_shrink(tab_background, 1),
                        ui->theme->foreground.color, TextAlignmentCenter, label);
  }

  return false;
}

bool dropdown_expanded(dd_ui_t* ui, const dd_ui_dropdown_t* dropdown,
                       const ui_rect_t r_closed_dropdown, uint32* out_index) {
  const dd_stringview_t* options = dropdown->options;
  const uint32 options_count     = dropdown->options_count;

  bool did_select         = false;
  const float item_height = 13;
  const float max_height  = 100;
  bool all_options_fit    = (options_count * item_height < max_height);

  ui_rect_t r_scroll_view = r_closed_dropdown;
  r_scroll_view.y += r_scroll_view.height;
  r_scroll_view.height = all_options_fit ? options_count * item_height : max_height;

  // Swap the quads buffers
  ui->quads_normal    = ui->draw_data.quads;
  ui->draw_data.quads = ui->overlay_quads;

  ui->current_ui_layer++;

  if (rect_contains_point(r_scroll_view, ui->mouse.x, ui->mouse.y)) {
    ui_set_next_hover(ui, ui->current_ui_layer, __LINE__);
  }

  g_draw2d_api_->rect_stroke(&ui->draw_data, r_scroll_view, 1, ui->theme->foreground.color);
  r_scroll_view = rect_shrink(r_scroll_view, 1);

  ui_rect_t r_item = r_scroll_view;
  r_item.height    = item_height;

  ui_rect_t canvas = {.height = (int16)(options_count * r_item.height), .width = r_item.width};
  dd_ui_scrollview_t scrollview = {.id = __LINE__, .rect = r_scroll_view, .canvas = canvas};

  float scroll_x        = 0;  // Don't care about horizontal scrolling
  static float scroll_y = 0;
  if (!all_options_fit) {
    ui_scrollview_begin(ui, &scrollview, &scroll_x, &scroll_y, &r_scroll_view);

    r_item.width = r_scroll_view.width;  // Adjust for shown scrollbar
  }

  const ui_rect_t prev_clip_rect = g_draw2d_api_->set_clip_rect(&ui->draw_data, r_scroll_view);

  g_draw2d_api_->rect(&ui->draw_data, r_scroll_view, ui->theme->background.color);

  const uint32 first_visible_option = (uint32)(scroll_y / r_item.height);
  r_item.y = r_scroll_view.y - (scroll_y - first_visible_option * r_item.height);
  for (uint16 i = (uint16)first_visible_option; i < (uint16)options_count; i++) {
    const bool is_option_enabled =
        (dropdown->options_disabled == NULL || !(dropdown->options_disabled[i]));
    if (is_option_enabled && rect_contains_point(r_item, ui->mouse.x, ui->mouse.y)) {
      ui_set_next_hover(ui, ui->current_ui_layer, __LINE__);
      g_draw2d_api_->rect(&ui->draw_data, r_item, ui->theme->button.hover);
      if (ui->event.event_type == UIEventMouseButton0Up) {
        *out_index = (uint32)i;
        did_select = true;
      }
    }

    const color_rgba8_t text_color =
        is_option_enabled ? ui->theme->foreground.color : ui->theme->foreground.disabled;
    ui_label(ui, r_item, text_color, TextAlignmentLeft, options[i]);
    r_item.y += r_item.height;
  }

  (void)g_draw2d_api_->set_clip_rect(&ui->draw_data, prev_clip_rect);

  if (!all_options_fit) {
    ui_scrollview_end(ui, &scrollview, &scroll_x, &scroll_y);
  }

  ui->current_ui_layer--;

  // Swap the quads buffers
  ui->overlay_quads   = ui->draw_data.quads;
  ui->draw_data.quads = ui->quads_normal;
  return did_select;
}

static bool ui_dropdown(dd_ui_t* ui, const dd_ui_dropdown_t* dropdown,
                        uint32* in_out_selected_index) {
  const ui_rect_t r_dropdown_original = dropdown->rect;
  const dd_stringview_t* in_options   = dropdown->options;
  const uint32 options_count          = dropdown->options_count;
  const uint32 id                     = dropdown->id;
  if (options_count == 0) return false;

  assert(options_count > 0);

  // It's ok to only be able to track a single dropdown, as more than a single dropdown will never
  // be open at the same time.
  static uint32 visible_dropdown_id = 0;

  // Add indicator to show it's a drop down
  // TODO: do a proper arrow. Could add to the font sheet
  ui_rect_t r_dropdown   = r_dropdown_original;
  ui_rect_t r_arrow      = rect_split_off_right(&r_dropdown, r_dropdown.height, 1);
  r_arrow                = rect_shrink(r_arrow, 1);
  const dd_ui_button_t b = {.rect        = r_dropdown_original,
                            .id          = (uint32)id,
                            .is_disabled = false,
                            .label       = in_options[*in_out_selected_index]};
  if (ui_button(ui, &b)) {
    // Expand or cancel
    visible_dropdown_id = (visible_dropdown_id == 0) ? id : 0;
    ui_clear_event(ui);
  } else if ((ui->active_id != id) && id == (visible_dropdown_id)) {
    visible_dropdown_id = 0;
  }

  char n[2] = {15, 0};
  ui_label(ui, r_arrow, ui->theme->foreground.color, TextAlignmentCenter, stringview(n));

  if (visible_dropdown_id == id) {
    ui_rect_t r_scroll_view = r_dropdown_original;
    uint32 out_index;
    if (dropdown_expanded(ui, dropdown, r_scroll_view, &out_index)) {
      *in_out_selected_index = out_index;
      visible_dropdown_id    = 0;
      ui_clear_event(ui);
      return true;
    }
  }

  return false;
}

// Interaction
// ====================
static void ui_interaction_tab_focus_register(dd_ui_t* ui, uint32 id) {
  if (ui->tab_focus.focus_on_next) {
    ui->active_id               = id;
    ui->tab_focus.focus_on_next = false;
  }

  if (ui->active_id == id) {
    bool go_to_prev_control = false;
    bool go_to_next_control = false;
    if (ui->event.event_type == UIEventKeyInputNonChar) {
      dd_ui_key_event_t e = *(dd_ui_key_event_t*)&(ui->event);
      if (e.key == '\t') {
        if (e.modifier & EUIEventKeyModifierShift)
          go_to_prev_control = true;
        else
          go_to_next_control = true;

        ui_clear_event(ui);
      }
    }
    if (go_to_prev_control) {
      if (ui->tab_focus.previous_control_id) {
        ui->active_id = ui->tab_focus.previous_control_id;
      }
    } else if (go_to_next_control) {
      ui->tab_focus.focus_on_next = true;
    }
  }

  ui->tab_focus.previous_control_id = id;
}

static void responder_scope_begin(dd_ui_t* ui, uint32 id) {
  assert(ui->responder.current_scope_depth < MAX_RESPONDER_CHAIN_DEPTH);
  ui->responder.scopes[ui->responder.current_scope_depth++] = id;
}
static void responder_scope_end(dd_ui_t* ui, uint32 id) {
  assert(ui->responder.current_scope_depth > 0);

  // Check that the end matches the begin
  assert(ui->responder.scopes[ui->responder.current_scope_depth - 1] == id);

  ui->responder.scopes[--ui->responder.current_scope_depth] = 0;
}
static bool responder_is_in_responder_scopes(dd_ui_t* ui, uint32 id) {
  // Start at end, since it'll likely be closest to the end rather than the beginning
  for (int32 i = (int32)ui->responder.current_scope_depth - 1; i >= 0; i--) {
    if (ui->responder.scopes[i] == id) return true;
  }

  return false;
}
static bool responder_is_first_responder(dd_ui_t* ui, uint32 id) {
  return ((ui->responder.current_scope_depth > 0) &&
          (ui->responder.scopes[ui->responder.current_scope_depth - 1] == id));
}

void ui_scrollview_begin(dd_ui_t* ui, const dd_ui_scrollview_t* view, float* in_out_scroll_x,
                         float* in_out_scroll_y, ui_rect_t* out_content_rect) {
  if (rect_contains_point(view->rect, ui->mouse.x, ui->mouse.y)) {
    responder_scope_begin(ui, view->id);
    ui_set_next_hover(ui, ui->current_ui_layer, view->id);
  }

  ui_rect_t c    = view->rect;
  float scroll_y = *in_out_scroll_y;

  ui_rect_t r_bar   = rect_split_off_right(&c, UIScrollbarThickness, 0);
  const float delta = 10;
  ui_scrollbar_vertical(ui, r_bar, view->rect.height, view->canvas.height, delta, &scroll_y,
                        view->id);

  *in_out_scroll_y  = scroll_y;
  *out_content_rect = c;
}
void ui_scrollview_end(dd_ui_t* ui, const dd_ui_scrollview_t* view, float* in_out_scroll_x,
                       float* in_out_scroll_y) {
  const float delta = 10;

  if (responder_is_in_responder_scopes(ui, view->id)) {
    {
      const float offset_max = view->canvas.height - view->rect.height;
      float offset           = *in_out_scroll_y;
      if (ui->event.event_type == UIEventMouseScroll) {
        int32 md = (*(int32*)&(ui->event.data));
        if (md < 0) {
          offset = min(offset_max, offset + delta);
        } else {
          offset = max(offset, delta) - delta;
        }
        ui_clear_event(ui);
      } else if (ui->event.event_type == UIEventKeyInputNonChar) {
        // Want to keep a line overlap when using keys to scroll
        if (ui->event.data == DD_KEY_PAGEUP) {
          offset = max(offset, view->rect.height - 1) - (view->rect.height - 1);
        } else if (ui->event.data == DD_KEY_PAGEDOWN) {
          offset = min(offset + view->rect.height - 1, offset_max);
        }
      }
      *in_out_scroll_y = offset;
    }
  }

  if (responder_is_first_responder(ui, view->id)) {
    responder_scope_end(ui, view->id);
  }
}
void ui_scrollview_ensure_rect_in_view(dd_ui_t* ui, const dd_ui_scrollview_t* view,
                                       ui_rect_t in_rect, float* in_out_scroll_x,
                                       float* in_out_scroll_y) {
  ui_rect_t r_content = view->rect;

  (void)rect_split_off_right(&r_content, UIScrollbarThickness, 0);

  float bottom = in_rect.y + in_rect.height;
  if (bottom > (*in_out_scroll_y + r_content.height)) {
    *in_out_scroll_y = max(0.f, bottom - r_content.height);
  }
  if (in_rect.y < *in_out_scroll_y) {
    *in_out_scroll_y = in_rect.y;
  }
}

void ui_init(dd_ui_t* ui, struct dd_draw2d_api* draw2d_api) { g_draw2d_api_ = draw2d_api; }

const struct dd_ui_api ui_api = {
    .init        = &ui_init,
    .widget      = {.label                          = &ui_label,
               .button                         = &ui_button,
               .checkbox                       = &ui_checkbox,
               .inputfield                     = &ui_inputfield,
               .pane                           = &ui_pane,
               .tab_bar                        = &ui_tab_bar,
               .tab_bar_item                   = &ui_tab_bar_item,
               .dropdown                       = &ui_dropdown,
               .textarea                       = &ui_textarea,
               .scrollview_begin               = &ui_scrollview_begin,
               .scrollview_end                 = &ui_scrollview_end,
               .scrollview_ensure_rect_in_view = &ui_scrollview_ensure_rect_in_view,
               .scrollbar_vertical             = &ui_scrollbar_vertical},
    .interaction = {.clear_event                 = &ui_clear_event,
                    .pop_event                   = &ui_pop_event,
                    .push_event                  = &ui_push_event,
                    .update_mouse_pos            = &ui_update_mouse_pos,
                    .set_next_hover              = &ui_set_next_hover,
                    .register_tab_focus_interest = &ui_interaction_tab_focus_register,
                    .responder_scope_begin       = &responder_scope_begin,
                    .responder_scope_end         = &responder_scope_end,
                    .is_in_responder_scope       = &responder_is_in_responder_scopes,
                    .is_first_responder          = &responder_is_first_responder},
    .begin       = &ui_begin,
    .end         = &ui_end};

#if defined(_MSC_VER)
  #define DD_DLL_EXPORT __declspec(dllexport)
#else
  #define DD_DLL_EXPORT
#endif

DD_DLL_EXPORT const struct dd_ui_api* get_ui_api() { return &ui_api; }
DD_DLL_EXPORT const struct dd_ui_api* get_api() { return &ui_api; }

#include "core/plugins.h"
DD_DLL_EXPORT bool plugin_link_apis(const dd_api_registry_t* registry,
                                    struct dd_allocator* allocator) {
  // Check dependencies
  {
    g_draw2d_api_ = (struct dd_draw2d_api*)registry->get_api("draw2d", 1);
    g_os_api_     = (struct dd_os_api*)registry->get_api("os", 1);

    if (g_draw2d_api_ == NULL) return false;
    if (g_os_api_ == NULL) return false;
  }

  // Register own apis
  {
    registry->register_api("ui", 1, &ui_api, sizeof(ui_api));

    return true;
  }
}
