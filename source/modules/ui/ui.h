#pragma once

/*
References:
  Single draw call UI:
    https://ourmachinery.com/post/ui-rendering-using-primitive-buffers/
    https://ourmachinery.com/post/one-draw-call-ui/
    https://gist.github.com/niklas-ourmachinery/9e37bdcad5bacaaf09ad4f5bb93ecfaf

  Colors:
    https://code.visualstudio.com/api/references/theme-color

  IMGUI:
    https://github.com/ocornut/imgui/blob/master/imgui_demo.cpp
    https://ourmachinery.com/post/keyboard-focus-and-event-trickling-in-immediate-mode-guis/
*/
#include "core/hid.h"
#include "core/types.h"
#include "draw2d/draw_2d.h"
#include "os/os.h"

enum { MAX_RESPONDER_CHAIN_DEPTH = 32 };

typedef enum EUIEvent {
  UIEventNone = 0,
  // Think left-mouse button, but may be reversed by setting or by user
  UIEventMouseButton0Down,
  UIEventMouseButton1Down,
  UIEventMouseButton2Down,
  UIEventMouseButton0Up,
  UIEventMouseButton1Up,
  UIEventMouseButton2Up,
  UIEventMouseButton0DoubleClick,
  UIEventMouseScroll,
  UIEventKeyInputChar,
  UIEventKeyInputNonChar,
  UIEventSelectAll,
  UIEventCopy,
  UIEventCut,
  UIEventPaste
} EUIEvent;

enum EUIEventKeyModifier {
  EUIEventKeyModifierShift = (1 << 0),
  EUIEventKeyModifierCtrl  = (1 << 1),
  EUIEventKeyModifierAlt   = (1 << 2),
};

typedef struct dd_mouse_event_data_t {
  int32 ctrl : 1;
  int32 shift : 1;
} dd_mouse_event_data_t;

typedef struct dd_ui_event_t {
  EUIEvent event_type;
  int32 data;
} dd_ui_event_t;

typedef struct dd_ui_key_event_t {
  EUIEvent event_type;
  // EUIEventKeyModifier
  int32 modifier : 8;
  // Keycode
  int32 key : 24;
} dd_ui_key_event_t;

enum { UIScrollbarThickness = 16 };

/*
Elements to color:
Textbox
border
inactive background
active background
hover background
text
Scrollbar
Buttons
Gutter
Thumb
Table
header background
header thumbs to resize columns/rows
alternating background colors for lines
*/

typedef struct dd_ui_style_t {
  struct {
    color_rgba8_t color;
    color_rgba8_t disabled;
  } background;
  struct {
    color_rgba8_t color;
    color_rgba8_t disabled;
  } foreground;
  struct {
    color_rgba8_t color;
    color_rgba8_t hover;
    color_rgba8_t down;
  } button;
  struct {
    color_rgba8_t color_neutral, color_error, color_success;
  } status_bar;
} dd_ui_style_t;

typedef struct dd_ui_t {
  dd_draw2d_t draw_data;

  /* Main quad data */
  uint32* quads_normal; /* array */
  /* Some UI elements need to be drawn on top of others (dropdowns etc). Put that data into this
   * separate buffer, which will be appended to the main one in ui_api.end. The extended data is
   * always put in the same array, just the quad info part isn't. */
  uint32* overlay_quads; /* array */

  // Input device data
  dd_mouse_t mouse;

  /// Tab focus information
  struct {
    uint32 previous_control_id;  // id of previous control that could take focus. Overwritten by
                                 // each interested control
    bool focus_on_next;          // true if next control should take focus
  } tab_focus;

  // Maximum of a single event per frame. If more have to be handled, then simply do a full update
  // multiple times in that frame.
  dd_ui_event_t event;

  uint32 current_ui_layer; /* temporarily increase this when rendering overlay of some kind */
  uint32 current_hover_layer;

  uint32 hover_id;
  uint32 active_id;

  uint32 next_hover_layer;
  uint32 next_hover_id;
  bool hover_id_locked;  // Enable this during drag operations so hover effects don't take place on
                         // other elements

  const dd_ui_style_t* theme;

  struct {
    uint32 scopes[MAX_RESPONDER_CHAIN_DEPTH];
    uint32 current_scope_depth;
  } responder;

  // Output data for a frame.
  struct {
    // Which mouse cursor should be shown, if not the default?
    dd_cursor_t cursor_type;
  } out;
} dd_ui_t;

/*
 * Returns true if the text changed
 */
enum EInputFieldType {
  EInputFieldTypeText = 0,  // Allows any character to be entered
  EInputFieldTypeInteger,   // Only numbers allowed
  EInputFieldTypeDecimal,   // Only allow numbers, and decimal separator (. hardcoded for now)
};

typedef struct dd_ui_button_t {
  ui_rect_t rect;
  uint32 id;
  bool is_disabled;
  const dd_stringview_t label;
} dd_ui_button_t;

typedef struct dd_ui_dropdown_t {
  ui_rect_t rect;
  uint32 id;
  bool is_disabled;

  // Array with strings to show.
  const dd_stringview_t* options;

  // Array with true for un-selectable options. May be NULL if all options are allowed.
  const bool* options_disabled;
  const uint32 options_count;
} dd_ui_dropdown_t;

typedef struct dd_ui_inputfield_t {
  uint32 id;
  ui_rect_t rect;

  // Control what input characters are allowed
  enum EInputFieldType input_type;

  // If text is empty, this text will be shown
  const char* placeholder_text;
} dd_ui_inputfield_t;

typedef struct dd_ui_scrollview_t {
  uint32 id;

  // Rect for the view. Both the content and the scrollbars will be contained within this rect.
  ui_rect_t rect;

  // Rect for the contents that is viewed.
  ui_rect_t canvas;

} dd_ui_scrollview_t;

struct dd_ui_api {
  // Call this before doing anything with the UI object
  void (*init)(dd_ui_t* ui_o, struct dd_draw2d_api* draw2d_api);

  struct {
    void (*label)(dd_ui_t* ui, ui_rect_t rect, color_rgba8_t color, ETextAligment align,
                  const dd_stringview_t text);

    /* Returns true if the button was clicked */
    bool (*button)(dd_ui_t* ui, const dd_ui_button_t* button);

    /* Returns true if the status changed */
    bool (*checkbox)(dd_ui_t* ui, ui_rect_t rect, const dd_stringview_t label, uint32 id,
                     bool* in_out_is_checked);

    /*
     * max_chars includes terminating 0
     */
    bool (*inputfield)(dd_ui_t* ui, const dd_ui_inputfield_t* inputfield, char* text,
                       uint32 max_chars);

    void (*pane)(dd_ui_t* ui, ui_rect_t rect, color_rgba8_t color);

    void (*tab_bar)(dd_ui_t* ui, ui_rect_t* rect);
    bool (*tab_bar_item)(dd_ui_t* ui, ui_rect_t* rect, const dd_stringview_t label,
                         bool is_active_tab);

    // Returns true when selected item has changed
    bool (*dropdown)(dd_ui_t* ui, const dd_ui_dropdown_t* dropdown, uint32* in_out_selected_index);

    void (*textarea)(dd_ui_t* ui, ui_rect_t rect, const char* text, float* in_out_line_offset,
                     uint32 id);

    /*  Modifies incoming rect to the visible area (in this case the scrollbar is removed from the
     * area) Modifies the offset depending on input events.
     */
    void (*scrollview_begin)(dd_ui_t* ui, const dd_ui_scrollview_t* view, float* in_out_scroll_x,
                             float* in_out_scroll_y, ui_rect_t* out_content_rect);
    void (*scrollview_end)(dd_ui_t* ui, const dd_ui_scrollview_t* view, float* in_out_scroll_x,
                           float* in_out_scroll_y);
    void (*scrollview_ensure_rect_in_view)(dd_ui_t* ui, const dd_ui_scrollview_t* view,
                                           ui_rect_t in_rect, float* in_out_scroll_x,
                                           float* in_out_scroll_y);

    // TODO: Do I really need this so explicitly???
    void (*scrollbar_vertical)(dd_ui_t* ui, ui_rect_t rect_scrollbar, float visible_lines,
                               float total_line_count, float delta, float* in_out_line_offset,
                               uint32 id);

  } widget;

  struct {
    /* Event processing code. Backed by ring buffer
     */
    void (*clear_event)(dd_ui_t* ui);
    dd_ui_event_t (*pop_event)();
    void (*push_event)(dd_ui_event_t e, struct dd_allocator* allocator);

    void (*update_mouse_pos)(int16 x, int16 y);
    void (*set_next_hover)(dd_ui_t* ui, uint32 next_hover_layer, uint32 next_hover_id);

    void (*register_tab_focus_interest)(dd_ui_t* ui, uint32 id);

    void (*responder_scope_begin)(dd_ui_t* ui, uint32 id);
    void (*responder_scope_end)(dd_ui_t* ui, uint32 id);
    bool (*is_in_responder_scope)(dd_ui_t* ui, uint32 id);
    bool (*is_first_responder)(dd_ui_t* ui, uint32 id);
  } interaction;

  /* Updating/drawing a frame of UI
   */
  void (*begin)(dd_ui_t* ui, const dd_ui_style_t* theme, dd_ui_event_t event);
  const dd_draw2d_t* (*end)(dd_ui_t* ui);
};

#if defined(_MSC_VER)
  #if !defined(daedalus_ui_EXPORTS)
    #define DLL_EXPORT __declspec(dllimport)
  #else
    #define DLL_EXPORT __declspec(dllexport)
  #endif
#elif defined(__clang__)
  #define DLL_EXPORT
#endif

#if defined(__cplusplus)
extern "C" {
#endif

// const struct dd_ui_api* get_api();

#if defined(__cplusplus)
}
#endif
