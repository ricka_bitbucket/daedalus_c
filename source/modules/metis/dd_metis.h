#pragma once

#include "core/types.h"

struct dd_allocator;

enum { cluster_size = 128 };

typedef struct dd_mesh_cluster_data_t {
  uint32 base_vertex;
  uint32 base_index;
  int32 padding_A;
  int32 padding_B;
} dd_mesh_cluster_data_t;

typedef struct dd_cluster_mesh_t {
  uint32 cluster_count;
  uint32 cluster_size;
  uint32 vertex_count;

  // Offsets are from start of array
  uint32 cluster_offset;
  uint32 triangle_index_offset;
  uint32 vertex_offset;

  // Data follows here.
  // dd_mesh_cluster_data_t[cluster_count]
  // uint32 packed_triangle_indexes[cluster_count*cluster_size]
  // float4 positions [vertex_count]
} dd_cluster_mesh_t;

struct dd_metis_api {
  /// \return DDF data. Cast returned pointer to array(uint8) and free it when done.
  uint8* (*clusterize_mesh)(array(uint8) mesh_data, struct dd_allocator* allocator);
};
