#include "dd_metis.h"

#include <assert.h>
#include <string.h>

#include "core/containers.h"
#include "core/dd_math.h"
#include "core/ddf.h"
#include "core/filesystem.h"
#include "core/types.h"
#include "core/utils.h"
#include "formats/ply_reader/ply_reader.h"

// Thirdparty
#include "metis.h"

void calculate_vertex_normals(const float4* vertexes, const uint32* indexes,
                              const size_t vertex_count, const size_t triangle_count,
                              float3* vertex_normals) {
  memset(vertex_normals, 0, sizeof(float3) * vertex_count);

  for (size_t it = 0; it < triangle_count; ++it) {
    const uint32* tris_indexes = indexes + it * 3;
    float4 vertex_a            = vertexes[tris_indexes[0]];
    float4 vertex_b            = vertexes[tris_indexes[1]];
    float4 vertex_c            = vertexes[tris_indexes[2]];
    float3 dir_a  = {vertex_b.x - vertex_a.x, vertex_b.y - vertex_a.y, vertex_b.z - vertex_a.z};
    float3 dir_b  = {vertex_c.x - vertex_a.x, vertex_c.y - vertex_a.y, vertex_c.z - vertex_a.z};
    dir_a         = float3_normalize(dir_a);
    dir_b         = float3_normalize(dir_b);
    float3 normal = float3_normalize(float3_cross(dir_a, dir_b));
    vertex_normals[tris_indexes[0]] = float3_add(vertex_normals[tris_indexes[0]], normal);
    vertex_normals[tris_indexes[1]] = float3_add(vertex_normals[tris_indexes[1]], normal);
    vertex_normals[tris_indexes[2]] = float3_add(vertex_normals[tris_indexes[2]], normal);
  }

  for (size_t iv = 0; iv < vertex_count; ++iv) {
    float3 v = float3_normalize(vertex_normals[iv]);
    /*assert(!isnan(v.x));
    assert(!isnan(v.y));
    assert(!isnan(v.z));*/
    vertex_normals[iv] = v;
  }
}

uint8* metis_clusterize_mesh(array(uint8) mesh_data, dd_allocator* allocator) {
  dd_mesh_t* mesh = (dd_mesh_t*)mesh_data;

  dd_allocator* temp_allocator = allocator;

  uint32* index_data        = (uint32*)(mesh_data + mesh->index_offset);
  float4* original_vertexes = (float4*)(mesh_data + mesh->vertex_offset);
  uint32 index_count        = mesh->index_count;

  {
    idx_t* triangle_index = (idx_t*)index_data;

    idx_t triangle_count = index_count / 3;
    idx_t node_count     = mesh->vertex_count;
    // TODO(Rick): partitioning isn't perfect, when creating the minimum amount of clusters, some
    // clusters get more than cluster_size triangles. By increasing the count a bit the
    // partitioning does succeed in getting <= cluster size triangles in all clusters.
    idx_t cluster_count = (idx_t)(1.05f * align_up(triangle_count, cluster_size) / cluster_size);
    idx_t options[METIS_NOPTIONS] = {0};
    idx_t objval                  = 0;

    array(idx_t) eptr = NULL;
    array_resize(eptr, triangle_count + 1, temp_allocator);
    for (idx_t i = 0; i <= triangle_count; ++i) {
      eptr[i] = i * 3;
    }

    options[METIS_OPTION_NUMBERING] = 0;
    options[METIS_OPTION_NCUTS]     = 1;
    options[METIS_OPTION_NITER]     = 1;
    options[METIS_OPTION_UFACTOR]   = 30;

    array(idx_t) out_element_part = NULL;
    array_resize(out_element_part, triangle_count, temp_allocator);

    const char* stored_data_path = "cluster_data.bin";
    bool is_data_reloaded        = false;
    if (dd_filesystem_api.file_exists(stored_data_path)) {
      array(uint8) ddf_element_data = read_file(stored_data_path, temp_allocator);
      if (ddf_element_data) {
        ddf_block_descriptor_t bd =
            ddf_api.read.block_descriptor_with_label(ddf_element_data, "element_parts");

        if (bd.data_byte_count == triangle_count * sizeof(*out_element_part)) {
          memcpy(out_element_part, ddf_element_data + bd.data_offset, bd.data_byte_count);
          is_data_reloaded = true;
        }

        array_free(ddf_element_data, temp_allocator);
      }
    }

    if (!is_data_reloaded) {
      array(idx_t) out_node_part = NULL;
      array_resize(out_node_part, index_count, temp_allocator);

      dd_devprintf("Starting METIS with %i triangles.\n", (int)triangle_count);
      /*int result = METIS_PartMeshNodal(&triangle_count, &node_count, eptr, triangle_index, NULL,
                                       NULL, &cluster_count, NULL, (idx_t*)options, &objval,
                                       out_element_part, out_node_part);
      /*/
      idx_t common_node_count = 2;
      int result              = METIS_PartMeshDual(
          &triangle_count, &node_count, eptr, triangle_index, NULL, NULL, &common_node_count,
          &cluster_count, NULL, (idx_t*)options, &objval, out_element_part, out_node_part);  //*/
      dd_devprintf("METIS complete.\n");
      if (result != METIS_OK) {
        dd_devprintf("METIS result: %i\n", result);
        return NULL;
      }

      array(uint8) ddf_element_data = NULL;
      ddf_api.write.init_header(&ddf_element_data, temp_allocator);
      const size_t ep_block_index =
          ddf_api.write.add_block(&ddf_element_data, temp_allocator, "element_parts", "uint32");
      /*const size_t np_block_index =
          ddf_api.write.add_block(&element_data, temp_allocator, "node_parts", "uint32");*/
      ddf_api.write.set_block_data(&ddf_element_data, temp_allocator, ep_block_index,
                                   out_element_part, triangle_count * sizeof(*out_element_part));

      FILE* data_file = fopen(stored_data_path, "wb");
      // fwrite(out_element_part, sizeof(*out_element_part), triangle_count, data_file);
      fwrite(ddf_element_data, 1, array_count(ddf_element_data), data_file);
      fclose(data_file);

      array_free(out_node_part, temp_allocator);
    }

    array(idx_t) tris_cluster_count = NULL;
    array_resize(tris_cluster_count, cluster_count, temp_allocator);
    memset(tris_cluster_count, 0, sizeof(*tris_cluster_count) * array_count(tris_cluster_count));

    for (idx_t it = 0; it < triangle_count; ++it) {
      tris_cluster_count[out_element_part[it]]++;
    }

    array(float4) cluster_vertexes       = NULL;
    array(float3) cluster_vertex_normals = NULL;
    array(uint32) cluster_indexes        = NULL;

    array(float3) original_vertex_normals = NULL;
    size_t vertex_count                   = mesh->vertex_count;
    array_resize(original_vertex_normals, vertex_count, temp_allocator);
    calculate_vertex_normals(original_vertexes, index_data, vertex_count, triangle_count,
                             original_vertex_normals);

    // Reorder indexes, and copy all vertices to new order. This duplicates all vertices on the
    // border of a cluster, but that may be improved in the future.
    index_count                                       = cluster_count * cluster_size * 3;
    array(uint32) original_clustered_triangle_indexes = NULL;
    array(uint32) triangle_count_per_cluster          = NULL;
    // Make space for one extra triangle in each cluster, so can track if a cluster has too
    // much data.
    array_resize(original_clustered_triangle_indexes, cluster_count * cluster_size,
                 temp_allocator);
    array_resize(triangle_count_per_cluster, cluster_count, temp_allocator);
    memset(triangle_count_per_cluster, 0, sizeof(*triangle_count_per_cluster) * cluster_count);
    for (idx_t it = 0; it < triangle_count; ++it) {
      const uint32 cluster_index = out_element_part[it];
      original_clustered_triangle_indexes
          [cluster_index * cluster_size +
           min((uint32)(cluster_size - 1), (uint32)(triangle_count_per_cluster[cluster_index]))] =
              it;
      triangle_count_per_cluster[cluster_index]++;
    }
    for (idx_t ic = 0; ic < cluster_count; ++ic) {
      if (triangle_count_per_cluster[ic] > (uint32)cluster_size) {
        dd_devprintf("Full cluster %i/%i: %i/%i\n", (int)ic, (int)cluster_count,
                     (int)triangle_count_per_cluster[ic], (int)cluster_size);
      }
    }

    array_resize(cluster_indexes, index_count, temp_allocator);
    memset(cluster_indexes, 0, sizeof(*cluster_indexes) * index_count);
    index_data = cluster_indexes;

    // Make space for maximum number of verts
    array_reserve(cluster_vertexes, cluster_count * cluster_size * 3, temp_allocator);
    array_reserve(cluster_vertex_normals, cluster_count * cluster_size * 3, temp_allocator);

    uint32 used_original_index[cluster_size * 3];

    for (idx_t ic = 0; ic < cluster_count; ++ic) {
      if (ic % 10000 == 0 && ic != 0) {
        dd_devprintf("Processing cluster %i/%i\n", (int)ic, (int)cluster_count);
      }

      uint32 used_original_index_count = 0;

      uint32* const cluster_start_index = cluster_indexes + ic * cluster_size * 3;
      uint32* const cluster_end_index   = cluster_indexes + (ic + 1) * cluster_size * 3;
      uint32* out_index                 = cluster_start_index;

      uint32 cluster_triangle_count            = min(cluster_size, triangle_count_per_cluster[ic]);
      const uint32 vertex_count_before_cluster = (uint32)array_count(cluster_vertexes);
      for (uint32 ict = 0; ict < cluster_triangle_count; ++ict) {
        uint32 it = original_clustered_triangle_indexes[cluster_size * ic + ict];

        for (size_t iv = 0; iv < 3; ++iv) {
          uint32 original_vertex_index = triangle_index[it * 3 + iv];
          uint32 cluster_vertex_index  = (uint32)array_count(cluster_vertexes);
          uint32 icached               = 0;
          for (; icached < used_original_index_count; ++icached) {
            if (used_original_index[icached] == original_vertex_index) break;
          }
          if (icached == used_original_index_count) {
            used_original_index[used_original_index_count++] = original_vertex_index;
            array_push_no_alloc(cluster_vertexes, original_vertexes[original_vertex_index]);
            float3 n = {original_vertex_normals[original_vertex_index].x,
                        original_vertex_normals[original_vertex_index].y,
                        original_vertex_normals[original_vertex_index].z};
            array_push_no_alloc(cluster_vertex_normals, n);
          } else {
            cluster_vertex_index = vertex_count_before_cluster + icached;
          }

          *(out_index++) = cluster_vertex_index;
        }
      }

      // Repeat last index to fill the cluster with degenerate triangles. This way shaders can
      // assume cluster_size triangles in each cluster.
      uint32 last_index = *(out_index - 1);
      while (out_index < cluster_end_index) {
        *(out_index++) = last_index;
      }
    }

    array_free(eptr, temp_allocator);
    array_free(out_element_part, temp_allocator);
    array_free(original_vertex_normals, temp_allocator);

    const size_t clustered_vertex_count = array_count(cluster_vertexes);
    array(uint8) ddf_data               = NULL;
    ddf_api.write.init_header(&ddf_data, allocator);
    size_t cluster_block_id =
        ddf_api.write.add_block(&ddf_data, allocator, "clusters", "cluster16byte");
    size_t index_block_id = ddf_api.write.add_block(&ddf_data, allocator, "indexes", "uint10_3");
    size_t position_block_id =
        ddf_api.write.add_block(&ddf_data, allocator, "positions", "float_4");
    size_t normal_block_id = ddf_api.write.add_block(&ddf_data, allocator, "normals", "float_4");

    ddf_api.write.reserve_block_data(&ddf_data, allocator, cluster_block_id,
                                     cluster_count * sizeof(dd_mesh_cluster_data_t));
    ddf_api.write.reserve_block_data(&ddf_data, allocator, index_block_id,
                                     cluster_count * cluster_size * sizeof(uint32));
    ddf_api.write.reserve_block_data(&ddf_data, allocator, position_block_id,
                                     (uint32)clustered_vertex_count * sizeof(float4));
    ddf_api.write.reserve_block_data(
        &ddf_data, allocator, normal_block_id,
        (uint32)clustered_vertex_count * sizeof(*cluster_vertex_normals));
    ddf_block_descriptor_t cluster_block =
        ddf_api.read.block_descriptor(ddf_data, cluster_block_id);
    ddf_block_descriptor_t index_block = ddf_api.read.block_descriptor(ddf_data, index_block_id);
    ddf_block_descriptor_t position_block =
        ddf_api.read.block_descriptor(ddf_data, position_block_id);
    ddf_block_descriptor_t normal_block = ddf_api.read.block_descriptor(ddf_data, normal_block_id);

    memcpy(ddf_data + position_block.data_offset, cluster_vertexes,
           position_block.data_byte_count);
    memcpy(ddf_data + normal_block.data_offset, cluster_vertex_normals,
           normal_block.data_byte_count);
    // Now convert the indexes to offsets within a cluster, with each cluster having a base index.
    // Pack the offsets from the base vertex to uint10x3 format.
    dd_mesh_cluster_data_t* const cluster_datas =
        (dd_mesh_cluster_data_t*)(ddf_data + cluster_block.data_offset);
    uint32* const cluster_indexes_packed_begin = (uint32*)(ddf_data + index_block.data_offset);
    uint32* cluster_indexes_packed             = cluster_indexes_packed_begin;

    dd_mesh_cluster_data_t* ddf_cluster_data =
        (dd_mesh_cluster_data_t*)(ddf_data + cluster_block.data_offset);

    uint32 max_distance = 0;
    for (idx_t ip = 0; ip < cluster_count; ++ip) {
      uint32* const cluster_start_index = cluster_indexes + ip * cluster_size * 3;
      uint32* const cluster_end_index   = cluster_indexes + (ip + 1) * cluster_size * 3;
      uint32* cluster_index             = cluster_start_index;

      uint32 min_index = *cluster_index;
      uint32 max_index = *cluster_index;
      while (cluster_index != cluster_end_index) {
        min_index = min(min_index, (uint32)*cluster_index);
        max_index = max(max_index, (uint32)*cluster_index);
        ++cluster_index;
      }

      // TODO(Rick): calculate average normal of cluster.
      float3 n                            = cluster_vertex_normals[*cluster_start_index];
      dd_mesh_cluster_data_t cluster_data = {.base_vertex = min_index,
                                             .base_index  = cluster_size * ip,
                                             .padding_A   = (int32)(n.x * 10000),
                                             .padding_B   = (int32)(n.z * 10000)};
      cluster_datas[ip]                   = cluster_data;
      ddf_cluster_data[ip]                = cluster_data;

      // Adjust indexes so they are offsets from the cluster.base_vertex.
      cluster_index = cluster_start_index;
      while (cluster_index != cluster_end_index) {
        uint32 relative_index_0 = cluster_index[0] - min_index;
        uint32 relative_index_1 = cluster_index[1] - min_index;
        uint32 relative_index_2 = cluster_index[2] - min_index;
        *(cluster_indexes_packed) =
            pack_uint10x3(relative_index_0, relative_index_1, relative_index_2);

        uint32 triangle_indexes[3];
        unpack_uint10x3(*(cluster_indexes_packed), triangle_indexes + 0, triangle_indexes + 1,
                        triangle_indexes + 2);

        assert(triangle_indexes[0] == relative_index_0);
        assert(triangle_indexes[1] == relative_index_1);
        assert(triangle_indexes[2] == relative_index_2);

        cluster_indexes_packed++;
        cluster_index += 3;
      }

      uint32 distance = max_index - min_index;
      max_distance    = max(max_distance, distance);
    }
    assert(max_distance < 1024);

    array_free(cluster_vertexes, temp_allocator);
    array_free(cluster_vertex_normals, temp_allocator);

    // Check
    if (0) {
      for (uint32 iv = 0; iv < 320; ++iv) {
        uint32 base_vertex              = cluster_datas[iv / (cluster_size * 3)].base_vertex;
        uint32 triangle_index2          = iv / 3;
        uint32 vertex_in_triangle_index = iv % 3;

        uint32 packed_triangle_indexes = cluster_indexes_packed_begin[triangle_index2];
        uint32 triangle_indexes[3];
        unpack_uint10x3(packed_triangle_indexes, triangle_indexes + 0, triangle_indexes + 1,
                        triangle_indexes + 2);

        uint32 vertex_index = base_vertex + triangle_indexes[vertex_in_triangle_index];
        assert(vertex_index == (uint32)cluster_indexes[iv]);
        dd_devprintf("%u %u/%u\n", iv, vertex_index, (uint32)cluster_indexes[iv]);
      }
    }

    array_free(mesh_data, allocator);

    if (0) {
      FILE* data_file = fopen("temp_result_fastprocessing.bin", "wb");
      fwrite(ddf_data, 1, array_count(ddf_data), data_file);
      fclose(data_file);
    }

    dd_devprintf("After processing have %i clusters\n", (int)cluster_count);
    dd_devprintf("After processing have %i verts\n", (int)clustered_vertex_count);

    return ddf_data;
  }
}

static const struct dd_metis_api dd_metis_api = {.clusterize_mesh = &metis_clusterize_mesh};

#if defined(_MSC_VER)
  #define DD_DLL_EXPORT __declspec(dllexport)
#else
  #define DD_DLL_EXPORT
#endif

#include "core/api_registry.h"
#include "core/plugins.h"
DD_DLL_EXPORT bool plugin_link_apis(const dd_api_registry_t* registry,
                                    struct dd_allocator* allocator) {
  // Don't depend on anything, so simply register.
  registry->register_api("metis", 1, &dd_metis_api, sizeof(dd_metis_api));

  return true;
}
