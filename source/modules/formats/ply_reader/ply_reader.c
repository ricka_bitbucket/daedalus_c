#include "ply_reader.h"

#include <assert.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#include "core/containers.h"
#include "core/dd_string.h"
#include "core/filesystem.h"

void parse_ascii_data(const dd_stringview_t* data_rows, dd_mesh_t* out_mesh) {
  uint8* out_data   = (uint8*)out_mesh;
  uint32* indexes   = (uint32*)(out_data + out_mesh->index_offset);
  float4* positions = (float4*)(out_data + out_mesh->vertex_offset);

  size_t ir                   = 0;
  const size_t end_vertex_row = ir + out_mesh->vertex_count;
  size_t iv                   = 0;
  float3 bb_min               = {FLT_MAX, FLT_MAX, FLT_MAX};
  float3 bb_max               = {FLT_MIN, FLT_MIN, FLT_MIN};
  for (; ir < end_vertex_row; ++ir, ++iv) {
    dd_stringview_t row = data_rows[ir];
    const char* r       = row.string;
    positions[iv].x     = (float)strtod(r, (char**)&r);
    positions[iv].y     = (float)strtod(r, (char**)&r);
    positions[iv].z     = (float)strtod(r, (char**)&r);
    bb_min.x            = min(bb_min.x, positions[iv].x);
    bb_min.y            = min(bb_min.y, positions[iv].y);
    bb_min.z            = min(bb_min.z, positions[iv].z);
    bb_max.x            = max(bb_max.x, positions[iv].x);
    bb_max.y            = max(bb_max.y, positions[iv].y);
    bb_max.z            = max(bb_max.z, positions[iv].z);
  }

  dd_devprintf("Model BB %f,%f,%f - %f,%f,%f\n", bb_min.x, bb_min.y, bb_min.z, bb_max.x, bb_max.y,
               bb_max.z);

  const size_t end_face_row = ir + out_mesh->index_count / 3;
  size_t ii                 = 0;
  for (; ir < end_face_row; ++ir) {
    dd_stringview_t row         = data_rows[ir];
    const char* r               = row.string;
    long index_count_in_polygon = strtol(r, (char**)&r, 10);
    if (index_count_in_polygon == 3) {
      indexes[ii++] = (uint32)strtol(r, (char**)&r, 10);
      indexes[ii++] = (uint32)strtol(r, (char**)&r, 10);
      indexes[ii++] = (uint32)strtol(r, (char**)&r, 10);
    }
  }
}

#define SWAP_ENDIAN(a)                 \
  {                                    \
    uint8 tmp       = ((uint8*)&a)[0]; \
    ((uint8*)&a)[0] = ((uint8*)&a)[3]; \
    ((uint8*)&a)[3] = tmp;             \
    tmp             = ((uint8*)&a)[1]; \
    ((uint8*)&a)[1] = ((uint8*)&a)[2]; \
    ((uint8*)&a)[2] = tmp;             \
  }

uint8* load_from_file_path(const dd_stringview_t file_path, struct dd_allocator* allocator) {
  if (!dd_filesystem_api.file_exists(file_path.string)) {
    return NULL;
  }

  dd_allocator* temp_allocator = allocator;

  array(uint8) file_data = read_file(file_path.string, temp_allocator);
  assert(file_data);

  dd_stringview_t file_string = {(char*)file_data, array_count(file_data)};

  array(dd_stringview_t) header_and_data =
      stringview_split(file_string, stringview("end_header"), temp_allocator);

  // Should have exactly 1 header, and 1 block of data
  if (array_count(header_and_data) != 2) return NULL;

  dd_stringview_t header = header_and_data[0];
  // Try different new line types to find the correct one.
  const dd_stringview_t possible_end_lines[] = {stringview("\n"), stringview("\r"),
                                                stringview("\r\n")};
  int endline_index                          = -1;
  for (int ie = 0; ie < countof(possible_end_lines); ++ie) {
    const dd_stringview_t result = stringviewstr(header, possible_end_lines[ie]);

    if (result.length > 0) {
      endline_index = ie;
    }
  }

  array(dd_stringview_t) header_rows =
      stringview_split(header, possible_end_lines[endline_index], temp_allocator);
  const size_t row_count = array_count(header_rows);
  size_t vertex_count    = 0;
  size_t triangle_count  = 0;
  size_t ir;

  bool is_ascii = true;
  for (ir = 0; ir < row_count; ++ir) {
    dd_stringview_t row = header_rows[ir];
    dd_stringview_t first =
        stringview_pop_front(&row, stringview_chr(row, ' ').string - row.string + 1);
    if (are_strings_equal(first, stringview("format "))) {
      dd_stringview_t type =
          stringview_pop_front(&row, stringview_chr(row, ' ').string - row.string + 1);
      if (are_strings_equal(type, stringview("ascii "))) {
        // All good
      } else if (are_strings_equal(type, stringview("binary_little_endian "))) {
        // TODO(Rick): unable to parse this format at the moment. Lucy has binary data for example.
        return NULL;
      } else if (are_strings_equal(type, stringview("binary_big_endian "))) {
        // Works on PC endianness
        is_ascii = false;
      }
    } else if (are_strings_equal(first, stringview("element "))) {
      dd_stringview_t type =
          stringview_pop_front(&row, stringview_chr(row, ' ').string - row.string + 1);
      if (are_strings_equal(type, stringview("vertex "))) {
        vertex_count = atoi(row.string);
      } else if (are_strings_equal(type, stringview("face "))) {
        triangle_count = atoi(row.string);
      }
    }
  }

  array(uint8) out = NULL;

  dd_mesh_t out_mesh = {
      .index_count   = (uint32)(triangle_count * 3),
      .vertex_count  = (uint32)(vertex_count),
      .index_offset  = (uint32)(sizeof(dd_mesh_t)),
      .vertex_offset = (uint32)(sizeof(dd_mesh_t) + triangle_count * 3 * sizeof(uint32))};
  array_append(out, &out_mesh, sizeof(out_mesh), allocator);
  size_t total_byte_count = out_mesh.vertex_offset + vertex_count * sizeof(float4);
  array_resize(out, total_byte_count, allocator);

  if (is_ascii) {
    assert(vertex_count < 0xFFFF);
    array(dd_stringview_t) data_rows =
        stringview_split(header_and_data[1], possible_end_lines[endline_index], temp_allocator);
    parse_ascii_data(data_rows, (dd_mesh_t*)out);
    array_free(data_rows, temp_allocator);
  } else {
    dd_stringview_t ply_data = header_and_data[1];
    while (ply_data.string[0] == '\n' || ply_data.string[0] == '\r') {
      ply_data.string++;
      ply_data.length--;
    }

    // Armadillo contains intensity before index count + indexes
    const size_t vertex_byte_count = vertex_count * sizeof(float3);
    const size_t index_byte_count  = (triangle_count * (sizeof(uint8)
#ifdef ARMADILLO
                                                       + sizeof(uint8)
#endif
                                                       + sizeof(int) * 3));
    const size_t expected_data_length = index_byte_count + vertex_byte_count;
    if (ply_data.length == expected_data_length) {
      float4* const out_vertexes = (float4*)(out + out_mesh.vertex_offset);

      array(float3) parsed_vertexes = NULL;
      array_resize(parsed_vertexes, vertex_count, temp_allocator);
      memcpy(parsed_vertexes, ply_data.string, vertex_count * sizeof(float3));

      float3 bb_min = {parsed_vertexes[0].x, parsed_vertexes[0].y, parsed_vertexes[0].z};
      SWAP_ENDIAN(bb_min.x);
      SWAP_ENDIAN(bb_min.y);
      SWAP_ENDIAN(bb_min.z);
      float3 bb_max = bb_min;
      for (size_t iv = 0; iv < vertex_count; ++iv) {
        float x = parsed_vertexes[iv].x;
        float y = parsed_vertexes[iv].y;
        float z = parsed_vertexes[iv].z;
        SWAP_ENDIAN(x);
        SWAP_ENDIAN(y);
        SWAP_ENDIAN(z);
        bb_min.x         = min(bb_min.x, x);
        bb_min.y         = min(bb_min.y, y);
        bb_min.z         = min(bb_min.z, z);
        bb_max.x         = max(bb_max.x, x);
        bb_max.y         = max(bb_max.y, y);
        bb_max.z         = max(bb_max.z, z);
        out_vertexes[iv] = (float4){x, y, z, 1};
      }

      array_free(parsed_vertexes, temp_allocator);

      // float y_min = -100;
      // bb_max.y - (bb_max.y - bb_min.y) / 20.f;
      dd_devprintf("Model BB %f,%f,%f - %f,%f,%f\n", bb_min.x, bb_min.y, bb_min.z, bb_max.x,
                   bb_max.y, bb_max.z);
      ply_data.string += vertex_byte_count;
      ply_data.length -= vertex_byte_count;

      uint32* const first_index_out = (uint32*)(out + out_mesh.index_offset);
      uint32* index_out             = first_index_out;
      const uint8* index_data       = (const uint8*)ply_data.string;
      for (size_t ii = 0; ii < triangle_count; ++ii) {
#ifdef ARMADILLO
        ++index_data;
#endif
        assert(index_data[0] == 3);
        uint32 a, b, c;
        a = *(uint32*)(index_data + 1);
        b = *(uint32*)(index_data + 5);
        c = *(uint32*)(index_data + 9);
        SWAP_ENDIAN(a);
        SWAP_ENDIAN(b);
        SWAP_ENDIAN(c);
        assert(a < vertex_count);
        assert(b < vertex_count);
        assert(c < vertex_count);

        /*if ((out_vertex_data[a].y < y_min) || (out_vertex_data[b].y < y_min) ||
            (out_vertex_data[c].y < y_min)) */
        {
          index_out[0] = a;
          index_out[1] = b;
          index_out[2] = c;
          index_out += 3;
          index_data += sizeof(uint8) + 3 * sizeof(uint32);
        }
      }
      out_mesh.index_count = (uint32)(index_out - first_index_out);
    }
  }

  array_free(header_and_data, temp_allocator);
  array_free(header_rows, temp_allocator);
  array_free(file_data, temp_allocator);

  return out;
}

const struct dd_ply_reader_api dd_ply_reader = {.load_from_file_path = &load_from_file_path};

#if defined(_MSC_VER)
  #define DD_DLL_EXPORT __declspec(dllexport)
#else
  #define DD_DLL_EXPORT
#endif

#include "core/api_registry.h"
#include "core/plugins.h"

DD_DLL_EXPORT bool plugin_link_apis(const dd_api_registry_t* registry,
                                    struct dd_allocator* allocator) {
  // Don't depend on anything, so simply register.
  registry->register_api("ply_reader", 1, &dd_ply_reader, sizeof(dd_ply_reader));

  return true;
}
