#pragma once

#include "core/types.h"

struct dd_allocator;

typedef struct dd_mesh_t {
  uint32 index_count;
  uint32 vertex_count;

  // Offsets are from start of array
  uint32 index_offset;
  uint32 vertex_offset;

  // Data follows here.
  // uint32 index[index_count]
  // float4 positions [vertex_count]
} dd_mesh_t;

struct dd_ply_reader_api {
  // Returns blob of data. Caller is responsible for freeing it.
  uint8* (*load_from_file_path)(const dd_stringview_t file_path, struct dd_allocator* allocator);
};
