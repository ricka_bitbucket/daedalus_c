echo off

IF "%VULKAN_SDK%"=="" (
  ECHO Vulkan not installed, doing that now
  ECHO Downloading Vulkan ...
  powershell.exe -Command "Invoke-WebRequest -OutFile ./vulkan_installer.exe https://sdk.lunarg.com/sdk/download/1.2.154.1/windows/VulkanSDK-1.2.154.1-Installer.exe?u="
  ECHO Installing Vulkan ...
  vulkan_installer.exe /S
  del vulkan_installer.exe
) ELSE (
  ECHO Vulkan already installed at %VULKAN_SDK%
)

call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat"
echo on

set COMPILE_CONSTRUCT_COMMAND=cl.exe /D_MBCS /EHsc /FC /Fe: cconstruct.exe /nologo 

%COMPILE_CONSTRUCT_COMMAND% config.cc
if %errorlevel% neq 0 exit /b %errorlevel%

cconstruct.exe --generate-projects
if %errorlevel% neq 0 exit /b %errorlevel%

msbuild build/daedalus.sln
