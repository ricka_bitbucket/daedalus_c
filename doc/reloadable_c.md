# Hot-reloading C code.

Code must go into dynamic library (DLL/dynlib/)

When loading a new version, need to update instance of interface struct, so need to get that from DLL. DLL wouldn't need to do other initialization if data structures haven't changed, since the data remains valid (assuming same allocator between old/new version of DLL, or if all allocated in main process (through custom memory API))

On windows, the embedded PDB file path is replaced by the loading code to point at the new file path. This only works if the file path is shorter or equal in length. To 'solve this' the extension of the dll is changed to be mydll.dXX and the pdb to match mydll.pXX which can store 99 versions after which the code needs to wrap around (could go higher is allowing hex revisions).

# TODO

- [] Find a way to prevent a DLL accidentally using the registry from the core lib. Maybe disable the global instance if a fixed DLL export define is used.
