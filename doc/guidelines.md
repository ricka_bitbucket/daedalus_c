# UI
1. Check hover region at beginning of component.

   _Justification_: allow children to take precedence for hover interaction.

   _Code_:

   ```c++
   if (rect_contains_point(rect, ui->mouse.x, ui->mouse.y)) {
     dd_ui_set_next_hover(ui, ui->current_ui_layer, id);
   }
   ```
1. Zero an input event after handling it.
   
   _Justification_: prevent multiple components from handling the same event.

   _Example_: If you `drawTable(..)`, which calls to `drawRow(..)`, the delete key should maybe be handled by a row if it's being edited, but by the table if the row isn't. Zeroing the event in `drawRow(..)` prevents it being handled in `drawTable(..)`.

1. Handle input at the end of a function, not at the beginning.
   
   _Justification_: child components need the opportunity to handle inputs before their parents do so.

   _Example_: If you `drawTable(..)`, which calls to `drawRow(..)`, the delete key should maybe be handled by a row if it's being edited, but by the table if the row isn't. Handling the input at the beginning of the table function would be wrong.

# Links
https://ourmachinery.com/files/guidebook.md.html#omg-codeorg:codeorganization/omg-codeorg-2:headerfiles(.h)shouldbewritteninc11

http://bitsquid.blogspot.com/2010/09/custom-memory-allocation-in-c.html

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

Links for timers:
- 
https://arstechnica.com/civis/viewtopic.php?f=20&t=240425
http://www.songho.ca/misc/timer/timer.html
http://codearcana.com/posts/2013/05/15/a-cross-platform-monotonic-timer.html
https://gist.github.com/ForeverZer0/0a4f80fc02b96e19380ebb7a3debbee5