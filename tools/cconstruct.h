/*
 * Windows with Visual Studio
 * ==========================
 * 1a. Open a 'Developer Command Prompt' or
 * 1b. In an existing prompt call 'C:\Program Files (x86)\Microsoft Visual
 * Studio\2019\Community\Common7\\Tools\\VsDevCmd.bat' or similar
 * 2. Compile cconstruct exectuble with the following command
 *      'cl.exe /FC /Fo%TEMP% /Fecconstruct.exe your_config.cc'
 * 3. Run the resulting cconstruct.exe, which will regenerate itself from the config file
 *    and then construct the project files you specified in the config.
 *
 * After this you don't need the developer prompt anymore, the cconstruct.exe binary
 * stores the proper settings.
 *
 * MacOS with Clang
 * ==========================
 * 1. Open a Terminal
 * 2. Compile cconstruct exectuble with the following command
 *      'clang -x c your_config.cc -o cconstruct'
 * 3. Run the resulting cconstruct, which will regenerate itself from the config file
 *    and then construct the project files you specified in the config.
 */
#ifndef CC_CONSTRUCT_H
#define CC_CONSTRUCT_H

#pragma warning(disable : 4996)

#include <stdbool.h>

enum ErrorCodes {
  ERR_NO_ERROR      = 0,
  ERR_COMPILING     = 1,  // An error compiling the new CConstruct binary
  ERR_CONSTRUCTION  = 2,  // An error running the CConstruct binary to construct projects
  ERR_CONFIGURATION = 3,  // An error in the configuration. Check error output for more details
};

typedef enum {
  CCProjectTypeConsoleApplication = 0,
  CCProjectTypeWindowedApplication,
  CCProjectTypeStaticLibrary,
  CCProjectTypeDynamicLibrary,
} EProjectType;
typedef enum {
  EArchitectureX86 = 0,
  EArchitectureX64,
  EArchitectureARM,
} EArchitecture;
typedef enum {
  EPlatformDesktop = 0, /* MacOS, Windows */
  EPlatformPhone,       /* iOS */
} EPlatform;
typedef enum {
  EStateWarningLevelDefault = 0,
  EStateWarningLevelHigh    = EStateWarningLevelDefault,
  EStateWarningLevelMedium,
  EStateWarningLevelLow,
  EStateWarningLevelAll,   // This really enables everything, not recommended for production use
  EStateWarningLevelNone,  // This really disables everything, not recommended for production use
} EStateWarningLevel;

// Opaque handles in public interface
typedef struct cc_project_impl_t* cc_project_t;
typedef void* cc_group_t;
typedef struct cc_architecture_impl_t* cc_architecture_t;
typedef struct cc_architecture_impl_t* cc_platform_t;
typedef struct cc_configuration_impl_t* cc_configuration_t;
typedef struct cc_state_impl_t* cc_state_t;

typedef struct cconstruct_t {
  struct {
    cc_configuration_t (*create)(const char* in_label);
  } configuration;

  struct {
    cc_architecture_t (*create)(EArchitecture in_type);
  } architecture;

  struct {
    cc_platform_t (*create)(EPlatform in_type);
  } platform;

  struct {
    /* Create a group/folder in the workspace, at any level (files and projects can go into
     * groups).
     *
     * @param in_parent may be NULL, the parent will then be whatever is applicable at that level
     * (workspace/project)
     */
    cc_group_t (*create)(const char* in_group_name, const cc_group_t in_parent_group);
  } group;

  const struct {
    cc_state_t (*create)();

    void (*reset)(cc_state_t in_out_state);
    void (*addIncludeFolder)(cc_state_t in_out_state, const char* in_include_folder);
    void (*addPreprocessorDefine)(cc_state_t in_out_state, const char* in_define);
    void (*addCompilerFlag)(cc_state_t in_out_state, const char* in_compiler_flag);
    void (*addLinkerFlag)(cc_state_t in_out_state, const char* in_linker_flag);

    /* Add dependency on external library or framework.
     */
    void (*linkExternalLibrary)(cc_state_t in_out_state, const char* in_external_library_path);

    void (*setWarningLevel)(cc_state_t in_out_state, EStateWarningLevel in_level);
    void (*disableWarningsAsErrors)(cc_state_t in_out_state);
  } state;

  const struct {
    cc_project_t (*create)(const char* in_project_name, EProjectType in_project_type,
                           const cc_group_t in_parent_group);

    /* Add files to a project. .c/.cpp files are automatically added to be compiled, everything
     * else is treated as a header file.
     */
    void (*addFiles)(cc_project_t in_project, unsigned num_files, const char* in_file_names[],
                     const cc_group_t in_parent_group);
    void (*addFilesFromFolder)(cc_project_t in_project, const char* folder, unsigned num_files,
                               const char* in_file_names[], const cc_group_t in_parent_group);

    void (*addFileWithCustomCommand)(cc_project_t in_project, const char* in_file_names,
                                     const cc_group_t in_parent_group, const char* command,
                                     const char* in_output_file_path);

    /* Add dependency between projects. This will only link the dependency and set the build order
     * in the IDE. It will *NOT* automatically add include-folders.
     */
    void (*addInputProject)(cc_project_t target_project, const cc_project_t on_project);

    /* Add dependency between projects. This will only set the build order in the IDE. It will
     * *NOT* automatically add include-folders or link the earlier project.
     */
    void (*setBuildOrder)(cc_project_t earlier_project, const cc_project_t later_project);

    /* Set state on a project.
     *
     * @param in_platform may be NULL if state is for all architectures
     * @param in_configuration may be NULL if state is for all configurations
     */
    void (*setFlags)(cc_project_t in_out_project, const cc_state_t in_state,
                     cc_architecture_t in_architecture, cc_configuration_t in_configuration);

    /* Add a command line instruction to execute before the build is started.
     */
    void (*addPreBuildAction)(cc_project_t in_out_project, const char* in_action_command);

    /* Add a command line instruction to execute after the build has finished successfully.
     */
    void (*addPostBuildAction)(cc_project_t in_out_project, const char* in_action_command);

    /* Output folder for project build results (default:"${platform}/${configuration}")
     */
    void (*setOutputFolder)(cc_project_t in_out_project, const char* in_output_folder);
  } project;

  const struct {
    /* Label of the workspace, used for filename of the workspace.
     */
    void (*setLabel)(const char* label);

    /* Add a configuration
     */
    void (*addConfiguration)(const cc_configuration_t in_configuration);
    void (*addArchitecture)(const cc_architecture_t in_architecture);
    void (*addPlatform)(const cc_platform_t in_platform);

  } workspace;

} cconstruct_t;

extern void (*cc_default_generator)(const char* workspace_folder);

/* Call this once at the start of your main config file. You can usually call it with __FILE__, and
 * simply forward argc and argv from the main function.
 */
cconstruct_t cc_init(const char* in_absolute_config_file_path, int argc, const char* const* argv);

/***********************************************************************************************************************
 *                                             Implementation starts here
 ***********************************************************************************************************************/

#if defined(_WIN32)
  // First
  #include <Windows.h>
  // Later
  #include <DbgHelp.h>
  #include <direct.h>
  #include <errno.h>
  #include <malloc.h>
  #include <tchar.h>
  #pragma comment(lib, "DbgHelp.lib")
#else
  #include <err.h>
  #include <errno.h>
  #include <signal.h>
  #include <sys/stat.h>
  #include <sys/types.h>
  #include <unistd.h>
#endif

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Tools
// clang-format off
#if defined(_WIN32)
void PrintStrackFromContext(PCONTEXT Context) {
  SymSetOptions(SYMOPT_LOAD_LINES);
  SymInitialize(GetCurrentProcess(), NULL, TRUE);

  HANDLE process_handle = GetCurrentProcess();

  DWORD machine_type;
  STACKFRAME64 stack_frame = {0};

  // Set up stack frame.
  #ifdef _M_IX86
  machine_type                 = IMAGE_FILE_MACHINE_I386;
  stack_frame.AddrPC.Offset    = Context->Eip;
  stack_frame.AddrFrame.Offset = Context->Ebp;
  stack_frame.AddrStack.Offset = Context->Esp;
  #elif _M_X64
  machine_type                 = IMAGE_FILE_MACHINE_AMD64;
  stack_frame.AddrPC.Offset    = Context->Rip;
  stack_frame.AddrFrame.Offset = Context->Rsp;
  stack_frame.AddrStack.Offset = Context->Rsp;
  #elif _M_IA64
  machine_type                  = IMAGE_FILE_MACHINE_IA64;
  stack_frame.AddrPC.Offset     = Context->StIIP;
  stack_frame.AddrFrame.Offset  = Context->IntSp;
  stack_frame.AddrBStore.Offset = Context->RsBSP;
  stack_frame.AddrBStore.Mode   = AddrModeFlat;
  stack_frame.AddrStack.Offset  = Context->IntSp;
  #else
    #error "Unsupported platform"
  #endif
  stack_frame.AddrPC.Mode    = AddrModeFlat;
  stack_frame.AddrFrame.Mode = AddrModeFlat;
  stack_frame.AddrStack.Mode = AddrModeFlat;

  char buffer[sizeof(SYMBOL_INFO) + MAX_SYM_NAME * sizeof(TCHAR)];
  PSYMBOL_INFO symbol  = (PSYMBOL_INFO)buffer;
  symbol->SizeOfStruct = sizeof(SYMBOL_INFO);
  symbol->MaxNameLen   = MAX_SYM_NAME;

  IMAGEHLP_LINE64 line = {0};
  line.SizeOfStruct    = sizeof(IMAGEHLP_LINE64);

  unsigned frame_count = 0;
  while (true) {
    if (!StackWalk64(machine_type, GetCurrentProcess(), GetCurrentThread(), &stack_frame,
                     machine_type == IMAGE_FILE_MACHINE_I386 ? NULL : Context, NULL,
                     SymFunctionTableAccess64, SymGetModuleBase64, NULL)) {
      // Maybe it failed, maybe we have finished walking the stack.
      break;
    }

    if (stack_frame.AddrPC.Offset != 0) {
      const DWORD64 address  = stack_frame.AddrPC.Offset;
      DWORD64 displacement64 = 0;
      DWORD displacement     = 0;

      if (SymFromAddr(process_handle, address, &displacement64, symbol)) {
        if (SymGetLineFromAddr64(process_handle, address, &displacement, &line)) {
          const bool has_passed_main_function = (strstr(line.FileName, "exe_common.inl") != NULL);
          if (has_passed_main_function) return;

          fprintf(stderr, "%i: %s[%s(%i)]\n", frame_count, symbol->Name, line.FileName,
                  line.LineNumber);
        } else {
          DWORD error = GetLastError();
          fprintf(stderr, "%i: %s   SymGetLineFromAddr64 returned error : %d\n", frame_count,
                  symbol->Name, error);
        }
      } else {
        DWORD error = GetLastError();
        fprintf(stderr, "SymFromAddr returned error : %d\n", error);
      }

    } else {
      // Base reached.
      break;
    }
  }
}
#else
void DoStack() {}
#endif
#define countof(a) sizeof(a) / sizeof(a[0])

bool cc_is_verbose          = false;
bool cc_only_generate       = false;
bool cc_generate_cc_project = false;  // Generate a project for the CConstruct config itself

#if defined(_MSC_VER)
void printStack(void);
void printStack(void) {
  #if 0
     unsigned int   i;
     void         * stack[ 100 ];
     unsigned short frames;
     SYMBOL_INFO  * symbol;
     HANDLE         process;

     process = GetCurrentProcess();

     
     SymInitialize( process, NULL, TRUE );

     SymSetOptions(SYMOPT_LOAD_LINES);

     frames               = CaptureStackBackTrace( 0, 100, stack, NULL );
     symbol               = ( SYMBOL_INFO * )calloc( sizeof( SYMBOL_INFO ) + 256 * sizeof( char ), 1 );
     symbol->MaxNameLen   = 255;
     symbol->SizeOfStruct = sizeof( SYMBOL_INFO );

     IMAGEHLP_LINE lineInfo = {0};
     for( i = 0; i < frames; i++ )
     {
         SymFromAddr( process, ( DWORD64 )( stack[ i ] ), 0, symbol );
         if( SymGetLineFromAddr(process, symbol->Address, 0, &lineInfo) ) {
           printf("%s(%i)\n", lineInfo.FileName, lineInfo.LineNumber);
         }

    #if defined(_WIN64)
         printf( "%i: %s - 0x%0llX\n", frames - i - 1, symbol->Name, symbol->Address );
    #else
         printf( "%i: %s - 0x%0X\n", frames - i - 1, symbol->Name, symbol->Address );
    #endif
     }

     free( symbol );
  #endif
}
#else
void printStack() {
  // Not yet implemented
}
#endif

#define LOG_ERROR_AND_QUIT(error, ...) \
  {                                    \
    fprintf(stderr, __VA_ARGS__);      \
    fprintf(stderr, "\n");             \
    printStack();                      \
    error_quit(error);                 \
  }
#define LOG_VERBOSE(...) \
  if (cc_is_verbose) fprintf(stdout, __VA_ARGS__)

void error_quit(int error_code) {
#if defined(_WIN32)
  // The program could have been run from the command prompt, or by double clicking on a previously
  // built CConstruct executable. In the first case, the output is readily visible. In the second
  // case keep the window open so users can read the error.
  DWORD p[2];
  int count = (int)GetConsoleProcessList((LPDWORD)&p, 2);
  if (count <= 1) {
    // Last one, or error querying, so probably started with a double-click. Should wait for the
    // user to read output.
    system("pause");
  }
#endif

  exit(error_code);
}

// Returns the extension of a file name (excluding the .).
// Returns "" if no extension is present.
const char* file_extension(const char* file_path) {
  const char* result = strrchr(file_path, '.');
  if (result) {
    return result + 1;
  } else {
    return "";
  }
}
bool is_header_file(const char* file_path) { return strstr(file_path, ".h") != 0; }
bool is_source_file(const char* file_path) {
  const char* ext = file_extension(file_path);

  const char* const source_extensions[] = {
      "c",     "cc", "cpp", /* C variant source files*/
      "m",     "mm",        /* Objective-C source files */
      "metal",              /* Apples Metal shading language */
  };
  for (unsigned i = 0; i < countof(source_extensions); i++) {
    if (strcmp(ext, source_extensions[i]) == 0) return true;
  }
  return false;
}
bool is_buildable_resource_file(const char* file_path) {
  const char* ext = file_extension(file_path);

  const char* const resource_extensions[] = {
      "storyboard",
      "xcassets",
  };
  for (unsigned i = 0; i < countof(resource_extensions); i++) {
    if (strcmp(ext, resource_extensions[i]) == 0) return true;
  }
  return false;
}

#if defined(_MSC_VER)

int make_folder(const char* folder_path) {
  char buffer[1024] = {0};

  const char* next_sep = folder_path;
  while ((next_sep = strchr(next_sep, '/')) != NULL) {
    strncpy(buffer, folder_path, next_sep - folder_path);
    int result = _mkdir(buffer);
    if (result != 0) {
      if (errno != EEXIST) return errno;
    }
    next_sep += 1;
  }
  int result = _mkdir(folder_path);
  if (result != 0) {
    if (errno != EEXIST) return errno;
  }
  return 0;
}
#else

int make_folder(const char* folder_path) {
  char buffer[1024] = {0};

  const char* next_sep = folder_path;
  while ((next_sep = strchr(next_sep, '/')) != NULL) {
    strncpy(buffer, folder_path, (size_t)(next_sep - folder_path));
    if (buffer[0]) {
      int result = mkdir(buffer, 0777);
      if (result != 0) {
        if (errno != EEXIST) return errno;
      }
    }
    next_sep += 1;
  }
  int result = mkdir(folder_path, 0777);
  if (result != 0) {
    if (errno != EEXIST) return errno;
  }
  return 0;
}
#endif

static uintptr_t cc_next_free           = (uintptr_t)NULL;
static uintptr_t cc_end_next_free       = (uintptr_t)NULL;
static size_t cc_total_bytes_allocated_ = 0;

void* cc_alloc_(size_t size) {
  cc_total_bytes_allocated_ += size;
  if ((cc_end_next_free - cc_next_free) < size) {
    // Doesn't fit, allocate a new block
    size_t byte_count = 1024 * 1024;
    if (size > byte_count) byte_count = size;
    cc_next_free     = (uintptr_t)malloc(byte_count);
    cc_end_next_free = cc_next_free + byte_count;
  }

  void* out = (void*)cc_next_free;
  cc_next_free += size;
  return out;
}

typedef struct array_header_t {
  unsigned count_;
  unsigned capacity_;
} array_header_t;

void* array_grow(void* a, unsigned element_size);

#define array_header(a) ((a) ? ((array_header_t*)((char*)(a) - sizeof(array_header_t))) : 0)

#define array_count(a) ((a) ? array_header(a)->count_ : 0)
#define array_capacity(a) ((a) ? array_header(a)->capacity_ : 0)
#define array_reset(a) ((a) ? array_header(a)->count_ = 0 : 0)
#define array_full(a) ((a) ? (array_header(a)->count_ == array_header(a)->capacity_) : true)

#define array_push(a, item)                                                \
  (array_full(a) ? (*((void**)&a) = array_grow((void*)a, sizeof(*a))) : 0, \
   (a[array_header(a)->count_++] = item))

#define array_remove_at_index(a, idx)                                                       \
  memmove((void*)(a + idx), (void*)(a + idx + 1), (array_count(a) - idx - 1) * sizeof(*a)), \
      array_header(a)->count_--

#define array_append(a, data, count)                                                            \
  (((array_count(a) + (count)) > array_capacity(a))                                             \
       ? (*((void**)&(a)) = array_reserve((a), sizeof(*(a)),                                    \
                                          (array_count(a) + (count)) > (array_count(a) * 3 / 2) \
                                              ? (array_count(a) + (count))                      \
                                              : (array_count(a) * 3 / 2)))                      \
       : 0,                                                                                     \
   (memcpy(a + array_count(a), data, ((count) + 0) * sizeof(*a))),                              \
   (array_header(a)->count_ = array_count(a) + (count)))

void* array_grow(void* a, unsigned element_size) {
  unsigned prev_capacity      = 0;
  unsigned prev_count         = 0;
  array_header_t* prev_header = array_header(a);
  if (prev_header) {
    prev_capacity = prev_header->capacity_;
    prev_count    = prev_header->count_;
  }

  unsigned new_capacity = (unsigned)(prev_capacity * 1.75);
  if (new_capacity == prev_capacity) {
    new_capacity = prev_capacity + 8;
  }

  array_header_t* new_header =
      (array_header_t*)cc_alloc_(element_size * new_capacity + sizeof(array_header_t));
  void* out             = new_header + 1;
  new_header->count_    = prev_count;
  new_header->capacity_ = new_capacity;
  memcpy(out, a, element_size * prev_count);

  // At this point could free the prev data, but not doing so in CConstruct

  return out;
}

void* array_reserve(void* a, unsigned element_size, unsigned new_capacity) {
  unsigned prev_capacity      = 0;
  unsigned prev_count         = 0;
  array_header_t* prev_header = array_header(a);
  if (prev_header) {
    prev_capacity = prev_header->capacity_;
    prev_count    = prev_header->count_;
  }

  if (new_capacity > prev_capacity) {
    array_header_t* new_header =
        (array_header_t*)cc_alloc_(element_size * new_capacity + sizeof(array_header_t));
    void* out             = new_header + 1;
    new_header->count_    = prev_count;
    new_header->capacity_ = new_capacity;
    memcpy(out, a, element_size * prev_count);

    // At this point could free the prev data, but not doing so in CConstruct

    return out;
  } else {
    return a;
  }
}

#if !defined(_MSC_VER)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wformat-nonliteral"
#endif
const char* cc_printf(const char* format, ...) {
  unsigned length = (unsigned)strlen(format);

  // Guess length of output format
  unsigned alloc_size = (2 * length > 256) ? 2 * length : 256;

  char* out = (char*)cc_alloc_(alloc_size);

  va_list args;

  va_start(args, format);
  unsigned output_length = (unsigned)vsnprintf(out, alloc_size - 1, format, args);
  va_end(args);

  if (output_length >= alloc_size) {
    alloc_size = output_length + 1;
    out        = (char*)cc_alloc_(alloc_size);
    va_start(args, format);
    vsprintf(out, format, args);
    va_end(args);
  }

  out[alloc_size - 1] = 0;
  return out;
}
#if !defined(_MSC_VER)
  #pragma clang diagnostic pop
#endif

const char* cc_substitute(const char* in_original, const char** keys, const char** values,
                          unsigned num_keys) {
  char key_search[128] = {0};

  const char* in = in_original;
  for (unsigned i = 0; i < num_keys; ++i) {
    char* out_string = {0};
    out_string       = (char*)array_reserve(out_string, sizeof(*out_string), 128);

    const char* key = keys[i];
    snprintf(key_search, sizeof(key_search) - 1, "${%s}", key);
    const char* value       = values[i];
    const char* offset      = in;
    const char* piece_start = in;
    while ((offset = strstr(offset, key_search)) != 0) {
      array_append(out_string, piece_start, (unsigned)(offset - piece_start));
      array_append(out_string, value, (unsigned)strlen(value));
      offset      = offset + strlen(key_search);
      piece_start = offset;
    }
    array_append(out_string, piece_start, (unsigned)strlen(piece_start));
    array_push(out_string, 0);
    // Breaking connection to stretchy buffer but that's ok, since we're not tracking memory in
    // CConstruct.
    in = out_string;
  }

  return in;
}

const char** string_array_clone(const char** in) {
  char** out                   = {0};
  const array_header_t* header = array_header(in);
  if (header && header->count_ > 0) {
    array_header_t* out_header =
        (array_header_t*)cc_alloc_(sizeof(array_header_t) + sizeof(const char*) * header->count_);
    out_header->count_ = out_header->capacity_ = header->count_;
    out                                        = (char**)(out_header + 1);
    memcpy(out, in, sizeof(const char*) * header->count_);
  }
  return (const char**)out;
}

/* Strip path information returning only the filename (with extension) */
const char* strip_path(const char* path) {
  const char* last_slash = strrchr(path, '/');
  if (last_slash)
    return last_slash + 1;
  else
    return path;
}

char* make_uri(const char* in_path) {
  // Win32 api PathCanonicalize only handles '\' instead of '/', doesn't handle multiple // next to
  // each other
  char* uri = (char*)cc_printf("%s", in_path);
  char* c   = uri;
  while (*c) {
    if (*c == '\\') {
      *c = '/';
    }
    c++;
  }

  // Remove parent paths, start at offset 3 since can't do anything about a path starting with ../
  if (uri[0] && uri[1] && uri[2]) {
    char* uri_read  = uri + 3;
    char* uri_write = uri + 3;
    while (*uri_read) {
      bool is_double_slash = (uri_read[0] == '/') && (uri_read[1] == '/');
      if (is_double_slash) {
        uri_read++;
      }
      bool is_parent_path = (uri_read[0] == '/') && (uri_read[1] == '.') && (uri_read[2] == '.') &&
                            (uri_read[3] == '/');
      if (is_parent_path) {
        // Backtrack uri_write
        uri_read = uri_read + 3;
        uri_write--;
        while (*uri_write != '/') {
          uri_write--;
        }
      }
      *uri_write = *uri_read;
      uri_write++;
      uri_read++;
    }
    *uri_write = 0;
  }

  return uri;
}

/* Remove filename from path, and return the folder
 *
 * @return guaranteed to end in '/', and data is copied so may be modified
 */
const char* folder_path_only(const char* in_path) {
  char* uri        = make_uri(in_path);
  char* last_slash = strrchr(uri, '/');
  if (last_slash) {
    *(last_slash + 1) = 0;
    return uri;
  } else {
    return "./";
  }
}

/* This function isn't yet super efficient
 *
 * @param in_base_folder is assumed to be an absolute folder path
 */
char* make_path_relative(const char* in_base_folder, const char* in_change_path) {
  // Create copies of the paths so they can be modified
  char* base_folder = make_uri(in_base_folder);
  char* change_path = make_uri(in_change_path);

  char* output = NULL;

  bool is_absolute_path = (change_path[0] == '/') || (change_path[1] == ':');
  if (!is_absolute_path) {
    output = change_path;
  } else {
    // Find common root
    char* end_common_root = base_folder;
    while ((tolower(*end_common_root) == tolower(*change_path)) && *end_common_root &&
           *change_path) {
      end_common_root++;
      change_path++;
    }

    // Count remaining / in end_common_root to determine levels to go up from base
    unsigned num_folders = *end_common_root ? 1 : 0;
    while (*end_common_root) {
      if (*end_common_root == '/') {
        num_folders++;
      }
      end_common_root++;
    }
    bool should_not_count_trailing_slash = num_folders > 0 && *(end_common_root - 1) == '/';
    if (should_not_count_trailing_slash) {
      num_folders--;
    }

    char* parent_folders = (char*)cc_alloc_(3 * num_folders + 1);
    for (unsigned i = 0; i < num_folders; i++) {
      parent_folders[3 * i + 0] = '.';
      parent_folders[3 * i + 1] = '.';
      parent_folders[3 * i + 2] = '/';
    }
    parent_folders[3 * num_folders] = 0;
    output                          = (char*)cc_printf("%s%s", parent_folders, change_path);
  }

  return output;
}

const char* str_strip_spaces(const char* in) {
  static char project_name_nospaces[512];
  char* po       = project_name_nospaces;
  const char* pi = in;
  while (*pi) {
    if (*pi != ' ') {
      *po++ = *pi;
    }
    pi++;
  }
  *po = 0;
  return project_name_nospaces;
}

typedef struct cc_project_impl_t cc_project_impl_t;

typedef struct cc_configuration_impl_t {
  const char label[256];
} cc_configuration_impl_t;
typedef struct cc_architecture_impl_t {
  EArchitecture type;
} cc_architecture_impl_t;
typedef struct cc_platform_impl_t {
  EPlatform type;
} cc_platform_impl_t;

enum EFileType {
  FileTypeNone = 0,      /* added to the project, but not compiled in any way */
  FileTypeCompileable,   /* should be compiled by the C/C++ compiler */
  FileTypeCustomCommand, /* has a custom build command, will not be included in the linking step */
};

struct cc_file_t_ {
  enum EFileType file_type;
  const char* path;
  size_t parent_group_idx;
};

struct cc_file_custom_command_t_ {
  enum EFileType file_type;
  const char* path;
  size_t parent_group_idx;
  const char* output_file;
  const char* command;
};

typedef struct cc_group_impl_t {
  const char* name;
  size_t parent_group_idx;
} cc_group_impl_t;

typedef struct cc_state_impl_t {
  const char** defines;         /* stretch array */
  const char** include_folders; /* stretch array */
  const char** compile_options; /* stretch array */
  const char** link_options;    /* stretch array */
  const char** external_libs;   /* stretch array */

  /* By default warnings are turned up to the highest level below _all_ */
  EStateWarningLevel warningLevel;
  /* By default warnings are treated as errors */
  bool disableWarningsAsErrors;
} cc_state_impl_t;

typedef struct cc_project_impl_t {
  EProjectType type;
  const char* name;
  struct cc_file_t_** file_data;                               /* stretch array */
  struct cc_file_custom_command_t_** file_data_custom_command; /* stretch array */
  cc_project_impl_t** dependantOn;                             /* stretch array */
  cc_project_impl_t** dependantOnNoLink;                       /* stretch array */

  cc_state_impl_t* state;                 /* stretch array */
  cc_configuration_impl_t** configs;      /* stretch array */
  cc_architecture_impl_t** architectures; /* stretch array */
  const char* preBuildAction;
  const char* postBuildAction;

  size_t parent_group_idx;
  const char* outputFolder;

} cc_project_impl_t;

struct {
  bool is_inited;
  const char* base_folder;
  const char* workspaceLabel;
  cc_project_impl_t** projects;                   /* stretch array */
  const cc_configuration_impl_t** configurations; /* stretch array */
  const cc_architecture_impl_t** architectures;   /* stretch array */
  const cc_platform_impl_t** platforms;           /* stretch array */
  cc_group_impl_t* groups;                        /* stretch array */
} cc_data_ = {0};

cc_group_t cc_group_create(const char* in_group_name, const cc_group_t in_parent_group) {
  cc_group_impl_t group;
  group.name             = cc_printf("%s", in_group_name);
  group.parent_group_idx = (size_t)in_parent_group;
  array_push(cc_data_.groups, group);
  return (cc_group_t)(size_t)(array_count(cc_data_.groups) - 1);
}

cc_state_t cc_state_create() {
  cc_state_impl_t* out = (cc_state_impl_t*)cc_alloc_(sizeof(cc_state_impl_t));
  memset(out, 0, sizeof(cc_state_impl_t));
  return (cc_state_t)out;
}

cc_project_t cc_project_create_(const char* in_project_name, EProjectType in_project_type,
                                const cc_group_t in_parent_group) {
  // TODO: having no workspace label crashes on XCode generator
  if (cc_data_.workspaceLabel == 0) {
    cc_data_.workspaceLabel = "workspace";
  }
  if (cc_data_.base_folder == 0) {
    cc_data_.base_folder = "";
  }

  cc_project_impl_t* p = (cc_project_impl_t*)malloc(sizeof(cc_project_impl_t));
  memset(p, 0, sizeof(cc_project_impl_t));
  p->outputFolder    = "${platform}/${configuration}";
  p->type            = in_project_type;
  size_t name_length = strlen(in_project_name);

  char* name_copy = (char*)cc_alloc_(name_length + 1);
  memcpy(name_copy, in_project_name, name_length);
  name_copy[name_length] = 0;
  p->name                = name_copy;
  array_push(cc_data_.projects, p);

  p->parent_group_idx = (size_t)in_parent_group;
  return p;
}

/**
 * Add multiple files to a project, from a NULL-terminated array
 *
 * @param in_parent_group may be NULL
 */
void addFilesToProject(cc_project_t in_out_project, unsigned num_files,
                       const char* in_file_names[], const cc_group_t in_parent_group) {
  assert(in_out_project);

  cc_project_impl_t* project = (cc_project_impl_t*)in_out_project;
  for (unsigned i = 0; i < num_files; ++i, ++in_file_names) {
    struct cc_file_t_* file_data = (struct cc_file_t_*)cc_alloc_(sizeof(struct cc_file_t_));
    file_data->file_type         = FileTypeCustomCommand;
    file_data->path              = cc_printf("%s", *in_file_names);
    file_data->parent_group_idx  = (size_t)in_parent_group;
    array_push(project->file_data, file_data);
  }
}

void addFilesFromFolderToProject(cc_project_t in_out_project, const char* folder,
                                 unsigned num_files, const char* in_file_names[],
                                 const cc_group_t in_parent_group) {
  assert(in_out_project);
  assert(folder);
  assert(num_files == 0 || (in_file_names != NULL));

  cc_project_impl_t* project = (cc_project_impl_t*)in_out_project;
  const char* relative_path  = make_path_relative(cc_data_.base_folder, folder);
  if (relative_path[strlen(relative_path) - 1] != '/') {
    relative_path = cc_printf("%s/", relative_path);
  }
  for (unsigned i = 0; i < num_files; ++i, ++in_file_names) {
    const char* file_path = cc_printf("%s%s", relative_path, make_uri(*in_file_names));

    struct cc_file_t_* file_data = (struct cc_file_t_*)cc_alloc_(sizeof(struct cc_file_t_));
    file_data->file_type         = FileTypeCustomCommand;
    file_data->path              = file_path;
    file_data->parent_group_idx  = (size_t)in_parent_group;
    array_push(project->file_data, file_data);
  }
}

void cc_project_addFileWithCustomCommand(cc_project_t in_out_project, const char* in_file_name,
                                         const cc_group_t in_parent_group,
                                         const char* in_custom_command,
                                         const char* in_output_file_name) {
  assert(in_out_project);

  cc_project_impl_t* project = (cc_project_impl_t*)in_out_project;

  struct cc_file_custom_command_t_* file_data =
      (struct cc_file_custom_command_t_*)cc_alloc_(sizeof(struct cc_file_custom_command_t_));
  file_data->file_type        = FileTypeCustomCommand;
  file_data->path             = cc_printf("%s", in_file_name);
  file_data->parent_group_idx = (size_t)in_parent_group;
  file_data->command          = cc_printf("%s", in_custom_command);
  file_data->output_file      = cc_printf("%s", in_output_file_name);
  array_push(project->file_data_custom_command, file_data);
}

void cc_project_addInputProject(cc_project_t target_project, const cc_project_t on_project) {
  assert(target_project);
  assert(on_project);

  array_push(((cc_project_impl_t*)target_project)->dependantOn, (cc_project_impl_t*)on_project);
}

void cc_project_addDependency(cc_project_t earlier_project, const cc_project_t later_project) {
  assert(earlier_project);
  assert(later_project);

  array_push(((cc_project_impl_t*)later_project)->dependantOnNoLink,
             (cc_project_impl_t*)earlier_project);
}

void addConfiguration(const cc_configuration_t in_configuration) {
  array_push(cc_data_.configurations, (const cc_configuration_impl_t*)in_configuration);
}
void addArchitecture(const cc_architecture_t in_architecture) {
  array_push(cc_data_.architectures, (const cc_architecture_impl_t*)in_architecture);
}
void addPlatform(const cc_platform_t in_platform) {
  array_push(cc_data_.platforms, (const cc_platform_impl_t*)in_platform);
}

void setWorkspaceLabel(const char* label) { cc_data_.workspaceLabel = label; }

void cc_state_reset(cc_state_t out_flags) {
  // TODO: free memory where needed
  memset((cc_project_impl_t*)out_flags, 0, sizeof(cc_state_impl_t));
}

void cc_state_addIncludeFolder(cc_state_t in_state, const char* in_include_folder) {
  cc_state_impl_t* flags = ((cc_state_impl_t*)in_state);
  array_push(flags->include_folders, make_uri(in_include_folder));
}

void cc_state_addPreprocessorDefine(cc_state_t in_state, const char* in_define_string) {
  array_push(((cc_state_impl_t*)in_state)->defines, cc_printf("%s", in_define_string));
}

void cc_state_addCompilerFlag(cc_state_t in_state, const char* in_compiler_flag) {
  array_push(((cc_state_impl_t*)in_state)->compile_options, cc_printf("%s", in_compiler_flag));
}

void cc_state_linkExternalLibrary(cc_state_t in_state, const char* in_external_library_path) {
  array_push(((cc_state_impl_t*)in_state)->external_libs,
             cc_printf("%s", in_external_library_path));
}

void cc_state_addLinkerFlag(cc_state_t in_state, const char* in_linker_flag) {
  array_push(((cc_state_impl_t*)in_state)->link_options, cc_printf("%s", in_linker_flag));
}

void cc_state_setWarningLevel(cc_state_t in_state, EStateWarningLevel in_level) {
  ((cc_state_impl_t*)in_state)->warningLevel = in_level;
  /*
  clang:
  -W -Wall
  -W -Wall -Wextra -pedantic
  -W -Wall -Wextra -pedantic -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization
-Wformat=2 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wnoexcept -Wold-style-cast
-Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-promo -Wstrict-null-sentinel
-Wstrict-overflow=5 -Wundef -Wno-unused -Wno-variadic-macros -Wno-parentheses
-fdiagnostics-show-option

-Weverything

  -Werror warnings as errors

  /W2
  /W3
  /W4

  /Wall

  /WX
  */
}
void cc_state_disableWarningsAsErrors(cc_state_t in_state) {
  ((cc_state_impl_t*)in_state)->disableWarningsAsErrors = true;
}

void cc_project_addPreBuildAction(cc_project_t in_out_project, const char* in_action_command) {
  ((cc_project_impl_t*)in_out_project)->preBuildAction = cc_printf("%s", in_action_command);
}

void cc_project_addPostBuildAction(cc_project_t in_out_project, const char* in_action_command) {
  ((cc_project_impl_t*)in_out_project)->postBuildAction = cc_printf("%s", in_action_command);
}

void cc_project_setOutputFolder(cc_project_t in_out_project, const char* of) {
  ((cc_project_impl_t*)in_out_project)->outputFolder = cc_printf("%s", of);
}

void cc_project_setFlags_(cc_project_t in_out_project, const cc_state_t in_state,
                          cc_architecture_t in_architecture, cc_configuration_t in_configuration) {
  // Clone the flags, so later changes aren't applied to this version
  cc_state_impl_t stored_flags = *(const cc_state_impl_t*)in_state;
  stored_flags.defines         = string_array_clone(((const cc_state_impl_t*)in_state)->defines);
  stored_flags.include_folders =
      string_array_clone(((const cc_state_impl_t*)in_state)->include_folders);
  stored_flags.compile_options =
      string_array_clone(((const cc_state_impl_t*)in_state)->compile_options);
  stored_flags.link_options = string_array_clone(((const cc_state_impl_t*)in_state)->link_options);

  array_push(((cc_project_impl_t*)in_out_project)->state, stored_flags);
  array_push(((cc_project_impl_t*)in_out_project)->architectures,
             (cc_architecture_impl_t*)in_architecture);
  array_push(((cc_project_impl_t*)in_out_project)->configs,
             (cc_configuration_impl_t*)in_configuration);
}

cc_architecture_t cc_architecture_create(EArchitecture in_type) {
  unsigned byte_count       = sizeof(cc_architecture_impl_t);
  cc_architecture_impl_t* p = (cc_architecture_impl_t*)cc_alloc_(byte_count);
  p->type                   = in_type;
  return p;
}
cc_platform_t cc_platform_create(EPlatform in_type) {
  unsigned byte_count   = sizeof(cc_platform_impl_t);
  cc_platform_impl_t* p = (cc_platform_impl_t*)cc_alloc_(byte_count);
  p->type               = in_type;
  return (cc_platform_t)p;
}
cc_configuration_t cc_configuration_create(const char* in_label) {
  cc_configuration_impl_t* c =
      (cc_configuration_impl_t*)cc_alloc_(sizeof(cc_configuration_impl_t));
  strncpy((char*)c->label, in_label, sizeof(c->label));
  ((char*)c->label)[sizeof(c->label) - 1] = 0;
  return c;
}

/*
Thinking of XML:
An object can contain:
  - any number of child objects
    or
  - a single value (string or number)
  - any number of value-string property pairs

For XCode format this is the same, although XCode files also contain commenting (in between even)
*/

struct data_tree_object_t {
  const char* name;
  const char* comment;
  bool has_children;  // If has children, then no value, else has value and no children
  bool is_array;
  union {
    const char* value;
    unsigned int first_child;  // !=0 if has childen, ==0 if no children
  } value_or_child;
  unsigned int next_sibling;  // !=0 if has more siblings, ==0 if no more siblings (siblings can be
                              // objects or parameters)

  unsigned int first_parameter;  // !=0 if has parameters, ==0 if no parameters
};

struct data_tree_t {
  struct data_tree_object_t* objects;
};

struct data_tree_api {
  struct data_tree_t (*create)();

  /* Create a new child object under 'parent_object' with name 'name'
   * parent_object == 0 means it's a top level object, usually not allowed more than once anyway.
   * name == 0 may mean the node is treated specially in output
   * returns index of created object
   */
  unsigned int (*create_object)(struct data_tree_t* tree, unsigned int parent_object,
                                const char* name);

  /* Find a child object under 'parent_object' with name 'name'. If child object is not found, then
   * it is created. parent_object == 0 means it's a top level object, usually not allowed more than
   * once anyway.
   *
   * returns index of created object
   */
  unsigned int (*get_or_create_object)(struct data_tree_t* tree, unsigned int parent_object,
                                       const char* name);

  /* Set the value of object at index 'object'. If the object has children they will be removed.
   */
  void (*set_object_value)(struct data_tree_t* tree, unsigned int object,
                           const char* object_value);

  /* Set the comment of object at index 'object'.
   */
  void (*set_object_comment)(struct data_tree_t* tree, unsigned int object, const char* comment);

  /* If 'object' already has a parameter with the same name, then overwrite the value, else add a
   * new parameter 'param_name' with value 'param_value'
   */
  void (*set_object_parameter)(struct data_tree_t* tree, unsigned int object,
                               const char* param_name, const char* param_value);
};

/***********************************************************************************************************************
 *                                             Implementation starts here
 ***********************************************************************************************************************/
struct data_tree_t dt_create() {
  struct data_tree_t out              = {0};
  struct data_tree_object_t empty_obj = {0};
  array_push(out.objects, empty_obj);
  return out;
}

unsigned int dt_create_object(struct data_tree_t* dt, unsigned int parent_object,
                              const char* name) {
  assert(dt);
  assert((parent_object == 0) || (parent_object < array_count(dt->objects)));

  struct data_tree_object_t obj = {(name ? cc_printf("%s", name) : NULL)};
  array_push(dt->objects, obj);

  unsigned int node_index               = array_count(dt->objects) - 1;
  struct data_tree_object_t* parent_obj = dt->objects + parent_object;
  if (parent_obj->has_children) {
    struct data_tree_object_t* sibling_obj = dt->objects + parent_obj->value_or_child.first_child;
    while (sibling_obj->next_sibling) {
      sibling_obj = dt->objects + sibling_obj->next_sibling;
    }
    sibling_obj->next_sibling = node_index;
  } else {
    parent_obj->has_children               = true;
    parent_obj->value_or_child.first_child = node_index;
  }

  return node_index;
}

unsigned int dt_get_or_create_object(struct data_tree_t* dt, unsigned int parent_object,
                                     const char* name) {
  assert(dt);
  assert((parent_object == 0) || (parent_object < array_count(dt->objects)));
  assert(name);

  // Search for a node with the same name
  struct data_tree_object_t* parent_obj  = dt->objects + parent_object;
  struct data_tree_object_t* sibling_obj = NULL;
  if (parent_obj->has_children) {
    unsigned int child_id = parent_obj->value_or_child.first_child;
    sibling_obj           = dt->objects + child_id;
    do {
      if (strcmp(sibling_obj->name, name) == 0) return child_id;

      // Prepare for next iteration
      child_id = sibling_obj->next_sibling;
      if (child_id) {
        sibling_obj = dt->objects + child_id;
      }
    } while (child_id);
  }

  struct data_tree_object_t obj = {cc_printf("%s", name)};
  array_push(dt->objects, obj);

  unsigned int node_index = array_count(dt->objects) - 1;
  if (sibling_obj) {
    sibling_obj->next_sibling = node_index;
  } else {
    parent_obj->has_children               = true;
    parent_obj->value_or_child.first_child = node_index;
  }

  return node_index;
}

void dt_set_object_value(struct data_tree_t* dt, unsigned int object, const char* object_value) {
  struct data_tree_object_t* obj = dt->objects + object;
  obj->has_children              = false;
  if (object_value) {
    obj->value_or_child.value = cc_printf("%s", object_value);
  }
}

void dt_set_object_comment(struct data_tree_t* dt, unsigned int object, const char* comment) {
  if (comment) {
    struct data_tree_object_t* obj = dt->objects + object;
    obj->comment                   = cc_printf("%s", comment);
  }
}

void dt_set_object_parameter(struct data_tree_t* dt, unsigned int object, const char* param_name,
                             const char* param_value) {
  struct data_tree_object_t* obj       = dt->objects + object;
  struct data_tree_object_t* param_obj = NULL;
  if (obj->first_parameter) {
    param_obj = dt->objects + obj->first_parameter;
    do {
      if (strcmp(param_obj->name, param_name) == 0) {
        param_obj->value_or_child.value = cc_printf("%s", param_value);
        return;
      }

      // Prepare for next iteration
      if (param_obj->next_sibling) {
        param_obj = dt->objects + param_obj->next_sibling;
      }
    } while (param_obj->next_sibling);
  }

  struct data_tree_object_t new_param_obj = {0};
  new_param_obj.name                      = cc_printf("%s", param_name);
  new_param_obj.value_or_child.value      = cc_printf("%s", param_value);

  if (param_obj) {
    param_obj->next_sibling = array_count(dt->objects);
  } else {
    obj->first_parameter = array_count(dt->objects);
  }

  array_push(dt->objects, new_param_obj);
}

const struct data_tree_api data_tree_api = {
    &dt_create,           &dt_create_object,      &dt_get_or_create_object,
    &dt_set_object_value, &dt_set_object_comment, &dt_set_object_parameter};


// Constructors
#ifdef _WIN32

  #pragma warning(disable : 4996)

typedef struct system_np_s {
  HANDLE child_stdout_read;
  HANDLE child_stderr_read;
  HANDLE reader;
  PROCESS_INFORMATION pi;
  const char* command;
  char* stdout_data;
  int stdout_data_size;
  char* stderr_data;
  int stderr_data_size;
  int* exit_code;
  int timeout;  // timeout in milliseconds or -1 for INIFINTE
} system_np_t;

static int peek_pipe(HANDLE pipe, char* data, int size) {
  char buffer[4 * 1024];
  DWORD read      = 0;
  DWORD available = 0;
  bool b          = PeekNamedPipe(pipe, NULL, sizeof(data), NULL, &available, NULL);
  if (!b) {
    return -1;
  } else if (available > 0) {
    int bytes = min(sizeof(buffer), available);
    b         = ReadFile(pipe, buffer, bytes, &read, NULL);
    if (!b) {
      return -1;
    }
    if (data != NULL && size > 0) {
      int n = min(size - 1, (int)read);
      memcpy(data, buffer, n);
      data[n + 1] = 0;  // always zero terminated
      return n;
    }
  }
  return 0;
}

static DWORD WINAPI read_from_all_pipes_fully(void* p) {
  system_np_t* system             = (system_np_t*)p;
  unsigned long long milliseconds = GetTickCount64();  // since boot time
  char* out =
      system->stdout_data != NULL && system->stdout_data_size > 0 ? system->stdout_data : NULL;
  char* err =
      system->stderr_data != NULL && system->stderr_data_size > 0 ? system->stderr_data : NULL;
  int out_bytes = system->stdout_data != NULL && system->stdout_data_size > 0
                      ? system->stdout_data_size - 1
                      : 0;
  int err_bytes = system->stderr_data != NULL && system->stderr_data_size > 0
                      ? system->stderr_data_size - 1
                      : 0;
  for (;;) {
    int read_stdout = peek_pipe(system->child_stdout_read, out, out_bytes);
    if (read_stdout > 0 && out != NULL) {
      out += read_stdout;
      out_bytes -= read_stdout;
    }
    int read_stderr = peek_pipe(system->child_stderr_read, err, err_bytes);
    if (read_stderr > 0 && err != NULL) {
      err += read_stderr;
      err_bytes -= read_stderr;
    }
    if (read_stdout < 0 && read_stderr < 0) {
      break;
    }  // both pipes are closed
    unsigned long long time_spent_in_milliseconds = GetTickCount64() - milliseconds;
    if (system->timeout > 0 && time_spent_in_milliseconds > system->timeout) {
      break;
    }
    if (read_stdout == 0 && read_stderr == 0) {  // nothing has been read from both pipes
      HANDLE handles[2] = {system->child_stdout_read, system->child_stderr_read};
      WaitForMultipleObjects(2, handles, false,
                             1);  // wait for at least 1 millisecond (more likely 16)
    }
  }
  if (out != NULL) {
    *out = 0;
  }
  if (err != NULL) {
    *err = 0;
  }
  return 0;
}

static int create_child_process(system_np_t* system) {
  SECURITY_ATTRIBUTES sa    = {0};
  sa.nLength                = sizeof(SECURITY_ATTRIBUTES);
  sa.bInheritHandle         = true;
  sa.lpSecurityDescriptor   = NULL;
  HANDLE child_stdout_write = INVALID_HANDLE_VALUE;
  HANDLE child_stderr_write = INVALID_HANDLE_VALUE;
  if (!CreatePipe(&system->child_stderr_read, &child_stderr_write, &sa, 0)) {
    return GetLastError();
  }
  if (!SetHandleInformation(system->child_stderr_read, HANDLE_FLAG_INHERIT, 0)) {
    return GetLastError();
  }
  if (!CreatePipe(&system->child_stdout_read, &child_stdout_write, &sa, 0)) {
    return GetLastError();
  }
  if (!SetHandleInformation(system->child_stdout_read, HANDLE_FLAG_INHERIT, 0)) {
    return GetLastError();
  }
  // Set the text I want to run
  STARTUPINFO siStartInfo = {0};
  siStartInfo.cb          = sizeof(STARTUPINFO);
  siStartInfo.hStdError   = child_stderr_write;
  siStartInfo.hStdOutput  = child_stdout_write;
  siStartInfo.dwFlags |= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW;
  siStartInfo.wShowWindow = SW_HIDE;

  const char* szCmdline = system->command;

  bool b  = CreateProcessA(NULL, (char*)szCmdline,
                          NULL,              // process security attributes
                          NULL,              // primary thread security attributes
                          true,              // handles are inherited
                          CREATE_NO_WINDOW,  // creation flags
                          NULL,              // use parent's environment
                          NULL,              // use parent's current directory
                          &siStartInfo,      // STARTUPINFO pointer
                          &system->pi);      // receives PROCESS_INFORMATION
  int err = GetLastError();
  CloseHandle(child_stderr_write);
  CloseHandle(child_stdout_write);
  if (!b) {
    CloseHandle(system->child_stdout_read);
    system->child_stdout_read = INVALID_HANDLE_VALUE;
    CloseHandle(system->child_stderr_read);
    system->child_stderr_read = INVALID_HANDLE_VALUE;
  }
  return b ? 0 : err;
}

int system_np(const char* command, int timeout_milliseconds, char* stdout_data,
              int stdout_data_size, char* stderr_data, int stderr_data_size, int* exit_code) {
  system_np_t system = {0};
  if (exit_code != NULL) {
    *exit_code = 0;
  }
  if (stdout_data != NULL && stdout_data_size > 0) {
    stdout_data[0] = 0;
  }
  if (stderr_data != NULL && stderr_data_size > 0) {
    stderr_data[0] = 0;
  }
  system.timeout          = timeout_milliseconds > 0 ? timeout_milliseconds : -1;
  system.command          = command;
  system.stdout_data      = stdout_data;
  system.stderr_data      = stderr_data;
  system.stdout_data_size = stdout_data_size;
  system.stderr_data_size = stderr_data_size;
  int r                   = create_child_process(&system);
  if (r == 0) {
    // auto thr = new std::thread(read_from_all_pipes_fully, &system);
    system.reader = CreateThread(NULL, 0, read_from_all_pipes_fully, &system, 0, NULL);
    {
      bool thread_done  = WaitForSingleObject(system.pi.hThread, timeout_milliseconds) == 0;
      bool process_done = WaitForSingleObject(system.pi.hProcess, timeout_milliseconds) == 0;
      if (!thread_done || !process_done) {
        TerminateProcess(system.pi.hProcess, ETIME);
      }
      if (exit_code != NULL) {
        GetExitCodeProcess(system.pi.hProcess, (DWORD*)exit_code);
      }
      CloseHandle(system.pi.hThread);
      CloseHandle(system.pi.hProcess);
      CloseHandle(system.child_stdout_read);
      system.child_stdout_read = INVALID_HANDLE_VALUE;
      CloseHandle(system.child_stderr_read);
      system.child_stderr_read = INVALID_HANDLE_VALUE;
    }
  }
  if (stdout_data != NULL && stdout_data_size > 0) {
    stdout_data[stdout_data_size - 1] = 0;
  }
  if (stderr_data != NULL && stderr_data_size > 0) {
    stderr_data[stderr_data_size - 1] = 0;
  }
  return r;
}
#endif

#if defined(_WIN32)
const char* cconstruct_binary_name          = "cconstruct.exe";
const char* cconstruct_internal_binary_name = "cconstruct_internal.exe";
const char* cconstruct_old_binary_name      = "cconstruct.exe.old";
#else
const char* cconstruct_binary_name          = "cconstruct";
const char* cconstruct_internal_binary_name = "cconstruct_internal";
const char* cconstruct_old_binary_name      = "cconstruct.old";
#endif

#if defined(_WIN32)
  #include <Windows.h>

static char stdout_data[16 * 1024 * 1024] = {0};
static char stderr_data[16 * 1024 * 1024] = {0};

// This function finds the location of the VcDevCmd.bat file on your system. This is needed to set
// the environment when compiling a new version of CConstruct binary.
// It currently used vswhere executable which should be at a fixed location.
const char* cc_find_VcDevCmd_bat_() {
  char program_files_x86_path[MAX_PATH];
  GetEnvironmentVariable("ProgramFiles(x86)", program_files_x86_path, MAX_PATH);

  int exit_code       = 0;
  const char* command = cc_printf(
      //"\"%s\\Microsoft Visual Studio\\Installer\\vswhere\" -latest -property "
      "\"%s\\Microsoft Visual Studio\\Installer\\vswhere\" -version [16,17) -property "
      "installationPath",
      program_files_x86_path);
  int r = system_np(command, 100 * 1000, stdout_data, sizeof(stdout_data), stderr_data,
                    sizeof(stderr_data), &exit_code);
  if (r != 0) {
    // Not much to do at this point
    LOG_ERROR_AND_QUIT(ERR_COMPILING,
                       "Couldn't rebuild cconstruct exe, please do so manually if you changed the "
                       "configuration file.\n");
  }

  // Remove new line from stdout_data
  char* s = stdout_data;
  while (*s) {
    if (*s == '\n' || *s == '\r') {
      *s = 0;
      break;
    }
    s++;
  }

  #if _M_X64
  return cc_printf("%s\\VC\\Auxiliary\\Build\\vcvars64.bat", stdout_data);
  #else
  return cc_printf("%s\\VC\\Auxiliary\\Build\\vcvars32.bat", stdout_data);
  #endif
}

void cc_recompile_binary_(const char* cconstruct_config_file_path) {
  char temp_path[MAX_PATH];
  GetEnvironmentVariable("temp", temp_path, MAX_PATH);

  const char* VsDevCmd_bat      = cc_find_VcDevCmd_bat_();
  const char* recompile_command = cc_printf(
      "\"%s\" > nul && pushd %s && cl.exe "
      // Enable exception handling so can help users fix issues in their config files.
      "-EHsc "
      // Always add debug symbols so can give stack trace on exceptions.
      // Would prefer to embed them into binary, but that isn't possible. The binary will reference
      // the PDB file in the %TEMP% folder where it was built.
      "/ZI "
  #ifndef NDEBUG
      "/DEBUG "
  #endif
      "/Fe%s %s "
      "/nologo "
      "/INCREMENTAL:NO "
  #ifdef __cplusplus
      // Set compiler option so that the file is compiled as C or C++, depending on the compiler
      // settings used to manually compile the first CConstruct binary.
      "/TP "
  #else
      "/TC "
  #endif
      "&& popd",
      VsDevCmd_bat, temp_path, cconstruct_internal_binary_name, cconstruct_config_file_path);

  LOG_VERBOSE("Compiling new version of CConstruct binary with the following command:\n'%s'\n\n",
              recompile_command);
  int exit_code = 0;
  int result    = system_np(recompile_command, 100 * 1000, stdout_data, sizeof(stdout_data),
                            stderr_data, sizeof(stderr_data), &exit_code);
  if (result != 0) {
    char* message;
    DWORD rfm = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL,
                              result, 0, (LPSTR)&message, 1024, NULL);
    if (rfm != 0) {
      LOG_ERROR_AND_QUIT(ERR_COMPILING, "Error recompiling with command '%s'\nError: %s",
                         recompile_command, message);
    } else {
      LOG_ERROR_AND_QUIT(ERR_COMPILING, "Error recompiling with command '%s'\nError: %i",
                         recompile_command, result);
    }
  } else {
    if (exit_code == 0) {
      LOG_VERBOSE("Built new CConstruct binary at '%s'\n", cconstruct_internal_binary_name);
    } else {
      // Succesfully ran the command, but there was an error, likely an issue compiling the config
      // file
      printf("stdout: %s\n", stdout_data);
      printf("stderr: %s\n", stderr_data);

      LOG_ERROR_AND_QUIT(
          ERR_COMPILING,
          "Error (%i) recompiling CConstruct config file. You can add '--generate-projects' to "
          "generate projects with the settings built into the existing CConstruct binary.\n",
          exit_code);
    }
  }

  const char* from_path = cc_printf("%s\\%s", temp_path, cconstruct_internal_binary_name);
  const char* to_path   = cc_printf("%s", cconstruct_internal_binary_name);

  if (!MoveFileEx(from_path, to_path, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING)) {
    char buf[256];
    int err = GetLastError();
    FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err,
                   MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buf, (sizeof(buf) / sizeof(wchar_t)),
                   NULL);
    LOG_ERROR_AND_QUIT(ERR_COMPILING,
                       "Couldn't move internal binary from '%s' to '%s'\nError %i: '%s'\n",
                       from_path, to_path, err, buf);
  }
}

// On Windows cannot delete a binary while it is being run. It is possible to move the binary
// though (as long as it's on the same drive), so move the existing binary to a different name and
// then rename the new one to the original name.
void cc_activateNewBuild_() {
  char temp_folder_path[MAX_PATH];
  GetEnvironmentVariable("temp", temp_folder_path, MAX_PATH);

  char existing_binary_path[MAX_PATH];
  char existing_binary_name[MAX_PATH];
  if (!GetModuleFileNameA(NULL, existing_binary_path, MAX_PATH)) {
    exit(1);
  }

  strcpy(existing_binary_name, strrchr(existing_binary_path, '\\') + 1);

  if (!MoveFileEx(existing_binary_path, cconstruct_old_binary_name, MOVEFILE_REPLACE_EXISTING)) {
    LOG_ERROR_AND_QUIT(ERR_COMPILING, "Error: Couldn't move old binary from '%s' to '%s'\n",
                       existing_binary_path, cconstruct_old_binary_name);
  } else {
    LOG_VERBOSE("Moved old binary from '%s' to '%s'\n", existing_binary_path,
                cconstruct_old_binary_name);
  }

  if (!MoveFileEx(cconstruct_internal_binary_name, existing_binary_path,
                  MOVEFILE_REPLACE_EXISTING)) {
    LOG_ERROR_AND_QUIT(ERR_COMPILING, "Error: Couldn't move new binary from '%s' to '%s'\n",
                       cconstruct_internal_binary_name, existing_binary_path);
  } else {
    LOG_VERBOSE("Moved new binary from '%s' to '%s'\n", cconstruct_internal_binary_name,
                existing_binary_path);
  }

  // This will delete the old file after a reboot. Not great, but cleans up a little bit.
  (void)MoveFileEx(cconstruct_old_binary_name, NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
}

#elif defined(__APPLE__)
  #include <stdio.h>
void cc_recompile_binary_(const char* cconstruct_config_file_path) {
  #if __cplusplus
  const char* language_revision =
    #if (__cplusplus > 201700)
      "17";
    #elif (__cplusplus > 201400)
      "14";
    #elif (__cplusplus > 201100)
      "11";
    #else
      "98";
    #endif
  const char* recompile_command =
      cc_printf("clang++ %s -x c++ -std=c++%s -o %s", cconstruct_config_file_path,
                language_revision, cconstruct_internal_binary_name);

  #else
  const char* recompile_command = cc_printf("clang %s -x c -o %s", cconstruct_config_file_path,
                                            cconstruct_internal_binary_name);
  #endif
  LOG_VERBOSE("Compiling new version of CConstruct binary with the following command: '%s'\n",
              recompile_command);

  FILE* pipe = popen(recompile_command, "r");

  if (!pipe) {
    LOG_ERROR_AND_QUIT(ERR_COMPILING, "popen(%s) failed!", recompile_command);
  }

  char buffer[128];
  const char* result = "";

  while (!feof(pipe)) {
    if (fgets(buffer, sizeof(buffer), pipe) != NULL) {
      result = cc_printf("%s%s", result, buffer);
    }
  }

  int rc = pclose(pipe);

  if (rc == EXIT_SUCCESS) {  // == 0
    LOG_VERBOSE("Built new CConstruct binary at '%s'\n", cconstruct_internal_binary_name);
  } else {
    // It looks like the errors have already been printed to the console, so no need to to do that
    // manually.
    LOG_ERROR_AND_QUIT(
        ERR_COMPILING,
        "Error (%i) recompiling CConstruct config file. You can add '--generate-projects' to "
        "generate projects "
        "with the settings built into the existing CConstruct binary.\n",
        rc);
  }
}

void cc_activateNewBuild_() {
  const char* path = getprogname();

  if (rename(cconstruct_internal_binary_name, path) != 0) {
    LOG_ERROR_AND_QUIT(ERR_COMPILING, "Error: Couldn't move binary from '%s' to '%s'\n",
                       cconstruct_internal_binary_name, path);
  } else {
    LOG_VERBOSE("Moved binary from '%s' to '%s'\n", cconstruct_internal_binary_name, path);
  }
}
#endif

const char* vs_generateUUID() {
  static size_t count = 0;
  ++count;

  char* buffer = (char*)cc_alloc_(37);
  sprintf(buffer, "00000000-0000-0000-0000-%012zi", count);
  return buffer;
};

const char* vs_findUUIDForProject(const char** uuids, const cc_project_impl_t* project) {
  unsigned i = 0;
  while (cc_data_.projects[i] != project) {
    ++i;
  }

  return uuids[i];
}

const char* vs_projectArch2String_(EArchitecture arch) {
  switch (arch) {
    case EArchitectureX86:
      return "Win32";
    case EArchitectureX64:
      return "x64";
    case EArchitectureARM:
      return "ARM";
  }
  return "";
}

void vs_replaceForwardSlashWithBackwardSlashInPlace(char* in_out) {
  if (in_out == 0) return;

  char* in_out_start = in_out;

  while (*in_out) {
    if (*in_out == '/') {
      // On Windows commands may use / for flags (eg xcopy ... /s). Don't want to change those.
      if (in_out == in_out_start || *(in_out - 1) != ' ') {
        *in_out = '\\';
      }
    }
    in_out++;
  }
}

typedef struct vs_compiler_setting {
  const char* key;
  const char* value;
} vs_compiler_setting;

#pragma warning(disable : 4706)
void export_tree_as_xml(FILE* f, const struct data_tree_t* dt, unsigned int node,
                        unsigned int depth) {
  assert(node < array_count(dt->objects));

  const int SPACES_PER_DEPTH = 2;

  const struct data_tree_object_t* obj = dt->objects + node;
  if (obj->name) {
    fprintf(f, "%*s<%s", depth * SPACES_PER_DEPTH, "", obj->name);
    if (obj->first_parameter) {
      const struct data_tree_object_t* param_obj = dt->objects + obj->first_parameter;
      do {
        fprintf(f, " %s=\"%s\"", param_obj->name, param_obj->value_or_child.value);
      } while (param_obj->next_sibling && (param_obj = dt->objects + param_obj->next_sibling));

      /*  // Prepare for next iteration
        if (param_obj->next_sibling) {
          param_obj = dt->objects + param_obj->next_sibling;
        }
      } while (param_obj->next_sibling && param_obj);*/
    }
  }

  if (obj->has_children) {
    if (obj->name) {
      fprintf(f, ">\n");
    }
    unsigned int child_index                   = obj->value_or_child.first_child;
    const struct data_tree_object_t* child_obj = dt->objects + child_index;
    do {
      export_tree_as_xml(f, dt, child_index, (node == 0) ? 0 : (depth + 1));

      /*// Prepare for next iteration
      if (child_obj->next_sibling) {
        child_index = child_obj->next_sibling;
        if (child_index) {
          child_obj = dt->objects + child_obj->next_sibling;
        }
      }
    } while (child_obj);*/
    } while (child_obj->next_sibling && (child_index = child_obj->next_sibling) &&
             (child_obj = dt->objects + child_obj->next_sibling));
    fprintf(f, "%*s", depth * SPACES_PER_DEPTH, "");

    if (obj->name) {
      fprintf(f, "</%s>\n", obj->name);
    }
  } else if (obj->value_or_child.value) {
    fprintf(f, ">%s</%s>\n", obj->value_or_child.value, obj->name);
  } else {
    fprintf(f, " />\n");
  }
}

void vs2019_createFilters(const cc_project_impl_t* in_project, const char* in_output_folder) {
  const char* projectfilters_file_path = cc_printf("%s.vcxproj.filters", in_project->name);

  struct data_tree_t dt = data_tree_api.create();

  unsigned int project = data_tree_api.create_object(&dt, 0, "Project");
  data_tree_api.set_object_parameter(&dt, project, "ToolsVersion", "4.0");
  data_tree_api.set_object_parameter(&dt, project, "xmlns",
                                     "http://schemas.microsoft.com/developer/msbuild/2003");

  // Create list of groups needed.
  const cc_project_impl_t* p = (cc_project_impl_t*)in_project;
  bool* groups_needed        = (bool*)cc_alloc_(array_count(cc_data_.groups) * sizeof(bool));
  memset(groups_needed, 0, array_count(cc_data_.groups) * sizeof(bool));
  for (size_t fi = 0; fi < array_count(p->file_data); fi++) {
    size_t gi = p->file_data[fi]->parent_group_idx;
    while (gi) {
      groups_needed[gi] = true;
      gi                = cc_data_.groups[gi].parent_group_idx;
    }
  }
  for (size_t fi = 0; fi < array_count(p->file_data_custom_command); fi++) {
    size_t gi = p->file_data_custom_command[fi]->parent_group_idx;
    while (gi) {
      groups_needed[gi] = true;
      gi                = cc_data_.groups[gi].parent_group_idx;
    }
  }

  // Create names for the needed groups. Nested groups append their name in the filter file
  //    Group A
  //    Group A\Nested Group
  //    Group B
  const char** unique_group_names = {0};
  for (unsigned gi = 0; gi < array_count(cc_data_.groups); gi++) {
    const char* name = "";
    if (groups_needed[gi]) {
      const cc_group_impl_t* g = &cc_data_.groups[gi];
      name                     = g->name[0] ? g->name : "<group>";
      while (g->parent_group_idx) {
        g    = &cc_data_.groups[g->parent_group_idx];
        name = cc_printf("%s\\%s", g->name[0] ? g->name : "<group>", name);
      }
    }
    array_push(unique_group_names, name);
  }

  unsigned int itemgroup = data_tree_api.create_object(&dt, project, "ItemGroup");
  for (unsigned gi = 0; gi < array_count(cc_data_.groups); gi++) {
    if (groups_needed[gi]) {
      const char* group_name = unique_group_names[gi];
      unsigned int filter    = data_tree_api.create_object(&dt, itemgroup, "Filter");
      data_tree_api.set_object_parameter(&dt, filter, "Include", group_name);
      unsigned int uid = data_tree_api.create_object(&dt, filter, "UniqueIdentifier");
      data_tree_api.set_object_value(&dt, uid, vs_generateUUID());
    }
  }

  for (unsigned fi = 0; fi < array_count(p->file_data); ++fi) {
    const struct cc_file_t_* file = p->file_data[fi];

    itemgroup                      = data_tree_api.create_object(&dt, project, "ItemGroup");
    const char* f                  = file->path;
    const size_t gi                = file->parent_group_idx;
    const char* group_name         = NULL;
    const char* relative_file_path = make_path_relative(in_output_folder, f);
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)relative_file_path);
    group_name = unique_group_names[gi];

    unsigned int g = 0;

    if (is_header_file(f)) {
      g = data_tree_api.create_object(&dt, itemgroup, "ClInclude");
    } else if (is_source_file(f)) {
      g = data_tree_api.create_object(&dt, itemgroup, "ClCompile");
    } else {
      g = data_tree_api.create_object(&dt, itemgroup, "None");
    }
    data_tree_api.set_object_parameter(&dt, g, "Include", relative_file_path);
    unsigned int fil = data_tree_api.create_object(&dt, g, "Filter");
    data_tree_api.set_object_value(&dt, fil, group_name);
  }

  for (unsigned fi = 0; fi < array_count(p->file_data_custom_command); ++fi) {
    const struct cc_file_custom_command_t_* file = p->file_data_custom_command[fi];

    const char* f                  = file->path;
    const size_t gi                = file->parent_group_idx;
    const char* group_name         = NULL;
    const char* relative_file_path = make_path_relative(in_output_folder, f);
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)relative_file_path);
    group_name = unique_group_names[gi];

    itemgroup       = data_tree_api.create_object(&dt, project, "ItemGroup");
    unsigned int cb = data_tree_api.create_object(&dt, itemgroup, "CustomBuild");
    data_tree_api.set_object_parameter(&dt, cb, "Include", relative_file_path);
    unsigned int fil = data_tree_api.create_object(&dt, cb, "Filter");
    data_tree_api.set_object_value(&dt, fil, group_name);
  }

  FILE* filter_file = fopen(projectfilters_file_path, "wb");
  fprintf(filter_file, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
  export_tree_as_xml(filter_file, &dt, 0, 0);
  fclose(filter_file);
}

typedef void (*func)(struct data_tree_t* dt, unsigned int compile_group,
                     const char* remaining_flag_value);

void optionZi(struct data_tree_t* dt, unsigned int compile_group,
              const char* remaining_flag_value) {
  (void)remaining_flag_value;
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "DebugInformationFormat"),
      "ProgramDatabase");
}
void optionZI(struct data_tree_t* dt, unsigned int compile_group,
              const char* remaining_flag_value) {
  (void)remaining_flag_value;
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "DebugInformationFormat"),
      "EditAndContinue");
}
void optionMT(struct data_tree_t* dt, unsigned int compile_group,
              const char* remaining_flag_value) {
  (void)remaining_flag_value;
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "RuntimeLibrary"),
      "MultiThreaded");
}
void optionMTd(struct data_tree_t* dt, unsigned int compile_group,
               const char* remaining_flag_value) {
  (void)remaining_flag_value;
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "RuntimeLibrary"),
      "MultiThreadedDebug");
}
void optionMD(struct data_tree_t* dt, unsigned int compile_group,
              const char* remaining_flag_value) {
  (void)remaining_flag_value;
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "RuntimeLibrary"),
      "MultiThreadedDLL");
}
void optionMDd(struct data_tree_t* dt, unsigned int compile_group,
               const char* remaining_flag_value) {
  (void)remaining_flag_value;
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "RuntimeLibrary"),
      "MultiThreadedDebugDLL");
}
void optionPDB(struct data_tree_t* dt, unsigned int compile_group,
               const char* remaining_flag_value) {
  size_t len        = strlen(remaining_flag_value);
  const char* value = remaining_flag_value;
  // Strip exterior quotes as VS will add them again
  if (value[0] == '"' && value[len - 1] == '"') {
    value                   = cc_printf("%s", value + 1);
    ((char*)value)[len - 2] = 0;
  }
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "ProgramDatabaseFile"), value);
}

typedef struct vs_compiler_flag {
  const char* flag;
  const func action;
} vs_compiler_flag;

const vs_compiler_flag known_compiler_flags[] = {{"/Zi", &optionZi}, {"/ZI", &optionZI},
                                                 {"/MT", &optionMT}, {"/MTd", &optionMTd},
                                                 {"/MD", &optionMD}, {"/MDd", &optionMDd}};
const vs_compiler_flag known_linker_flags[]   = {{"/PDB:", &optionPDB}};

void vs2019_createProjectFile(const cc_project_impl_t* p, const char* project_id,
                              const char** project_ids, const char* in_output_folder,
                              const char* build_to_base_path) {
  const char* project_file_path = cc_printf("%s.vcxproj", p->name);

  struct data_tree_t dt = data_tree_api.create();
  unsigned int project  = data_tree_api.create_object(&dt, 0, "Project");
  data_tree_api.set_object_parameter(&dt, project, "DefaultTargets", "Build");
  data_tree_api.set_object_parameter(&dt, project, "ToolsVersion", "15.0");
  data_tree_api.set_object_parameter(&dt, project, "xmlns",
                                     "http://schemas.microsoft.com/developer/msbuild/2003");

  {
    unsigned int itemgroup = data_tree_api.create_object(&dt, project, "ItemGroup");
    data_tree_api.set_object_parameter(&dt, itemgroup, "Label", "ProjectConfigurations");
    for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
      const char* c = cc_data_.configurations[ci]->label;
      for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
        const char* platform_label = vs_projectArch2String_(cc_data_.architectures[pi]->type);

        unsigned int pc = data_tree_api.create_object(&dt, itemgroup, "ProjectConfiguration");
        data_tree_api.set_object_parameter(&dt, pc, "Include",
                                           cc_printf("%s|%s", c, platform_label));
        unsigned int cobj = data_tree_api.create_object(&dt, pc, "Configuration");
        data_tree_api.set_object_value(&dt, cobj, c);
        unsigned int platformobj = data_tree_api.create_object(&dt, pc, "Platform");
        data_tree_api.set_object_value(&dt, platformobj, platform_label);
      }
    }
  }

  {
    unsigned int pg = data_tree_api.create_object(&dt, project, "PropertyGroup");
    data_tree_api.set_object_parameter(&dt, pg, "Label", "Globals");
    data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "VCProjectVersion"),
                                   "15.0");
    data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "ProjectGuid"),
                                   cc_printf("{%s}", project_id));
    data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "Keyword"),
                                   "Win32Proj");
    data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "RootNamespace"),
                                   "builder");
    data_tree_api.set_object_value(
        &dt, data_tree_api.create_object(&dt, pg, "WindowsTargetPlatformVersion"), "10.0");

    data_tree_api.set_object_parameter(&dt, data_tree_api.create_object(&dt, project, "Import"),
                                       "Project", "$(VCTargetsPath)\\Microsoft.Cpp.Default.props");
  }

  for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
    const char* c = cc_data_.configurations[ci]->label;
    for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
      const char* platform_label = vs_projectArch2String_(cc_data_.architectures[pi]->type);
      unsigned int pg            = data_tree_api.create_object(&dt, project, "PropertyGroup");
      data_tree_api.set_object_parameter(
          &dt, pg, "Condition",
          cc_printf("'$(Configuration)|$(Platform)' == '%s|%s'", c, platform_label));
      data_tree_api.set_object_parameter(&dt, pg, "Label", "Configuration");

      unsigned int ct      = data_tree_api.create_object(&dt, pg, "ConfigurationType");
      const char* ct_value = NULL;
      switch (p->type) {
        case CCProjectTypeConsoleApplication:  // Intentional fallthrough
        case CCProjectTypeWindowedApplication:
          ct_value = "Application";
          break;
        case CCProjectTypeStaticLibrary:
          ct_value = "StaticLibrary";
          break;
        case CCProjectTypeDynamicLibrary:
          ct_value = "DynamicLibrary";
          break;
      }
      data_tree_api.set_object_value(&dt, ct, ct_value);

      data_tree_api.set_object_value(
          &dt, data_tree_api.create_object(&dt, pg, "UseDebugLibraries"), "true");
      data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "PlatformToolset"),
                                     "v142");
      data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "CharacterSet"),
                                     "MultiByte");
    }
  }

  data_tree_api.set_object_parameter(&dt, data_tree_api.create_object(&dt, project, "Import"),
                                     "Project", "$(VCTargetsPath)\\Microsoft.Cpp.props");
  data_tree_api.set_object_parameter(&dt, data_tree_api.create_object(&dt, project, "ImportGroup"),
                                     "Label", "ExtensionSettings");
  data_tree_api.set_object_parameter(&dt, data_tree_api.create_object(&dt, project, "ImportGroup"),
                                     "Label", "Shared");

  for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
    const char* c = cc_data_.configurations[ci]->label;
    for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
      const char* platform_label = vs_projectArch2String_(cc_data_.architectures[pi]->type);
      unsigned int importgroup   = data_tree_api.create_object(&dt, project, "ImportGroup");
      data_tree_api.set_object_parameter(&dt, importgroup, "Label", "PropertySheets");
      data_tree_api.set_object_parameter(
          &dt, importgroup, "Condition",
          cc_printf("'$(Configuration)|$(Platform)' == '%s|%s'", c, platform_label));
      unsigned int import_obj = data_tree_api.create_object(&dt, importgroup, "Import");
      data_tree_api.set_object_parameter(&dt, import_obj, "Project",
                                         "$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props");
      data_tree_api.set_object_parameter(
          &dt, import_obj, "Condition",
          "exists('$(UserRootDir)\\Microsoft.Cpp.$(Platform).user.props')");
      data_tree_api.set_object_parameter(&dt, import_obj, "Label", "LocalAppDataPlatform");
    }
  }

  const char* substitution_keys[]   = {"configuration", "platform"};
  const char* substitution_values[] = {"$(Configuration)", "$(Platform)"};

  for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
    const char* c = cc_data_.configurations[ci]->label;
    for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
      const char* platform_label = vs_projectArch2String_(cc_data_.architectures[pi]->type);
      const char* resolved_output_folder = cc_substitute(
          p->outputFolder, substitution_keys, substitution_values, countof(substitution_keys));
      vs_replaceForwardSlashWithBackwardSlashInPlace((char*)resolved_output_folder);

      unsigned int pg = data_tree_api.create_object(&dt, project, "PropertyGroup");
      data_tree_api.set_object_parameter(&dt, pg, "Label", "UserMacros");
      pg = data_tree_api.create_object(&dt, project, "PropertyGroup");
      data_tree_api.set_object_parameter(
          &dt, pg, "Condition",
          cc_printf("'$(Configuration)|$(Platform)'=='%s|%s'", c, platform_label));

      // TODO: fix detection of type of config
      bool is_debug_build = (strcmp(c, "Debug") == 0);
      data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "LinkIncremental"),
                                     is_debug_build ? "true" : "false");

      // Because users can add post-build commands, make sure these custom build commands have
      // executed before then by doing it after BuildCompile instead of after Build.
      data_tree_api.set_object_value(
          &dt, data_tree_api.create_object(&dt, pg, "CustomBuildAfterTargets"), "BuildCompile");
      data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pg, "OutDir"),
                                     cc_printf("$(SolutionDir)\\%s\\", resolved_output_folder));

      // VS2019 warns if multiple projects have the same intermediate directory, so avoid that
      // here
      data_tree_api.set_object_value(
          &dt, data_tree_api.create_object(&dt, pg, "IntDir"),
          cc_printf("$(SolutionDir)\\%s\\Intermediate\\$(ProjectName)\\", resolved_output_folder));
    }
  }

  for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
    const cc_configuration_impl_t* config = cc_data_.configurations[ci];
    const char* configuration_label       = config->label;
    const bool is_debug_build             = (strcmp(configuration_label, "Debug") == 0);
    for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
      const cc_architecture_impl_t* arch = cc_data_.architectures[pi];

      unsigned int idg = data_tree_api.create_object(&dt, project, "ItemDefinitionGroup");
      const char* platform_label = vs_projectArch2String_(arch->type);
      data_tree_api.set_object_parameter(&dt, idg, "Condition",
                                         cc_printf("'$(Configuration)|$(Platform)'=='%s|%s'",
                                                   configuration_label, platform_label));
      unsigned int compile_obj = data_tree_api.create_object(&dt, idg, "ClCompile");
      unsigned int link_obj    = data_tree_api.create_object(&dt, idg, "Link");

      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "PrecompiledHeader"),
          "NotUsing");
      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "SDLCheck"), "true");
      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "ConformanceMode"), "true");

      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, link_obj, "SubSystem"),
          ((p->type == CCProjectTypeWindowedApplication) ? "Windows" : "Console"));
      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, link_obj, "GenerateDebugInformation"),
          "true");

      if (is_debug_build) {
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "RuntimeLibrary"),
            "MultiThreadedDebugDLL");
      } else {
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "RuntimeLibrary"),
            "MultiThreadedDLL");
      }

      if (is_debug_build) {
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "Optimization"), "Disabled");
      } else {
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "Optimization"), "MaxSpeed");
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "BasicRuntimeChecks"),
            "Default");
      }

      const char* preprocessor_defines = "%(PreprocessorDefinitions)";
      switch (p->type) {
        case CCProjectTypeConsoleApplication:
          preprocessor_defines = cc_printf("%s;%s", "_CONSOLE", preprocessor_defines);
          break;
        case CCProjectTypeWindowedApplication:
          preprocessor_defines = cc_printf("%s;%s", "_WINDOWS", preprocessor_defines);
          break;
        case CCProjectTypeStaticLibrary:
          preprocessor_defines = cc_printf("%s;%s", "_LIB", preprocessor_defines);
          break;
        case CCProjectTypeDynamicLibrary: {
          preprocessor_defines =
              cc_printf("%s%s;%s", str_strip_spaces(p->name), "_EXPORTS", preprocessor_defines);
        } break;
        default:
          LOG_ERROR_AND_QUIT(ERR_CONFIGURATION, "Unknown project type for project '%s'\n",
                             p->name);
      }
      const char* additional_compiler_flags    = "%(AdditionalOptions)";
      const char* additional_link_flags        = "%(AdditionalOptions)";
      const char** additional_include_folders  = NULL;
      const char** link_additional_directories = NULL;
      const char* link_additional_dependencies = "";

      EStateWarningLevel combined_warning_level = EStateWarningLevelDefault;
      bool shouldDisableWarningsAsError         = false;
      for (unsigned ipc = 0; ipc < array_count(p->state); ++ipc) {
        const cc_state_impl_t* flags = &(p->state[ipc]);

        // TODO ordering and combination so that more specific flags can override general ones
        if ((p->configs[ipc] != config) && (p->configs[ipc] != NULL)) continue;
        if ((p->architectures[ipc] != arch) && (p->architectures[ipc] != NULL)) continue;

        shouldDisableWarningsAsError = flags->disableWarningsAsErrors;
        combined_warning_level       = flags->warningLevel;
        for (unsigned pdi = 0; pdi < array_count(flags->defines); ++pdi) {
          preprocessor_defines = cc_printf("%s;%s", flags->defines[pdi], preprocessor_defines);
        }
        for (unsigned cfi = 0; cfi < array_count(flags->compile_options); ++cfi) {
          // Index in known flags
          const char* current_flag = flags->compile_options[cfi];
          bool found               = false;
          for (unsigned kfi = 0; kfi < countof(known_compiler_flags); kfi++) {
            if (strcmp(known_compiler_flags[kfi].flag, current_flag) == 0) {
              found = true;
              known_compiler_flags[kfi].action(&dt, compile_obj, NULL);
            }
          }
          if (!found) {
            additional_compiler_flags =
                cc_printf("%s %s", flags->compile_options[cfi], additional_compiler_flags);
          }
        }
        for (unsigned lfi = 0; lfi < array_count(flags->link_options); ++lfi) {
          const char* current_flag = flags->link_options[lfi];
          bool found               = false;
          for (unsigned kfi = 0; kfi < countof(known_linker_flags); kfi++) {
            if (strstr(current_flag, known_linker_flags[kfi].flag) == current_flag) {
              found = true;
              known_linker_flags[kfi].action(&dt, link_obj,
                                             current_flag + strlen(known_linker_flags[kfi].flag));
            }
          }
          if (!found) {
            additional_link_flags = cc_printf("%s %s", current_flag, additional_link_flags);
          }
        }
        for (unsigned ifi = 0; ifi < array_count(flags->include_folders); ++ifi) {
          array_push(additional_include_folders, flags->include_folders[ifi]);
        }

        for (unsigned di = 0; di < array_count(flags->external_libs); di++) {
          const char* lib_path_from_base = flags->external_libs[di];
          const bool is_absolute_path =
              (lib_path_from_base[0] == '/') || (lib_path_from_base[1] == ':');
          const bool starts_with_env_variable = (lib_path_from_base[0] == '$');
          if (!is_absolute_path && !starts_with_env_variable) {
            lib_path_from_base = cc_printf("%s%s", build_to_base_path, lib_path_from_base);
          }

          const char* relative_lib_path = make_path_relative(in_output_folder, lib_path_from_base);
          const char* lib_name          = strip_path(relative_lib_path);
          const char* lib_folder        = make_uri(folder_path_only(relative_lib_path));
          const char* resolved_lib_folder = cc_substitute(
              lib_folder, substitution_keys, substitution_values, countof(substitution_keys));

          vs_replaceForwardSlashWithBackwardSlashInPlace((char*)resolved_lib_folder);
          link_additional_dependencies =
              cc_printf("%s;%s", link_additional_dependencies, lib_name);
          // Check if this path is already in there
          bool did_find = false;
          for (size_t ilf = 0; ilf < array_count(link_additional_directories); ilf++) {
            if (strcmp(link_additional_directories[ilf], resolved_lib_folder) == 0)
              did_find = true;
          }
          if (!did_find) {
            array_push(link_additional_directories, resolved_lib_folder);
          }
        }
      }

      {
        const char* warning_strings[] = {"Level4", "Level3", "Level2", "EnableAllWarnings",
                                         "TurnOffAllWarnings"};
        assert(EStateWarningLevelHigh == 0);
        assert(EStateWarningLevelAll == 3);
        assert(EStateWarningLevelNone == 4);
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "WarningLevel"),
            warning_strings[combined_warning_level]);
      }

      // Disable unreferenced parameter warning
      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "DisableSpecificWarnings"),
          "4100");

      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "TreatWarningAsError"),
          shouldDisableWarningsAsError ? "false" : "true");

      if (is_debug_build) {
        preprocessor_defines = cc_printf("_DEBUG;%s", preprocessor_defines);
      } else {
        preprocessor_defines = cc_printf("NDEBUG;%s", preprocessor_defines);
      }
      const bool is_win32 = (cc_data_.architectures[pi]->type == EArchitectureX86);
      if (is_win32) {
        preprocessor_defines = cc_printf("WIN32;%s", preprocessor_defines);
      }

      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "PreprocessorDefinitions"),
          preprocessor_defines);
      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, compile_obj, "AdditionalOptions"),
          additional_compiler_flags);

      if (array_count(additional_include_folders) != 0) {
        // Put later folders before earlier ones
        const char* combined_include_folders = additional_include_folders[0];
        for (size_t i = 1; i < array_count(additional_include_folders); i++) {
          combined_include_folders =
              cc_printf("%s;%s", additional_include_folders[i], combined_include_folders);
        }
        vs_replaceForwardSlashWithBackwardSlashInPlace((char*)additional_include_folders);
        data_tree_api.set_object_value(
            &dt,
            data_tree_api.get_or_create_object(&dt, compile_obj, "AdditionalIncludeDirectories"),
            combined_include_folders);
      }

      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, link_obj, "AdditionalOptions"),
          additional_link_flags);

      link_additional_dependencies =
          cc_printf("%s;%%(AdditionalDependencies)", link_additional_dependencies);

      if (array_count(link_additional_directories) != 0) {
        // Put later folders in front of earlier folders
        const char* combined_link_folders = link_additional_directories[0];
        for (size_t i = 1; i < array_count(link_additional_directories); i++) {
          combined_link_folders =
              cc_printf("%s;%s", link_additional_directories[i], combined_link_folders);
        }
        vs_replaceForwardSlashWithBackwardSlashInPlace((char*)additional_include_folders);
        data_tree_api.set_object_value(
            &dt, data_tree_api.get_or_create_object(&dt, link_obj, "AdditionalLibraryDirectories"),
            combined_link_folders);
      }
      data_tree_api.set_object_value(
          &dt, data_tree_api.get_or_create_object(&dt, link_obj, "AdditionalDependencies"),
          link_additional_dependencies);

      const bool have_pre_build_action = (p->preBuildAction != 0);
      if (have_pre_build_action) {
        const char* windowsPreBuildAction = cc_printf("%s", p->preBuildAction);
        windowsPreBuildAction             = cc_substitute(windowsPreBuildAction, substitution_keys,
                                                          substitution_values, countof(substitution_keys));
        vs_replaceForwardSlashWithBackwardSlashInPlace((char*)windowsPreBuildAction);
        unsigned int pbe = data_tree_api.create_object(&dt, idg, "PreBuildEvent");
        data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pbe, "Command"),
                                       windowsPreBuildAction);
      }
      const bool have_post_build_action = (p->postBuildAction != 0);
      if (have_post_build_action) {
        const char* windowsPostBuildAction = cc_printf("%s", p->postBuildAction);
        windowsPostBuildAction = cc_substitute(windowsPostBuildAction, substitution_keys,
                                               substitution_values, countof(substitution_keys));
        vs_replaceForwardSlashWithBackwardSlashInPlace((char*)windowsPostBuildAction);
        unsigned int pbe = data_tree_api.create_object(&dt, idg, "PostBuildEvent");
        data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pbe, "Command"),
                                       windowsPostBuildAction);
      }
    }
  }

  for (unsigned fi = 0; fi < array_count(p->file_data); ++fi) {
    const char* f                  = p->file_data[fi]->path;
    const char* relative_file_path = make_path_relative(in_output_folder, f);
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)relative_file_path);
    unsigned int itemgroup = data_tree_api.create_object(&dt, project, "ItemGroup");
    unsigned int obj       = 0;
    if (is_header_file(f)) {
      obj = data_tree_api.create_object(&dt, itemgroup, "ClInclude");
    } else if (is_source_file(f)) {
      obj             = data_tree_api.create_object(&dt, itemgroup, "ClCompile");
      unsigned int ca = data_tree_api.create_object(&dt, obj, "CompileAs");
      data_tree_api.set_object_value(
          &dt, ca, ((strstr(f, ".cpp") != NULL) ? "CompileAsCpp" : "CompileAsC"));
    } else {
      obj = data_tree_api.create_object(&dt, itemgroup, "None");
    }
    data_tree_api.set_object_parameter(&dt, obj, "Include", relative_file_path);
  }

  for (unsigned fi = 0; fi < array_count(p->file_data_custom_command); ++fi) {
    const struct cc_file_custom_command_t_* file = p->file_data_custom_command[fi];
    const char* relative_in_file_path = make_path_relative(in_output_folder, file->path);
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)relative_in_file_path);
    const char* relative_out_file_path = make_path_relative(in_output_folder, file->output_file);
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)relative_out_file_path);

    /*
    Apparently VS2019 Custom Build Tool does not reset paths between multiple invocations of Custom
    Build Tool. That's why there needs to be an absolute path here, instead of only the simpler
    relative build_to_base_path.
    */
    const char* in_file_path_from_base = make_path_relative(
        cc_data_.base_folder, make_uri(cc_printf("%s/%s", in_output_folder, file->path)));
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)in_file_path_from_base);
    const char* out_file_path_from_base = make_path_relative(
        cc_data_.base_folder, make_uri(cc_printf("%s/%s", in_output_folder, file->output_file)));
    vs_replaceForwardSlashWithBackwardSlashInPlace((char*)out_file_path_from_base);
    const char* input_output_substitution_keys[]   = {"input", "output"};
    const char* input_output_substitution_values[] = {in_file_path_from_base,
                                                      out_file_path_from_base};

    const char* in_file_path   = cc_substitute(relative_in_file_path, substitution_keys,
                                               substitution_values, countof(substitution_keys));
    const char* custom_command = cc_substitute(file->command, substitution_keys,
                                               substitution_values, countof(substitution_keys));
    custom_command =
        cc_substitute(custom_command, input_output_substitution_keys,
                      input_output_substitution_values, countof(input_output_substitution_keys));
    const char* out_file_path = cc_substitute(relative_out_file_path, substitution_keys,
                                              substitution_values, countof(substitution_keys));
    unsigned int itemgroup    = data_tree_api.create_object(&dt, project, "ItemGroup");
    unsigned int cb           = data_tree_api.create_object(&dt, itemgroup, "CustomBuild");
    data_tree_api.set_object_parameter(&dt, cb, "Include", in_file_path);
    unsigned int command_obj = data_tree_api.create_object(&dt, cb, "Command");
    data_tree_api.set_object_value(
        &dt, command_obj,
        cc_printf("cd $(ProjectDir)%s &amp;&amp; %s", build_to_base_path, custom_command));
    unsigned int outputs_obj = data_tree_api.create_object(&dt, cb, "Outputs");
    data_tree_api.set_object_value(&dt, outputs_obj, out_file_path);
  }

  // Add Visual Studio references (automatically links projects).
  for (unsigned i = 0; i < array_count(p->dependantOn); ++i) {
    const char* id            = vs_findUUIDForProject(project_ids, p->dependantOn[i]);
    const char* project_label = p->dependantOn[i]->name;

    unsigned int itemgroup = data_tree_api.create_object(&dt, project, "ItemGroup");
    unsigned int pr        = data_tree_api.create_object(&dt, itemgroup, "ProjectReference");
    data_tree_api.set_object_parameter(&dt, pr, "Include", cc_printf("%s.vcxproj", project_label));
    data_tree_api.set_object_value(&dt, data_tree_api.create_object(&dt, pr, "Project"),
                                   cc_printf("{%s}", id));
  }

  unsigned int importobj = data_tree_api.create_object(&dt, project, "Import");
  data_tree_api.set_object_parameter(&dt, importobj, "Project",
                                     "$(VCTargetsPath)\\Microsoft.Cpp.targets");
  unsigned int importgroup = data_tree_api.create_object(&dt, project, "ImportGroup");
  data_tree_api.set_object_parameter(&dt, importgroup, "Label", "ExtensionTargets");

  FILE* project_file = fopen(project_file_path, "wb");
  fprintf(project_file, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
  export_tree_as_xml(project_file, &dt, 0, 0);
  fclose(project_file);
}

const char* replaceSpacesWithUnderscores(const char* in) {
  size_t length = strlen(in);
  char* out     = (char*)cc_alloc_(length + 1);  // +1 for terminating 0
  memcpy(out, in, length);
  out[length] = 0;
  char* it    = out;
  for (unsigned i = 0; i < length; ++i, ++it) {
    if (*it == ' ') {
      *it = '_';
    }
  }
  return in;
}

const char* solutionArch2String_(EArchitecture arch) {
  switch (arch) {
    case EArchitectureX86:
      return "x86";
    case EArchitectureX64:
      return "x64";
    case EArchitectureARM:
      return "ARM";
  }
  return "";
}

void vs2019_createSolutionFile(const char** project_ids) {
  // Create list of groups needed.
  bool* groups_needed = (bool*)cc_alloc_(array_count(cc_data_.groups) * sizeof(bool));
  memset(groups_needed, 0, array_count(cc_data_.groups) * sizeof(bool));
  for (size_t i = 0; i < array_count(cc_data_.projects); i++) {
    size_t g = cc_data_.projects[i]->parent_group_idx;
    while (g) {
      groups_needed[g] = true;
      g                = cc_data_.groups[g].parent_group_idx;
    }
  }

  const char* workspace_file_path = cc_printf("%s.sln", cc_data_.workspaceLabel);
  FILE* workspace                 = fopen(workspace_file_path, "wb");
  if (workspace == NULL) {
    fprintf(stderr, "Couldn't create workspace.sln\n");
    return;
  }

  fprintf(workspace,
          "Microsoft Visual Studio Solution File, Format Version 12.00\n# Visual Studio Version "
          "16\nVisualStudioVersion = 16.0.29709.97\nMinimumVisualStudioVersion = 10.0.40219.1\n");

  // Add projects
  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    const char* projectId      = project_ids[i];
    const cc_project_impl_t* p = cc_data_.projects[i];
    fprintf(workspace,
            "Project(\"{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}\") = \"%s\", \"%s.vcxproj\", "
            "\"{%s}\"\n",
            p->name, replaceSpacesWithUnderscores(p->name), projectId);

    // Add Visual Studio dependencies (only influences build order, does not automatically link).
    if (array_count(p->dependantOnNoLink) > 0) {
      fprintf(workspace, "\tProjectSection(ProjectDependencies) = postProject\n");
      for (unsigned ipnl = 0; ipnl < array_count(p->dependantOnNoLink); ++ipnl) {
        const char* id = vs_findUUIDForProject(project_ids, p->dependantOnNoLink[ipnl]);
        fprintf(workspace, "\t\t{%s} = {%s}\n", id, id);
      }
      fprintf(workspace, "\tEndProjectSection\n");
    }

    fprintf(workspace, "EndProject\n");
  }

  // Add solution folders
  const char** unique_groups_id = {0};
  for (unsigned i = 0; i < array_count(cc_data_.groups); i++) {
    const char* id = NULL;
    if (groups_needed[i]) {
      id                       = vs_generateUUID();
      const cc_group_impl_t* g = &cc_data_.groups[i];
      fprintf(workspace,
              "Project(\"{2150E333-8FDC-42A3-9474-1A3956D46DE8}\") = \"%s\", \"%s\", "
              "\"{%s}\"\n",
              g->name, g->name, id);
      fprintf(workspace, "EndProject\n");
    }
    array_push(unique_groups_id, id);
  }

  fprintf(workspace, "Global\n");

  fprintf(workspace, "	GlobalSection(SolutionConfigurationPlatforms) = preSolution\n");
  for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
    const char* c = cc_data_.configurations[ci]->label;
    for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
      const char* p = solutionArch2String_(cc_data_.architectures[pi]->type);
      fprintf(workspace, "		%s|%s = %s|%s\n", c, p, c, p);
    }
  }
  fprintf(workspace, "	EndGlobalSection\n");

  fprintf(workspace, "	GlobalSection(ProjectConfigurationPlatforms) = postSolution\n");
  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    const char* projectId = project_ids[i];
    for (unsigned ci = 0; ci < array_count(cc_data_.configurations); ++ci) {
      const char* c = cc_data_.configurations[ci]->label;
      for (unsigned pi = 0; pi < array_count(cc_data_.architectures); ++pi) {
        const EArchitecture p  = cc_data_.architectures[pi]->type;
        const char* arch_label = vs_projectArch2String_(p);
        fprintf(workspace, "		{%s}.%s|%s.ActiveCfg = %s|%s\n", projectId, c,
                solutionArch2String_(p), c, arch_label);
        fprintf(workspace, "		{%s}.%s|%s.Build.0 = %s|%s\n", projectId, c,
                solutionArch2String_(p), c, arch_label);
      }
    }
  }
  fprintf(workspace, "	EndGlobalSection\n");

  fprintf(workspace,
          "	GlobalSection(SolutionProperties) = preSolution\n"
          "		HideSolutionNode = FALSE\n"
          "	EndGlobalSection\n");

  fprintf(workspace, "	GlobalSection(NestedProjects) = preSolution\n");
  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    const char* projectId      = project_ids[i];
    const cc_project_impl_t* p = cc_data_.projects[i];
    if (p->parent_group_idx) {
      fprintf(workspace, "		{%s} = {%s}\n", projectId,
              unique_groups_id[p->parent_group_idx]);
    }
  }
  for (unsigned i = 0; i < array_count(cc_data_.groups); i++) {
    if (groups_needed[i]) {
      const cc_group_impl_t* g = &cc_data_.groups[i];
      if (g->parent_group_idx) {
        fprintf(workspace, "		{%s} = {%s}\n", unique_groups_id[i],
                unique_groups_id[g->parent_group_idx]);
      }
    }
  }
  fprintf(workspace, "	EndGlobalSection\n");

  fprintf(workspace,
          "	GlobalSection(ExtensibilityGlobals) = postSolution\n"
          "		SolutionGuid = "
          "{7354F2AC-FB49-4B5D-B080-EDD798F580A5}\n"
          "	EndGlobalSection\nEndGlobal\n");

  fclose(workspace);

  printf("Constructed VS2019 workspace at '%s'\n", workspace_file_path);
}

void vs2019_generateInFolder(const char* in_workspace_path) {
  in_workspace_path = make_uri(in_workspace_path);
  if (in_workspace_path[strlen(in_workspace_path) - 1] != '/')
    in_workspace_path = cc_printf("%s/", in_workspace_path);

  char* output_folder = make_uri(cc_printf("%s%s", cc_data_.base_folder, in_workspace_path));

  char* build_to_base_path = make_path_relative(output_folder, cc_data_.base_folder);

  for (unsigned project_idx = 0; project_idx < array_count(cc_data_.projects); project_idx++) {
    // Adjust all the files to be relative to the build output folder
    cc_project_impl_t* project = cc_data_.projects[project_idx];
    for (unsigned file_idx = 0; file_idx < array_count(project->file_data); file_idx++) {
      project->file_data[file_idx]->path =
          cc_printf("%s%s", build_to_base_path, project->file_data[file_idx]->path);
    }
    for (unsigned file_idx = 0; file_idx < array_count(project->file_data_custom_command);
         file_idx++) {
      project->file_data_custom_command[file_idx]->path =
          cc_printf("%s%s", build_to_base_path, project->file_data_custom_command[file_idx]->path);
      project->file_data_custom_command[file_idx]->output_file = cc_printf(
          "%s%s", build_to_base_path, project->file_data_custom_command[file_idx]->output_file);
    }
    for (unsigned state_idx = 0; state_idx < array_count(project->state); state_idx++) {
      const cc_state_impl_t* state = project->state + state_idx;
      // Also all the include paths
      for (unsigned includes_idx = 0; includes_idx < array_count(state->include_folders);
           includes_idx++) {
        const char* include_path            = state->include_folders[includes_idx];
        const bool is_absolute_path         = (include_path[0] == '/') || (include_path[1] == ':');
        const bool starts_with_env_variable = (include_path[0] == '$');
        if (!is_absolute_path && !starts_with_env_variable) {
          state->include_folders[includes_idx] =
              cc_printf("%s%s", build_to_base_path, include_path);
        }
      }
    }
  }

  printf("Generating Visual Studio 2019 solution and projects in '%s'...\n", output_folder);

  int result = make_folder(output_folder);
  if (result != 0) {
    fprintf(stderr, "Error %i creating path '%s'\n", result, output_folder);
  }
  (void)chdir(output_folder);

  const char** project_ids =
      (const char**)cc_alloc_(sizeof(const char*) * array_count(cc_data_.projects));

  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    project_ids[i] = vs_generateUUID();
  }

  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    const cc_project_impl_t* p = cc_data_.projects[i];
    const char* project_id     = project_ids[i];
    vs2019_createFilters(p, output_folder);
    vs2019_createProjectFile(p, project_id, project_ids, output_folder, build_to_base_path);

    printf("Constructed VS2019 project '%s.vcxproj'\n", p->name);
  }

  vs2019_createSolutionFile(project_ids);
}


typedef struct xcode_uuid {
  unsigned int uuid[3];
} xcode_uuid;

static_assert(sizeof(xcode_uuid) == 12, "Incorrect size of UUID");

xcode_uuid xCodeGenerateUUID() {
  static unsigned count = 0;

  xcode_uuid out = {0};
  out.uuid[0]    = ++count;
  return out;
}
const char* xCodeUUID2String(xcode_uuid uuid) {
  return cc_printf("%08x%08x%08x", uuid.uuid[2], uuid.uuid[1], uuid.uuid[0]);
}

xcode_uuid findUUIDForProject(const xcode_uuid* uuids, const cc_project_impl_t* project) {
  for (unsigned i = 0; i < array_count(uuids); ++i) {
    if (cc_data_.projects[i] == project) return uuids[i];
  }
  xcode_uuid empty = {0};
  return empty;
}

void add_build_setting(struct data_tree_t* dt, unsigned int node_build_settings, const char* k,
                       const char* v) {
  data_tree_api.set_object_value(dt, data_tree_api.create_object(dt, node_build_settings, k), v);
}

const char* xCodeStringFromGroup(const cc_group_impl_t** unique_groups, const char** group_ids,
                                 const cc_group_impl_t* group) {
  const unsigned num_groups = array_count(unique_groups);
  for (unsigned i = 0; i < num_groups; ++i) {
    if (unique_groups[i] == group) {
      return cc_printf("%s /* %s */", group_ids[i], group->name);
    }
  }

  assert(false && "Couldn't find group name");
  return "";
}

const char* xcodeFileTypeFromExtension(const char* ext) {
  if (strcmp(ext, "cpp") == 0) {
    return "sourcecode.cpp.cpp";
  } else if (strcmp(ext, "h") == 0) {
    return "sourcecode.c.h";
  } else if (strcmp(ext, "m") == 0) {
    return "sourcecode.c.objc";
  } else if (strcmp(ext, "mm") == 0) {
    return "sourcecode.cpp.objcpp";
  } else if (strcmp(ext, "metal") == 0) {
    return "sourcecode.metal";
  } else if (strcmp(ext, "plist") == 0) {
    return "text.plist.xml";
  } else if (strcmp(ext, "storyboard") == 0) {
    return "file.storyboard";
  } else if (strcmp(ext, "xcassets") == 0) {
    return "folder.assetcatalog";

    // Library file types
  } else if (strcmp(ext, "a") == 0) {
    return "archive.ar";
  } else if (strcmp(ext, "framework") == 0) {
    return "wrapper.framework";
  }

  // Else simply assume C file
  else {
    return "sourcecode.c.c";
  }
}

void export_tree_as_xcode_recursive_impl(FILE* f, const struct data_tree_t* dt, unsigned int node,
                                         unsigned int depth, bool no_newlines) {
  assert(node < array_count(dt->objects));

  const struct data_tree_object_t* obj = dt->objects + node;

  const bool is_root_section = (obj->name == NULL);

  bool child_no_newlines = no_newlines;

  // Skip the section if nothing is in it
  if (is_root_section) {
    if (obj->value_or_child.first_child == 0) {
      return;
    }

    // For clarity of build file section, keep those on a single line, just like XCode does
    if ((strstr(obj->comment, "PBXBuildFile") != NULL) ||
        (strstr(obj->comment, "PBXFileReference") != NULL)) {
      child_no_newlines = true;
    }
  }

  const char* preamble = "";
  if (!no_newlines) {
    for (unsigned int i = 0; i < depth; i++) {
      preamble = cc_printf("\t%s", preamble);
    }
  }

  if (obj->name != NULL) {
    fprintf(f, "%s%s", preamble, obj->name);
    if (obj->has_children && obj->comment) {
      fprintf(f, " /* %s */", obj->comment);
    }
    /*if (obj->first_parameter) {
      const struct data_tree_object_t* param_obj = dt->objects + obj->first_parameter;
      do {
        fprintf(f, " %s=\"%s\"", param_obj->name, param_obj->value_or_child.value);
      } while (param_obj->next_sibling && (param_obj = dt->objects + param_obj->next_sibling));
    }*/
  } else if (node != 0) {
    // Print as a top level comment in the file
    fprintf(f, "\n/* Begin %s section */\n", obj->comment);
  }

  const char newline_separator = (no_newlines ? ' ' : '\n');

  if (obj->has_children || obj->is_array) {
    if (obj->name) {
      if (obj->is_array) {
        fprintf(f, " = (%c", newline_separator);
      } else {
        fprintf(f, " = {%c", newline_separator);
      }
    }
    unsigned int child_index = obj->value_or_child.first_child;
    if (child_index) {
      const struct data_tree_object_t* child_obj = dt->objects + child_index;
      const char child_separator                 = obj->is_array ? ',' : ';';
      unsigned int child_depth                   = (obj->name) ? (depth + 1) : depth;
      do {
        if (!no_newlines && child_no_newlines) {
          fprintf(f, "%s", preamble);
        }
        export_tree_as_xcode_recursive_impl(f, dt, child_index, child_depth, child_no_newlines);
        if (child_obj->name) {
          fprintf(f, "%c%c", child_separator, newline_separator);
        }

        // Prepare for next iteration
        child_index = child_obj->next_sibling;
        child_obj   = (child_index > 0) ? (dt->objects + child_obj->next_sibling) : NULL;
      } while (child_obj);
    }
    if (obj->name) {
      const char node_closer = obj->is_array ? ')' : '}';
      fprintf(f, "%s%c", preamble, node_closer);
    }
  } else if (obj->value_or_child.value) {
    if (obj->name && obj->name[0]) {
      fprintf(f, " = %s", obj->value_or_child.value);
    } else {
      fprintf(f, "%s", obj->value_or_child.value);
    }
    if (obj->comment) {
      fprintf(f, " /* %s */", obj->comment);
    }
  }

  if (obj->name == NULL) {
    // Print as a top level comment in the file
    fprintf(f, "/* End %s section */\n", obj->comment);
  }
}

void export_tree_as_xcode(FILE* f, const struct data_tree_t* dt) {
  fprintf(f, "// !$*UTF8*$!\n");
  fprintf(f, "{\n");

  const struct data_tree_object_t* obj = dt->objects;
  unsigned int child_index             = obj->value_or_child.first_child;
  if (child_index) {
    const struct data_tree_object_t* child_obj = dt->objects + child_index;
    do {
      export_tree_as_xcode_recursive_impl(f, dt, child_index, 1, false);
      fprintf(f, ";\n");

      // Prepare for next iteration
      child_index = child_obj->next_sibling;
      child_obj   = (child_index > 0) ? (dt->objects + child_obj->next_sibling) : NULL;
    } while (child_obj);
  }
  fprintf(f, "}\n");
}

// Creates a child node with no name, and sets the value and comment on it. Used in XCode projects
// to create entries of an array with : ID /* comment */
void dt_api_add_child_value_and_comment(struct data_tree_t* dt, unsigned int parent_id,
                                        const char* value, const char* comment) {
  unsigned int node = data_tree_api.create_object(dt, parent_id, "");
  data_tree_api.set_object_value(dt, node, value);
  data_tree_api.set_object_comment(dt, node, comment);
}


void optionDeploymentTarget(struct data_tree_t* dt, unsigned int compile_group,
               const char* remaining_flag_value) {
  data_tree_api.set_object_value(
      dt, data_tree_api.get_or_create_object(dt, compile_group, "MACOSX_DEPLOYMENT_TARGET"), remaining_flag_value);
}

typedef struct xcode_compiler_flag {
  const char* flag;
  const func action;
} xcode_compiler_flag;

const xcode_compiler_flag xcode_known_compiler_flags_[] = {{"MACOSX_DEPLOYMENT_TARGET=", &optionDeploymentTarget}};

void xCodeCreateProjectFile(FILE* f, const cc_project_impl_t* in_project,
                            const xcode_uuid* projectFileReferenceUUIDs,
                            const char* build_to_base_path) {
  const cc_project_impl_t* p         = (cc_project_impl_t*)in_project;
  const struct data_tree_api* dt_api = &data_tree_api;
  struct data_tree_t dt              = dt_api->create();

  dt_api->set_object_value(&dt, dt_api->create_object(&dt, 0, "archiveVersion"), "1");
  dt.objects[dt_api->create_object(&dt, 0, "classes")].has_children = true;
  dt_api->set_object_value(&dt, dt_api->create_object(&dt, 0, "objectVersion"), "50");
  unsigned int nodeObjects = dt_api->create_object(&dt, 0, "objects");

  const char* substitution_keys[] = {"configuration", "platform"};
  // For MacOS currently the only allowed platform is  64-bit
  const char* substitution_values[] = {"$CONFIGURATION", "x64"};

  const unsigned files_count  = array_count(p->file_data);
  const char** file_ref_paths = {0};
  for (unsigned i = 0; i < files_count; ++i) {
    array_push(file_ref_paths, cc_printf("%s%s", build_to_base_path, p->file_data[i]->path));
  }

  for (unsigned state_idx = 0; state_idx < array_count(p->state); state_idx++) {
    const cc_state_impl_t* state = p->state + state_idx;
    // Also all the include paths
    for (unsigned includes_idx = 0; includes_idx < array_count(state->include_folders);
         includes_idx++) {
      const char* include_path            = state->include_folders[includes_idx];
      const bool is_absolute_path         = (include_path[0] == '/') || (include_path[1] == ':');
      const bool starts_with_env_variable = (include_path[0] == '$');
      if (!is_absolute_path && !starts_with_env_variable) {
        state->include_folders[includes_idx] = cc_printf("%s%s", build_to_base_path, include_path);
      }
    }
    // And referenced external libs
    for (unsigned libs_idx = 0; libs_idx < array_count(state->external_libs); libs_idx++) {
      const char* lib_path                = state->external_libs[libs_idx];
      const bool is_absolute_path         = (lib_path[0] == '/') || (lib_path[1] == ':');
      const bool starts_with_env_variable = (lib_path[0] == '$');
      if (!is_absolute_path && !starts_with_env_variable) {
        state->external_libs[libs_idx] = cc_printf("%s%s", build_to_base_path, lib_path);
      }
    }
  }

  const char** fileReferenceUUID = {0};
  const char** fileUUID          = {0};
  for (unsigned fi = 0; fi < files_count; ++fi) {
    array_push(fileReferenceUUID, xCodeUUID2String(xCodeGenerateUUID()));
    array_push(fileUUID, xCodeUUID2String(xCodeGenerateUUID()));
  }

  const char** dependencyFileReferenceUUID   = {0};
  const char** dependencyBuildUUID           = {0};
  const char** dependencyEmbedLibrairiesUUID = {0};
  for (unsigned fi = 0; fi < array_count(p->dependantOn); ++fi) {
    array_push(dependencyFileReferenceUUID, xCodeUUID2String(xCodeGenerateUUID()));
    array_push(dependencyBuildUUID, xCodeUUID2String(xCodeGenerateUUID()));
    array_push(dependencyEmbedLibrairiesUUID, xCodeUUID2String(xCodeGenerateUUID()));
  }

  // Extract frameworks from list of external libs
  const char** external_frameworks = {0};
  for (unsigned ipc = 0; ipc < array_count(p->state); ++ipc) {
    const bool is_global_state = ((p->configs[ipc] == NULL) && (p->architectures[ipc] == NULL));

    const cc_state_impl_t* s = &(p->state[ipc]);
    for (unsigned fi = 0; fi < array_count(s->external_libs);) {
      const char* lib_path = s->external_libs[fi];
      if (strstr(lib_path, ".framework") == NULL) {
        fi++;
      } else {
        if (!is_global_state) {
          LOG_ERROR_AND_QUIT(
              ERR_CONFIGURATION,
              "Apple framework added to state with configuration or architecture. This is not yet "
              "supported");
        }
        array_push(external_frameworks, lib_path);
        array_remove_at_index(s->external_libs, fi);
      }
    }
  }
  // No frameworks left in the state now.

  const char** dependencyExternalLibraryFileReferenceUUID = {0};
  const char** dependencyExternalLibraryBuildUUID         = {0};
  for (unsigned fi = 0; fi < array_count(external_frameworks); ++fi) {
    array_push(dependencyExternalLibraryFileReferenceUUID, xCodeUUID2String(xCodeGenerateUUID()));
    array_push(dependencyExternalLibraryBuildUUID, xCodeUUID2String(xCodeGenerateUUID()));
  }

  xcode_uuid outputFileReferenceUIID = findUUIDForProject(projectFileReferenceUUIDs, p);
  xcode_uuid outputTargetUIID        = xCodeGenerateUUID();

  const char* outputName = p->name;
  if (p->type == CCProjectTypeStaticLibrary) {
    outputName = cc_printf("lib%s.a", outputName);
  } else if (p->type == CCProjectTypeDynamicLibrary) {
    outputName = cc_printf("%s.dylib", outputName);
  }

  // Create list of groups needed.
  const cc_group_impl_t** unique_groups = {0};
  const char** unique_groups_id         = {0};
  for (size_t ig = 0; ig < array_count(p->file_data); ++ig) {
    size_t g = p->file_data[ig]->parent_group_idx;
    while (g) {
      bool already_contains_group = false;
      for (size_t i = 0; i < array_count(unique_groups); ++i) {
        if (&cc_data_.groups[g] == unique_groups[i]) {
          already_contains_group = true;
        }
      }
      if (!already_contains_group) {
        array_push(unique_groups, &cc_data_.groups[g]);
        array_push(unique_groups_id, xCodeUUID2String(xCodeGenerateUUID()));
      }
      g = cc_data_.groups[g].parent_group_idx;
    }
  }
  for (size_t ig = 0; ig < array_count(p->file_data_custom_command); ++ig) {
    size_t g = p->file_data_custom_command[ig]->parent_group_idx;
    while (g) {
      bool already_contains_group = false;
      for (size_t i = 0; i < array_count(unique_groups); ++i) {
        if (&cc_data_.groups[g] == unique_groups[i]) {
          already_contains_group = true;
        }
      }
      if (!already_contains_group) {
        array_push(unique_groups, &cc_data_.groups[g]);
        array_push(unique_groups_id, xCodeUUID2String(xCodeGenerateUUID()));
      }
      g = cc_data_.groups[g].parent_group_idx;
    }
  }
  const unsigned num_unique_groups = array_count(unique_groups);

  // Setup the top level sections in the file
  unsigned int PBXBuildFileSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXBuildFileSection, "PBXBuildFile");
  unsigned int PBXCopyFilesBuildPhaseSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXCopyFilesBuildPhaseSection, "PBXCopyFilesBuildPhase");
  unsigned int PBXFileReferenceSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXFileReferenceSection, "PBXFileReference");
  unsigned int PBXFrameworksBuildPhaseSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXFrameworksBuildPhaseSection, "PBXFrameworksBuildPhase");
  unsigned int PBXGroupSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXGroupSection, "PBXGroup");
  unsigned int PBXNativeTargetSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXNativeTargetSection, "PBXNativeTarget");
  unsigned int PBXProjectSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXProjectSection, "PBXProject");
  unsigned int PBXResourcesBuildPhaseSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXResourcesBuildPhaseSection, "PBXResourcesBuildPhase");
  unsigned int PBXSourcesBuildPhaseSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, PBXSourcesBuildPhaseSection, "PBXSourcesBuildPhase");
  unsigned int XCBuildConfigurationSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, XCBuildConfigurationSection, "XCBuildConfiguration");
  unsigned int XCConfigurationListSection = dt_api->create_object(&dt, nodeObjects, NULL);
  dt_api->set_object_comment(&dt, XCConfigurationListSection, "XCConfigurationList");
  {
    for (unsigned fi = 0; fi < files_count; ++fi) {
      const char* filename = p->file_data[fi]->path;
      if (is_source_file(filename) || is_buildable_resource_file(filename)) {
        unsigned int nodeFile = dt_api->create_object(&dt, PBXBuildFileSection, fileUUID[fi]);
        dt_api->set_object_comment(&dt, nodeFile,
                                   cc_printf("%s in Sources", strip_path(filename)));
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"), "PBXBuildFile");
        unsigned int fr = dt_api->create_object(&dt, nodeFile, "fileRef");
        dt_api->set_object_value(&dt, fr, fileReferenceUUID[fi]);
        dt_api->set_object_comment(&dt, fr, strip_path(filename));
      }
    }
    for (unsigned i = 0; i < array_count(p->dependantOn); ++i) {
      const char* id             = dependencyFileReferenceUUID[i];
      const char* buildID        = dependencyBuildUUID[i];
      const char* dependantName  = NULL;
      bool is_dynamic_dependency = (p->dependantOn[i]->type == CCProjectTypeDynamicLibrary);
      if (!is_dynamic_dependency) {
        dependantName = cc_printf("lib%s.a", p->dependantOn[i]->name);
      } else {
        dependantName = cc_printf("lib%s.dylib", p->dependantOn[i]->name);
      }
      {
        unsigned int nodeFile = dt_api->create_object(&dt, PBXBuildFileSection, buildID);
        dt_api->set_object_comment(&dt, nodeFile, cc_printf("%s in Frameworks", dependantName));
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"), "PBXBuildFile");
        unsigned int fr = dt_api->create_object(&dt, nodeFile, "fileRef");
        dt_api->set_object_value(&dt, fr, id);
        dt_api->set_object_comment(&dt, fr, dependantName);
      }
      if (is_dynamic_dependency) {
        unsigned int nodeFile =
            dt_api->create_object(&dt, PBXBuildFileSection, dependencyEmbedLibrairiesUUID[i]);
        dt_api->set_object_comment(&dt, nodeFile,
                                   cc_printf("%s in Embed Libraries", dependantName));
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"), "PBXBuildFile");
        unsigned int fr = dt_api->create_object(&dt, nodeFile, "fileRef");
        dt_api->set_object_value(&dt, fr, id);
        dt_api->set_object_comment(&dt, fr, dependantName);

        unsigned int settings = dt_api->create_object(&dt, nodeFile, "settings");
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, settings, "ATTRIBUTES"),
                                 "(CodeSignOnCopy, )");
      }
    }
    for (unsigned i = 0; i < array_count(external_frameworks); ++i) {
      const char* id            = dependencyExternalLibraryFileReferenceUUID[i];
      const char* buildID       = dependencyExternalLibraryBuildUUID[i];
      const char* dependantName = cc_printf("%s", strip_path(external_frameworks[i]));
      unsigned int nodeFile     = dt_api->create_object(&dt, PBXBuildFileSection, buildID);
      dt_api->set_object_comment(&dt, nodeFile, cc_printf("%s in Frameworks", dependantName));
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"), "PBXBuildFile");
      unsigned int fr = dt_api->create_object(&dt, nodeFile, "fileRef");
      dt_api->set_object_value(&dt, fr, id);
      dt_api->set_object_comment(&dt, fr, dependantName);
    }
  }

  {
    for (unsigned fi = 0; fi < files_count; ++fi) {
      const char* filename = p->file_data[fi]->path;
      const char* fileType = xcodeFileTypeFromExtension(file_extension(filename));
      unsigned int nodeFile =
          dt_api->create_object(&dt, PBXFileReferenceSection, fileReferenceUUID[fi]);
      dt_api->set_object_comment(&dt, nodeFile, strip_path(filename));
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"),
                               "PBXFileReference");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "fileEncoding"), "4");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "lastKnownFileType"),
                               fileType);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "name"),
                               strip_path(filename));
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "path"),
                               file_ref_paths[fi]);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "sourceTree"),
                               "SOURCE_ROOT");

      if (cc_is_verbose) {
        // printf("Adding file '%s' as '%s'\n", filename, file_ref_paths[fi]);
      }
    }

    {
      unsigned int nodeFile = dt_api->create_object(&dt, PBXFileReferenceSection,
                                                    xCodeUUID2String(outputFileReferenceUIID));
      dt_api->set_object_comment(&dt, nodeFile, outputName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"),
                               "PBXFileReference");
      if (p->type == CCProjectTypeConsoleApplication) {
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "explicitFileType"),
                                 "compiled.mach-o.executable");
      } else if (p->type == CCProjectTypeStaticLibrary) {
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "explicitFileType"),
                                 "archive.ar");
      } else if (p->type == CCProjectTypeDynamicLibrary) {
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "explicitFileType"),
                                 "compiled.mach-o.dylib");
      }
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "includeInIndex"), "0");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "path"), outputName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "sourceTree"),
                               "BUILT_PRODUCTS_DIR");
    }

    for (unsigned i = 0; i < array_count(p->dependantOn); ++i) {
      const char* id               = dependencyFileReferenceUUID[i];
      unsigned int nodeFile        = dt_api->create_object(&dt, PBXFileReferenceSection, id);
      const char* dependantName    = NULL;
      const char* explicitFileType = NULL;
      if (p->dependantOn[i]->type == CCProjectTypeStaticLibrary) {
        dependantName    = cc_printf("lib%s.a", p->dependantOn[i]->name);
        explicitFileType = "archive.ar";
      } else {
        dependantName    = cc_printf("lib%s.dylib", p->dependantOn[i]->name);
        explicitFileType = "compiled.mach-o.dylib";
      }
      dt_api->set_object_value(&dt, nodeFile, id);
      dt_api->set_object_comment(&dt, nodeFile, dependantName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"),
                               "PBXFileReference");

      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "explicitFileType"),
                               explicitFileType);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "path"), dependantName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "sourceTree"),
                               "BUILT_PRODUCTS_DIR");
    }
    for (unsigned i = 0; i < array_count(external_frameworks); ++i) {
      const char* id            = dependencyExternalLibraryFileReferenceUUID[i];
      const char* dependantName = cc_printf("%s", strip_path(external_frameworks[i]));
      const char* fileType      = xcodeFileTypeFromExtension(file_extension(dependantName));
      unsigned int nodeFile     = dt_api->create_object(&dt, PBXFileReferenceSection, id);
      dt_api->set_object_value(&dt, nodeFile, id);
      dt_api->set_object_comment(&dt, nodeFile, dependantName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "isa"),
                               "PBXFileReference");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "explicitFileType"),
                               fileType);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "name"), dependantName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "path"),
                               external_frameworks[i]);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFile, "sourceTree"),
                               "SDK_ROOT");
    }
  }

  const char* main_group_id      = xCodeUUID2String(xCodeGenerateUUID());
  const char* main_group_comment = "Main Group";
  {
    unsigned int group_children;
    {
      unsigned int nodeNT = dt_api->create_object(&dt, PBXGroupSection, main_group_id);
      dt_api->set_object_comment(&dt, nodeNT, main_group_comment);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeNT, "isa"), "PBXGroup");
      {
        unsigned int nodeChildren         = dt_api->create_object(&dt, nodeNT, "children");
        group_children                    = nodeChildren;
        dt.objects[nodeChildren].is_array = true;

        for (unsigned i = 0; i < array_count(unique_groups); ++i) {
          if (unique_groups[i]->parent_group_idx == 0) {
            unsigned int node = dt_api->create_object(&dt, nodeChildren, "");
            dt_api->set_object_value(
                &dt, node,
                xCodeStringFromGroup(unique_groups, unique_groups_id, unique_groups[i]));
          }
        }
        for (unsigned i = 0; i < array_count(p->file_data); ++i) {
          if (p->file_data[i]->parent_group_idx == 0) {
            dt_api_add_child_value_and_comment(&dt, nodeChildren, fileReferenceUUID[i],
                                               strip_path(p->file_data[i]->path));
          }
        }
      }
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeNT, "sourceTree"),
                               "\"<group>\"");
    }
    {
      for (unsigned i = 0; i < num_unique_groups; ++i) {
        const cc_group_impl_t* g = unique_groups[i];
        unsigned int nodeGroup   = dt_api->create_object(
            &dt, PBXGroupSection, xCodeStringFromGroup(unique_groups, unique_groups_id, g));
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "isa"), "PBXGroup");
        unsigned int nodeChildren         = dt_api->create_object(&dt, nodeGroup, "children");
        dt.objects[nodeChildren].is_array = true;

        for (unsigned fi = 0; fi < files_count; ++fi) {
          const char* filename              = p->file_data[fi]->path;
          const cc_group_impl_t* file_group = &cc_data_.groups[p->file_data[fi]->parent_group_idx];
          if (file_group == g) {
            dt_api_add_child_value_and_comment(&dt, nodeChildren, fileReferenceUUID[fi],
                                               strip_path(filename));
          }
        }
        for (unsigned gi = 0; gi < num_unique_groups; ++gi) {
          const cc_group_impl_t* child_group = unique_groups[gi];
          if (&cc_data_.groups[child_group->parent_group_idx] == g) {
            unsigned int node = dt_api->create_object(&dt, nodeChildren, "");
            dt_api->set_object_value(
                &dt, node, xCodeStringFromGroup(unique_groups, unique_groups_id, child_group));
          }
        }
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "name"),
                                 cc_printf("\"%s\"", g->name));
        dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "sourceTree"),
                                 "\"<group>\"");
      }
    }
    const bool references_libraries =
        (array_count(p->dependantOn) > 0) || (array_count(external_frameworks) > 0);
    if (references_libraries) {
      const char* id      = xCodeUUID2String(xCodeGenerateUUID());
      const char* comment = "Frameworks";
      dt_api_add_child_value_and_comment(&dt, group_children, id, comment);
      unsigned int nodeGroup = dt_api->create_object(&dt, PBXGroupSection, id);
      dt_api->set_object_comment(&dt, nodeGroup, comment);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "isa"), "PBXGroup");
      unsigned int nodeChildren         = dt_api->create_object(&dt, nodeGroup, "children");
      dt.objects[nodeChildren].is_array = true;
      for (unsigned i = 0; i < array_count(p->dependantOn); ++i) {
        const char* buildID       = dependencyFileReferenceUUID[i];
        const char* dependentName = NULL;
        if (p->dependantOn[i]->type == CCProjectTypeStaticLibrary) {
          dependentName = cc_printf("lib%s.a", strip_path(p->dependantOn[i]->name));
        } else if (p->dependantOn[i]->type == CCProjectTypeDynamicLibrary) {
          dependentName = cc_printf("lib%s.dylib", strip_path(p->dependantOn[i]->name));
        }
        dt_api_add_child_value_and_comment(&dt, nodeChildren, buildID, dependentName);
      }
      for (unsigned i = 0; i < array_count(external_frameworks); ++i) {
        const char* buildID       = dependencyExternalLibraryFileReferenceUUID[i];
        const char* dependantName = cc_printf("%s", strip_path(external_frameworks[i]));
        dt_api_add_child_value_and_comment(&dt, nodeChildren, buildID, dependantName);
      }
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "name"), "Frameworks");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "sourceTree"),
                               "\"<group>\"");
    }
    {
      const char* id = xCodeUUID2String(xCodeGenerateUUID());
      dt_api_add_child_value_and_comment(&dt, group_children, id, "Products");

      unsigned int nodeGroup = dt_api->create_object(&dt, PBXGroupSection, id);
      dt_api->set_object_comment(&dt, nodeGroup, "Products");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "isa"), "PBXGroup");
      unsigned int nodeChildren         = dt_api->create_object(&dt, nodeGroup, "children");
      dt.objects[nodeChildren].is_array = true;
      dt_api_add_child_value_and_comment(&dt, nodeChildren,
                                         xCodeUUID2String(outputFileReferenceUIID), outputName);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "name"), "Products");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeGroup, "sourceTree"),
                               "\"<group>\"");
    }
  }

  unsigned int node_build_phases;

  const char* build_configurations_id  = xCodeUUID2String(xCodeGenerateUUID());
  const char* build_configurations_id2 = xCodeUUID2String(xCodeGenerateUUID());
  const char* build_configurations_comment =
      cc_printf("Build configuration list for PBXNativeTarget \"%s\"", p->name);
  {
    unsigned int nodeNT =
        dt_api->create_object(&dt, PBXNativeTargetSection, xCodeUUID2String(outputTargetUIID));
    dt_api->set_object_comment(&dt, nodeNT, p->name);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeNT, "isa"), "PBXNativeTarget");
    unsigned int nodeBCL = dt_api->create_object(&dt, nodeNT, "buildConfigurationList");
    dt_api->set_object_value(&dt, nodeBCL, build_configurations_id2);
    dt_api->set_object_comment(&dt, nodeBCL, build_configurations_comment);
    node_build_phases                      = dt_api->create_object(&dt, nodeNT, "buildPhases");
    dt.objects[node_build_phases].is_array = true;

    unsigned int nodeBR                   = dt_api->create_object(&dt, nodeNT, "buildRules");
    dt.objects[nodeBR].is_array           = true;
    unsigned int nodeDependencies         = dt_api->create_object(&dt, nodeNT, "dependencies");
    dt.objects[nodeDependencies].is_array = true;
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeNT, "name"), p->name);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeNT, "productName"), p->name);
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeNT, "productReference"),
        cc_printf("%s /* %s */", xCodeUUID2String(outputFileReferenceUIID), outputName));
    unsigned int nodePT = dt_api->create_object(&dt, nodeNT, "productType");

    switch (p->type) {
      case CCProjectTypeConsoleApplication:
        dt_api->set_object_value(&dt, nodePT, "\"com.apple.product-type.tool\"");
        break;
      case CCProjectTypeWindowedApplication:
        dt_api->set_object_value(&dt, nodePT, "\"com.apple.product-type.application\"");
        break;
      case CCProjectTypeStaticLibrary:
        dt_api->set_object_value(&dt, nodePT, "\"com.apple.product-type.library.static\"");
        break;
      case CCProjectTypeDynamicLibrary:
        dt_api->set_object_value(&dt, nodePT, "\"com.apple.product-type.library.dynamic\"");
        break;
    }
  }

  bool has_dynamic_lib_dependency = false;
  {
    const char* id = xCodeUUID2String(xCodeGenerateUUID());
    dt_api_add_child_value_and_comment(&dt, node_build_phases, id, "Frameworks");
    unsigned int nodeFrameworks = dt_api->create_object(&dt, PBXFrameworksBuildPhaseSection, id);
    dt_api->set_object_comment(&dt, nodeFrameworks, "Frameworks");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFrameworks, "isa"),
                             "PBXFrameworksBuildPhase");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFrameworks, "buildActionMask"),
                             "2147483647");
    unsigned int nodeFiles         = dt_api->create_object(&dt, nodeFrameworks, "files");
    dt.objects[nodeFiles].is_array = true;
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeFrameworks, "runOnlyForDeploymentPostprocessing"),
        "0");

    for (unsigned i = 0; i < array_count(p->dependantOn); ++i) {
      const char* buildID       = dependencyBuildUUID[i];
      const char* dependentName = NULL;
      if (p->dependantOn[i]->type == CCProjectTypeStaticLibrary) {
        dependentName = cc_printf("lib%s.a", p->dependantOn[i]->name);
      } else if (p->dependantOn[i]->type == CCProjectTypeDynamicLibrary) {
        dependentName = cc_printf("lib%s.dylib", p->dependantOn[i]->name);
      }
      dt_api_add_child_value_and_comment(&dt, nodeFiles, buildID,
                                         cc_printf("%s in Frameworks", dependentName));
      if (p->dependantOn[i]->type == CCProjectTypeDynamicLibrary) {
        has_dynamic_lib_dependency = true;
      }
    }
    for (unsigned i = 0; i < array_count(external_frameworks); ++i) {
      const char* buildID       = dependencyExternalLibraryBuildUUID[i];
      const char* dependantName = cc_printf("%s", strip_path(external_frameworks[i]));
      dt_api_add_child_value_and_comment(&dt, nodeFiles, buildID,
                                         cc_printf("%s in Frameworks", dependantName));
    }
  }

  const char* project_object_id = xCodeUUID2String(xCodeGenerateUUID());
  {
    unsigned int nodeProject = dt_api->create_object(&dt, PBXProjectSection, project_object_id);
    dt_api->set_object_comment(&dt, nodeProject, "Project object");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeProject, "isa"), "PBXProject");
    unsigned int nodeAttributes = dt_api->create_object(&dt, nodeProject, "attributes");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeAttributes, "LastUpgradeCheck"),
                             "1130");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeAttributes, "ORGANIZATIONNAME"),
                             "\"Daedalus Development\"");
    unsigned int nodeTA = dt_api->create_object(&dt, nodeAttributes, "TargetAttributes");
    unsigned int node   = dt_api->create_object(&dt, nodeTA, xCodeUUID2String(outputTargetUIID));
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "CreatedOnToolsVersion"),
                             "11.3");

    unsigned int bcl = dt_api->create_object(&dt, nodeProject, "buildConfigurationList");
    dt_api->set_object_value(&dt, bcl, build_configurations_id);
    dt_api->set_object_comment(&dt, bcl, build_configurations_comment);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeProject, "compatibilityVersion"),
                             "\"Xcode 9.3\"");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeProject, "developmentRegion"),
                             "en");
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeProject, "hasScannedForEncodings"), "0");
    unsigned int kr         = dt_api->create_object(&dt, nodeProject, "knownRegions");
    dt.objects[kr].is_array = true;
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, kr, ""), "en");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, kr, ""), "Base");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeProject, "mainGroup"),
                             main_group_id);
    unsigned int prg = dt_api->create_object(&dt, nodeProject, "productRefGroup");
    dt_api->set_object_value(&dt, prg, "403CC53C23EB479400558E07");
    dt_api->set_object_comment(&dt, prg, "Products");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeProject, "projectDirPath"),
                             "\"\"");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeProject, "projectRoot"), "\"\"");
    unsigned int nodeTargets         = dt_api->create_object(&dt, nodeProject, "targets");
    dt.objects[nodeTargets].is_array = true;
    dt_api_add_child_value_and_comment(&dt, nodeTargets, xCodeUUID2String(outputTargetUIID),
                                       p->name);
  }

  {
    const char* id = xCodeUUID2String(xCodeGenerateUUID());
    dt_api_add_child_value_and_comment(&dt, node_build_phases, id, "Resources");
    unsigned int nodeResources = dt_api->create_object(&dt, PBXResourcesBuildPhaseSection, id);
    dt_api->set_object_comment(&dt, nodeResources, "Resources");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeResources, "isa"),
                             "PBXResourcesBuildPhase");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeResources, "buildActionMask"),
                             "2147483647");
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeResources, "files");
      dt.objects[nodeFiles].is_array = true;
      for (unsigned fi = 0; fi < files_count; ++fi) {
        const char* filename = p->file_data[fi]->path;
        if (is_buildable_resource_file(filename)) {
          dt_api_add_child_value_and_comment(&dt, nodeFiles, fileUUID[fi], strip_path(filename));
        }
      }
    }
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeResources, "runOnlyForDeploymentPostprocessing"), "0");
  }

  for (unsigned fi = 0; fi < array_count(p->file_data_custom_command); fi++) {
    const struct cc_file_custom_command_t_* file = p->file_data_custom_command[fi];

    const char* input_output_substitution_keys[]   = {"input", "output"};
    const char* input_output_substitution_values[] = {file->path, file->output_file};

    const char* custom_command = cc_substitute(file->command, substitution_keys,
                                               substitution_values, countof(substitution_keys));
    custom_command =
        cc_substitute(custom_command, input_output_substitution_keys,
                      input_output_substitution_values, countof(input_output_substitution_keys));

    const char* id      = xCodeUUID2String(xCodeGenerateUUID());
    const char* comment = "ShellScript - CustomCommand";
    dt_api_add_child_value_and_comment(&dt, node_build_phases, id, comment);
    unsigned int nodeShellScript = dt_api->create_object(&dt, nodeObjects, id);
    dt_api->set_object_comment(&dt, nodeShellScript, comment);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "isa"),
                             "PBXShellScriptBuildPhase");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "buildActionMask"),
                             "2147483647");
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeShellScript, "files");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles = dt_api->create_object(&dt, nodeShellScript, "inputFileListPaths");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeShellScript, "inputPaths");
      dt.objects[nodeFiles].is_array = true;
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFiles, ""),
                               cc_printf("\"$(SRCROOT)/%s\"", file->path));
    }
    {
      unsigned int nodeFiles = dt_api->create_object(&dt, nodeShellScript, "outputFileListPaths");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeShellScript, "outputPaths");
      dt.objects[nodeFiles].is_array = true;
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeFiles, ""),
                               cc_printf("\"$(SRCROOT)/%s\"", file->output_file));
    }
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeShellScript, "runOnlyForDeploymentPostprocessing"),
        "0");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "shellPath"),
                             "/bin/sh");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "shellScript"),
                             cc_printf("\"cd $SRCROOT && %s\"", custom_command));
  }

  {
    const char* id      = xCodeUUID2String(xCodeGenerateUUID());
    const char* comment = "Sources";
    dt_api_add_child_value_and_comment(&dt, node_build_phases, id, comment);
    unsigned int nodeSourcesBuildPhase =
        dt_api->create_object(&dt, PBXSourcesBuildPhaseSection, id);
    dt_api->set_object_comment(&dt, nodeSourcesBuildPhase, comment);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeSourcesBuildPhase, "isa"),
                             "PBXSourcesBuildPhase");
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeSourcesBuildPhase, "buildActionMask"), "2147483647");
    unsigned int nodeFiles = dt_api->create_object(&dt, nodeSourcesBuildPhase, "files");
    dt_api->set_object_value(
        &dt,
        dt_api->create_object(&dt, nodeSourcesBuildPhase, "runOnlyForDeploymentPostprocessing"),
        "0");
    dt.objects[nodeFiles].is_array = true;
    for (unsigned fi = 0; fi < files_count; ++fi) {
      const char* filename = p->file_data[fi]->path;
      if (is_source_file(filename)) {
        dt_api_add_child_value_and_comment(&dt, nodeFiles, fileUUID[fi],
                                           cc_printf("%s in Sources", strip_path(filename)));
      }
    }
  }

  if (p->type == CCProjectTypeConsoleApplication) {
    // XCode builds to some internal location. Use an additional step to then copy the binary to
    // where CConstruct wants it.
    {
      const char* id      = xCodeUUID2String(xCodeGenerateUUID());
      const char* comment = "CopyFiles - Binary To CConstruct location";
      dt_api_add_child_value_and_comment(&dt, node_build_phases, id, comment);
      unsigned int node = dt_api->create_object(&dt, PBXCopyFilesBuildPhaseSection, id);
      dt_api->set_object_comment(&dt, node, comment);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "isa"),
                               "PBXCopyFilesBuildPhase");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "buildActionMask"),
                               "2147483647");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "dstPath"),
                               "/usr/share/man/man1/");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "dstSubfolderSpec"), "0");

      unsigned int nodeFiles         = dt_api->create_object(&dt, node, "files");
      dt.objects[nodeFiles].is_array = true;
      dt_api->set_object_value(
          &dt, dt_api->create_object(&dt, node, "runOnlyForDeploymentPostprocessing"), "1");
    }

    if (has_dynamic_lib_dependency) {
      const char* id      = xCodeUUID2String(xCodeGenerateUUID());
      const char* comment = "Embed Libraries";
      dt_api_add_child_value_and_comment(&dt, node_build_phases, id, comment);
      unsigned int node = dt_api->create_object(&dt, PBXCopyFilesBuildPhaseSection, id);
      dt_api->set_object_comment(&dt, node, comment);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "isa"),
                               "PBXCopyFilesBuildPhase");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "buildActionMask"),
                               "2147483647");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "dstPath"), "\"\"");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "dstSubfolderSpec"), "10");
      unsigned int nodeFiles         = dt_api->create_object(&dt, node, "files");
      dt.objects[nodeFiles].is_array = true;
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, node, "name"),
                               cc_printf("\"%s\"", comment));
      dt_api->set_object_value(
          &dt, dt_api->create_object(&dt, node, "runOnlyForDeploymentPostprocessing"), "0");

      for (unsigned i = 0; i < array_count(p->dependantOn); ++i) {
        if (p->dependantOn[i]->type == CCProjectTypeDynamicLibrary) {
          dt_api_add_child_value_and_comment(
              &dt, nodeFiles, dependencyEmbedLibrairiesUUID[i],
              cc_printf("lib%s.dylib in Embed Libraries", p->dependantOn[i]->name));
        }
      }
    }
  }

  // This actually has to be added after Sources so that the binary has been built before the
  // post-build action
  bool has_post_build_action = (p->postBuildAction != 0);
  if (has_post_build_action) {
    const char* id      = xCodeUUID2String(xCodeGenerateUUID());
    const char* comment = "ShellScript - Post-Build Action";
    dt_api_add_child_value_and_comment(&dt, node_build_phases, id, comment);

    unsigned int nodeShellScript = dt_api->create_object(&dt, nodeObjects, id);
    dt_api->set_object_comment(&dt, nodeShellScript, comment);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "isa"),
                             "PBXShellScriptBuildPhase");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "buildActionMask"),
                             "2147483647");
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeShellScript, "files");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles = dt_api->create_object(&dt, nodeShellScript, "inputFileListPaths");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeShellScript, "inputPaths");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles = dt_api->create_object(&dt, nodeShellScript, "outputFileListPaths");
      dt.objects[nodeFiles].is_array = true;
    }
    {
      unsigned int nodeFiles         = dt_api->create_object(&dt, nodeShellScript, "outputPaths");
      dt.objects[nodeFiles].is_array = true;
    }
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeShellScript, "runOnlyForDeploymentPostprocessing"),
        "0");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "shellPath"),
                             "/bin/sh");
    const char* postBuildAction = cc_printf("%s", p->postBuildAction);
    postBuildAction = cc_substitute(postBuildAction, substitution_keys, substitution_values,
                                    countof(substitution_keys));

    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeShellScript, "shellScript"),
                             cc_printf("\"%s\"", postBuildAction));
  }

  unsigned int node_project_configurations;
  unsigned int node_target_configurations;
  {
    unsigned int nodeConfigurationList =
        dt_api->create_object(&dt, XCConfigurationListSection, build_configurations_id);
    dt_api->set_object_comment(&dt, nodeConfigurationList, build_configurations_comment);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeConfigurationList, "isa"),
                             "XCConfigurationList");
    node_project_configurations =
        dt_api->create_object(&dt, nodeConfigurationList, "buildConfigurations");
    dt.objects[node_project_configurations].is_array = true;
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeConfigurationList, "defaultConfigurationIsVisible"),
        "0");
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, nodeConfigurationList, "defaultConfigurationName"),
        "Release");
  }

  {
    unsigned int node_xc =
        dt_api->create_object(&dt, XCConfigurationListSection, build_configurations_id2);
    dt_api->set_object_comment(&dt, node_xc, build_configurations_comment);
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, node_xc, "isa"),
                             "XCConfigurationList");
    node_target_configurations = dt_api->create_object(&dt, node_xc, "buildConfigurations");
    dt.objects[node_target_configurations].is_array = true;
    dt_api->set_object_value(
        &dt, dt_api->create_object(&dt, node_xc, "defaultConfigurationIsVisible"), "0");
    dt_api->set_object_value(&dt, dt_api->create_object(&dt, node_xc, "defaultConfigurationName"),
                             "Release");
  }

  // Find Info.plist
  const char* info_plist_path = NULL;
  for (unsigned i = 0; i < files_count; i++) {
    const char* filename = strip_path(p->file_data[i]->path);
    if (strcmp(filename, "Info.plist") == 0) {
      info_plist_path = file_ref_paths[i];
    }
  }

  const unsigned num_configurations = array_count(cc_data_.configurations);
  for (unsigned i = 0; i < num_configurations; ++i) {
    const cc_configuration_impl_t* config = cc_data_.configurations[i];
    const char* config_name               = cc_data_.configurations[i]->label;

    EStateWarningLevel combined_warning_level = EStateWarningLevelDefault;
    bool shouldDisableWarningsAsError         = false;
    for (unsigned ipc = 0; ipc < array_count(p->state); ++ipc) {
      const cc_state_impl_t* flags = &(p->state[ipc]);

      // TODO ordering and combination so that more specific flags can override general ones
      if ((p->configs[ipc] != config) && (p->configs[ipc] != NULL)) continue;
      // if ((p->architectures[ipc] != arch) && (p->architectures[ipc] != NULL)) continue;

      shouldDisableWarningsAsError = flags->disableWarningsAsErrors;
      combined_warning_level       = flags->warningLevel;
    }
    
    {
      const char* config_id = xCodeUUID2String(xCodeGenerateUUID());

      dt_api_add_child_value_and_comment(&dt, node_project_configurations, config_id, config_name);

      const unsigned int nodeBuildConfigurationList =
          dt_api->create_object(&dt, XCBuildConfigurationSection, config_id);
      dt_api->set_object_comment(&dt, nodeBuildConfigurationList, config_name);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeBuildConfigurationList, "isa"),
                               "XCBuildConfiguration");
      const unsigned int nodeBuildSettings =
          dt_api->create_object(&dt, nodeBuildConfigurationList, "buildSettings");

      const unsigned int node_preprocessor_defines =
          dt_api->create_object(&dt, nodeBuildSettings, "GCC_PREPROCESSOR_DEFINITIONS");
      dt.objects[node_preprocessor_defines].is_array = true;
      dt_api_add_child_value_and_comment(&dt, node_preprocessor_defines, "\"$(inherited)\"", NULL);
      if (strcmp(config->label, "Debug") == 0) {
        dt_api_add_child_value_and_comment(&dt, node_preprocessor_defines, "\"DEBUG=1\"", NULL);
        add_build_setting(&dt, nodeBuildSettings, "DEBUG_INFORMATION_FORMAT", "dwarf");
        add_build_setting(&dt, nodeBuildSettings, "ENABLE_TESTABILITY", "YES");
        add_build_setting(&dt, nodeBuildSettings, "GCC_OPTIMIZATION_LEVEL", "0");
      } else {
        add_build_setting(&dt, nodeBuildSettings, "DEBUG_INFORMATION_FORMAT",
                          "\"dwarf-with-dsym\"");
        add_build_setting(&dt, nodeBuildSettings, "ENABLE_NS_ASSERTIONS", "NO");
      }

      const unsigned int node_header_search_paths =
          dt_api->create_object(&dt, nodeBuildSettings, "HEADER_SEARCH_PATHS");
      dt.objects[node_header_search_paths].is_array = true;

      const unsigned int node_additional_compiler_flags =
          dt_api->create_object(&dt, nodeBuildSettings, "OTHER_CFLAGS");
      dt.objects[node_additional_compiler_flags].is_array = true;


      assert(array_count(cc_data_.architectures) == 1);
      assert(cc_data_.architectures[0]->type == EArchitectureX64);
      const char* resolved_output_folder = cc_substitute(
          p->outputFolder, substitution_keys, substitution_values, countof(substitution_keys));

      const char* safe_output_folder = cc_printf("\"%s\"", resolved_output_folder);

      add_build_setting(&dt, nodeBuildSettings, "GCC_C_LANGUAGE_STANDARD",
                        "c11");  // TODO:Expose this?
      add_build_setting(&dt, nodeBuildSettings, "CLANG_CXX_LANGUAGE_STANDARD",
                        "\"c++0x\"");  // TODO:Expose this?

      if (!shouldDisableWarningsAsError) {
        add_build_setting(&dt, nodeBuildSettings, "GCC_TREAT_WARNINGS_AS_ERRORS", "YES");
      } else {
        add_build_setting(&dt, nodeBuildSettings, "GCC_TREAT_WARNINGS_AS_ERRORS", "NO");
      }
      const char* default_enabled_warnings[] = {
          "CLANG_WARN_DELETE_NON_VIRTUAL_DTOR",
          "CLANG_WARN_DIRECT_OBJC_ISA_USAGE",
          "CLANG_WARN_MISSING_NOESCAPE",
          "CLANG_WARN_OBJC_ROOT_CLASS",
          "CLANG_WARN_PRAGMA_PACK",
          "CLANG_WARN_PRIVATE_MODULE",
          "CLANG_WARN_UNGUARDED_AVAILABILITY",
          "CLANG_WARN_VEXING_PARSE",
          "CLANG_WARN__ARC_BRIDGE_CAST_NONARC",
          "GCC_WARN_ABOUT_DEPRECATED_FUNCTIONS",
          "GCC_WARN_ABOUT_INVALID_OFFSETOF_MACRO",
          "GCC_WARN_ABOUT_POINTER_SIGNEDNESS",
          "GCC_WARN_ALLOW_INCOMPLETE_PROTOCOL",
          "GCC_WARN_CHECK_SWITCH_STATEMENTS",
          "GCC_WARN_MISSING_PARENTHESES",
          "GCC_WARN_TYPECHECK_CALLS_TO_PRINTF",
      };
      const char* high_enabled_warnings[] = {
          "CLANG_WARN_EMPTY_BODY",
          "CLANG_WARN_IMPLICIT_SIGN_CONVERSION",
          "CLANG_WARN_SEMICOLON_BEFORE_METHOD_BODY",
          "CLANG_WARN_SUSPICIOUS_IMPLICIT_CONVERSION",
          "CLANG_WARN_UNREACHABLE_CODE",
          "CLANG_WARN_SUSPICIOUS_IMPLICIT_CONVERSION",
          "CLANG_WARN_EMPTY_BODY",
          "CLANG_WARN_IMPLICIT_SIGN_CONVERSION",
          "CLANG_WARN_SUSPICIOUS_IMPLICIT_CONVERSION",
          "CLANG_WARN_UNREACHABLE_CODE",
          "GCC_WARN_64_TO_32_BIT_CONVERSION",
          "GCC_WARN_ABOUT_MISSING_FIELD_INITIALIZERS",
          "GCC_WARN_ABOUT_MISSING_NEWLINE",
          "GCC_WARN_ABOUT_RETURN_TYPE",
          "GCC_WARN_CHECK_SWITCH_STATEMENTS",
          "GCC_WARN_HIDDEN_VIRTUAL_FUNCTIONS",
          "GCC_WARN_INITIALIZER_NOT_FULLY_BRACKETED",
          "GCC_WARN_MISSING_PARENTHESES",
          "GCC_WARN_PEDANTIC",
          "GCC_WARN_SHADOW",
          "GCC_WARN_SIGN_COMPARE",
          "GCC_WARN_TYPECHECK_CALLS_TO_PRINTF",
          "GCC_WARN_UNINITIALIZED_AUTOS",
          "GCC_WARN_UNKNOWN_PRAGMAS",
          "GCC_WARN_UNUSED_VALUE",
          "GCC_WARN_UNUSED_FUNCTION",
          "GCC_WARN_UNUSED_LABEL",
          "GCC_WARN_UNUSED_VARIABLE",
          //"RUN_CLANG_STATIC_ANALYZER",
      };
      if (combined_warning_level == EStateWarningLevelHigh) {
        for (size_t ibs = 0; ibs < countof(high_enabled_warnings); ++ibs) {
          add_build_setting(&dt, nodeBuildSettings, high_enabled_warnings[ibs], "YES");
        }
        for (size_t ibs = 0; ibs < countof(default_enabled_warnings); ++ibs) {
          add_build_setting(&dt, nodeBuildSettings, default_enabled_warnings[ibs], "YES");
        }

        dt_api_add_child_value_and_comment(&dt, node_additional_compiler_flags, "\"-Wformat=2\"",
                                           NULL);
        dt_api_add_child_value_and_comment(&dt, node_additional_compiler_flags, "\"-Wextra\"",
                                           NULL);
        // disabling unused parameters needs to be done after -Wextra
        dt_api_add_child_value_and_comment(&dt, node_additional_compiler_flags,
                                           "\"-Wno-unused-parameter\"", NULL);
        // This warning can complain about my_struct s = {0};
        // Ref:
        // https://stackoverflow.com/questions/13905200/is-it-wise-to-ignore-gcc-clangs-wmissing-braces-warning
        dt_api_add_child_value_and_comment(&dt, node_additional_compiler_flags,
                                           "\"-Wno-missing-braces\"", NULL);

        // https://github.com/boredzo/Warnings-xcconfig/wiki/Warnings-Explained

      } else if (combined_warning_level == EStateWarningLevelNone) {
        add_build_setting(&dt, nodeBuildSettings, "GCC_WARN_INHIBIT_ALL_WARNINGS", "YES");
      } else if (combined_warning_level == EStateWarningLevelMedium) {
        for (size_t iw = 0; iw < countof(default_enabled_warnings); ++iw) {
          add_build_setting(&dt, nodeBuildSettings, default_enabled_warnings[iw], "YES");
        }
      }

      add_build_setting(&dt, nodeBuildSettings, "ALWAYS_SEARCH_USER_PATHS", "NO");
      add_build_setting(&dt, nodeBuildSettings, "CONFIGURATION_BUILD_DIR", safe_output_folder);

      add_build_setting(&dt, nodeBuildSettings, "ONLY_ACTIVE_ARCH", "YES");
      assert(array_count(cc_data_.platforms) == 1);
      switch (cc_data_.platforms[0]->type) {
        case EPlatformDesktop:
          add_build_setting(&dt, nodeBuildSettings, "MACOSX_DEPLOYMENT_TARGET", "10.14");
          add_build_setting(&dt, nodeBuildSettings, "SDKROOT", "macosx");
          break;
        case EPlatformPhone:
          add_build_setting(&dt, nodeBuildSettings, "IPHONEOS_DEPLOYMENT_TARGET", "13.2");
          add_build_setting(&dt, nodeBuildSettings, "SDKROOT", "iphoneos");
          add_build_setting(&dt, nodeBuildSettings, "CLANG_ENABLE_MODULES", "YES");
          add_build_setting(&dt, nodeBuildSettings, "CLANG_ENABLE_OBJC_ARC", "YES");
          add_build_setting(&dt, nodeBuildSettings, "CLANG_ENABLE_OBJC_WEAK", "YES");

          break;
      }

      for (unsigned ipc = 0; ipc < array_count(p->state); ++ipc) {
        const cc_state_impl_t* flags = &(p->state[ipc]);

        // TODO ordering and combination so that more specific flags can override general ones
        if ((p->configs[ipc] != config) && (p->configs[ipc] != NULL)) continue;
        // if ((p->architectures[ipc] != arch) && (p->architectures[ipc] != NULL)) continue;

        shouldDisableWarningsAsError = flags->disableWarningsAsErrors;
        combined_warning_level       = flags->warningLevel;

        for (unsigned pdi = 0; pdi < array_count(flags->defines); ++pdi) {
          dt_api_add_child_value_and_comment(&dt, node_preprocessor_defines,
                                             cc_printf("\"%s\"", flags->defines[pdi]), NULL);
        }

        for (unsigned cfi = 0; cfi < array_count(flags->compile_options); ++cfi) {
          // Index in known flags
          const char* current_flag = flags->compile_options[cfi];
          bool found               = false;
          for (unsigned kfi = 0; kfi < countof(known_compiler_flags); kfi++) {
            const char* foundString = strstr(current_flag, xcode_known_compiler_flags_[kfi].flag);
            if ( foundString) {
              found = true;
              xcode_known_compiler_flags_[kfi].action(&dt, nodeBuildSettings, foundString+strlen(xcode_known_compiler_flags_[kfi].flag));
            }
          }
          if (!found) {
            dt_api_add_child_value_and_comment(&dt, node_additional_compiler_flags,
                                              cc_printf("\"%s\"", flags->compile_options[cfi]),
                                              NULL);
          }
        }
        for (unsigned ifi = 0; ifi < array_count(flags->include_folders); ++ifi) {
          // Order matters here, so append
          add_build_setting(&dt, node_header_search_paths, flags->include_folders[ifi], NULL);
        }
      }
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeBuildConfigurationList, "name"),
                               config_name);
    }
    {
      const char* id2 = xCodeUUID2String(xCodeGenerateUUID());
      dt_api_add_child_value_and_comment(&dt, node_target_configurations, id2, config_name);
      unsigned int nodeBuildConfigurationList =
          dt_api->create_object(&dt, XCBuildConfigurationSection, id2);
      dt_api->set_object_comment(&dt, nodeBuildConfigurationList,
                                 cc_data_.configurations[i]->label);
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeBuildConfigurationList, "isa"),
                               "XCBuildConfiguration");
      unsigned int nodeBuildSettings =
          dt_api->create_object(&dt, nodeBuildConfigurationList, "buildSettings");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeBuildConfigurationList, "name"),
                               cc_data_.configurations[i]->label);

      switch (cc_data_.platforms[0]->type) {
        case EPlatformDesktop: {
          dt_api->set_object_value(
              &dt, dt_api->create_object(&dt, nodeBuildSettings, "CODE_SIGN_STYLE"), "Automatic");
          if (info_plist_path) {
            dt_api->set_object_value(
                &dt, dt_api->create_object(&dt, nodeBuildSettings, "INFOPLIST_FILE"),
                info_plist_path);
          }

          unsigned int nodeSearchPaths =
              dt_api->create_object(&dt, nodeBuildSettings, "LIBRARY_SEARCH_PATHS");
          dt.objects[nodeSearchPaths].is_array = true;
          unsigned int node                    = dt_api->create_object(&dt, nodeSearchPaths, "");
          dt_api->set_object_value(&dt, node, "\"$(inherited)\"");
          const char* link_additional_dependencies = "";
          for (unsigned ipc = 0; ipc < array_count(p->state); ++ipc) {
            const cc_state_impl_t* flags = &(p->state[ipc]);

            // TODO ordering and combination so that more specific flags can override general
            // ones
            if ((p->configs[ipc] != config) && (p->configs[ipc] != NULL)) continue;

            for (unsigned di = 0; di < array_count(flags->external_libs); di++) {
              const char* lib_path_from_base = flags->external_libs[di];
              const char* relative_lib_path =
                  make_path_relative(build_to_base_path, lib_path_from_base);
              const char* lib_name            = strip_path(relative_lib_path);
              const char* lib_folder          = make_uri(folder_path_only(lib_path_from_base));
              const char* resolved_lib_folder = cc_substitute(
                  lib_folder, substitution_keys, substitution_values, countof(substitution_keys));

              link_additional_dependencies =
                  cc_printf("%s -l%s", link_additional_dependencies, lib_name);
              node = dt_api->create_object(&dt, nodeSearchPaths, "");
              dt_api->set_object_value(&dt, node,
                                       cc_printf("\"$(PROJECT_DIR)/%s\"", resolved_lib_folder));
            }
          }
          if (link_additional_dependencies[0] != 0) {
            dt_api->set_object_value(
                &dt, dt_api->create_object(&dt, nodeBuildSettings, "OTHER_LDFLAGS"),
                cc_printf("\"%s\"", link_additional_dependencies));
          }
          if (p->type == CCProjectTypeDynamicLibrary) {
            dt_api->set_object_value(
                &dt, dt_api->create_object(&dt, nodeBuildSettings, "EXECUTABLE_PREFIX"), "lib");
            dt_api->set_object_value(&dt,
                                     dt_api->create_object(&dt, nodeBuildSettings, "INSTALL_PATH"),
                                     "\"@executable_path\"");
          }
        } break;
        case EPlatformPhone: {
          dt_api->set_object_value(&dt,
                                   dt_api->create_object(&dt, nodeBuildSettings, "INFOPLIST_FILE"),
                                   "../src/Info.plist");
          unsigned int nodeSearchPaths =
              dt_api->create_object(&dt, nodeBuildSettings, "LIBRARY_SEARCH_PATHS");
          dt.objects[nodeSearchPaths].is_array = true;
          unsigned int node                    = dt_api->create_object(&dt, nodeSearchPaths, "");
          dt_api->set_object_value(&dt, node, "\"$(inherited)\"");
          node = dt_api->create_object(&dt, nodeSearchPaths, "");
          dt_api->set_object_value(&dt, node, "\"@executable_path/Frameworks\"");
          dt_api->set_object_value(
              &dt, dt_api->create_object(&dt, nodeBuildSettings, "PRODUCT_BUNDLE_IDENTIFIER"),
              "\"net.daedalus-development.TestGame\"");
        } break;
      }
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeBuildSettings, "PRODUCT_NAME"),
                               "\"$(TARGET_NAME)\"");
      dt_api->set_object_value(&dt, dt_api->create_object(&dt, nodeBuildSettings, "SRCROOT"),
                               cc_printf("\"%s\"", build_to_base_path));
    }
  }

  unsigned int nodeRootObject = dt_api->create_object(&dt, 0, "rootObject");
  dt_api->set_object_value(&dt, nodeRootObject, project_object_id);
  dt_api->set_object_comment(&dt, nodeRootObject, "Project object");

  export_tree_as_xcode(f, &dt);
}

void xCode_addWorkspaceFolder(FILE* f, const size_t* unique_groups, const size_t parent_group,
                              int folder_depth) {
  const char* prepend_path = "";
  for (int i = 0; i < folder_depth; ++i) {
    prepend_path = cc_printf("%s  ", prepend_path);
  }

  for (unsigned i = 0; i < array_count(unique_groups); ++i) {
    if (cc_data_.groups[unique_groups[i]].parent_group_idx == parent_group) {
      fprintf(f, "%s  <Group\n", prepend_path);
      fprintf(f, "%s    location = \"container:\"\n", prepend_path);
      fprintf(f, "%s    name = \"%s\">\n", prepend_path, cc_data_.groups[unique_groups[i]].name);
      xCode_addWorkspaceFolder(f, unique_groups, unique_groups[i], folder_depth + 1);
      fprintf(f, "%s  </Group>\n", prepend_path);
    }
  }

  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    const cc_project_impl_t* p = cc_data_.projects[i];
    if (p->parent_group_idx == parent_group) {
      fprintf(f, "%s  <FileRef\n", prepend_path);
      fprintf(f, "%s    location = \"group:%s.xcodeproj\">\n", prepend_path, p->name);
      fprintf(f, "%s  </FileRef>\n", prepend_path);
    }
  }
}

void xCodeCreateWorkspaceFile(FILE* f) {
  fprintf(f, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<Workspace\n   version = \"1.0\">\n");

  // Create list of groups needed.
  bool* groups_needed = (bool*)cc_alloc_(array_count(cc_data_.groups) * sizeof(bool));
  memset(groups_needed, 0, array_count(cc_data_.groups) * sizeof(bool));
  for (unsigned i = 0; i < array_count(cc_data_.projects); i++) {
    size_t g = cc_data_.projects[i]->parent_group_idx;
    while (g) {
      groups_needed[g] = true;
      g                = cc_data_.groups[g].parent_group_idx;
    }
  }

  size_t* unique_groups = {0};
  for (unsigned i = 0; i < array_count(cc_data_.groups); i++) {
    if (groups_needed[i]) {
      array_push(unique_groups, i);
    }
  }

  xCode_addWorkspaceFolder(f, unique_groups, 0, 0);

  fprintf(f, "</Workspace>");
}

void xcode_generateInFolder(const char* in_project_output_path) {
  in_project_output_path = make_uri(in_project_output_path);
  if (in_project_output_path[strlen(in_project_output_path) - 1] != '/')
    in_project_output_path = cc_printf("%s/", in_project_output_path);

  char* output_folder = make_uri(cc_printf("%s%s", cc_data_.base_folder, in_project_output_path));

  char* build_to_base_path = make_path_relative(output_folder, cc_data_.base_folder);

  printf("Generating XCode workspace and projects in '%s'...\n", output_folder);

  int result = make_folder(output_folder);
  if (result != 0) {
    fprintf(stderr, "Error %i creating path '%s'\n", result, output_folder);
  }
  (void)chdir(output_folder);

  // Before doing anything, generate a UUID for each projects output file
  xcode_uuid* projectFileReferenceUUIDs = 0;
  projectFileReferenceUUIDs =
      (xcode_uuid*)array_reserve(projectFileReferenceUUIDs, sizeof(*projectFileReferenceUUIDs),
                                 array_count(cc_data_.projects));
  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    array_push(projectFileReferenceUUIDs, xCodeGenerateUUID());
  }

  for (unsigned i = 0; i < array_count(cc_data_.projects); ++i) {
    const cc_project_impl_t* p = cc_data_.projects[i];

    const char* project_path = cc_printf("%s.xcodeproj", p->name);
    (void)make_folder(project_path);

    const char* project_file_path = cc_printf("%s/project.pbxproj", project_path);

    FILE* f = fopen(project_file_path, "wb");

    if (f) {
      xCodeCreateProjectFile(f, p, projectFileReferenceUUIDs, build_to_base_path);
      fclose(f);
    }

    printf("Constructed XCode project '%s'\n", project_path);
  }

  {
    const char* workspace_path = cc_printf("%s.xcworkspace", cc_data_.workspaceLabel);
    (void)make_folder(workspace_path);
    const char* workspace_file_path = cc_printf("%s/contents.xcworkspacedata", workspace_path);
    FILE* f                         = fopen(workspace_file_path, "wb");
    if (f) {
      xCodeCreateWorkspaceFile(f);
      fclose(f);
      printf("Constructed XCode workspace at '%s'\n", workspace_path);
    }
  }
}

// clang-format on

#if defined(_WIN32)
LONG WINAPI ExceptionHandler(PEXCEPTION_POINTERS pExceptionInfo) {
  fprintf(stderr, "Unhandled exception occurred with the following stack:\n");
  PrintStrackFromContext(pExceptionInfo->ContextRecord);

  exit(ERR_CONSTRUCTION);
}
#else
void posix_signal_handler(int sig, siginfo_t* siginfo, void* context) {
  (void)sig;
  (void)siginfo;
  (void)context;
  // posix_print_stack_trace();
  exit(ERR_CONSTRUCTION);
}

static uint8_t alternate_stack[SIGSTKSZ];
void set_signal_handler() {
  /* setup alternate stack */
  {
    stack_t ss = {};
    /* malloc is usually used here, I'm not 100% sure my static allocation
       is valid but it seems to work just fine. */
    ss.ss_sp    = (void*)alternate_stack;
    ss.ss_size  = SIGSTKSZ;
    ss.ss_flags = 0;

    if (sigaltstack(&ss, NULL) != 0) {
      err(1, "sigaltstack");
    }
  }

  /* register our signal handlers */
  {
    struct sigaction sig_action = {};
    sig_action.sa_sigaction     = posix_signal_handler;
    sigemptyset(&sig_action.sa_mask);

  #ifdef __APPLE__
    /* for some reason we backtrace() doesn't work on osx
       when we use an alternate stack */
    sig_action.sa_flags = SA_SIGINFO;
  #else
    sig_action.sa_flags = SA_SIGINFO | SA_ONSTACK;
  #endif

    if (sigaction(SIGSEGV, &sig_action, NULL) != 0) {
      err(1, "sigaction");
    }
    if (sigaction(SIGFPE, &sig_action, NULL) != 0) {
      err(1, "sigaction");
    }
    if (sigaction(SIGINT, &sig_action, NULL) != 0) {
      err(1, "sigaction");
    }
    if (sigaction(SIGILL, &sig_action, NULL) != 0) {
      err(1, "sigaction");
    }
    if (sigaction(SIGTERM, &sig_action, NULL) != 0) {
      err(1, "sigaction");
    }
    if (sigaction(SIGABRT, &sig_action, NULL) != 0) {
      err(1, "sigaction");
    }
  }
}
#endif

int cc_runNewBuild_() {
  const char* new_construct_command =
      cc_printf("%s --generate-projects", cconstruct_internal_binary_name);
#if !defined(_WIN32)
  new_construct_command = cc_printf("./%s", new_construct_command);
#endif

  if (cc_is_verbose) {
    new_construct_command = cc_printf("%s --verbose", new_construct_command);
  }
  LOG_VERBOSE("Executing new binary: '%s'\n", new_construct_command);
  int result = system(new_construct_command);
#if !defined(_WIN32)
  // The spec for system() doesn't require it to return the error from the executed command.
  // On MacOS it doesn't, so query the actual error here.
  result = WEXITSTATUS(result);
#endif
  return result;
}

// Generates a project to build cconstruct itself. Use this when cconstruct crashes and you can't
// figure out why.
void generate_cc_project(cconstruct_t cc, const char* cc_config_path) {
  printf("Generating project for CConstruct config\n");
  cc_architecture_t arch = cc.architecture.create(EArchitectureX64);
  cc_platform_t platform = cc.platform.create(EPlatformDesktop);
  cc.workspace.addArchitecture(arch);
  cc.workspace.addPlatform(platform);

  cc_configuration_t configuration_debug = cc.configuration.create("Debug");
  cc.workspace.addConfiguration(configuration_debug);

  cc_project_t p = cc.project.create("cconstruct", CCProjectTypeConsoleApplication, NULL);

  const char* files[] = {strip_path(make_uri(cc_config_path))};
  cc.project.addFiles(p, countof(files), files, NULL);

  cc.workspace.setLabel("cconstruct");

  cc_default_generator("build");
}

static void cc_print_statistics_(void) {
  if (cc_total_bytes_allocated_) {
    printf("======================\nCConstruct used %u KiB of memory\n",
           (unsigned int)(cc_total_bytes_allocated_ / 1024));
  }
}

cconstruct_t cc_init(const char* in_absolute_config_file_path, int argc, const char* const* argv) {
#if defined(_WIN32)
  (void)DeleteFile(cconstruct_old_binary_name);
  SetUnhandledExceptionFilter(ExceptionHandler);
#else
  set_signal_handler();
#endif

  bool is_path_absolute =
      (in_absolute_config_file_path[0] == '/') || (in_absolute_config_file_path[1] == ':');
  if (!is_path_absolute) {
    fprintf(stderr,
            "Error: config path passed to cc_init('%s', ...) is not an absolute path. If you are "
            "using "
            "__FILE__ check your compiler settings.\n",
            in_absolute_config_file_path);
#if defined(_MSC_VER)
    LOG_ERROR_AND_QUIT(
        ERR_CONFIGURATION,
        "When using the Microsoft compiler cl.exe add the /FC flag to ensure __FILE__ emits "
        "an absolute path.\n");
#elif defined(__APPLE__)
    LOG_ERROR_AND_QUIT(
        ERR_CONFIGURATION,
        "You can make the file you are compiling absolute by adding $PWD/ in front of it.\n");
#endif
  }

  if (cc_data_.is_inited) {
    LOG_ERROR_AND_QUIT(ERR_CONFIGURATION,
                       "Error: calling cc_init() multiple times. Don't do this.\n");
  }
  cc_data_.is_inited = true;

  for (int i = 0; i < argc; i++) {
    if (strcmp(argv[i], "--verbose") == 0) {
      cc_is_verbose = true;
    }
    if (strcmp(argv[i], "--generate-projects") == 0) {
      cc_only_generate = true;
    }
    if (strcmp(argv[i], "--generate-cconstruct-project") == 0) {
      cc_only_generate       = true;
      cc_generate_cc_project = true;
    }
  }

  // If not only generating, then a new binary is built, that is run, and only then do we attempt
  // to clean up this existing version. This binary doesn't do any construction of projects.
  if (!cc_only_generate) {
    printf("Rebuilding CConstruct ...");
    cc_recompile_binary_(in_absolute_config_file_path);
    printf(" done\n");
    int result = cc_runNewBuild_();
    if (result == 0) {
      cc_activateNewBuild_();
    }
    exit(result);
  }

  if (atexit(cc_print_statistics_) != 0) {
    // Oh well, failed to install that, but don't care much as it doesn't affect operation of
    // CConstruct.
  }

  // First group is no group
  cc_group_impl_t null_group = {0};
  array_push(cc_data_.groups, null_group);

  cc_data_.base_folder = folder_path_only(in_absolute_config_file_path);

  // Keep this as a local, so that users are forced to call cc_init to get an instance the struct.
  cconstruct_t out = {
      {&cc_configuration_create},
      {&cc_architecture_create},
      {&cc_platform_create},
      {&cc_group_create},
      {&cc_state_create, &cc_state_reset, &cc_state_addIncludeFolder,
       &cc_state_addPreprocessorDefine, &cc_state_addCompilerFlag, &cc_state_addLinkerFlag,
       &cc_state_linkExternalLibrary, &cc_state_setWarningLevel,
       &cc_state_disableWarningsAsErrors},
      {
          &cc_project_create_,
          &addFilesToProject,
          &addFilesFromFolderToProject,
          &cc_project_addFileWithCustomCommand,
          &cc_project_addInputProject,  // Visual Studio terminology, add a reference
          &cc_project_addDependency,    // Visual Studio terminology, add a dependency
          &cc_project_setFlags_,
          &cc_project_addPreBuildAction,
          &cc_project_addPostBuildAction,
          &cc_project_setOutputFolder,
      },
      {&setWorkspaceLabel, &addConfiguration, &addArchitecture, &addPlatform}};

  if (cc_generate_cc_project) {
    generate_cc_project(out, in_absolute_config_file_path);
    exit(0);
  }
  return out;
}


// For ease of use set a default CConstruct generator for each OS
#if defined(_MSC_VER)
void (*cc_default_generator)(const char* workspace_folder) = vs2019_generateInFolder;
#else
void (*cc_default_generator)(const char* workspace_folder) = xcode_generateInFolder;
#endif

#endif  // CC_CONSTRUCT_H
