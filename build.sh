#!/bin/bash
set -e
set -x

COMPILE_CCONSTRUCT_COMMAND='clang++ -std=c++11 -x c++'
COMPILE_CCONSTRUCT_DEBUG_COMMAND='clang++ -g -std=c++11 -x c++'
rm -rf build
$COMPILE_CCONSTRUCT_COMMAND $PWD/config.cc -o cconstruct
./cconstruct --generate-projects

