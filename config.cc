#include <string>
#include <vector>

#include "tools/cconstruct.h"

cc_state_t common_flags;
struct {
  cc_project_t core;
  cc_project_t draw2d;
  cc_project_t ui;
  cc_project_t telemetry;
} g_projects = {0};

cc_architecture_t arch32 = {0};
cc_architecture_t arch64 = {0};

void addModuleCore(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("daedalus_core", CCProjectTypeStaticLibrary, group_modules);

  const char* core_files[] = {
      "api_registry.h", "api_registry.c",
      "application.h",  "application.c",
      "containers.h",   "containers.c",
      "dd_assert.h",    "dd_string.h",
      "dd_string.c",    "ddf.h",
      "ddf.c",          "error.h",
      "filesystem.h",   "filesystem.c",
      "hid.h",          "http.h",
      "http.c",         "log.h",
      "log_remote.h",   "log_structs.inl",
      "log.c",          "log_process.c",
      "dd_math.h",      "memory.h",
      "memory.c",       "memory.inl",
      "network.h",      "network.c",
      "platform.h",     "plugins.h",
      "plugins.c",      "dll.h",
      "dll.c",          "thread.h",
      "thread.c",       "thread_mutex.c",
      "test.h",         "timer.h",
      "timer.c",        "types.h",
      "utils.h",        "utils.c",
      "profiler.c",     "profiler.h",
      "profiler.inl",   "profiler_report_chrome.c",
  };
  const char* thirdparty_files[] = {
      "dlmalloc.h",
      "dlmalloc.c",
  };

  cc.project.addFilesFromFolder(p, "source/modules/core/", countof(core_files), core_files, NULL);
  cc.project.addFilesFromFolder(p, "source/modules/core/", countof(thirdparty_files),
                                thirdparty_files, NULL);

  const char* apple_files[] = {
      "http_objc.m",
      "os.m",
  };
#if defined(__APPLE__)
  cc.project.addFilesFromFolder(p, "source/modules/core/", countof(apple_files), apple_files,
                                NULL);
#endif

  cc.project.setFlags(p, common_flags, NULL, NULL);

  g_projects.core = p;
}

void addModuleDraw2D(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("draw2d", CCProjectTypeDynamicLibrary, group_modules);

  const char* files[] = {
      "draw_2d.h",
      "draw_2d.c",
  };

  cc.project.addFilesFromFolder(p, "source/modules/draw2d/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc.project.addInputProject(p, g_projects.core);

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  g_projects.draw2d = p;
}

void addModuleUI(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("ui", CCProjectTypeDynamicLibrary, group_modules);

  const char* files[] = {
      "ui.h",
      "ui.c",
  };

  cc.project.addFilesFromFolder(p, "source/modules/ui/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");
#if defined(__APPLE__)
  cc.state.linkExternalLibrary(flags, "System/Library/Frameworks/AppKit.framework");
#endif

  cc.project.addInputProject(p, g_projects.core);

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  g_projects.ui = p;
}

cc_project_t addModulesGraphicsAPI(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p =
      cc.project.create("daedalus_graphics", CCProjectTypeStaticLibrary, group_modules);

  const char* files[] = {
      "graphics.h",
      "graphics.c",
  };
  cc.project.addFilesFromFolder(p, "source/modules/graphics_api/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  return p;
}

cc_project_t addModulesTelemetry(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p =
      cc.project.create("daedalus_telemetry", CCProjectTypeStaticLibrary, group_modules);

  const char* files[] = {
      "telemetry_client.h", "telemetry_types.inl", "telemetry_server.h",
      "telemetry_client.c", "telemetry_server.c",
  };
  cc.project.addFilesFromFolder(p, "source/modules/telemetry/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  g_projects.telemetry = p;
  return p;
}

cc_project_t addModuleOSAPI(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("daedalus_os", CCProjectTypeDynamicLibrary, group_modules);

  const char* files[] = {"os.c", "os.h"};
  cc.project.addFilesFromFolder(p, "source/modules/os/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  cc.project.addInputProject(p, g_projects.core);

  return p;
}

cc_project_t addModulesVulkanAPI(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("daedalus_vulkan", CCProjectTypeStaticLibrary, group_modules);

  const char* files[] = {
      "vulkan_api.c",
      "scene.h",
      "data.c",
      "demo.h",
      "vulkan_types.inl",
      "vk_material.c",
      "vk_window_surface.h",
      "vk_window_surface.c",
      "default_vulkan_window.c",
  };
  cc.project.addFilesFromFolder(p, "source/modules/vulkan_api/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.reset(flags);
  cc.state.addIncludeFolder(flags, "source/modules");
  cc.state.addIncludeFolder(flags, "$(VULKAN_SDK)/Include/");
  cc.state.addPreprocessorDefine(flags, "VK_USE_PLATFORM_WIN32_KHR");
  cc.state.addPreprocessorDefine(flags, "VK_PROTOTYPES");

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  return p;
}

void addModulesVulkanTest(cconstruct_t cc, cc_project_t vulkan_api_project,
                          cc_project_t graphics_api_project, const cc_group_t group_tests) {
  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc_project_t p = cc.project.create("vulkan_test", CCProjectTypeConsoleApplication, group_tests);

  const char* files[] = {
      "graphics_test.c",
      "graphics_test_draw_types.c",
      "graphics_test_helpers.c",
      "graphics_test.inl",
  };

  cc.project.addFilesFromFolder(p, "source/modules/graphics_api/test/", countof(files), files,
                                NULL);
  const cc_group_t group_glsl = cc.group.create("glsl", NULL);

  const char* shader_variants[] = {
      "vertex_color",
      "vertex_color_instanced",
      "vertex_color_push_constants",
      "textured",
      "textured_push_constants",
      "textured_instanced_array_of_textures",
      "textured_instanced_texture_array",
      "textured_array_of_textures_dynamic_indexed",
      "vertex_color_interleaved",
  };
  for (int i = 0; i < countof(shader_variants); i++) {
    const char* vs_src =
        cc_printf("source/modules/vulkan_api/test/vulkan/%s.vs.glsl", shader_variants[i]);
    const char* vs_dst =
        cc_printf("source/modules/vulkan_api/test/vulkan/%s.vs.spv", shader_variants[i]);
    const char* fs_src =
        cc_printf("source/modules/vulkan_api/test/vulkan/%s.fs.glsl", shader_variants[i]);
    const char* fs_dst =
        cc_printf("source/modules/vulkan_api/test/vulkan/%s.fs.spv", shader_variants[i]);
    cc.project.addFileWithCustomCommand(
        p, vs_src, group_glsl, "glslc -fshader-stage=vertex ${input} -o ${output}", vs_dst);
    cc.project.addFileWithCustomCommand(
        p, fs_src, group_glsl, "glslc -fshader-stage=fragment ${input} -o ${output}", fs_dst);
  }

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);
  cc.project.addInputProject(p, g_projects.core);
  cc.project.addInputProject(p, g_projects.ui);
  cc.project.addInputProject(p, graphics_api_project);
  cc.project.addInputProject(p, vulkan_api_project);
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib/vulkan-1.lib");
    cc.project.setFlags(p, s, arch64, NULL);
  }
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib32/vulkan-1.lib");
    cc.project.setFlags(p, s, arch32, NULL);
  }

  cc.project.addPostBuildAction(p, "./${platform}/${configuration}/vulkan_test");
}

void addModulePLYAPI(cconstruct_t cc, const cc_group_t group_formats) {
  cc_project_t p =
      cc.project.create("daedalus_plyreader", CCProjectTypeDynamicLibrary, group_formats);

  const char* files[] = {"ply_reader.c", "ply_reader.h"};
  cc.project.addFilesFromFolder(p, "source/modules/formats/ply_reader/", countof(files), files,
                                NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc.project.addInputProject(p, g_projects.core);

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);
}

void addModuleMetisApi(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("daedalus_metis", CCProjectTypeDynamicLibrary, group_modules);

  const char* files[] = {"dd_metis.c", "dd_metis.h"};
  cc.project.addFilesFromFolder(p, "source/modules/metis/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "thirdparty/metis/include");
  cc.state.addIncludeFolder(flags, "source/modules");
  cc.state.linkExternalLibrary(flags, "thirdparty/metis/lib/vs2019/metis.lib");

  cc.project.addInputProject(p, g_projects.core);

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);
}

cc_project_t addModulesMetalAPI(cconstruct_t cc, const cc_group_t group_modules) {
  cc_project_t p = cc.project.create("daedalus_metal", CCProjectTypeStaticLibrary, group_modules);

  const char* files[] = {
      "metal_api.m",
      "metal_types.inl",
  };
  cc.project.addFilesFromFolder(p, "source/modules/metal_api/", countof(files), files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.reset(flags);
  cc.state.addIncludeFolder(flags, "source/modules");

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  return p;
}

void addModulesMetalTest(cconstruct_t cc, cc_project_t metal_api_project,
                         cc_project_t graphics_api_project, const cc_group_t group_tests) {
  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc_project_t p = cc.project.create("metal_test", CCProjectTypeConsoleApplication, group_tests);

  const char* files[] = {
      "graphics_test.c",
      "graphics_test_draw_types.c",
      "graphics_test_helpers.c",
      "graphics_test.inl",
  };
  cc.project.addFilesFromFolder(p, "source/modules/graphics_api/test/", countof(files), files,
                                NULL);
  const cc_group_t group_metal = cc.group.create("metal", NULL);
  const char* metal_files[]    = {
      "vertex_color.metal",
      "vertex_color_instanced.metal",
      "vertex_color_push_constants.metal",
      "textured.metal",
      "textured_push_constants.metal",
      "textured_instanced_array_of_textures.metal",
      "textured_array_of_textures_dynamic_indexed.metal",
      "textured_instanced_texture_array.metal",
      "vertex_color_interleaved.metal",
  };
  cc.project.addFilesFromFolder(p, "source/modules/metal_api/test/", countof(metal_files),
                                metal_files, group_metal);

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);
  cc.project.addInputProject(p, g_projects.core);
  cc.project.addInputProject(p, g_projects.ui);
  cc.project.addInputProject(p, graphics_api_project);
  cc.project.addInputProject(p, metal_api_project);

  cc_state_t s = cc.state.create();
  cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Metal.framework");
  cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Cocoa.framework");
  cc.project.setFlags(p, s, NULL, NULL);

  cc.project.addPostBuildAction(p, "./${platform}/${configuration}/metal_test");
}

cc_project_t addModuleCoreTest(cconstruct_t cc, const cc_group_t group_tests) {
  cc_project_t project_core_test =
      cc.project.create("daedalus_core_test", CCProjectTypeConsoleApplication, group_tests);

  const char* files[] = {
      "main_test.c",  "containers_test.c", "ddf_test.c",    "log_test.c",
      "math_test.c",  "memory_test.c",     "plugin_test.c", "strings_test.c",
      "timer_test.c", "network_test.c",    "types_test.c",  "profiler_test.c",
  };

  cc.project.addFilesFromFolder(project_core_test, "source/modules/core/test/", countof(files),
                                files, NULL);

  cc_state_t flags = cc.state.create();
  cc.state.reset(flags);
  cc.state.addPreprocessorDefine(flags, "_CRT_SECURE_NO_WARNINGS");
  cc.state.addIncludeFolder(flags, "source/modules");

#if defined(__APPLE__)
  cc.state.linkExternalLibrary(flags, "System/Library/Frameworks/AppKit.framework");
#endif

  cc.project.setFlags(project_core_test, common_flags, NULL, NULL);
  cc.project.setFlags(project_core_test, flags, NULL, NULL);

  cc.project.addPostBuildAction(project_core_test,
                                "./${platform}/${configuration}/daedalus_core_test");

  {
    cc_project_t ptest =
        cc.project.create("test_plugin1", CCProjectTypeDynamicLibrary, group_tests);
    const char* files[] = {"plugin_test_lib.c"};
    cc.project.addFilesFromFolder(ptest, "source/modules/core/test/", countof(files), files, NULL);
    cc_state_t flags = cc.state.create();
    cc.state.reset(flags);
    cc.state.addIncludeFolder(flags, "source/modules");
    cc.state.addPreprocessorDefine(flags, "TEST_PLUGIN_VALUE=5");
    cc.project.setFlags(ptest, common_flags, NULL, NULL);
    cc.project.setFlags(ptest, flags, NULL, NULL);
#if defined(__APPLE__)
    cc.project.addPostBuildAction(ptest,
                                  "mv ./${platform}/${configuration}/libtest_plugin1.dylib "
                                  "./${platform}/${configuration}/libtest_plugin1.tmp");
#else
    cc.project.addPostBuildAction(ptest,
                                  "move ./${platform}/${configuration}/test_plugin1.dll "
                                  "./${platform}/${configuration}/test_plugin1.tmp");
#endif
    cc.project.setBuildOrder(ptest, project_core_test);
  }
  {
    cc_project_t ptest =
        cc.project.create("test_plugin2", CCProjectTypeDynamicLibrary, group_tests);
    const char* files[] = {"plugin_test_lib.c"};
    cc.project.addFilesFromFolder(ptest, "source/modules/core/test/", countof(files), files, NULL);
    cc_state_t flags = cc.state.create();
    cc.state.reset(flags);
    cc.state.addIncludeFolder(flags, "source/modules");
    cc.state.addPreprocessorDefine(flags, "TEST_PLUGIN_VALUE=16");
    cc.project.setFlags(ptest, common_flags, NULL, NULL);
    cc.project.setFlags(ptest, flags, NULL, NULL);
#if defined(__APPLE__)
    cc.project.addPostBuildAction(ptest,
                                  "mv ./${platform}/${configuration}/libtest_plugin2.dylib "
                                  "./${platform}/${configuration}/libtest_plugin2.tmp");
#else
    cc.project.addPostBuildAction(ptest,
                                  "move ./${platform}/${configuration}/test_plugin2.dll "
                                  "./${platform}/${configuration}/test_plugin2.tmp");
#endif
    cc.project.setBuildOrder(ptest, project_core_test);
  }

  return project_core_test;
}

void addModuleOsTest(cconstruct_t cc, const cc_group_t group_tests) {
  cc_project_t project_os_test =
      cc.project.create("daedalus_os_test", CCProjectTypeConsoleApplication, group_tests);

  const char* files[] = {"os_test.c"};

  cc.project.addFilesFromFolder(project_os_test, "source/modules/os/test/", countof(files), files,
                                NULL);

  cc_state_t flags = cc.state.create();
  cc.state.reset(flags);
  cc.state.addPreprocessorDefine(flags, "_CRT_SECURE_NO_WARNINGS");
  cc.state.addIncludeFolder(flags, "source/modules");

#if defined(__APPLE__)
  cc.state.linkExternalLibrary(flags, "System/Library/Frameworks/AppKit.framework");
#endif

  cc.project.setFlags(project_os_test, common_flags, NULL, NULL);
  cc.project.setFlags(project_os_test, flags, NULL, NULL);

  cc.project.addInputProject(project_os_test, g_projects.core);

  cc.project.addPostBuildAction(project_os_test,
                                "./${platform}/${configuration}/daedalus_os_test");
}

void addExamplesLogging(cconstruct_t cc, const cc_group_t group_examples) {
  cc_state_t flags = cc.state.create();
  cc.state.reset(flags);
  cc.state.addIncludeFolder(flags, "source/modules");

  {
    cc_project_t p =
        cc.project.create("logging_ignore", CCProjectTypeConsoleApplication, group_examples);

    const char* files[] = {"logging_main.c", "implementations/log_ignore.c"};
    cc.project.addFilesFromFolder(p, "source/examples/logging/", countof(files), files, NULL);
    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
  }
  {
    cc_project_t p =
        cc.project.create("logging_stdout", CCProjectTypeConsoleApplication, group_examples);

    const char* files[] = {"logging_main.c", "implementations/log_stdout.c"};
    cc.project.addFilesFromFolder(p, "source/examples/logging/", countof(files), files, NULL);
    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
  }
  {
    cc_project_t p =
        cc.project.create("logging_file", CCProjectTypeConsoleApplication, group_examples);

    const char* files[] = {"logging_main.c", "implementations/log_file.c"};
    cc.project.addFilesFromFolder(p, "source/examples/logging/", countof(files), files, NULL);
    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
  }
  {
    cc_project_t p =
        cc.project.create("logging_remote_print", CCProjectTypeConsoleApplication, group_examples);

    const char* files[] = {"logging_main.c", "implementations/log_remote_print.c"};
    cc.project.addFilesFromFolder(p, "source/examples/logging/", countof(files), files, NULL);
    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
  }
  {
    cc_project_t p = cc.project.create("logging_remote_combine_print",
                                       CCProjectTypeConsoleApplication, group_examples);

    const char* files[] = {"logging_main.c", "implementations/log_remote_combine_print.c"};
    cc.project.addFilesFromFolder(p, "source/examples/logging/", countof(files), files, NULL);
    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
  }
  {
    cc_project_t p = cc.project.create("logging_remote_process", CCProjectTypeConsoleApplication,
                                       group_examples);

    const char* files[] = {"logging_main.c", "implementations/log_remote_process.c"};
    cc.project.addFilesFromFolder(p, "source/examples/logging/", countof(files), files, NULL);
    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
  }
}

void addExampleUI(cconstruct_t cc, cc_project_t graphics_impl_project,
                  cc_project_t graphics_api_project, const cc_group_t group_examples) {
  cc_project_t p =
      cc.project.create("ui_example", CCProjectTypeWindowedApplication, group_examples);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");
  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  cc.project.addInputProject(p, g_projects.core);
  cc.project.addInputProject(p, graphics_api_project);
  cc.project.addInputProject(p, graphics_impl_project);
  cc.project.addInputProject(p, g_projects.telemetry);

  cc_group_t g = cc.group.create("Source", NULL);
  {
    const char* files[] = {
        "ui_main.c",
        "ui_sample.c",
    };
    cc.project.addFilesFromFolder(p, "source/examples/ui/", countof(files), files, g);
  }

#if defined(__APPLE__)
  const cc_group_t group_macos = cc.group.create("macos", g);
  {
    const char* files[] = {
        "AppDelegate.h", "AppDelegate.m", "GameViewController.h", "GameViewController.m",
        "Renderer.h",    "Renderer.m",    "UIShaders.metal",      "Info.plist",
    };
    cc.project.addFilesFromFolder(p, "source/examples/ui/macos/", countof(files), files,
                                  group_macos);
  }
  {
    const char* files[] = {"Main.storyboard"};
    cc.project.addFilesFromFolder(p, "source/examples/ui/macos/Base.lproj/", countof(files), files,
                                  group_macos);
  }

  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Metal.framework");
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/MetalKit.framework");
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/ModelIO.framework");
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Cocoa.framework");
    cc.project.setFlags(p, s, NULL, NULL);
  }

#elif defined(_WIN32)

  const cc_group_t group_glsl = cc.group.create("glsl", NULL);
  cc.project.addFileWithCustomCommand(p, "source/examples/ui/vulkan/ui.vs.glsl", group_glsl,
                                      "glslc -fshader-stage=vertex ${input} -o ${output}",
                                      "source/examples/ui/vulkan/ui.vs.spv");
  cc.project.addFileWithCustomCommand(p, "source/examples/ui/vulkan/ui.fs.glsl", group_glsl,
                                      "glslc -fshader-stage=fragment ${input} -o ${output}",
                                      "source/examples/ui/vulkan/ui.fs.spv");

  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib/vulkan-1.lib");
    cc.project.setFlags(p, s, arch64, NULL);
  }
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib32/vulkan-1.lib");
    cc.project.setFlags(p, s, arch32, NULL);
  }

#endif
}

void addExamplesVulkan(cconstruct_t cc, cc_project_t vulkan_api_project,
                       cc_project_t graphics_api_project, const cc_group_t group_examples) {
  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  {
    cc_project_t p = cc.project.create("vulkan", CCProjectTypeWindowedApplication, group_examples);

    const char* files[] = {"vulkan_main.c"};
    cc.project.addFilesFromFolder(p, "source/examples/vulkan/", countof(files), files, NULL);
    const cc_group_t group_glsl = cc.group.create("glsl", NULL);
    cc.project.addFileWithCustomCommand(
        p, "source/examples/vulkan/data/cube.vert.glsl", group_glsl,
        "glslc -fshader-stage=vertex source/examples/vulkan/data/cube.vert.glsl -o "
        "source/examples/vulkan/data/vert.inc",
        "source/examples/vulkan/data/vert.inc");
    cc.project.addFileWithCustomCommand(
        p, "source/examples/vulkan/data/cube.frag.glsl", group_glsl,
        "glslc -fshader-stage=fragment source/examples/vulkan/data/cube.frag.glsl "
        "-o source/examples/vulkan/data/frag.inc",
        "source/examples/vulkan/data/frag.inc");

    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
    cc.project.addInputProject(p, graphics_api_project);
    cc.project.addInputProject(p, vulkan_api_project);
    {
      cc_state_t s = cc.state.create();
      cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib/vulkan-1.lib");
      cc.project.setFlags(p, s, arch64, NULL);
    }
    {
      cc_state_t s = cc.state.create();
      cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib32/vulkan-1.lib");
      cc.project.setFlags(p, s, arch32, NULL);
    }
  }
}

void addExamplesMetis(cconstruct_t cc, cc_project_t vulkan_api_project,
                      cc_project_t graphics_api_project, const cc_group_t group_examples) {
  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  {
    cc_project_t p = cc.project.create("metis", CCProjectTypeWindowedApplication, group_examples);

    const char* files[] = {"metis_main.c"};
    cc.project.addFilesFromFolder(p, "source/examples/metis/", countof(files), files, NULL);
    const cc_group_t group_glsl = cc.group.create("glsl", NULL);
    cc.project.addFileWithCustomCommand(
        p, "source/examples/metis/data/metis_cluster.vert.glsl", group_glsl,
        "glslc -fshader-stage=vertex source/examples/metis/data/metis_cluster.vert.glsl -o "
        "source/examples/metis/data/metis_cluster_vert.inc",
        "source/examples/metis/data/metis_cluster_vert.inc");
    cc.project.addFileWithCustomCommand(
        p, "source/examples/metis/data/metis_cluster.frag.glsl", group_glsl,
        "glslc -fshader-stage=fragment source/examples/metis/data/metis_cluster.frag.glsl "
        "-o source/examples/metis/data/metis_cluster_frag.inc",
        "source/examples/metis/data/metis_cluster_frag.inc");
    cc.project.addFileWithCustomCommand(
        p, "source/examples/metis/data/cluster_cull.compute.glsl", group_glsl,
        "glslc -fshader-stage=compute source/examples/metis/data/cluster_cull.compute.glsl "
        "-o source/examples/metis/data/cluster_cull_compute.inc",
        "source/examples/metis/data/cluster_cull_compute.inc");
    cc.project.addFileWithCustomCommand(
        p, "source/examples/metis/data/regular.vert.glsl", group_glsl,
        "glslc -fshader-stage=vertex source/examples/metis/data/regular.vert.glsl -o "
        "source/examples/metis/data/regular_vert.inc",
        "source/examples/metis/data/regular_vert.inc");
    cc.project.addFileWithCustomCommand(
        p, "source/examples/metis/data/regular.frag.glsl", group_glsl,
        "glslc -fshader-stage=fragment source/examples/metis/data/regular.frag.glsl "
        "-o source/examples/metis/data/regular_frag.inc",
        "source/examples/metis/data/regular_frag.inc");

    cc.project.setFlags(p, common_flags, NULL, NULL);
    cc.project.setFlags(p, flags, NULL, NULL);
    cc.project.addInputProject(p, g_projects.core);
    cc.project.addInputProject(p, g_projects.draw2d);
    cc.project.addInputProject(p, g_projects.ui);
    cc.project.addInputProject(p, graphics_api_project);
    cc.project.addInputProject(p, vulkan_api_project);
    {
      cc_state_t s = cc.state.create();
      cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib/vulkan-1.lib");
      cc.project.setFlags(p, s, arch64, NULL);
    }
    {
      cc_state_t s = cc.state.create();
      cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib32/vulkan-1.lib");
      cc.project.setFlags(p, s, arch32, NULL);
    }
  }
}

void addExamplesMetal(cconstruct_t cc, cc_project_t metal_api_project,
                      cc_project_t graphics_api_project, const cc_group_t group_examples) {
  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");

  cc_project_t p =
      cc.project.create("metal_render_to_file", CCProjectTypeConsoleApplication, group_examples);

  const char* files[] = {
      "main.m", "quads.h", "Shaders.metal", "stb_image_write.h", "stb_image.h",
  };
  cc.project.addFilesFromFolder(p, "source/examples/metal_render_to_file/", countof(files), files,
                                NULL);

  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);
  cc.project.addInputProject(p, g_projects.core);
  cc.project.addInputProject(p, g_projects.draw2d);
  cc.project.addInputProject(p, g_projects.ui);
  cc.project.addInputProject(p, graphics_api_project);
  cc.project.addInputProject(p, metal_api_project);
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Metal.framework");
    cc.project.setFlags(p, s, NULL, NULL);
  }
}

void addToolsFixedFont(cconstruct_t cc, const cc_group_t group_tools) {
  cc_project_t p = cc.project.create("fixed_font", CCProjectTypeConsoleApplication, group_tools);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");
  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  {
    const char* files[] = {"fixedfont_main.c"};
    cc.project.addFilesFromFolder(p, "source/tools/fixed_font/", countof(files), files, NULL);
  }

  cc.project.addInputProject(p, g_projects.core);
}

cc_project_t addToolsDaedalusHub(cconstruct_t cc, cc_project_t graphics_impl_project,
                                 cc_project_t graphics_api_project, cc_project_t telemetry_project,
                                 const cc_group_t group_tools) {
  cc_project_t p =
      cc.project.create("daedalus_hub", CCProjectTypeWindowedApplication, group_tools);

  cc_state_t flags = cc.state.create();
  cc.state.addIncludeFolder(flags, "source/modules");
  cc.project.setFlags(p, common_flags, NULL, NULL);
  cc.project.setFlags(p, flags, NULL, NULL);

  cc.project.addInputProject(p, g_projects.core);
  cc.project.addInputProject(p, g_projects.draw2d);
  cc.project.addInputProject(p, g_projects.ui);
  cc.project.addInputProject(p, graphics_api_project);
  cc.project.addInputProject(p, graphics_impl_project);
  cc.project.addInputProject(p, telemetry_project);

  cc_group_t g = cc.group.create("Source", NULL);
  {
    const char* files[] = {
        "hub_main.c",    "hub.inl",           "hub_ui.c",
        "hub_ui_logs.c", "hub_ui_profiler.c", "hub_ui_memory.c",
    };
    cc.project.addFilesFromFolder(p, "source/tools/hub/", countof(files), files, g);
  }

#if defined(__APPLE__)
  const cc_group_t group_macos = cc.group.create("macos", g);
  {
    const char* files[] = {
        "AppDelegate.h", "AppDelegate.m", "GameViewController.h", "GameViewController.m",
        "Renderer.h",    "Renderer.m",    "UIShaders.metal",      "Info.plist",
    };
    cc.project.addFilesFromFolder(p, "source/examples/ui/macos/", countof(files), files,
                                  group_macos);
  }
  {
    const char* files[] = {"Main.storyboard"};
    cc.project.addFilesFromFolder(p, "source/examples/ui/macos/Base.lproj/", countof(files), files,
                                  group_macos);
  }
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Metal.framework");
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/MetalKit.framework");
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/ModelIO.framework");
    cc.state.linkExternalLibrary(s, "System/Library/Frameworks/Cocoa.framework");
    cc.project.setFlags(p, s, NULL, NULL);
  }

#elif defined(_WIN32)

  const cc_group_t group_glsl = cc.group.create("glsl", NULL);
  cc.project.addFileWithCustomCommand(p, "source/examples/ui/vulkan/ui.vs.glsl", group_glsl,
                                      "glslc -fshader-stage=vertex ${input} -o ${output}",
                                      "source/examples/ui/vulkan/ui.vs.spv");
  cc.project.addFileWithCustomCommand(p, "source/examples/ui/vulkan/ui.fs.glsl", group_glsl,
                                      "glslc -fshader-stage=fragment ${input} -o ${output}",
                                      "source/examples/ui/vulkan/ui.fs.spv");
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib/vulkan-1.lib");
    cc.project.setFlags(p, s, arch64, NULL);
  }
  {
    cc_state_t s = cc.state.create();
    cc.state.linkExternalLibrary(s, "$(VULKAN_SDK)/Lib32/vulkan-1.lib");
    cc.project.setFlags(p, s, arch32, NULL);
  }

#endif

  return p;
}

int main(int argc, const char** argv) {
  cconstruct_t cc = cc_init(__FILE__, argc, argv);

  cc.workspace.setLabel("daedalus");

  common_flags = cc.state.create();
  cc.state.reset(common_flags);
#ifdef _WIN32
  cc.state.addPreprocessorDefine(common_flags, "_CRT_SECURE_NO_WARNINGS");

  // TODO need to set multibyte program rather than unicode

  // Visual Studio doesn't have 100% support of C99, where the following is allowed:
  //    mystruct d = { .member = func() };
  // so disable the warning about that.
  cc.state.addCompilerFlag(common_flags, "/wd4204");

  // Ignore unknown pragmas (likely for other platforms)
  cc.state.addCompilerFlag(common_flags, "/wd4068");

  // Allow mystruct = { .member = nonconstant };  // Ignore unknown pragmas (likely for other
  // platforms)
  cc.state.addCompilerFlag(common_flags, "/wd4068");

  cc.state.addCompilerFlag(common_flags, "/wd4221");

  // Newer compilers warning on preprocessor defined which causes warnings inside Windows.h, so
  // disable it.
  cc.state.addCompilerFlag(common_flags, "/wd5105");

  cc.state.addCompilerFlag(common_flags, "/std:c11");

  // ref:
  // https://stackoverflow.com/questions/5312913/visual-c-2010-express-c2099-error-for-structures-initialized-with-constants-in
  // The logging system uses this
  cc.state.addCompilerFlag(common_flags, "/Zi");
  // Set in ClCompile <DebugInformationFormat>ProgramDatabase</DebugInformationFormat>
#else
  cc.state.addCompilerFlag(common_flags, "MACOSX_DEPLOYMENT_TARGET=10.15");
  cc.state.setWarningLevel(common_flags, EStateWarningLevelMedium);
#endif

  const cc_group_t group_modules  = cc.group.create("modules", NULL);
  const cc_group_t group_formats  = cc.group.create("modules", group_modules);
  const cc_group_t group_tests    = cc.group.create("tests", NULL);
  const cc_group_t group_examples = cc.group.create("examples", NULL);
  const cc_group_t group_tools    = cc.group.create("tools", NULL);

  auto platform = cc.platform.create(EPlatformDesktop);
  cc.workspace.addPlatform(platform);
  arch64 = cc.architecture.create(EArchitectureX64);
  cc.workspace.addArchitecture(arch64);

  // The automated registering of test cases doesn't work for VS2019 64-bit, so on that platform
  // build 32-bit also.
#ifdef _WIN32
  arch32 = cc.architecture.create(EArchitectureX86);
  cc.workspace.addArchitecture(arch32);
#endif

  auto configuration_debug   = cc.configuration.create("Debug");
  auto configuration_release = cc.configuration.create("Release");

  cc.workspace.addConfiguration(configuration_debug);
  cc.workspace.addConfiguration(configuration_release);

  addModuleCore(cc, group_modules);
  addModuleDraw2D(cc, group_modules);
  addModuleUI(cc, group_modules);
  addModuleOSAPI(cc, group_modules);
  addModuleMetisApi(cc, group_modules);
  cc_project_t graphics_project = addModulesGraphicsAPI(cc, group_modules);
  cc_project_t test_project     = addModuleCoreTest(cc, group_tests);
  addModuleOsTest(cc, group_tests);
  cc_project_t telemetry_project = addModulesTelemetry(cc, group_modules);
  addExamplesLogging(cc, group_examples);
  cc.project.addInputProject(test_project, g_projects.core);

  addModulePLYAPI(cc, group_formats);

#if defined(_WIN32)
  cc_project_t vulkan_project = addModulesVulkanAPI(cc, group_modules);
  cc.project.addInputProject(vulkan_project, g_projects.core);
  addModulesVulkanTest(cc, vulkan_project, graphics_project, group_tests);

  addExampleUI(cc, vulkan_project, graphics_project, group_examples);

  addExamplesVulkan(cc, vulkan_project, graphics_project, group_examples);
  addExamplesMetis(cc, vulkan_project, graphics_project, group_examples);

  cc_project_t daedalus_hub_project =
      addToolsDaedalusHub(cc, vulkan_project, graphics_project, telemetry_project, group_tools);

#else
  cc_project_t metal_project = addModulesMetalAPI(cc, group_modules);
  cc.project.addInputProject(metal_project, g_projects.core);
  addModulesMetalTest(cc, metal_project, graphics_project, group_tests);

  addExampleUI(cc, metal_project, graphics_project, group_examples);

  addExamplesMetal(cc, metal_project, graphics_project, group_examples);

  cc_project_t daedalus_hub_project =
      addToolsDaedalusHub(cc, metal_project, graphics_project, telemetry_project, group_tools);
#endif

  addToolsFixedFont(cc, group_tools);

  cc_default_generator("build");

  return 0;
}